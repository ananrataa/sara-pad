//
//  TutorCollectionViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 17/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class TutorCollectionViewCell: UICollectionViewCell {
    let courseImageView: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleToFill
        imgV.clipsToBounds = true
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let labelRoomName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: mainFont, size: 17)
        label.minimumScaleFactor = 0.5
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    let labelCourseCount: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(sukhumvitLightFont)", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
        label.minimumScaleFactor = 0.5
        label.textAlignment = .center
        label.textColor = .darkGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(courseImageView)
        contentView.addSubview(labelRoomName)
        contentView.addSubview(labelCourseCount)
        let w = frame.width*0.5
        let pad = frame.width*0.25
        contentView.addConstraintsWithFormat(format: "H:|-\(pad)-[v0]-\(pad)-|", views: courseImageView)
        contentView.addConstraintsWithFormat(format: "H:|-4-[v0]-4-|", views: labelRoomName)
        contentView.addConstraintsWithFormat(format: "H:|-4-[v0]-4-|", views: labelCourseCount)
        contentView.addConstraintsWithFormat(format: "V:|-8-[v0(\(w))]-8-[v1]", views: courseImageView, labelRoomName)
        contentView.addConstraintsWithFormat(format: "V:[v0]-4-|", views: labelCourseCount)
        
        contentView.layer.borderColor = UIColor.gray.cgColor
        contentView.layer.borderWidth = 0.5
        contentView.layer.cornerRadius = 7
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setView(courseLib: SPCourseLibrary) {
        labelRoomName.text = courseLib.roomName
        if SP.shared.currentLanguage() == "en" {
            labelCourseCount.text = "Total courses: \(courseLib.courseCount)"
        } else {
            labelCourseCount.text = "คอร์สที่เปิดสอน: \(courseLib.courseCount)"
        }
        
        if let url = URL(string: courseLib.roomLogo) {
            do {
                courseImageView.image = UIImage(data: try Data(contentsOf: url))
            } catch {
                
            }
        }
        courseImageView.layer.cornerRadius = courseImageView.frame.height/2
    }
}

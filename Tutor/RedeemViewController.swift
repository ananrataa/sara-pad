//
//  RedeemViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 16/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import Lottie

class RedeemViewController: UIViewController {
    @IBOutlet weak var effectView: UIVisualEffectView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var textRedeemCode: UITextField!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var animateView: UIView!
    @IBOutlet weak var labelStatus: UILabel!
    
    let lotView: LOTAnimationView = {
        let lotView = LOTAnimationView(name: LottieName.checkmark_animation.rawValue)
        lotView.contentMode = .scaleAspectFit
        lotView.backgroundColor = .clear
        lotView.clipsToBounds = true
        return lotView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard MemberInfo.isSignin() else {
            dismiss(animated: true) {
                NotificationCenter.default.post(name: .signIn, object: nil)
            }
            return
        }
        //LotView
        animateView.alpha = 0
        animateView.layer.cornerRadius = 7
        labelStatus.text = SP.shared.currentLanguage() == "en" ? "Redemption Successful!" : "ลงทะเบียนสำเร็จ"
        animateView.addSubview(lotView)
        let x = (animateView.frame.width - 100)/2
        animateView.addConstraintsWithFormat(format: "H:|-\(x)-[v0]-\(x)-|", views: lotView)
        animateView.addConstraintsWithFormat(format: "V:|-12-[v0(100)]", views: lotView)
        
        
        if let memberInfo = DBManager.selecttMemberMenuInfo(), memberInfo.count >= 2 {
            labelEmail.text = memberInfo[1] as? String ?? ""
        }
        bgView.layer.borderColor = UIColor.white.cgColor
        bgView.layer.cornerRadius = 7
        effectView.alpha = 0.95
        //TextField
        textRedeemCode.addTarget(self, action: #selector(textfieldChanged(_:)), for: .editingChanged)
        textRedeemCode.delegate = self
        textRedeemCode.placeholder = "ป้อนรหัส Redeem Code"
        let h = textRedeemCode.frame.height*0.8
        let imgView = UIImageView(image: UIImage(named: "password")!)
        imgView.frame = CGRect(x: 0, y: 0, width: h, height: h)
        textRedeemCode.leftView = imgView
        textRedeemCode.leftViewMode = .always
    }

    @IBAction func login(_ sender: Any) {
        guard let redeemCode = textRedeemCode.text, redeemCode.count > 0 else {
            SVProgressHUD.showInfo(withStatus: "Please input redeem code!")
            return
        }
        RoomAPI.shared.redeemCode(code: redeemCode) {[weak self] (code, message1, message2) in
            switch code {
            case "200": self?.redeemSuccess()
            case "201": self?.redeemSuccess(message: message1)
            case "203", "": self?.redeemFailed(errorCode: "203", errorText: String.localizedString(key: "redeem203"))
            case "202","204","205", "209":self?.redeemFailed(errorCode: code, errorText: String.localizedString(key: "redeem\(code)"))
            default:self?.redeemFailed(errorCode: code, errorText: message1)
            }
        }
    }
//    200 = เปิดใช้งานสำเร็จ
//    201 = เปิดใช้งานไม่สำเร็จ
//    202 = ผู้ใช้ไม่ถูกต้อง
//    203 = Redreem Code ไม่ถูกต้อง
//    204 = Redreem Code ถูกใช้งานโดยผู้อื่นเเล้ว
//    205 = ท่านเคยใช้งาน Redreem Code นี้เเล้ว
//    206 = กรุณาลงทะเบียนเป็นสมาชิกของระบบ
    @IBAction func cancel(_ sender: Any) {
        isRedeem = false
        dismiss(animated: true, completion: nil)
    }
    
    func redeemSuccess() {
        isRedeem = false
        DispatchQueue.main.async {
            self.lotView.setAnimation(named: LottieName.done.rawValue)
            self.labelStatus.text = String.localizedString(key: "redeem200")
            self.animateView.alpha = 1
            self.lotView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {[weak self] in
                self?.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    func redeemSuccess(message: String) {
        isRedeem = false
        DispatchQueue.main.async {
            self.lotView.setAnimation(named: LottieName.done.rawValue)
            self.labelStatus.text = String.localizedString(key: "redeem201")
            self.animateView.alpha = 1
            self.lotView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {[weak self] in
                self?.dismiss(animated: true) {
                    let vc = WebOSViewController(urlPath: message, title: "")
                    let nav = UINavigationController(rootViewController: vc)
                    if let topVC = SP.shared.topViewController() {
                        topVC.present(nav, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    func redeemFailed(errorCode: String, errorText: String) {
        DispatchQueue.main.async {
            self.labelStatus.text = "\(errorCode): \(errorText)"
            self.animateView.alpha = 1
            self.lotView.setAnimation(named: LottieName.error_cross.rawValue)
            self.lotView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {[weak self] in
                self?.animateView.alpha = 0
            })
        }
    }
    func memberRegister(roomId: String, roomName: String) {
        guard let memberId = DBManager.selectMemberID() else {
            SVProgressHUD.showError(withStatus: "ลงทะเบียนไม่สำเร็จ")
            return
        }
        RoomAPI.shared.postLibraryApply(roomId: roomId, memberId: memberId) {[weak self] (json) in
            print(json.debugDescription)
            if let json = json, let messageCode = json["MessageCode"] as? String {
                let path = json["MessageText"] as? String ?? ""
                if messageCode == "200" {
                    let titleText = "\(String.localizedString(key: "Library")): \(roomName)"
                    let vc = WebOSViewController(urlPath: path, title: titleText)
                    let nav = UINavigationController(rootViewController: vc)
                    self?.present(nav, animated: true, completion: nil)
                } else {
                    SVProgressHUD.showError(withStatus: "Error\(messageCode): \(path)")
                }
            } else {
                SVProgressHUD.showError(withStatus: "ลงทะเบียนไม่สำเร็จ")
            }
        }
    }
}

extension RedeemViewController: UITextFieldDelegate {
    @objc func textfieldChanged(_ textField: UITextField) {
        buttonLogin.isEnabled = !(textField.text?.isEmpty ?? true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        buttonLogin.isEnabled = !(textField.text?.isEmpty)!
        textField.resignFirstResponder()
        return true
    }
    
}

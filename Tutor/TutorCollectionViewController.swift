//
//  TutorCollectionViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 14/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SafariServices

class TutorCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AlertHandler {
    var courseLibs: [SPCourseLibrary] = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData({
                    SVProgressHUD.dismiss()
                })
            }
        }
    }
    var loadIndex: Int = 0 {
        didSet {
            RoomAPI.shared.courseLibralies(index: loadIndex) {[weak self] (items) in
                self?.courseLibs = items
                DispatchQueue.main.async {
//                    if let url = URL(string: "https://drive.google.com/file/d/1OSJPKYZQ2DxqNQu4DkC4s33VGrJpLvTI/view?usp=sharing") {
//                        let svc = SFSafariViewController(url: url)
//                        self?.present(svc, animated: true, completion: nil)
//                    }
//                    let vc = WebOSViewController(urlPath: "https://drive.google.com/file/d/1OSJPKYZQ2DxqNQu4DkC4s33VGrJpLvTI/view?usp=sharing")
//                    self?.present(vc, animated: true, completion: nil)

                    self?.collectionView.reloadData()
                    if items.count == 0 {
                        let message = SP.shared.currentLanguage() == "en" ? "No schools available yet.\nTry again later." : "ยังไม่มีโรงเรียนกวดวิชา.\nลองใหม่ในภายหลัง."
                        self?.presentAlert(withTitle: "Tutor School", message: message)
                    }
                    
                }
            }
        }
    }
    lazy var libButton: UIButton = {
        let btn = UIButton()
        btn.addTarget(self, action: #selector(libraryTapped), for: .touchUpInside)
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 0.35
        btn.clipsToBounds = true
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupLeftMenuNotifications()
        if SP.shared.currentLanguage() == "en" {
            title = "Tutor School"
        } else {
            title = "สถาบันกวดวิชา"
        }
        collectionView?.backgroundColor = UIColor(hexString: "#efefef")
        collectionView?.contentInset = UIEdgeInsets(top: 12, left: 12, bottom: 0, right: 12)
        
        collectionView?.register(TutorCollectionViewCell.self, forCellWithReuseIdentifier: TutorCollectionViewCell.identification)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SP.shared.loadSVProgressHUD(status: "Loading...") {
            self.loadIndex = 1
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .libraryChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: .history, object: nil)
        NotificationCenter.default.removeObserver(self, name: .signOut, object: nil)
        NotificationCenter.default.removeObserver(self, name: .signIn, object: nil)
        NotificationCenter.default.removeObserver(self, name: .setting, object: nil)
        NotificationCenter.default.removeObserver(self, name: .help, object: nil)
        NotificationCenter.default.removeObserver(self, name: .about, object: nil)
        NotificationCenter.default.removeObserver(self, name: .memberProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: .redeemCourse, object: nil)
    }
    
    func setupNavigation() {
        LightTheme.shared.applyNavigationBar(vc: self)
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .done, target: self, action: #selector(menuTapped))
        let h = (navigationController?.navigationBar.frame.height)!*0.8
        libButton.frame = CGRect(x: 0, y: 0, width: h, height: h)
        let image = Utility.rescale(UIImage(named: "default_library"), scaledTo: .init(width: h, height:h))
        libButton.setImage(image, for: .normal)
        if let curLibraryId = UserDefaults.standard.string(forKey: KeyCuurrentUsedLribaryID) {
            DB.getRoomLogo(id: curLibraryId) {[weak self] (img) in
                if let img = img {
                    let image = Utility.rescale(img, scaledTo: .init(width: h, height:h))
                    DispatchQueue.main.async {
                        self?.libButton.setImage(image, for: .normal)
                        self?.libButton.layer.cornerRadius = h/2
                        let libBarButtonItem = UIBarButtonItem(customView: (self?.libButton)!)
                        self?.navigationItem.leftBarButtonItems = [leftBarButtonItem, libBarButtonItem]
                    }
                }
            }
        }
    }
    @objc func menuTapped() {
        if let appDel = UIApplication.shared.delegate as? AppDelegate {
            appDel.drawerContainer.toggle(.left, animated: true) { (_) in
                
            }
        }
    }
    @objc func libraryTapped() {
        guard let roomId = UserDefaults.standard.string(forKey: KeyCuurrentUsedLribaryID), let roomData = DB.getRoom(id: roomId)  else {return}
        let memberId = SP.shared.getMemberId()
        RoomAPI.shared.libraryInfo(roomId: roomId, memberId: memberId) { (json) in
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            if let json = json, let libraryDetailVC = mainSB.instantiateViewController(withIdentifier: "LibraryDetailVC") as? LibraryDetailVC {
                libraryDetailVC.libraryTitle = roomData.name ?? ""
                libraryDetailVC.libraryID = roomId
                libraryDetailVC.jsonLibraryInfo = json
                RoomAPI.shared.libraryMediaItems(roomId: roomId, completion: {[weak self] (json) in
                    if let json = json {
                        libraryDetailVC.jsonMediaItems = json
                        DispatchQueue.main.async {
                            self?.navigationController?.pushViewController(libraryDetailVC, animated: true)
                        }
                    }
                })
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return courseLibs.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorCollectionViewCell.identification, for: indexPath) as! TutorCollectionViewCell
    
        cell.setView(courseLib: courseLibs[indexPath.item])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let deviceName = UIDevice.modelName
        let n: CGFloat = deviceName.contains("iPad") ? 4 : (deviceName.contains("iPhone 5") ? 2 : 2)
        let w = (collectionView.frame.width - 24 - (n-1)*8)/n
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedLib = courseLibs[indexPath.item]
        ///set Library Info to ViewController
        let memberId = DBManager.selectMemberID() ?? ""
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        if let libraryDetailVC = mainSB.instantiateViewController(withIdentifier: "LibraryDetailVC") as? LibraryDetailVC {
            libraryDetailVC.libraryTitle = selectedLib.roomName
            libraryDetailVC.libraryID = selectedLib.roomId
            RoomAPI.shared.libraryInfo(roomId: selectedLib.roomId, memberId: memberId) { (jsonDict) in
                if var json = jsonDict {
                    if (json["SYMBOL"] as? String) ?? "" == "" {
                        json["SYMBOL"] = selectedLib.roomLogo
                    }
                    libraryDetailVC.jsonLibraryInfo = json
                    RoomAPI.shared.libraryCourses(roomId: selectedLib.roomId, completion: { (jsonArray) in
                        libraryDetailVC.jsonMediaItems = jsonArray
                        DispatchQueue.main.async {[weak self] in
                            self?.navigationController?.pushViewController(libraryDetailVC, animated: true)
                        }
                    })
                }
            }
        }
        
    }
}

//MARK:- Left Menu
extension TutorCollectionViewController {
    func setupLeftMenuNotifications() {
        NotificationCenter.default.addObserver(forName: .libraryChanged, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                self?.tabBarController?.selectedIndex = 0
            }
        }
        
        NotificationCenter.default.addObserver(forName: .signOut, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let alertTitle = "\(String.localizedString(key: "Youwant")) \(String.localizedString(key: "SignOut"))"
                self?.confirmAlert(title: alertTitle, messgae: "", okTitle: "OK", selectedAction: { (btn) in
                    if btn == .OK {
                        let memberId = DBManager.selectMemberID() ?? ""
                        MemberAPi.shared.memberSignOut(memberId: memberId, completion: {[weak self] (code) in
                            if code == 200 {
                                DispatchQueue.main.sync {
                                    DBManager.deleteAllRowInMemberTable()
                                    DBManager.deleteAllRowInLibraryTable()
                                    UserDefaults.standard.removeObject(forKey: LastLeaveApp)
                                    UserDefaults.standard.removeObject(forKey: AutoSyncPdfAnnatationKey)
                                    UserDefaults.standard.synchronize()
                                    
                                    //Line
                                    Login.sharedInstance().canLoginWithLineApp()
                                    //FB
                                    FBSDKLoginManager.init().logOut()
                                    
                                    //start splash
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    if let vc = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreen") as? SplashScreen {
                                        self?.view.window?.rootViewController = vc
                                    }
                                }
                            }
                        })
                    }
                })
            }
        }
        NotificationCenter.default.addObserver(forName: .setting, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let vc = SettingViewController()
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: {
                    
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: .signIn, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                NotificationCenter.default.removeObserver(self!, name: .signIn, object: nil)
            }
            if self?.tabBarController?.selectedIndex == 3 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "SignInVC") as? SignInVC {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .help, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "Help") as? Help {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }            }
        }
        
        NotificationCenter.default.addObserver(forName: .about, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "AboutTVC") as? AboutTVC {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .history, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let vc = HistoryViewController()
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .memberProfile, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                guard let memberId = DBManager.selectMemberID() else {return}
                let vc = MemberProfileViewController(memberId: memberId)
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .redeemCourse, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 3 {
                let vc = RedeemViewController()
                vc.modalPresentationStyle = .overFullScreen
                self?.present(vc, animated: true, completion: nil)
            }
        }
    }
}


//
//  TutorDetailViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 18/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class TutorDetailViewController: UIViewController {

    var curCourse: SPPreviewMedia
    
    init(course: SPPreviewMedia) {
        curCourse = course
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


}

//
//  QRCodeViewController.swift
//  Anapanasati
//
//  Created by Anan Ratanasethakul on 12/12/18.
//  Copyright © 2018 Anan Ratanasethakul. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController, AlertHandler {
    @IBOutlet weak var qrView: UIView!
    @IBOutlet weak var effectView: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        effectView.alpha = 0
        
    }

    func scanQR(completion: @escaping (String?)->()) {
        UIView.animate(withDuration: 0.2, animations: {
            self.effectView.alpha = 0.75
        }) { (_) in
            do {
                try QRCode.shared.scanQRCode(in: self.qrView) {[weak self] (msg) in
                    if let msg = msg {
                        self?.dismiss(animated: true, completion: {
                            completion(msg)
                        })
                    } else {
                        SVProgressHUD.showError(withStatus: "Failed to scan QR Code!")
                    }
                }
            } catch  {
                print("Failed to scan QR Code.")
            }
        }
    }
    @IBAction func done(_ sender: Any) {
        QRCode.shared.dismiss()
        dismiss(animated: true, completion: nil)
    }
    
}

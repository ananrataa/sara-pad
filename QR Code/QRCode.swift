//
//  QRCode.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 12/12/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import AVFoundation

enum error: Error {
    case noCamera
    case initFail
}
class QRCode: NSObject, AVCaptureMetadataOutputObjectsDelegate {
    static let shared: QRCode = QRCode()
    private override init(){}
    var avCaptureSession: AVCaptureSession!
    var videoView: UIView!
    var completion: ((String?)->())?
    func scanQRCode(in videoView: UIView, completion: @escaping (String?)->()) throws {
        guard let avCaptureDevice = AVCaptureDevice.default(for: .video) else {
            print("No camera.")
            throw error.noCamera
        }
        
        guard let avCaptureInput = try? AVCaptureDeviceInput(device: avCaptureDevice) else {
            print("Failed to init camera.")
            throw error.initFail
        }
        self.videoView = videoView
        self.completion = completion
        let avCaptureMetaDataOutput = AVCaptureMetadataOutput()
        avCaptureMetaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        avCaptureSession = AVCaptureSession()
        avCaptureSession.addInput(avCaptureInput)
        avCaptureSession.addOutput(avCaptureMetaDataOutput)
        
        avCaptureMetaDataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        let avCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        avCaptureVideoPreviewLayer.videoGravity = .resizeAspectFill
        avCaptureVideoPreviewLayer.frame = videoView.bounds
        videoView.layer.addSublayer(avCaptureVideoPreviewLayer)
        avCaptureSession.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if let machineReadableCode = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            if machineReadableCode.type == .qr {
                AudioServicesPlaySystemSound(4095)
                UIView.animate(withDuration: 1.5, animations: {
                    self.videoView.alpha = 0
                }) { (_) in
                    if let completion = self.completion {
                        if let subLayers = self.videoView.layer.sublayers {
                            subLayers.forEach { (caLayer) in
                                if caLayer is AVCaptureVideoPreviewLayer {
                                    caLayer.removeFromSuperlayer()
                                }
                            }
                        }
                        self.avCaptureSession.stopRunning()
                        completion(machineReadableCode.stringValue)
                    }
                }
            } else {
                if let completion = completion {
                    completion(nil)
                }
            }
        } else {
//            SVProgressHUD.showInfo(withStatus: "No QR code is detected")
        }
    }
    func dismiss() {
        if avCaptureSession != nil, avCaptureSession.isRunning {
            avCaptureSession.stopRunning()
        }
    }
}


//MARK:- QR Code detector
extension QRCode {
    func detectQRCode(_ image: UIImage?) -> [CIFeature]? {
        if let image = image, let ciImage = CIImage.init(image: image){
            var options: [String: Any]
            let context = CIContext()
            options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
            let qrDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: options)
            if ciImage.properties.keys.contains((kCGImagePropertyOrientation as String)){
                options = [CIDetectorImageOrientation: ciImage.properties[(kCGImagePropertyOrientation as String)] ?? 1]
            }else {
                options = [CIDetectorImageOrientation: 1]
            }
            let features = qrDetector?.features(in: ciImage, options: options)
            return features
        }
        return nil
    }
    
    /*Usage:
     if let features = detectQRCode(#imageLiteral(resourceName: "qrcode")), !features.isEmpty{
     for case let row as CIQRCodeFeature in features{
     print(row.messageString ?? "nope")
     }
     }
 
     */
}

extension UIImage {
    func parseQR() -> [String] {
        guard let image = CIImage(image: self) else {
            return []
        }
        let detector = CIDetector(ofType: CIDetectorTypeQRCode,
                                  context: nil,
                                  options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        
        let features = detector?.features(in: image) ?? []
        
        return features.compactMap { feature in
            return (feature as? CIQRCodeFeature)?.messageString
        }
    }
}

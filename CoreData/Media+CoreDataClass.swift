//
//  Media+CoreDataClass.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 3/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Media)
public class Media: NSManagedObject {
    
    func getAds() -> [MediaAd] {
        if let data = self.mediaAds?.allObjects as? [MediaAd] {
            return data
        }
        return []
    }
    
    func updateAds(adId: String) {
        guard let context = self.managedObjectContext else {
            return
        }
        let newAd = findAd(adId: adId) ?? MediaAd(context: context)
        newAd.adId = adId
        
        let adsSet = self.mutableSetValue(forKey: "mediaAds")
        adsSet.add(newAd)
        
        DB.saveData()
    }
    
    func findAd(adId: String) -> MediaAd? {
        return getAds().filter{$0.adId ?? "" == adId}.first
    }
    
    func clearAds() {
        let ads = getAds()
        for ad in ads {
            self.removeFromMediaAds(ad)
            self.managedObjectContext!.delete(ad)
        }
        DB.saveData()
    }
}

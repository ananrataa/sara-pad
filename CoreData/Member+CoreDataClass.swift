//
//  Member+CoreDataClass.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 13/3/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Member)
public class Member: NSManagedObject {
    func isEqual(data: ReadingData) -> Bool {
        return
            self.memberId ?? "" == data.memberId &&
                self.mediaId ?? "" == data.mediaId &&
                self.date ?? "" == data.date &&
                self.page == data.page
    }
}

extension DB {
    class func saveReadingData(data: ReadingData) {
        guard let context = context else {return}
        var dbData = DB.getReadingData(data: data)
        if dbData == nil {
            dbData = Member.init(context: context)
            dbData?.memberId = data.memberId
            dbData?.mediaId = data.mediaId
            dbData?.date = data.date
            dbData?.page = Int16(data.page)
            dbData?.status = 0
        }
        
        dbData?.duration += data.duration
        saveData()
    }
    class func readingRecords() -> [Member] {
        guard let context = context else {return []}
        let id = SP.shared.getMemberId()
        do {
            let members = try context.fetch(NSFetchRequest<Member>(entityName: "Member"))
            return members.filter{$0.memberId ?? "" == id}
        } catch  {
            return []
        }
    }
    class func getReadingData(data: ReadingData) -> Member? {
        //status 0 = open
        let filteredData = readingRecords().filter{$0.isEqual(data: data) && $0.status == 0}
        return filteredData.first
    }
    
    class func postReadingData() {
        var parameters = [[String: String]]()
        let allData = readingRecords().filter{$0.status == 0}
        allData.forEach { (data) in
            let param: [String: String] = [
                "memberId": data.memberId ?? "",
                "mediaId": data.mediaId ?? "",
                "date": data.date ?? "",
                "page": "\(data.page)",
                "seconds": "\(data.duration)"]
            parameters.append(param)
        }
    }
}


class ReadingData {
    var memberId: String = ""
    var mediaId: String = ""
    var date: String = ""
    var page: Int = 0
    var duration: Double = 0
}

/*
//declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid

let parameters: [String: String] = ["name": nametextField.text, "password": passwordTextField.text]

//create the url with URL
let url = URL(string: "http://myServerName.com/api")! //change the url

//create the session object
let session = URLSession.shared

//now create the URLRequest object using the url object
var request = URLRequest(url: url)
request.httpMethod = "POST" //set http method as POST

do {
    request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
    
} catch let error {
    print(error.localizedDescription)
}

request.addValue("application/json", forHTTPHeaderField: "Content-Type")
request.addValue("application/json", forHTTPHeaderField: "Accept")

//create dataTask using the session object to send data to the server
let task = session.dataTask(with: request, completionHandler: { data, response, error in
    
    guard error == nil else {
        return
    }
    
    guard let data = data else {
        return
    }
    
    do {
        //create json object from data
        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
            print(json)
            // handle json...
        }
        
    } catch let error {
        print(error.localizedDescription)
    }
})
task.resume()
*/

//
//  MemberData+CoreDataClass.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//
//

import Foundation
import CoreData

@objc(MemberData)
public class MemberData: NSManagedObject {
    func updateData(membeData: NSDictionary) {
        
    }
    
    @objc func memberRooms() -> [RoomData] {
        if let data = self.rooms?.allObjects as? [RoomData] {
            return data.sorted(by: {$0.index < $1.index})
        }
        return []
    }
    @available(iOS 11.0, *)
    @objc func addRoom(info: NSDictionary) -> RoomData? {
        let id = (info["ROOMID"] as? String) ?? ""
        if let newRoom = DB.addRoom(info: info, curRoom: self.getRoom(id: id)) {
            newRoom.member = self
            self.rooms?.adding(newRoom)
            DB.saveData()
            return newRoom
        }
        return nil
    }
    func clearRoom() {
        if let rooms = self.rooms?.allObjects as? [RoomData], let context = DB.context {
            let roomSet = self.mutableSetValue(forKey: "rooms")
            rooms.forEach { (room) in
                roomSet.remove(room)
                context.delete(room)
            }
            DB.saveData()
        }
    }
    func getRoom(id: String) -> RoomData? {
        if let rooms = self.rooms?.allObjects as? [RoomData] {
            return rooms.filter({$0.id == id}).first
        }
        return nil
    }
    
    
}

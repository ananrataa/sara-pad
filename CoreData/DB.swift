//
//  DB.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import CoreData

let Shelf_Public = "3"
let Shelf_Member = "1"
let Shelf_Sale = "0"
let Shelf_Preset = "2"
@available(iOS 11.0, *)
class DB: NSObject {
    fileprivate static let shared = DB()
    static let context: NSManagedObjectContext? = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil}
        return appDelegate.persistentContainer.viewContext
    }()
    
    @objc class func saveData() {
        if let context = DB.context {
            if  context.hasChanges {
                do {
                    try context.save()
                } catch let error as NSError {
                    print("Ops there was an error \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:- Media
    class func updateMedia(media: SPMedia, memberId: String, info: SPMediaDownload, subjectId: String? = "", cId: String? = "", completion:()->()) {
        guard let context = context else {return}
        let medias = offlineMedias().filter({
            $0.mediaId ?? "" == media.mediaId &&
                $0.memberId ?? "" == memberId &&
                $0.libraryId ?? "" == media.roomId &&
                $0.subjectId ?? "" == subjectId &&
                $0.cId ?? "" == cId})
        var curMedia = medias.first
        if curMedia == nil {
            curMedia = OfflineShelfData(context: context)
        }
        curMedia?.author = media.author
        curMedia?.coverUrl = media.coverFile?.url
        curMedia?.dateBorrow = info.rentDate
        curMedia?.dateExp = info.expDate
        curMedia?.fileUrl = media.mediaFile?.url
        curMedia?.libraryId = media.roomId
        curMedia?.libraryName = media.roomName
        curMedia?.mediaId = media.mediaId
        curMedia?.mediaTitle = media.mediaName
        curMedia?.mediaType = media.mediaKind
        curMedia?.memberId = memberId
        if let str = media.mediaFile?.url {
            curMedia?.originalFileName = NSString(string: str).lastPathComponent
        }
        curMedia?.publisherName = media.publisherName
        curMedia?.shelfId = ((subjectId?.count)! > 0) ? Shelf_Preset : info.shelfId
        curMedia?.subjectId = subjectId
        //file path
        curMedia?.originalFileName = NSString(string: media.mediaFile?.url ?? "").lastPathComponent
        if let path = SP.shared.getLocalMediaPath(roomId: media.roomId, fileUrl: media.mediaFile?.url ?? "") {
            curMedia?.filePath = path
        } else {
            curMedia?.filePath = nil
        }
        //cover path
        let folderName = "\(sarapadFloderName)/\(curMedia?.libraryId ?? "")"
        let fileName = "\(memberId)_\(curMedia?.mediaId ?? "")_thumb.png"
        let filePath = "\(folderName)/\(fileName)"
        curMedia?.coverPath = filePath
        curMedia?.isNewDownload = true
        //Real Books
        curMedia?.cId = info.cId
        saveData()
        completion()
        
    }
    class func updatePresetMedia(media: SPPreviewMedia, memberId: String, subject: PresetSubject, completion:(()->())?) {
        guard let context = context else {return}
        let medias = offlineMedias().filter({
            $0.mediaId ?? "" == media.mediaId &&
                $0.memberId ?? "" == memberId &&
                $0.libraryId ?? "" == media.roomId &&
                $0.subjectId ?? "" == subject.subjectId &&
                $0.shelfId == Shelf_Preset})
        var curMedia = medias.first
        if curMedia == nil {
            curMedia = OfflineShelfData(context: context)
            curMedia?.isNewDownload = true
        }
        curMedia?.author = media.author
        curMedia?.coverUrl = media.mediaCoverUrl
        curMedia?.dateBorrow = media.rentDate
        curMedia?.dateExp = media.rentExpire
        curMedia?.libraryId = media.roomId
        if let room = getRoom(id: media.roomId) {
            curMedia?.libraryName = room.name ?? ""
        }
        curMedia?.mediaId = media.mediaId
        curMedia?.mediaTitle = media.mediaName
        curMedia?.mediaType = media.mediaKind
        curMedia?.memberId = memberId
        curMedia?.shelfId = Shelf_Preset
        curMedia?.subjectId = subject.subjectId
        curMedia?.subjectCode = subject.subjectCode
        curMedia?.subjectName = subject.subjectName
        curMedia?.year = subject.year
        curMedia?.semester = subject.semestor
        curMedia?.sectionCode = subject.sectionCode
        //No file path
        
        //cover path
        let folderName = "\(sarapadFloderName)/\(curMedia?.libraryId ?? "")"
        let fileName = "\(memberId)_\(curMedia?.mediaId ?? "")_thumb.png"
        let filePath = "\(folderName)/\(fileName)"
        curMedia?.coverPath = filePath
        
        saveData()
        if let completion = completion {
            completion()
        }
    }
    class func deleteOfflineMedia(media: OfflineShelfData) {
        guard let context = context else { return }
        Utility.removeFile(inDirectory: media.coverPath)
        Utility.removeFile(inDirectory: media.filePath)
        context.delete(media)
        saveData()
    }
    class func deletePresetMedia(subject: PresetSubject, media: SPPreviewMedia, memberId: String) {        
        let medias = offlineMedias().filter({
            $0.mediaId ?? "" == media.mediaId &&
                $0.memberId ?? "" == memberId &&
                $0.libraryId ?? "" == media.roomId &&
                $0.subjectId ?? "" == subject.subjectId &&
                $0.shelfId == Shelf_Preset})
        medias.forEach { (offlineMedia) in
            deleteOfflineMedia(media: offlineMedia)
        }
    }
    
    class func offlineMedias() -> [OfflineShelfData] {
        guard let context = context else {return []}
        do {
            let medias = try context.fetch(NSFetchRequest<OfflineShelfData>(entityName: "OfflineShelfData"))
            return medias.sorted{$0.mediaId ?? "" > $1.mediaId ?? ""}
        } catch  {
            return []
        }
    }
    class func getMedia(id: String, memberId: String, roomId: String) -> OfflineShelfData? {
        return offlineMedias().filter({($0.mediaId ?? "" == id) && ($0.memberId ?? "" == memberId) && ($0.libraryId ?? "" == roomId)}).first
    }
    class func getMedia(id: String) -> OfflineShelfData? {
        return offlineMedias().filter({$0.mediaId ?? "" == id}).first
    }
    class func getMedia(curMedia: SPMedia, memberId: String) -> OfflineShelfData? {
        let id = curMedia.mediaId
        let roomId = curMedia.roomId
        
        return offlineMedias().filter{$0.mediaId == id && $0.libraryId == roomId && $0.memberId == memberId}.first
    }
    //MARK:- Member
    @objc class func getMemberInfo() -> MemberData? {
        guard DBManager.isMember(),
            let memberInfo = DBManager.selectMemberInfo(),
            let memberId = memberInfo["MEMBERID"] as? String
            else {
                return nil
        }
        return getMember(id: memberId)
    }
    @objc class func addMember(info: NSDictionary) -> MemberData? {
        guard let context = context,
            let id = info["MEMBERID"] as? String, id.count > 0
        else {return nil}
        let member = getMember(id: id)
        let newMember = member ?? MemberData(context: context)
        newMember.id = id
        newMember.dateOfBirth = (info["BRTHDATE"] as? String) ?? ""
        newMember.diaplayName = (info["MEMBER_FNAME"] as? String) ?? ""
        newMember.email = (info["EMAIL"] as? String) ?? ""
        newMember.telephone = (info["PHONENUMBER"] as? String) ?? ""
        newMember.userName = (info["USERNAME"] as? String) ?? ""
        saveData()
        //update rooms
        newMember.clearRoom()
        RoomAPI.shared.deviceLimitRent(items: 0, days: 0) { (items) in
            DispatchQueue.main.async {
                for item in items {
                    let newRoom = RoomData(context: context)
                    newRoom.id = item.roomId
                    newRoom.name = item.roomName
                    newRoom.rentLimit = Int16(item.limitRent)
                    newRoom.amount = Int16(item.amount)
                    newRoom.balance = Int16(item.balance)
                }
                saveData()
            }
        }
        return newMember
    }
    class func members() -> [MemberData] {
        guard let context = context else {
            return []
        }
        do {
            let data = try context.fetch(NSFetchRequest<MemberData>(entityName: "MemberData"))
            return data
        } catch  {
            return []
        }
    }
    @objc class func getMember(id: String) -> MemberData? {
        return members().filter({$0.id == id}).first
    }
    //Rooms
    @objc class func clearRooms() {
        if let member = getMemberInfo(), let context = context {
            let rooms = member.memberRooms()
            for room in rooms {
                context.delete(room)
            }
            saveData()
        }
    }
    @objc class func addRoom(info: NSDictionary, curRoom: RoomData?) -> RoomData? {
        guard let context = context else {
            return nil
        }
        let id = (info["ROOMID"] as? String) ?? ""
        let newRoom = curRoom ?? createNewRoom(id: id, context: context)
        newRoom.id = id
        newRoom.code = (info["ROOMCODE"] as? String) ?? ""
        newRoom.name = (info["ROOMNAME"] as? String) ?? ""
        newRoom.symbol = (info["SYMBOL"] as? String) ?? ""
        newRoom.defaultRoom = ((info["DEFAULT"] as? Int) ?? 0) == 1
        newRoom.typeId = (info["ROOMTYPEID"] as? String) ?? ""
        newRoom.typeCode = (info["ROOMTYPECODE"] as? String) ?? ""
        newRoom.typeName = (info["ROOMTYPENAME"] as? String) ?? ""
        newRoom.messageText = (info["MessageText"] as? String) ?? ""
        newRoom.index = (info["ROOID"] as? Int16) ?? 0
        newRoom.date = (info["DATE"] as? String) ?? ""
        saveData()
        return newRoom
    }
    @objc class func createNewRoom(id: String, context: NSManagedObjectContext) -> RoomData {
        //delete existing
        do {
            let data = try context.fetch(NSFetchRequest<RoomData>(entityName: "RoomData"))
            for room in data {
                if room.id == id {
                    context.delete(room)
                }
            }
        } catch  {
            
        }
        return RoomData(context: context)
    }
    @objc class func getRoom(id: String) -> RoomData? {
        guard let context = context else {
            return nil
        }
        do {
            let data = try context.fetch(NSFetchRequest<RoomData>(entityName: "RoomData"))
            return data.filter({$0.id ?? "" == id}).first
        } catch  {
            return nil
        }
    }
    @objc class func getRoomLogo(id: String, completion: @escaping (UIImage?)->()) {
        if let room = getRoom(id: id), let strUrl = room.symbol {
            RoomAPI.shared.getData(urlString: strUrl) { (data) in
                if let data = data as Data? {
                    completion(UIImage(data: data))
                    return
                }
            }
        } else {
            completion(nil)
        }
    }
    //MARK: Media
    class func addMedia(mediaId: String, pageInterval: Int) -> Media? {
        guard let context = context else {
            return nil
        }
        do {
            let medias = try context.fetch(NSFetchRequest<Media>(entityName: "Media"))
            let newMedia = medias.filter{$0.mediaId ?? "" == mediaId}.first ?? Media(context: context)
            newMedia.mediaId = mediaId
            newMedia.mediaName = ""
            newMedia.mediaURL = ""
            newMedia.lastUpdated = Date()
            newMedia.interval = Int16(pageInterval)
            saveData()
            return newMedia
        } catch  {
            
        }
        return nil
    }
    
    //MARK:- Advertising
    class func updateAds(adsId: String, image: UIImage, linkUrl: String) {
        guard let context = context else {
            return
        }
        let ads = getAds(adsId: adsId) ?? Advertise(context: context)
        ads.adId = adsId
        ads.adURL = linkUrl
        if let data:Data = image.pngData() {
            ads.offlineAd = data
        } else if let data:Data = image.jpegData(compressionQuality: 1.0) {
            ads.offlineAd = data
        }
        saveData()
    }
    class func roomAds() -> [Advertise] {
        guard let context = context else {return []}
        do {
            let ads = try context.fetch(NSFetchRequest<Advertise>(entityName: "Advertise"))
            return ads.sorted{$0.adId ?? "" > $1.adId ?? ""}
        } catch  {
            return []
        }
    }
    class func getAds(adsId: String) -> Advertise? {
        return (roomAds().filter{$0.adId ?? "" == adsId}).first
    }
    class func getPdfAds(for mediaId: String) -> ADPdf {
        guard let context = context else {return ADPdf(interval: 0, images:[])}
        do {
            let medias = try context.fetch(NSFetchRequest<Media>(entityName: "Media"))
            if let media = medias.filter({$0.mediaId ?? "" == mediaId}).first {
                if let ads = media.mediaAds?.allObjects as? [MediaAd] {
                    var imgAds = [ADImage]()
                    for ad in ads {
                        if let roomAd = getAds(adsId: ad.adId ?? ""),
                            let data = roomAd.offlineAd,
                            let img = UIImage(data: data),
                            let linkUrl = roomAd.adURL {
                            imgAds.append(ADImage(image: img, adLink: linkUrl, name: roomAd.adName ?? " "))
                        }
                    }
                    return ADPdf(interval: Int(media.interval), images: imgAds)
                }
            }
        } catch  {
        }
        return ADPdf(interval: 0, images:[])
    }
    
}

//MARK:-Extension string for expired date
extension String {
    func isExpired() -> Bool {
        guard let curDate = Utility.currentDateTime(withFormatter: "yyyyMMdd") else {return false}
        // "0" = no expired
        if self == "0" {
            return false
        }
        if self.count < 8 {
            return true
        }
        let expDate = String(self.prefix(8))
        //convert to int and compare
        return expDate.toInt() < curDate.toInt()
    }
}
//MARK:- Sync Preset Data
extension DB {
    class func removeExpiredMedias() {
        let allMedias = offlineMedias()
        allMedias.forEach { (media) in
            //expDate format:
            if let expDate = media.dateExp, !expDate.isEmpty {
                if expDate.isExpired() {
                    deleteOfflineMedia(media: media)
                }
            } else {
                deleteOfflineMedia(media: media)
            }
        }
    }
    class func syncPresetShelf(data: PresetShelf, memberId: String, completion:()->()) {
        data.items.forEach { (subject) in
            subject.items.forEach({ (media) in
                if media.rentExpire == "0" {
                    DB.deletePresetMedia(subject: subject, media: media, memberId: memberId)
                } else {
                    DB.updatePresetMedia(media: media, memberId: memberId, subject: subject, completion: nil)
                }
            })
        }
        completion()
    }
    
    class func getPresetShelfData(memberId: String, reponse:([String], [[String]], [[[OfflineShelfData]]])->()) {
        let memberData = offlineMedias().filter{$0.memberId ?? "" == memberId && $0.shelfId ?? "" == Shelf_Preset}
        let roomData = Dictionary(grouping: memberData, by: {$0.libraryName ?? ""})
        let libNames = Array(roomData.keys)
        
        let libData = Array(roomData.values)
        var subjectNames: [[String]] = []
        var subjectMedias:[[[OfflineShelfData]]] = []
        libData.forEach { (data) in
            let subjectData = Dictionary(grouping: data, by: {$0.subjectName ?? ""})
            subjectNames.append(Array(subjectData.keys))
            subjectMedias.append(Array(subjectData.values))
        }
        
        reponse(libNames, subjectNames, subjectMedias)
    }
}

//MARK:- SYnc Public Data
extension DB {
    class func syncPublicShelf(completion: @escaping ()->()) {
        DispatchQueue.main.async {
            let medias = offlineMedias().filter{$0.shelfId ?? "" == Shelf_Public}
            medias.forEach({ (m) in
                if (m.dateExp?.isExpired())! {
                    deleteOfflineMedia(media: m)
                }
            })
            completion()
        }
    }
}
//MARK:- Sync Member Data
extension DB {
    class func syncMemberShelf(data: MemberShelf, memberId: String, completion:@escaping ()->()) {
        DispatchQueue.main.async {
            //delete expire medias
            let medias = offlineMedias()
            medias.forEach({ (m) in
                if let exp = m.dateExp, exp.isExpired() {
                    deleteOfflineMedia(media: m)
                }
            })
            for room in data.roomMedias {
                for media in room.mediaItems {
                    if media.rentExpire.isExpired() {
                        //Delete OfflineMedia
                        let medias = offlineMedias().filter({
                            $0.mediaId ?? "" == media.mediaId &&
                                $0.memberId ?? "" == memberId &&
                                $0.libraryId ?? "" == media.roomId &&
                                $0.shelfId ?? "" == Shelf_Member &&
                                $0.cId ?? "" == media.cId})
                        medias.forEach({ (m) in
                            deleteOfflineMedia(media: m)
                        })
                    } else {
                        updateMemberMedia(media: media, memberId: memberId)
                    }
                }
            }
            completion()
        }
    }
    class func updateMemberMedia(media: SPPreviewMedia, memberId: String) {
        guard let context = context else {return}
        let medias = offlineMedias().filter({
            $0.mediaId ?? "" == media.mediaId &&
                $0.memberId ?? "" == memberId &&
                $0.libraryId ?? "" == media.roomId &&
                $0.subjectId ?? "" == "" &&
                $0.cId ?? "" == media.cId})
        var curMedia = medias.first
        if curMedia == nil {
            curMedia = OfflineShelfData(context: context)
            curMedia?.isNewDownload = true
        }
        curMedia?.author = media.author
        curMedia?.coverUrl = media.mediaCoverUrl
        curMedia?.dateBorrow = media.rentDate
        curMedia?.dateExp = media.rentExpire
        curMedia?.libraryId = media.roomId
        if let room = getRoom(id: media.roomId) {
            curMedia?.libraryName = room.name ?? ""
        }
        curMedia?.mediaId = media.mediaId
        curMedia?.mediaTitle = media.mediaName
        curMedia?.mediaType = media.mediaKind
        curMedia?.memberId = memberId
        curMedia?.shelfId = Shelf_Member
        curMedia?.subjectId = ""
        curMedia?.cId = media.cId
        //set external link when mediaKind != "D"
        if media.mediaKind.uppercased() != "D" && media.mediaKind.uppercased() != "M" {
            curMedia?.fileUrl = media.externalUrl
        }
        
        //cover path
        let folderName = "\(sarapadFloderName)/\(curMedia?.libraryId ?? "")"
        let fileName = "\(memberId)_\(curMedia?.mediaId ?? "")_thumb.png"
        let filePath = "\(folderName)/\(fileName)"
        curMedia?.coverPath = filePath
        
        if !FileManager.default.fileExists(atPath: filePath) {
            //create directory
            let documentPath = SP.shared.documentPath()
            let roomId = curMedia?.libraryId ?? ""
            let coverUrl = curMedia?.coverUrl ?? ""
            let logsPath = documentPath.appendingPathComponent("\(sarapadFloderName)/\(roomId)")
            
            do {
                try FileManager.default.createDirectory(atPath: logsPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError{
                print("Unable to create directory",error)
            }
            
            Utility.loadMediaCoverImage(fromURL: coverUrl, orHaveNSData: nil, withFileName: fileName, inDirectory: folderName)
            
        }
        
        saveData()
    }
}


extension DB {
    class func historyMedias() -> [HistoryData] {
        guard let context = context else {return []}
        do {
            let medias = try context.fetch(NSFetchRequest<HistoryData>(entityName: "HistoryData"))
            return medias
        } catch  {
            return []
        }
    }
    class func getHistoryData(mediaId: String) -> HistoryData? {
        let medias = historyMedias()
        guard medias.count > 0 else {return nil}
        return medias.filter{$0.mediaId ?? "" == mediaId}.first
    }
    class func saveHistoryView(media: SPMedia) {
        guard let context = context else {return}
        var newMedia = getHistoryData(mediaId: media.mediaId)
        if newMedia == nil {
            newMedia = HistoryData(context: context)
        }
        newMedia?.author = media.author
        newMedia?.categoryName = media.categoriesName
        newMedia?.coverUrl = media.coverFile?.url
        newMedia?.date = Date().stringWithFormat("yyyyMMdd", locale: "en_US_POSIX")
        newMedia?.mediaId = media.mediaId
        newMedia?.libraryId = media.roomId
        newMedia?.mediaTitle = media.mediaName
        newMedia?.memberId = ""
        saveData()
    }
    @objc class func getMediaCoverImage(_ mediaId: String) -> UIImage? {
        if let media = getHistoryData(mediaId: mediaId), let url = URL(string: media.coverUrl ?? "") {
            let imageView = UIImageView()
            imageView.sd_setImage(with: url)
            return imageView.image
        }
        return nil
    }
}

//
//  OfflineShelfData+CoreDataClass.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//
//

import Foundation
import CoreData

@objc(OfflineShelfData)

public class OfflineShelfData: NSManagedObject {
    func isOwnPurchase() -> Bool {
        guard let shelfId = self.shelfId, shelfId == Shelf_Sale, let expDate = self.dateExp else {return false}
        
        return expDate == "99999999999999"
    }
    
    func getDocPath() -> String? {
        guard let docPath = SP.shared.getMediaUrl(media: self), let mediaId = self.mediaId, let roomId = self.libraryId, let memberId = self.memberId,
            let _ = DB.getMedia(id: mediaId, memberId: memberId, roomId: roomId)
            else {return nil}
        let path = docPath.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path) {
            return path 
        } else {
            return nil
        }
    }
}

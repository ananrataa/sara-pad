//
//  PurchaseViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 1/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class PurchaseViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtBalance: UITextFieldX!
    
    var media: SPMedia
    var saleId: String?
    var memberData: MemberData?
    
    @objc var startDownload: (()->())?
    
    @objc init(media: SPMedia) {
        self.media = media
        super.init(nibName: nil, bundle: nil)
        if media.priceList.count > 0 {
            saleId = media.priceList[0].saleId
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        btnBuy.layer.borderWidth = 0.75
        btnBuy.layer.borderColor = UIColor.white.cgColor
        btnBuy.layer.cornerRadius = btnBuy.frame.height/2
        
        btnCancel.layer.borderWidth = 0.75
        btnCancel.layer.borderColor = UIColor.white.cgColor
        btnCancel.layer.cornerRadius = btnBuy.frame.height/2
        
        //Money bag
        if MemberInfo.isSignin(), let memberInfo = DB.getMemberInfo(), let memberId = memberInfo.id {
            self.memberData = memberInfo
            MemberAPi.shared.getMemberBag(memberId: memberId) { (balance) in
                DispatchQueue.main.async {
                    self.txtBalance.text = balance.formatPoints()
                }
            }
            txtBalance.layer.borderColor = UIColor.clear.cgColor
            txtBalance.layer.borderWidth = 0.35
            txtBalance.layer.cornerRadius = 5
            txtBalance.rightString = "★"
        } else {
            txtBalance.alpha = 0
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if pickerView.subviews.count >= 3 {
            pickerView.subviews[1].backgroundColor = .darkGray
            pickerView.subviews[2].backgroundColor = .darkGray
        }
    }
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func done(_ sender: UIButton) {
        SVProgressHUD.dismiss()
        if media.mediaKind.uppercased() == "CR" && pickerView.selectedRow(inComponent: 0) >= media.priceList.count {
            //Redeem Code
            let redeemVC = RedeemViewController()
            redeemVC.modalPresentationStyle = .overFullScreen
            dismiss(animated: true) {
                if let topVC = SP.shared.topViewController() {
                    topVC.present(redeemVC, animated: true, completion: {
                        
                    })
                }
            }
        } else {
            let alert = UIAlertController(title: "ยืนยันการซื้อ", message: "", preferredStyle: .alert)
            alert.addTextField { (txt) in
                txt.placeholder = "กรุณาใส่รหัสผ่านของคุณ"
                txt.isSecureTextEntry = true
            }
            alert.addAction(UIAlertAction(title: String.localizedString(key: "Cancel"), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: String.localizedString(key: "OK"), style: .default, handler: { (_) in
                if let txt = alert.textFields?.first, let password = txt.text {
                    if password.count > 0 {
                        self.startBookBuy(password: password)
                    } else {
                        ARProgressHUD.shared.show(with: .warning, info: "กรุณาใส่รหัสผ่านของคุณ")
                    }
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func startBookBuy(password: String) {
        guard
            let info = memberData,
            let memberId = info.id,
            let saleId = saleId
            else {return}
        
        RoomAPI.shared.bookBuy(memberId: memberId, roomId: media.roomId, saleId: saleId, password: password) { (messageCode) in
            switch messageCode {
            case 200:
                self.dismiss(animated: true) {
                    if let start = self.startDownload {
                        start()
                    }
                }
            case 201:
                SVProgressHUD.showError(withStatus: " ซื้อไม่สำเร็จ ")
            case 202:
                SVProgressHUD.showError(withStatus: " จำนวนเงินไม่พอ ")
            case 203:
                ARProgressHUD.show(error: "รหัสผ่านไม่ถูกต้อง\n")
            case 205:
                SVProgressHUD.showError(withStatus: " เคยซื้อแล้ว ")
            default:
                SVProgressHUD.showSuccess(withStatus: messageCode.toString())
            }
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if media.mediaKind.uppercased() == "CR" {
            return media.priceList.count + 1
        } else {
            return media.priceList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 60))
        lbl.font = UIFont(name: "Kanit-Thin", size: 32)
        lbl.textColor = .white
        lbl.textAlignment = .center
        if media.mediaKind.uppercased() == "CR" {
            if row < media.priceList.count {
                lbl.text = media.priceList[row].payType
            } else {
                lbl.text = SP.shared.currentLanguage() == "en" ? "Redeem Code" : "ใช้ Redeem Code"
            }
        } else {
            lbl.text = media.priceList[row].payType
        }
        return lbl
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row < media.priceList.count {
            saleId = media.priceList[row].saleId
        }
    }
}



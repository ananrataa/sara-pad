//
//  TagCollectionViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 29/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class TagCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    let font = UIFont(name: "Prompt", size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .light)
    lazy var att:[NSAttributedString.Key:Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor:UIColor(hexString: "#0096FF"), NSAttributedString.Key.backgroundColor:UIColor.black.withAlphaComponent(0.2)]

    var tagTapped: ((String)->())?
    var tags: [String]
    init(tags:[String]) {
        self.tags = tags
        let fl = UICollectionViewFlowLayout()
        fl.scrollDirection = .horizontal
        super.init(collectionViewLayout: fl)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: TagCollectionViewCell.identification)

    }


    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCollectionViewCell.identification, for: indexPath) as! TagCollectionViewCell
    
//        let attText = NSAttributedString(string: tags[indexPath.item], attributes: att)
        
        cell.tagLabel.text = tags[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = NSString(string: tags[indexPath.item]).size(withAttributes: att)
        return CGSize(width: min(collectionView.frame.width*0.5 - 8, size.width + 8), height: size.height + 4)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let tagTapped = self.tagTapped {
            SVProgressHUD.show()
            tagTapped(tags[indexPath.item])
        }
    }
}


class TagCollectionViewCell: BaseCell {
    let tagLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "Prompt", size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .light)
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor(hexString: "#0096FF")
        lbl.textColor = UIColor.white
        lbl.clipsToBounds = true
        return lbl
    }()
    
    override func setup() {
        super.setup()
        backgroundColor = .clear
        addSubview(tagLabel)
        addConstraintsWithFormat(format: "V:|-2-[v0]-2-|", views: tagLabel)
        addConstraintsWithFormat(format: "H:|-2-[v0]-2-|", views: tagLabel)
        
        tagLabel.layer.cornerRadius = 7// tagLabel.frame.height/2
        tagLabel.layer.borderWidth = 0.5
        tagLabel.layer.borderColor = UIColor(hexString: "#0096FF").cgColor
    }
}

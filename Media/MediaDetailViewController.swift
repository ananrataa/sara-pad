//
//  MediaDetailViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 24/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxCocoa
import PDFKit
import WebKit
import Lottie

var curSelectedMediaId: String?
var curSelectedShelfId: Int?
class MediaDetailViewController: UIViewController, AlertHandler {
    let mediaCell = "media_cell"
    let detailCell = "media_detail"
    let courseDetailCell = "course_detail"
    let suggestCell = "suggest_cell"
    let reviewCell = "review_cell"
    var isBannerShown = false
    
    let userBooksLimit:Int = {
        guard let data = Utility.checkUserLimitDayAndMedia() as? [Int], data.count > 1 else {return Int(MaximunFilesDownload)}
        return data[1]
    }()
    var curMedia: SPMedia!
    var numRows: Int = 0 {
        didSet {
            tableView.reloadData()
        }
    }
    var tagVC: TagCollectionViewController!
    var suggestMedias:[SPMediaSuggest] = []
    let headers:[String] = ["", String.localizedString(key: "Media_Details"), String.localizedString(key: "Suggestion"), String.localizedString(key: "Review")]
    let headers2:[String] = ["", String.localizedString(key: "Position"),String.localizedString(key: "Media_Details"), String.localizedString(key: "Suggestion"), String.localizedString(key: "Review")]

    let tableView: UITableView = {
        let tb = UITableView(frame: .zero, style: .plain)
        tb.separatorStyle = .none
        tb.rowHeight = UITableView.automaticDimension
        return tb
    }()
    
    //Lottie
    lazy var animationView: LOTAnimationView = {
        let lotView = LOTAnimationView(name: LottieName.books.rawValue)
        lotView.contentMode = .scaleAspectFit
        lotView.backgroundColor = .clear
        lotView.clipsToBounds = true
        return lotView
    }()
    lazy var labelTitle: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: sukhumvitLightFont, size: 15) ?? UIFont.preferredFont(forTextStyle: .title3)
        lbl.text = SP.shared.currentLanguage() == "en" ? "Download completed!" : "ดาว์นโหลดสำเร็จ"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.white
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()
    var memberId: String!
    var rentBalance: Int = 0
    var isMember = false
    var mediaId: String
    var roomId: String
    var curHeightSection1: CGFloat = 450
    var shelfSubject: String?
    var curLocSectionHeight: CGFloat = 120
    @objc var isNavigation: Bool = false {
        didSet {
            if isNavigation {
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
            }
        }
    }
    @objc var completion: ((SPMediaDownload)->())?
    
    @objc init(mediaId: String, roomId: String) {
        self.mediaId = mediaId
        self.roomId = roomId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        LightTheme.shared.applyNavigationBar(vc: self)
        //set nil to current media & shelf ID
        curSelectedMediaId = nil
        curSelectedShelfId = nil
        SP.shared.loadSVProgressHUD(status: "Loading...") {[weak self] in
            self?.setup()
            self?.loadMedia()
        }
    }
    deinit {
        print("Deinit")
    }
    func setup() {
        //if isNavigation
        //User
        memberId = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            memberId = DBManager.selectMemberID()
            isMember = true
        }
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        //UITableView
        view.addSubview(tableView)
        tableView.pin(to: view)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "MediaTableViewCell", bundle: nil), forCellReuseIdentifier: mediaCell)
        tableView.register(SuggestMediaTableViewCell.self, forCellReuseIdentifier: suggestCell)
        tableView.register(ReviewTableViewCell.self, forCellReuseIdentifier: reviewCell)
        tableView.register(UINib(nibName: "BooksTableViewCell", bundle: nil), forCellReuseIdentifier: BooksTableViewCell.identification)
    }
    func loadMedia() {
        RoomAPI.shared.browseMediaDetail(roomId: roomId, mediaId: mediaId, limit: userBooksLimit) {[weak self] (media) in
            if let media = media {
                //save history data
                DB.saveHistoryView(media: media)
                
                self?.curMedia = media
                self?.loadMediaSuggestion()
                //register media detail
                DispatchQueue.main.async {
                    self?.title = media.mediaName
                    if media.mediaKind.uppercased() == "CR" {
                        self?.curHeightSection1 = 200
                        self?.tableView.register(UINib.init(nibName: "CourseDetailTableViewCell", bundle: nil), forCellReuseIdentifier: (self?.courseDetailCell)!)
                    } else {
                        self?.curHeightSection1 = 450
                        if NSLocale.preferredLanguages.first?.lowercased() == "en" {
                            self?.tableView.register(UINib.init(nibName: "MediaDetailTableViewCellEN", bundle: nil), forCellReuseIdentifier: (self?.detailCell)!)
                        } else {
                            self?.tableView.register(UINib.init(nibName: "MediaDetailTableViewCellTH", bundle: nil), forCellReuseIdentifier: (self?.detailCell)!)
                        }
                    }
                    self?.numRows = 1
                }
            } else {
                self?.errorAlert(message: "Cannot show media!", duration: 2, completion: {
                    SVProgressHUD.dismiss()
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        }
        
    }
    func loadMediaSuggestion() {
        RoomAPI.shared.mediaSuggested(roomId: roomId, mediaId: mediaId, mediaKind: "") {[weak self] (items) in
            self?.suggestMedias = items
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.scrollToNearestSelectedRow(at: .top, animated: true)
                SVProgressHUD.dismiss()
            }
        }

    }
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    func refreshView() {
        if let media = curMedia {
            title = media.mediaName
            tableView.reloadData()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        self.tabBarController?.tabBar.isHidden = false
    }

    func showRegisterBanner() {
        //Show register Ads
        if !self.isMember && !curMedia.isDownLoad && !isBannerShown {
            isBannerShown.toggle()
            let popVC = FullScrrenViewController(index: 0)
            popVC.modalPresentationStyle = .overFullScreen
            popVC.registerDidTap = {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "SignInVC") as? SignInVC {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.present(popVC, animated: true, completion: nil)
            }
        }

    }
}

extension MediaDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (numRows == 0 || curMedia == nil) ? 0 : curMedia.mediaKind.lowercased() == "t" ? 5: 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numRows
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if curMedia.mediaKind.uppercased() == "T" {
            return [300, curLocSectionHeight, curHeightSection1, 300, 162][indexPath.section]
        } else if indexPath.section == 2 {
            let n: CGFloat = UIDevice.modelName.contains("iPad") ? 4 : 3
            let w = (tableView.frame.width - (n-1)*12)/n
            let h = w*4/3 + 88
            return h
        } else {
            return [300, curHeightSection1, 300, 162][indexPath.section]
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerTitles = curMedia.mediaKind.uppercased() == "T" ? headers2 : headers
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = .white
        let lbl = UILabel()
        let border = UIView()
        
        lbl.font = UIFont(name: mainFont, size: 21)
        lbl.textColor = .black
        border.backgroundColor = .black// (section == 1 ? .clear : .black)
        if curMedia.mediaKind.uppercased() == "CR" && section == 1 {
            lbl.text = SP.shared.currentLanguage() == "en" ? "Course Summary" : "คอร์สเรียน"
            border.backgroundColor = .black
        } else {
            lbl.text = headerTitles[section]
        }
        headerView.addSubview(lbl)
        headerView.addSubview(border)
        
        headerView.addConstraintsWithFormat(format: "H:|-12-[v0]|", views: lbl)
        headerView.addConstraintsWithFormat(format: "H:|-4-[v0]-4-|", views: border)
        headerView.addConstraintsWithFormat(format: "V:|[v0(39)][v1]|", views: lbl, border)
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if curMedia.mediaKind.uppercased() == "T" {
            switch indexPath.section {
            case 0: return mediaTableviewCell(indexPath: indexPath)
            case 1: return booksTableViewCell(indexPath: indexPath)
            case 2: return mediaDetailTableviewCell(indexPath: indexPath)
            case 3: return suggestMediaTableViewCell(indexPath: indexPath)
            case 4: return reviewTableViewCell(indexPath: indexPath)
            default: return UITableViewCell()
            }
        } else {
            switch indexPath.section {
            case 0: return mediaTableviewCell(indexPath: indexPath)
            case 1: return mediaDetailTableviewCell(indexPath: indexPath)
            case 2: return suggestMediaTableViewCell(indexPath: indexPath)
            case 3: return reviewTableViewCell(indexPath: indexPath)
            default: return UITableViewCell()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if curMedia.mediaKind.uppercased() == "T" && indexPath.section == 1 {
            guard let cell = tableView.cellForRow(at: indexPath) as? BooksTableViewCell else {return}
            tableView.deselectRow(at: indexPath, animated: true)
            curLocSectionHeight = curLocSectionHeight > 120 ? 120 : 270
            UIView.animate(withDuration: 0.3, animations: {
                tableView.beginUpdates()
                tableView.endUpdates()
            }) { (finished) in
                if finished {
                    UIView.animate(withDuration: 0.3, animations: { [weak self] in
                        cell.svDetail.alpha = (self?.curLocSectionHeight)! <= 120 ? 0 : 1
                    })
                }
            }
            
        }
    }
    func mediaTableviewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: mediaCell, for: indexPath) as! MediaTableViewCell
        cell.media = self.curMedia
        //Download button
        if curMedia.isPlayerMedia() {
            cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "play_video_en.png") : #imageLiteral(resourceName: "play_video_th.png"), for: .normal)
            cell.btnDownLoad.isEnabled = curMedia.isDownLoad
            if curMedia.isDownLoad {
                cell.btnDownLoad.addTarget(self, action: #selector(dowloadMedia), for: .touchUpInside)
            } else {
                cell.btnDownLoad.backgroundColor = UIColor(hexString: "#EBEBEB")
            }
        } else if curMedia.mediaKind.uppercased() == "T" {
            cell.btnDownLoad.setTitle("", for: .normal)
            cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "qr_code_eng.png") : #imageLiteral(resourceName: "qr_code"), for: .normal)
            cell.btnDownLoad.backgroundColor = .clear
            if curMedia.isQRCode {
                cell.btnDownLoad.addTarget(self, action: #selector(scanQRCode), for: .touchUpInside)
            }
            cell.btnDownLoad.isEnabled = curMedia.isQRCode
        } else if ["YU", "EP", "ES", "EL", "EV"].contains(curMedia.mediaKind.uppercased()) {
            if DB.getMedia(curMedia: curMedia, memberId: memberId) != nil {
                switch curMedia.mediaKind.uppercased() {
                case "YU": cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "play_youtube_eng") : #imageLiteral(resourceName: "play_youtube_thai"), for: .normal)
                case "EV", "ES": cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ?  #imageLiteral(resourceName: "play_video_en.png") : #imageLiteral(resourceName: "play_video_th.png") , for: .normal)
                default: cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "book_read_eng.png") : #imageLiteral(resourceName: "book_read.png"), for: .normal)
                }
                
                cell.btnDownLoad.removeTarget(self, action: #selector(dowloadMedia), for: .touchUpInside)
                cell.btnDownLoad.addTarget(self, action: #selector(openExternalMedia), for: .touchUpInside)
                cell.btnDownLoad.isEnabled = true
            } else {
                cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "book_download_eng.png") : #imageLiteral(resourceName: "book_download_thai.png"), for: .normal)
                cell.btnDownLoad.backgroundColor = .clear
                cell.btnDownLoad.isEnabled = curMedia.isDownLoad
                if curMedia.isDownLoad {
                    cell.btnDownLoad.removeTarget(self, action: #selector(readMedia), for: .touchUpInside)
                    cell.btnDownLoad.addTarget(self, action: #selector(dowloadMedia), for: .touchUpInside)
                }
            }
        } else if curMedia.mediaKind.uppercased() == "CR" {
            cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "course_enrol_eng") : #imageLiteral(resourceName: "course_enrol_th"), for: .normal)
            cell.btnDownLoad.backgroundColor = .clear
            cell.btnDownLoad.isEnabled = curMedia.isBuy
            if curMedia.isBuy {
                cell.btnDownLoad.addTarget(self, action: #selector(courseEnrolling(_:)), for: .touchUpInside)
            }
        } else {
            let isFileExist = SP.shared.isFileExist(media: curMedia, memberId: memberId)
            if isFileExist {
                cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "book_read_eng.png") : #imageLiteral(resourceName: "book_read.png"), for: .normal)
                cell.btnDownLoad.removeTarget(self, action: #selector(dowloadMedia), for: .touchUpInside)
                cell.btnDownLoad.addTarget(self, action: #selector(readMedia), for: .touchUpInside)
                cell.btnDownLoad.isEnabled = true
            } else {
                cell.btnDownLoad.setImage(SP.shared.currentLanguage() == "en" ? #imageLiteral(resourceName: "book_download_eng.png") : #imageLiteral(resourceName: "book_download_thai.png"), for: .normal)
                cell.btnDownLoad.backgroundColor = .clear
                cell.btnDownLoad.isEnabled = curMedia.isDownLoad
                if curMedia.isDownLoad {
                    cell.btnDownLoad.removeTarget(self, action: #selector(readMedia), for: .touchUpInside)
                    cell.btnDownLoad.addTarget(self, action: #selector(dowloadMedia), for: .touchUpInside)
                }
            }
            //show register banner
            showRegisterBanner()
        }
        //Preview
        if curMedia.mediaKind.uppercased() != "CR" {
            if (curMedia.previewFile?.url ?? "").count > 0 {
                cell.btnPreview.addTarget(self, action: #selector(previewMedia), for: .touchUpInside)
                cell.btnPreview.alpha = 1
            } else {
                cell.btnPreview.alpha = 0.35
            }
        }
        
        //Follow
        cell.btnFollow.alpha = 0.35
        if isMember {
            if curMedia.isFollow {
                cell.btnFollow.addTarget(self, action: #selector(followMedia), for: .touchUpInside)
                cell.btnFollow.alpha = 1
            } 
        }
        
        //Share
        cell.btnShare.setTitle("", for: .normal)
        if SP.shared.currentLanguage() == "en" {
            cell.btnShare.setImage(#imageLiteral(resourceName: "book_share_eng"), for: .normal)
        } else {
            cell.btnShare.setImage(#imageLiteral(resourceName: "book_share_thai"), for: .normal)
        }
        cell.btnShare.addTarget(self, action: #selector(shareMedia), for: .touchUpInside)
        return cell
    }
    
    func mediaDetailTableviewCell(indexPath: IndexPath) -> UITableViewCell {
        let isCourse = curMedia.mediaKind == "CR"
        let cell = tableView.dequeueReusableCell(withIdentifier: isCourse ? courseDetailCell : detailCell, for: indexPath) as! MediaDetailTableViewCell
        cell.curMedia = self.curMedia
        if curMedia.mediaKind.uppercased() != "CR" {
            cell.btnMore.addTarget(self, action: #selector(moreAbstract(_:)), for: .touchUpInside)
            //Tap Gesture
            cell.lblCategory.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(categoryTap)))
            cell.lblPublisher.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(publisherTap)))
        }
        cell.lblAuthor.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(authorTap)))
        cell.lblLibrary.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(libraryTap)))
        //Tags
        if cell.tagView != nil {
            cell.tagView.removeAllSubviews()
            tagVC = TagCollectionViewController(tags: curMedia.tags)
            tagVC.tagTapped = self.tagTapped
            tagVC.collectionView.frame = cell.tagView.bounds
            cell.tagView.addSubview(tagVC.collectionView)
            tagVC.collectionView.pin(to: cell.tagView)
        }
        return cell
    }
    
    @objc func tagTapped(tag: String) {
        RoomAPI.shared.mediaItemsByTag(roomId: curMedia.roomId, mediaKind: "", tag: tag, sort: 0, loadIndex: 1) {[weak self] (jsons) in
            DispatchQueue.main.async {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainStoryboard.instantiateViewController(withIdentifier: "MediaListCollection") as? MediaListCollection {
                    vc.usingLibraryID = self?.curMedia.roomId
                    vc.navigationTitle = tag
                    vc.categoryID = ""
                    vc.subCategoryID = ""
                    vc.jsonData = jsons
                    vc.kindOfRoomKeyValue = ["1",tag]
                    self?.navigationController?.pushViewController(vc, animated: true)
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    @objc func moreAbstract(_ sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 1)
        if let cell = tableView.cellForRow(at: indexPath) as? MediaDetailTableViewCell {
            cell.lblAbstract.numberOfLines = cell.lblAbstract.numberOfLines == 0 ? 2 : 0
            sender.setImage(cell.lblAbstract.numberOfLines == 0 ? #imageLiteral(resourceName: "up-arrow") : #imageLiteral(resourceName: "down-arrow"), for: .normal)
            
            cell.lblAbstract.sizeToFit()
            let rect = cell.lblAbstract.frame
            curHeightSection1 = rect.origin.y + rect.height + 50
            tableView.reloadData()
            tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        }
    }
    
    func suggestMediaTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: suggestCell, for: indexPath) as! SuggestMediaTableViewCell
        cell.medias = suggestMedias
        cell.mediaSuggestSelected = mediaSuggestSelected
        return cell
    }
    
    func mediaSuggestSelected(media: SPMediaSuggest) {
        print("\(media.mediaName)")
        numRows = 0
        self.mediaId = media.mediaId
        self.roomId = media.roomId
        setup()
        loadMedia()
    }
    
    func reviewTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reviewCell, for: indexPath) as! ReviewTableViewCell
        
        cell.media = curMedia
        return cell
    }
    
    func booksTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BooksTableViewCell.identification, for: indexPath) as! BooksTableViewCell
        
        cell.svDetail.alpha = curLocSectionHeight <= 120 ? 0 : 1
        cell.setMedia(media: curMedia)
        return cell
    }
}

extension MediaDetailViewController {
    @objc func authorTap() {
        RoomAPI.shared.searchMediaAuthor(with: curMedia) {[weak self] (json) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "MediaListCollection") as? MediaListCollection,
                let json = json {
                vc.usingLibraryID = self?.curMedia.roomId
                vc.navigationTitle = self?.curMedia.author
                vc.categoryID = ""
                vc.subCategoryID = ""
                vc.jsonData = json
                vc.kindOfRoomKeyValue = [KindOfRoomTypeAuthor, self?.curMedia.author ?? ""]
                DispatchQueue.main.async {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func categoryTap() {
        RoomAPI.shared.searchMediaCategory(with: curMedia) {[weak self] (json) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "MediaListCollection") as? MediaListCollection,
                let json = json {
                vc.usingLibraryID = self?.curMedia.roomId
                vc.navigationTitle = self?.curMedia.categoriesName
                vc.categoryID = self?.curMedia.categoriesId
                vc.subCategoryID = self?.curMedia.subCategoriesId
                vc.jsonData = json
                vc.kindOfRoomKeyValue = [KindOfRoomTypeDefault, ""]
                DispatchQueue.main.async {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    
    @objc func publisherTap() {
        RoomAPI.shared.searchMediaPublisher(with: curMedia) {[weak self] (json) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "MediaListCollection") as? MediaListCollection,
                let json = json {
                vc.usingLibraryID = self?.curMedia.roomId
                vc.navigationTitle = self?.curMedia.author
                vc.categoryID = ""
                vc.subCategoryID = ""
                vc.jsonData = json
                vc.kindOfRoomKeyValue = [KindOfRoomTypeAuthor, self?.curMedia.author ?? ""]
                DispatchQueue.main.async {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    @objc func libraryTap() {
        let roomId = curMedia.roomId
        RoomAPI.shared.deviceRoomDetail(memberId: memberId, roomId: roomId) {[weak self] (spRoom, json) in
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            if let vc = mainSB.instantiateViewController(withIdentifier: "LibraryDetailVC") as? LibraryDetailVC {
                vc.libraryTitle = spRoom?.roomName
                vc.libraryID = spRoom?.roomId
                vc.jsonLibraryInfo = json
                guard let media = self?.curMedia else {return}
                RoomAPI.shared.searchMedias(with: media, completion: { (jsonItems) in
                    vc.jsonMediaItems = jsonItems
                    DispatchQueue.main.async {
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                })
            }
        }
    }
    
}

//MARK:- Buy course
extension MediaDetailViewController {
    @objc func courseEnrolling(_ sender: UIButton) {
        if curMedia.mediaKind.uppercased() != "CR" {
            sender.isEnabled = false
        }
        if curMedia.isBuy && curMedia.priceList.count > 0 {
            let vc = PurchaseViewController(media: curMedia)
            vc.modalPresentationStyle = .overCurrentContext
            vc.startDownload = prepareForDownload
            present(vc, animated: true, completion: nil)
        }
    }
    func showEnrolSuccess() {
        self.view.isUserInteractionEnabled = false
        let messageText = SP.shared.currentLanguage() == "en" ? "Enrollment course success.\nPlease check course schedules & medias in your shelf." : "สมัครคอร์สเรียนสำเร็จ\nตารางเรียน, หนังสือและสื่อการเรียน ถูกจัดเก็บใน ชั้นหนังสือของคุณ"
        let lotView = ARProgressView(lottieName: .done, status: messageText, rect: CGRect(x: 0, y: 0, width: 150, height: 200))
        lotView.center = CGPoint(x: view.center.x, y: 0.75*view.center.y)
        self.view.addSubview(lotView)
        lotView.animationView.play { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {[weak self] in
                lotView.removeFromSuperview()
                self?.view.isUserInteractionEnabled = true
                curSelectedMediaId = self?.mediaId
                curSelectedShelfId = Shelf_Preset.toInt()
                self?.tabBarController?.selectedIndex = 2
            }
        }
    }
}

//MARK:- Download & Read
extension MediaDetailViewController {
    @objc func dowloadMedia(_ sender: UIButton) {
        if curMedia.mediaKind.uppercased() == "D" || curMedia.mediaKind.uppercased() == "M"{
            sender.isEnabled = false
        }
        if curMedia.isBuy && curMedia.priceList.count > 0 {
            let vc = PurchaseViewController(media: curMedia)
            vc.modalPresentationStyle = .overCurrentContext
            vc.startDownload = prepareForDownload
            present(vc, animated: true, completion: nil)
        } else {
            if curMedia.isPlayerMedia() {
                playMedia()
            } else {
                prepareForDownload()
            }
        }
    }
    private func prepareForDownload() {
        switch curMedia.mediaKind {
        case "D", "M": startDownload()
        case "YU", "EP", "EL", "ES", "EV": downloadExternalMedia()
        case "CR": showEnrolSuccess()
        default: break
        }
    }
    private func startDownload() {
        RoomAPI.shared.mediaDownload(mediaId: curMedia.mediaId, roomId: curMedia.roomId) {[weak self] (mediaInfo) in
            if let info = mediaInfo {
                //check member right
                guard info.canDownload else {
                    SVProgressHUD.showInfo(withStatus: "ไม่สามารถดำเนินการได้\nเนื่องจากเกินจำนวนสิทธิที่กำหนด")
                    return
                }
                //Download
                guard let media = self?.curMedia else {return}
                DownloadMedia.shared.startDownload(with: media) {
                    DispatchQueue.main.async {
                        self?.setExpiredNotification(info: info)
                        self?.addLotView()
                        //Update coreData
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                            DB.updateMedia(media: media, memberId: self?.memberId ?? "", info: info, subjectId: self?.shelfSubject ?? "", completion: {
                                if let completion = self?.completion {
                                    self?.navigationController?.popViewController(animated: true)
                                    curSelectedMediaId = self?.mediaId
                                    curSelectedShelfId = info.shelfId.toInt()
                                    if (self?.isNavigation)! {
                                        self?.dismiss(animated: true, completion: {
                                            completion(info)
                                        })
                                    } else {
                                        completion(info)
                                    }
                                } else  {
                                    for v in (self?.view.subviews)! {
                                        if v.tag == 111 {
                                            v.removeFromSuperview()
                                            break
                                        }
                                    }
                                    self?.tableView.reloadData()
                                }
                            })
                        })
                    }
                }
            } else {
                SVProgressHUD.showInfo(withStatus: "ไม่สามารถดำเนินการได้")
            }
        }
    }
    private func addLotView() {
        let lotView = UIView(frame: .init(x: 0, y: 0, width: 150, height: 150))
        lotView.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        lotView.layer.cornerRadius = 7
        lotView.center = CGPoint(x: view.center.x, y: 0.75*view.center.y)
        lotView.clipsToBounds = true
        lotView.tag = 111
        self.view.addSubview(lotView)
        
        lotView.addSubview(labelTitle)
        lotView.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
        lotView.addConstraintsWithFormat(format: "V:[v0]-12-|", views: labelTitle)
        
        lotView.addSubview(animationView)
        lotView.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: animationView)
        lotView.addConstraintsWithFormat(format: "V:|-12-[v0(100)]", views: animationView)
        animationView.play()
    }
    private func setExpiredNotification(info: SPMediaDownload) {
        //1 day pior to expired date
        let timePresentNotification = Utility.date(toSecond: info.rentDate, endDate: info.expDate, dateFormat: "yyyyMMddHHmmss") - 24*60*60
        if timePresentNotification > 0,  let expDate = Utility.getDateTime(forNotification: info.expDate, inputFormat: "yyyyMMddHHmmss", outputFormat: "EEEE d MMM yyyy") {
            
            let ntfBody = "\(String.localizedString(key: "ExpDate")) \(expDate)"
            let notification = SPNotification()
            notification.notification(withIdentifier: mediaId,
                                      title: "\(String.localizedString(key: "Book")) \(curMedia.mediaName))",
                                      body: ntfBody,
                                      sound: "default",
                                      badge: 1,
                                      userInfo: [MediaExpNotificationHeader:"Header"],
                                      attachment: nil,
                                      timePresent: timePresentNotification,
                                      repeat:false)
            if info.shelfId != Shelf_Preset {
                Utility.updateBookExpireNotification([mediaId])
                Utility.setBookExpireNotification(notification)
            }
        }
    }
    private func downloadExternalMedia() {
        guard let curMedia = curMedia, let memberId = memberId else {return}
        RoomAPI.shared.mediaDownload(mediaId: curMedia.mediaId, roomId: curMedia.roomId) {[weak self] (mediaInfo) in
            if let info = mediaInfo {
                //create directory & save cover
                let documentPath = SP.shared.documentPath()
                let logsPath = documentPath.appendingPathComponent("\(sarapadFloderName)/\(curMedia.roomId)")
                do {
                    try FileManager.default.createDirectory(atPath: logsPath.path, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError{
                    print("Unable to create directory",error)
                }
                let folderName = "\(sarapadFloderName)/\(curMedia.roomId)"
                let fileName = "\(memberId)_\(curMedia.mediaId)_thumb.png"
                let filePath = "\(folderName)/\(fileName)"
                let localUrl = documentPath.appendingPathComponent(filePath)
                if !FileManager.default.fileExists(atPath: localUrl.path) {
                    Utility.loadMediaCoverImage(fromURL: curMedia.coverFile?.url, orHaveNSData: nil, withFileName: fileName, inDirectory: folderName)
                }
                guard let media = self?.curMedia else {return}
                DispatchQueue.main.async {
                    DB.updateMedia(media: media, memberId: self?.memberId ?? "", info: info, completion: {
                        self?.navigationController?.popViewController(animated: true)
                        if let completion = self?.completion {
                            curSelectedMediaId = self?.mediaId
                            curSelectedShelfId = info.shelfId.toInt()
                            completion(info)
                        }
                    })
                }
            }
        }
    }
    @objc func readMedia() {
        guard let docPath = SP.shared.getMediaUrl(media: curMedia) else {return}
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...")
        }
        let roomId = curMedia.roomId
        let mediaId = curMedia.mediaId
        let fileName = curMedia.mediaFile?.fileName
        let fileType = (docPath.lastPathComponent as NSString).pathExtension
        let fileUrl = curMedia.mediaFile?.url ?? ""
        if fileType.lowercased() == "pdf" {
            var pdfPassword = ""
            //try local file
            if let path = SP.shared.getLocalMediaPath(roomId: roomId, fileUrl: fileUrl) {
                let url = URL(fileURLWithPath: path)
                if let pdfDoc = PDFDocument(url: url) {
                    if pdfDoc.isLocked {
                        pdfPassword = Utility.generatePassword(fileName)
                        guard pdfDoc.unlock(withPassword: pdfPassword) else {
                            SVProgressHUD.showError(withStatus: "Document locked!")
                            return
                        }
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    } else {
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    }
                }
                //else try online
            } else if InternetConnection.isConnectedToNetwork(),
                let docPath = URL(string: fileUrl),
                let pdfDoc = PDFDocument(url: docPath) {
                var pdfPassword = ""
                if pdfDoc.isLocked {
                    pdfPassword = Utility.generatePassword(fileName)
                    guard pdfDoc.unlock(withPassword: pdfPassword) else {return}
                }
                loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
            } else {
                SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
            }
        } else {
            SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
        }
    }
    fileprivate func loadPdfAds(_ roomId: String, _ mediaId: String, _ pdfDoc: PDFDocument, _ docPath: URL, _ pdfPassword: String) {
        SP.shared.loadPdfAds(roomId: roomId, mediaId: mediaId) {[weak self] (pdfAds) in
            if let ads = pdfAds {
                let filePath = String(format: "%@/An_%@_%@_%@_ios.txt", PDFAnnotationFolderName, self?.memberId ?? "", mediaId, roomId)
                let strUrl = String(format: "%@api/DeviceMedia/UploadAnnotation", hostIpRoomAPI)
                let info = PdfAnnotationInfo(roomId: roomId,
                                             mediaId: mediaId,
                                             pdfDocument: pdfDoc,
                                             filePath: filePath,
                                             uploadPath: strUrl,
                                             removeFileOnExit: false,
                                             canUpload: (self?.isMember)!,
                                             isPreview: false,
                                             pdfURL: docPath,
                                             password: pdfPassword,
                                             ads: ads)
                let pdfVC = PdfViewController(info: info)
                let nav = UINavigationController(rootViewController: pdfVC)
                if let topVC = SP.shared.topViewController() {
                    topVC.present(nav, animated: true){
                        ARProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    @objc func playMedia() {
        let type = curMedia.mediaKind == MediaKind.Video.rawValue ? "Video" : "Audio"
        RoomAPI.shared.getMediaAds(roomId: roomId, adsType: type, mediaId: curMedia.mediaId) {[weak self] (json) in
            if let json = json {
                let vc = Area5ViewController(roomId: self?.roomId ?? "", mediaPath: self?.curMedia.mediaFile?.url ?? "", json: json, title: self?.curMedia.mediaName ?? "")
                let nav = UINavigationController(rootViewController: vc)
                SP.shared.loadSVProgressHUD(status: "Loading...", completing: {
                    self?.present(nav, animated: true, completion: nil)
                })
            } else {
                SVProgressHUD.showError(withStatus: "Cannot play media!")
            }
        }
    }
    //MARK:- External Media
    @objc func openExternalMedia() {
        if let offMedia = DB.getMedia(curMedia: curMedia, memberId: memberId) {
            switch curMedia.mediaKind.uppercased() {
            case "YU": self.openYoutube(offMedia: offMedia)
            case "EP", "EL", "ES", "EV": self.openExternalLink(offMedia: offMedia)
            default:break
            }
        }
    }
    func openYoutube(offMedia: OfflineShelfData) {
        guard let roomId = offMedia.libraryId,
            let id = NSString(string: offMedia.fileUrl ?? "").lastPathComponent.components(separatedBy: "?v=").last
            else {return}
        let yt = YT(roomID: roomId, id: id)
        let vc = YoutubeViewController(yt: yt)
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: true, completion: nil)
        }
    }
    func openExternalLink(offMedia: OfflineShelfData) {
        let vc = WebOSViewController(urlPath: offMedia.fileUrl ?? "", title: offMedia.mediaTitle ?? "")
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: true) {
                if (offMedia.mediaType ?? "").uppercased() == "ES" {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
}


//MARK:- Preview
extension MediaDetailViewController {
    @objc func previewMedia() {
        
    }
}

//MARK:- Follow
extension MediaDetailViewController {
    @objc func followMedia() {
        if curMedia.isFollow {
            self.confirmAlert(title: "Media Follow", messgae: "ยกเลิกการติดตาม", okTitle: "OK") {[weak self] (actionButton) in
                if actionButton == .OK {
                    self?.confirmFollow()
                }
            }
        } else {
            confirmFollow()
        }
        
    }
    
    func confirmFollow() {
        RoomAPI.shared.mediaFollow(mediaId: curMedia.mediaId, follow: curMedia.isFollow) {[weak self] (isDone) in
            if isDone {
                self?.curMedia.isFollow.toggle()
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    if (self?.curMedia.isFollow)! {
                        ARProgressHUD.show(with: .success, info: "ตั้งค่า\n'ติดตาม'")
                    } else {
                        ARProgressHUD.show(with: .success, info: "ตั้งค่า\n'ยกเลิกติดตาม'")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    ARProgressHUD.show(with: .warning, info: "เกิดข้อผิดพลาด")
                }
            }
        }
    }
}
//MARK:- Share
extension MediaDetailViewController: MFMailComposeViewControllerDelegate {
    @objc func shareMedia() {
        let vc = ShareMediaViewController()
        vc.modalPresentationStyle = .overCurrentContext
        vc.shareSelected = {(index) in
            RoomAPI.shared.shareMedia(memberId: self.memberId, mediaId: self.curMedia.mediaId, roomId: self.curMedia.roomId) {[weak self] (str) in
                DispatchQueue.main.async {
                    let link2share = "\(ShareMediaUrl)\(str)"
                    switch index {
                    case 0: ShareMedia().shareMedia(toFacebook: self, url: link2share)
                    case 1: ShareMedia().shareMedia(toLine: link2share)
                    case 2: ShareMedia().shareMedia(toTwitter: link2share)
                    case 3: self?.share2Mail(link2share: link2share)
                    case 4: self?.copyLink(link2share: link2share)
                    default: break
                    }
                }
            }
        }
        present(vc, animated: true, completion: nil)
    }
    func share2Mail(link2share: String) {
        if MFMailComposeViewController.canSendMail() {
            //Your code will go here
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self;
            mailVC.setSubject("\(curMedia.mediaName) (Sara+Pad Application)")
            mailVC.setMessageBody(link2share, isHTML: false)
            present(mailVC, animated: true, completion: nil)
        } else {
            //This device cannot send email
            SVProgressHUD.showError(withStatus: String.localizedString(key: "ToastFailed"))
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        var strToast = "ToastSuccess"
        switch result {
        case .sent:break
        case .saved:break
        case .cancelled:
            //Handle cancelling of the email
            strToast = "ToastFailed"
        case .failed:
            //Handle failure to send.
            strToast = "ToastFailed"
        default:
            break;
        }
        controller.dismiss(animated: true) {
            SVProgressHUD.showInfo(withStatus: String.localizedString(key: strToast))
        }
    }
    func copyLink(link2share: String) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = link2share
        SVProgressHUD.showSuccess(withStatus: "Copy shared link\n\n\(link2share)")
    }
}
//MARK:- QRCode
extension MediaDetailViewController: WKNavigationDelegate {
    @objc func scanQRCode() {
        let vc = QRCodeViewController()
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
        vc.scanQR {[weak self] (stringUrl) in
            if let url = URL(string: stringUrl ?? "") {
                do {
                    let results = try String(contentsOf: url, encoding: .ascii)
                    let info = results.slices(from: "\"al:ios:url\" content=\"sarapad://", to: "\"")
                    if let infoString = info.first {
                        let parsedString = String(infoString).parsingComponents("&", sep2: "=")
                        if parsedString.count >= 5 {
                            //FUNCTION/UID/LID/MID/CID
                            let mId = parsedString[3]
                            let rId = parsedString[2]
                            let cId = parsedString[4]
                            let msg = "\nหนังสือ:\(self?.curMedia.mediaName ?? "")\nชุดที่:\(cId)"
                            self?.confirmAlert(title: "ยืนยันการยืมหนังสือ", messgae: msg, okTitle: "OK", selectedAction: { (actionBtn) in
                                if actionBtn == .OK {
                                    RoomAPI.shared.bookBorrow(mediaId: mId, roomId: rId, cId: cId, completion: { (msgCode, expDate) in
                                        switch msgCode {
                                        case 200:
                                            let info = SPMediaDownload()
                                            info.expDate = expDate
                                            info.shelfId = Shelf_Member
                                            self?.bookBorrowSuccess(info: info)
                                        case 201, 203: SVProgressHUD.showInfo(withStatus: "หนังสือถูกยืมไปแล้ว")
                                        default: SVProgressHUD.showError(withStatus: "คุณไม่สามารถยืมหนังสือนี้ได้")
                                        }
                                    })
                                }
                            })
                            
                        }
                    }
                } catch  {
                    print("ERROR")
                }
            }
        }
    }
    func bookBorrowSuccess(info: SPMediaDownload) {
        DispatchQueue.main.async {
            ARProgressHUD.show(with: .books, info: "ยืมหนังสือสำเร็จ")
            //Update coreData
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                DB.updateMedia(media: self.curMedia, memberId: self.memberId, info: info, subjectId: self.shelfSubject ?? "", completion: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                    if let completion = self?.completion {
                        curSelectedMediaId = self?.mediaId
                        curSelectedShelfId = info.shelfId.toInt()
                        if (self?.isNavigation)! {
                            self?.dismiss(animated: true, completion: {
                                completion(info)
                            })
                        } else {
                            completion(info)
                        }
                    }
                })
            })
        }
    }
}

extension String {
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func slices(from: String, to: String) -> [Substring] {
        let pattern = "(?<=" + from + ").*?(?=" + to + ")"
        return ranges(of: pattern, options: .regularExpression)
            .map{ self[$0] }
    }
    func parsingComponents(_ sep1: String, sep2: String) -> [String] {
        var retArray = [String]()
        let components = self.components(separatedBy: sep1)
        components.forEach { (string) in
            let strComponents = string.components(separatedBy: sep2)
            retArray.append(strComponents.last ?? "")
        }
        
        return retArray
    }
}

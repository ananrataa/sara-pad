//
//  DownloadMedia.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 7/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation



class DownloadMedia: NSObject, URLSessionDownloadDelegate {
    static let shared = DownloadMedia()
    private override init() {}
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data!
    var downloadSize: Double!
    var completion: (()->())?
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    

    lazy var dirName: String = {
        let name = "\(sarapadFloderName)/\(curMedia.roomId)"
        return name
    }()
    
    var curMedia: SPMedia!
    var memberId: String!
    
    func startDownload(with media: SPMedia, completion: @escaping ()->()) {
        //User
        memberId = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            memberId = DBManager.selectMemberID()
        }

        curMedia = media
        self.completion = completion
        guard let fileUrl = curMedia.mediaFile?.url,
            let url = URL(string: fileUrl) else {return}
        
        //check if media file already exist
        let destinationURL = localFilePath(for: url)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationURL.path) {
            downloadMediaCover()
        }
        
        
        //Download media
        DispatchQueue.main.async {
            SVProgressHUD.showProgress(0, status: "Downloading...")
        }
        
        let req = URLRequest(url: url)
        self.task = self.downloadsSession.downloadTask(with: req)
        self.task?.resume()
        self.isDownloading = true
    }
    
    func localFilePath(for url: URL) -> URL {
        //create directory
        let documentPath = SP.shared.documentPath()
        let logsPath = documentPath.appendingPathComponent("\(sarapadFloderName)/\(curMedia.roomId)")
        print(logsPath)
        
        do {
            try FileManager.default.createDirectory(atPath: logsPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError{
            print("Unable to create directory",error)
        }
        let filePath = "\(sarapadFloderName)/\(curMedia.roomId)/\(url.lastPathComponent)"
        return documentPath.appendingPathComponent(filePath)
    }
    func downloadMediaCover() {
        guard let curMedia = curMedia else {return}
        let documentPath = SP.shared.documentPath()
        let folderName = "\(sarapadFloderName)/\(curMedia.roomId)"
        
        let fileName = "\(memberId!)_\(curMedia.mediaId)_thumb.png"
        let filePath = "\(folderName)/\(fileName)"
        let localUrl = documentPath.appendingPathComponent(filePath)
        
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            Utility.loadMediaCoverImage(fromURL: curMedia.coverFile?.url, orHaveNSData: nil, withFileName: fileName, inDirectory: folderName)
        }
    }
    //MARK:- Annotation
    func downloadAnnotation() {
        if curMedia.isMember {
            if let url = URL(string: curMedia.annotation) {
                do {
                    let data = try Data(contentsOf: url)
                    let annString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? ""
                    let fileName = "\(PDFAnnotationFolderName)/Annotation_\(memberId!)_\(curMedia.mediaId)_\(curMedia.roomId).txt"
                    Utility.writePdfAnotationJson(fileName, jsonString: annString as String)
                } catch let error as NSError {
                    print("Ops there was an error \(error.localizedDescription)")
                }
            }
        }
    }
}

extension DownloadMedia {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        //Download media cover
        downloadMediaCover()
        downloadAnnotation()
        guard let sourceURL = downloadTask.originalRequest?.url else {return}
        //Local file path
        let destinationURL = localFilePath(for: sourceURL)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationURL.path) {
            if let completion = self.completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    SVProgressHUD.dismiss()
                    completion()
                }
            }
        } else {
            do {
                try fileManager.copyItem(at: location, to: destinationURL)
                if let completion = self.completion {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        SVProgressHUD.dismiss()
                        completion()
                    }
                }
            } catch let error {
                debugPrint("error: \(error)")
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        var progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        progress = min(100, max(0, progress))
        DispatchQueue.main.async {
            SVProgressHUD.showProgress(progress, status: "⏱ Downloading...\n\(Int(progress*100))%")
        }
    }
}

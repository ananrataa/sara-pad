//
//  PdfPageViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 5/2/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

class PdfPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, PDFViewDelegate, UIDocumentInteractionControllerDelegate {

    var pdfDocument: PDFDocument
    var pdfView: PDFView!
    var curIndex: Int
    var pageChanged: ((Int)->())?
    var beforeVC: UIViewController?
    var afterVC: UIViewController?
    init(pdfDoc: PDFDocument, pdfTitle: String, curIndex: Int) {
        self.pdfDocument = pdfDoc
        self.curIndex = curIndex
        super.init(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: [:])
        title = pdfTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "close_black"), style: .done, target: self, action: #selector(done))
        
        dataSource = self
        delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        if let firstViewController = getViewController(at: curIndex) {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    private func getViewController(at index: Int) -> UIViewController? {
        guard self.pdfDocument.pageCount > 0 else {
            return nil
        }
        let page: PDFPage = self.pdfDocument.page(at: index)!
        let pdfView: PDFView = PDFView(frame: self.view.frame)
        pdfView.document = self.pdfDocument
        pdfView.displayMode = .singlePage
        pdfView.go(to: page)
        pdfView.autoScales = true
        pdfView.delegate = self
        let vc = UIViewController()
        vc.view = pdfView
        vc.view.tag = index
        return vc
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let vc = beforeVC {
            return vc
        }
        let previousIndex = viewController.view.tag - 1
        guard previousIndex >= 0 else {return nil}
        guard self.pdfDocument.pageCount > previousIndex else {return nil}
        return getViewController(at: previousIndex)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let vc = afterVC {
            return vc
        }
        let nextIndex = viewController.view.tag + 1
        guard self.pdfDocument.pageCount != nextIndex else {return nil}
        guard self.pdfDocument.pageCount > nextIndex else {return nil}
        return getViewController(at: nextIndex)
    }
    
    @objc func done() {
        if let pageChanged = self.pageChanged {
            if let vc = self.viewControllers?.first {
                pageChanged(vc.view.tag)
            }
        }
        dismiss(animated: false) {
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard finished && completed else {
            beforeVC = nil
            afterVC = nil
            return
        }
        if let vc = self.viewControllers?.first, let pdfView = vc.view as? PDFView, let page = pdfView.currentPage {
            let n = pdfDocument.index(for: page)
            //set beforeVC
            if n > 0 {
                beforeVC = getViewController(at: n-1)
            } else {
                beforeVC = nil
            }
            
            //set afterVC
            if n < pdfDocument.pageCount - 1 {
                afterVC = getViewController(at: n+1)
            } else {
                afterVC = nil
            }
        }
    }

    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.viewControllers?.first?.view
    }
    
    func pdfViewWillClick(onLink sender: PDFView, with url: URL) {
        print(url.absoluteString)
        let vc = WebOSViewController(urlPath: url.absoluteString)
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
}

//
//  UITableViewCellExt.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 11/12/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class var identification: String {
        return String(describing: self)
    }
}

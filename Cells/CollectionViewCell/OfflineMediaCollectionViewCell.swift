//
//  OfflineMediaCollectionViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 14/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class OfflineMediaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var lblMediaName: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblExpired: UILabel!
    @IBOutlet weak var newDownloadImgView: UIImageView!
    @IBOutlet weak var mediaTypeImgView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var btnRead: UIButton!
    @IBOutlet weak var btnreturn: UIButton!
    @IBOutlet weak var mediaSV: UIStackView!
    @IBOutlet weak var btnShareAnnotation: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
    var curMedia: OfflineShelfData? {
        didSet {
            refreshView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnRead.imageView?.contentMode = .scaleAspectFit
        btnreturn.imageView?.contentMode = .scaleAspectFit
        
        coverImageView.layer.cornerRadius = 7
        coverImageView.layer.borderWidth = 0.35
        coverImageView.layer.borderColor = UIColor.lightGray.cgColor
        coverImageView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        bgView.layer.borderColor = UIColor.lightGray.cgColor
        bgView.layer.borderWidth = 0.35
        bgView.layer.cornerRadius = 7
        
        contentView.layer.shadowOpacity = 0.35
        contentView.layer.shadowOffset = .init(width: 1, height: 2)

        cardView.layer.cornerRadius = 7
    }

    func refreshView() {
        let isEng = SP.shared.currentLanguage() == "en"
        btnShareAnnotation.isHidden = true
        if let curMedia = curMedia {
            let mediaType = (curMedia.mediaType ?? "").uppercased()
            if mediaType == "T", let cId = curMedia.cId {
                lblMediaName.text = "\(curMedia.mediaTitle ?? "")\n(\(cId))"
            } else {
                lblMediaName.text = curMedia.mediaTitle ?? ""
            }
            lblAuthor.text = curMedia.author ?? ""
            lblExpired.text = "Exp: \(Utility.covertDate(toFormat: curMedia.dateExp ?? "", inputFormat: "yyyyMMddHHmmss", outputFormat: "dd-MMM-yyyy") ?? "-")"
            //new download
            newDownloadImgView.isHidden = !curMedia.isNewDownload
            //icon
            
            switch mediaType {
            case "D": mediaTypeImgView.image = UIImage(named: "iconbook")
            case "M": mediaTypeImgView.image = UIImage(named: "iconmagazine")
            case "V": mediaTypeImgView.image = UIImage(named: "iconvideo")
            case "A": mediaTypeImgView.image = UIImage(named: "iconaudio")
            case "E": mediaTypeImgView.image = UIImage(named: "iconepub")
            case "T": mediaTypeImgView.image = UIImage(named: "icon_material")
            case "YU": mediaTypeImgView.image = UIImage(named: "icon_youtube")
            case "EP": mediaTypeImgView.image = UIImage(named: "icon_pdf")
            case "ES": mediaTypeImgView.image = UIImage(named: "icon_sound")
            case "EL": mediaTypeImgView.image = UIImage(named: "icon_link")
            case "EV": mediaTypeImgView.image = UIImage(named: "icon_video")
            case "CR": mediaTypeImgView.image = UIImage(named: "icon_tutor")
            default: mediaTypeImgView.image = nil
            }
            
            switch mediaType {
            case "YU": btnRead.setImage(UIImage(named: "shelf_youtube"), for: .normal)
            case "D", "M": btnRead.setImage(UIImage(named: "shelf_read"), for: .normal)
            case "EP": btnRead.setImage(UIImage(named: "shelf_read_pdf"), for: .normal)
            case "EL": btnRead.setImage(UIImage(named: "shelf_read_link"), for: .normal)
            case "ES": btnRead.setImage(UIImage(named: "shelf_play_sound"), for: .normal)
            case "A", "V", "EV": btnRead.setImage(isEng ? #imageLiteral(resourceName: "play_video_en.png"):#imageLiteral(resourceName: "play_video_th.png"), for: .normal)
            default:
                btnRead.setTitle("อ่าน", for: .normal)
                btnRead.layer.cornerRadius = 7
                btnRead.backgroundColor = UIColor.flatPumkinColor
                btnRead.layer.borderWidth = 0.5
                btnRead.layer.borderColor = btnRead.backgroundColor?.cgColor
            }
//            btnShareAnnotation.isHidden = (mediaType != "D")
            
            //check if media file is exist or not for mediaKind "D" & "M"
            if ["D", "M"].contains(curMedia.mediaType?.uppercased()) {
                let filePath = SP.shared.getLocalMediaPath(roomId: curMedia.libraryId ?? "", fileUrl: curMedia.fileUrl ?? "") ?? ""
                let fileManager = FileManager.default
                if !fileManager.fileExists(atPath: filePath) {
                    coverImageView.alpha = 0.35
                } else {
                    coverImageView.alpha = 1
                }
            }
            
        }
    }
    
    override var isSelected: Bool {
        didSet {
            cardView.isHidden = !self.isSelected
        }
    }
    
}



//
//  SuggestMediaTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class SuggestMediaTableViewCell: UITableViewCell {
    let cellId = "suggest_cell"
    let collectionView: UICollectionView = {
        let lo = UICollectionViewFlowLayout()
        lo.scrollDirection = .horizontal
        lo.minimumInteritemSpacing = 12
        let cv = UICollectionView(frame: .zero, collectionViewLayout: lo)
        cv.backgroundColor = .white
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    var medias: [SPMediaSuggest] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    var mediaSuggestSelected: ((SPMediaSuggest)->())?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        contentView.addSubview(collectionView)
        contentView.addConstraintsWithFormat(format: "H:|-8-[v0]|", views: collectionView)
        contentView.addConstraintsWithFormat(format: "V:|-8-[v0]|", views: collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SuggestMediaCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
}

extension SuggestMediaTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let n: CGFloat = UIDevice.modelName.contains("iPad") ? 4 : 3
        let w = (collectionView.frame.width - (n-1)*12)/n
        let h = w*4/3 + 80
//        return CGSize(width: w, height: w*4/3 + 88)
//        let w = (contentView.frame.width - 8)/3
//        let h = contentView.frame.height - 8
        return CGSize(width: w, height: h)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SuggestMediaCollectionViewCell
        
        let media = medias[indexPath.item]
        if let url = URL(string: media.mediaCoverUrl) {
            cell.mediaCover.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "default_media_cover"))
        } else {
            cell.mediaCover.image = #imageLiteral(resourceName: "default_media_cover")
        }
        cell.lblTitle1.text = media.mediaName
        cell.lblTitle2.text = media.author
        
        for v in cell.voteView.subviews {
            if let v = v as? UIImageView {
                if v.tag <= media.popularVote {
                    v.image = #imageLiteral(resourceName: "iconvote-yellow")
                } else {
                    v.image = #imageLiteral(resourceName: "iconvote-gray")
                }
            }
        }
        //Media type
        switch media.mediaKind {
        case "D": cell.mediaType.image = UIImage(named: "iconbook")
        case "M": cell.mediaType.image = UIImage(named: "iconmagazine")
        case "V": cell.mediaType.image = UIImage(named: "iconvideo")
        case "A": cell.mediaType.image = UIImage(named: "iconaudio")
        case "E": cell.mediaType.image = UIImage(named: "iconepub")
        case "T": cell.mediaType.image = UIImage(named: "icon_material")
        case "YU": cell.mediaType.image = UIImage(named: "icon_youtube")
        case "EP": cell.mediaType.image = UIImage(named: "icon_pdf")
        case "ES": cell.mediaType.image = UIImage(named: "icon_sound")
        case "EL": cell.mediaType.image = UIImage(named: "icon_link")
        case "EV": cell.mediaType.image = UIImage(named: "icon_video")
        case "CR": cell.mediaType.image = UIImage(named: "icon_tutor")
        default: cell.mediaType.image = nil
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let mediaSelected = self.mediaSuggestSelected {
            let media = medias[indexPath.item]
            mediaSelected(media)
        }
    }
}

class SuggestMediaCollectionViewCell: BaseCell {
    let mediaCover: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleToFill
        imgV.clipsToBounds = true
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let mediaType: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleToFill
        imgV.clipsToBounds = true
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let lblTitle1: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: mainFont, size: 15)
        lbl.textAlignment = .center
        lbl.textColor = .darkGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    let lblTitle2: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: mainFont, size: 13)
        lbl.textAlignment = .center
        lbl.textColor = UIColor(hexString: "#0096FF")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    let voteView: UIStackView = {
        var imgs:[UIImageView] = []
        for tag in 1...5 {
            let imgVote = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            imgVote.image = #imageLiteral(resourceName: "iconvote-gray")
            imgVote.tag = tag
            imgVote.contentMode = .scaleAspectFit
            imgs.append(imgVote)
        }
        let sv = UIStackView(arrangedSubviews: imgs)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .equalCentering
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    override func setup() {
        contentView.layer.borderWidth = 0.35
        contentView.layer.borderColor = UIColor.lightGray.cgColor
        contentView.backgroundColor = UIColor(hexString: "#efefef")
        
        
        contentView.addSubview(mediaCover)
        contentView.addSubview(lblTitle1)
        contentView.addSubview(lblTitle2)
        contentView.addSubview(voteView)
        
        mediaCover.heightAnchor.constraint(equalTo: mediaCover.widthAnchor, multiplier: 1.33).isActive = true
        contentView.addConstraintsWithFormat(format: "H:|[v0]|", views: mediaCover)
        contentView.addConstraintsWithFormat(format: "H:|[v0]|", views: lblTitle1)
        contentView.addConstraintsWithFormat(format: "H:|[v0]|", views: lblTitle2)
        contentView.addConstraintsWithFormat(format: "H:|-8-[v0]-8-|", views: voteView)
        
        contentView.addConstraintsWithFormat(format: "V:|[v0]-8-[v1(28)]-4-[v2(20)]-4-[v3(10)]", views: mediaCover, lblTitle1, lblTitle2, voteView)
        //mediaType
        mediaCover.addSubview(mediaType)
        mediaCover.addConstraintsWithFormat(format: "V:|-3-[v0(25)]", views: mediaType)
        mediaCover.addConstraintsWithFormat(format: "H:[v0(25)]-3-|", views: mediaType)
        mediaType.image = #imageLiteral(resourceName: "icon_material.png")
    }
}

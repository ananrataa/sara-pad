//
//  MediaTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 24/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SDWebImage
class MediaTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaCover: UIImageView!
    @IBOutlet weak var btnDownLoad: UIButton!
    @IBOutlet weak var btnPreview: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblMediaName: UILabel!
    @IBOutlet weak var mediaTypeImgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnPreviewCourse: UIButtonX!
    @IBOutlet weak var mediaCoverBG: UIImageView!
    @IBOutlet weak var vsEffectView: UIVisualEffectView!
    
    @IBOutlet var voteStar: [UIImageView]!
    var media: SPMedia? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mediaCover.layer.shadowOpacity = 0.35
        mediaCover.layer.shadowOffset = .init(width: 3, height: 3)
        btnDownLoad.setTitle("", for: .normal)
        btnPreview.setTitle("", for: .normal)
        btnFollow.setTitle("", for: .normal)
        btnShare.setTitle("", for: .normal)
        
        btnDownLoad.addBorder(width: 0.5, color: btnDownLoad.backgroundColor!, radius: 7)
        btnPreview.addBorder(width: 0.5, color: btnPreview.backgroundColor!, radius: 7)
        btnFollow.addBorder(width: 0.5, color: btnFollow.backgroundColor!, radius: 7)
        btnShare.addBorder(width: 0.5, color: btnShare.backgroundColor!, radius: 7)
        bgView.addBorder(width: 0.5, color: bgView.backgroundColor!, radius: 7)
        
        btnDownLoad.layer.shadowOpacity = 0.35
        btnDownLoad.layer.shadowOffset = .init(width: 1, height: 2)
        btnPreview.layer.shadowOpacity = 0.35
        btnPreview.layer.shadowOffset = .init(width: 1, height: 2)
        btnFollow.layer.shadowOpacity = 0.35
        btnFollow.layer.shadowOffset = .init(width: 1, height: 2)
        btnShare.layer.shadowOpacity = 0.35
        btnShare.layer.shadowOffset = .init(width: 1, height: 2)
        
        if SP.shared.currentLanguage() == "en" {
            btnPreview.setImage(#imageLiteral(resourceName: "book_preview_eng.png"), for: .normal)
            btnFollow.setImage(#imageLiteral(resourceName: "book_follow_eng.png"), for: .normal)
        } else {
            btnPreview.setImage(#imageLiteral(resourceName: "book_preview.png"), for: .normal)
            btnFollow.setImage(#imageLiteral(resourceName: "book_follow.png"), for: .normal)
        }
        
        btnPreviewCourse.tintColor = UIColor(hexString: "#F5541E")
        vsEffectView.alpha = 0.975
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func refreshView() {
        if let media = media {
            if let url = URL(string: (media.coverFile?.url)!) {
                mediaCover.sd_setImage(with: url)
                lblMediaName.text = media.mediaName
            } else {
                mediaCover.image = #imageLiteral(resourceName: "default_media_cover")
            }
            mediaCoverBG.image = mediaCover.image
            voteStar.forEach {
                $0.image = media.popularVote >= $0.tag ? #imageLiteral(resourceName: "iconvote-yellow") : #imageLiteral(resourceName: "iconvote-gray")
            }
            
            //icon
            let mediaType = (media.mediaKind).uppercased()
            switch mediaType {
            case "D": mediaTypeImgView.image = UIImage(named: "iconbook")
            case "M": mediaTypeImgView.image = UIImage(named: "iconmagazine")
            case "V": mediaTypeImgView.image = UIImage(named: "iconvideo")
            case "A": mediaTypeImgView.image = UIImage(named: "iconaudio")
            case "E": mediaTypeImgView.image = UIImage(named: "iconepub")
            case "T": mediaTypeImgView.image = UIImage(named: "icon_material")
            case "YU": mediaTypeImgView.image = UIImage(named: "icon_youtube")
            case "EP": mediaTypeImgView.image = UIImage(named: "icon_pdf")
            case "ES": mediaTypeImgView.image = UIImage(named: "icon_sound")
            case "EL": mediaTypeImgView.image = UIImage(named: "icon_link")
            case "EV": mediaTypeImgView.image = UIImage(named: "icon_video")
            case "CR":
                mediaTypeImgView.image = UIImage(named: "icon_tutor")
                if SP.shared.currentLanguage() == "en" {
                    btnPreview.setImage(UIImage(named: "book_details_eng"), for: .normal)
                } else {
                    btnPreview.setImage(UIImage(named: "book_details_th"), for: .normal)
                }
                if let details = media.courseDetail, let _ = URL(string: details.url) {
                    btnPreview.addTarget(self, action: #selector(openCourseDetails), for: .touchUpInside)
                    btnPreview.isEnabled = true
                } else {
                    btnPreview.isEnabled = false
                }
                
                //coursePreview
                btnPreviewCourse.addTarget(self, action: #selector(openCoursePreview), for: .touchUpInside)
            default: mediaTypeImgView.image = nil
            }
            
            btnPreviewCourse.isHidden = (mediaType != "CR")
        }
    }
    @objc func openCourseDetails() {
        if let media = media, let details = media.courseDetail {
            if let topVC = SP.shared.topViewController() {
                let vc = WebOSViewController(urlPath: details.url, title: media.mediaName)
                let nav = UINavigationController(rootViewController: vc)
                topVC.present(nav, animated: true, completion: nil)
            }
        }
    }
    @objc func openCoursePreview() {
        if let media = media, let urlString = media.demoCourseVideo?.url, let _ = URL(string: urlString) {
            if let topVC = SP.shared.topViewController() {
                let vc = WebOSViewController(urlPath: urlString, title: media.mediaName)
                let nav = UINavigationController(rootViewController: vc)
                topVC.present(nav, animated: true) { 
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
}

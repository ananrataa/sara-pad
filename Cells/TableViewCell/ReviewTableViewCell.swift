//
//  ReviewTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 27/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxSwift

@available(iOS 11.0, *)
class ReviewTableViewCell: UITableViewCell, Jitterable, Flashable {
    let disposeBag = DisposeBag()
    let voteView: UIStackView = {
        var buttons:[UIButton] = []
        for tag in 1...5 {
            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            btn.setImage(#imageLiteral(resourceName: "iconvote-gray"), for: .normal)
            btn.tag = tag
            buttons.append(btn)
        }
        let sv = UIStackView(arrangedSubviews: buttons)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .equalCentering
        sv.spacing = 4
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let submitBtn: UIButton = {
       let btn = UIButton()
        btn.setTitle(String.localizedString(key: "Submit_Review"), for: .normal)
        btn.backgroundColor = OrangeTheme.shared.barBackground
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = UIFont(name: mainFont, size: 17)
        btn.addBorder(width: 0.5, color: .clear, radius: 7)
        btn.layer.shadowOpacity = 0.25
        btn.layer.shadowOffset = .init(width: 0, height: 3)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let flashLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = String.localizedString(key: "vote_error")
        lbl.font = UIFont(name: mainFont, size: 17)
        lbl.textColor = .red
        lbl.alpha = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    var media: SPMedia? {
        didSet {
            refreshView()
        }
    }
    var voteScore: Int = 0
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        selectionStyle = .none
        addSubview(voteView)
        addSubview(submitBtn)
        addSubview(flashLabel)
        
        voteView.topAnchor.constraint(equalTo: topAnchor, constant: 25).isActive = true
        voteView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        submitBtn.topAnchor.constraint(equalTo: voteView.bottomAnchor, constant: 36).isActive = true
        submitBtn.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        submitBtn.widthAnchor.constraint(equalTo: voteView.widthAnchor).isActive = true
        flashLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        flashLabel.topAnchor.constraint(equalTo: submitBtn.bottomAnchor, constant: 12).isActive = true
        
        submitBtn.addTarget(self, action: #selector(submitVote(_:)), for: .touchUpInside)
        for v in voteView.subviews {
            if let btn = v as? UIButton {
                btn.addTarget(self, action: #selector(voteButtonTap(_:)), for: .touchUpInside)
            }
        }
    }
    
    @objc func voteButtonTap(_ sender: UIButton) {
        voteScore = sender.tag
        for v in voteView.subviews {
            if let btn = v as? UIButton {
                btn.setImage(btn.tag <= voteScore ? #imageLiteral(resourceName: "iconvote-yellow") : #imageLiteral(resourceName: "iconvote-gray"), for: .normal)
            }
        }
    }
    
    @objc func submitVote(_ sender: UIButton) {
        if let media = media {
            if media.isMember {
                guard voteScore > 0 else {
                    flashLabel.text = String.localizedString(key: "invalid_vote")
                    flashLabel.flash()
                    return
                }
                MediaModel.shared.createMediaVoteObservable(mediaId: media.mediaId, score: voteScore)
                    .subscribe(onNext: {[weak self] (msg) in
                        if !msg.isEmpty {
                            self?.flashLabel.text = msg
                            self?.flashLabel.flash()
                        } else {
                            SVProgressHUD.showSuccess(withStatus: String.localizedString(key: "vote_success"))
                            
                        }
                    }, onError: {[weak self] (err) in
                        self?.flashLabel.text = err.localizedDescription
                        self?.flashLabel.flash()
                    }).disposed(by: disposeBag)
            } else {
                flashLabel.text = String.localizedString(key: "vote_error")
                sender.jitter()
                flashLabel.flash()
                for v in voteView.subviews {
                    if let btn = v as? UIButton {
                        btn.setImage(#imageLiteral(resourceName: "iconvote-gray"), for: .normal)
                    }
                }
            }
        }
    }
    
    func refreshView() {
        if let media = media {
            for v in voteView.subviews {
                if let btn = v as? UIButton {
                    btn.setImage((btn.tag <= media.scoreVote && media.isMember) ? #imageLiteral(resourceName: "iconvote-yellow") : #imageLiteral(resourceName: "iconvote-gray"), for: .normal)
                }
            }
        }
    }
}

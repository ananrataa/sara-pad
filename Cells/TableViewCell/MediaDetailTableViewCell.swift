//
//  MediaDetailTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 24/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class MediaDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblPages: UILabel!
    @IBOutlet weak var lblFileSize: UILabel!
    @IBOutlet weak var lblPublisher: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblAMount: UILabel!
    @IBOutlet weak var lblLibrary: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblAbstract: UILabel!
    @IBOutlet weak var tagView: UIView!
    
    @IBOutlet weak var imageViewRoom: UIImageView!
    @IBOutlet weak var imageViewTutor: UIImageView!
    @IBOutlet weak var imageViewTag: UIImageView!
    
    @IBOutlet weak var btnMore: UIButton!
    
    var curMedia: SPMedia? {
        didSet {
            if curMedia?.mediaKind.uppercased() == "CR" {
                refreshCourse()
            } else {
                refreshView()
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let imgView = imageViewRoom {
            imgView.tintColor = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        }
        if let imgView = imageViewTutor {
            imgView.tintColor = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        }
        if let imgView = imageViewTag {
            imgView.tintColor = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        lblAbstract.addBorder(width: 0.5, color: .clear, radius: 5)
        
    }

    func refreshCourse() {
        if let media = curMedia {
            let att:[NSAttributedString.Key:Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor:UIColor(named: "sp_blue")!]
            lblLibrary.attributedText = NSAttributedString(string: media.roomName, attributes: att)
            lblAuthor.attributedText = NSAttributedString(string: media.author, attributes: att)
            lblAbstract.text = media.abstract
        }
    }
    func refreshView() {
        if let media = curMedia {
            let att:[NSAttributedString.Key:Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor:UIColor(named: "sp_blue")!]
            lblAuthor.attributedText = NSAttributedString(string: media.author, attributes: att)
            lblCategory.attributedText = NSAttributedString(string: media.categoriesName, attributes: att)
            
            if let page = media.mediaFile?.page, page > 0 {
                lblPages.text = "\(page)"
            } else {
                lblPages.text = ""
            }

            if let sizeMB = media.mediaFile?.fileSizeMB {
                lblFileSize.text = String(format: "%.2f", sizeMB/1048576) + " \(String.localizedString(key: "Megabyte"))"
            } else {
                lblFileSize.text = "-"
            }
            lblPublisher.attributedText = NSAttributedString(string: media.publisherName, attributes: att)
            lblYear.text = media.year
            if media.version.toInt() > 0 {
                lblVersion.text = media.version
            } else {
                lblVersion.text = "-"
            }
            if media.amount.toInt() > 0 {
                lblAMount.text = media.amount
            } else {
                lblAMount.text = "-"
            }
            lblLibrary.attributedText = NSAttributedString(string: media.roomName, attributes: att)
            lblCode.text = media.mediaCode
            lblAbstract.text = String.localizedString(key: "Abstract") + ": " + media.abstract
            
        }
    }
}

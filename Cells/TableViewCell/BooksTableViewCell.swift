//
//  BooksTableViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 12/12/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class BooksTableViewCell: UITableViewCell {

    @IBOutlet weak var svDetail: UIStackView!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var lblBuilding: UILabel!
    @IBOutlet weak var lblFloor: UILabel!
    @IBOutlet weak var lblRoom: UILabel!
    @IBOutlet weak var lblRow: UILabel!
    @IBOutlet weak var lblCabinet: UILabel!
    @IBOutlet weak var lblShelf: UILabel!
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var availableCopies: UILabel!
    @IBOutlet weak var availableDate: UILabel!
    @IBOutlet weak var building: UILabel!
    @IBOutlet weak var floor: UILabel!
    @IBOutlet weak var room: UILabel!
    @IBOutlet weak var row: UILabel!
    @IBOutlet weak var cabinet: UILabel!
    @IBOutlet weak var shelf: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if SP.shared.currentLanguage() == "en" {
            lblPosition.text = "Location"
            lblAvailable.text = "Books available"
            lblReturnDate.text = "Returning on"
            
            lblBuilding.text = "Building"
            lblFloor.text = "Floor"
            lblRoom.text = "Room"
            lblRow.text = "Row"
            lblCabinet.text = "Cabinet"
            lblShelf.text = "Shelf"
        } else {
            lblPosition.text = "ตำแหน่งหนังสือ"
            lblAvailable.text = "จำนวนเล่มบนชั้น"
            lblReturnDate.text = "จะมีหนังสือคืน"
            
            lblBuilding.text = "อาคาร"
            lblFloor.text = "ชั้น"
            lblRoom.text = "ห้อง"
            lblRow.text = "แถว"
            lblCabinet.text = "ตู้"
            lblShelf.text = "ชั้น"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMedia(media: SPMedia) {
        if SP.shared.currentLanguage() == "en" {
            location.text = "row:\(media.row) cab:\(media.cabinet) shelf:\(media.shelf)"
        } else {
            location.text = "แถว:\(media.row) ตู้:\(media.cabinet) ชั้น:\(media.shelf)"
        }
        availableCopies.text = media.availableCopies
        availableDate.text = media.availableDate
        building.text = media.building
        floor.text = media.floor
        room.text = media.room
        row.text = media.row
        cabinet.text = media.cabinet
        shelf.text = media.shelf
    }
}

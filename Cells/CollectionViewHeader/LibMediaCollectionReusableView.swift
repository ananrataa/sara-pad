//
//  LibMediaCollectionReusableView.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 22/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class LibMediaCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var mediaTypeSegments: UISegmentedControl!
    
    @objc var onSegmentSelected: ((Int)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let title0 = SP.shared.currentLanguage() == "en" ? "Courses" : "คอร์สเรียน"
        let title1 = SP.shared.currentLanguage() == "en" ? "Book & Medias" : "หนังสือ & สื่อ"
        let font = UIFont(name: sukhumvitLightFont, size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
        let att = [NSAttributedString.Key.font: font]
        mediaTypeSegments.setTitle(title0, forSegmentAt: 0)
        mediaTypeSegments.setTitle(title1, forSegmentAt: 1)
        mediaTypeSegments.setTitleTextAttributes(att, for: .normal)
    }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        if let segmentSelected = self.onSegmentSelected {
            segmentSelected(sender.selectedSegmentIndex)
        }
    }
}

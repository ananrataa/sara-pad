//
//  GoogleModel.swift
//  TestRx
//
//  Created by MBOX Multimedia Co., Ltd. on 17/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxCocoa
import RxSwift

final class GoogleModel {
    static let shared = GoogleModel()
    private init() {}
    func createGoogleTranslationObservable(text: String, source: String, target: String) -> Observable<String> {
        let stringURL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=\(source)&tl=\(target)&ie=UTF-8&oe=UTF-8&dt=t&q=\(text)"
        guard let strUrlEncode = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string: strUrlEncode) else {
            return Observable<String>.create({ (observer) -> Disposable in
                observer.onNext("")
                return Disposables.create()
            })
        }
        return Observable<String>.create({[weak self] (observer) -> Disposable in
            let session = URLSession.shared
            let task = session.dataTask(with: url) { (data, response, error)->Void in
                DispatchQueue.main.async {
                    if let translationResult = self?.parseGoogleTanslation(data: data) {
                        observer.onNext(translationResult)
                    } else {
                        observer.onNext("Cannot translate!")
                    }
                }
            }
            task.resume()
            return Disposables.create(with: {
                task.cancel()
            })
        })
    }
    
    private func parseGoogleTanslation(data: Data?) -> String? {
        guard let data = data else {return nil}
        do {
            let result = try JSON(data: data)
            let jsonArray = result.array
            guard let arrayTranslate = jsonArray?.first?.array else {return nil}
            var translateResult = ""
            for js in arrayTranslate {
                if let jsArray = js.array, jsArray.count > 0, let strTranslate = jsArray[0].string {
                    translateResult += strTranslate
                }
            }
            return translateResult
        } catch  {
            print("error trying to convert data to JSON")
            return nil
        }
    }
}

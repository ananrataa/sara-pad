//
//  SearchModel.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

@available(iOS 11.0, *)
class SearchModel {
    static let shared = SearchModel()
    private init() {}
    
    func createSearchMediaObserverable(deviceCode:String, roomId:String, mediaKind:String, catId: String, subId:String, findValue:String, loadIndex:Int) -> Observable<[SPPreviewMedia]> {
        return Observable<[SPPreviewMedia]>.create({(observer) -> Disposable in
            RoomAPI.shared.getPreviewMedias(deviceCode: deviceCode, roomId: roomId, mediaKind: "", catId: "", subId: "", findValue: findValue, loadIndex: 1) { (items) in
                if let items = items {
                    DispatchQueue.main.async {
                        observer.onNext(items)
                    }
                } else {
                    DispatchQueue.main.async {
                        observer.onNext([])
                    }
                }
            }
            return Disposables.create()
        })
    }
}

//
//  MediaMocel.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit
import RxCocoa
import RxSwift

let api_key = "IE11bHRpbWVkaWEgIA0KIA0KICANCiBEaWdpdGFsIENvbnRlbnRzIA0K"

@available(iOS 11.0, *)

class MediaModel {
    static let shared = MediaModel()
    private init() {}
    /*
    func createMediaSuggestionObservable(roomId: String, mediaId: String, mediaKind: String) -> Observable<[SPMediaSuggest]> {
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        return Observable<[SPMediaSuggest]>.create({ (observer) -> Disposable in
            RoomAPI.shared.mediaSuggested(roomId: roomId, mediaId: mediaId, mediaKind: mediaKind) { (json) in
                var mediaItems:[SPMediaSuggest] = []
                if let js = json as? [[String:Any]] {
                    for j in js {
                        mediaItems.append(SPMediaSuggest(json: j))
                    }
                }
                DispatchQueue.main.async {
                    observer.onNext(mediaItems)
                }
            }
            return Disposables.create()
        })
    }
    
    func createMediaBrowseObservable(roomId: String, mediaId: String, booksLimit: Int, isPreset: Bool) -> Observable<SPMedia?> {
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        return Observable<SPMedia?>.create({ (observer) -> Disposable in
            RoomAPI.shared.browseMediaDetail(roomId: roomId, mediaId: mediaId, limit: booksLimit, completion: { (json) in
                DispatchQueue.main.async {
                    if let json = json as? [String: Any] {
                        observer.onNext(SPMedia(json: json))
                    } else {
                        observer.onNext(nil)
                    }
                }
            })
            return Disposables.create()
        })
    }
    */
    func createMediaVoicesObservable(mediaId: String) -> Observable<[SPMediaVoiceList]>  {
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        return Observable<[SPMediaVoiceList]>.create({(observer) -> Disposable in
            RoomAPI.shared.getMediaVoices(deviceCode: deviceCode, mediaId: mediaId){ (items) in
                DispatchQueue.main.async {
                    observer.onNext(items)
                }
            }
            return Disposables.create()
        })
    }
    
    
    func createMediaPdfObservable(id:String) -> Observable<PDFDocument?> {
        guard let media = DB.getMedia(id: id), let url = URL(string: media.fileUrl ?? "") else {
            return Observable<PDFDocument?>.create({ (observer) -> Disposable in
                return Disposables.create()
            })
        }
        return Observable<PDFDocument?>.create({(observer) -> Disposable in
            if let pdf = PDFDocument(url: url) {
                if pdf.isLocked {
                    if let password = Utility.generatePassword(media.originalFileName ?? ""), pdf.unlock(withPassword: password) {
                        observer.onNext(pdf)
                    }
                } else {
                    observer.onNext(pdf)
                }
                observer.onNext(nil)
                
            } else {
                observer.onNext(nil)
            }
            return Disposables.create()
        })
    }
    
    func createMediaVoteObservable(mediaId: String, score: Int) -> Observable<String> {
        let apiStr = "%@api/DeviceMedia/MediaVote?deviceCode=%@&MediaId=%@&ScoreVote=%i"
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        let strUrl = String(format: apiStr, hostIpRoomAPI, deviceCode, mediaId, score)
        
        guard let url = URL(string: strUrl) else {
            return Observable<String>.create({ (observer) -> Disposable in
                observer.onNext("Error cannot vote. Try again later.")
                return Disposables.create()
            })
        }
        
        return Observable<String>.create({ (observer) -> Disposable in
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
            request.addValue(api_key, forHTTPHeaderField: "API_KEY")
            request.httpMethod = "GET"
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let err = error {
                    observer.onError(err)
                } else {
                    if let data = data, let stringData = String(data: data, encoding: .utf8) {
                        observer.onNext(stringData)
                    }
                }
            })
            task.resume()
            return Disposables.create(with: {
                task.cancel()
            })
        })
    }
    
    func createMediaCategoriesObservable(loadIndex: Int) -> Observable<[SPCategory]> {
        return Observable<[SPCategory]>.create({ (observer) -> Disposable in
            RoomAPI.shared.mediaCategories(loadIndex: loadIndex, completion: { (items) in
                observer.onNext(items)
            })
            return Disposables.create()
        })
    }
    
    func createDeviceLimitRentObservable(items: Int, days: Int) -> Observable<[SPDeviceLimitRent]> {
        return Observable<[SPDeviceLimitRent]>.create({ (observer) -> Disposable in
            RoomAPI.shared.deviceLimitRent(items: items, days: days, completion: { (items) in
                observer.onNext(items)
            })
            return Disposables.create()
        })
    }
    
}

@available(iOS 11.0, *)
extension MediaModel {
    func createBookReadingObservable() {
        
    }
    
    
}

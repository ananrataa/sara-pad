//
//  AnnotationModel.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 25/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum AndroidAnnotationType: String {
    case HIGHLIGHT = "highlight"
    case UNDERLINE = "underline"
    case STRIKEOUT = "strikeOut"
    case INK = "ink"
}
struct AndroidAnnotation {
    let annotation: AndroidAnnotationList?
    var points: [CGPoint]
    let color: UIColor
    let size: CGFloat
    let page: Int
    init(json:[String:Any]) {
        if let annotationJson = json["annotationList"] as? [String:Any] {
            annotation = AndroidAnnotationList(json: annotationJson)
        } else {
            annotation = nil
        }
        if let pointArray = json["pointF"] as? [[String:CGFloat]], pointArray.count > 0 {
            var pointData:[CGPoint] = []
            for p in pointArray {
                pointData.append(CGPoint(x: p["x"] ?? 0, y: p["y"] ?? 0))
            }
            points = pointData
        } else {
            points = []
        }
        if let c = json["colorF"] as? Int {
            color = UIColor(android: c)
        } else {
            color = .black
        }
        size = (json["inkDrawingSize"] as? CGFloat) ?? 0
        page = (json["pageNumber"] as? Int) ?? 0
    }
}

struct AndroidAnnotationList {
    let type: String
    let left: CGFloat
    let top: CGFloat
    let right: CGFloat
    let bottom: CGFloat
    init(json:[String:Any]) {
        type = (json["type"] as? String) ?? ""
        left = (json["left"] as? CGFloat) ?? 0
        top = (json["top"] as? CGFloat) ?? 0
        right = (json["right"] as? CGFloat) ?? 0
        bottom = (json["bottom"] as? CGFloat) ?? 0
    }
}

class AnnotationModel {
    static let shared = AnnotationModel()
    private init() {}
    func data(json:[[String:Any]]) -> [AndroidAnnotation] {
        var annotations:[AndroidAnnotation] = []
        for js in json {
            if let ann = js["annotationList"] as? [String:Any], let type = ann["type"] as? String {
                if type == "INK" {
                    annotations.append(contentsOf: getAndroidInkAnnotations(json: js))
                } else {
                    annotations.append(AndroidAnnotation(json: js))
                }
            }
        }
        return annotations
    }
    
    func createAndroidAnnotation(json: [[String:Any]]) -> (annotationArray:[NSDictionary], inkArray:[NSDictionary]) {
        let androidData = self.data(json: json)
        let annotations = androidData.filter{$0.annotation?.type != AndroidAnnotationType.INK.rawValue }
        var annotationArray:[NSDictionary] = []
        var inkArray:[NSDictionary] = []
        for (tag,value) in annotations.enumerated() {
            let color = value.color.hexCode
            let width = value.size
            if value.annotation?.type == "INK" {
                var points:[NSDictionary] = []
                for p in value.points {
                   points.append(NSDictionary(dictionaryLiteral: ("X", p.x), ("Y", p.y)))
                }
                let dict = NSDictionary(dictionaryLiteral: ("color", color),
                                        ("tag", tag),
                                        ("width", width),
                                        ("points", points))
                let newDict = NSDictionary(dictionaryLiteral: ("pageNumber", value.page), ("annotationList", dict))
                inkArray.append(newDict)
            } else {
                let pointsArray = value.points.chunks(4)
                for p in pointsArray {
                    let type = value.annotation?.type ?? ""
                    let x = p.map{$0.x}.min()!
                    let y = p.map{$0.y}.min()!
                    let w = abs(p.map{$0.x}.max()! - x)
                    let h = abs(p.map{$0.y}.max()! - y)
                    print("page:\(value.page), type:\(type), x:\(x), y:\(y), w:\(w), h:\(h), color:\(color)")
                    let dict = NSDictionary(dictionaryLiteral:
                                            ("type", type),
                                            ("tag", tag),
                                            ("X", x),
                                            ("Y", y),
                                            ("W", w),
                                            ("H", h),
                                            ("color", color))
                    let newDict = NSDictionary(dictionaryLiteral: ("pageNumber", value.page), ("annotationList", dict))
                    annotationArray.append(newDict)
                }
            }
        }
        return (annotationArray, inkArray)
    }
    func getAndroidInkAnnotations(json: [String:Any]) -> [AndroidAnnotation] {
        var annotations:[AndroidAnnotation] = []
        let inkAnn = AndroidAnnotation(json: json)
        if let points = json["pointFS"] as? [[[String:CGFloat]]] {
            for p in points {
                var pointData:[CGPoint] = []
                for pDict in p {
                    pointData.append(CGPoint(x: pDict["x"] ?? 0, y: pDict["y"] ?? 0))
                }
                var inkLine = inkAnn
                inkLine.points = pointData
                annotations.append(inkLine)
            }
        }
        return annotations
    }
}

//
//  BalanceTableViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 2/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class BalanceTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var media: MediaHistory? {
        didSet {
            refreshView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 7
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func refreshView() {
        if let media = media {
            lblDate.text = media.date
            lblType.text = media.buyType
            lblDescription.text = "\(media.rommName)\n\(media.mediaName)"
            lblPrice.text = media.price
            lblBalance.text = media.balance
            let color_flat_red = UIColor(hexString: "#E14938").withAlphaComponent(0.85)
            let color_flat_green = UIColor(hexString: "#60BC6C").withAlphaComponent(0.85)

            bgView.backgroundColor = media.price.toInt() < 0 ? color_flat_red : color_flat_green
        }
    }
}

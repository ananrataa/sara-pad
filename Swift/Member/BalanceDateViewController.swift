//
//  BalanceDateViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 2/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class BalanceDateViewController: UIViewController {
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var segmentOption: UISegmentedControl!
    
    var dateChanged:((String, String)->())?
    var refreshBalance: (()->())?
    var date1: Date
    var date2: Date
    
    init(date1: Date, date2: Date) {
        self.date1 = date1
        self.date2 = date2
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.setValue(UIColor.white, forKey: "textColor")
        if SP.shared.currentLanguage() == "en" {
            datePicker.locale = Locale(identifier: "en_us")
        } else {
            datePicker.locale = Locale(identifier: "th")
        }
        datePicker.calendar = Calendar.current
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        datePicker.date = date1
        if let pickerView = self.datePicker.subviews.first {
            for subview in pickerView.subviews {
                
                if subview.frame.height <= 5 {
                    subview.backgroundColor = UIColor.darkGray
                    subview.tintColor = UIColor.white
                    subview.layer.borderColor = UIColor.darkGray.cgColor
                    subview.layer.borderWidth = 0.5
                }
            }
        }
    }

    @IBAction func dateOptionChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            datePicker.date = date1
        } else {
            datePicker.date = date2
        }
    }
    
    @IBAction func done(_ sender: Any) {
        dismiss(animated: true) {
            if let refresh = self.refreshBalance {
                SVProgressHUD.show()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    refresh()
                })
            }
        }
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        if segmentOption.selectedSegmentIndex == 0 {
            date1 = sender.date
        } else {
            date2 = sender.date
        }
        if let dateChanged = dateChanged {
            let d1 = "\(date1.getDateString(with: "YYYY"))-\(date1.getDateString(with: "MM"))-\(date1.getDateString(with: "dd"))"
            let d2 = "\(date2.getDateString(with: "YYYY"))-\(date2.getDateString(with: "MM"))-\(date2.getDateString(with: "dd"))"
            dateChanged(d1, d2)
        }
    }
}

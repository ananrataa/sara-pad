//
//  MemberProfileViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 10/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class MemberProfileViewController: UIViewController {
    let cellId = "balance_cell"
    let headerId = "balance_header_cell"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnRange: UIButton!
    
    var date1 = ""
    var date2 = ""
    var items: [MediaHistory] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    var memberId: String
    
    @objc init(memberId: String) {
        self.memberId = memberId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LightTheme.shared.applyNavigationBar(vc: self)
        
        if let info = DBManager.selecttMemberMenuInfo() as? [String], info.count >= 2 {
            self.title = info.first ?? "User Profile"
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(refill))
        
        tableView.register(UINib.init(nibName: "BalanceTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        tableView.register(UINib.init(nibName: "BalanceHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: headerId)
        
        MemberAPi.shared.getMemberDetail(memberId: memberId) { (memberDetail) in
            if let memDetail = memberDetail {
                DispatchQueue.main.async {
                    if let name = memDetail.member?.memberFname, name.count > 0 {
                        self.title = "User: \(name)"
                    }
                }
            }
        }
        date1 = "\(Date().getDateString(with: "YYYY"))-\(Date().getDateString(with: "MM"))-01"
        date2 = "\(Date().getDateString(with: "YYYY"))-\(Date().getDateString(with: "MM"))-\(Date().getDateString(with: "dd"))"
        refreshDateButton()
        
        refreshBalance()
    }

    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func refill() {
        RoomAPI.shared.moneyRefill(memberId: memberId) { (link) in
            DispatchQueue.main.async {
                if let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    SVProgressHUD.showError(withStatus: "เกิดข้อผิดพลาด\nไม่สามารถดำเนินการได้")
                }
            }
        }
    }
    
    func refreshBalance() {
        RoomAPI.shared.getMediaHistory(4, from: date1, to: date2, loadIndex: 1) { (medias) in
            self.items = medias
        }
    }
    
    func dateChabged(date1: String, date2: String) {
        self.date1 = date1
        self.date2 = date2
        refreshDateButton()
    }
    
    func refreshDateButton() {
        let d1 = Utility.covertDate(toFormat: date1, inputFormat: "YYYY-MM-dd", outputFormat: "d MMM yyyy") ?? ""
        let d2 = Utility.covertDate(toFormat: date2, inputFormat: "YYYY-MM-dd", outputFormat: "d MMM yyyy") ?? ""
        btnRange.setTitle("  ระหว่างวันที่ \(d1) - \(d2)  ", for: .normal)
        btnRange.layer.cornerRadius = btnRange.frame.height/2
        btnRange.layer.borderColor = UIColor.black.cgColor
        btnRange.layer.borderWidth = 0.5

    }
    @IBAction func dateTapped(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let d1 = dateFormatter.date(from: date1) ?? Date()
        let d2 = dateFormatter.date(from: date2) ?? Date()
        let vc = BalanceDateViewController(date1: d1, date2: d2)
        vc.dateChanged = self.dateChabged
        vc.refreshBalance = self.refreshBalance
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion:nil)
    }
    
}

extension MemberProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: headerId) as! BalanceHeaderTableViewCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BalanceTableViewCell
        
        cell.media = items[indexPath.row]
        return cell
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

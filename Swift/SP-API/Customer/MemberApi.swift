//
//  CustomApi.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 10/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

@available(iOS 10.0, *)
class MemberAPi: NSObject {
    var timer: Timer?
    let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""

    private override init() {}
    @objc static let shared = MemberAPi.init()
    func mergeMember(from memberId:String, to email: String, completion:@escaping ((code: String, message: String)?)->Void) {
        let stringUrl = String(format: SPApiUrl.mergeMember, hostIpCustomAPI, memberId, email)
        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "POST") { (json) in
            if let json = json as? [String:Any] {
                let msgCode = (json["MessageCode"] as? String) ?? ""
                let msgText = (json["MessageText"] as? String) ?? ""
                completion((msgCode, msgText))
            } else {
                completion(nil)
            }
        }
    }
    func getMemberDetail(memberId: String, completion:@escaping (MemberDetail?)->Void) {
        let stringUrl = String(format: SPApiUrl.memberDetail, hostIpCustomAPI, memberId)
        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:Any] {
                completion(MemberDetail(json: json))
            } else {
                completion(nil)
            }
        }
    }
    //MARK:- URL Request
    func sendHTTPRequest(_ url: String, returnType: String, method: String? = "GET", completion: @escaping (Any?)->Void) {
        guard
            let stringUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = NSURL(string: stringUrl) as URL?
            else {
                completion(nil)
                return
        }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        timer = Timer.scheduledTimer(withTimeInterval: 32, repeats: false, block: { (timer) in
            session.invalidateAndCancel()
            timer.invalidate()
        })
        
        let queue = DispatchQueue(label: "th.co.mbox.MBlaze")
        queue.async {
            if SPUtility.shared.isConnectedToNetwork() {
                DispatchQueue.main.async {[weak self] in
                    let request: NSMutableURLRequest = NSMutableURLRequest()
                    request.url = url
                    request.httpMethod = method!
                    request.timeoutInterval = 30
                    request.addValue(SPApiUrl.apiKey, forHTTPHeaderField: "API_KEY")
                    
                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                        guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                            else {
                                print("error: not a valid http response")
                                self?.cancelTimer()
                                completion(nil)
                                return
                        }
                        if httpResponse.statusCode == 200 {
                            if returnType == "JSON" {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: receivedData, options: [])
                                    self?.cancelTimer()
                                    completion(json)
                                } catch  {
                                    self?.cancelTimer()
                                    completion(nil)
                                }
                            } else if returnType == "STRING" {
                                if let data = data, let text = String.init(data: data, encoding: String.Encoding.utf8) {
                                    completion(text)
                                } else {
                                    completion(nil)
                                }
                            }
                        } else if let error = error {
                            self?.cancelTimer()
                            print(error.localizedDescription)
                            completion(nil)
                        } else {
                            self?.cancelTimer()
                            completion(nil)
                        }
                    })
                    dataTask.resume()
                }
            } else {
                self.cancelTimer()
                completion(nil)
                print("No internet connection!")
            }
        }
    }
    
    func cancelTimer() {
        if let timer = timer {
            timer.invalidate()
            self.timer = nil
        }
    }
}

//MARK:- Payment
extension MemberAPi {
    @objc func getMemberBag(memberId: String, completion:@escaping (Int)->Void) {
        let api = "%@api/DevicePlatform/Bag_Member?memberID=%@&DeviceCode=%@"
        let stringUrl = String(format: api, hostIpCustomAPI, memberId, deviceCode)
        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String,
                let balance = json["MessageText"] as? String {
                if code == "200" {
                    completion(balance.toInt())
                } else {
                    completion(0)
                }
            } else {
                completion(0)
            }
        }
    }
//    @objc func getMemberPayment(memberId: String, completion:@escaping ()->Void) {
//        let api = "%@api/DeviceMedia/UrlPayment?deviceCode=%@&memberId=%@"
//        let stringUrl = String(format: api, hostIpCustomAPI, deviceCode, memberId)
//        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
//            if let json = json as? [String:Any] {
//                print(json.debugDescription)
//            }
//        }
//    }
    
    func bookBuy(memberId: String, roomId: String, saleId: String, password: String, completion:@escaping (Int)->Void) {
        let api = "%@api/DeviceMedia/ProductBuy?memberID=%@&DeviceCode=%@&saleID=%@&roomID=%@&Password=%@"
        let stringUrl = String(format: api, hostIpCustomAPI, memberId, deviceCode, saleId, roomId, password)
        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String {
                completion(code.toInt())
            } else {
                completion(204)
            }
        }
    }
    
    func memberSignOut(memberId: String, completion:@escaping (Int)->Void) {
        let api = "%@api/DevicePlatform/LogingOut?deviceCode=%@&memberID=%@"
        let stringUrl = String(format: api, hostIpCustomAPI,deviceCode,memberId)
        MemberAPi.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String {
                completion(code.toInt())
            } else {
                completion(204)
            }
        }
    }
}

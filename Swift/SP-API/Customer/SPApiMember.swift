//
//  SPApiMember.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/13/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
enum ApiMember {
    static let memberId = "MEMBERID"
    static let memberCode = "MEMBERCODE"
    static let memberTitle = "MEMBER_TITLE"
    static let memberFname = "MEMBER_FNAME"
    static let memberLname = "MEMBER_LNAME"
    static let idCard = "IDCARD"
    static let sex = "SEX"
    static let birthDay = "BRTHDATE"
    static let memberTypeId = "MEMBERTYPEID"
    static let eMail = "EMAIL"
    static let phoneNumber = "PHONENUMBER"
    static let webUrl = "WEBURL"
    static let socialCode = "SOCIALCODE"
    static let memberRoleId = "MEMBERROLEID"
    static let groupId = "GROUPID"
    static let roomOwn = "ROOMOWN"
    static let userName = "USERNAME"
    static let stt = "STT"
    static let organizeName = "ORGANIZNAME"
}
enum ApiMemberAddress {
    static let addressId = "ADDRID"
    static let memberId = "MEMBERID"
    static let address = "ADDR"
    static let subDistrictCode = "SUBDISTRICTCODE"
    static let districtCode = "DISTRICTCODE"
    static let provinceCode = "PROVINCECODE"
    static let zipCode = "ZIPCODE"
}
enum ApiMemberThaiAddress {
    static let provinceCode = "PROVINCE_CODE"
    static let provinceName = "PROVINCE_NAME"
    static let ampurCode = "AMPHUR_CODE"
    static let ampurName = "AMPHUR_NAME"
    static let districrCode = "DISTRICT_CODE"
    static let districtName = "DISTRICT_NAME"
    static let geoId = "GEO_ID"
    static let geoName = "GEO_NAME"
    static let zipCode = "ZIPCODE"
}
enum ApiCertificate {
    static let certificateId = "CERTIFICATEID"
    static let certificateCode = "CERTIFICATECODE"
    static let certificateName = "CERTIFICATENAME"
    static let roomId = "ROOMID"
}
enum ApiEducationLevel {
    static let educationLevelId = "EDULEVID"
    static let educationCode = "EDUCODE"
    static let educationName = "EDUNAME"
    static let roomId = "ROOMID"
}
enum ApiCustomerGroup {
    static let groupId = "GROUPID"
    static let groupCode = "GROUPCODE"
    static let groupName = "GROUPNAME"
    static let groupDescription = "GROUPDESP"
    static let groupTypeId = "GROUPTYPEID"
    static let roleId = "ROLEID"
    static let permissionId = "PERMISSIONID"
}
enum ApiMemberEducation {
    static let memberId = "MEMBERID"
    static let educationLevelId = "EDULEVID"
    static let part = "PART"
    static let certificateId = "CERTIFICATEID"
    static let certificateName = "CERTIFICATENAME"
    static let startYear = "STARTYEAR"
    static let graduation = "GRADUATION"
}
enum ApiMemberDetail {
    static let member = "Member"
    static let memberAddress = "MemberAddress"
    static let thaiAddress = "ThaiAddress"
    static let certificate = "Certificate"
    static let educationLevel = "EducateLev"
    static let group = "Group"
    static let lastLogin = "Lastlogin"
    static let memberRole = "MemberRole"
    static let memberEducation = "MemberEducate"
    static let messageText = "MessageText"
    static let isRegister = "IsRegister"
    static let registerDate = "RegisterDate"
    static let durationTime = "DurationTime"
}
enum ApiMemberRole {
    static let memberRoleId = "MEMBERROLEID"
    static let memberRoleCode = "MEMBERROLECODE"
    static let memberRoleName = "MEMBERROLENAME"
    static let memberRoleDescription = "MEMBERROLEDESP"
    static let registerPeriod = "REGISTERPERIOD"
    static let itemsLimit = "ITEMLIMIT"
    static let daysLimit = "ITEMLIMITTIMEDAY"
    static let sizesLimit = "USEDSIZETOLLOMIT"
    static let categoriesLimit = "CATEGORIESPERMISS"
    static let groupId = "GROUPID"
    static let tag = "TAG"
}
//MARK:- Struct
struct MemberInformation {
    let memberId: String
    let memberCode: String
    let memberTitle: String
    let memberFname: String
    let memberLname: String
    let idCard: String
    let sex: String
    let birthDay: Date
    let memberTypeId: String
    let eMail: String
    let phoneNumber: String
    let webUrl: String
    let socialCode: String
    let memberRoleId: String
    let groupId: String
    let roomOwn: String
    let userName: String
    let stt: String
    let organizeName: String
    
    init(json:[String:Any]) {
         memberId = (json[ApiMember.memberId] as? String) ?? ""
         memberCode = (json[ApiMember.memberCode] as? String) ?? ""
         memberTitle = (json[ApiMember.memberTitle] as? String) ?? ""
         memberFname = (json[ApiMember.memberFname] as? String) ?? ""
         memberLname = (json[ApiMember.memberLname] as? String) ?? ""
         idCard = (json[ApiMember.idCard] as? String) ?? ""
         sex = (json[ApiMember.sex] as? String) ?? ""
         birthDay = (json[ApiMember.birthDay] as? Date) ?? Date()
         memberTypeId = (json[ApiMember.memberTypeId] as? String) ?? ""
         eMail = (json[ApiMember.eMail] as? String) ?? ""
         phoneNumber = (json[ApiMember.phoneNumber] as? String) ?? ""
         webUrl = (json[ApiMember.webUrl] as? String) ?? ""
         socialCode = (json[ApiMember.socialCode] as? String) ?? ""
         memberRoleId = (json[ApiMember.memberRoleId] as? String) ?? ""
         groupId = (json[ApiMember.groupId] as? String) ?? ""
         roomOwn = (json[ApiMember.roomOwn] as? String) ?? ""
         userName = (json[ApiMember.userName] as? String) ?? ""
         stt = (json[ApiMember.stt] as? String) ?? ""
         organizeName = (json[ApiMember.organizeName] as? String) ?? ""
    }
}

struct MemberAddress {
    let addressId: String
    let memberId: String
    let address: String
    let subDistrictCode: String
    let districtCode: String
    let provinceCode: String
    let zipCode: String
    
    init(json: [String:Any]) {
        addressId = (json[ApiMemberAddress.addressId] as? String) ?? ""
        memberId = (json[ApiMemberAddress.memberId] as? String) ?? ""
        address = (json[ApiMemberAddress.address] as? String) ?? ""
        subDistrictCode = (json[ApiMemberAddress.subDistrictCode] as? String) ?? ""
        districtCode = (json[ApiMemberAddress.districtCode] as? String) ?? ""
        provinceCode = (json[ApiMemberAddress.provinceCode] as? String) ?? ""
        zipCode = (json[ApiMemberAddress.zipCode] as? String) ?? ""
    }
}
struct MemberThaiAddress {
    let provinceCode: String
    let provinceName: String
    let ampurCode: String
    let ampurName: String
    let districrCode: String
    let districtName: String
    let geoId: Int
    let geoName: String
    let zipCode: String
    
    init(json:[String:Any]) {
        provinceCode = (json[ApiMemberThaiAddress.provinceCode] as? String) ?? ""
        provinceName = (json[ApiMemberThaiAddress.provinceName] as? String) ?? ""
        ampurCode = (json[ApiMemberThaiAddress.ampurCode] as? String) ?? ""
        ampurName = (json[ApiMemberThaiAddress.ampurName] as? String) ?? ""
        districrCode = (json[ApiMemberThaiAddress.districrCode] as? String) ?? ""
        districtName = (json[ApiMemberThaiAddress.districtName] as? String) ?? ""
        geoId = (json[ApiMemberThaiAddress.geoId] as? Int) ?? 0
        geoName = (json[ApiMemberThaiAddress.geoName] as? String) ?? ""
        zipCode = (json[ApiMemberThaiAddress.zipCode] as? String) ?? ""
    }
}
struct Certificate {
    let certificateId: String
    let certificateCode: String
    let certificateName: String
    let roomId: String
    
    init(json:[String:Any]) {
        certificateId = (json[ApiCertificate.certificateId] as? String) ?? ""
        certificateCode = (json[ApiCertificate.certificateCode] as? String) ?? ""
        certificateName = (json[ApiCertificate.certificateName] as? String) ?? ""
        roomId = (json[ApiCertificate.roomId] as? String) ?? ""
    }
}
struct EducationLevel {
    let educationLevelId: String
    let educationCode: String
    let educationName: String
    let roomId: String
    
    init(json:[String:Any]) {
        educationLevelId = (json[ApiEducationLevel.educationLevelId] as? String) ?? ""
        educationCode = (json[ApiEducationLevel.educationCode] as? String) ?? ""
        educationName = (json[ApiEducationLevel.educationName] as? String) ?? ""
        roomId = (json[ApiEducationLevel.roomId] as? String) ?? ""
    }
}
struct MemberGroup {
    let groupId: String
    let groupCode: String
    let groupName: String
    let groupDescription: String
    let groupTypeId: String
    let roleId: String
    let permissionId: String
    
    init(json:[String:Any]) {
         groupId = (json[ApiCustomerGroup.groupId] as? String) ?? ""
         groupCode = (json[ApiCustomerGroup.groupCode] as? String) ?? ""
         groupName = (json[ApiCustomerGroup.groupName] as? String) ?? ""
         groupDescription = (json[ApiCustomerGroup.groupDescription] as? String) ?? ""
         groupTypeId = (json[ApiCustomerGroup.groupTypeId] as? String) ?? ""
         roleId = (json[ApiCustomerGroup.roleId] as? String) ?? ""
         permissionId = (json[ApiCustomerGroup.permissionId] as? String) ?? ""
    }
}
struct MemberEducation {
    let memberId: String
    let educationLevelId: String
    let part: String
    let certificateId: String
    let certificateName: String
    let startYear: String
    let graduation: String
    
    init(json:[String:Any]) {
         memberId = (json[ApiMemberEducation.memberId] as? String) ?? ""
         educationLevelId = (json[ApiMemberEducation.educationLevelId] as? String) ?? ""
         part = (json[ApiMemberEducation.part] as? String) ?? ""
         certificateId = (json[ApiMemberEducation.certificateId] as? String) ?? ""
         certificateName = (json[ApiMemberEducation.certificateName] as? String) ?? ""
         startYear = (json[ApiMemberEducation.startYear] as? String) ?? ""
         graduation = (json[ApiMemberEducation.graduation] as? String) ?? ""
    }
}
struct MemberRole {
    let memberRoleId: String
    let memberRoleCode: String
    let memberRoleName: String
    let memberRoleDescription: String
    let registerPeriod: Int
    let itemsLimit: Int
    let daysLimit: Int
    let sizesLimit: Int
//    let categoriesLimit: String
    let groupId: String
    let tag: String
    
    init(json:[String:Any]) {
        memberRoleId = (json[ApiMemberRole.memberRoleId] as? String) ?? ""
        memberRoleCode = (json[ApiMemberRole.memberRoleCode] as? String) ?? ""
        memberRoleName = (json[ApiMemberRole.memberRoleName] as? String) ?? ""
        memberRoleDescription = (json[ApiMemberRole.memberRoleDescription] as? String) ?? ""
        groupId = (json[ApiMemberRole.groupId] as? String) ?? ""
        tag = (json[ApiMemberRole.tag] as? String) ?? ""
        
        registerPeriod = (json[ApiMemberRole.registerPeriod] as? Int) ?? 0
        itemsLimit = (json[ApiMemberRole.itemsLimit] as? Int) ?? 0
        daysLimit = (json[ApiMemberRole.daysLimit] as? Int) ?? 0
        sizesLimit = (json[ApiMemberRole.sizesLimit] as? Int) ?? 0
        
    }
    
}
struct MemberDetail {
    let member: MemberInformation?
    let memberAddress: MemberAddress?
    let thaiAddress: MemberThaiAddress?
    let certificate: Certificate?
    let educationLevel: EducationLevel?
    let memberGroup: MemberGroup?
    let lastLogin: String
    let memberRole: MemberRole?
    let memberEducation: MemberEducation?
    let messageText: String
    let isRegister: Bool
    let registerDate: Date?
    let durationTime: Int
    
    init(json: [String:Any]) {
        if let js = json[ApiMemberDetail.member] as? [String:Any] {
            member = MemberInformation.init(json: js)
        } else {
            member = nil
        }
        if let js = json[ApiMemberDetail.memberAddress] as? [String:Any] {
            memberAddress = MemberAddress.init(json: js)
        } else {
            memberAddress = nil
        }
        if let js = json[ApiMemberDetail.thaiAddress] as? [String:Any] {
            thaiAddress = MemberThaiAddress.init(json: js)
        } else {
            thaiAddress = nil
        }
        if let js = json[ApiMemberDetail.certificate] as? [String:Any] {
            certificate = Certificate.init(json: js)
        } else {
            certificate = nil
        }
        if let js = json[ApiMemberDetail.educationLevel] as? [String:Any] {
            educationLevel = EducationLevel.init(json: js)
        } else {
            educationLevel = nil
        }
        if let js = json[ApiMemberDetail.group] as? [String:Any] {
            memberGroup = MemberGroup.init(json: js)
        } else {
            memberGroup = nil
        }
        if let js = json[ApiMemberDetail.memberEducation] as? [String:Any] {
            memberEducation = MemberEducation.init(json: js)
        } else {
            memberEducation = nil
        }
        if let js = json[ApiMemberDetail.memberRole] as? [String:Any] {
            memberRole = MemberRole.init(json: js)
        } else {
            memberRole = nil
        }
        lastLogin = (json[ApiMemberDetail.lastLogin] as? String) ?? ""
        messageText = (json[ApiMemberDetail.messageText] as? String) ?? ""
        isRegister = (json[ApiMemberDetail.isRegister] as? Bool) ?? false
        registerDate = (json[ApiMemberDetail.registerDate] as? Date)
        durationTime = (json[ApiMemberDetail.durationTime] as? Int) ?? 0
        
        
    }
}

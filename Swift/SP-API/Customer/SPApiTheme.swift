//
//  SPApiTheme.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/12/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiTheme {
    static let set1 = "SET1"
    static let set2 = "SET2"
    static let set3 = "SET3"
    static let set4 = "SET4"
    static let set5 = "SET5"
    static let set6 = "SET6"
    static let set7 = "SET7"
}
struct SPTheme {
    let set1: String
    let set2: String
    let set3: String
    let set4: String
    let set5: String
    let set6: String
    let set7: String
    
    init(json: [String:Any]) {
        set1 = (json[ApiTheme.set1] as? String) ?? "#ffffff"
        set2 = (json[ApiTheme.set2] as? String) ?? "#383232"
        set3 = (json[ApiTheme.set3] as? String) ?? "#d44c27"
        set4 = (json[ApiTheme.set4] as? String) ?? "#ffffff"
        set5 = (json[ApiTheme.set5] as? String) ?? "#939597"
        set6 = (json[ApiTheme.set6] as? String) ?? "#d44c27"
        set7 = (json[ApiTheme.set7] as? String) ?? "#323232"
    }
    init() {
        set1 =  "#ffffff"
        set2 =  "#383232"
        set3 =  "#d44c27"
        set4 =  "#ffffff"
        set5 =  "#939597"
        set6 =  "#d44c27"
        set7 =  "#323232"
    }
}

//
//  SPDevicePreviewMedia.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/22/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiPreviewMedia {
    static let mediaId = "MEDIAID"
    static let roomId = "ROOMID"
    static let mediaCode = "MEDIACODE"
    static let mediaName = "MEDIANAME"
    static let categoryId = "CATEGORIESID"
    static let categoryName = "CATEGORIESNAME"
    static let subCategoryId = "SUBCATEGORIESID"
    static let subCategoryName = "SUBCATEGORIESNAME"
    static let popularVote = "POPULARVOTE"
    static let isbn = "BOOKISBN"
    static let author = "AUTHOR"
    static let coverUrl = "MediaCoverUrl"
    static let fileUrl = "EXTERNALURL"
    static let mediaKind = "MEDIAKIND"
    static let rentExpire = "RENTEXPIRE"
    static let rentDate = "RENTDATE"
    static let updateDate = "UPDATEDATE"
    static let messageText = "MessaageText"
    static let itemCount = "ITEMCOUNT"
    static let cid = "CID"
    static let hourCount = "NUMHOUR"
}
struct SPPreviewMedia {
    let mediaId:String
    let roomId:String
    let mediaCode:String
    let mediaName:String
    let categoriesId:String
    let categoriesName:String
    let subCategoriesId:String
    let subCategoriesName:String
    let popularVote:Int
    let bookISBN:String
    let author:String
    let mediaCoverUrl:String
    let externalUrl:String
    let mediaKind:String
    let rentExpire:String
    let rentDate:String
    let updateDate:String
    let messageText:String
    let cId:String
    let hourCount: String
    
    init(json:[String:Any]) {
        mediaId = (json[ApiPreviewMedia.mediaId] as? String) ?? ""
        roomId = (json[ApiPreviewMedia.roomId] as? String) ?? ""
        mediaCode = (json[ApiPreviewMedia.mediaCode] as? String) ?? ""
        mediaName = (json[ApiPreviewMedia.mediaName] as? String) ?? ""
        categoriesId = (json[ApiPreviewMedia.categoryId] as? String) ?? ""
        categoriesName = (json[ApiPreviewMedia.categoryName] as? String) ?? ""
        subCategoriesId = (json[ApiPreviewMedia.subCategoryId] as? String) ?? ""
        subCategoriesName = (json[ApiPreviewMedia.subCategoryName] as? String) ?? ""
        popularVote = (json[ApiPreviewMedia.popularVote] as? Int) ?? 0
        bookISBN = (json[ApiPreviewMedia.isbn] as? String) ?? ""
        author = (json[ApiPreviewMedia.author] as? String) ?? ""
        mediaCoverUrl = (json[ApiPreviewMedia.coverUrl] as? String) ?? ""
        externalUrl = (json[ApiPreviewMedia.fileUrl] as? String) ?? ""
        mediaKind = (json[ApiPreviewMedia.mediaKind] as? String) ?? ""
        rentExpire = (json[ApiPreviewMedia.rentExpire] as? String) ?? ""
        rentDate = (json[ApiPreviewMedia.rentDate] as? String) ?? ""
        updateDate = (json[ApiPreviewMedia.updateDate] as? String) ?? ""
        messageText = (json[ApiPreviewMedia.messageText] as? String) ?? ""
        cId = (json[ApiPreviewMedia.cid] as? String) ?? ""
        hourCount = (json[ApiPreviewMedia.hourCount] as? String) ?? ""
    }
}


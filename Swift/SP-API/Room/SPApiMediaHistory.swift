//
//  SPApiMediaHistory.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/12/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

//enum ApiMediaHistory {
//    static let history = "HISTYPE"
//    static let roomId = "ROOMID"
//    static let roomName = "ROOMNAME"
//    static let mediaId = "MEDIAID"
//    static let mediaCode = "MEDIACODE"
//    static let mediaName = "MEDIANAME"
//    static let mediaKind = "MEDIAKIND"
//    static let author = "AUTHOR"
//    static let categoriesName = "CATEGORIESNAME"
//    static let date = "DATE"
//    static let scoreVote = "SCOREVOTE"
//    static let messageText = "MessaageText"
//}
//
//struct SPMediaHistory {
//    let history: String
//     let roomId: String
//     let roomName: String
//     let mediaId: String
//     let mediaCode: String
//     let mediaName: String
//     let mediaKind: String
//     let author: String
//     let categoriesName: String
//     let date: String
//     let scoreVote: Int
//     let messageText: String
//
//    init(jsonData: [String:Any]) {
//        history = (jsonData[ApiMediaHistory.history] as? String) ?? ""
//        roomId = (jsonData[ApiMediaHistory.roomId] as? String) ?? ""
//        roomName = (jsonData[ApiMediaHistory.roomName] as? String) ?? ""
//        mediaId = (jsonData[ApiMediaHistory.mediaId] as? String) ?? ""
//        mediaCode = (jsonData[ApiMediaHistory.mediaCode] as? String) ?? ""
//        mediaName = (jsonData[ApiMediaHistory.mediaName] as? String) ?? ""
//        mediaKind = (jsonData[ApiMediaHistory.mediaKind] as? String) ?? ""
//        author = (jsonData[ApiMediaHistory.author] as? String) ?? ""
//        categoriesName = (jsonData[ApiMediaHistory.categoriesName] as? String) ?? ""
//        date = (jsonData[ApiMediaHistory.date] as? String) ?? ""
//        scoreVote = (jsonData[ApiMediaHistory.scoreVote] as? Int) ?? 0
//        messageText = (jsonData[ApiMediaHistory.messageText] as? String) ?? ""
//    }
//
//}
enum ApiMediaHistory {
    static let historyType = "TypeHISTYPE"
    static let rommName = "ROOMNAME"
    static let mediaId = "MEDIAID"
    static let mediaCode = "MEDIACODE"
    static let mediaName = "MEDIANAME"
    static let mediaKind = "MEDIAKIND"
    static let author = "AUTHOR"
    static let categoryName = "CATEGORIESNAME"
    static let date = "DATE"
    static let scoreVote = "SCOREVOTE"
    static let messageText = "MessaageText"
    static let buyType = "BUYTYPE"
    static let price = "PRICE"
    static let balance = "BALANCE"
    static let description = "DESP"
}

struct MediaHistory {
    let historyType: String
    let rommName: String
    let mediaId: String
    let mediaCode: String
    let mediaName: String
    let mediaKind: String
    let author: String
    let categoryName: String
    let date: String
    let scoreVote: String
    let messageText: String
    let buyType: String
    let price: String
    let balance: String
    let description: String
    init(json: [String:Any]) {
        historyType = (json[ApiMediaHistory.historyType] as? String) ?? ""
        rommName = (json[ApiMediaHistory.rommName] as? String) ?? ""
        mediaId = (json[ApiMediaHistory.mediaId] as? String) ?? ""
        mediaCode = (json[ApiMediaHistory.mediaCode] as? String) ?? ""
        mediaName = (json[ApiMediaHistory.mediaName] as? String) ?? ""
        mediaKind = (json[ApiMediaHistory.mediaKind] as? String) ?? ""
        author = (json[ApiMediaHistory.author] as? String) ?? ""
        categoryName = (json[ApiMediaHistory.categoryName] as? String) ?? ""
        date = (json[ApiMediaHistory.date] as? String) ?? ""
        scoreVote = (json[ApiMediaHistory.scoreVote] as? String) ?? ""
        messageText = (json[ApiMediaHistory.messageText] as? String) ?? ""
        buyType = (json[ApiMediaHistory.buyType] as? String) ?? ""
        price = ((json[ApiMediaHistory.price] as? String) ?? "0").toInt().toString()
        balance = ((json[ApiMediaHistory.balance] as? String) ?? "0").toInt().toString()
        description = (json[ApiMediaHistory.description] as? String) ?? ""
        
       
    }
}








//
//  Category.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiCategory {
    static let categryId = "CATEGORIESID"
    static let categoryCode = "CATEGORIESCODE"
    static let categoryName = "CATEGORIESNAME"
    static let subCategories = "DeviceSubCategories"
    static let messageText = "MessaageText"
}

enum ApiSubCategory {
    static let subCategoryId = "SUBCATEGORIESID"
    static let subCategoryCode = "SUBCATEGORIESCODE"
    static let subCategoryName = "SUBCATEGORIESNAME"
}

struct SPCategory {
    let categryId: String
    let categoryCode: String
    let categoryName: String
    let subCategories: [SPSubCategory]
    let messageText: String
    
    init(json:[String:Any]) {
        categryId = (json[ApiCategory.categryId] as? String) ?? ""
        categoryCode = (json[ApiCategory.categoryCode] as? String) ?? ""
        categoryName = (json[ApiCategory.categoryName] as? String) ?? ""
        messageText = (json[ApiCategory.messageText] as? String) ?? ""
        var items: [SPSubCategory] = []
        if let json = json[ApiCategory.subCategories] as? [[String:Any]] {
            
            for js in json {
                items.append(SPSubCategory(json: js))
            }
        }
        subCategories = items
    }
    
}

struct SPSubCategory {
     let subCategoryId: String
     let subCategoryCode: String
     let subCategoryName: String
    init(json: [String:Any]) {
        subCategoryId = (json[ApiSubCategory.subCategoryId] as? String) ?? ""
        subCategoryCode = (json[ApiSubCategory.subCategoryCode] as? String) ?? ""
        subCategoryName = (json[ApiSubCategory.subCategoryName] as? String) ?? ""
    }
}

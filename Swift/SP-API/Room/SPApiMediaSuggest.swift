//
//  SPApiMediaSuggest.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/13/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiMediaSuggest {
    static let mediaId = "MEDIAID"
    static let roomId = "ROOMID"
    static let mediaCode = "MEDIACODE"
    static let mediaName = "MEDIANAME"
    static let categoriesId = "CATEGORIESID"
    static let categoriesName = "CATEGORIESNAME"
    static let subCategoriesId = "SUBCATEGORIESID"
    static let subcategoriesName = "SUBCATEGORIESNAME"
    static let popularVote = "POPULARVOTE"
    static let bookISBN = "BOOKISBN"
    static let author = "AUTHOR"
    static let mediaCoverUrl = "MediaCoverUrl"
    static let mediaKind = "MEDIAKIND"
    static let rentExpire = "RENTEXPIRE"
    static let rentDate = "RENTDATE"
    static let updateDate = "UPDATEDATE"
    static let messageText = "MessaageText"
}

struct SPMediaSuggest {
    let mediaId: String
    let roomId: String
    let mediaCode: String
    let mediaName: String
    let categoriesId: String
    let categoriesName: String
    let subCategoriesId: String
    let subcategoriesName: String
    let popularVote: Int
    let bookISBN: String
    let author: String
    let mediaCoverUrl: String
    let mediaKind: String
    let rentExpire: String
    let rentDate: String
    let updateDate: String
    let messageText: String
    
    init(json:[String:Any]) {
         mediaId = (json[ApiMediaSuggest.mediaId] as? String) ?? ""
         roomId = (json[ApiMediaSuggest.roomId] as? String) ?? ""
         mediaCode = (json[ApiMediaSuggest.mediaId] as? String) ?? ""
         mediaName = (json[ApiMediaSuggest.mediaName] as? String) ?? ""
         categoriesId = (json[ApiMediaSuggest.categoriesId] as? String) ?? ""
         categoriesName = (json[ApiMediaSuggest.categoriesName] as? String) ?? ""
         subCategoriesId = (json[ApiMediaSuggest.subCategoriesId] as? String) ?? ""
         subcategoriesName = (json[ApiMediaSuggest.subcategoriesName] as? String) ?? ""
         popularVote = (json[ApiMediaSuggest.popularVote] as? Int) ?? 0
         bookISBN = (json[ApiMediaSuggest.bookISBN] as? String) ?? ""
         author = (json[ApiMediaSuggest.author] as? String) ?? ""
         mediaCoverUrl = (json[ApiMediaSuggest.mediaCoverUrl] as? String) ?? ""
         mediaKind = (json[ApiMediaSuggest.mediaKind] as? String) ?? ""
         rentExpire = (json[ApiMediaSuggest.rentExpire] as? String) ?? ""
         rentDate = (json[ApiMediaSuggest.rentDate] as? String) ?? ""
         updateDate = (json[ApiMediaSuggest.updateDate] as? String) ?? ""
         messageText = (json[ApiMediaSuggest.messageText] as? String) ?? ""
    }
}


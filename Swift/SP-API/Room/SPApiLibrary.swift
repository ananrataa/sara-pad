//
//  SPApiLibrary.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 10/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiLibrary {
    static let roomId = "ROOMID"
    static let roomCode = "ROOMCODE"
    static let roomName = "ROOMNAME"
    static let symbol = "SYMBOL"
    static let description = "DESP"
    static let tel1 = "TEL1"
    static let tel2 = "TEL2"
    static let tel3 = "TEL3"
    static let email = "EMAIL"
    static let tag = "TAG"
    static let memberStatus = "MemSTT"
    static let theme = "THEME"
    static let message = "MessaageText"
}

struct SPLibrary {
    let roomId: String
    let roomCode: String
    let roomName: String
    let symbol: String
    let description: String
    let tel1: String
    let tel2: String
    let tel3: String
    let email: String
    let tag: String
    let memberStatus: Int
    let theme: SPTheme
    let message: String
    
    init(json: [String: Any]) {
        roomId = (json[ApiLibrary.roomId] as? String) ?? ""
        roomCode = (json[ApiLibrary.roomCode] as? String) ?? ""
        roomName = (json[ApiLibrary.roomName] as? String) ?? ""
        symbol = (json[ApiLibrary.symbol] as? String) ?? ""
        description = (json[ApiLibrary.description] as? String) ?? ""
        tel1 = (json[ApiLibrary.tel1] as? String) ?? ""
        tel2 = (json[ApiLibrary.tel2] as? String) ?? ""
        tel3 = (json[ApiLibrary.tel3] as? String) ?? ""
        email = (json[ApiLibrary.email] as? String) ?? ""
        tag = (json[ApiLibrary.tag] as? String) ?? ""
        memberStatus = (json[ApiLibrary.memberStatus] as? Int) ?? 1
        if let js = json[ApiLibrary.theme] as? [String:Any] {
            theme = SPTheme(json: js)
        } else {
            theme = SPTheme()
        }
        message = (json[ApiLibrary.message] as? String) ?? ""
    }
}

class SPLib: NSObject {
    @objc var libId: String
    @objc var name: String
    @objc var mediaCount: Int
    @objc var image: UIImage?
    
    @objc init(json:[String: Any]) {
        libId = (json["ROOMID"] as? String) ?? ""
        name = (json["ROOMNAME"] as? String) ?? ""
        mediaCount = ((json["MEDIANUM"] as? String) ?? "0").toInt()
        if let url = URL(string: (json["ROOMIMG"] as? String) ?? "") {
            do {
                image = UIImage(data: try Data(contentsOf: url))
            } catch {
                
            }
        }
    }
}

enum ApiCourseLibrary {
    static let roomId = "ROOMID"
    static let mediaCount = "MEDIANUM"
    static let roomLogo = "ROOMIMG"
    static let roomCode = "ROOMCODE"
    static let roomName = "ROOMNAME"
    static let courseCount = "COURSENUM"
    static let mediaItems = "MediaItems"
    static let messageText = "MessaageText"
}

class SPCourseLibrary {
    let roomId: String
    let mediaCount: String
    let roomLogo: String
    let roomCode: String
    let roomName: String
    let courseCount: String
    let mediaItems: [SPPreviewMedia]
    let messageText: String
    
    init(json: [String:Any]) {
        roomId = (json[ApiCourseLibrary.roomId] as? String) ?? ""
        mediaCount = (json[ApiCourseLibrary.mediaCount] as? String) ?? ""
        roomLogo = (json[ApiCourseLibrary.roomLogo] as? String) ?? ""
        roomCode = (json[ApiCourseLibrary.roomCode] as? String) ?? ""
        roomName = (json[ApiCourseLibrary.roomName] as? String) ?? ""
        courseCount = (json[ApiCourseLibrary.courseCount] as? String) ?? ""
        messageText = (json[ApiCourseLibrary.messageText] as? String) ?? ""
        if let jsons = json[ApiCourseLibrary.mediaItems] as? [[String:Any]] {
            var items = [SPPreviewMedia]()
            jsons.forEach { (js) in
                items.append(SPPreviewMedia(json: js))
            }
            mediaItems = items
        } else {
            mediaItems = []
        }
    }
}

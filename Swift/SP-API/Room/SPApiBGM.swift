//
//  SPApiBGM.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/14/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiBGMPlayList {
    static let bgmId = "BGMID"
    static let bgmName = "BGMNAME"
    static let duration = "DURATION"
    static let tracks = "ITEMS"
}

enum ApiBGMItem {
    static let trackId = "TRACKID"
    static let trackName = "TRACKNAME"
    static let trackUrl = "TRACKURL"
}


struct SPBGMTrack {
    let trackId: String
    let trackName: String
    let trackUrl: String
    
    init(json:[String:Any]) {
        trackId = (json[ApiBGMItem.trackId] as? String) ?? ""
        trackName = (json[ApiBGMItem.trackName] as? String) ?? ""
        trackUrl = (json[ApiBGMItem.trackUrl] as? String) ?? ""
    }
    
}

struct SPBGMPlayList {
    let bgmId: String
    let bgmName: String
    let duration: String
    var tracks: [SPBGMTrack] = []
    var selected: Bool = false
    init(json:[String:Any]) {
        bgmId = (json[ApiBGMPlayList.bgmId] as? String) ?? ""
        bgmName = (json[ApiBGMPlayList.bgmName] as? String) ?? ""
        duration = (json[ApiBGMPlayList.duration] as? String) ?? ""
        if let js = json[ApiBGMPlayList.tracks] as? [[String:Any]] {
            for jsItem in js {
                tracks.append(SPBGMTrack(json: jsItem))
            }
        }
    }
}

struct SPBGM {
    var playLists: [SPBGMPlayList] = []
    init(json:[[String:Any]]) {
        for js in json {
            playLists.append(SPBGMPlayList(json: js))
        }
    }
}

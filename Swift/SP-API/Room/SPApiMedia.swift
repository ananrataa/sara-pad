//
//  SPApi.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/12/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum MediaKind: String {
    case Audio = "A"            //Audio / เสียง;
    case Document = "D"         //Book / หนังสือ;
    case Magazine = "M"         //Magazine / นิตยสาร;
    case Video = "V"            //Video / วีดีโอ;
    case Youtube = "YU"         //Youtube / ยูทูป; (ลิ้งภายนอก)
    case ExternalPDF = "EP"     //External PDF / หนังสือภายนอก; (ลิ้งภายนอก)
    case ExternalSound = "ES"   //External Sound / เสียงภายนอก; (ลิ้งภายนอก)
    case ExternalVideo = "EV"   //External Video / วิดีโอภายนอก; (ลิ้งภายนอก)
    case ExternalLink = "EL"    //External Link / ลิ้งภายนอก; (ลิ้งภายนอก)
    case Material = "T"         //Material / สิ่งของ; (Label สถานที่)
}
enum ApiMedia {
    static let mediaId = "MEDIAID"
    static let mediaCode = "MEDIACODE"
    static let mediaName = "MEDIANAME"
    static let mediaKind = "MEDIAKIND"
    static let author = "AUTHOR"
    static let abstract = "ABSTRACT"
    static let tag = "TAG"
    static let version = "VERSION"
    static let volume = "VOLUME"
    static let amount = "AMOUNT"
    static let annotation = "ANNOTATION"
    static let categoriesId = "CATEGORIESID"
    static let categoriesName = "CATEGORIESNAME"
    static let subCategoriesId = "SUBCATEGORIESID"
    static let subCategoriesName = "SUBCATEGORIESNAME"
    static let roomId = "ROOMID"
    static let roomName = "ROOMNAME"
    static let publisherId = "PUBLISHERID"
    static let publisherName = "PUBLISHERNAME"
    static let mediaTypeId = "MEDIATYPEID"
    static let demoFileURLs = "DemoFileURLs"
    static let coverFileURLs = "CoverFileURLs"
    static let contentFileURLs = "ContentFileURLs"
    static let theme = "THEME"
    static let rentLimitDay = "RENTLIMITDAY"
    static let popularVote = "POPULARVOTE"
    static let scoreVote = "SCOREVOTE"
    static let isDownLoad = "isDownload"
    static let isMember = "isMember"
    static let isFollow = "isFollow"
    static let isFirst = "isFirst"
    static let isPublic = "isPublic"
    static let bookISBN = "BOOKISBN"
    static let mediaCoverUrl = "MediaCoverUrl"
    static let rentExpire = "RENTEXPIRE"
    static let rentDate = "RENTDATE"
    static let updateDate = "UPDATEDATE"
    static let messageText = "MessaageText"
    //new
    static let isBuy = "isBuy"
    static let priceList = "PriceList"
    //Material
    static let availableCopies = "CURRENTMETERIA"
    static let availableDate = "LASTEXPRIE"
    static let location = "LOCATION"
    static let isQRCode = "SERVEREABLE"
    //Course
    static let demoCourseVideo = "demoVideoFile"
}
enum ApiMediaCover {
    static let url = "URL"
    static let fileExtension = "FileExten"
    static let fileSizeMB = "FileSizeMB"
    static let fileType = "FileType"
    static let duration = "DURATION"
    static let page = "PAGE"
}

class SPMedia: NSObject {
    @objc let mediaId: String
    @objc let mediaCode: String
    @objc let author: String
    @objc let mediaName: String
    @objc let roomId: String
    @objc let roomName: String
    @objc let categoriesId: String
    @objc let categoriesName: String
    @objc let subCategoriesId: String
    @objc let subCategoriesName: String
    @objc let popularVote: Int
    @objc let scoreVote: Int
    @objc let mediaKind: String
    @objc let abstract: String
    @objc let isDownLoad: Bool
    @objc let rentLimitDay: Int
    @objc let isMember: Bool
    @objc let isPublic: Bool
    @objc var isFollow: Bool
    @objc let annotation: String
    @objc let publisherName: String
    @objc let publisherId: String
    @objc let version: String //ครั้งที่พิมพ์
    @objc let amount: String //จำนวนที่พิมพ์
    @objc let year: String //ปีที่พิมพ์
    @objc let coverFile: SPMediaFileURL?
    @objc let mediaFile: SPMediaFileURL?
    @objc let previewFile: SPMediaFileURL?
    @objc let rentDate: String
    @objc let expireDate: String
    @objc let tags: [String]
    //New
    @objc let isBuy: Bool
    @objc let priceList: [SPMediaPrice]
    //Materials
    @objc let building: String
    @objc let floor: String
    @objc let room: String
    @objc let cabinet: String
    @objc let row: String
    @objc let shelf: String
    @objc let availableCopies: String
    @objc let availableDate: String
    @objc let isQRCode: Bool
    //Course
    let demoCourseVideo: SPMediaFileURL?
    let courseDetail: SPMediaFileURL?
    
    @objc init(json:[String:Any]) {
        mediaId = (json[ApiMedia.mediaId] as? String) ?? ""
        mediaCode = (json[ApiMedia.mediaCode] as? String) ?? ""
        author = (json[ApiMedia.author] as? String) ?? ""
        mediaName = (json[ApiMedia.mediaName] as? String) ?? ""
        roomId = (json[ApiMedia.roomId] as? String) ?? ""
        roomName = (json[ApiMedia.roomName] as? String) ?? ""
        categoriesId = (json[ApiMedia.categoriesId] as? String) ?? ""
        categoriesName = (json[ApiMedia.categoriesName] as? String) ?? ""
        subCategoriesId = (json[ApiMedia.subCategoriesId] as? String) ?? ""
        subCategoriesName = (json[ApiMedia.subCategoriesName] as? String) ?? ""
        popularVote = (json[ApiMedia.popularVote] as? Int) ?? 0
        scoreVote = (json[ApiMedia.scoreVote] as? Int) ?? 0
        mediaKind = (json[ApiMedia.mediaKind] as? String) ?? ""
        abstract = (json[ApiMedia.abstract] as? String) ?? ""
        isDownLoad = ((json[ApiMedia.isDownLoad] as? Int) ?? 0) == 1
        rentLimitDay = (json[ApiMedia.rentLimitDay] as? Int) ?? 0
        isMember = ((json[ApiMedia.isMember] as? Int) ?? 0) == 1
        isPublic = ((json[ApiMedia.isPublic] as? Int) ?? 0) == 1
        isFollow = ((json[ApiMedia.isFollow] as? Int) ?? 0) == 1
        annotation = (json[ApiMedia.annotation] as? String) ?? ""
        publisherName = (json[ApiMedia.publisherName] as? String) ?? ""
        publisherId = (json[ApiMedia.publisherId] as? String) ?? ""
        version = (json[ApiMedia.volume] as? String) ?? ""
        amount = (json[ApiMedia.amount] as? String) ?? ""
        year = (json[ApiMedia.version] as? String) ?? ""
        rentDate = (json[ApiMedia.rentDate] as? String) ?? ""
        expireDate = (json[ApiMedia.rentExpire] as? String) ?? ""
        if let tag = (json[ApiMedia.tag] as? String) {
            tags = tag.components(separatedBy: ",")
            
        } else {
            tags = []
        }
        
        if let json = json[ApiMedia.coverFileURLs] as? [String:Any] {
            coverFile = SPMediaFileURL.init(json: json)
        } else {
            coverFile = nil
        }
        if let json = json[ApiMedia.contentFileURLs] as? [String:Any] {
            mediaFile = SPMediaFileURL.init(json: json)
        } else {
            mediaFile = nil
        }
        if let json = json[ApiMedia.demoFileURLs] as? [String:Any] {
            previewFile = SPMediaFileURL.init(json: json)
        } else {
            previewFile = nil
        }
        //Course
        courseDetail = previewFile
        if let json = json[ApiMedia.demoCourseVideo] as? [String:Any] {
            demoCourseVideo = SPMediaFileURL.init(json: json)
        } else {
            demoCourseVideo = nil
        }
        
        isBuy = ((json[ApiMedia.isBuy] as? Int) ?? 0) == 1
        var pl = [SPMediaPrice]()
        if let jsArray = json[ApiMedia.priceList] as? [[String:Any]] {
            for js in jsArray {
                pl.append(SPMediaPrice(json: js))
            }
        }
        priceList = pl
        
        //Material
        availableCopies = (json[ApiMedia.availableCopies] as? String) ?? "-"
        availableDate = (json[ApiMedia.availableDate] as? String) ?? "-"
        isQRCode = ((json[ApiMedia.isQRCode] as? Int) ?? 0) == 1
        if let locString = json[ApiMedia.location] as? String {
            let components = NSString(string: locString).components(separatedBy: ",")
            if components.count >= 6 {
                building = components[0]
                floor = components[1]
                room = components[2]
                row = components[3]
                cabinet = components[4]
                shelf = components[5]
                return
            }
        }
        building = "-"
        floor = "-"
        room = "-"
        cabinet = "-"
        row = "-"
        shelf = "-"
    }
    func isPlayerMedia() -> Bool {
        let kind = mediaKind.uppercased()
        return kind == MediaKind.Video.rawValue || kind == MediaKind.Audio.rawValue
    }
}

class SPMediaFileURL: NSObject {
    @objc let url: String
    @objc let fileExtension: String
    @objc let fileSizeMB: Double
    @objc let fileName: String
    @objc let duration: Double
    @objc let page: Int
    
    init(json:[String:Any]) {
        url = (json[ApiMediaCover.url] as? String) ?? ""
        fileName = (url as NSString).lastPathComponent
        fileExtension = (json[ApiMediaCover.fileExtension] as? String) ?? ""
        fileSizeMB = (json[ApiMediaCover.fileSizeMB] as? Double) ?? 0.0
        duration = (json[ApiMediaCover.duration] as? Double) ?? 0.0
        page = (json[ApiMediaCover.page] as? Int) ?? 0
    }
    
}

enum ApiDeviceLimitRent {
    static let roomId = "ROOMID"
    static let roomName = "ROOMNAME"
    static let symbol = "SYMBOL"
    static let limitRent = "LIMITRENT"
    static let balance = "BALANCE"
    static let amount = "AMOUNT"
    static let messageText = "MessaageText"
}

class SPDeviceLimitRent: NSObject {
    @objc let roomId: String
    @objc let roomName: String
    @objc let symbol: String
    @objc let limitRent: Int
    @objc let balance: Int
    @objc let amount: Int
    @objc let messageText: String
    
    init(json: [String:Any]) {
        roomId = (json[ApiDeviceLimitRent.roomId] as? String) ?? ""
        roomName = (json[ApiDeviceLimitRent.roomName] as? String) ?? ""
        symbol = (json[ApiDeviceLimitRent.symbol] as? String) ?? ""
        limitRent = (json[ApiDeviceLimitRent.limitRent] as? Int) ?? 0
        balance = (json[ApiDeviceLimitRent.balance] as? Int) ?? 0
        amount = (json[ApiDeviceLimitRent.amount] as? Int) ?? 0
        messageText = (json[ApiDeviceLimitRent.messageText] as? String) ?? ""
    }
}

class SPMediaPrice: NSObject {
    @objc let payType: String
    @objc let saleId: String
    init(json: [String:Any]) {
        payType = (json["PAY_TYPE"] as? String) ?? ""
        saleId = (json["SALEID"] as? String) ?? ""
    }
}

class SPMediaDownload: NSObject {
    @objc var mediaId: String
    @objc var rentDate: String
    @objc var expDate: String
    @objc var shelfId: String
    @objc var cId: String = ""
    @objc var canDownload: Bool //ISHASROLE = TRUE
    @objc init(json: [String:Any]) {
        mediaId = (json["MEDIAID"] as? String) ?? ""
        rentDate = (json["RENTDATE"] as? String) ?? ""
        expDate = (json["RENTEXPIRE"] as? String) ?? ""
        shelfId = "\((json["TYPESHEF"] as? Int) ?? 0)"
        canDownload = ((json["ISHASROLE"] as? Int) ?? 0) == 1
    }
    override init() {
        mediaId =  ""
        rentDate =  ""
        expDate =  ""
        shelfId = "0"
        canDownload = false
    }
}


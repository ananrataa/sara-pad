//
//  SPApiMemberShelfMedia.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiMemberShelf {
    static let roomId = "ROOMID"
    static let roomCode = "ROOMCODE"
    static let roomName = "ROOMNAME"
    static let mediaNum = "MEDIANUM"
    static let roomImage = "ROOMIMG"
    static let mediaItems = "MediaItems"
    static let messageText = "MessaageText"
}


class MemberRoomShelf {
    let roomId:String
    let roomCode:String
    let roomName:String
    let mediaItems:[SPPreviewMedia]
    let messageText:String
    
    init(json:[String:Any]) {
        roomId = (json[ApiMemberShelf.roomId] as? String) ?? ""
        roomCode = (json[ApiMemberShelf.roomCode] as? String) ?? ""
        roomName = (json[ApiMemberShelf.roomName] as? String) ?? ""
        messageText = (json[ApiMemberShelf.messageText] as? String) ?? ""
        var items = [SPPreviewMedia]()
        if let jsItems = json[ApiMemberShelf.mediaItems] as? [[String:Any]] {
            for js in jsItems {
                items.append(SPPreviewMedia(json: js))
            }
            mediaItems = items
        } else {
            mediaItems = []
        }
    }
}


class MemberShelf: NSObject {
    let roomMedias: [MemberRoomShelf]
    init(json: [[String:Any]]) {
        var medias = [MemberRoomShelf]()
        for js in json {
            medias.append(MemberRoomShelf(json: js))
        }
        roomMedias = medias
    }
}

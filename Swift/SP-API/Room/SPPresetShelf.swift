//
//  SPPresetShelf.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 21/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiPresetShelf {
    static let roomId = "ROOMID"
    static let roomName = "ROOMNAME"
    static let items = "PresetItems"
    static let messageText = "MessaageText"
}
enum ApiPresetSubject {
    static let roomId = "ROOMID"
    static let roomName = "ROOMNAME"
    static let subjectId = "SUBJECTID"
    static let subjectCode = "SUBJECTCODE"
    static let subjectName = "SUBJECTNAME"
    static let sectionCode = "SECTIONCODE"
    static let year = "YEAR"
    static let semester = "SEMESTER"
    static let updateDate = "UPDATEDATE"
    static let items = "MediaItems"
}
class PresetShelf {
    let roomId: String
    let roomName: String
    let items: [PresetSubject]
    let messageText: String
    init(json: [String:Any]) {
        roomId = (json[ApiPresetShelf.roomId] as? String) ?? ""
        roomName = (json[ApiPresetShelf.roomName] as? String) ?? ""
        messageText = (json[ApiPresetShelf.messageText] as? String) ?? ""
        var subjects: [PresetSubject] = []
        if let data = json[ApiPresetShelf.items] as? [[String:Any]] {
            data.forEach { (js) in
                subjects.append(PresetSubject(json: js))
            }
        }
        items = subjects
    }
}

class PresetSubject {
    let roomId: String
    let roomName: String
    let subjectId: String
    let subjectCode: String
    let subjectName: String
    let sectionCode: String
    let year: String
    let semestor: String
    let updateDate: String
    let items: [SPPreviewMedia]
    
    init(json: [String:Any]) {
        roomId = (json[ApiPresetSubject.roomId] as? String) ?? ""
        roomName = (json[ApiPresetSubject.roomName] as? String) ?? ""
        subjectId = (json[ApiPresetSubject.subjectId] as? String) ?? ""
        subjectCode = (json[ApiPresetSubject.subjectCode] as? String) ?? ""
        subjectName = (json[ApiPresetSubject.subjectName] as? String) ?? ""
        sectionCode = (json[ApiPresetSubject.sectionCode] as? String) ?? ""
        year = (json[ApiPresetSubject.year] as? String) ?? ""
        semestor = (json[ApiPresetSubject.semester] as? String) ?? ""
        updateDate = (json[ApiPresetSubject.updateDate] as? String) ?? ""
        var medias: [SPPreviewMedia] = []
        if let data = json[ApiPresetSubject.items] as? [[String:Any]] {
            data.forEach { (js) in
                medias.append(SPPreviewMedia(json: js))
            }
        }
        items = medias
    }
}

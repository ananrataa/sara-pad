//
//  ฆญฤยรฤืืนะฟะรนื.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiAnnotationList {
    static let path = "PATH"
    static let pathLoad = "PATHLOAD"
    static let displayName = "DISPLAY_NAME"
}

struct SPAnnotationList {
    let path: String
    let pathLoad: String
    let displayName: String
    
    init(json:[String:Any]) {
        path = (json[ApiAnnotationList.path] as? String) ?? ""
        pathLoad = (json[ApiAnnotationList.pathLoad] as? String) ?? ""
        displayName = (json[ApiAnnotationList.displayName] as? String) ?? ""
    }
}


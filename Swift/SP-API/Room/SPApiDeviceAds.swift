//
//  SPApiDeviceAds.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 16/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
enum AdsType {
    static let None = 0           //ไม่ระบุประเภท
    static let SplashScreen = 1   //เต็มจอหลังจากโหลด SplashScreen
    static let TopSwapped = 2     //ป้ายชุดหน้าหลัก
    static let PullPopup = 3      //ป้ายที่ขึ้นตอนเลื่อนหน้าจอ
    static let Content = 4        //ป้ายภายในรายละเอียด
}
enum ApiAdsItem {
    static let adsId = "ADSID"
    static let roomId = "ROOMID"
    static let adsCode = "ADSCODE"
    static let adsName = "ADSNAME"
    static let description = "DESP"
    static let linkUrl = "LNKURL"
    static let sytemOwner = "SYSTEMOWNER"
    static let adsImageUrl = "AdsImageUrl"
    static let piority = "Priority"
    static let messageText = "MessaageText"
    static let skip = "SKIP"
    static let duration1 = "TIMEMANUL"
    static let duration2 = "TIMEAUTO"
}

struct SPApiAdsItem {
    let adsId: String
    let roomId: String
    let adsCode: String
    let adsName: String
    let description: String
    let linkUrl: String
    let isSytemOwner: Bool
    let adsImageUrl: String
    let piority: Int
    let messageText: String
    let duration1: Double
    let duration2: Double
    let skip: Bool
    init(json:[String:Any]) {
        adsId = (json[ApiAdsItem.adsId] as? String) ?? ""
        roomId = (json[ApiAdsItem.roomId] as? String) ?? ""
        adsCode = (json[ApiAdsItem.adsCode] as? String) ?? ""
        adsName = (json[ApiAdsItem.adsName] as? String) ?? ""
        description = (json[ApiAdsItem.description] as? String) ?? ""
        linkUrl = (json[ApiAdsItem.linkUrl] as? String) ?? ""
        isSytemOwner = (json[ApiAdsItem.sytemOwner] as? Bool) ?? false
        adsImageUrl = (json[ApiAdsItem.adsImageUrl] as? String) ?? ""
        piority = (json[ApiAdsItem.piority] as? Int) ?? 0
        messageText = (json[ApiAdsItem.messageText] as? String) ?? ""
        skip = ((json[ApiAdsItem.skip] as? String) ?? "") == "Y"
        if let t1 = json[ApiAdsItem.duration1] as? String {
            let strArray = t1.components(separatedBy: ".")
            duration1 = strArray.count > 1 ? (60*strArray[0].toDouble() + strArray[1].toDouble()) : 0
        } else {
            duration1 = 0
        }
        if let t2 = json[ApiAdsItem.duration2] as? String {
            let strArray = t2.components(separatedBy: ".")
            duration2 = strArray.count > 1 ? (60*strArray[0].toDouble() + strArray[1].toDouble()) : 0
        } else {
            duration2 = 0
        }
    }
}


@available(iOS 11.0, *)
struct SPRoomAds {
    var area1: [SPApiAdsItem] = []
    var area2: [SPApiAdsItem] = []
    var area3: [SPApiAdsItem] = []
    var area4: [SPApiAdsItem] = []
    var area5: [SPApiAdsItem] = []
    var area6: [SPApiAdsItem] = []
    init(json: [String:Any]) {
        if let js = json["FULLSCEEN"] as?[[String:Any]] {
            for j in js {
                self.area1.append(SPApiAdsItem(json: j))
            }
        }
        if let js = json["ADS_TOP5"] as?[[String:Any]] {
            for j in js {
                self.area2.append(SPApiAdsItem(json: j))
            }
        }
        if let js = json["ADS_CATAGORIE"] as?[[String:Any]] {
            for j in js {
                self.area3.append(SPApiAdsItem(json: j))
            }
        }
        if let js = json["ADS_READER"] as?[[String:Any]] {
            for j in js {
                self.area4.append(SPApiAdsItem(json: j))
            }
        }
        if let js = json["ADS_Video"] as?[[String:Any]] {
            for j in js {
                self.area5.append(SPApiAdsItem(json: j))
            }
        }
        if let js = json["ADS_Audio"] as?[[String:Any]] {
            for j in js {
                self.area6.append(SPApiAdsItem(json: j))
            }
        }
    }
}

class ADImage: NSObject {
    @objc var adImage: UIImage
    @objc var adLink: String
    @objc var adName: String
    @objc init(image: UIImage, adLink: String, name: String) {
        self.adImage = image
        self.adLink = adLink
        self.adName = name
        super.init()
    }
}


class ADPdf: NSObject {
    @objc var adX: Int
    @objc var adImages: [ADImage]
    
    @objc init(interval: Int, images: [ADImage]) {
        self.adX = interval
        self.adImages = images
        super.init()
    }
}

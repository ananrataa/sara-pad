//
//  RoomAPI.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/13/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum SPApiUrl {
    static let apiKey = "IE11bHRpbWVkaWEgIA0KIA0KICANCiBEaWdpdGFsIENvbnRlbnRzIA0K"
    
    static let deviceAds = "%@api/DeviceAds/GetAds?deviceCode=%@&RoomId=%@&adsType=%i&isSystemOwner=%@"
    static let bgm = "%@api/DeviceRoom/RoomBgm?deviceCode=%@&roomId=%@"
    static let mediaSuggested = "%@api/DeviceMedia/MediasSuggested?deviceCode=%@&roomId=%@&mediaId=%@&mediaKind=%@"
//    static let browseMemdiaDetail = "%@api/DeviceMedia/BrowsMediaDetail?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i&isPreset=%@"
    static let browseMemdiaDetail = "%@api/DeviceMedia/BrowsMediaDetail_new?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i"
    static let devicePreviewMedias = "%@api/DeviceMedia/MediaSearch?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&findValue=%@&loadIndex=%i"
    static let devicePreviewMediasRoomMember = "%@api/DeviceMedia/MediaSearchRoomMember?deviceCode=%@&mediaKind=%@&catId=&subId=&findValue=%@&loadIndex=%i"
    static let devicePreviewMediaTotal = "%@api/DeviceMedia/MediaSearchTotal?deviceCode=%@&mediaKind=%@&catId=&subId=&findValue=%@&loadIndex=%i"
    static let deviceRoomDetail = "%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@"
    static let memberShelf = "%@api/DeviceMedia/MediasMemberShelf?deviceCode=%@&memberId=%@"
    static let annotationList = "%@api/DeviceMedia/AnnotationListMy?memberID=%@&mediaID=%@&roomID=%@&OS=%@"
    static let mediaVoice = "%@api/DeviceMedia/MediaVoice?deviceCode=%@&mediaId=%@"
    static let category = "%@api/DeviceMedia/Categorie?deviceCode=%@&roomId=%@&loadIndex=%i"
    static let deviceLimitRent = "%@api/DeviceMedia/LimitRent?deviceCode=%@&itemLimit=%i&dayLimit=%i"
    static let libraryApply = "%@api/DeviceRoom/RegisterRoomGenUrl?memberID=%@&roomId=%@"
    //Member
    static let memberDetail = "%@api/DevicePlatform/GetMembersDetailAsync?MemberId=%@"
    static let mergeMember = "%@api/DevicePlatform/SendMergeCode?MergeMemberID=%@&MainUserID=%@"
    
    //Advertising
    static let roomAds = "%@api/DeviceAds/GetAds_New?deviceCode=%@&roomID=%@&AdsType=%@"
}

@available(iOS 11.0, *)
class RoomAPI: NSObject {
    var timer: Timer?
    let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
    private override init() {}
    @objc static let shared = RoomAPI.init()
    
    @objc func postLibraryApply(roomId: String, memberId: String, completion:@escaping ([String:Any]?)->Void) {
        let stringUrl = String(format: SPApiUrl.libraryApply, hostIpRoomAPI, memberId, roomId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "POST") { (json) in
            if let json = json as? [String:Any] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func getMediaVoices(deviceCode: String, mediaId: String, completion:@escaping ([SPMediaVoiceList])->Void) {
        let stringUrl = String(format: SPApiUrl.mediaVoice, hostIpRoomAPI, deviceCode, mediaId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]] {
                var medias = [SPMediaVoiceList]()
                for js in json {
                    let mv = SPMediaVoiceList(json: js)
                    medias.append(mv)
                }
                //sort by voices.count (using overload > function)
                medias.sort(by: >)
                completion(medias)
            } else {
                completion([])
            }
        }
    }
    func getPreviewMedias(deviceCode:String, roomId:String, mediaKind:String, catId: String, subId:String, findValue:String, loadIndex:Int, completion:@escaping ([SPPreviewMedia]?)->Void) {
        let stringUrl = String(format: SPApiUrl.devicePreviewMedias, hostIpRoomAPI, deviceCode, roomId, mediaKind, catId, subId, findValue, loadIndex)
        guard !findValue.isEmpty else {
            completion([])
            return
        }
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            
            if let json = json as? [[String:Any]] {
                var medias = [SPPreviewMedia]()
                for js in json {
                    medias.append(SPPreviewMedia(json: js))
                }
                completion(medias)
            } else {
                completion(nil)
            }
            
        }
    }
    func getPreviewMediasRoomMember(deviceCode:String, mediaKind:String, findValue:String, loadIndex:Int, completion:@escaping ([SPPreviewMedia]?)->Void) {
        let stringUrl = String(format: SPApiUrl.devicePreviewMediasRoomMember, hostIpRoomAPI, deviceCode, mediaKind, findValue, loadIndex)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]] {
                var medias = [SPPreviewMedia]()
                for js in json {
                    medias.append(SPPreviewMedia(json: js))
                }
                completion(medias)
            } else {
                completion(nil)
            }
            
        }
    }
    func getPreviewMediasTotal(deviceCode:String, mediaKind:String, findValue:String, loadIndex:Int, completion:@escaping ([SPPreviewMedia]?)->Void) {
        let stringUrl = String(format: SPApiUrl.devicePreviewMediaTotal, hostIpRoomAPI, deviceCode, mediaKind, findValue, loadIndex)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]] {
                var medias = [SPPreviewMedia]()
                for js in json {
                    medias.append(SPPreviewMedia(json: js))
                }
                completion(medias)
            } else {
                completion(nil)
            }
            
        }
    }
    func roomBGM(deviceCode: String, roomId: String, completion: @escaping (Any?)->Void) {
        let stringUrl = String(format: SPApiUrl.bgm, hostIpRoomAPI, deviceCode, roomId)
        sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            completion(json)
        }
    }
    func deviceRoomDetail(memberId: String, roomId: String, completeion: @escaping (SPRoomDetail?, [String:Any]?)->Void) {
        let api = "%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@&memberID=%@"
        let stringUrl = String(format: api,hostIpRoomAPI, deviceCode, roomId, memberId)
        sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:Any] {
                completeion(SPRoomDetail(json: json), json)
            } else {
                completeion(nil, nil)
            }
        }
    }
    func mediaSuggested(roomId: String, mediaId: String, mediaKind: String, completion: @escaping ([SPMediaSuggest])->Void)  {
        let stringUrl = String(format: SPApiUrl.mediaSuggested, hostIpRoomAPI, deviceCode, roomId, mediaId, mediaKind)
        
        sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let js = json as? [[String:Any]] {
                var mediaItems = [SPMediaSuggest]()
                for j in js {
                    mediaItems.append(SPMediaSuggest(json: j))
                }
                completion(mediaItems)
            } else {
                completion([])
            }
        }
    }
    
    func browseMediaDetail(roomId: String, mediaId: String, limit: Int, completion: @escaping (SPMedia?)->Void)  {
        let api = "%@api/DeviceMedia/BrowsMediaDetail_new?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, mediaId, limit)
        
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:Any] {
                completion(SPMedia(json: json))
            } else {
                completion(nil)
            }
        }
    }
    func sharedAnnotations(roomId: String, mediaId: String, memberId: String, os: String, completion: @escaping ([SPAnnotationList])->Void) {
        let stringUrl = String(format: SPApiUrl.annotationList, hostIpRoomAPI, memberId, mediaId, roomId, os)
        sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]] {
                var items:[SPAnnotationList] = []
                for js in json {
                    items.append(SPAnnotationList(json: js))
                }
                completion(items)
            } else {
                completion([])
            }
        }
    }
    func mediaCategories(loadIndex: Int, completion: @escaping ([SPCategory])->Void) {
        
        let curLibraryId = UserDefaults.standard.string(forKey: KeyCuurrentUsedLribaryID) ?? ""
        let strUrl = String(format: SPApiUrl.category, hostIpRoomAPI, deviceCode, curLibraryId, loadIndex)
        sendHTTPRequest(strUrl, returnType: "JSON") { (json) in
            var items:[SPCategory] = []
            if let json = json as? [[String:Any]] {
                for js in json {
                    items.append(SPCategory(json: js))
                }
            }
            completion(items)
        }
    }
    func deviceLimitRent(items: Int, days: Int, completion: @escaping ([SPDeviceLimitRent])->Void) {
        
        let strUrl = String(format: SPApiUrl.deviceLimitRent, hostIpRoomAPI, deviceCode, items, days)
        sendHTTPRequest(strUrl, returnType: "JSON") { (json) in
            var items:[SPDeviceLimitRent] = []
            if let json = json as? [[String:Any]] {
                for js in json {
                    items.append(SPDeviceLimitRent(json: js))
                }
            }
            completion(items)
        }
    }
    @objc func getData(urlString: String, complettion:@escaping (NSData?)->Void) {
        sendHTTPRequest(urlString, returnType: "DATA") { (data) in
            if let data = data as? NSData {
                complettion(data)
            } else {
                complettion(nil)
            }
        }
    }
    //MARK:- URL Request
    func sendHTTPRequest(_ url: String, returnType: String, method: String? = "GET", completion: @escaping (Any?)->Void) {
        guard
            let stringUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = NSURL(string: stringUrl) as URL?
        else {
            completion(nil)
            return
        }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

        timer = Timer.scheduledTimer(withTimeInterval: 32, repeats: false, block: { (timer) in
            session.invalidateAndCancel()
            timer.invalidate()
        })
        
        let queue = DispatchQueue(label: "th.co.mbox.MBlaze")
        queue.async {
            if SPUtility.shared.isConnectedToNetwork() {
                DispatchQueue.main.async {[weak self] in
                    let request: NSMutableURLRequest = NSMutableURLRequest()
                    request.url = url
                    request.httpMethod = method!
                    request.timeoutInterval = 30
                    request.addValue(SPApiUrl.apiKey, forHTTPHeaderField: "API_KEY")
                    
                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                        guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                            else {
                                print("error: not a valid http response")
                                self?.cancelTimer()
                                completion(nil)
                                return
                        }
                        if httpResponse.statusCode == 200 {
                            self?.cancelTimer()
                            if returnType == "JSON" {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: receivedData, options: [])
                                    completion(json)
                                } catch  {
                                    completion(nil)
                                }
                            } else if returnType == "STRING" {
                                if let data = data, let text = String.init(data: data, encoding: String.Encoding.utf8) {
                                    completion(text)
                                } else {
                                    completion(nil)
                                }
                            } else if returnType == "DATA" {
                                if let data = data as NSData? {
                                    completion(data)
                                }
                            }
                        } else if let error = error {
                            self?.cancelTimer()
                            print(error.localizedDescription)
                            completion(nil)
                        } else {
                            self?.cancelTimer()
                            completion(nil)
                        }
                    })
                    dataTask.resume()
                }
            } else {
                self.cancelTimer()
                completion(nil)
                print("No internet connection!")
            }
        }
    }
    
    func cancelTimer() {
        if let timer = timer {
            timer.invalidate()
            self.timer = nil
        }
    }
}

//MARK:- Advertising
@available(iOS 11.0, *)
extension RoomAPI {
    func getRoomAds(roomId: String, adsType: String, completion: @escaping (SPRoomAds?)->Void) {
        let stringUrl = String(format: SPApiUrl.roomAds, hostIpRoomAPI, deviceCode, roomId, adsType)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:AnyObject] {
                completion(SPRoomAds(json: json))
            } else {
                completion(nil)
            }
        }
    }
    func getMediaAds(roomId: String, adsType: String, mediaId: String, completion: @escaping ([String:Any]?)->Void) {
        let api = "%@api/DeviceAds/GetAds_New?deviceCode=%@&roomID=%@&AdsType=%@&MediaID=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, adsType, mediaId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [String:Any] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
}

//MARK:- Media History
extension RoomAPI {
    /*
     api/DeviceMedia/MediaHistory_new?deviceCode={deviceCode}&HisType={HisType}&startDate={startDate}&endDate={endDate}&loadIndex={loadIndex}
     parameter:
     hostIpRoomAPI
     deviceCode
     HisType
     startDate: format yyyy-MM-dd
     endDate: format yyyy-MM-dd
     loadIndex
     */
    func getMediaHistory(_ type: Int, from date1: String, to date2: String, loadIndex: Int, completion: @escaping ([MediaHistory])->Void) {
        let api = "%@api/DeviceMedia/MediaHistory_new?deviceCode=%@&HisType=%i&startDate=%@&endDate=%@&loadIndex=%i"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, type, date1, date2, loadIndex)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            var medias = [MediaHistory]()
            if let jsonArray = json as? [[String:Any]] {
                for js in jsonArray {
                    medias.append(MediaHistory(json: js))
                }
                completion(medias)
                return
            }
        }
        completion([])
    }
    
    func bookBuy(memberId: String, roomId: String, saleId: String, password: String, completion:@escaping (Int)->Void) {
        let api = "%@api/DeviceMedia/ProductBuy?memberID=%@&DeviceCode=%@&saleID=%@&roomID=%@&Password=%@"
        
        let stringUrl = String(format: api, hostIpRoomAPI, memberId, deviceCode, saleId, roomId, password)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            DispatchQueue.main.async {
                if let json = json as? [String:Any],
                    let code = json["MessageCode"] as? String {
                    completion(code.toInt())
                } else {
                    completion(204)
                }
            }
        }
    }
    
    func moneyRefill(memberId: String, completion:@escaping (String)->Void)  {
        let api = "%@api/DeviceMedia/UrlPayment?deviceCode=%@&memberId=%@"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, memberId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String,
                let link = json["MessageText"] as? String {
                if code == "200" {
                    completion(link)
                    return
                }
            }
            completion("")
        }
    }
    
    @objc func mediaDownload(mediaId: String, roomId: String, completion:@escaping (SPMediaDownload?)->Void) {
        let api = "%@api/DeviceMedia/MediaDownload_new?deviceCode=%@&MediaId=%@&RoomId=%@&SubjectId=%@"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, mediaId, roomId, "")
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any] {
                let data = SPMediaDownload(json: json)
                completion(data)
                return
            }
            completion(nil)
        }
    }
    
    func mediaFollow(mediaId: String, follow: Bool, completion:@escaping (Bool)->Void) {
        let api = "%@api/DeviceMedia/MediaFollow?deviceCode=%@&mediaId=%@&followType=%i"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, mediaId, follow ? 1:0)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String {
                completion(code == "200")
                return
            }
            completion(false)
        }
    }
    
    func searchMediaAuthor(with media: SPMedia, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaSearchAuthor?deviceCode=%@&roomId=%@&mediaKind=%@&findValue=%@&sort=%i&loadIndex=%i"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, media.roomId, "", media.author, 0, 1)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func searchMediaCategory(with media: SPMedia, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&sort=%i&loadIndex=%i&version=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, media.roomId, "", media.categoriesId, media.subCategoriesId, 0, 1, CurrentAppVersion)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func searchMediaPublisher(with media: SPMedia, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaSearchPublisher?deviceCode=%@&roomId=%@&mediaKind=%@&publisherId=%@&sort=%i&loadIndex=%i"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, media.roomId, "", media.publisherId, 0, 1)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func searchMedias(with media: SPMedia, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&sort=%i&loadIndex=%i&version=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, media.roomId, "", "", "", 0, 1, CurrentAppVersion)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func searchMediaTag(with media: SPMedia, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaSearchAuthor?deviceCode=%@&roomId=%@&mediaKind=%@&findValue=%@&sort=%i&loadIndex=%i"
        
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, media.roomId, "", media.publisherId, 0, 1)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func bookBorrow(mediaId: String, roomId: String, cId: String, completion:@escaping (Int, String)->()) {
        let api = "%@api/DeviceMedia/MediaDownload_new?deviceCode=%@&MediaId=%@&RoomId=%@&CopyID=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, mediaId, roomId, cId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["METERIAGET"] as? String {
                let expDate = (json["RENTEXPIRE"] as? String) ?? "-"
                completion(code.toInt(), expDate)
                return
            }
            completion(0,"")
        }
    }
    func returnMedia(media: OfflineShelfData, completion:@escaping (Bool)->()) {
        let api = "%@api/DeviceMedia/MediaReturn?deviceCode=%@&MediaId=%@&RoomId=%@&MemberId=%@"
        
        let mediaId = media.mediaId ?? ""
        let memberId = (media.memberId ?? "" == deviceCode) ? "" : media.memberId ?? ""
        let roomId = media.libraryId ?? ""
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, mediaId, roomId, memberId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any],
                let code = json["MessageCode"] as? String {
                completion(code == "200")
                return
            }
            completion(false)
        }
    }
    //MARK:- Sync Medias
    func syncMemberMedia(memberId: String, completion:@escaping (Bool)->()) {
        let api = "%@api/DeviceMedia/MediasMemberShelf?deviceCode=%@&memberId=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, memberId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]], json.count > 0 {
                let memberShelf = MemberShelf(json: json)
                DB.syncMemberShelf(data: memberShelf, memberId: memberId, completion: {
                    completion(true)
                })
            } else {
                completion(false)
            }
        }
        
    }
    func syncPresetMedia(memberId: String, completion:@escaping (PresetShelf?)->()) {
        let api = "%@api/DeviceMedia/MediasPresetShelf?deviceCode=%@&memberId=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, memberId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            if let json = json as? [[String:Any]], json.count > 0 {
                completion(PresetShelf(json: json[0]))
            } else {
                completion(nil)
            }
        }
        
    }
    
    func shareMedia(memberId: String, mediaId: String, roomId: String, completion:@escaping (String)->()) {
        let api = "%@api/DeviceMedia/MediaGenQ?F=media&U=%@&M=%@&L=%@"
        let stringUrl = String(format: api, hostIpRoomAPI,memberId,mediaId,roomId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "STRING", method: "GET") { (json) in
            if let json = json as? String {
                print(json.debugDescription)
                completion(json)
            }
        }
    }
    
    
    func libraryInfo(roomId: String, memberId: String, completion: @escaping ([String:Any]?)->()) {
        let api = "%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@&memberID=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, memberId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    func libraryMediaItems(roomId: String, completion: @escaping ([[String:Any]]?)->()) {
        let api = "%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&sort=%i&loadIndex=%i&version=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, "", "", "", 0, 1, CurrentAppVersion)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion(nil)
            }
        }
    }
    
    func mediaItemsByTag(roomId: String, mediaKind: String, tag: String, sort: Int, loadIndex: Int, completion: @escaping ([[String:Any]])->()) {
        let api = "%@api/DeviceMedia/MediaSearchTag?deviceCode=%@&roomId=%@&mediaKind=%@&findValue=%@&sort=%i&loadIndex=%i"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, mediaKind, tag, sort, loadIndex)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [[String:Any]] {
                completion(json)
            } else {
                completion([])
            }
        }

    }
}
//MARK:- Course
extension RoomAPI {
    func courseLibralies(index: Int, completion:@escaping ([SPCourseLibrary])->()) {
        let api = "%@api/DeviceMedia/Libraries_new?deviceCode=%@&RoomKind=%@&loadIndex=%i"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, "C", index)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let jsons = json as? [[String:Any]] {
                var items = [SPCourseLibrary]()
                jsons.forEach({ (js) in
                    items.append(SPCourseLibrary(json: js))
                })
                completion(items)
            } else {
                completion([])
            }
        }
    }
    
    func libraryCourses(roomId: String, completion: @escaping ([[String:Any]])->()) {
        let api = "%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=&catId=&subId=&sort=%i&loadIndex=%i&version=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, 0, 1, CurrentAppVersion)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let jsons = json as? [[String:Any]] {
                completion(jsons)
            } else {
                completion([])
            }
        }
    }
    
    func redeemCode(code: String, completion: @escaping (String, String, String)->()) {
        let api = "%@api/DeviceMedia/subjectRedreem?RedreemCode=%@&deviceCode=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, code, deviceCode)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any] {
                if let messageCode = json["MessageCode"] as? String, let messageText = json["MessageText"] as? String {
                    completion(messageCode, messageText, "")
                }
            } else {
                completion("", "", "")
            }
        }
    }
    
    func activateCourseStartDate(mediaId: String, completion: @escaping (String, String)->()) {
        let api = "%@api/DeviceMedia/ActiveSubject?deviceCode=%@&MediaID=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, mediaId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON", method: "GET") { (json) in
            if let json = json as? [String:Any] {
                if let messageCode = json["MessageCode"] as? String, let messageText = json["MessageText"] as? String {
                    completion(messageCode, messageText)
                }
            } else {
                completion("", "")
            }
        }
    }
}



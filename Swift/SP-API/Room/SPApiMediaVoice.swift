//
//  SPApiMediaVoice.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit

enum ApiMediaVoiceList {
    static let listId = "PLAYLISTID"
    static let listName = "PLAYLISTNAME"
    static let mediaVoices = "DMediavoice"
    static let message = "MessaageText"
}
struct SPMediaVoiceList {
    let listId: String
    let listName: String
    let mediaVoices: [SPMediaVoice]
    let message: String
    init(json:[String:Any]) {
        listId = (json[ApiMediaVoiceList.listId] as? String) ?? ""
        listName = (json[ApiMediaVoiceList.listName] as? String) ?? ""
        message = (json[ApiMediaVoiceList.message] as? String) ?? ""
        if let js = json[ApiMediaVoiceList.mediaVoices] as? [[String:Any]] {
            var items:[SPMediaVoice] = []
            for j in js {
                items.append(SPMediaVoice(json: j))
            }
            let sortedItems = items.sorted(by: { (m1, m2) -> Bool in
                if m1.chapter == m2.chapter {
                    return m1.series < m2.series
                }
                return m1.chapter < m2.chapter
            })
            mediaVoices = sortedItems
        } else {
            mediaVoices = []
        }
    }
    func totalDuration() -> TimeInterval {
        var seconds: Double = 0
        for mv in mediaVoices {
            seconds += mv.duration.toDouble()*60
        }
        return seconds
    }
    func totalPages() -> Int {
        var pages: Int = 0
        for mv in mediaVoices {
            pages += ((mv.toPage - mv.fromPage) + 1)
        }
        return pages
    }
    
    static func > (lhs: SPMediaVoiceList, rhs: SPMediaVoiceList) -> Bool {
        if lhs.mediaVoices.count == rhs.mediaVoices.count {
            return lhs.listName < rhs.listName
        } else {
            return lhs.mediaVoices.count > rhs.mediaVoices.count
        }
    }
}

enum ApiMediaVoice {
    static let voiceId = "VOICEID"
    static let voiceName = "VOICENAME"
    static let voiceUrl = "VOICEURL"
    static let timeUrl = "TIMEEURL"
    static let duration = "DURATION"
    static let page = "PAGETO"
    static let reader = "READER"
    static let series = "VOICESERIES"
    static let chapter = "VOICECHAPTER"
}

struct SPMediaVoice {
    let voiceId: String
    let voiceName: String
    let voiceUrl: String
    let timeUrl: String
    let duration: String
    let page: String
    let reader: String
    let series: String
    let chapter: String
    let fromPage: Int
    let toPage: Int
    let timePages: [SPMediaVoicePageInfo]
    init(json:[String:Any]) {
        voiceId = (json[ApiMediaVoice.voiceId] as? String) ?? ""
        voiceName = (json[ApiMediaVoice.voiceName] as? String) ?? ""
        voiceUrl = (json[ApiMediaVoice.voiceUrl] as? String) ?? ""
        timeUrl = (json[ApiMediaVoice.timeUrl] as? String) ?? ""
        duration = (json[ApiMediaVoice.duration] as? String) ?? ""
        page = (json[ApiMediaVoice.page] as? String) ?? ""
        reader = (json[ApiMediaVoice.reader] as? String) ?? ""
        series = (json[ApiMediaVoice.series] as? String) ?? ""
        chapter = (json[ApiMediaVoice.chapter] as? String) ?? ""
        let pages = page.components(separatedBy: ["-"])
        if pages.count > 1 {
            fromPage = pages[0].toInt()
            toPage = pages[1].toInt()
        } else {
            fromPage = 0
            toPage = 0
        }
        //pages info
        if let url = URL(string: timeUrl) {
            do {
                let csv = try String(contentsOf: url)
                var rows = csv.components(separatedBy: "\r\n").map({$0.components(separatedBy: ",")})
                if rows.count > 0 {
                    rows.removeFirst()
                }
                let x = Dictionary(grouping: rows.map{SPMediaVoicePageInfo(info: $0)}, by: {$0.page})
                let y = Array(x.values)
                var pages = y.map{$0.min(by: {$0.seconds < $1.seconds})!}
                pages.sort(by: {$0.page < $1.page})
                //calculate pageLength
                for i in 0..<pages.count-1 {
                    var tp1 = pages[i]
                    let tp2 = pages[i+1]
                    tp1.pageLength = tp2.seconds - tp1.seconds
                    pages[i] = tp1
                }
                timePages = pages
            } catch {
                timePages = []
            }
        } else {
            timePages = []
        }
    }
    
    func checkPageDuration(at page: Int) -> Double {
        if page >= fromPage && page <= toPage {
            if let x = timePages.filter({$0.page == page}).first {
                return x.pageLength
            }
        }
        return 0
    }
}

struct SPMediaVoicePageInfo {
    let page: Int
    let seconds: Double
    var pageLength: Double = 0
    init(info:[String]) {
        guard info.count >= 3 else {
            page = 0
            seconds = 0
            return
        }
        page = info[1].toInt()
        let strTime = info[0]
        let times = strTime.components(separatedBy: ":")
        if times.count == 3 {
            seconds = 3600*times[0].toDouble() + 60*times[1].toDouble() + times[2].toDouble()
        } else {
            seconds = 0
        }
    }
    
    static func ==(lhs: SPMediaVoicePageInfo, rhs: SPMediaVoicePageInfo) -> Bool {
        return (lhs.page == rhs.page) && (lhs.seconds.isEqual(to: rhs.seconds)) && (lhs.pageLength.isEqual(to: rhs.pageLength))
    }
}

@available(iOS 11.0, *)
struct SPSelectedMediaVoices {
    let source: Int
    let startPage: PDFPage?
    let endPage: PDFPage?
    let voices: [SPMediaVoice]
    let language: String
    
}

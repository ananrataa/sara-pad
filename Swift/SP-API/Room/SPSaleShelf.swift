//
//  SPSaleShelf.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 6/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiSaleShelf {
    static let roomId = "ROOMID"
    static let mediaId = "MEDIANUM"
    static let roomImage = "ROOMIMG"
    static let roomCode = "ROOMCODE"
    static let roomName = "ROOMNAME"
    static let items = "MediaItems"
    static let messageText = "MessaageText"
}

class SPSaleShelf: NSObject {
    @objc var roomShelves: [SPRoomShelf]
    @objc init(json:[[String:Any]]) {
        var items = [SPRoomShelf]()
        for js in json {
            items.append(SPRoomShelf(json: js))
        }
        roomShelves = items
    }
}
class SPRoomShelf: NSObject {
    @objc let roomId: String
    @objc let mediaId: String
    @objc let roomImage: String
    @objc let roomCode: String
    @objc let roomName: String
    @objc let items: [SPMedia]
    @objc let messageText: String
    @objc init(json:[String:Any]) {
        roomId = (json[ApiSaleShelf.roomId] as? String) ?? ""
        mediaId = (json[ApiSaleShelf.mediaId] as? String) ?? ""
        roomImage = (json[ApiSaleShelf.roomImage] as? String) ?? ""
        roomCode = (json[ApiSaleShelf.roomCode] as? String) ?? ""
        roomName = (json[ApiSaleShelf.roomName] as? String) ?? ""
        messageText = (json[ApiSaleShelf.messageText] as? String) ?? ""
        var medias = [SPMedia]()
        if let js = json[ApiSaleShelf.items] as? [[String:Any]] {
            for jsData in js {
                medias.append(SPMedia(json: jsData))
            }
        }
        items = medias
    }
}


//
//  SPApiDeviceRoomDetail.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/29/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum ApiRoom {
    static let roomId = "ROOMID"
    static let roomCode = "ROOMCODE"
    static let roomName = "ROOMNAME"
    static let symbol = "SYMBOL"
    static let description = "DESP"
    static let telephone1 = "TEL1"
    static let telephone2 = "TEL2"
    static let telephone3 = "TEL3"
    static let email = "EMAIL"
    static let tag = "TAG"
    static let memberStatus = "MemSTT"
    static let theme = "THEME"
    static let message = "MessaageText"
}


@available(iOS 11.0, *)
struct SPRoomDetail {
    let roomId:String
    let roomCode:String
    let roomName:String
    let symbol:String
    let description:String
    let telephone1:String
    let telephone2:String
    let telephone3:String
    let email:String
    let tag:String
    let memberStatus: Int
    let theme:SPTheme
    let message:String
    
    init(json:[String:Any]) {
        self.roomId = (json[ApiRoom.roomId] as? String) ?? ""
        self.roomCode = (json[ApiRoom.roomCode] as? String) ?? ""
        self.roomName = (json[ApiRoom.roomName] as? String) ?? ""
        self.symbol = (json[ApiRoom.symbol] as? String) ?? ""
        self.description = (json[ApiRoom.description] as? String) ?? ""
        self.telephone1 = (json[ApiRoom.telephone1] as? String) ?? ""
        self.telephone2 = (json[ApiRoom.telephone2] as? String) ?? ""
        self.telephone3 = (json[ApiRoom.telephone3] as? String) ?? ""
        self.email = (json[ApiRoom.email] as? String) ?? ""
        self.tag = (json[ApiRoom.tag] as? String) ?? ""
        self.memberStatus = (json[ApiRoom.memberStatus] as? Int) ?? 1
        self.theme = SPTheme(json: json)
        self.message = (json[ApiRoom.message] as? String) ?? ""
        
    }
}

//
//  SwiftUtility.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/13/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

class SPUtility: NSObject {
    private override init() {}
    static let shared = SPUtility.init()
    
    func isConnectedToNetwork() -> Bool {
        guard let reachability = Reachability(hostName: "google.com") else {return false}
        
        let remoteHostStatus = reachability.currentReachabilityStatus()
        
        switch remoteHostStatus {
        case NetworkStatus(0): return false //NotReachable
        case NetworkStatus(1): return true  //ReachableViaWWAN
        case NetworkStatus(2): return true  //ReachableViaWiFi
        default: return false
        }
        
    }
    
    func localizeString(forKey key: String, value: String?, table: String?) -> String {
        guard let langs = UserDefaults.standard.value(forKey: "AppleLanguages") as? [String],
            langs.count > 0,
            let path = Bundle.main.path(forResource: langs[0], ofType: "lproj"),
            let languageBundle = Bundle(path: path)
            else {return ""}
        
            return languageBundle.localizedString(forKey: key, value: value, table: table)
    }
    
    func userLimitDays() -> (borrow: Int, download:Int) {
        if DBManager.isMember() {
            return (borrow: Int(MaximunDateForBorrow), download: Int(MaximunFilesDownload))
        } else {
            return (borrow: Int(MaximunDateForBorrow), download: Int(MaximunFilesDownload))
        }
    }
}

class Downloader {
    class func load(URL: URL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: URL)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            if (error == nil) {
                // Success
                if let httpUrlResponse = response as? HTTPURLResponse {
                    print("Success: \(httpUrlResponse.statusCode)")
                    
                }
            }
            else if let error = error {
                // Failure
                print("Failure: %@", error.localizedDescription);
            }
        }
        
        task.resume()
    }
}

//
//  TestApiViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/12/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

@available(iOS 11.0, *)
class TestApiViewController: UIViewController {
    lazy var playerQueue : AVQueuePlayer = {
        let player = AVQueuePlayer()
        return player
    }()
    var currentTrackIndex: Int = 0
    var isPlaying: Bool = false
    
    var seletedTrackUrls: [URL] = []
    var mediaId: String
    var roomId: String
    
    let bgmView:UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    @objc init(mediaId: String, roomId: String) {
        self.mediaId = mediaId
        self.roomId = roomId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
//        let userLimitDays = SPUtility.shared.userLimitDays()
        
//        RoomAPI.shared.browsMediaDetail(deviceCode: deviceCode, roomId: roomId, mediaId: mediaId, limit: userLimitDays.download, isPreset: false) { (json) in
//            let media = SPMedia.init(json: json)
//            print("\(media)")
//        }
        
//        RoomAPI.shared.mediaSuggested(deviceCode: deviceCode, roomId: roomId, mediaId: mediaId, mediaKind: "") { (json) in
//            if let json = json as? [[String:Any]] {
//                let suggestMedias = SPSuggestMedias(json: json)
//
//                suggestMedias.medias.forEach{print("\($0.mediaName)\n")}
//            }
//        }
        
        //BGM
        
        RoomAPI.shared.roomBGM(deviceCode: deviceCode, roomId: roomId) {[unowned self] (json) in
            if let json = json as? [[String:Any]] {
                let roomBGM = SPBGM(json: json)
                for pl in roomBGM.playLists {
                    print("\(pl.bgmName): \(pl.tracks.count) track (\(pl.duration))")
                    for track in pl.tracks {
                        if let url = URL(string: track.trackUrl) {
                            let playerItem = AVPlayerItem(url: url)
                            self.playerQueue.insert(playerItem, after: nil)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            self.seletedTrackUrls.append(url)
                        }
                    }
                    if self.playerQueue.items().count == 1 {
                        self.playerQueue.actionAtItemEnd = .none
                    }
                    print("Track count: \(self.playerQueue.items().count)")
                    print("Duration: \(pl.duration)")
                    self.playerQueue.play()
                    break
                }
            }
        }
        
        view.addSubview(bgmView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: bgmView)
        view.addConstraintsWithFormat(format: "V:|[v0(46)]", views: bgmView)
        
        let btnPlay = UIButton(title: "Play", target: self, action: #selector(play))
        let btnPause = UIButton(title: "Pause", target: self, action: #selector(pause))
        let btnStop = UIButton(title: "Stop", target: self, action: #selector(stop))
        let sv = UIStackView(arrangedSubviews: [btnPlay, btnPause, btnStop])
        sv.axis = .horizontal
        sv.spacing = 12
        sv.distribution = .equalCentering
        bgmView.addSubview(sv)
        bgmView.addConstraintsWithFormat(format: "H:|[v0]|", views: sv)
        bgmView.addConstraintsWithFormat(format: "V:|[v0]|", views: sv)
        
    }
    
    @objc func play() {
        playerQueue.play()
    }
    @objc func pause() {
        playerQueue.pause()
    }
    @objc func stop() {
        playerQueue.pause()
        playerQueue.removeAllItems()
    }
    @objc func playerItemDidReachEnd(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        if seletedTrackUrls.count > 1 {
            playerQueue.advanceToNextItem()
        }
        if playerQueue.items().count == 1 {
            for url in seletedTrackUrls {
                let playerItem = AVPlayerItem(url: url)
                playerQueue.insert(playerItem, after: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
            }
            playerQueue.advanceToNextItem()
        }
    }
    
}

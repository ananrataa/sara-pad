//
//  SetttingViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 6/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit


@available(iOS 11.0, *)
class SettingViewController: UIViewController {
    let isEng:Bool = Locale.preferredLanguages[0] == "en"
    let cellId = "setting_cell"
    let items:[String] = {
        if MemberInfo.isSignin() {
            return ["language", "password_setting", "libraries"]
        } else {
            return ["language", "register"]
        }
    }()
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barStyle = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        
        collectionView.register(SettingCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        TransparentTheme.shared.applyNavigationBar(vc: self)
        title = String.localizedString(key: "settings")
    }
    deinit {
        print("XXXXXXXXXXXXXX")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    @objc func done() {
        navigationController?.navigationBar.barStyle = .default
        dismiss(animated: true, completion: nil)
    }
}


@available(iOS 11.0, *)
extension SettingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = min(120, (collectionView.frame.width - 24)/3)
        return .init(width: w, height: 1.1*w)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingCell
        
        let curLanguage = isEng ? "🇺🇸\nEng" : "🇹🇭\nไทย"
        cell.cellInfo = (indexPath.item, items[indexPath.item], indexPath.item == 0 ? curLanguage : "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if MemberInfo.isSignin() {
            switch indexPath.item {
            case 0: languageSetting()
            case 1: changePassword()
            case 2: mergeAccount()
            default: break
            }
        } else {
            switch indexPath.item {
            case 0: languageSetting()
            case 1: register()
            default: break
            }
        }
    }
    
    func languageSetting() {
        let alert = UIAlertController(title: String.localizedString(key: "language"),
                                      message: String.localizedString(key: "SelectLanguage"),
                                      preferredStyle: .alert)
        let thaiTitle = "🇹🇭 ภาษาไทย" + (isEng ? "" : String.localizedString(key: "CurrUsing"))
        let engTitle = "🇺🇸 English" + (isEng ? String.localizedString(key: "CurrUsing") : "")
        
        alert.addAction(UIAlertAction(title: thaiTitle, style: .default, handler: { (_) in
            self.setCurrentLanguage(newLanaguage: "th")
        }))
        alert.addAction(UIAlertAction(title: engTitle, style: .default, handler: { (_) in
            self.setCurrentLanguage(newLanaguage: "en")
        }))
        alert.addAction(UIAlertAction(title: String.localizedString(key: "Cancel"), style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    func changePassword() {
        title = ""
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func setCurrentLanguage(newLanaguage: String) {
        if let data = UserDefaults.standard.object(forKey: "AppleLanguages") as? NSArray, let curLanguage = data.firstObject as? String {
            if curLanguage.caseInsensitiveCompare(newLanaguage) != .orderedSame {
                UserDefaults.standard.setValue([newLanaguage], forKey: "AppleLanguages")
                UserDefaults.standard.setValue(newLanaguage.uppercased(), forKey: AppLanguageKey)
                UserDefaults.standard.synchronize()
                if let appDel = UIApplication.shared.delegate as? AppDelegate {
                    appDel.relaunchApplication()
                }
            }
        }
    }
    func register() {
        title = ""
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: "RegisterMemberVC") as? RegisterMemberVC {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func mergeAccount() {
        title = ""
        if let memberId = DBManager.selectMemberID() {
            let vc = MergeAccountViewController(id: memberId)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            SVProgressHUD.showError(withStatus: String.localizedString(key: "merge_error"))
        }
    }
}

@available(iOS 11.0, *)
class SettingCell: BaseCell {
    let images:[UIImage] = {
        if MemberInfo.isSignin() {
            return [#imageLiteral(resourceName: "cell_setting"), #imageLiteral(resourceName: "cell_password"), #imageLiteral(resourceName: "cell_library")]
        } else {
            return [#imageLiteral(resourceName: "cell_setting"), #imageLiteral(resourceName: "cell_register")]
        }
    }()
    let cardView: UIView = {
        let v = UIView()
        v.layer.borderWidth = 1
        v.layer.cornerRadius = 7
        return v
    }()
    let cellImage: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleAspectFit
        imgV.tintColor = .white
        return imgV
    }()
    let cellTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white.withAlphaComponent(0.8)
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: sukhumvitLightFont, size: 19)
        lbl.numberOfLines = 0
        lbl.minimumScaleFactor = 0.75
        
        return lbl
    }()
    let cellValue: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white.withAlphaComponent(0.8)
        lbl.textAlignment = .right
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: mainFont, size: 18)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 0
        lbl.minimumScaleFactor = 0.75
        return lbl
    }()
    var cellInfo: (item: Int, title:String, value: String)? {
        didSet {
            refreshView()
        }
    }
    override func setup() {
        let w = contentView.frame.width/2 - 8
        cellImage.frame = .init(x: 4, y: 4, width: w, height: w)
        cardView.addSubview(cellImage)
        
        cardView.addSubview(cellTitle)
        cellTitle.anchor(top: nil, leading: cardView.leadingAnchor, bottom: cardView.bottomAnchor, trailing: cardView.trailingAnchor, padding: .init(top: 0, left: 4, bottom: 4, right: 0))
        
        cardView.addSubview(cellValue)
        cellValue.anchor(top: cardView.topAnchor, leading: nil, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 4, left: 0, bottom: 0, right: 4))

        addSubview(cardView)
        cardView.pin(to: self.contentView)
    }
    
    func refreshView() {
        if let info = cellInfo {
            let color = UIColor(named: "Color-\(info.item)") ?? UIColor.white
            cardView.backgroundColor = color.withAlphaComponent(0.1)
            cardView.layer.borderColor = color.cgColor
            cellImage.image = images[info.item]
            
            cellValue.text = info.value
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1
            paragraphStyle.lineHeightMultiple = 0.85
            paragraphStyle.alignment = .left
            
            let attrString = NSMutableAttributedString()
            attrString.append( NSMutableAttributedString(string: String.localizedString(key: info.title)))
            attrString.addAttribute(NSAttributedString.Key.font, value: cellTitle.font, range: NSMakeRange(0, attrString.length))
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
            cellTitle.attributedText = attrString
        }
    }
}

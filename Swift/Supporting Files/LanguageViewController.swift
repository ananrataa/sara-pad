//
//  LanguageViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 20/8/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    let cellId = "language_cell"
    var languageKeys:[String] = []
    var languageDict: Dictionary<String,String>!
    var curLanguageKey: String
    var selectionCallback: ((String)->())?
    init(curKey: String) {
        self.curLanguageKey = curKey
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.addBorder(width: 1, color: .lightGray, radius: 7)
        if let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"),
            let lngDict = NSDictionary(contentsOfFile: path) as? Dictionary<String,String> {
            languageDict = lngDict
            languageKeys = Array(lngDict.keys).sorted{lngDict[$0] ?? "" < lngDict[$1] ?? ""}
            if let index = languageKeys.firstIndex(of: "auto") {
                languageKeys.remove(at: index)
                languageKeys.insert("auto", at: 0)
            }
            tableView.reloadData()
            if let row = languageKeys.firstIndex(of: curLanguageKey) {
                tableView.scrollToRow(at: IndexPath(row: row, section: 0), at: .middle, animated: true)
            }
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageKeys.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.tintColor = UIColor.red
        let key = languageKeys[indexPath.row]
        let flag = String.localizedString(key: key.trimmingCharacters(in: .whitespaces))
        let language = languageDict[key] ?? ""
        cell.textLabel?.text = "\(flag) \(language)"
        if curLanguageKey == key {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let callback = selectionCallback {
            callback(languageKeys[indexPath.row])
        }
        dismiss(animated: true, completion: nil)
    }

}

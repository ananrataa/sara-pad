//
//  ARProgressHUD.swift
//  Animation
//
//  Created by Anan Ratanasethakul on 5/11/2561 BE.
//  Copyright © 2561 ananratana. All rights reserved.
//

import UIKit
import Lottie

let pi: CGFloat = CGFloat(Double.pi)
class ARProgressHUD: UIView {
    static let shared = ARProgressHUD(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
    lazy var topController: UIViewController? = {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }()
    let animationView: LOTAnimationView = {
        let lotView = LOTAnimationView(name: "material_loading")
        lotView.contentMode = .scaleAspectFit
        lotView.backgroundColor = .clear
        lotView.clipsToBounds = true
        return lotView
    }()
    let labelTitle: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: sukhumvitLightFont, size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()
    var foregroundColor: UIColor = .lightGray
    var textColor: UIColor = UIColor.flatSilverColor
    var bgColor: UIColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var accent: UIColor = UIColor.flatRed3
    var lineThick: CGFloat = 1
    var centerPoint: CGPoint = .zero
    var radius: CGFloat = 0
    var duration: TimeInterval = 2
    var progress: CGFloat = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        guard let p = self.superview?.convert(self.center, to: self) else {return}
//        centerPoint = p
//        let offset: CGFloat = 4
//        radius = self.frame.width/2 - offset
//        let path = UIBezierPath(ovalIn: rect.insetBy(dx: offset, dy: offset))
//        path.lineWidth = lineThick*5
//        foregroundColor.setStroke()
//        path.stroke()
//
//        if let sl = self.layer.sublayers?.last, sl.name == "" {
//            sl.removeFromSuperlayer()
//        }
//        var position:CGFloat = 1.5*pi + 2*pi*progress/100
//        if position >= 2*pi {
//            position -= 2*pi
//        }
//        let shapeLayer = createShape(rad1: 1.5*pi, rad2: position, lineWidth: lineThick, color: accent)
//        shapeLayer.name = ""
//        self.layer.addSublayer(shapeLayer)
//    }
    
    private func createShape(rad1: CGFloat, rad2: CGFloat, lineWidth: CGFloat, color: UIColor) -> CAShapeLayer {
        let path = UIBezierPath(arcCenter: centerPoint, radius: radius, startAngle: rad1, endAngle: rad2, clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = color.cgColor
        //you can change the line width
        shapeLayer.lineWidth = lineWidth*5
        
        return shapeLayer
    }
    private func addLabel(text: String) {
        labelTitle.text = text
        labelTitle.textColor = textColor
        self.addSubview(labelTitle)
        self.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
        self.addConstraintsWithFormat(format: "V:[v0]-12-|", views: labelTitle)
    }
    private func showAnimationView(status: String) {
        if let vc = topController {
            vc.view.isUserInteractionEnabled = false
            animationView.setAnimation(named: "material_loading")
            animationView.loopAnimation = true
            self.center = vc.view.center
            self.layer.cornerRadius = 13
            self.backgroundColor = bgColor
            self.clipsToBounds = true
            //Add label
            addLabel(text: status)
            
            self.addSubview(animationView)
            self.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: animationView)
            self.addConstraintsWithFormat(format: "V:|-25-[v0]-25-|", views: animationView)
            vc.view.addSubview(self)
            animationView.play()
        }
    }
    private func showSuccess(text: String) {
        setNeedsDisplay()
        self.layer.cornerRadius = 7
        self.backgroundColor = bgColor
        self.clipsToBounds = true
        if let vc = topController {
            animationView.setAnimation(named: "green_circle")
            self.center = vc.view.center
            labelTitle.text = text
            labelTitle.textColor = textColor
            self.addSubview(labelTitle)
            self.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
            self.addConstraintsWithFormat(format: "V:[v0]-2-|", views: labelTitle)
            
            self.addSubview(animationView)
            self.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: animationView)
            self.addConstraintsWithFormat(format: "V:|-25-[v0]-25-|", views: animationView)
            vc.view.addSubview(self)
            animationView.play { (done) in
                if done {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.remove()
                    }
                }
            }
            
        }
    }
    private func showError(text: String) {
        setNeedsDisplay()
        self.layer.cornerRadius = 7
        self.backgroundColor = bgColor
        self.clipsToBounds = true
        if let vc = topController {
            vc.view.isUserInteractionEnabled = false
            animationView.setAnimation(named: "error_cross")
            self.center = vc.view.center
            labelTitle.text = text
            labelTitle.textColor = textColor
            self.addSubview(labelTitle)
            self.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
            self.addConstraintsWithFormat(format: "V:[v0]-2-|", views: labelTitle)
            
            self.addSubview(animationView)
            self.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: animationView)
            self.addConstraintsWithFormat(format: "V:|[v0(100)]", views: animationView)
            vc.view.addSubview(self)
            animationView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                self.remove()
                vc.view.isUserInteractionEnabled = true
            }
        }
    }
    func show(with name: LottieName, info: String, loop: Bool = false, completion:((Bool) ->())? = nil) {
        setNeedsDisplay()
        self.layer.cornerRadius = 7
        self.backgroundColor = bgColor
        self.clipsToBounds = true
        if let vc = topController {
            vc.view.isUserInteractionEnabled = false
            if let completion = completion {
                animationView.completionBlock = completion
            }
            animationView.setAnimation(named: name.rawValue)
            self.center = vc.view.center
            labelTitle.text = info
            labelTitle.textColor = textColor
            self.addSubview(labelTitle)
            self.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
            self.addConstraintsWithFormat(format: "V:[v0]-12-|", views: labelTitle)
            
            self.addSubview(animationView)
            self.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: animationView)
            self.addConstraintsWithFormat(format: "V:|-12-[v0(100)]", views: animationView)
            vc.view.addSubview(self)
            self.animationView.loopAnimation = loop
            
            DispatchQueue.main.async {
                self.animationView.play()
            }
            
            if !loop {
                DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                    self.remove()
                    vc.view.isUserInteractionEnabled = true
                }
            }
        }
    }
    private func remove() {
        DispatchQueue.main.async {
            if let vc = self.topController {
                vc.view.isUserInteractionEnabled = true
                for sv in vc.view.subviews {
                    if let v = sv as? ARProgressHUD {
                        v.removeFromSuperview()
                    }
                }
            }
        }
    }
    
}


extension ARProgressHUD {
    class func setAccent(_ color: UIColor) {
        shared.accent = color
    }
    class func setTextColor(_ color: UIColor) {
        shared.textColor = color
    }
    class func setForegroundColor(_ color: UIColor) {
        shared.foregroundColor = color
    }
    class func setBackgroundColor(_ color: UIColor) {
        shared.bgColor = color
    }
    class func setDarkTheme() {
        shared.accent = UIColor.white
        shared.bgColor = UIColor.black
        shared.foregroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.9)
        shared.textColor = .lightGray
    }
    class func setLightTheme() {
        shared.accent = UIColor(hexString: "#5E4534")
        shared.bgColor = UIColor(hexString: "#D5C295")
        shared.foregroundColor = UIColor(hexString: "#F0DEB4")
        shared.textColor = UIColor.flatPumkinColor
    }
    class func dismiss() {
        shared.remove()
    }
    class func startProgress(status: String) {
        shared.lineThick = 1
        shared.showAnimationView(status: status)
    }
    class func progress(_ value: Int) {
        if value > 100 {
            shared.animationView.stop()
            shared.labelTitle.text = "Completed"
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                shared.remove()
            }
        } else {
            shared.progress = CGFloat(value)
            shared.labelTitle.text = "\(value)%"
        }
    }
    class func show(success: String) {
        shared.lineThick = 0
        shared.showSuccess(text: success)
    }
    class func show(error: String) {
        shared.lineThick = 0
        shared.showError(text: error)
    }
    class func show(with name: LottieName, info: String, loop: Bool = false, completion:((Bool)->())? = nil) {
        shared.lineThick = 0
        shared.show(with: name, info: info, loop: loop, completion: completion)
    }
    class func show(with name: LottieName, info: String, completion:()->()) {
        shared.lineThick = 0
        shared.show(with: name, info: info, loop: false)
    }
}

enum LottieName: String {
    case books, checkmark, checkmark_animation, done, done_ticks, dotted_loader, error_cross, keys, material_loading_animation, material_loading, new_button, new_notification_bell, newAnimation, snap_loader_white, success, trail_loading, warning
    
}

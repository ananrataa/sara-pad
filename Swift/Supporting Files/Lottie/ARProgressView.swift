//
//  ARProgressView.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 22/1/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import Lottie

class ARProgressView: UIView {

    let animationView: LOTAnimationView = {
        let lotView = LOTAnimationView(name: "material_loading")
        lotView.contentMode = .scaleAspectFit
        lotView.backgroundColor = .clear
        lotView.clipsToBounds = true
        return lotView
    }()
    let labelTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: sukhumvitLightFont, size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .light)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()
    
    init(lottieName: LottieName, status: String, rect: CGRect) {
        super.init(frame: rect)
        self.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        self.layer.cornerRadius = 7
        self.clipsToBounds = true
        //Add label
        labelTitle.text = status
        labelTitle.textColor = .white
        self.addSubview(labelTitle)
        self.addConstraintsWithFormat(format: "H:|[v0]|", views: labelTitle)
        self.addConstraintsWithFormat(format: "V:[v0]-12-|", views: labelTitle)
        
        //AnnimationView
        animationView.setAnimation(named: lottieName.rawValue)
        self.addSubview(animationView)
        let w: CGFloat = 100
        let margin = (rect.width - w)/2
        self.addConstraintsWithFormat(format: "H:|-\(margin)-[v0(\(w))]-\(margin)-|", views: animationView)
        self.addConstraintsWithFormat(format: "V:|-12-[v0(\(w))]", views: animationView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

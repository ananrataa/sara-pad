//
//  Extension.swift
//  SaraPadReader
//
//  Created by mbox imac on 6/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import UIKit
import PDFKit
import AVFoundation

let DEFAULT_VOLUME = "default_volume"




extension Array {
    func chunks(_ chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map {
            Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
        }
    }
}


extension UIImageView {
    @objc func jitterX(duration: TimeInterval, n: Float) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = n
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
    @objc func springY(duration: TimeInterval, n: Float) {        
        let spring = CASpringAnimation(keyPath: "position.y")
        spring.duration = duration
        spring.autoreverses = true
        spring.damping = 1
        spring.repeatCount = n
        spring.fromValue = self.center.y
        spring.toValue = self.center.y + 20
        layer.add(spring, forKey: nil)
    }
}


extension PDFPage {
    var pageNo: Int {
        if let doc = self.document {
            return doc.index(for: self)
        }
        return 0
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return ((rate != 0) && (error == nil))
    }
}



import UIKit

public extension UIDevice {
    @objc static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}

enum ActionType {
    case ok
    case cancel
}
@available(iOS 11.0, *)
class SP: NSObject {
    @objc static let shared = SP()
    private override init() {}
    
    func showAlert(title: String, message: String, actions: [ActionType], response:@escaping (ActionType)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        actions.forEach { (type) in
            switch type {
            case .ok:
                alert.addAction(UIAlertAction(title: String.localizedString(key: "OK"), style: .default, handler: { (action: UIAlertAction!) in
                    response(.ok)
                }))
            case .cancel:
                alert.addAction(UIAlertAction(title: String.localizedString(key: "Cancel"), style: .cancel, handler: { (action: UIAlertAction!) in
                    response(.cancel)
                }))
            }
        }
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: mainFont, size: 18.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: sukhumvitLightFont, size: 15.0)!]
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        if let vc = topViewController() {
            vc.present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                alert.dismiss(animated: false, completion: nil)
            }
        }
    }

    func getMemberId() -> String {
        var memberId = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            memberId = DBManager.selectMemberID()
        }
        return memberId
    }
    func getDeviceId() -> String {
        return UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
    }
    @objc func topViewController() -> UIViewController? {
        var topController = UIApplication.shared.keyWindow?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController
    }
    func downloadMediaCover(curMedia: OfflineShelfData) -> UIImage? {
        let memberId = curMedia.memberId ?? ""
        let roomId = curMedia.libraryId ?? ""
        let mediaId = curMedia.mediaId ?? ""
        let coverUrl = curMedia.coverUrl ?? ""
        
        //create directory
        let documentPath = self.documentPath()
        let logsPath = documentPath.appendingPathComponent("\(sarapadFloderName)/\(roomId)")
        
        do {
            try FileManager.default.createDirectory(atPath: logsPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError{
            print("Unable to create directory",error)
        }
        
        let folderName = "\(sarapadFloderName)/\(roomId)"
        let fileName = "\(memberId)_\(mediaId)_thumb.png"
        let filePath = "\(folderName)/\(fileName)"
        let localUrl = documentPath.appendingPathComponent(filePath)
        
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            Utility.loadMediaCoverImage(fromURL: coverUrl, orHaveNSData: nil, withFileName: fileName, inDirectory: folderName)
        }
        return UIImage(contentsOfFile: localUrl.path)
    }
    func loadSVProgressHUD(status: String, completing:@escaping ()->()) {
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: status)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            completing()
        }

    }
    func documentPath() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    func getLocalMediaPath(roomId: String, fileUrl: String) -> String? {
        guard let mediaUrl = URL(string: fileUrl) else {return nil}
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = "\(sarapadFloderName)/\(roomId)/\(mediaUrl.lastPathComponent)"
        let docPath = documentsPath.appendingPathComponent(filePath)
        return docPath.path
    }
    func getMediaUrl(media: SPMedia) -> URL? {
        guard let stringUrl = media.mediaFile?.url,
            let mediaUrl = URL(string: stringUrl) else {return nil}
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = "\(sarapadFloderName)/\(media.roomId)/\(mediaUrl.lastPathComponent)"
        return documentsPath.appendingPathComponent(filePath)
    }
    func getMediaUrl(media: OfflineShelfData) -> URL? {
        guard let stringUrl = media.fileUrl,
            let mediaUrl = URL(string: stringUrl) else {return nil}
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = "\(sarapadFloderName)/\(media.libraryId ?? "")/\(mediaUrl.lastPathComponent)"
        return documentsPath.appendingPathComponent(filePath)
    }
    func getCoverUrl(memberId: String, media: SPMedia) -> URL? {
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = "\(memberId)_\(media.mediaId)_thumb.png"
        let filePath = "\(sarapadFloderName)/\(media.roomId)/\(fileName)"
        return documentsPath.appendingPathComponent(filePath)
    }
    func isFileExist(media: SPMedia, memberId: String) -> Bool {
        guard let docPath = getMediaUrl(media: media),
            let _ = DB.getMedia(id: media.mediaId, memberId: memberId, roomId: media.roomId)
            else {return false}
        let path = docPath.path
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: path)
    }
    func isFileExist(media: OfflineShelfData) -> Bool {
        guard let docPath = getMediaUrl(media: media), let mediaId = media.mediaId, let roomId = media.libraryId, let memberId = media.memberId,
            let _ = DB.getMedia(id: mediaId, memberId: memberId, roomId: roomId)
            else {return false}
        let path = docPath.path
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: path)
    }
    func setTextFieldRightButton(image: UIImage, controller: UIViewController, textField: UITextField, action: Selector) {
        let h = textField.frame.height
        textField.rightViewMode = .always
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: h, height: h))
        btn.addTarget(controller, action: action, for: .touchUpInside)
        btn.setImage(image, for: .normal)
        textField.rightView = btn
    }
    
    func setListView(contentView: UIView, items: [String], textField: UITextField) -> ListItemTableViewController {
        var rect = textField.frame
        let v = textField.superview
        rect = (v?.convert(rect, to: contentView))!
        
        let listView = ListItemTableViewController(items: items, viewRect: rect, font: textField.font!)
        contentView.addSubview((listView.tableView)!)
        listView.tableView.isHidden = true
        return listView
    }
    func currentLanguage() -> String {
        return Locale.preferredLanguages[0]
    }
    func getLanguageString(key: String) -> String {
        guard
            let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"),
            let languageDict = NSDictionary(contentsOfFile: path) as? Dictionary<String,String>
            else {
                return key
        }
        let flag = "\(String.localizedString(key: key.trimmingCharacters(in: .whitespaces))) "
        return flag + (languageDict[key] ?? key)
    }
    func getFont(size: CGFloat) -> UIFont {
        return UIFont(name: mainFont, size: size) ?? UIFont.systemFont(ofSize: size, weight: .regular)
    }
    
    func actualPageNumber(page: PDFPage) -> Int {
        guard adsInterval > 0 else {
            return page.pageNo
        }
        let n = adsInterval + 1
        let n1 = page.pageNo    //Page with ads
        let i = n1 % n          //
        let j = (n1-i)/n        //numbers of ads insert
        return n1 - j           //actual page
    }
    func adjustedPageNumber(page: PDFPage) -> Int {
        guard page.pageNo > 0 else {
            return 1
        }
        guard adsInterval > 0 else {
            return page.pageNo + 1
        }
        let n = adsInterval
        let x = page.pageNo + 1
        let n1 = x % (n+1)
        let n2 = (x - n1)/(n+1)
        return n2*n + n1
    }
    func isAdPage(page: PDFPage) -> Bool {
        guard adsInterval > 0 else {
            return false
        }
        let n = adsInterval
        let n1 = page.pageNo
        return ((n1+1) % (n+1)) == 0
    }
    func convert2AdsPageIndex(actual: Int) -> Int {
        guard
            adsInterval > 0,
            actual >= adsInterval
            else {
                return actual
        }
        let n = actual + 1
        let n1 = n % adsInterval
        let n2 = (n-n1)/adsInterval
        
        return actual + n2 - (n1 == 0 ? 1 : 0)
    }
    
    @objc func loadImage(with path: String) -> UIImage? {
        if let url = URL(string: path) {
            do {
                let data = try Data(contentsOf: url)
                return UIImage(data: data)
            } catch  {
                return nil
            }
        }
        return nil
    }
    
    @objc func loadPdfAds(roomId: String, mediaId: String, completion:@escaping (ADPdf?)->()) {
        guard let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) else {
            completion(nil)
            return
        }
        if !InternetConnection.isConnectedToNetwork() {
            completion(DB.getPdfAds(for: mediaId))
            return
        }
        let api = "%@api/DeviceAds/GetAds_New?deviceCode=%@&roomID=%@&AdsType=%@&MediaID=%@"
        let stringUrl = String(format: api, hostIpRoomAPI, deviceCode, roomId, "Fullread", mediaId)
        RoomAPI.shared.sendHTTPRequest(stringUrl, returnType: "JSON") { (json) in
            DispatchQueue.main.async {
                if let json = json as? [String:Any],
                    let jsonAds = json["ADS_READER"] as? [[String:Any]],
                    let x = json["READER_PAGE"] as? Int {
                    var ads = [ADImage]()
                    //update/add Media
                    let media = DB.addMedia(mediaId: mediaId, pageInterval: x)
                    if let media = media {
                        media.clearAds()
                    }
                    for js in jsonAds {
                        if let path = js["AdsImageUrl"] as? String, let url = URL(string: path) {
                            do {
                                let data = try Data(contentsOf: url)
                                let linkUrl = (js["LNKURL"] as? String) ?? ""
                                let name = (js["ADSNAME"] as? String) ?? ""
                                if let image = UIImage(data: data) {
                                    ads.append(ADImage(image: image, adLink: linkUrl, name: name))
                                    //update coreData
                                    //1. udate Ads Binary data
                                    if let id = js["ADSID"] as? String {
                                        DB.updateAds(adsId: id, image: image, linkUrl: linkUrl)
                                        //2. update media Ads
                                        if let media = media {
                                            media.updateAds(adId: id)
                                        }
                                    }
                                    
                                }
                            } catch  {
                            }
                        }
                    }
                    completion(ADPdf(interval: x, images: ads))
                    return
                }
                completion(ADPdf(interval: 0, images: []))
            }
        }
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    @objc func saveVideoAds(json: Dictionary<String, Any>) {
        guard let js = json["ADS_Video"] as? [NSDictionary], js.count > 0 else {return}
        let adsDict = NSDictionary(dictionary: js[0])
        
        if let url = URL(string: (adsDict["AdsImageUrl"] as? String) ?? "") {
            
            let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
            let fileURL = DocumentDirURL.appendingPathComponent("sp_video").appendingPathExtension("m3u8")
            
            do {
                let data = try Data(contentsOf: url)
                try data.write(to: fileURL, options: .atomic)
            } catch {
                print("failed with error: \(error)")
            }
        }
    }
    
    @objc class func prepareJournal(roomID: String) -> [Journal] {
        var journals = [Journal]()
        let links = ["http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/472-ThaiHealth2018-TH.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-555.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-549.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-548.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-545.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-543.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-522.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-517.pdf",
                     "http://www.ipsr.mahidol.ac.th/ipsrbeta/FileUpload/PDF/Report-File-513.pdf"]
        for path in links {
            if let data = DB.getAds(adsId: path),
                let imgData = data.offlineAd,
                let image = UIImage(data: imgData) {
                let journal = Journal(roomID: roomID, id: path, image: image)
                journal.title = data.adURL ?? ""
                journals.append(journal)
            } else {
                let journal = Journal(roomID: roomID, id: path)
                journals.append(journal)
            }
        }
        SVProgressHUD.dismiss()
        return journals
    }
}


//MARK:- Youtube
extension URL {
    var queryDictionary: [String: String]? {
        guard let query = self.query else { return nil}
        
        var queryStrings = [String: String]()
        for pair in query.components(separatedBy: "&") {
            
            let key = pair.components(separatedBy: "=")[0]
            
            let value = pair
                .components(separatedBy:"=")[1]
                .replacingOccurrences(of: "+", with: " ")
                .removingPercentEncoding ?? ""
            
            queryStrings[key] = value
        }
        return queryStrings
    }
}

class YT: NSObject {
    let api_key = "AIzaSyDV9Wjgp3FLNuBcj54PyLF__wNImX1clws"
    @objc var roomID: String
    @objc var videoID: String
    @objc var image: UIImage?
    @objc var title: String = ""
    var timer: Timer?
    //Statistic
    @objc var commentCount:Int = 0
    @objc var dislikeCount:Int = 0
    @objc var favoriteCount:Int = 0
    @objc var likeCount:Int = 0
    @objc var viewCount:Int = 0
    
    @objc init(roomID: String, id: String) {
        self.roomID = roomID
        videoID = id
        super.init()
        
        if let url = URL(string: youtubeInfoPath()) {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!)
                } else {
                    if let usableData = data {
                        do {
                            if let json = try JSONSerialization.jsonObject(with: usableData, options: []) as? [String: Any],
                                let items = json["items"] as? [[String:Any]],
                                let info = items.first,
                                let snippet = info["snippet"] as? [String:Any],
                                let title = snippet["title"] as? String {
                                self.title = title
                                //Statistic
                                if let stats = info["statistics"] as? [String:Any] {
                                    if let comments = stats["commentCount"] as? String {
                                        self.commentCount = comments.toInt()
                                    }
                                    if let dislikes = stats["dislikeCount"] as? String {
                                        self.dislikeCount = dislikes.toInt()
                                    }
                                    if let count = stats["favoriteCount"] as? String {
                                        self.favoriteCount = count.toInt()
                                    }
                                    if let count = stats["likeCount"] as? String {
                                        self.likeCount = count.toInt()
                                    }
                                    if let count = stats["viewCount"] as? String {
                                        self.viewCount = count.toInt()
                                    }
                                }
                            }
                        } catch {
                            
                        }
                        
                    }
                }
            }
            task.resume()
        }
        if let url = URL(string: thumbnailImagePath()) {
            do {
                let data = try Data(contentsOf: url)
                image  = UIImage(data: data)
            } catch  {
                
            }
            
        }
    }
    @objc func thumbnailImagePath() -> String {
        return "http://img.youtube.com/vi/\(videoID)/0.jpg"
    }
    
    @objc func youtubeVideoPath() -> String {
        return "http://www.youtube.com/embed/\(videoID)"
    }
    
    @objc func youtubeInfoPath() -> String {
        return "https://www.googleapis.com/youtube/v3/videos?id=\(videoID)&key=\(api_key)&fields=items(id,snippet(title),statistics)&part=snippet,statistics"
    }

}

class Journal: NSObject {
    @objc var roomID: String = ""
    @objc var path: String = ""
    @objc var title: String = ""
    @objc var image: UIImage!
    @objc init(roomID: String, id: String, image: UIImage? = nil) {
        self.roomID = roomID
        path = id
        super.init()
        guard image == nil else {
            self.image = image
            return
        }
        if let url = URL(string: path) {
            let queue = DispatchQueue(label: "th.co.mbox.MBlaze")
            queue.async {
                let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                    if error != nil {
                        print(error!)
                    } else {
                        if let data = data {
                            DispatchQueue.main.async {
                                if let pdfDoc = PDFDocument(data: data),
                                    pdfDoc.pageCount > 0,
                                    let page = pdfDoc.page(at: 0) {
                                    self.image = page.thumbnail(of: page.bounds(for: .artBox).size, for: .mediaBox)
                                    if let attributes = pdfDoc.documentAttributes,
                                        let title = attributes["Title"] as? String {
                                        self.title = title
                                    }
                                    DB.updateAds(adsId: id, image: self.image, linkUrl: self.title)
                                }
                            }
                        }
                    }
                }
                task.resume()
            }
        }
    }
}





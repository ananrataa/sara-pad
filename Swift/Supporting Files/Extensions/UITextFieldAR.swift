//
//  UITextFieldAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

extension UITextField {
    @objc func setLeftImage(image: UIImage, tint: UIColor) {
        let imgView = UIImageView(image: image)
        imgView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgView.tintColor = tint
        imgView.contentMode = .scaleAspectFill
        self.leftView = imgView
        self.leftViewMode = .always
    }
}

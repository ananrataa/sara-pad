//
//  StringAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 19/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

extension String {
    func convertDate(from fm1:String, to fm2:String) -> String {
        let df = DateFormatter()
        df.dateFormat = fm1
        if let date = df.date(from: self) {
            df.dateFormat = fm2
            return df.string(from: date)
        }
        return "invalid date"
    }
    func toNSString() -> NSString {
        return NSString(string: self)
    }
    func toDouble() -> Double {
        if let val = NumberFormatter().number(from: self)?.doubleValue {
            return val
        }
        return 0
    }
    func toFloat() -> Float {
        if let val = NumberFormatter().number(from: self)?.floatValue {
            return val
        }
        return 0
    }
    func toInt() -> Int {
        if let val = (NumberFormatter().number(from: self)?.intValue) {
            return val
        }
        return 0
    }
    func toInt16() -> Int16? {
        if let val = (NumberFormatter().number(from: self)?.int16Value) {
            return val
        }
        return 0
    }
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1", "YES":
            return true
        case "False", "false", "no", "0", "NO":
            return false
        default:
            return nil
        }
    }
    public func pairs(every n:Int) -> [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: count, by: n).forEach {
            result.append(String(characters[$0..<min($0+n, count)]))
        }
        return result
    }
    
    static func localizedString(key: String) -> String {
        return Utility.nsLocalizedString(key)
    }
    
    func toDateWith(format: String, locale: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: locale)
        return dateFormatter.date(from:self)
    }
}

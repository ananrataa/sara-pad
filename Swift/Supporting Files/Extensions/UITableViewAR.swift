//
//  UITableViewAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

extension UITableView {
    func indexPathForView(view: AnyObject) -> IndexPath? {
        let originInTableView = self.convert(CGPoint.zero, from: (view as! UIView))
        return self.indexPathForRow(at: originInTableView)
    }
}

extension UITableViewCell {
    class var identification: String {
        return String(describing: self)
    }
}


//
//  ARPdf.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 30/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit
extension PDFDocument {
    var firstPage: PDFPage? {
        if pageCount > 0 {
            return page(at: 0)
        }
        return nil
    }
    var lastPage: PDFPage? {
        if pageCount > 0 {
            return page(at: pageCount - 1)
        }
        return nil
    }
}

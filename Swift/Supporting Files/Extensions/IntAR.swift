//
//  IntAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

extension Int {
    func toString() -> String {
        let amtFormatter = NumberFormatter()
        amtFormatter.formatterBehavior = .behavior10_4
        amtFormatter.positiveFormat = "#,##0"
        
        return amtFormatter.string(from: NSNumber(value: self))!
    }
    
    func formatPoints() -> String {
        let doubleVal = Double(self)
        let thousandNum = doubleVal/1000
        let millionNum = doubleVal/1000000
        if self >= 1000 && doubleVal < 1000000{
            if(floor(thousandNum) == thousandNum){
                return ("\(Int(thousandNum))k").replacingOccurrences(of: ".0", with: "")
            }
            return("\(thousandNum.roundTo(places: 1))k").replacingOccurrences(of: ".0", with: "")
        }
        if self > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k").replacingOccurrences(of: ".0", with: "")
            }
            return ("\(millionNum.roundTo(places: 1))M").replacingOccurrences(of: ".0", with: "")
        }
        else{
            if(floor(doubleVal) == doubleVal){
                return ("\(Int(doubleVal))")
            }
            return ("\(doubleVal)")
        }
    }
}

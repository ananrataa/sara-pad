//
//  DateAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 19/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

enum DateLocale: String {
    case us = "en_US_POSIX"
    case th = "th_TH"
}
extension Date {
    /*Locale
     en_US_POSIX    = English (United States, Computer)
     th_TH              = Thai (Thailand)
     
     use format "EEE" for Sun, Tue, Wed...
     */
    
    func stringWithFormat(_ format: String, locale: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.init(identifier: locale)
        return formatter.string(from: self)
    }
    
    //Sun = 1, Mon = 2
    func dayOfWeek() -> Int {
        return Calendar.current.component(.weekday, from: self)
    }
    
    func nextDateWith(days: Int) -> Date? {
        var dateComp = DateComponents()
        dateComp.day = days
        return Calendar.current.date(byAdding: dateComp, to: self)
    }
    
    func getDateString(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
}

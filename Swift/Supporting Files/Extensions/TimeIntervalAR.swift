//
//  TimeIntervalAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

extension TimeInterval{
    func stringFromTimeInterval() -> String {
        let ti = NSInteger(self)
        // let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
    func stringFromTimeIntervalMMss() -> String {
        let ti = NSInteger(self)
        // let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        return String(format: "%0.2d:%0.2d",minutes + 60*hours,seconds)
    }
}

//
//  DoubleAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 19/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

extension Double {
    func toString(dec: Int) -> String {
        let isScientific = !self.isZero && (abs(self) > 1e5 || Decimal(abs(self)) < pow(10, -dec))
        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        let decStr = String.init(repeating: "0", count: dec)
        let posString = dec > 0 ?  "#,##0.\(decStr)"  : "#,##0"
        let negString = dec > 0 ? "(#,##0.\(decStr))" : "(#,##0)"
        let posSci = dec > 0 ? "0.\(decStr)E+00" : "0E+00"
        let negSci = dec > 0 ? "(0.\(decStr)E+00)" : "(0E+00)"
        
        numberFormat.positiveFormat = isScientific ? posSci : posString
        numberFormat.negativeFormat = isScientific ? negSci : negString
        
        let num = NSNumber(value: self)
        let numFormat = numberFormat.string(from: num) ?? ""
        return numFormat
    }
    
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

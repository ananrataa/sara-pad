//
//  NotificationAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let menuSelected = Notification.Name("menu_selected")
    static let googleTranslate = Notification.Name("GoogleTranslate")
    
    static let subCategoryTouched = Notification.Name("sub_category_touched")
    
    static let libraryChanged = Notification.Name("ChangeLibrary")
    static let history = Notification.Name("WatchHistory")
    static let signOut = Notification.Name("SignOut")
    static let setting = Notification.Name("Setting")
    static let signIn = Notification.Name("Signin")
    static let help = Notification.Name("Help")
    static let about = Notification.Name("About")
    
    static let memberProfile = Notification.Name("MemberProfile")
    static let redeemCourse = Notification.Name("Redeem")
}

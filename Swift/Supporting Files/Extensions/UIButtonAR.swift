//
//  UIButtonAR.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

extension UIButton {
    convenience init(image: UIImage, target: UIViewController?, action: Selector) {
        self.init(type: .custom)
        setImage(image, for: .normal)
        addTarget(target, action: action, for: .touchUpInside)
    }
    convenience init(color: UIColor, target: UIViewController?, action: Selector) {
        self.init(type: .custom)
        backgroundColor = color
        addTarget(target, action: action, for: .touchUpInside)
    }
    convenience init(title: String, target: UIViewController?, action: Selector) {
        self.init(type: .custom)
        setTitle(title, for: .normal)
        addTarget(target, action: action, for: .touchUpInside)
    }
    
    @objc func jitter() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
}

//
//  ARPickerView.swift
//  Anapanasati
//
//  Created by Anan Ratanasethakul on 10/23/18.
//  Copyright © 2018 Anan Ratanasethakul. All rights reserved.
//

import UIKit

protocol ARPickerViewDelegate: class {
    func pickerView(_ pickerView: ARPickerView, didSelectRow row: Int, inComponent component: Int)
}

@IBDesignable
class ARPickerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate {
    weak var delegate: ARPickerViewDelegate?
    var fontSize: CGFloat = 17
    lazy var att = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: fontSize, weight: .ultraLight),
               NSAttributedString.Key.foregroundColor: UIColor.white,
               NSAttributedString.Key.backgroundColor: UIColor.clear]
    lazy var attSelected = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: fontSize, weight: .ultraLight),
               NSAttributedString.Key.foregroundColor: UIColor.white,
               NSAttributedString.Key.backgroundColor: UIColor.clear]

    lazy var pickerView: UIPickerView = {
        let pv = UIPickerView(frame: self.bounds)
        pv.backgroundColor = .clear
        pv.tag = 11
        pv.dataSource = self
        pv.delegate = self
        return pv
    }()
    var rowHeight:CGFloat = 0
    var items: [String] = [] {
        didSet {
            rowHeight = items.map({$0.size(withAttributes: att).width}).max() ?? pickerView.frame.width/2
            setupPickerView()
            pickerView.reloadAllComponents()
        }
    }
    var images: [UIImage] = [] {
        didSet {
            items = []
            rowHeight = frame.height
            setupPickerView()
            pickerView.reloadAllComponents()
        }
    }
    
    fileprivate func setupPickerView() {
        if self.subviews.count == 0 {
            addSubview(pickerView)
            
            let rect = self.frame
            let p = pickerView.frame.origin
            pickerView.transform = CGAffineTransform(translationX: -p.x, y: 0)
            pickerView.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
            pickerView.reloadAllComponents()
            pickerView.frame = CGRect(x: 0, y: p.y, width: rect.width, height: rect.height)
            bringSubviewToFront(pickerView)
        }
        self.backgroundColor = .clear
    }
}

extension ARPickerView {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return max(items.count, images.count)
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.frame.height
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return rowHeight
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if items.count > 0, row <= items.count {
            let lbl = UILabel()
            lbl.backgroundColor = .clear
            lbl.textAlignment = .center
            lbl.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            lbl.attributedText = NSAttributedString(string: items[row], attributes: att)
            return lbl
        }
        if row <= images.count {
            let h = 0.85*rowHeight
            let img = images[row]
            let imgView = UIImageView(image: img.scaleToSize(aSize: CGSize(width: h, height: h)))
            imgView.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return imgView
        }
        return UIView()
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let delegate = delegate {
            delegate.pickerView(self, didSelectRow: row, inComponent: component)
        }
    }
}

extension ARPickerView {
    func selectRow(_ row: Int, inComponet: Int, animated: Bool)  {
        pickerView.selectRow(row, inComponent: inComponet, animated: animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            //to highlight red text animated must be false
            self.pickerView.selectRow(row, inComponent: inComponet, animated: false)
            self.pickerView(self.pickerView, didSelectRow: row, inComponent: inComponet)
        }
        
    }
}

extension UIImage {
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (self.size.equalTo(aSize)) {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0)
        self.draw(in: CGRect(origin: .zero, size: aSize))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? self
    }
}

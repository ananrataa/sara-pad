//
//  DesignableSlider.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 8/8/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableSlider: UISlider {
    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }

    var thumbCenterX: CGFloat {
        return thumbRect(forBounds: frame, trackRect: trackRect(forBounds: frame), value: value).midX
    }
    
}

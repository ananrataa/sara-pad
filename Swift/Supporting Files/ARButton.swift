//
//  ARButton.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 20/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class ARButton: UIButton {

    var badge: Int = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    var value: Int = 0 {
        didSet {
            if badge == 0 {
                setNeedsDisplay()
            }
        }
    }
    override func draw(_ rect: CGRect) {
        // Drawing code
        for v in subviews {
            if let lbl = v as? UILabel {
                lbl.removeFromSuperview()
            }
        }
        if badge > 0 {
            let font = UIFont.preferredFont(forTextStyle: .body)
            let attStr = NSAttributedString(string: " \(badge) ", attributes: [NSAttributedString.Key.font: font])
            let h = rect.height/2
            let w = max(h, attStr.size().width)
            let lblRect = CGRect(origin: CGPoint(x: rect.width - w + h/2, y: -h/2), size: .init(width: w, height: h))
            let label = UILabel(frame: lblRect)
            label.clipsToBounds = true
            label.backgroundColor = .red
            label.textColor = .white
            label.font = UIFont.preferredFont(forTextStyle: .body)
            label.text = "\(badge)"
            label.textAlignment = .center
            label.layer.cornerRadius = h/2
            self.addSubview(label)
        } else if value > 0 {
            let font = UIFont.preferredFont(forTextStyle: .body)
            let attStr = NSAttributedString(string: " \(value) ", attributes: [NSAttributedString.Key.font: font])
            let h = rect.height/2
            let w = max(h, attStr.size().width)
            let lblRect = CGRect(origin: CGPoint(x: rect.width - w + h/2, y: -h/2), size: .init(width: w, height: h))
            let label = UILabel(frame: lblRect)
            label.clipsToBounds = true
            label.backgroundColor = .darkGray
            label.textColor = .white
            label.font = UIFont.preferredFont(forTextStyle: .body)
            label.text = "\(value)"
            label.textAlignment = .center
            label.layer.cornerRadius = h/2
            self.addSubview(label)
        }
    }
 

}

//
//  CardCollectionViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class CardCollectionViewCell: BaseCell {
    var data:(row: Int, title: String, subTitles:[String])? {
        didSet {
            refreshView()
        }
    }
    let cardView: UIView = {
        let v = UIView()
        return v
    }()
    let iconView: UIImageView = {
        let imgV = UIImageView(image: #imageLiteral(resourceName: "icon_book32"))
        imgV.tintColor = .white
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let labelTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: sukhumvitLightFont, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: .light)
        lbl.textColor = .white
        lbl.backgroundColor = .clear
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    let accessoryButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "down_arrow24"), for: .normal)
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var sv: UIStackView!
    
    override func setup() {
        
        cardView.addSubview(iconView)
        cardView.addSubview(labelTitle)
        cardView.addSubview(accessoryButton)
        contentView.addSubview(cardView)
        
        cardView.addConstraintsWithFormat(format: "V:|-4-[v0(20)][v1]", views: iconView, labelTitle)
        cardView.addConstraintsWithFormat(format: "H:|-4-[v0(20)]|", views: iconView)
        cardView.addConstraintsWithFormat(format: "H:|-4-[v0]|", views: labelTitle)
        cardView.addConstraintsWithFormat(format: "H:[v0(20)]-8-|", views: accessoryButton)
        cardView.addConstraintsWithFormat(format: "V:|-4-[v0(20)]", views: accessoryButton)
        
        cardView.pin(to: contentView)
    }
    
    func refreshView() {
        if let data = data {
            let dfColor = UIColor(hexString: "")
            let color = UIColor(named: "Color-\(data.row)") ?? dfColor
            cardView.layer.borderColor = color.cgColor
            cardView.backgroundColor = color.withAlphaComponent(0.20)
            labelTitle.text = data.title
            accessoryButton.tag = data.row
            accessoryButton.isHidden = data.subTitles.count <= 0
            if data.subTitles.count > 0 {
                let font = UIFont(name: sukhumvitLightFont, size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .thin)
                var buttons: [UIButton] = []
                for (i, s) in data.subTitles.enumerated() {
                    let btn = UIButton()
                    btn.tag = i
                    btn.setTitle(s, for: .normal)
                    btn.titleLabel?.font = font
                    btn.backgroundColor = UIColor(hexString: "#1EB8D6").withAlphaComponent(0.35)
                    btn.layer.cornerRadius = 7
                    btn.addTarget(self, action: #selector(subCatTap(_:)), for: .touchUpInside)
                    btn.showsTouchWhenHighlighted = true
                    buttons.append(btn)
                }
                sv = UIStackView(arrangedSubviews: buttons)
                sv.axis = .vertical
                sv.alignment = .fill
                sv.distribution = .equalSpacing
                sv.spacing = 8
                cardView.addSubview(sv)
                cardView.addConstraintsWithFormat(format: "H:|-6-[v0]-6-|", views: sv)
                sv.anchor(top: labelTitle.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
                sv.alpha = 0
            } else {
                sv = nil
            }
        }
    }
    
    @objc func subCatTap(_ sender: UIButton) {
        if let data = data {
            let indexPath = IndexPath(row: sender.tag, section: data.row)
            NotificationCenter.default.post(name: .subCategoryTouched, object: indexPath)
        }
    }
    
    override func prepareForReuse() {
        for v in cardView.subviews {
            if v is UIStackView {
                v.removeFromSuperview()
            }
        }
    }
}

@available(iOS 11.0, *)
class RoundRectCard: CardCollectionViewCell {
    
    override func setup() {
        super.setup()
        cardView.layer.cornerRadius = 7
        cardView.layer.borderWidth = 1
    }
}

@available(iOS 11.0, *)
class CircleCard: CardCollectionViewCell {
    override func setup() {
        super.setup()
        cardView.layer.cornerRadius = cardView.frame.height/2
        cardView.layer.borderWidth = 0.75
    }
}

//
//  ListItemTableViewController.swift
//  A-Beam
//
//  Created by Anan Ratanasethakul on 6/23/17.
//  Copyright © 2017 Anan Ratanasethakul. All rights reserved.
//

import UIKit

enum ListItemState {
    case close
    case expand
}
@available(iOS 11.0, *)
protocol ListItemDelegate {
    func listItemSelected(listItem: ListItemTableViewController, index: Int)
}
@available(iOS 11.0, *)
class ListItemTableViewController: UITableViewController {
    var delegate: ListItemDelegate?
    
    var items: [String] {
        didSet {
            setRect()
            tableView.reloadData()
        }
    }
    var viewRect: CGRect {
        didSet {
            setRect()
        }
    }
    var font: UIFont
    var maxRect: CGRect!
    var state: ListItemState = .close
    let hlColor = UIColor.red
    var curIndex: Int = 0 {
        didSet {
            tableView.selectRow(at: IndexPath(row: curIndex, section: 0), animated: true, scrollPosition: .middle)
        }
    }
    lazy var selectedItem: String = {
        return items[curIndex]
    }()
    
    init(items: [String], viewRect: CGRect, font: UIFont) {
        self.items = items
        self.viewRect = viewRect
        self.font = font
        super.init(style: .plain)
        setRect()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.showsVerticalScrollIndicator = false
        tableView.frame = viewRect
        tableView.layer.borderColor = UIColor.lightGray.cgColor
        tableView.layer.borderWidth = 0.35
        tableView.layer.cornerRadius = 6
    }

    func setRect() {
        let x = viewRect.origin.x
        let y = viewRect.origin.y
        let w = viewRect.width
        let h = viewRect.height
        if items.count < 7 {
            maxRect = CGRect(x: x, y: y, width: w, height: CGFloat(items.count)*h)
        } else {
            maxRect = CGRect(x: x, y: y, width: w, height: 7*h)
        }
    }
    
    func expand() {
        state = .expand
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.frame = self.maxRect
            for (i, _) in self.items.enumerated() {
                let cell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0))
                cell?.textLabel?.textColor = i == self.curIndex ? self.hlColor : UIColor.black
            }
        }, completion: { (_) in
            self.tableView.scrollToNearestSelectedRow(at: .middle, animated: true)
            self.tableView.isHidden = false
        })
    }
    func close() {
        state = .close
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.frame = self.viewRect
            for (i, _) in self.items.enumerated() {
                let cell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0))
                cell?.textLabel?.textColor = UIColor.black
            }
        }, completion: { (_) in
            self.tableView.scrollToNearestSelectedRow(at: .top, animated: true)
            self.tableView.isHidden = true
        })
    }
    func selectItem(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        UIView.animate(withDuration: 0.3, animations: { 
            self.tableView.frame = self.viewRect
            for (i, _) in self.items.enumerated() {
                let cell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0))
                cell?.textLabel?.textColor = UIColor.darkGray
            }
        }) { (_) in
            self.tableView.scrollToNearestSelectedRow(at: .top, animated: true)
            self.state = .close
            self.curIndex = index
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewRect.height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "LIST_ITEM")
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "LIST_ITEM")
            cell?.textLabel?.font = font
            cell?.textLabel?.backgroundColor = LightTheme.shared.textBackground.withAlphaComponent(0.75)
        }
        cell?.textLabel?.textColor = (indexPath.row == curIndex) ? self.hlColor : UIColor.black
        cell?.textLabel?.text = items[indexPath.row]


        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if items.count <= 1 {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        if state == .close {
            expand()
        } else {
            close()
            self.delegate?.listItemSelected(listItem: self, index: indexPath.row)
            curIndex = indexPath.row
        }
    }
}

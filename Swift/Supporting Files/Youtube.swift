//
//  Youtube.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 25/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

/*
class YT: NSObject {
    let api_key = "AIzaSyDV9Wjgp3FLNuBcj54PyLF__wNImX1clws"
    @objc var roomID: String
    @objc var videoID: String
    @objc var image: UIImage?
    @objc var title: String = ""
    var timer: Timer?
    //Statistic
    @objc var commentCount:Int = 0
    @objc var dislikeCount:Int = 0
    @objc var favoriteCount:Int = 0
    @objc var likeCount:Int = 0
    @objc var viewCount:Int = 0
    
    @objc init(roomID: String, id: String) {
        self.roomID = roomID
        videoID = id
        super.init()
        sendHTTPRequest(youtubeInfoPath(), returnType: "JSON") { (json) in
            if let json = json as? [String: Any],
                let items = json["items"] as? [[String:Any]],
                let info = items.first,
                let snippet = info["snippet"] as? [String:Any],
                let title = snippet["title"] as? String {
                self.title = title
                //Statistic
                if let stats = info["statistics"] as? [String:Any] {
                    self.commentCount = (stats["commentCount"] as? Int) ?? 0
                    self.dislikeCount = (stats["dislikeCount"] as? Int) ?? 0
                    self.favoriteCount = (stats["favoriteCount"] as? Int) ?? 0
                    self.likeCount = (stats["likeCount"] as? Int) ?? 0
                    self.viewCount = (stats["viewCount"] as? Int) ?? 0
                }
                print("\(title)")
            }
        }
        
        if let url = URL(string: thumbnailImagePath()) {
            do {
                let data = try Data(contentsOf: url)
                image  = UIImage(data: data)
            } catch  {
                
            }
            
        }
    }
    @objc func thumbnailImagePath() -> String {
        return "http://img.youtube.com/vi/\(videoID)/0.jpg"
    }
    
    @objc func youtubeVideoPath() -> String {
        return "http://www.youtube.com/embed/\(videoID)"
    }
    
    @objc func youtubeInfoPath() -> String {
        return "https://www.googleapis.com/youtube/v3/videos?id=\(videoID)&key=\(api_key)&fields=items(id,snippet(title),statistics)&part=snippet,statistics"
    }
    //MARK:- URL Request
    func sendHTTPRequest(_ url: String, returnType: String, method: String? = "GET", completion: @escaping (Any?)->Void) {
        guard
            let stringUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = NSURL(string: stringUrl) as URL?
            else {
                completion(nil)
                return
        }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        timer = Timer.scheduledTimer(withTimeInterval: 32, repeats: false, block: { (timer) in
            session.invalidateAndCancel()
            timer.invalidate()
        })
        
        let queue = DispatchQueue(label: "th.co.mbox.MBlaze")
        queue.async {
            if SPUtility.shared.isConnectedToNetwork() {
                DispatchQueue.main.async {[weak self] in
                    let request: NSMutableURLRequest = NSMutableURLRequest()
                    request.url = url
                    request.httpMethod = method!
                    request.timeoutInterval = 30
                    
                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                        guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                            else {
                                print("error: not a valid http response")
                                self?.cancelTimer()
                                completion(nil)
                                return
                        }
                        if httpResponse.statusCode == 200 {
                            self?.cancelTimer()
                            if returnType == "JSON" {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: receivedData, options: [])
                                    completion(json)
                                } catch  {
                                    completion(nil)
                                }
                            } else if returnType == "STRING" {
                                if let data = data, let text = String.init(data: data, encoding: String.Encoding.utf8) {
                                    completion(text)
                                } else {
                                    completion(nil)
                                }
                            } else if returnType == "DATA" {
                                if let data = data as NSData? {
                                    completion(data)
                                }
                            }
                        } else if let error = error {
                            self?.cancelTimer()
                            print(error.localizedDescription)
                            completion(nil)
                        } else {
                            self?.cancelTimer()
                            completion(nil)
                        }
                    })
                    dataTask.resume()
                }
            } else {
                self.cancelTimer()
                completion(nil)
                print("No internet connection!")
            }
        }
    }
    
    func cancelTimer() {
        if let timer = timer {
            timer.invalidate()
            self.timer = nil
        }
    }
}
*/

//
//  Alerts.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 16/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
enum ActionButton {
    case OK
    case CANCEL
}

protocol AlertHandler {}

extension AlertHandler where Self: UIViewController {
    func handle(error: Error) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    func successAlert(message: String, duration: Double, completion: (()->Void)?) {
        SVProgressHUD.showSuccess(withStatus: message)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            SVProgressHUD.dismiss()
            if let completion = completion {
                completion()
            }
        }
    }
    func infoAlert(message: String, duration: Double) {
        SVProgressHUD.showInfo(withStatus: message)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            SVProgressHUD.dismiss()
        }
    }
    func confirmAlert(title: String, messgae: String, okTitle: String, selectedAction: @escaping (ActionButton)->Void) {
        let strCancel = String.localizedString(key: "Cancel")
        let alert = UIAlertController(title: title, message: messgae, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: strCancel, style: .cancel, handler: { (_) in
            selectedAction(.CANCEL)
        })
        let okAction = UIAlertAction(title: okTitle, style: .destructive, handler: { (_) in
            selectedAction(.OK)
        })
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func errorAlert(message: String, duration: Double, completion: @escaping ()->()) {
        SVProgressHUD.showError(withStatus: message)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            SVProgressHUD.dismiss()
            completion()
        }
    }

}

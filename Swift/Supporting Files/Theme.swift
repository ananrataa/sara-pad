//
//  Theme.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 20/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

protocol Theme {
    var barBackground:UIColor {get}
    var barTitleFont:UIFont {get}
    var barTitleTint:UIColor {get}
    var barButtonFont:UIFont {get}
    var barButtonItemTint:UIColor {get}
    var textBackground: UIColor {get}
    //UILabel & UITextField
    var textColor:UIColor {get}
}

@available(iOS 11.0, *)
extension Theme {
    func applyNavigationBar(vc: UIViewController) {
        if self is TransparentTheme {
            vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            vc.navigationController?.navigationBar.shadowImage = UIImage()
            vc.navigationController?.navigationBar.backgroundColor = .clear
            vc.navigationController?.navigationBar.isTranslucent = true
        } else {
            vc.navigationController?.navigationBar.isTranslucent = false
            vc.navigationController?.navigationBar.barTintColor = barBackground
        }
        vc.navigationController?.navigationBar.tintColor = barButtonItemTint
        vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: barTitleFont, NSAttributedString.Key.foregroundColor: barTitleTint]
        if self is LightTheme {
            vc.navigationController?.navigationBar.barStyle = .default
        } else {
            vc.navigationController?.navigationBar.barStyle = .black
        }
    }
    
    func createLabel(with font: UIFont?, alignment: NSTextAlignment) -> UILabel {
        let lbl = UILabel()
        lbl.font = font ?? UIFont.systemFont(ofSize: font?.pointSize ?? 17)
        lbl.textColor = textColor
        lbl.textAlignment = alignment
        lbl.backgroundColor = .clear
        return lbl
    }
}

@available(iOS 11.0, *)
struct DarkTheme: Theme {
    var textBackground: UIColor = UIColor(hexString: "#474654")
    
    var textColor:UIColor = .white
    
    var barButtonItemTint:UIColor = .white
    
    var barButtonFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
    
    var barBackground:UIColor = (UIImage(named: "wood_1") != nil) ? UIColor(patternImage: UIImage(named: "wood_1")!) : #colorLiteral(red: 0.0588, green: 0.0588, blue: 0.0588, alpha: 1) /* #0f0f0f */
    
    var barTitleFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 21) ?? UIFont.systemFont(ofSize: 21, weight: .light)
    
    var barTitleTint:UIColor = .white
    
    static let shared = DarkTheme()
    private init() {}
}

@available(iOS 11.0, *)
struct LightTheme: Theme {
    var textBackground: UIColor = UIColor(hexString: "#FFFFFF")
    
    var textColor: UIColor = UIColor(named: "sp_darkGray") ?? .darkGray
    
    var barButtonItemTint:UIColor = UIColor(named: "sp_darkGray") ?? .darkGray
    
    var barButtonFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
    
    var barBackground:UIColor = .white
    
    var barTitleFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 21) ?? UIFont.systemFont(ofSize: 21, weight: .light)
    
    var barTitleTint:UIColor = UIColor(named: "sp_darkGray") ?? .darkGray
    
    static let shared = LightTheme()
    private init() {}
}

@available(iOS 11.0, *)
struct OrangeTheme: Theme {
    var textBackground: UIColor = UIColor.clear
    
    var textColor: UIColor = .black
    
    var barButtonItemTint:UIColor = UIColor(named: "sp_darkGray") ?? .darkGray
    
    var barButtonFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
    
    var barBackground:UIColor = UIColor(named: "sp_orange") ?? .orange
    
    var barTitleFont:UIFont = UIFont(name: "\(sukhumvitFont)", size: 21) ?? UIFont.systemFont(ofSize: 21, weight: .light)
    
    var barTitleTint:UIColor = UIColor(named: "sp_darkGray") ?? .darkGray
    
    static let shared = OrangeTheme()
    private init() {}
}

@available(iOS 11.0, *)
struct TransparentTheme: Theme {
    var barBackground: UIColor = .clear
    
    var barTitleFont: UIFont = UIFont(name: "\(sukhumvitFont)", size: 21) ?? UIFont.systemFont(ofSize: 21, weight: .light)
    
    var barTitleTint: UIColor = .white
    
    var barButtonFont: UIFont = UIFont(name: "\(sukhumvitFont)", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .light)
    
    var barButtonItemTint: UIColor = .white
    
    var textBackground: UIColor = .clear
    
    var textColor: UIColor = .white
    
    static let shared = TransparentTheme()
    private init() {}
}

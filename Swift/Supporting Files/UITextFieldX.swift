//
//  DesignableUITextField.swift
//  SkyApp
//
//  Created by Mark Moeykens on 12/16/16.
//  Copyright © 2016 Mark Moeykens. All rights reserved.
//

import UIKit

@IBDesignable
class UITextFieldX: UITextField {
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftString: String? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var rightString: String? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    private var _isRightViewVisible: Bool = true
    var isRightViewVisible: Bool {
        get {
            return _isRightViewVisible
        }
        set {
            _isRightViewVisible = newValue
            updateView()
        }
    }
    
    func updateView() {
        setLeftImage()
        setRightImage()
        setLeftString()
        setRightString()
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: tintColor ?? UIColor.darkGray])
    }
    
    func setLeftImage() {
        leftViewMode = UITextField.ViewMode.always
        var view: UIView
        
        if let image = leftImage {
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            imageView.contentMode = .scaleAspectFill
            
            var width = imageView.frame.width + leftPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: 20))
        }
        
        leftView = view
    }
    
    func setRightImage() {
        rightViewMode = UITextField.ViewMode.always
        
        var view: UIView
        
        if let image = rightImage, isRightViewVisible {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            
            var width = imageView.frame.width + rightPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: 20))
        }
        
        rightView = view
    }
    func setLeftString() {
        leftViewMode = UITextField.ViewMode.always
        
        var view: UIView
        
        if let str = leftString, isRightViewVisible {
            let h = self.frame.height
            let lbl = UILabel(frame: CGRect(x: leftPadding, y: 0, width: 30, height: h))
            lbl.text = str
            let fontName = self.font?.fontName
            let fontSize = self.font?.pointSize
            lbl.font = UIFont(name: fontName!, size: 0.8*fontSize!)
            lbl.textColor = .white
            lbl.backgroundColor = self.textColor
            lbl.adjustsFontSizeToFitWidth = true
            lbl.layer.borderWidth = self.layer.borderWidth
            lbl.layer.borderColor = self.layer.borderColor
            
            var width = lbl.frame.width + leftPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: h))
            view.addSubview(lbl)
        } else {
            let h = self.frame.height
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: h))
        }
        
        leftView = view
    }
    
    func setRightString() {
        var view: UIView
        if let str = rightString, isRightViewVisible, rightImage == nil {
            let fontName = self.font?.fontName
            let fontSize = self.font?.pointSize
            let h = self.frame.height
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: h))
            lbl.text = str
            lbl.font = UIFont(name: fontName!, size: 0.8*fontSize!)
            lbl.textColor = .white
            lbl.backgroundColor = self.tintColor
            lbl.adjustsFontSizeToFitWidth = true
            lbl.textAlignment = .center
            
            var width = lbl.frame.width + rightPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: h))
            view.addSubview(lbl)
        } else {
            let h = self.frame.height
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: h))
        }
        
        rightView = view
    }
    // MARK: - Corner Radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}


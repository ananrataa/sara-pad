//
//  MenuBar.swift
//  A-Beam
//
//  Created by Anan Ratanasethakul on 20/4/18.
//  Copyright © 2018 Anan Ratanasethakul. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class MenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let cellId = "menuCell"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor(named: "sp_orange")
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    var menuTitles: [String]
    var selectedIndex = 0
    
    init(titles: [String]) {
        menuTitles = titles
        super.init(frame: .zero)
        setupCollectionView()
    }
    
    fileprivate func setupCollectionView() {
        collectionView.register(MemnuCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: collectionView)
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MemnuCell
        let menuTitle = Bundle.main.localizedString(forKey: menuTitles[indexPath.item], value: nil, table: nil)
        cell.lblTitle.text = menuTitle
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/CGFloat(menuTitles.count), height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        SVProgressHUD.show()
        perform(#selector(menuSelected(indexPath:)), with: indexPath, afterDelay: 0.1)
    }
    @objc func menuSelected(indexPath: IndexPath) {
        NotificationCenter.default.post(name: .menuSelected, object: indexPath.item)
    }
}

@available(iOS 11.0, *)
class MemnuCell: BaseCell {
    let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .black
        lbl.font = UIFont(name: mainFont, size: 17)
        return lbl
    }()
    let tabView: UIView = {
        let v = UIView()
        return v
    }()
    
    override func setup() {
        super.setup()
        addSubview(lblTitle)
        addSubview(tabView)
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 9, left: 0, bottom: 9, right: 0))

        addConstraintsWithFormat(format: "H:|[v0]|", views: tabView)
        addConstraintsWithFormat(format: "V:|-46-[v0]|", views: tabView)
    }
    
    override var isHighlighted: Bool {
        didSet {
            lblTitle.tintColor = isHighlighted ? .white : UIColor.rgb(red: 91, green: 14, blue: 13)
            tabView.backgroundColor = isHighlighted ? .black : UIColor(named: "sp_orange")
        }
    }
    
    override var isSelected: Bool {
        didSet {
            lblTitle.tintColor = isSelected ? .white : UIColor.rgb(red: 91, green: 14, blue: 13)
            tabView.backgroundColor = isSelected ? .black : UIColor(named: "sp_orange")
        }
    }
}

//
//  MediaSearchViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/22/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

@available(iOS 11.0, *)
class MediaSearchViewController: UIViewController, UISearchDisplayDelegate, UISearchResultsUpdating {
    let disposeBag = DisposeBag()
    let searchId = "search_cell"
    var isLoadingMore = false
    var curLoadingCount = 1
    var endOfData = false
    
    let searchBar: UISearchBar = {
        let sb = UISearchBar()
        return sb
    }()
    let tableView: UITableView = {
        let tb = UITableView(frame: .zero, style: UITableView.Style.plain)
        tb.backgroundColor = .clear
        return tb
    }()
    let activityView: UIActivityIndicatorView = {
        let av = UIActivityIndicatorView(style: .white)
        av.hidesWhenStopped = true
        return av
    }()
    var mediaItems:[SPPreviewMedia] = []
    var roomId:String
    
    @objc init(roomId:String) {
        self.roomId = roomId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        view.backgroundColor = .black
        view.tintColor = .darkGray
        //TableView
        view.addSubview(tableView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: tableView)
        view.addConstraintsWithFormat(format: "V:|[v0]|", views: tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = Bundle.main.localizedString(forKey: " Search...", value: nil, table: nil)
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        
        
        navigationController?.navigationBar.tintColor = UIColor(hexString: ThemeTemplate.getThemeSet2())
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font:UIFont(name: mainFont, size: 36)!]
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close_black"), style: .done, target: self, action: #selector(done))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Grid"), style: .plain, target: self, action: #selector(viewAsGrid))
        
        //UIActivityIndicatorView
        view.addSubview(activityView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: activityView)
        view.addConstraintsWithFormat(format: "V:[v0]-20-|", views: activityView)
        
        //Rx
        searchBar
            .rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: {[unowned self] (value) in
                let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
                SearchModel.shared.createSearchMediaObserverable(deviceCode: deviceCode,
                                                                 roomId: self.roomId,
                                                                 mediaKind: "",
                                                                 catId: "",
                                                                 subId: "",
                                                                 findValue: value,
                                                                 loadIndex: 1)
                    .subscribe(onNext: { items in
                        self.mediaItems = items
                        self.tableView.reloadData()
                    }).disposed(by: self.disposeBag)
            }).disposed(by: disposeBag)
        
        //tableView selected
        self.tableView.rx.itemSelected.subscribe(onNext: {[weak self] (indexPath) in
            print(self?.mediaItems[indexPath.row].mediaName ?? "n/a")
        }).disposed(by: self.disposeBag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        title = Bundle.main.localizedString(forKey: "Search medias", value: nil, table: nil)
        
        searchBar.becomeFirstResponder()
    }
    
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    @objc func viewAsGrid() {
        if let text = searchBar.text, !text.isEmpty {
            self.title = ""
            searchBar.resignFirstResponder()
            let vc = MediaSearchListViewController(roomId: roomId, searchText: text, medias:mediaItems)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        if (contentOffset - maximumOffset) > 20 {
            activityView.startAnimating()
            view.bringSubviewToFront(activityView)
            if endOfData {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                    self?.activityView.stopAnimating()
                }
            }
        } else {
            view.bringSubviewToFront(tableView)
            activityView.stopAnimating()
        }
    }
}

@available(iOS 11.0, *)
extension MediaSearchViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        mediaItems = []
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
}

@available(iOS 11.0, *)
extension MediaSearchViewController: UITableViewDataSource, UITableViewDelegate, AlertHandler {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: searchId) else {
                let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: searchId)
                cell.textLabel?.font = UIFont(name: mainFont, size: 17)
                cell.detailTextLabel?.font = UIFont(name: sukhumvitLightFont, size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .light)
                return cell
            }
            return cell
        }()
        cell.backgroundColor = .clear
        cell.textLabel?.backgroundColor = .clear
        cell.detailTextLabel?.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.textColor = .lightGray
        
        let media = mediaItems[indexPath.row]
        
        if let text = searchBar.text {
            //Media name
            if media.mediaName.lowercased().contains(text.lowercased()) || media.mediaName.uppercased().contains(text.uppercased()) {
                let attrString: NSMutableAttributedString = NSMutableAttributedString(string: media.mediaName)
                let range: NSRange = (media.mediaName as NSString).range(of: text , options:.caseInsensitive)
                attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: range)
                cell.textLabel?.attributedText = attrString
            } else {
                cell.textLabel?.text = media.mediaName
            }
            
            //Author
            if media.author.lowercased().contains(text.lowercased()) || media.author.uppercased().contains(text.uppercased()) {
                let attrString: NSMutableAttributedString = NSMutableAttributedString(string: media.author)
                let range: NSRange = (media.author as NSString).range(of: text , options:.caseInsensitive)
                attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: range)
                cell.detailTextLabel?.attributedText = attrString
            } else {
                cell.detailTextLabel?.text = media.author
            }
        } else {
            cell.textLabel?.text = media.mediaName
            cell.detailTextLabel?.text = media.author
        }
        
        //Media Type
        switch media.mediaKind {
        case "D":cell.imageView?.image = #imageLiteral(resourceName: "iconbook")
        case "V":cell.imageView?.image = #imageLiteral(resourceName: "iconvideo")
        case "M":cell.imageView?.image = #imageLiteral(resourceName: "iconmagazine")
        case "A":cell.imageView?.image = #imageLiteral(resourceName: "iconaudio")
        default:cell.imageView?.image = nil
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let preloadingTreashold = Int(mediaItems.count)
        let threasholdReached = indexPath.row >= preloadingTreashold
        let reachedLastElement = indexPath.row == mediaItems.count - 1
        if !isLoadingMore && (threasholdReached || reachedLastElement) {
            let deadlineTime = DispatchTime.now() + .seconds(1)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.loadMore()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        SVProgressHUD.show()
        let media = mediaItems[indexPath.row]
        let vc = MediaDetailViewController(mediaId: media.mediaId, roomId: media.roomId)
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func loadMore() {
        guard let text = searchBar.text else {return}
        isLoadingMore = true
        DispatchQueue.global().async {
            self.curLoadingCount += 1
            let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
            RoomAPI.shared.getPreviewMedias(deviceCode: deviceCode, roomId: self.roomId, mediaKind: "", catId: "", subId: "", findValue: text, loadIndex: self.curLoadingCount) {[weak self] (items) in
                if let items = items {
                    if items.count == 0 {
                        DispatchQueue.main.async {
                            self?.infoAlert(message: "No more daata.", duration: 1)
                            self?.endOfData = true
                        }
                    } else {
                        self?.mediaItems.append(contentsOf: items)
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                } else {
                    
                }
                
            }
        }
    }
}

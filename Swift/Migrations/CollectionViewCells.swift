//
//  CollectionViewCells.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class MediaCell: BaseCell {
    let mediaImageView: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleToFill
        imgV.image = #imageLiteral(resourceName: "default_media_cover")
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let mediaTypeImageView: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleAspectFit
        imgV.image = #imageLiteral(resourceName: "default_media_cover")
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    let lblMediaTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: mainFont, size: 13)
        lbl.textAlignment = .center
        lbl.textColor = .black
        return lbl
    }()
    let lblMediaAuthor: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont(name: sukhumvitLightFont, size: 11) ?? UIFont.systemFont(ofSize: 11, weight: .light)
        lbl.textColor = .darkGray
        return lbl
    }()
    let rateStars:[UIButton] = {
        var buttons: [UIButton] = []
        for i in 0..<5 {
            let btn = UIButton()
            btn.setImage(#imageLiteral(resourceName: "iconvote-gray"), for: .normal)
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.widthAnchor.constraint(equalToConstant: 15).isActive = true
            btn.heightAnchor.constraint(equalToConstant: 15).isActive = true
            buttons.append(btn)
        }
        return buttons
    }()
    let activity: UIActivityIndicatorView = {
        let av = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        av.hidesWhenStopped = true
        av.startAnimating()
        return av
    }()
    var curMedia: SPPreviewMedia? {
        didSet {
            refreshView()
        }
    }
    override func setup() {
        backgroundColor = .white
        
        addSubview(mediaImageView)
        addSubview(mediaTypeImageView)
        addSubview(lblMediaTitle)
        addSubview(lblMediaAuthor)
        addSubview(activity)
        
        let sv = UIStackView(arrangedSubviews: rateStars)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .horizontal
        sv.distribution = .equalSpacing
        sv.spacing = 5
        addSubview(sv)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: mediaImageView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: lblMediaTitle)
        addConstraintsWithFormat(format: "H:|[v0]|", views: lblMediaAuthor)
        addConstraintsWithFormat(format: "V:|[v0]-8-[v1(20)]-8-[v2(14)]-5-[v3(15)]|", views: mediaImageView, lblMediaTitle, lblMediaAuthor, sv)
        mediaTypeImageView.anchor(top: mediaImageView.topAnchor,
                                  leading: nil,
                                  bottom: nil,
                                  trailing: mediaImageView.trailingAnchor,
                                  padding: .init(top: 5, left: 0, bottom: 0, right: 5),
                                  size: .init(width: 25, height: 25))
        sv.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        addConstraintsWithFormat(format: "H:|[v0]|", views: activity)
        addConstraintsWithFormat(format: "V:|[v0]|", views: activity)
        bringSubviewToFront(activity)
    }
    
    func refreshView() {
        if let media = curMedia {
            let url = URL(string: media.mediaCoverUrl)
            mediaImageView.sd_setImageWithPreviousCachedImage(with: url,
                                                              placeholderImage: #imageLiteral(resourceName: "default_media_cover"),
                                                              options: SDWebImageOptions(rawValue: 0),
                                                              progress: nil,
                                                            completed: nil)
            lblMediaTitle.text = media.mediaName
            lblMediaAuthor.text = media.author
            //Media Type
            switch media.mediaKind {
            case "D":mediaTypeImageView.image = #imageLiteral(resourceName: "iconbook")
            case "V":mediaTypeImageView.image = #imageLiteral(resourceName: "iconvideo")
            case "M":mediaTypeImageView.image = #imageLiteral(resourceName: "iconmagazine")
            case "A":mediaTypeImageView.image = #imageLiteral(resourceName: "iconaudio")
            default:mediaTypeImageView.image = nil
            }
            if media.popularVote > 0 && media.popularVote <= 5 {
                DispatchQueue.main.async {[weak self] in
                    for i in 0..<5 {
                        if i < media.popularVote {
                            self?.rateStars[i].setImage(#imageLiteral(resourceName: "iconvote-yellow"), for: .normal)
                        } else {
                            self?.rateStars[i].setImage(#imageLiteral(resourceName: "iconvote-gray"), for: .normal)
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {[weak self] in
                    for i in 0..<5 {
                        self?.rateStars[i].setImage(#imageLiteral(resourceName: "iconvote-gray"), for: .normal)
                    }
                }
            }
            activity.stopAnimating()
        }
    }
    
    @objc func rateMedia(_ sender: UIButton) {
        
    }
}



//
//  CategoriesCollectionViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxSwift


@available(iOS 11.0, *)
class CategoriesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    let cellId = "cat_cell"
    let disposeBag = DisposeBag()
    let bgImage: UIImageView = {
       let imgV = UIImageView(image: #imageLiteral(resourceName: "bg_mountain"))
        imgV.contentMode = .scaleAspectFill
        imgV.alpha = 0.175
        return imgV
    }()
    var categoryItems:[SPCategory] = []
    var curIndexPath: IndexPath?
    var itemHeights:[CGFloat] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DarkTheme.shared.applyNavigationBar(vc: self)
        title = String.localizedString(key: "CategoryName")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        self.collectionView!.register(RoundRectCard.self, forCellWithReuseIdentifier: cellId)
        self.collectionView?.contentInset = .init(top: 20, left: 20, bottom: 20, right: 20)
        
        //Background
        view.addSubview(bgImage)
        bgImage.pin(to: view)
        self.collectionView?.backgroundColor = .clear
        view.bringSubviewToFront(collectionView!)
        
        MediaModel.shared.createMediaCategoriesObservable(loadIndex: 1).subscribe(onNext: {[weak self] (items) in
            self?.categoryItems = items
            DispatchQueue.main.async {
                self?.collectionView?.reloadData()
                self?.calculateRowHeight()
                SVProgressHUD.dismiss()
            }
        }).disposed(by: disposeBag)
        
        
        //Subcategory touched
        NotificationCenter.default.addObserver(forName: .subCategoryTouched, object: nil, queue: nil) { (ntf) in
            if let indexPath = ntf.object as? IndexPath {
                print(self.categoryItems[indexPath.section].subCategories[indexPath.row])
            }
        }
    }

    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .subCategoryTouched, object: nil)
    }
    func calculateRowHeight() {
        for c in categoryItems {
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: ((collectionView?.frame.width)! - 60)/2, height: 32))
            lbl.font = UIFont(name: sukhumvitLightFont, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: .light)
            lbl.text = c.categoryName
            lbl.numberOfLines = 0
            lbl.sizeToFit()
            itemHeights.append(lbl.frame.height)
        }
    }
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryItems.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RoundRectCard
        
        cell.accessoryButton.addTarget(self, action: #selector(accessoryTouched(_:)), for: .touchUpInside)
        let curCat = categoryItems[indexPath.item]
        cell.data = (indexPath.item, curCat.categoryName, curCat.subCategories.map{$0.subCategoryName})
        if let sv = cell.sv {
            sv.alpha = 0
        }
        if let selectedIndexPath = curIndexPath, selectedIndexPath == indexPath {
            cell.labelTitle.numberOfLines = 0
            cell.accessoryButton.setImage(#imageLiteral(resourceName: "up_arrow24"), for: .normal)
            if let sv = cell.sv {
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                    sv.alpha = 1
                }, completion: nil)
            }
        } else {
            cell.labelTitle.numberOfLines = 2
            cell.accessoryButton.setImage(#imageLiteral(resourceName: "down_arrow24"), for: .normal)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let selectedIndexPath = curIndexPath, selectedIndexPath == indexPath {
            let subCats = categoryItems[indexPath.item].subCategories.count
            let h: CGFloat = 28 + itemHeights[indexPath.item]
            return CGSize(width: (collectionView.frame.width - 60)/2, height: h + CGFloat(subCats*32 + (subCats+1)*8))
        } else {
            return CGSize(width: (collectionView.frame.width - 60)/2, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    @objc func accessoryTouched(_ sender: UIButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if let selectedIndexPath = curIndexPath, selectedIndexPath == indexPath {
            curIndexPath = nil
        } else {
            curIndexPath = indexPath
        }
        collectionView?.reloadData()
        collectionView?.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

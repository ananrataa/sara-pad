//
//  Jittable.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 16/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class JitterButton: UIButton, Jitterable {}
class JitterTextField: UITextField, Jitterable {}
class FlashLabel: UILabel, Flashable, Jitterable {}

protocol Jitterable {}

extension Jitterable where Self: UIView {
    func jitter() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
}

protocol Flashable {}
extension Flashable where Self: UIView {
    func flash() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.alpha = 1.0
        }) { (animationComplete) in
            UIView.animate(withDuration: 0.3, delay: 2.0, options: .curveEaseOut, animations: {
                self.alpha = 0
            }, completion: nil)
        }
    }
}

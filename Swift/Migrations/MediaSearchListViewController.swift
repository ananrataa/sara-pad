//
//  MediaSearchListViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/22/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class MediaSearchListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AlertHandler {
    let mediaId = "media_cell"
    let spacing:CGFloat = 8
    var isLoadingMore = false
    var curLoadingCount = 1
    let menuBar: MenuBar = {
        let mb = MenuBar(titles:["ThisLibrary", "YourLibraries", "AllLibraries"])
        return mb
    }()
    let activityView: UIActivityIndicatorView = {
        let av = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        av.hidesWhenStopped = true
        av.stopAnimating()
        return av
    }()
    var roomId: String
    var searchText: String
    var medias:[SPPreviewMedia] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
                self.isLoadingMore = false
                if self.curLoadingCount == 1 {
                    if self.medias.count > 0 {
                        self.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                    }
                }
                SVProgressHUD.dismiss()
            }
        }
    }
    init(roomId: String, searchText: String, medias:[SPPreviewMedia]) {
        self.roomId = roomId
        self.searchText = searchText
        self.medias = medias
        let fl = UICollectionViewFlowLayout()
        fl.minimumInteritemSpacing = spacing
        fl.minimumLineSpacing = 20
        super.init(collectionViewLayout: fl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor(hexString: "#ffffff")
        setupNotification()
        setupUI()
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .menuSelected, object: nil)
    }
    func setupUI() {
        //Title
        title = Bundle.main.localizedString(forKey: "SearchFor", value: nil, table: nil) + "\"\(searchText)\""
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close_black"), style: .done, target: self, action: #selector(done))
        collectionView?.register(MediaCell.self, forCellWithReuseIdentifier: mediaId)
        collectionView?.contentInset = UIEdgeInsets.init(top: 62, left: spacing, bottom: 0, right: spacing)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets.init(top: 62, left: 0, bottom: 0, right: 0)
        collectionView?.scrollsToTop = true
        view.addSubview(menuBar)
        
        menuBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))
        
        //UIActivityIndicatorView
        view.addSubview(activityView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: activityView)
        view.addConstraintsWithFormat(format: "V:[v0]-20-|", views: activityView)
        
    }
    func setupNotification() {
        NotificationCenter.default.addObserver(forName: .menuSelected, object: nil, queue: nil) {(ntf) in
            if let index = ntf.object as? Int {
                self.curLoadingCount = 1
                let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
                SVProgressHUD.show()
                let deadlineTime = DispatchTime.now() + .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    switch index {
                    case 0: RoomAPI.shared.getPreviewMedias(deviceCode: deviceCode, roomId: self.roomId, mediaKind: "", catId: "", subId: "", findValue: self.searchText, loadIndex: 1, completion: {[weak self] (items) in
                        if let medias = items {
                            self?.medias = medias
                        }
                    })
                    case 1: RoomAPI.shared.getPreviewMediasRoomMember(deviceCode: deviceCode, mediaKind: "", findValue: self.searchText, loadIndex: self.curLoadingCount, completion: {[weak self] (items) in
                        if let medias = items {
                            self?.medias = medias
                        }
                    })
                    case 2: RoomAPI.shared.getPreviewMediasTotal(deviceCode: deviceCode, mediaKind: "", findValue: self.searchText, loadIndex: self.curLoadingCount, completion: {[weak self] (items) in
                        if let medias = items {
                            self?.medias = medias
                        }
                    })
                    default:break
                    }
                }
                
            }
        }
    }
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    //MARK:- UICollectionViewDatasource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mediaId, for: indexPath) as! MediaCell
        let media = medias[indexPath.item]
        cell.curMedia = media
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let row = CGFloat(Utility.calaulateCollectionItemInRow())
        let width = (view.frame.width - 24 - spacing*(row-1))/row
        let height = 4*width/3 + 70
        return CGSize(width: width, height: height)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        if (contentOffset - maximumOffset) > 20 {
            view.bringSubviewToFront(activityView)
        } else {
            view.bringSubviewToFront(collectionView!)
            view.bringSubviewToFront(menuBar)
        }
    }
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let preloadingTreashold = Int(medias.count)
        let threasholdReached = indexPath.item >= preloadingTreashold
        let reachedLastElement = indexPath.item == medias.count - 1
        if !isLoadingMore && (threasholdReached || reachedLastElement) {
            let deadlineTime = DispatchTime.now() + .seconds(1)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.loadMore()
            }
        }
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SVProgressHUD.show()
        let media = medias[indexPath.item]
        let vc = MediaDetailViewController(mediaId: media.mediaId, roomId: media.roomId)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func loadMore() {
        isLoadingMore = true
        activityView.startAnimating()
        DispatchQueue.global().async {
            self.curLoadingCount += 1
            let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
            RoomAPI.shared.getPreviewMedias(deviceCode: deviceCode, roomId: self.roomId, mediaKind: "", catId: "", subId: "", findValue: self.searchText, loadIndex: self.curLoadingCount) {[weak self] (items) in
                if let items = items {
                    if items.count == 0 {
                        DispatchQueue.main.async {
                            self?.infoAlert(message: "No more data.", duration: 2)
                        }
                    } else {
                        self?.medias.append(contentsOf: items)
                    }
                } else {
                    
                }
                
            }
        }
    }
}

@available(iOS 11.0, *)
extension MediaSearchListViewController {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
}




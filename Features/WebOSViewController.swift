//
//  WebOSViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 4/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import WebKit

class WebOSViewController: UIViewController, WKNavigationDelegate, AlertHandler {
    @IBOutlet weak var webKit: WKWebView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnNext: UIBarButtonItem!
    @IBOutlet weak var lblURL: UILabel!
    
    var path: String
    @objc init(urlPath: String, title: String? = " ") {
        path = urlPath
        super.init(nibName: nil, bundle: nil)
        self.title = title!
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webKit.configuration.allowsInlineMediaPlayback = true
        LightTheme.shared.applyNavigationBar(vc: self)
        DispatchQueue.main.async {
            // show status bar
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindow.Level.normal
            }
        }
        webKit.navigationDelegate = self
        webKit.uiDelegate = self
        btnBack.isEnabled = false
        btnNext.isEnabled = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close_black"), style: .done, target: self, action: #selector(done))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let url = URL(string: self.path) {
//                self.lblURL.text = url.path
//                if url.isFileURL {
//                    let fileName = NSString(string:url.lastPathComponent).deletingPathExtension
//                    let fileUrl = URL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: "pdf")!)
//                    self.webKit.loadFileURL(fileUrl, allowingReadAccessTo: fileUrl)
//                    let req = URLRequest(url: fileUrl)
//                    self.webKit.load(req)
//                } else {
                    let req = URLRequest(url: url)
                    self.webKit.load(req)
//                }
            } else {
                self.infoAlert(message: "Invalid url!", duration: 2)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    @objc func done() {
        SVProgressHUD.dismiss()
        if self == navigationController?.viewControllers[0] {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func navBack(_ sender: UIBarButtonItem) {
        if webKit.canGoBack {
            webKit.goBack()
        }
    }
    
    @IBAction func navNext(_ sender: UIBarButtonItem) {
        if webKit.canGoForward {
            webKit.goForward()
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show(withStatus: "Loading...")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        btnNext.isEnabled = webKit.canGoForward
        btnBack.isEnabled = webKit.canGoBack
        lblURL.text = " " + (webKit.url?.absoluteString)!
        SVProgressHUD.dismiss()
    }
}

extension WebOSViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.dismiss(animated: true, completion: {
                completionHandler()
            })
        }))
        present(alert, animated: true, completion: nil)
    }
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        print(message)
    }
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        return true
    }
}

//
//  YoutubeViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 24/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class YoutubeViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblViewCount: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblDislikeCount: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var playerView: PlayerView!
    var timer: Timer?
    
    var yt: YT
    init(yt: YT) {
        self.yt = yt
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        navigationController?.navigationBar.isHidden = (toInterfaceOrientation != .portrait)
        stackView.isHidden = (toInterfaceOrientation != .portrait)
        isFullScreen = (toInterfaceOrientation == .landscapeLeft || toInterfaceOrientation == .landscapeRight)
    }
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LightTheme.shared.applyNavigationBar(vc: self)
        if let room = DB.getRoom(id: yt.roomID) {
            title = room.name ?? ""
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.videoId = yt.videoID
        playerView.toggle = toggle
        //Info
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        
        lblTitle.text = yt.title
        lblViewCount.text = " \(formatter.string(from: NSNumber(value: yt.viewCount)) ?? "-") views"
        lblLikeCount.text = "👍 \(formatter.string(from: NSNumber(value: yt.likeCount)) ?? "")"
        lblDislikeCount.text = "👎 \(formatter.string(from: NSNumber(value: yt.dislikeCount)) ?? "")"
        
        titleView.layer.cornerRadius = 7
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addPlayerView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIView.animate(withDuration: 1) {
                let alpha = self.playerView.controlsView.alpha
                self.playerView.controlsView.alpha = alpha > 0 ? 0:1
            }
        }
    }
    
    @objc func done() {
        if let player = playerView, player.ytPlayerView.playerState == .playing {
            player.ytPlayerView.stopVideo()
        }
        dismiss(animated: true, completion: nil)
    }
    private func addPlayerView(){
        self.contentView.addSubview(playerView)
        playerView.pin(to: contentView)
        view.sendSubviewToBack(contentView)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 1) {
            let alpha = self.playerView.controlsView.alpha
            self.playerView.controlsView.alpha = alpha > 0 ? 0:1
        }
    }
    
    var isFullScreen = false
    func toggle() {
        isFullScreen = !isFullScreen
        if isFullScreen{
            let value = UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            titleView.alpha = 0
            stackView.alpha = 0
            navigationController?.navigationBar.isHidden = true
        }else{
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            titleView.alpha = 1
            stackView.alpha = 1
            navigationController?.navigationBar.isHidden = false
        }
    }
}

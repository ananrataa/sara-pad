//
//  MergeAccountViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 17/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class MergeAccountViewController: UIViewController, UITextFieldDelegate, AlertHandler {
    let isEng:Bool = Locale.preferredLanguages[0] == "en"
    @IBOutlet weak var lblMergeInfo: UILabel!
    @IBOutlet weak var txtFromEmail: UITextField!
    @IBOutlet weak var txtToEmail: UITextField!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblFromAccount: UILabel!
    @IBOutlet weak var lblToAccount: UILabel!
    
    var memberId: String
    
    init(id: String) {
        memberId = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TransparentTheme.shared.applyNavigationBar(vc: self)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        if UIDevice.modelName.contains("iPad") {
            let w = UIScreen.main.bounds.width
            leftConstraint.constant = (w - 315)/2
            rightConstraint.constant = (w - 315)/2
        }
        btnConfirm.layer.borderColor = UIColor(hexString: "#FB9826").cgColor
        btnConfirm.layer.borderWidth = 1
        btnConfirm.layer.cornerRadius = 19
        
        txtToEmail.text = ""
        //set Info
        lblMergeInfo.layer.cornerRadius = 7
        lblMergeInfo.layer.borderWidth = 0.5
        lblMergeInfo.layer.borderColor = lblMergeInfo.backgroundColor?.cgColor
        if let info = DBManager.selecttMemberMenuInfo() as? [String] {
            txtFromEmail.text = info[1]
            if isEng {
                self.title = "Account Merging"
                btnConfirm.setTitle("Start merging", for: .normal)
                lblFromAccount.text = "Current sign in email"
                lblToAccount.text = "Main account email to accept merging"
                lblMergeInfo.text = "\nYou can merge the membership of all current librries in '\(info[1])' to another account.\nCaution: Once you had confirmed the merging process, it cannot be cancelled!"
            } else {
                self.title = "การรวมบัญชีผู้ใช้"
                lblFromAccount.text = "อีเมล์ของบัญชีปัจจุบัน"
                lblToAccount.text = "อีเมล์ของบัญชีหลักที่ต้องการส่งไปรวม"
                lblMergeInfo.text = "\nคุณสามารถนำสิทธิ์การใช้ห้องสมุดทั้งหมดของบัญชีผู้ใช้ '\(info[1])' ไปรวมกับบัญชีผู้ใช้อื่นๆ ของคุณ\nโปรดระวัง การโอนสิทธิ์นี้ หากตอบยืนยันแล้วจะไม่สามารถยกเลิกได้\n"
            }
        }
        //UITextField
        txtFromEmail.setLeftImage(image: #imageLiteral(resourceName: "email"), tint: UIColor(hexString: "#FB9826"))
        txtToEmail.setLeftImage(image: #imageLiteral(resourceName: "email"), tint: UIColor(hexString: "#FB9826"))
    }

    @objc func done() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func confirmMerge(_ sender: Any) {
        view.endEditing(true)
        guard let txt = txtToEmail.text,
            Utility.validateEmailFormat(txt)
            else {
                lblError.text = String.localizedString(key: "EmailInValid")
                lblError.flash()
                btnConfirm.jitter()
                return
        }
        
        confirmAlert(title: String.localizedString(key: "warning"),
                     messgae: String.localizedString(key: "merge_warning"),
                     okTitle: "OK") {[weak self] (action) in
                        if action == .OK {
                            SVProgressHUD.show(withStatus: "Requesting...")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                self?.startMerging(email: txt)
                            })
                        }
        }
    }
    @objc func startMerging(email: String) {
        let mainEmail = txtFromEmail.text ?? ""
        MemberAPi.shared.mergeMember(from: memberId, to: email) {[weak self] (respond) in
            if let respond = respond {
                if respond.code == "200" {
                    let msg = (self?.isEng)! ?
                        "Merging success.\nWe have sent email to\n'\(mainEmail)'\nPlease check you mail."
                        : "การรวมบัญชีสำเร็จ\nระบบได้ส่งอีเมล์ไปยัง\n'\(mainEmail)'\nกรุณาเช็คเมล์ของคุณ"
                    self?.successAlert(message: msg, duration: 3, completion: {
                        self?.done()
                    })
                } else {
                    let msg = "Status: \(respond.code)\n\(respond.message)"
                    SVProgressHUD.showError(withStatus: msg)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

//
//  PdfViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/25/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit
import AVFoundation
import MediaPlayer
import SwiftyJSON
import RxSwift

var adsInterval: Int = 4
let inkAlpha: CGFloat = 0.85
@available(iOS 11.0, *)
class PdfViewController: UIViewController, TranslateViewDelegate, AlertHandler, PDFViewDelegate {
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var pdfThumbnailView: PDFThumbnailView!
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var menuView: UIViewX!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var drawingButton: UIButtonX!
    @IBOutlet weak var bgmButton: UIButtonX!
    @IBOutlet weak var translateButton: UIButtonX!
    @IBOutlet weak var readingButton: UIButtonX!
    @IBOutlet weak var bgmSV: UIView!
    @IBOutlet weak var pageCurlButton: UIButtonX!
    
    @IBOutlet weak var btnMusicPlay: UIButton!
    @IBOutlet weak var btnMusicStop: UIButton!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDrawing: UILabel!
    @IBOutlet weak var lblBGM: UILabel!
    @IBOutlet weak var lblTranslation: UILabel!
    @IBOutlet weak var lblBookReading: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    let disposeBag = DisposeBag()
    var bgmMode: BgmMode?
    var lastBool: Bool? //for BGM & speech translation toggle
    //bgmLocal Player
    var bgmTimer: Timer!
    var curTime: TimeInterval = 0
    let bgmLocalPlayer = MPMusicPlayerController.applicationQueuePlayer
    var isBgmPlaying = false
    //bgmCloud Player
    var bgmCloudPlayer:AVPlayer?
    var curBGMIndex:Int = 0
    var bgmCloudList:[String] = [String]()
    
    let tocSegmentedControl = UISegmentedControl(items: [#imageLiteral(resourceName: "Grid"), #imageLiteral(resourceName: "List"), #imageLiteral(resourceName: "Bookmark-N"), #imageLiteral(resourceName: "pen")])
    var lastOpenPageKey: String!
    var currentModifiedDate: Date!
    var bookMarBarButton:UIBarButtonItem!
    var curRegisterBanner: Int = 0
    var lastPageNo: Int = 0
    var currentPage: PDFPage! {
        didSet {
            if oldValue != currentPage {
                refreshBookMark(page: currentPage)
                
                //popup registration
                let pageNo = currentPage.pageNo
                guard pageNo > 0, pageNo % 5 == 0, lastPageNo != pageNo else {return}
                if !DBManager.isMember()   {
                    let popVC = FullScrrenViewController(index: curRegisterBanner)
                    curRegisterBanner += 1
                    if curRegisterBanner > 3 {
                        curRegisterBanner = 0
                    }
                    popVC.modalPresentationStyle = .overFullScreen
                    popVC.registerDidTap = {
                        self.navigationController?.navigationBar.alpha = 1
                        let mainSB = UIStoryboard(name: "Main", bundle: nil)
                        if let vc = mainSB.instantiateViewController(withIdentifier: "SignInVC") as? SignInVC {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.present(popVC, animated: true, completion: nil)
                        self.lastPageNo = pageNo
                    }
                }
            }
        }
    }
    var currentSelectedTag: Int?
    var currentAnnotationArray:[NSDictionary] = []
    var currentPdfBookMark:[PdfBookmark] = []
    var currentBookMarkArray:[Int] = []
    var currentBookNoteArray:[NSDictionary] = []
    var pdfSearchVC: PdfSearchViewController?
    var pdfThumbnailGridVC: ThumbnailGridViewController?
    var outlineVC: OutlineViewController?
    var bookmarkVC: BookmarkViewController?
    var inkBookmarkVC:InkBookmarkCollectionViewController?
    
    var pdfInfo: PdfAnnotationInfo
    //Advertise
    var totalPdfPages: Int = 0
    
    //Reading time
    var ntfResign: NSObjectProtocol?
    var ntfEnter: NSObjectProtocol?
    var pageStart: DispatchTime?
    var memberId: String = {
        var id = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            id = DBManager.selectMemberID()
        }
        return id
    }()
    
    @objc init(info: PdfAnnotationInfo) {
        self.pdfInfo = info
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeMenu()
        loadAnnotations()
        setupUI()
        setupPdf()
        setupMenu()
        setPdfNotification()
        setupInkFeature()
    }
    deinit {
        print("XXX: Deinit")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if lblDrawing.text == "" {
            lblDrawing.text = String.localizedString(key: "drawing")
            lblBGM.text = String.localizedString(key: "bgm")
            lblTranslation.text = String.localizedString(key: "translation")
            lblBookReading.text = String.localizedString(key: "book_reading")
            lblDrawing.sizeToFit()
            lblBGM.sizeToFit()
            lblTranslation.sizeToFit()
            lblBookReading.sizeToFit()
        }
        lblDrawing.frame.origin.x = drawingButton.frame.origin.x - lblDrawing.frame.width - 2
        lblBGM.frame.origin.x = bgmButton.frame.origin.x - lblBGM.frame.width - 2
        lblTranslation.frame.origin.x = translateButton.frame.origin.x - lblTranslation.frame.width - 2
        lblBookReading.frame.origin.x = readingButton.frame.origin.x - lblBookReading.frame.width - 2
    }
    func loadAnnotations() {
        //Test Android annotation
//        if let url = Bundle.main.url(forResource: "android_annotation", withExtension: "txt") {
//            do {
//                let jsonText = try String(contentsOf: url, encoding: .utf8)
//                let data = jsonText.data(using: String.Encoding.utf8, allowLossyConversion: false)!
//                let jsonObj = try JSON(data: data)
//                if let json = jsonObj["mEditorModelList_Android"].arrayObject as? [[String:Any]]  {
//                    (currentAnnotationArray, currentInkArray) = AnnotationModel.shared.createAndroidAnnotation(json: json)
//                }
//
//            } catch {
//                print("Error reading text file!")
//            }
//        }
        
        
        (currentAnnotationArray,currentBookMarkArray,currentBookNoteArray,currentInkArray) = FileDataManager.init().readAnnotationFromFile(readFileAtPath: pdfInfo.filePath)
        currentPdfBookMark = []
        for bk in currentBookNoteArray {
            if let page = Int((bk.allKeys[0] as? String) ?? ""), let note = bk.allValues[0] as? String {
                currentPdfBookMark.append(PdfBookmark(page: page, note: note))
            }
        }
    }
    
    func setupUI() {
        bgmSV.alpha = 0
        if let media = DB.getMedia(id: pdfInfo.mediaId), (media.mediaTitle ?? "").count > 0 {
            lblTitle.text = media.mediaTitle
        } else if let attributes = pdfInfo.pdfDocument.documentAttributes, let pdfTitle = attributes[PDFDocumentAttribute.titleAttribute] as? String {
            lblTitle.text = pdfTitle
        }
        
        //Right buttons
        bookMarBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Bookmark-N"), style: .plain, target: self, action: #selector(bookMarkTap(_:)))
        let brightnessButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Brightness"), style: .plain, target: self, action: #selector(brightness(_:)))
        let searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style: .plain, target: self, action: #selector(pdfSearch))
        navigationItem.rightBarButtonItems = [searchButton, brightnessButton, bookMarBarButton]
        //Left Buttons
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .plain, target: self, action: #selector(done))
        let tableOfContentsButton = UIBarButtonItem(image: #imageLiteral(resourceName: "List"), style: .plain, target: self, action: #selector(showTableOfContents(_:)))
        navigationItem.leftBarButtonItems = [backButton, tableOfContentsButton]

        //Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(pdfViewTouched(_:)))
        tapGesture.name = "pdf"
        pdfView.addGestureRecognizer(tapGesture)
        //Double Tap
//        let dblGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTapPdfView))
//        dblGesture.numberOfTapsRequired = 2
//        pdfView.addGestureRecognizer(dblGesture);
        
        
        let tapBlurViewGesture = UITapGestureRecognizer(target: self, action: #selector(menuTapped(_:)))
        blurView.addGestureRecognizer(tapBlurViewGesture)
        //Hide menu button
        self.menuButton.alpha = 0
        self.menuView.alpha = 0
        self.blurView.alpha = 0
        //Show thumbNail
        pdfThumbnailView.alpha = 1
        //lblPage
        lblPage.font = UIFont.boldSystemFont(ofSize: 17)
        lblPage.addBorder(width: 1, color: .clear, radius: 7)
    }
    
    //DoubleTap
    @objc func doubleTapPdfView() {
        let vc = WebOSViewController(urlPath: pdfInfo.pdfURL.absoluteString, title: self.title)
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: false) {
                
            }
        }
    }
    
    func setupPdf() {
        //Add ads
        adsInterval = pdfInfo.pdfAds.adX
        var ads = pdfInfo.pdfAds.adImages
        totalPdfPages = pdfInfo.pdfDocument.pageCount
        if adsInterval > 0 && ads.count > 0 {
            let n = adsInterval
            var count = 0
            for index in 1..<totalPdfPages {
                if index % n == 0 {
                    if ads.count <= 0 {
                        ads = pdfInfo.pdfAds.adImages
                    }
                    if let page = PDFPage(image: ads.removeFirst().adImage) {
                        pdfInfo.pdfDocument.insert(page, at: index + count)
                        count += 1
                    }
                }
            }
        }
        
        pdfView.document = pdfInfo.pdfDocument
        pdfView.autoScales = true
        pdfView.displayMode = .singlePage
        pdfView.displayDirection = .horizontal
        pdfView.usePageViewController(true, withViewOptions: [convertFromUIPageViewControllerOptionsKey(UIPageViewController.OptionsKey.interPageSpacing): 20])
        pdfView.delegate = self
        
        //GO TO LAST OPEN PAGE
        let lastOpenPageKey = pdfInfo.filePath
        let lastOpenActualPage = max(0, UserDefaults.standard.integer(forKey: lastOpenPageKey))
        let goToPage = SP.shared.convert2AdsPageIndex(actual: lastOpenActualPage)
        pdfView.go(to: (pdfInfo.pdfDocument.page(at: goToPage))!)
        currentPage = pdfView.document?.page(at: goToPage)
        currentModifiedDate = FileDataManager.init().lastModified(path: pdfInfo.filePath)
        
        pdfThumbnailView.layoutMode = .horizontal
        pdfThumbnailView.pdfView = pdfView
        pdfThumbnailView.backgroundColor = UIColor(hexString: "#ebebeb")
        refreshPageAnnotation(currentPage)
    }
    
    func pdfViewWillClick(onLink sender: PDFView, with url: URL) {
        print(url.absoluteString)
        let vc = WebOSViewController(urlPath: url.absoluteString)
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
    func setPdfNotification() {
        //Page changed
        NotificationCenter.default.addObserver(forName: .PDFViewPageChanged, object: nil, queue: nil) {[weak self] (_) in
            if let page = self?.pdfView.currentPage {
                //refresh page annotation
                if let lastPage = self?.currentPage {
                    if lastPage != page {
                        self?.refreshPageAnnotation(page)
                        //set reading time for lastPage
                        self?.saveReadingData(page: lastPage)
                    }
                } else {
                    self?.refreshPageAnnotation(page)
                    self?.pageStart = DispatchTime.now()
                }
                self?.currentPage = page
                //reset pageStart
                self?.pageStart = DispatchTime.now()
            }
        }
        
        //toc
        tocSegmentedControl.selectedSegmentIndex = 0
        tocSegmentedControl.addTarget(self, action: #selector(tocSelected(_:)), for: .valueChanged)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        toggleNavigationBar()
        if pageStart == nil {
            pageStart = DispatchTime.now()
        }
        
        //add observer
        //Reading time
        self.ntfResign = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("resign active")
                if let page = self?.currentPage {
                    self?.saveReadingData(page: page)
                    self?.pageStart = nil
                }
            }
        }
        self.ntfEnter = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("enter active")
                if self?.pageStart == nil {
                    self?.pageStart = DispatchTime.now()
                }
            }
        }

        SVProgressHUD.dismiss()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //remover observer
        if let ntf = ntfResign {
            NotificationCenter.default.removeObserver(ntf)
        }
        if let ntf = ntfEnter {
            NotificationCenter.default.removeObserver(ntf)
        }
        
        if let page = currentPage {
            saveReadingData(page: page)
        }
        pageStart = nil
    }
    @IBAction func pageCurl(_ sender: Any) {
        let curIndex = pdfInfo.pdfDocument.index(for: currentPage)
        let vc = PdfPageViewController(pdfDoc: pdfInfo.pdfDocument, pdfTitle: self.title ?? "", curIndex: curIndex)
        vc.pageChanged = pageCurlChanged
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: false, completion: nil)
    }
    func pageCurlChanged(index: Int) {
        if let page = pdfInfo.pdfDocument.page(at: index) {
            pdfView.go(to: page)
        }
    }
    @objc func done() {
        //set timer for currentPage
        if let page = currentPage {
            self.saveReadingData(page: page)
        }

        stopBgmCloudMusic()
        stopBgmLocalMusic()
        SVProgressHUD.dismiss()
        if let page = pdfView.currentPage {
            //actualPage
            let pageNo = SP.shared.actualPageNumber(page: page)
            UserDefaults.standard.set(pageNo, forKey: pdfInfo.filePath)
            UserDefaults.standard.synchronize()
        }
        
        FileDataManager.init().saveAnnotationFileAndUploadFile(Highlight: currentAnnotationArray,
                                                               BookMark: currentBookMarkArray,
                                                               Note: currentBookNoteArray,
                                                               Ink: currentInkArray,
                                                               SaveFileToPath: pdfInfo.filePath,
                                                               url: pdfInfo.uploadPath,
                                                               allowToSync: pdfInfo.canUpload,
                                                               lastModifiedDate: currentModifiedDate!)
        
        if pdfInfo.removeFileOnExit {
            if let documentUrl = pdfInfo.pdfDocument.documentURL {
                FileDataManager.init().removeFileAtPath(PdfFile:documentUrl,AnnotationFile: pdfInfo.filePath)
            }
        }
        
        //Remove all notifications
        NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: nil)
        

        dismiss(animated: true, completion: {
            if InternetConnection.isConnectedToNetwork() {
                DB.postReadingData()
            }
        })
    }


    @objc func pdfViewTouched(_ sender: UITapGestureRecognizer) {
        SVProgressHUD.dismiss()
        if UIMenuController.shared.isMenuVisible {
            UIMenuController.shared.setMenuVisible(false, animated: true)
            pdfView.currentSelection = nil
            setupMenu()
            return
        }
        //First is Ads page
        if let page = currentPage, SP.shared.isAdPage(page: page) {
            guard InternetConnection.isConnectedToNetwork() else {return}
            let n = (page.pageNo + 1)/(adsInterval + 1)
            let adsCount = pdfInfo.pdfAds.adImages.count
            let nRem = n % adsCount
            let index = nRem == 0 ? (adsCount-1) : (nRem-1)
            if pdfInfo.pdfAds.adImages.count > index {
                let ad = pdfInfo.pdfAds.adImages[index]
                if URL(string: ad.adLink) != nil {
                    let vc = WebOSViewController(urlPath: ad.adLink, title: ad.adName)
                    let nav = UINavigationController(rootViewController: vc)
                    self.present(nav, animated: true, completion: nil)
                } else {
                    return
                }
            }
        }
        //check if user tap on annotation area
        let touchLocation: CGPoint = sender.location(in: sender.view)
        var isAnnotationFound = false
        if sender.state == .ended {
            if let page = currentPage {
                let p = pdfView.convert(touchLocation, to: page)
                let pageNo = SP.shared.actualPageNumber(page: page)
                let pageAnnotations = currentAnnotationArray.filter{getAnnotationPage(jsonDict: $0) == pageNo}
                if let jsDict = Annotation.init(pdfInfo.pdfDocument).getSelectedAnnotations(at: p, in: pageAnnotations) {
                    if let dict = jsDict["annotationList"] as? NSDictionary, let tag = dict["tag"] as? Int {
                        currentSelectedTag = tag
                        colorMenuActivate(at: CGRect(origin: touchLocation, size: CGSize(width:10, height:10)))
                    }
                    isAnnotationFound = true
                }
            }
        }
        //Check Ink Annotation
        if !isAnnotationFound {
            if let page = currentPage {
                let p = pdfView.convert(touchLocation, to: page)
                if let inkInfo = getInkPathInfo(at: page, nearestPoint: p) {
                    currentSelectedPathData = inkInfo
                    colorMenuActivate(at: CGRect(origin: touchLocation, size: CGSize(width:10, height:10)))
                }
            }
        }
        //toggle UI
        toggleNavigationBar()
        
        //PDFSelection line selection
//        if let page = currentPage {
//            let p = pdfView.convert(touchLocation, to: page)
//            pdfView.currentSelection = page.selectionForLine(at: p)
//        }
    }

    @objc func toggleNavigationBar() {
        guard let page = pdfView.currentPage else {
            return
        }
        let isAd = SP.shared.isAdPage(page: page)
        if let navigationController = navigationController {
            if navigationController.navigationBar.alpha > 0 {
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {[unowned self] in
                    navigationController.navigationBar.alpha = 0
                    self.pdfThumbnailView.alpha = 0
                    
                    self.menuButton.alpha = isAd ? 0 : 1
                    self.menuView.alpha = isAd ? 0 : 1
                    self.view.setNeedsLayout()
                }) {[unowned self] (animationComplete) in
                    if self.isBgmPlaying {
                        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseIn, animations: {
                            self.bgmSV.alpha = 1
                        })
                    }
                    UIView.animate(withDuration: 1, animations: {
                        self.lblTitle.alpha = isAd ? 0 : 1
                    })
                }
            } else {
                UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0, options: .curveEaseIn, animations: {[unowned self] in
                    navigationController.navigationBar.alpha = 1
                    self.pdfThumbnailView.alpha = 1
                    
                    self.closeMenu()
                    self.menuButton.alpha = 0
                    self.menuView.alpha = 0
                    self.view.setNeedsLayout()
                }) {[unowned self] (animationComplete) in
                    if self.isBgmPlaying {
                        DispatchQueue.main.async {
                            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                                self.bgmSV.alpha = 0
                            })
                        }
                    }
                    UIView.animate(withDuration: 1, animations: {
                        self.lblTitle.alpha = 0
                    })
                }
            }
            lblPage.alpha = pdfThumbnailView.alpha
            blurView.alpha = 0
        }
    }
    //MARK:- Ink Annotation
    var currentInkArray:[NSDictionary] = []
    var curInkTag = 0
    var pathData:[(page: Int, tag: Int, color: String, width: CGFloat, points:[CGPoint])] = [] {
        didSet {
            if let btn = undoButton {
                btn.isEnabled = pathData.count > 0
            }
        }
    }
    var redoPathData: [(page: Int, tag: Int, color: String, width: CGFloat, points:[CGPoint])] = [] {
        didSet {
            if let btn = redoButton {
                btn.isEnabled = redoPathData.count > 0
            }
        }
    }
    var pageLayerShapes:[[CAShapeLayer]] = []
    var redoLayerShapes:[[CAShapeLayer]] = []
    var pathPoints:[CGPoint] = []
    var pathShapes:[CAShapeLayer] = []
    var drawPath: UIBezierPath?
    var lastPoint: CGPoint = .zero
    var pathInkAnnotations:[PDFAnnotation] = [] //keep annotation for each line
    var penButtons:[UIButton] = []
    var colorButtons:[UIButton] = []
    var undoButton: UIButton!
    var redoButton: UIButton!
    let brushSize:[CGFloat] = [0, 2.5, 5, 10]
    var currentSelectedPathData: NSDictionary?
    let inkColor: [UIColor] = [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.231372549, green: 0.4862745098, blue: 0.9490196078, alpha: 1), #colorLiteral(red: 0.4745098039, green: 0.8235294118, blue: 0.4549019608, alpha: 1), #colorLiteral(red: 0.9568627451, green: 0.8078431373, blue: 0.3294117647, alpha: 1), #colorLiteral(red: 0.8666666667, green: 0.2588235294, blue: 0.2745098039, alpha: 1)]//"#FFFFFF","#000000","#3B7CF2","#79D274","#F4CE54","#DD4246"
    let dfPenColor = "df_color"
    let dfPenSelected = "df_width"
    let dfSubColor = "df_subColor"
    let dfPenSize = "df_pen_size"
    lazy var brushColor:UIColor = {
        if let i = UserDefaults.standard.value(forKey: dfPenColor) as? Int, i >= 0 {
            if i == 0 {
                if let hexColor = UserDefaults.standard.string(forKey: dfSubColor) {
                    return UIColor(hexString: hexColor)
                }
            }
            return inkColor[min(5, i)]
        } else {
            //set initial color = black
            UserDefaults.standard.set(1, forKey: dfPenColor)
            UserDefaults.standard.synchronize()
        }
        return inkColor[1]
    }()
    lazy var brushWidth:CGFloat = {
        if let i = UserDefaults.standard.value(forKey: dfPenSelected) as? Int, i >= 0 {
            if i == 0 {
                return (UserDefaults.standard.value(forKey: dfPenSize) as? CGFloat) ?? 0.5
            }
            return brushSize[i]
        }
        UserDefaults.standard.set(1, forKey: dfPenSelected)
        UserDefaults.standard.synchronize()
        return brushSize[1]
    }()
    let inkMenuView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.darkGray // UIColor(hexString: "#E8E8E8")
        v.tintColor = UIColor(hexString: "#c8c8c8")
        v.isHidden = true
        return v
    }()
}

//MARK:- Floating menu
@available(iOS 11.0, *)
extension PdfViewController {
    @IBAction func menuTapped(_ sender: UIButton) {
        SVProgressHUD.dismiss()
        UIView.animate(withDuration: 0.3, animations: {[unowned self] in
            if self.menuView.transform == .identity {
                self.closeMenu()
            } else {
                self.blurView.alpha = 0.95
                self.menuView.transform = .identity
                self.menuButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
                self.menuButton.setImage(#imageLiteral(resourceName: "x"), for: .normal)
                self.menuButton.tintColor = .darkGray
                self.menuButton.backgroundColor = UIColor.white.withAlphaComponent(0.85)
                self.view.setNeedsLayout()
            }
        })
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.2, initialSpringVelocity: 0, options: [], animations: {[unowned self] in
            if self.menuView.transform == .identity {
                self.drawingButton.transform = .identity
                self.bgmButton.transform = .identity
                self.translateButton.transform = .identity
                self.readingButton.transform = .identity
                self.pageCurlButton.transform = .identity
                self.view.setNeedsLayout()
            }
        })
    }
    @objc func closeMenu() {
        //bgm Memnu
        menuView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.menuButton.transform = .identity
        self.menuButton.setImage(#imageLiteral(resourceName: "dot"), for: .normal)
        self.menuButton.tintColor = .white
        self.menuButton.backgroundColor = .black
        self.blurView.alpha = 0
        drawingButton.transform = CGAffineTransform(translationX: 0, y: 13)
        bgmButton.transform = CGAffineTransform(translationX: 13, y: 13)
        translateButton.transform = CGAffineTransform(translationX: 13, y: 13)
        readingButton.transform = CGAffineTransform(translationX: 13, y: 0)
        pageCurlButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        view.setNeedsLayout()
    }
    
    @IBAction func menuSelected(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {[weak self] in
            self?.menuTapped((self?.menuButton)!)
        }) {[unowned self] (_) in
            switch sender.tag {
            case 1: self.inkManuTapped()
            case 2: self.loadBGM()
            case 3:
                SVProgressHUD.show(withStatus: "Translating...")
                self.perform(#selector(self.pageTranslate), with: nil, afterDelay: 0.1)
            case 4:
                self.bookReading()
            case 5: self.pageCurl(sender)
            default: break
            }
        }
    }
    
    func loadBGM() {
        stopBgmCloudMusic()
        stopBgmLocalMusic()
        let vc = BGMViewController(roomId: pdfInfo.roomId)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func pageTranslate() {
        if let page = currentPage {
            let vc = TranslateViewController(pdfView: pdfView, curPage: page)
            vc.delegate = self
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true){[unowned self] in
                self.pdfView.currentSelection = nil
            }
        }
    }
    func bookReading() {
        guard InternetConnection.isConnectedToNetwork() else {
            self.infoAlert(message: String.localizedString(key: "NoInternet"), duration: 2)
            return
        }
        if let page = self.pdfView.currentPage {
            let mediaId = self.pdfInfo.mediaId
            SVProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {[weak self] in
                MediaModel.shared.createMediaVoicesObservable(mediaId: mediaId)
                    .subscribe(onNext: { (items) in
                        //get maximum voices list
                        if let item = items.sorted(by: {$0.mediaVoices.count < $1.mediaVoices.count}).last,
                            let p2 = self?.pdfInfo.pdfDocument.lastPage {
                            let mediaList = SPSelectedMediaVoices(source: 0, startPage: page, endPage: p2, voices: item.mediaVoices, language: "")
                            let vc = StartBookReadingViewController(mediaId: mediaId, mediaList: mediaList)
                            vc.pageChanged = self?.bookReadingPageChanged(page:info:)
                            let nav = UINavigationController(rootViewController: vc)
                            self?.present(nav, animated: true, completion: nil)
                        } else {
                            if let pdf = self?.pdfInfo.pdfDocument,
                                let p1 = self?.pdfView.currentPage ?? pdf.firstPage,
                                let p2 = pdf.lastPage {
                                let voices = SPSelectedMediaVoices(source: 1, startPage: p1, endPage: p2, voices: [], language: "th")
                                let vc = RobotReadingViewController(mediaId: mediaId, pdfDoc: pdf, mediaInfo: voices)
                                vc.pageChanged = self?.bookReadingPageChanged(page:info:)
                                let nav = UINavigationController(rootViewController: vc)
                                self?.present(nav, animated: true, completion: nil)
                            }
                        }
                    }).disposed(by: (self?.disposeBag)!)
            }
        }
    }
    
    func bookReadingPageChanged(page: PDFPage, info: SPSelectedMediaVoices) {
        if let page = pdfInfo.pdfDocument.page(at: page.pageNo) {
            pdfView.go(to: page)
        }
    }
}
//MARK:- BMG Play cloud music
@available(iOS 11.0, *)
extension PdfViewController: BGMViewDelegate {
    func bgmViewDidSelectedCloundSongs(playLists: [SPBGMPlayList]) {
        bgmMode = .Cloud
        bgmSV.alpha = 0
        bgmSV.transform = .identity
        if playLists.count > 0 {
            bgmCloudList = []
            for pl in playLists {
                for track in pl.tracks {
                    bgmCloudList.append(track.trackUrl)
                }
            }
            curBGMIndex = 0
            prepareToPlayBgmCloudMusic(bgmCloudList[0])
        }
    }
    private func prepareToPlayBgmCloudMusic(_ strUrl:String){
        stopBgmCloudMusic()
        stopBgmLocalMusic()
        
        guard let url = URL.init(string: strUrl)
            else {
                print("Bgm Cloud url error")
                return
        }
        let playerItem:AVPlayerItem = AVPlayerItem(url: url)
        bgmCloudPlayer = AVPlayer(playerItem: playerItem)
        bgmCloudPlayer?.rate = 1.0
        bgmCloudPlayer?.play()
        isBgmPlaying = true
        //show remaining music duration
        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        bgmCloudPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) {
            [unowned self] time in
            if self.bgmCloudPlayer?.currentItem?.status == .readyToPlay {
                if let totalCMTime = self.bgmCloudPlayer?.currentItem?.duration,
                    let curCMTime = self.bgmCloudPlayer?.currentItem?.currentTime() {
                    self.lblDuration.text = (CMTimeGetSeconds(totalCMTime - curCMTime)).stringFromTimeInterval()
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: nil)
        {[unowned self] (_) in
            NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
            
            self.curBGMIndex += 1
            if self.bgmCloudList.count > 0 && self.curBGMIndex < self.bgmCloudList.count {
                self.prepareToPlayBgmCloudMusic(self.bgmCloudList[self.curBGMIndex])
            } else {
                self.bgmCloudPlayer = nil
                self.stopBGM(self.btnMusicStop)
            }
        }
        
    }

    
    //MARK:- Device Songs
    func bgmViewDidSelectedDeviceSongs(songs: [DeviceSongItem]) {
        bgmMode = .Device
        bgmSV.alpha = 0
        bgmSV.transform = .identity
        bgmCloudPlayer = nil
        stopBgmCloudMusic()
        stopBgmLocalMusic()
        var mediaSongs:[MPMediaItem] = []
        if songs.count > 0 {
            for song in MPMediaQuery.songs().items! {
                if songs.contains(where: {$0.persistentID == song.persistentID}){
                    mediaSongs.append(song)
                }
            }
            //sorting
            mediaSongs.sort { (s1, s2) -> Bool in
                if let a1 = s1.artist, let a2 = s2.artist, a1 != a2 {
                    return a1 < a2
                } else if let t1 = s1.albumTitle, let t2 = s2.albumTitle, t1 != t2 {
                    return t1 < t2
                } else if s1.albumTrackNumber != s2.albumTrackNumber {
                    return s1.albumTrackNumber < s2.albumTrackNumber
                } else {
                    return s1.title ?? "" < s2.title ?? ""
                }
            }
            bgmLocalPlayer.beginGeneratingPlaybackNotifications()
            let coll = MPMediaItemCollection(items: mediaSongs)
            bgmLocalPlayer.setQueue(with: coll)
            bgmLocalPlayer.nowPlayingItem = coll.items[0]
            bgmLocalPlayer.prepareToPlay()
            bgmLocalPlayer.play()
            isBgmPlaying = true
            
            //Notification when last music stop
            NotificationCenter.default.addObserver(forName: .MPMusicPlayerControllerPlaybackStateDidChange, object: nil, queue: nil) {[weak self] (ntf) in
                if let player = ntf.object as? MPMusicPlayerApplicationController {
                    if player.nowPlayingItem == coll.items.last &&
                        player.playbackState == MPMusicPlaybackState.stopped {
                        self?.stopBGM(self?.btnMusicStop)
                        self?.bgmTimer.invalidate()
                    }
                }
            }
            //Notification when music item changed
            NotificationCenter.default.addObserver(forName: .MPMusicPlayerControllerNowPlayingItemDidChange, object: nil, queue: nil) {[weak self] (ntf) in
                self?.curTime = 0
                self?.setDeviceTimer()
            }
        }
    }
    func setDeviceTimer() {
        //show timer
        if let timer = self.bgmTimer {
            timer.invalidate()
        }
        self.bgmTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {[weak self] (_) in
            if let item = self?.bgmLocalPlayer.nowPlayingItem {
                self?.curTime += 1
                self?.lblDuration.text = max(0, (item.playbackDuration - (self?.curTime)!)).stringFromTimeInterval()
            }
        })
    }
    //MARK:- BGMusic controller
    @IBAction func pauseBGM(_ sender: Any) {
        if bgmMode == .Device {
            if bgmLocalPlayer.playbackState == MPMusicPlaybackState.playing {
                bgmLocalPlayer.pause()
                btnMusicPlay.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
                bgmTimer.invalidate()
            }
            else  if bgmLocalPlayer.playbackState == MPMusicPlaybackState.paused {
                bgmLocalPlayer.play()
                btnMusicPlay.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
                setDeviceTimer()
            }
        } else {
            if bgmCloudPlayer != nil && (bgmCloudPlayer?.isPlaying)! {
                bgmCloudPlayer?.pause()
                bgmCloudPlayer?.rate = 0.0
                btnMusicPlay.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
            }
            else if bgmCloudPlayer != nil && !(bgmCloudPlayer?.isPlaying)!{
                bgmCloudPlayer?.rate = 1.0
                bgmCloudPlayer?.play()
                btnMusicPlay.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
            }
        }
    }
    @IBAction func stopBGM(_ sender: UIButton?) {
        stopBgmCloudMusic()
        stopBgmLocalMusic()
        UIView.animate(withDuration: 0.5, delay: 0.3, options: [], animations: {[weak self] in
            self?.bgmSV.transform = CGAffineTransform(translationX: -(20 + (self?.bgmSV.frame.width)!), y: 0)
        })
    }
    private func stopBgmCloudMusic() {
        if bgmCloudPlayer != nil && (bgmCloudPlayer?.isPlaying)! {
            bgmCloudPlayer?.pause()
            bgmCloudPlayer?.rate = 0.0
            bgmCloudPlayer?.replaceCurrentItem(with: nil)
            isBgmPlaying = false
        }
    }
    
    private func stopBgmLocalMusic(){
        if bgmLocalPlayer.playbackState == MPMusicPlaybackState.playing ||
            bgmLocalPlayer.playbackState == MPMusicPlaybackState.paused {
            bgmLocalPlayer.stop()
            isBgmPlaying = false
        }
    }
    //MARK:- Translate speech status changed
    func translateSpeechStatus(speaking isSpeak: Bool) {
        guard bgmMode != nil else {
            return
        }
        if lastBool == nil {
            pauseBGM(btnMusicPlay)
        } else if lastBool != isSpeak {
            pauseBGM(btnMusicPlay)
        }
        lastBool = isSpeak
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIPageViewControllerOptionsKey(_ input: UIPageViewController.OptionsKey) -> String {
	return input.rawValue
}

//MARK:- ReadingData
extension PdfViewController {
    func saveReadingData(page: PDFPage) {
        //actual page number
        let actualPageNumber = SP.shared.actualPageNumber(page: page) + 1
        //compute time reading in seconds
        if let start = self.pageStart {
            let seconds = Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
            print("Page-\(actualPageNumber): \(seconds) seconds")
            
            let data = ReadingData()
            data.memberId = memberId
            data.mediaId = pdfInfo.mediaId
            data.date = Date().getDateString(with: "yyyyMMdd")
            data.page = actualPageNumber//index 1 base
            data.duration = Double(Int(1000*seconds))/1000
            if data.duration > 0 {
                DB.saveReadingData(data: data)
            }
        }
    }
}

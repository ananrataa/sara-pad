//
//  PdfTableOFContents.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/27/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit

@available(iOS 11.0, *)
extension PdfViewController: ThumbnailGridViewControllerDelegate, OutlineViewControllerDelegate, BookmarkViewControllerDelegate, InkBookmarkViewControllerDelegate {

    @objc func showTableOfContents(_ sender: UIBarButtonItem) {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .plain, target: self, action: #selector(done))
        let resumeBarButton = UIBarButtonItem(title: NSLocalizedString("Resume", comment: ""), style: .plain, target: self, action: #selector(resume))
        navigationItem.titleView = tocSegmentedControl
        navigationItem.leftBarButtonItems = [backButton]
        navigationItem.rightBarButtonItems = [resumeBarButton]
        pdfThumbnailView.alpha = 0
        for g in pdfView.gestureRecognizers! {
            if g.name == "pdf" {
                pdfView.removeGestureRecognizer(g)
                break
            }
        }
        tocSelected(tocSegmentedControl)
    }
    
    @objc func resume() {
        SVProgressHUD.dismiss()
        if let vc = pdfThumbnailGridVC {
            let curIndex = IndexPath(item: currentPage.pageNo, section: 0)
            let cell = vc.collectionView?.cellForItem(at: curIndex)
            cell?.layer.borderWidth = 0
        }
        if let v = view.viewWithTag(111) {
            UIView.animate(withDuration: 0.3, animations: {
                v.alpha = 0
            }) { (_) in
                v.removeFromSuperview()
            }
        }
        navigationItem.titleView = nil
        setupUI()
    }
    
    @objc func tocSelected(_ sender: UISegmentedControl) {
        SVProgressHUD.dismiss()
        if let v = view.viewWithTag(111) {
            v.removeFromSuperview()
        }
        switch sender.selectedSegmentIndex {
        case 0:loadThumbnailGridVC()
        case 1:loadOutlineVC()
        case 2:loadBookmarkVC()
        case 3:loadInkBookmark()
        default: break
        }
    }
    func insertView(_ v: UIView) {
        v.tag = 111
        v.alpha = 1
        v.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(v)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        v.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                       leading: view.leadingAnchor,
                       bottom: view.bottomAnchor,
                       trailing: view.trailingAnchor)
    }
    //MARK:- ThumbnailGridViewController
    func loadThumbnailGridVC() {
        if pdfThumbnailGridVC == nil {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            if let thumbnailVC = sb.instantiateViewController(withIdentifier: "ThumbnailGridViewController") as? ThumbnailGridViewController {
                pdfThumbnailGridVC = thumbnailVC
                pdfThumbnailGridVC?.pdfDocument = pdfInfo.pdfDocument
                pdfThumbnailGridVC?.delegate = self
            }
        }
        if let vc = pdfThumbnailGridVC {
            insertView(vc.view)
            let curIndex = IndexPath(item: currentPage.pageNo, section: 0)
            vc.curIndexPath = curIndex
            let cell = vc.collectionView?.cellForItem(at: curIndex)
            cell?.layer.borderColor = UIColor.red.cgColor
            cell?.layer.borderWidth = 3
            vc.collectionView?.selectItem(at: curIndex, animated: true, scrollPosition: .centeredVertically)
        }
    }
    func thumbnailGridViewController(_ thumbnailGridViewController: ThumbnailGridViewController, didSelectPage page: PDFPage) {
        resume()
        pdfView.go(to: page)
        
    }
    //MARK:- OutlineViewController
    func loadOutlineVC() {
        if outlineVC == nil {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            if let vc = sb.instantiateViewController(withIdentifier: "OutlineViewController") as? OutlineViewController {
                outlineVC = vc
                outlineVC?.pdfDocument = pdfInfo.pdfDocument
                outlineVC?.delegate = self
            }
        }
        if let vc = outlineVC {
            insertView(vc.view)
            if vc.toc.count == 0 {
                infoAlert(message: "This book has no Table of Contents!", duration: 1.5)
            }
        }
    }
    
    func outlineViewController(_ outlineViewController: OutlineViewController, didSelectOutlineAt destination: PDFDestination) {
        resume()
        pdfView.go(to: destination)
    }
    
    //MARK:- BookmarkViewController
    func loadBookmarkVC() {
        bookmarkVC = nil
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: "BookmarkViewController") as? BookmarkViewController {
            bookmarkVC = vc
            bookmarkVC?.pdfDocument = pdfInfo.pdfDocument
            bookmarkVC?.annotationFilePath = pdfInfo.filePath
            bookmarkVC?.delegate = self
        }
        if let vc = bookmarkVC {
            insertView(vc.view)
        }
    }
    
    func bookmarkViewController(_ bookmarkViewController: BookmarkViewController, didSelectPage page: PDFPage) {
        resume()
        pdfView.go(to: page)
        refreshBookMark(page: page)
    }
    
    //MARK:- Bookmark for Drawing
    func loadInkBookmark() {
        let pages = currentInkArray.map({getAnnotationPage(jsonDict: $0)})
        let pageDict = Dictionary(grouping: pages, by: {$0})
        let inkPages = (Array(pageDict.keys)).sorted()
        if inkBookmarkVC == nil {
            inkBookmarkVC = InkBookmarkCollectionViewController(document: pdfInfo.pdfDocument, pages: inkPages)
            inkBookmarkVC?.delegate = self
        } else {
            inkBookmarkVC?.inkPages = inkPages
        }
        if let vc = inkBookmarkVC {
            insertView(vc.view)
        }
    }
    func inkBookmarkViewController(_ viewController: InkBookmarkCollectionViewController, didSelectPage page: PDFPage) {
        resume()
        pdfView.go(to: page)
    }
}

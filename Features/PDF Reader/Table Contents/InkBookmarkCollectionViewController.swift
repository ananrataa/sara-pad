//
//  InkBookmarkCollectionViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 7/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

private let reuseIdentifier = "Cell"
private let downloadQueue = DispatchQueue(label: "com.kishikawakatsumi.pdfviewer.thumbnail")

protocol InkBookmarkViewControllerDelegate: class {
    func inkBookmarkViewController(_ viewController: InkBookmarkCollectionViewController, didSelectPage page: PDFPage)
}
@available(iOS 11.0, *)
class InkBookmarkCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    weak var delegate: InkBookmarkViewControllerDelegate?
    
    let thumbnailCache = NSCache<NSNumber, UIImage>()
    var cellSize: CGSize {
        if let collectionView = collectionView {
            var width = collectionView.frame.width
            var height = collectionView.frame.height
            if width > height {
                swap(&width, &height)
            }
            width = (width - 20 * 4) / 3
            height = width * 1.5
            return CGSize(width: width, height: height)
        }
        return CGSize(width: 100, height: 150)
    }
    var pdfDocument: PDFDocument
    var inkPages:[Int] {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    init(document: PDFDocument, pages:[Int]) {
        self.pdfDocument = document
        self.inkPages = pages
        let fl = UICollectionViewFlowLayout()
        fl.minimumLineSpacing = 10
        fl.minimumInteritemSpacing = 10
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView?.contentInset = .init(top: 20, left: 20, bottom: 20, right: 20)
        collectionView?.register(UINib(nibName: "ThumbnailGridCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        collectionView?.backgroundColor = .gray
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inkPages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ThumbnailGridCell
        
        let pageNumber = inkPages[indexPath.item]
        if let page = pdfDocument.page(at: SP.shared.convert2AdsPageIndex(actual: pageNumber)) {
            cell.pageNumber = pageNumber + 1
            let key = NSNumber(value: pageNumber)
            if let thumbnail = thumbnailCache.object(forKey: key) {
                cell.image = thumbnail
            } else {
                let size = cellSize
                downloadQueue.async {[unowned self] in
                    let thumbnail = page.thumbnail(of: size, for: .cropBox)
                    self.thumbnailCache.setObject(thumbnail, forKey: key)
                    if cell.pageNumber == pageNumber {
                        DispatchQueue.main.async {
                            cell.image = thumbnail
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageNo = inkPages[indexPath.item]
        if let page = pdfDocument.page(at: SP.shared.convert2AdsPageIndex(actual: pageNo)) {
            delegate?.inkBookmarkViewController(self, didSelectPage: page)
        }
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }

}

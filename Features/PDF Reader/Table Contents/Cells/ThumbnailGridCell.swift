//
//  ThumbnailGridCell.swift
//  BookReader
//
//  Created by Kishikawa Katsumi on 2017/07/03.
//  Copyright © 2017 Kishikawa Katsumi. All rights reserved.
//

import UIKit

class ThumbnailGridCell: UICollectionViewCell {
    override var isHighlighted: Bool {
        didSet {
            imageView.alpha = isHighlighted ? 0.8 : 1
        }
    }
    var image: UIImage? = nil {
        didSet {
            imageView.image = image
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {[weak self] in
                self?.activityIndicatorView.stopAnimating()
            }
        }
    }
    var pageNumber = 0 {
        didSet {
            if let label = pageNumberLabel {
                label.text = pageNumber > 0 ?  String(pageNumber) : ""
            }
        }
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageNumberLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.shadowOffset = .init(width: 3, height: 3)
        contentView.layer.shadowOpacity = 0.35
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        contentView.layer.borderWidth = 0
    }
}

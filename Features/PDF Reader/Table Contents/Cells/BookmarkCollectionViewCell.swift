//
//  BookmarkCellCollectionViewCell.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/29/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class BookmarkCollectionViewCell: UICollectionViewCell {
    @IBOutlet var pdfImageView: UIImageView!
    @IBOutlet var lblPageNo: UILabel!
    @IBOutlet var lblNote: UILabel!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    var curPageNumber: Int? {
        didSet {
            if let n = curPageNumber {
                lblPageNo.text = "\(n + 1)"
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {[weak self] in
                    self?.activityIndicatorView.stopAnimating()
                }
            }
        }
    }
    
    override func awakeFromNib() {
        lblNote.addBorder(width: 0.1, color: .clear, radius: 4)
        contentView.layer.shadowOffset = .init(width: 3, height: 3)
        contentView.layer.shadowOpacity = 0.35
    }
}

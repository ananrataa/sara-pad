//
//  BookmarkViewController.swift
//  BookReader
//
//  Created by Kishikawa Katsumi on 2017/07/03.
//  Copyright © 2017 Kishikawa Katsumi. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
class BookmarkViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AlertHandler {
    let cellId = "book_cell"
    var pdfDocument: PDFDocument?
    var annotationFilePath:String?
    var pdfBookmarks:[PdfBookmark] = []
    weak var delegate: BookmarkViewControllerDelegate?

    let thumbnailCache = NSCache<NSNumber, UIImage>()
    private let downloadQueue = DispatchQueue(label: "com.kishikawakatsumi.pdfviewer.thumbnail")

    var cellSize: CGSize {
        if let collectionView = collectionView {
            var width = collectionView.frame.width
            var height = collectionView.frame.height
            if width > height {
                swap(&width, &height)
            }
            width = (width - 20 * 4) / 3
            height = width * 1.5 + 24
            return CGSize(width: width, height: height)
        }
        return CGSize(width: 100, height: 150)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let backgroundView = UIView()
        backgroundView.backgroundColor = .gray
        collectionView?.backgroundView = backgroundView

        collectionView?.register(UINib(nibName: String(describing: BookmarkCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: cellId)

        refreshData()
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pdfBookmarks.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BookmarkCollectionViewCell
        
        let bm = pdfBookmarks[indexPath.item]
        let pageNumber = bm.page
        if let page = pdfDocument?.page(at: SP.shared.convert2AdsPageIndex(actual: bm.page)) {
            cell.curPageNumber = bm.page
            cell.lblNote.text = bm.note
            
            let key = NSNumber(value: pageNumber)
            if let thumbnail = thumbnailCache.object(forKey: key) {
                cell.pdfImageView.image = thumbnail
            } else {
                let size = cellSize
                downloadQueue.async {[unowned self] in
                    let thumbnail = page.thumbnail(of: size, for: .cropBox)
                    self.thumbnailCache.setObject(thumbnail, forKey: key)
                    if cell.curPageNumber == pageNumber {
                        DispatchQueue.main.async {
                            cell.pdfImageView.image = thumbnail
                        }
                    }
                }
            }
        }
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bm = pdfBookmarks[indexPath.item]
        if let page = pdfDocument?.page(at: SP.shared.convert2AdsPageIndex(actual: bm.page)) {
            delegate?.bookmarkViewController(self, didSelectPage: page)
        }
        collectionView.deselectItem(at: indexPath, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }

    private func refreshData() {
        guard annotationFilePath != nil else {
            return
        }
        let (_,bookmarks,bookNotes,_) = FileDataManager.init().readAnnotationFromFile(readFileAtPath: annotationFilePath!)
        collectionView?.reloadData()
        if bookmarks.count == 0 {
            infoAlert(message: "This book has no Bookmarks!", duration: 1.5)
        } else {
            pdfBookmarks = bookNotes.map{PdfBookmark(page:Int(($0.allKeys[0] as? String) ?? "0")!, note:($0.allValues[0] as? String) ?? "")}
            pdfBookmarks.sort(by: {$0.page < $1.page})
        }
    }
}

protocol BookmarkViewControllerDelegate: class {
    func bookmarkViewController(_ bookmarkViewController: BookmarkViewController, didSelectPage page: PDFPage)
}

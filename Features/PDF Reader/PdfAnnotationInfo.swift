//
//  PdfAnnotation.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/26/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit

@available(iOS 11.0, *)
class PdfAnnotationInfo: NSObject {
    var roomId: String
    var mediaId: String
    var pdfDocument: PDFDocument
    var filePath: String
    var uploadPath: String
    var removeFileOnExit: Bool
    var canUpload: Bool
    var isPreview: Bool
    var pdfURL: URL
    var password: String
    var pdfAds: ADPdf
    @objc init(roomId: String, mediaId: String, pdfDocument: PDFDocument, filePath: String, uploadPath: String, removeFileOnExit: Bool, canUpload: Bool, isPreview: Bool, pdfURL: URL, password: String, ads: ADPdf) {
        self.roomId = roomId
        self.mediaId = mediaId
        self.pdfDocument = pdfDocument
        self.filePath = filePath
        self.uploadPath = uploadPath
        self.removeFileOnExit = removeFileOnExit
        self.canUpload = canUpload
        self.isPreview = isPreview
        self.pdfURL = pdfURL
        self.password = password
        self.pdfAds = ads
    }
}

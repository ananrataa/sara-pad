//
//  BookMarkNoteViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/28/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

protocol BookMarkNoteViewDelegate: class {
    func bookmarkNoteViewDidFinished(bookmarks: [PdfBookmark]?)
}
class BookMarkNoteViewController: UIViewController, UITextViewDelegate {
    weak var delegate: BookMarkNoteViewDelegate?
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var lblTextLength: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerWidthConstraint: NSLayoutConstraint!
    
    var isLoaded = false
    var isNewBookmark:Bool!
    var page:PDFPage
    var bookmarks:[PdfBookmark]
    var navBarHeight:CGFloat
    init(page:PDFPage, bookmarks:[PdfBookmark], barHeight:CGFloat) {
        self.page = page
        self.bookmarks = bookmarks
        self.navBarHeight = barHeight
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let pageNo = SP.shared.actualPageNumber(page: page)
        isNewBookmark = (bookmarks.filter{$0.page == pageNo}).count == 0
        if isNewBookmark {
            removeButton.setTitle(String.localizedString(key: "Cancel"), for: .normal)
            lblPage.text = "Page \(SP.shared.adjustedPageNumber(page: page))"
            lblTextLength.text = "0/250"
        } else {
            removeButton.setTitle(String.localizedString(key: "Remove"), for: .normal)
            if let bm = (bookmarks.filter{$0.page == pageNo}).first {
                textView.text = bm.note
                lblPage.text = "Page \(SP.shared.adjustedPageNumber(page: page))"
                lblTextLength.text = "\(bm.note.count)/250"
            }
        }
        
        saveButton.setTitle(String.localizedString(key: "Save"), for: .normal)
        textView.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !isLoaded {
            if UIDevice.current.userInterfaceIdiom == .pad {
                containerWidthConstraint.constant = -0.5*view.frame.width
            }
            topConstraint.constant = navBarHeight
            containerView.layer.cornerRadius = 6
            containerView.layer.shadowOpacity = 0.25
            containerView.layer.shadowOffset = .init(width: 7, height: 7)
            containerView.layer.shadowRadius = 5
            isLoaded = true
        }
    }
    @IBAction func save(_ sender: Any) {
        let pageNo = SP.shared.actualPageNumber(page: page)
        if isNewBookmark {
            bookmarks.append(PdfBookmark(page: pageNo, note: textView.text))
        } else {
            if let bm = (bookmarks.filter{$0.page == pageNo}).first,
                let index = bookmarks.firstIndex(where: {$0.page == bm.page}) {
                let newBookMark = PdfBookmark(page: bm.page, note: textView.text)
                bookmarks[index] = newBookMark
            }
        }
        dismiss(animated: true){
            self.delegate?.bookmarkNoteViewDidFinished(bookmarks: self.bookmarks)
        }
    }
    
    @IBAction func remove(_ sender: Any) {
        if isNewBookmark {
            delegate?.bookmarkNoteViewDidFinished(bookmarks: nil)
        } else {
            let pageNo = SP.shared.actualPageNumber(page: page)
            if let bm = (bookmarks.filter{$0.page == pageNo}).first,
                let index = bookmarks.firstIndex(where: {$0.page == bm.page}) {
                bookmarks.remove(at: index)
                delegate?.bookmarkNoteViewDidFinished(bookmarks: self.bookmarks)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        let maxChars = 250
        if let text = textView.text, text.count > maxChars {
            let range = text.index(text.startIndex, offsetBy: maxChars)
            textView.text = String(text[..<range])
        }
        lblTextLength.text = "\(textView.text.count)/\(maxChars)"
        lblTextLength.textColor = textView.text.count < maxChars ? .black : .red
    }

}

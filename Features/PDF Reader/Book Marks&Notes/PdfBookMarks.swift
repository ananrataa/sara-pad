//
//  PdfBookMarks.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/28/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit

struct PdfBookmark {
    let page: Int
    let note: String
}

@available(iOS 11.0, *)
extension PdfViewController: BookMarkNoteViewDelegate {
    func bookmarkNoteViewDidFinished(bookmarks: [PdfBookmark]?) {
        if let page = currentPage, let bm = bookmarks {
            currentPdfBookMark = bm
            refreshBookMark(page: page)
            //save to local file
            currentBookMarkArray = currentPdfBookMark.map{$0.page}
            currentBookNoteArray = currentPdfBookMark.map{["\($0.page)":$0.note]}
            FileDataManager.init().saveAnnotationFile(Highlight: currentAnnotationArray, BookMark: currentBookMarkArray, Note: currentBookNoteArray, Ink: currentInkArray, SaveFileToPath: pdfInfo.filePath)
        }
    }
    
    
    @objc func bookMarkTap(_ sender: UIBarButtonItem) {
        SVProgressHUD.dismiss()
        guard
            let page = currentPage
            else {return}
        if SP.shared.isAdPage(page: page) {
            if Locale.preferredLanguages[0] == "en" {
                SVProgressHUD.showInfo(withStatus: "Cannot bookmark this page!")
            } else {
                SVProgressHUD.showInfo(withStatus: "ไม่สามารถเขียนบันทึก ในหน้านี้!")
            }
            return
        }
        let navBarHeight:CGFloat = navigationController?.navigationBar.frame.height ?? 44
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let vc = BookMarkNoteViewController(page:page, bookmarks: currentPdfBookMark, barHeight: navBarHeight + statusBarHeight)
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func refreshBookMark(page: PDFPage) {
        let pageNo = SP.shared.actualPageNumber(page: page)
        if let bm = currentPdfBookMark.first(where: {$0.page == pageNo}) {
            infoAlert(message: "Note: Page \(bm.page+1)\n\(bm.note)", duration: 1.5)
            bookMarBarButton.image = #imageLiteral(resourceName: "Bookmark-P")
            bookMarBarButton.tintColor = .red
        } else {
            SVProgressHUD.dismiss()
            bookMarBarButton.image =  #imageLiteral(resourceName: "Bookmark-N")
            bookMarBarButton.tintColor = view.tintColor
        }
    }
    
}

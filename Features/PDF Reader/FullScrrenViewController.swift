//
//  FullScrrenViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 25/2/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class FullScrrenViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var labelFree: UILabelX!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var registerDidTap: (()->Void)?
    var index: Int
    let banners: [UIImage] = [#imageLiteral(resourceName: "add_sara+pad1.png"), #imageLiteral(resourceName: "add_sara+pad2.png"), #imageLiteral(resourceName: "add_sara+pad3.png"), #imageLiteral(resourceName: "add_sara+pad4.png")]
    init(index: Int) {
        self.index = index
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        guard index >= 0, index < banners.count else {
            return
        }
    }

    override func viewDidLayoutSubviews() {
        labelFree.layer.cornerRadius = labelFree.frame.height/2
        if pageControl.numberOfPages < 4 {
            loadAds()
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: Any) {
        dismiss(animated: true) {
            if let register = self.registerDidTap {
                register()
            }
        }
    }
    
    func loadAds() {
        var rect = scrollView.bounds
        for image in banners {
            let imageView = UIImageView(image: image)
            imageView.frame = rect
            imageView.contentMode = .scaleAspectFit
            scrollView.addSubview(imageView)
            rect.origin.x += rect.width
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(banners.count)*rect.width, height: rect.width)
        pageControl.numberOfPages = banners.count
        
        //scroll to page
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let p = CGPoint(x: self.scrollView.frame.size.width*CGFloat(self.index), y: 0)
            self.pageControl.currentPage = self.index
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollView.contentOffset = p
            })
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        pageControl.currentPage = Int(page)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        curMode = pageControl.currentPage
//        tabBar.selectedItem = tabBar.items?[curMode]
//        refreshPages()
    }
}

//
//  PdfSearch.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/27/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit

@available(iOS 11.0, *)
extension PdfViewController: UIPopoverPresentationControllerDelegate, SearchViewControllerDelegate  {
    func searchViewController(_ searchViewController: PdfSearchViewController, didSelectSearchResult selection: PDFSelection) {
        selection.color = .yellow
        pdfView.currentSelection = selection
        pdfView.go(to: selection)
    }
    
    @objc func pdfSearch() {
        if let vc = pdfSearchVC {
            let nav = UINavigationController(rootViewController: vc)
            present(nav, animated: true, completion: nil)
        } else {
            let vc = PdfSearchViewController(pdfDocument: pdfView.document!)
            vc.delegate = self
            let nav = UINavigationController(rootViewController: vc)
            present(nav, animated: true, completion: nil)
            pdfSearchVC = vc
        }
    }
    
    @objc func brightness(_ sender: UIBarButtonItem) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewController = sb.instantiateViewController(withIdentifier: "AppearanceViewController") as? AppearanceViewController {
            viewController.modalPresentationStyle = .popover
            viewController.preferredContentSize = CGSize(width: 300, height: 44)
            viewController.popoverPresentationController?.barButtonItem = sender
            viewController.popoverPresentationController?.delegate = self
            viewController.popoverPresentationController?.permittedArrowDirections = .up
            present(viewController, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

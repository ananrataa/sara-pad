//
//  PdfHighlight.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/27/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import PDFKit
import MenuItemKit

enum AnnotationType: String {
    case highlight = "highlight"
    case underline = "underline"
    case strikeOut = "strikeOut"
}
@available(iOS 11.0, *)
extension PdfViewController {
    func setupMenu() {
        let menu = UIMenuController.shared
        menu.arrowDirection = .default
        let highlightMenu = UIMenuItem(title: "hilight", image: #imageLiteral(resourceName: "highlight25")) {[unowned self] (_) in
            self.setAnnotation(type: .highlight)
        }
        let underlineMenu = UIMenuItem(title: "underline", image: #imageLiteral(resourceName: "underline25")) {[unowned self] (_) in
            self.setAnnotation(type: .underline)
        }
        let strikeOutMenu = UIMenuItem(title: "strike", image: #imageLiteral(resourceName: "strike25")) {[unowned self] (_) in
            self.setAnnotation(type: .strikeOut)
        }
        let translateMenu = UIMenuItem(title: "translate", image: #imageLiteral(resourceName: "translate25")) {[unowned self] (_) in
            SVProgressHUD.show(withStatus: "Translating...")
            self.perform(#selector(self.translate), with: nil, afterDelay: 0.2)
        }
        let copyMenu = UIMenuItem(title: "copy", image: #imageLiteral(resourceName: "copy25x25")) {[unowned self] (_) in
            if let text = self.pdfView.currentSelection?.string {
                UIPasteboard.general.string = text
                self.pdfView.currentSelection = nil
            }
        }
        
        menu.menuItems = [highlightMenu, underlineMenu, strikeOutMenu, translateMenu, copyMenu]
        UIMenuController.installTo(responder: pdfView) { (action, default) -> Bool in
            return UIMenuItem.isMenuItemKitSelector(action)
        }
    }
    func colorMenuActivate(at rect: CGRect) {
        let eraseMenu = UIMenuItem(title: "erase", image: #imageLiteral(resourceName: "erase20")) {[unowned self] (_) in
            self.eraseAnnotation()
        }
        let redMenu = UIMenuItem(title: "red", image: #imageLiteral(resourceName: "c5")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#FE0000"))
        }
        let greenMenu = UIMenuItem(title: "green", image: #imageLiteral(resourceName: "c3")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#79d274"))
        }
        let blueMenu = UIMenuItem(title: "blue", image: #imageLiteral(resourceName: "c2")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#3b7cf2"))
        }
        let orangeMenu = UIMenuItem(title: "orange", image: #imageLiteral(resourceName: "orange")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#ffa500"))
        }
        let yellowMenu = UIMenuItem(title: "yellow", image: #imageLiteral(resourceName: "c4")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#E6FF01"))
        }
        let purpleMenu = UIMenuItem(title: "purple", image: #imageLiteral(resourceName: "purple")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#CD31FF"))
        }
        let pinkMenu = UIMenuItem(title: "pink", image: #imageLiteral(resourceName: "pink")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#ff00ff"))
        }
        let aquaMenu = UIMenuItem(title: "aqua", image: #imageLiteral(resourceName: "aqua")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#00ffff"))
        }
        let limeMenu = UIMenuItem(title: "lime", image: #imageLiteral(resourceName: "lime")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#00ff00"))
        }
        let blackMenu = UIMenuItem(title: "black", image: #imageLiteral(resourceName: "c1")) {[unowned self] (_) in
            self.setAnnotationColor(UIColor(hexString: "#000000"))
        }
        
        
        let menu = UIMenuController.shared
        menu.menuItems = [eraseMenu, redMenu, greenMenu, orangeMenu, blueMenu, yellowMenu, purpleMenu, pinkMenu, aquaMenu, limeMenu, blackMenu]
        menu.setTargetRect(rect, in: pdfView)
        UIMenuController.installTo(responder: pdfView) { (action, default) -> Bool in
            return UIMenuItem.isMenuItemKitSelector(action)
        }
        menu.setMenuVisible(true, animated: true)
    }
    
    func eraseAnnotation() {
        if let page = currentPage  {
            if let tag = currentSelectedTag {
                //update data & save
                let pageNo = SP.shared.actualPageNumber(page: page)
                let filteredAnnotation = currentAnnotationArray.filter{(getAnnotationPage(jsonDict: $0) == pageNo && getAnnotationTag(jsonDict: $0) != tag) || getAnnotationPage(jsonDict: $0) != pageNo}
                currentAnnotationArray = filteredAnnotation
            } else if let inkInfo = currentSelectedPathData {
                if let inkAnn = (currentInkArray.filter{getAnnotationTag(jsonDict: $0) == getAnnotationTag(jsonDict: inkInfo)}).first,
                    let index = currentInkArray.firstIndex(of: inkAnn) {
                    currentInkArray.remove(at: index)
                }
                
            }
            
            //refresh page annotation
            refreshPageAnnotation(page)
            //save file to device
            FileDataManager.init().saveAnnotationFile(Highlight: currentAnnotationArray,
                                                      BookMark: currentBookMarkArray,
                                                      Note: currentBookNoteArray,
                                                      Ink: currentInkArray,
                                                      SaveFileToPath: pdfInfo.filePath)
            setupMenu()
        }
        currentSelectedTag = nil
        currentSelectedPathData = nil
    }
    func setAnnotation(type: AnnotationType) {
        if let selections = pdfView.currentSelection?.selectionsByLine(), let page = currentPage {
            let tags = currentAnnotationArray.map{getAnnotationTag(jsonDict: $0)}
            let tag = (tags.max() ?? 0) + 1
            var annotationArray:[NSDictionary] = []
            switch type {
            case .highlight:
                annotationArray = Annotation.init(pdfInfo.pdfDocument).highlight(selections, page: page, tag: tag)
            case .underline:
                annotationArray = Annotation.init(pdfInfo.pdfDocument).underline(selections, page: page, tag: tag)
            case .strikeOut:
                annotationArray = Annotation.init(pdfInfo.pdfDocument).strikeOut(selections, page: page, tag: tag)
            }
            currentAnnotationArray += annotationArray
            //clear selection
            pdfView.currentSelection = nil
            //save file to device
            FileDataManager.init().saveAnnotationFile(Highlight: currentAnnotationArray,
                                                      BookMark: currentBookMarkArray,
                                                      Note: currentBookNoteArray,
                                                      Ink: currentInkArray,
                                                      SaveFileToPath: pdfInfo.filePath)
        }
    }
    func setAnnotationColor(_ color: UIColor) {
        UserDefaults.standard.set(color.hexCode, forKey: "ColorPicker")
        UserDefaults.standard.synchronize()
        guard let page = currentPage else {return}
        if let tag = currentSelectedTag  {
            //update data & save
            let pageNo = SP.shared.actualPageNumber(page: page)
            var seledtedAnnotations = currentAnnotationArray.filter{getAnnotationPage(jsonDict: $0) == pageNo && getAnnotationTag(jsonDict: $0) == tag}
            let notSelectedAnnotations = currentAnnotationArray.filter{(getAnnotationPage(jsonDict: $0) == pageNo && getAnnotationTag(jsonDict: $0) != tag) || getAnnotationPage(jsonDict: $0) != pageNo}

            for jsDict in seledtedAnnotations {
                if let annotationDict = jsDict["annotationList"] as? NSDictionary {
                    let newData = NSMutableDictionary(dictionary: annotationDict)
                    newData["color"] = color.hexCode
                    let newJson = NSMutableDictionary(dictionary: jsDict)
                    newJson["annotationList"] = newData
                    if let index = seledtedAnnotations.firstIndex(of: jsDict) {
                        seledtedAnnotations[index] = newJson
                    }
                }
            }
            currentAnnotationArray = notSelectedAnnotations + seledtedAnnotations
        } else if let inkInfo = currentSelectedPathData {
            if let inkAnn = (currentInkArray.filter{getAnnotationTag(jsonDict: $0) == getAnnotationTag(jsonDict: inkInfo)}).first, let index = currentInkArray.firstIndex(of: inkInfo) {
                //set new color
                if let annotationDict = inkAnn["annotationList"] as? NSDictionary {
                    let newData = NSMutableDictionary(dictionary: annotationDict)
                    newData["color"] = color.hexCode
                    let newJson = NSMutableDictionary(dictionary: inkAnn)
                    newJson["annotationList"] = newData
                    currentInkArray[index] = newJson
                }
            }
        }
        //refresh Page
        refreshPageAnnotation(page)
        //save file to device
        FileDataManager.init().saveAnnotationFile(Highlight: currentAnnotationArray,
                                                  BookMark: currentBookMarkArray,
                                                  Note: currentBookNoteArray,
                                                  Ink: currentInkArray,
                                                  SaveFileToPath: pdfInfo.filePath)
        setupMenu()
        currentSelectedTag = nil
        currentSelectedPathData = nil
    }
    func refreshPageAnnotation(_ page: PDFPage) {
        for (_,ann) in page.annotations.enumerated().reversed() {
            page.removeAnnotation(ann)
        }
        //refresh lblPage
        if SP.shared.isAdPage(page: page) {
            lblPage.text = ""
            menuButton.alpha = 0
            menuView.alpha = 0
        } else {
            Annotation.init(pdfInfo.pdfDocument).setAnnotation(currentAnnotationArray: currentAnnotationArray, pdfView: pdfView)
            pathData = Annotation.init(pdfInfo.pdfDocument).setInkAnnotation(jsonDict: currentInkArray, pdfView: pdfView)
            lblPage.text = "  \(adjustedPageNumber(page: page)) of \(totalPdfPages)  "
            lblPage.addBorder(width: 1, color: .darkGray, radius: 4)
            menuButton.alpha = pdfThumbnailView.alpha == 0 ? 1 : 0
            menuView.alpha = pdfThumbnailView.alpha == 0 ? 1 : 0
        }
    }
    func getAnnotationPage(jsonDict: NSDictionary) -> Int {
        if let page = jsonDict["pageNumber"] as? Int{
            return page
        }
        return 0
    }
    func getAnnotationTag(jsonDict: NSDictionary) -> Int {
        if let dict = jsonDict["annotationList"] as? NSDictionary, let tag = dict["tag"] as? Int {
            return tag
        }
        return 0
    }
    
    func getAnnotationType(jsonDict: NSDictionary) -> String {
        if let dict = jsonDict["annotationList"] as? NSDictionary, let type = dict["type"] as? String {
            return type
        }
        return ""
    }
    
    //MARK:- Translation
    @objc func translate() {
        let vc = TranslateViewController(pdfView: pdfView, curPage: nil)
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
    //MARK:- calculate Pageno
    func adjustedPageNumber(page: PDFPage) -> Int {
        guard page.pageNo > 0 else {
            return 1
        }
        guard adsInterval > 0 else {
            return page.pageNo + 1
        }
        let n = adsInterval
        let x = page.pageNo + 1
        let n1 = x % (n+1)
        let n2 = (x - n1)/(n+1)
        return n2*n + n1
    }
    
}

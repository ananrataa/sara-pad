//
//  ReadingTime.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 13/3/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

extension PdfViewController {
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("Pause Reading")
    }
    
}

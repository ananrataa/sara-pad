//
//  InkAnnotation.swift
//  SaraPadReader
//
//  Created by Anan Ratanasethakul on 9/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
//MARK:- Setup UI
extension PdfViewController: ColorPickerViewDelegate, PenSizeViewDelegate {
    func setupInkFeature() {
        //inkMenu shadow
        menuButton.layer.cornerRadius = menuButton.bounds.height/2
        menuButton.layer.shadowOpacity = 0.25
        menuButton.layer.shadowOffset = .init(width: 3, height: 3)
        
        
        var deviceSafeAreaBottom:CGFloat = 0
        if let appDel = UIApplication.shared.delegate as? AppDelegate {
            deviceSafeAreaBottom = appDel.window?.safeAreaInsets.bottom ?? 0
        }
        view.addSubview(inkMenuView)
        inkMenuView.anchor(top: view.bottomAnchor,
                           leading: view.leadingAnchor,
                           bottom: nil,
                           trailing: view.trailingAnchor,
                           size: .init(width: 0, height: 90 + deviceSafeAreaBottom))
        //Pen StackView
        penButtons = [
            UIButton(image: #imageLiteral(resourceName: "line_size"), target: self, action: #selector(selectPen(_:))),
            UIButton(image: #imageLiteral(resourceName: "pen1"), target: self, action: #selector(selectPen(_:))),
            UIButton(image: #imageLiteral(resourceName: "pen2"), target: self, action: #selector(selectPen(_:))),
            UIButton(image: #imageLiteral(resourceName: "pen3"), target: self, action: #selector(selectPen(_:)))]
        
        let svPen = UIStackView(arrangedSubviews: penButtons)
        svPen.axis = .horizontal
        svPen.spacing = 6
        svPen.distribution = .equalSpacing
        for btn in penButtons {
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.widthAnchor.constraint(equalToConstant: 30).isActive = true
            btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
            btn.layer.cornerRadius = 15
        }
        
        //Color StackView
        colorButtons = [
            UIButton(image: #imageLiteral(resourceName: "color_wheel"), target: self, action: #selector(colorSelector(_:))),
            UIButton(image: #imageLiteral(resourceName: "c1"), target: self, action: #selector(selectColor(_:))),
            UIButton(image: #imageLiteral(resourceName: "c2"), target: self, action: #selector(selectColor(_:))),
            UIButton(image: #imageLiteral(resourceName: "c3"), target: self, action: #selector(selectColor(_:))),
            UIButton(image: #imageLiteral(resourceName: "c4"), target: self, action: #selector(selectColor(_:))),
            UIButton(image: #imageLiteral(resourceName: "c5"), target: self, action: #selector(selectColor(_:)))]
        let svColor = UIStackView(arrangedSubviews: colorButtons)
        svColor.axis = .horizontal
        svColor.spacing = 6
        svColor.distribution = .equalSpacing
        //Make buttons rounded
        for v in colorButtons {
            v.translatesAutoresizingMaskIntoConstraints = false
            v.widthAnchor.constraint(equalToConstant: 30).isActive = true
            v.heightAnchor.constraint(equalToConstant: 30).isActive = true
            v.addBorder(width: 1, color: .clear, radius: 15)
        }
        
        //Main StackView
        let sv = UIStackView(arrangedSubviews: [svPen, svColor])
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .horizontal
        sv.distribution = .fillProportionally
        sv.spacing = 12
        //Put main StackView in inkMenuView
        inkMenuView.addSubview(sv)
        sv.centerXAnchor.constraint(equalTo: inkMenuView.centerXAnchor).isActive = true
        inkMenuView.addConstraintsWithFormat(format: "V:|-52-[v0(30)]", views: sv)
        
        //Done button
        let btnDone = UIButton(title: "Done", target: self, action: #selector(inkDone))
        btnDone.setTitleColor(inkMenuView.tintColor, for: .normal)
        inkMenuView.addSubview(btnDone)
        inkMenuView.addConstraintsWithFormat(format: "H:[v0]-8-|", views: btnDone)
        inkMenuView.addConstraintsWithFormat(format: "V:|-8-[v0(30)]", views: btnDone)
        
        //ClearButton
        let btnClear = UIButton(title: "Clear", target: self, action: #selector(clearPageInkAnnotation))
        btnClear.setTitleColor(inkMenuView.tintColor, for: .normal)
        inkMenuView.addSubview(btnClear)
        inkMenuView.addConstraintsWithFormat(format: "H:|-8-[v0]", views: btnClear)
        inkMenuView.addConstraintsWithFormat(format: "V:|-8-[v0(30)]", views: btnClear)
        
        //undo, redo buttons
        undoButton = UIButton(image: #imageLiteral(resourceName: "undo"), target: self, action: #selector(undoDrawPath))
        redoButton = UIButton(image: #imageLiteral(resourceName: "redo"), target: self, action: #selector(redoAnnotation))
        let sv2 = UIStackView(arrangedSubviews: [undoButton, redoButton])
        sv2.translatesAutoresizingMaskIntoConstraints = false
        sv2.axis = .horizontal
        sv2.spacing = 12
        inkMenuView.addSubview(sv2)
        sv2.centerYAnchor.constraint(equalTo: btnClear.centerYAnchor).isActive = true
        sv2.centerXAnchor.constraint(equalTo: inkMenuView.centerXAnchor).isActive = true
        
        view.bringSubviewToFront(inkMenuView)
        refreshButton()
    }
    func refreshButton() {
        //color buttons
        let i = UserDefaults.standard.integer(forKey: dfPenColor)
        for (index,btn) in colorButtons.enumerated() {
            if index == 0 && i == 0 {
                btn.layer.borderColor = brushColor.cgColor
            } else {
                btn.layer.borderColor = index == i ? inkColor[i].cgColor : UIColor.clear.cgColor
            }
        }
        
        //pen buttons
        let j = UserDefaults.standard.integer(forKey: dfPenSelected)
        for (index,btn) in penButtons.enumerated() {
            btn.tintColor = index == j ? brushColor : inkMenuView.tintColor
            btn.transform = index == j ? CGAffineTransform(translationX: 0, y: -10) : .identity
        }
    }
}

//MARK:- Activate/Deactivate ink annotation
@available(iOS 11.0, *)
extension PdfViewController {
    @objc func inkManuTapped() {
        SVProgressHUD.dismiss()
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.inkMenuView.isHidden = false
            self?.pdfThumbnailView.isHidden = true
        }) {[unowned self] (_) in
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.inkMenuView.transform = CGAffineTransform(translationX: 0, y: -self.inkMenuView.frame.height)
            }, completion: { (_) in
                self.refreshButton()
            })
        }
        curInkTag = (currentInkArray.map{getAnnotationTag(jsonDict: $0)}).max() ?? 0
        pageLayerShapes = []
        redoPathData = []
        redoLayerShapes = []
        pdfView.isUserInteractionEnabled = false
        addPanGestureRecognition()
    }
    @objc func inkDone() {
        UIView.animate(withDuration: 0.3, animations: {[weak self] in
            self?.inkMenuView.transform = .identity
        }) {[unowned self] (_) in
            self.inkMenuView.isHidden = true
            self.pdfThumbnailView.isHidden = false
            self.pdfView.isUserInteractionEnabled = true
            
            //prepare Json dict by merging current + new data
            if let page = self.currentPage {
                let pdfDoc = self.pdfInfo.pdfDocument
                let pageNo = SP.shared.actualPageNumber(page: page)
                //prepare Json page
                let jsonPage = Annotation.init(pdfDoc).inkPath2Json(self.pathData)
                let filteredArray = self.currentInkArray.filter{self.getAnnotationPage(jsonDict: $0) != pageNo}
                self.currentInkArray = filteredArray + jsonPage
                //save file
                FileDataManager.init().saveAnnotationFile(Highlight: self.currentAnnotationArray,
                                                          BookMark: self.currentBookMarkArray,
                                                          Note: self.currentBookNoteArray,
                                                          Ink: self.currentInkArray,
                                                          SaveFileToPath: self.pdfInfo.filePath)
                self.refreshPageAnnotation(page)
            }
            
            //clear new data
            self.drawPath?.removeAllPoints()
            for ly in self.pdfView.layer.sublayers! {
                if ly.name == "ink" {
                    ly.removeFromSuperlayer()
                }
            }
            self.redoPathData = []
        }
        view.gestureRecognizers?.forEach({ (g) in
            if let name = g.name, name.contains("ink") {
                view.removeGestureRecognizer(g)
            }
        })
    }
}
//MARK:- Actions
@available(iOS 11.0, *)
extension PdfViewController {
    @objc func clearPageInkAnnotation() {
        if let page = currentPage {
            //clear shapeLayer
            for ly in pdfView.layer.sublayers! {
                if ly.name == "ink" {
                    ly.removeFromSuperlayer()
                }
            }
            //remove all currentInkArray of currentPage
            let pageNo = SP.shared.actualPageNumber(page: page)
            let excludedCurrentPageInkArray = currentInkArray.filter{getAnnotationPage(jsonDict: $0) != pageNo}
            currentInkArray = excludedCurrentPageInkArray
            pathData.removeAll()
            pageLayerShapes.removeAll()
            redoPathData.removeAll()
            redoLayerShapes.removeAll()
            refreshPageAnnotation(page)
        }
    }
    @objc func undoDrawPath() {
        if pageLayerShapes.count > 0 {
            //ShapeLayer
            let shapeLayers = pageLayerShapes.removeLast()
            redoLayerShapes.append(shapeLayers)
            for sh in shapeLayers {
                sh.removeFromSuperlayer()
            }
            //PathData
            let shapeData = pathData.removeLast()
            redoPathData.append(shapeData)
        }
    }
    
    @objc func redoAnnotation() {
        //ShapeLayer
        if redoLayerShapes.count > 0 {
            let shapeLayers = redoLayerShapes.removeLast()
            for sh in shapeLayers {
                pdfView.layer.addSublayer(sh)
            }
            pageLayerShapes.append(shapeLayers)
            
            //PathData
            let redoData = redoPathData.removeLast()
            pathData.append(redoData)
        }
    }
    
    @objc func selectPen(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1, animations: {
            sender.transform = CGAffineTransform(translationX: 0, y: -10)
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
                sender.transform = .identity
            }, completion: {[unowned self] (_) in
                let i = self.penButtons.firstIndex(of: sender) ?? 0
                if i == 0 {
                    DispatchQueue.main.async {
                        self.brushWidth = (UserDefaults.standard.value(forKey: self.dfPenSize) as? CGFloat) ?? 0.5
                        let vc = PenSizeViewController(color: self.brushColor, size: self.brushWidth)
                        vc.delegate = self
                        vc.modalPresentationStyle = .popover
                        vc.preferredContentSize = CGSize(width: 345, height: 126)
                        vc.popoverPresentationController?.sourceRect = sender.frame
                        vc.popoverPresentationController?.delegate = self
                        vc.popoverPresentationController?.permittedArrowDirections = .down
                        vc.popoverPresentationController?.sourceView = sender.superview!
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    self.brushWidth = self.brushSize[i]
                    UserDefaults.standard.set(self.brushWidth, forKey: self.dfPenSize)
                }
                UserDefaults.standard.set(i, forKey: self.dfPenSelected)
                UserDefaults.standard.synchronize()
                self.refreshButton()
            })
        }
    }
    @objc func selectColor(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1, animations: {
            sender.transform = CGAffineTransform(translationX: 0, y: -12)
        }) {[unowned self] (_) in
            UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
                sender.transform = .identity
            })
            if let i = self.colorButtons.firstIndex(of: sender) {
                UserDefaults.standard.set(i, forKey: self.dfPenColor)
                UserDefaults.standard.synchronize()
                self.brushColor = self.inkColor[i]
            }
            self.refreshButton()
        }
    }
    @objc func colorSelector(_ sender: UIButton) {
        let vc = ColorPickerViewController()
        vc.delegate = self
        vc.modalPresentationStyle = .popover
        vc.preferredContentSize = CGSize(width: 198, height: 160)
        vc.popoverPresentationController?.sourceRect = sender.frame
        vc.popoverPresentationController?.delegate = self
        vc.popoverPresentationController?.permittedArrowDirections = .down
        vc.popoverPresentationController?.sourceView = sender.superview!
        present(vc, animated: true, completion: nil)
    }
    func colorPickerDidSelect(color: UIColor) {
        UserDefaults.standard.set(0, forKey: dfPenColor)
        UserDefaults.standard.set(color.hexCode, forKey: dfSubColor)
        UserDefaults.standard.synchronize()
        brushColor = color
        refreshButton()
    }
    func penSizeDidSelect(size: CGFloat) {
        brushWidth = size
        UserDefaults.standard.set(0, forKey: dfPenSelected)
        UserDefaults.standard.set(size, forKey: dfPenSize)
        UserDefaults.standard.synchronize()
    }
}
//MARK:- Draw Ink Path
@available(iOS 11.0, *)
extension PdfViewController {
    func addPanGestureRecognition() {
        let panGes = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        panGes.name = "pan_ink"
        view.addGestureRecognizer(panGes)
    }
    @objc func handlePan(recognizer:UIPanGestureRecognizer) {
        let point: CGPoint = recognizer.location(in: pdfView)
        switch (recognizer.state) {
        case .began:
            touchesBegan(position: point)
        case .ended:
            touchesEnded(position: point)
        default:
            touchesMoved(position: point)
        }
    }
    func touchesBegan(position: CGPoint) {
        drawPath = UIBezierPath()
        lastPoint = position
        pathPoints = [position]
        pathShapes = []
    }
    
    func touchesMoved(position: CGPoint) {
        drawPath?.move(to: lastPoint)
        drawPath?.addLine(to: position)
        drawShapeLayer()
        lastPoint = position
        pathPoints.append(position)
    }
    func touchesEnded(position: CGPoint) {
        if let page = currentPage {
            curInkTag += 1
            let points = pathPoints.map{pdfView.convert($0, to: page)}
            let pageNo = SP.shared.actualPageNumber(page: page)
            pathData.append((page: pageNo, tag: curInkTag, color: brushColor.hexCode, width: brushWidth, points: points))
            pageLayerShapes.append(pathShapes)
        }
    }
    func drawShapeLayer() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = drawPath?.cgPath
        shapeLayer.strokeColor = brushColor.withAlphaComponent(inkAlpha).cgColor
        shapeLayer.lineWidth = brushWidth
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.name = "ink"
        pdfView.layer.addSublayer(shapeLayer)
        pathShapes.append(shapeLayer)
    }
}
//MARK:- Select ink annotation
@available(iOS 11.0, *)
extension PdfViewController {
    func getInkPathInfo(at page: PDFPage, nearestPoint: CGPoint) -> NSDictionary? {
        let precision: CGFloat = 10
        let pageNo = SP.shared.actualPageNumber(page: page)
        let pagePathDicts = currentInkArray.filter{getAnnotationPage(jsonDict: $0) == pageNo}
        for info in pagePathDicts {
            if let annotationInfo = info["annotationList"] as? NSDictionary,
            let points = annotationInfo["points"] as? [[String: CGFloat]],
            points.count > 0 {
                for p in points {
                    if let x = p["X"], let y = p["Y"] {
                        if abs(x - nearestPoint.x) <= precision && abs(y - nearestPoint.y) <= precision {
                            return info
                        }
                    }
                }
            }
        }
        
        return nil
    }
}

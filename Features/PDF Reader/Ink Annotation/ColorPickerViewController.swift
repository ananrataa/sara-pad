//
//  ColorPickerViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 13/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
protocol ColorPickerViewDelegate: class {
    func colorPickerDidSelect(color: UIColor)
}
class ColorPickerViewController: UIViewController {
    weak var delegate: ColorPickerViewDelegate?
    @IBOutlet var colorButtons: [UIButtonX]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func colorSelected(_ sender: UIButtonX) {
        sender.squeeze()
        delegate?.colorPickerDidSelect(color: sender.backgroundColor!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {[weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
}

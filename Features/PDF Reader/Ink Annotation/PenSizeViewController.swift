//
//  PenSizeViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 13/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
protocol PenSizeViewDelegate: class {
    func penSizeDidSelect(size:CGFloat)
}
class PenSizeViewController: UIViewController {
    weak var delegate: PenSizeViewDelegate?
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var sliderSize: UISlider!
    
    var curColor: UIColor
    var curSize:CGFloat
    
    init(color:UIColor, size:CGFloat) {
        self.curColor = color
        self.curSize = size
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lineView.backgroundColor = curColor
        lineView.frame.size.height = curSize
        sliderSize.value = Float(curSize)
    }
    @IBAction func penSizeChanged(_ sender: UISlider) {
        curSize = CGFloat(sender.value)
        lineView.frame.size.height = curSize
        delegate?.penSizeDidSelect(size: curSize)
    }
    
    
}

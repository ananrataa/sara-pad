//
//  TranslateViewController.swift
//  SaraPadReader
//
//  Created by Anan Ratanasethakul on 17/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit
import AVFoundation
import SwiftyJSON
import RxCocoa
import RxSwift

protocol TranslateViewDelegate: class {
    func translateSpeechStatus(speaking isSpeak: Bool)
}
@available(iOS 11.0, *)
class TranslateViewController: UIViewController {
    
    
    weak var delegate: TranslateViewDelegate?
    @IBOutlet weak var textSource: UITextFieldX!
    @IBOutlet weak var textTarget: UITextFieldX!
    @IBOutlet weak var sourceScrollView: UIScrollView!
    @IBOutlet weak var targetScrollView: UIScrollView!
    @IBOutlet weak var sourceSpeakButton: UIButtonX!
    @IBOutlet weak var targetSpeakButton: UIButtonX!
    @IBOutlet weak var sliderSpeechRate: UISlider!
    @IBOutlet weak var lblFromLang: PaddingLabel!
    @IBOutlet weak var lblToLang: PaddingLabel!
    
    let disposeBag = DisposeBag()
    var fontSize: CGFloat = 17
    var lblSource: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.backgroundColor = .clear
        return lbl
    }()
    var lblTarget: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.backgroundColor = .clear
        return lbl
    }()
    lazy var btnFontUp:UIButton = {
        let btn = UIButton(image: #imageLiteral(resourceName: "font_increase25"), target: self, action: #selector(fontChanged(_:)))
        return btn
    }()
    lazy var btnFontDown:UIButton = {
        let btn = UIButton(image: #imageLiteral(resourceName: "font_decrease25"), target: self, action: #selector(fontChanged(_:)))
        return btn
    }()
    
    var att:[NSAttributedString.Key:Any] = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17)]
    
    let speechSynthesizer = AVSpeechSynthesizer()
    var currentSourceLng:String?
    var currentDstLng:String?
    var curSpeechRate: Float = AVSpeechUtteranceDefaultSpeechRate {
        didSet {
            isSpeechRateChanged = true
            sliderSpeechRate.value = curSpeechRate
        }
    }
    var curMode = 0
    var curRect: CGRect?
    var languageDict: Dictionary<String,String>!
    var sourceText: String = ""
    var targetText: String = ""
    var sourceKey: String = ""
    var targetKey: String = ""
    var pdfView: PDFView
    var currentPage: PDFPage?
    var textComponents:[String] = []
    var isSpeechRateChanged = false
    init(pdfView: PDFView, curPage: PDFPage?) {
        self.pdfView = pdfView
        self.currentPage = curPage
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = String.localizedString(key: "Translation")

        SVProgressHUD.show(withStatus: "Translating...")
        if let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"), let lngDict = NSDictionary(contentsOfFile: path){
            currentSourceLng = lngDict.object(forKey: sourceKey) as? String
            currentDstLng = (lngDict.object(forKey: targetKey) as? String)
        }
        speechSynthesizer.delegate = self
        
        //check if exceeds 1,500 chars
        var strSelections = ""
        if let page = currentPage {
            strSelections = (page.attributedString?.string) ?? ""
        } else  {
            strSelections = pdfView.currentSelection?.string ?? ""
        }
        
        sourceText = strSelections.replacingOccurrences(of: "\"", with: " ")
        textComponents = strSelections.pairs(every: 1500)
        let text = textComponents.removeFirst()
        googleTranslate(text: text)
        setup()
        
    }

    func setup() {
        //NavigationItem
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .plain, target: self, action: #selector(done))

        let fontStackView = UIStackView(arrangedSubviews: [btnFontDown, btnFontUp])
        fontStackView.spacing = 12
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: fontStackView)
        //Language lists
        textSource.text = getLanguageString(key: sourceKey)
        textTarget.text = getLanguageString(key: targetKey)
        
        sourceScrollView.addBorder(width: 0.5, color: .gray, radius: 7)
        targetScrollView.addBorder(width: 0.5, color: .gray, radius: 7)
        
        let h = textSource.frame.height
        textSource.rightViewMode = .always
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: h, height: h))
        btn.tag = 0
        btn.addTarget(self, action: #selector(selectLanguage(_:)), for: .touchUpInside)
        btn.setImage(#imageLiteral(resourceName: "text_img_find"), for: .normal)
        textSource.rightView = btn
        
        textTarget.rightViewMode = .always
        let btnTarget = UIButton(frame: CGRect(x: 0, y: 0, width: h, height: h))
        btnTarget.tag = 1
        btnTarget.addTarget(self, action: #selector(selectLanguage(_:)), for: .touchUpInside)
        btnTarget.setImage(#imageLiteral(resourceName: "text_img_find"), for: .normal)
        textTarget.rightView = btnTarget
        
        //localized labels
        lblFromLang.text = String.localizedString(key: "From Language")
        lblToLang.text = String.localizedString(key: "To Language")
    }
    func getLanguageString(key: String) -> String {
        if languageDict == nil {
            guard
                let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"),
                let languageDict = NSDictionary(contentsOfFile: path) as? Dictionary<String,String>
            else {
                return key
            }
            self.languageDict = languageDict
        }
        let flag = "\(String.localizedString(key: key.trimmingCharacters(in: .whitespaces))) "
        return flag + (languageDict[key] ?? key)
    }
    @objc func fontChanged(_ sender: UIButton) {
        if sender == btnFontUp {
            fontSize *= 1.1
        } else {
            fontSize /= 1.1
        }
        fontSize = max(min(fontSize, 36), 12)
        att = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: fontSize)]
        refreshSourceText()
        refreshTargetText()
    }
    func refreshSourceText() {
        let w = sourceScrollView.bounds.width - 16
        let h = sourceScrollView.bounds.height - 16
        lblSource.removeFromSuperview()
        lblSource.frame = CGRect(x: 8, y: 8, width: w, height: h)
        lblSource.text = sourceText
        lblSource.font = UIFont.systemFont(ofSize: fontSize)
        lblSource.numberOfLines = 0
        lblSource.sizeToFit()
        var contentSize = lblSource.frame.size
        if lblSource.frame.size.height > h {
            contentSize.height += 50
        }
        sourceScrollView.addSubview(lblSource)
        sourceScrollView.contentSize = contentSize
    }
    func refreshTargetText() {
        let w = targetScrollView.frame.width - 16
        let h = targetScrollView.frame.height - 16
        lblTarget.removeFromSuperview()
        lblTarget.frame = CGRect(x: 8, y: 8, width: w, height: h)
        lblTarget.text = targetText
        lblTarget.font = UIFont.systemFont(ofSize: fontSize)
        lblTarget.numberOfLines = 0
        lblTarget.sizeToFit()
        var contentSize = lblTarget.frame.size
        if lblTarget.frame.size.height > h {
            contentSize.height += 50
        }
        targetScrollView.addSubview(lblTarget)
        targetScrollView.contentSize = contentSize
    }
    @objc func selectLanguage(_ sender: UIButton) {
        speechSynthesizer.stopSpeaking(at: .immediate)
        sourceSpeakButton.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
        targetSpeakButton.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
        curMode = sender.tag
        
        let languageKey = sender.tag == 0 ? sourceKey:targetKey
        let vc = SelectLanguageViewController(curKey: languageKey)
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
        
    }

    @objc func done() {
        SVProgressHUD.dismiss()
        if speechSynthesizer.isSpeaking {
            speechSynthesizer.stopSpeaking(at: .immediate)
        }
        pdfView.currentSelection = nil
        dismiss(animated: true, completion: nil)
    }
   
    @IBAction func sourceSpeechTap(_ sender: UIButton) {
        curRect = nil
        if curMode == 1 {
            speechSynthesizer.stopSpeaking(at: .immediate)
            curMode = 0
            startTextSpeech()
            sourceSpeakButton.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            targetSpeakButton.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
        } else {
            if speechSynthesizer.isPaused {
                speechSynthesizer.continueSpeaking()
                sender.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            } else if speechSynthesizer.isSpeaking {
                speechSynthesizer.pauseSpeaking(at: .word)
                sender.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
            } else {
                startTextSpeech()
                sender.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            }
        }
    }
    
    @IBAction func targetSpeechText(_ sender: UIButton) {
        curRect = nil
        if curMode == 0 {
            speechSynthesizer.stopSpeaking(at: .immediate)
            curMode = 1
            startTextSpeech()
            sender.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            sourceSpeakButton.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
        } else {
            if speechSynthesizer.isPaused {
                speechSynthesizer.continueSpeaking()
                sender.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            } else if speechSynthesizer.isSpeaking {
                speechSynthesizer.pauseSpeaking(at: .word)
                sender.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
            } else {
                startTextSpeech()
                sender.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
            }
        }
    }
    @IBAction func increaseSpeechRate(_ sender: Any) {
        curSpeechRate = min(curSpeechRate*1.1, AVSpeechUtteranceMaximumSpeechRate)
    }
    @IBAction func decreaseSpeechRate(_ sender: Any) {
        curSpeechRate = max(curSpeechRate/1.1, AVSpeechUtteranceMinimumSpeechRate)
    }
    @IBAction func defaultSpeechRate(_ sender: Any) {
        curSpeechRate = AVSpeechUtteranceDefaultSpeechRate
    }
    
    @IBAction func speechRateChanged(_ sender: UISlider) {
        curSpeechRate = sender.value
    }
    func refreshTextSpeaker() {
        if speechSynthesizer.isPaused {
            return
        }
        if speechSynthesizer.isSpeaking {
            speechSynthesizer.stopSpeaking(at: .immediate)
            startTextSpeech()
        }
        delegate?.translateSpeechStatus(speaking: speechSynthesizer.isSpeaking)
    }
    func startTextSpeech() {
        if let sv = curMode == 0 ? sourceScrollView : targetScrollView {
            let rect = CGRect(origin: .zero, size: sv.bounds.size)
            DispatchQueue.main.async {
                sv.scrollRectToVisible(rect, animated: true)
            }
        }
        let text = curMode == 0 ? sourceText : targetText
        let language = curMode == 0 ? sourceKey : targetKey
        let speechUtterance = AVSpeechUtterance(string:text)
        speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate
        speechUtterance.pitchMultiplier = 1
        speechUtterance.rate = curSpeechRate
        speechUtterance.volume = 1.0
        speechUtterance.voice =  AVSpeechSynthesisVoice(language: language)
        speechSynthesizer.speak(speechUtterance)
    }
    
    func refreshSpeech(with text: String) {
        if let sv = curMode == 0 ? sourceScrollView : targetScrollView {
            let rect = CGRect(origin: .zero, size: sv.bounds.size)
            DispatchQueue.main.async {
                sv.scrollRectToVisible(rect, animated: true)
            }
        }
        let language = curMode == 0 ? sourceKey : targetKey
        let speechUtterance = AVSpeechUtterance(string:text)
        speechUtterance.pitchMultiplier = 1
        speechUtterance.rate = curSpeechRate
        speechUtterance.volume = 1.0
        speechUtterance.voice =  AVSpeechSynthesisVoice(language: language)
        speechSynthesizer.speak(speechUtterance)
    }
}

@available(iOS 11.0, *)
extension TranslateViewController: AVSpeechSynthesizerDelegate, UIScrollViewDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        if isSpeechRateChanged {
            isSpeechRateChanged = false
            let text = utterance.speechString // curMode == 0 ? sourceText : targetText
            let remText = String(text.dropFirst(characterRange.location))
            if speechSynthesizer.isSpeaking {
                speechSynthesizer.stopSpeaking(at: .immediate)
                let language = curMode == 0 ? sourceKey : targetKey
                let speechUtterance = AVSpeechUtterance(string:remText)
                speechUtterance.pitchMultiplier = 1
                speechUtterance.rate = curSpeechRate
                speechUtterance.volume = 1.0
                speechUtterance.voice =  AVSpeechSynthesisVoice(language: language)
                speechSynthesizer.speak(speechUtterance)
            }
            return
        }
        let mutableAttributeString = NSMutableAttributedString(string: utterance.speechString)
        let att:[NSAttributedString.Key:Any] = [.foregroundColor:UIColor.red, .backgroundColor:UIColor.yellow]
        mutableAttributeString.addAttributes(att, range: characterRange)
        DispatchQueue.main.async {
            if self.curMode == 0 {
                self.lblSource.attributedText = mutableAttributeString
                self.scrollText(self.lblSource, range: characterRange)
            } else {
                self.lblTarget.attributedText = mutableAttributeString
                self.scrollText(self.lblTarget, range: characterRange)
            }
        }
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        if curMode == 0 {
            sourceSpeakButton.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
        } else {
            targetSpeakButton.setImage(#imageLiteral(resourceName: "pause25"), for: .normal)
        }
        delegate?.translateSpeechStatus(speaking: true)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        
        if curMode == 0 {
            lblSource.attributedText = NSMutableAttributedString(string: sourceText)
            sourceSpeakButton.setImage(#imageLiteral(resourceName: "speaker"), for: .normal)
        } else {
            lblTarget.attributedText = NSMutableAttributedString(string: targetText)
            targetSpeakButton.setImage(#imageLiteral(resourceName: "speaker25"), for: .normal)
        }
        delegate?.translateSpeechStatus(speaking: false)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance) {
        delegate?.translateSpeechStatus(speaking: false)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        delegate?.translateSpeechStatus(speaking: false)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance) {
        delegate?.translateSpeechStatus(speaking: true)
    }
    func scrollText(_ label: UILabel, range: NSRange) {
        //Scroll text
        if var rect = label.boundingRect(forCharacterRange: range, attributes: att),
            let sv = label.superview as? UIScrollView {
            if rect.origin.y >= sv.frame.height/2 {
                rect.origin.y -= sv.frame.height/2
                let point = label.convert(rect.origin, to: sv)
                let svRect = CGRect(origin: point, size: sv.bounds.size)
                if let curRect = self.curRect, svRect.origin.y > curRect.origin.y {
                    DispatchQueue.main.async {
                        sv.scrollRectToVisible(svRect, animated: true)
                    }
                }
                self.curRect = svRect
            }
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return curMode == 0 ? lblSource : lblTarget
    }
}

@available(iOS 11.0, *)
extension TranslateViewController: SelectLanguageDelegate {
    func selectLanguageDidSelect(key: String) {
        if curMode == 0 {
            sourceKey = key
            textSource.text = getLanguageString(key: key)
        } else {
            targetKey = key
            textTarget.text = getLanguageString(key: key)
        }
        targetText = ""
        var strSelections = ""
        if let page = currentPage {
            strSelections = (page.attributedString?.string) ?? ""
        } else  {
            strSelections = pdfView.currentSelection?.string ?? ""
        }
        sourceText = strSelections
        textComponents = strSelections.pairs(every: 1500)
        let text = textComponents.removeFirst()
        googleTranslate(text: text)
    }
}
@available(iOS 11.0, *)
extension TranslateViewController: UITextFieldDelegate {
    //MARK:- UITextField delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
@available(iOS 11.0, *)
extension TranslateViewController {
    func googleTranslate(text: String) {
        if InternetConnection.isConnectedToNetwork(){
            sourceKey = text.detectLanguage() ?? "auto"
            if targetKey.isEmpty {
                targetKey = sourceKey == "th" ? "en" : "th"
            }
            GoogleModel.shared.createGoogleTranslationObservable(text: text, source: sourceKey, target: targetKey)
                .subscribe(onNext: {[unowned self] (value) in
                    self.targetText += value
                    //check if more text
                    if self.textComponents.count > 0 {
                        let text = self.textComponents.removeFirst()
                        self.googleTranslate(text: text)
                    } else {
                        //Refresh source & target
                        self.refreshSourceText()
                        self.refreshTargetText()
                        SVProgressHUD.dismiss()
                    }
                }).disposed(by: disposeBag)
        }
        else{
            SVProgressHUD.showError(withStatus: "No internet connection!")
        }
    }
}

//
//  SelectLanguageViewController.swift
//  SaraPadReader
//
//  Created by Anan Ratanasethakul on 17/6/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

protocol SelectLanguageDelegate: class {
    func selectLanguageDidSelect(key: String)
}
class SelectLanguageViewController: UIViewController {
    let cellId = "language_cell"
    weak var delegate: SelectLanguageDelegate?
    @IBOutlet weak var tableView: UITableView!
    
    var languageKeys:[String] = []
    var languageDict: Dictionary<String,String>!
    var curLanguageKey: String
    
    init(curKey: String) {
        curLanguageKey = curKey
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.addBorder(width: 1, color: .lightGray, radius: 7)
        if let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"),
            let lngDict = NSDictionary(contentsOfFile: path) as? Dictionary<String,String> {
            languageDict = lngDict
            languageKeys = Array(lngDict.keys).sorted{lngDict[$0] ?? "" < lngDict[$1] ?? ""}
            if let index = languageKeys.firstIndex(of: "auto") {
                languageKeys.remove(at: index)
                languageKeys.insert("auto", at: 0)
            }
            tableView.reloadData()
            if let row = languageKeys.firstIndex(of: curLanguageKey) {
                tableView.scrollToRow(at: IndexPath(row: row, section: 0), at: .middle, animated: true)
            }
        }
    }
    
}

extension SelectLanguageViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageKeys.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 45))
        headerView.backgroundColor = UIColor(hexString: "#FF9300")
        headerView.text = " Select Language:"
        headerView.font = UIFont.systemFont(ofSize: 17)
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.tintColor = UIColor.red
        let key = languageKeys[indexPath.row]
        let flag = String.localizedString(key: key.trimmingCharacters(in: .whitespaces))
        let language = languageDict[key] ?? ""
        cell.textLabel?.text = "\(flag) \(language)"
        if curLanguageKey == key {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SVProgressHUD.show(withStatus: "Translating...")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.dismiss(animated: true) {
                self.delegate?.selectLanguageDidSelect(key: self.languageKeys[indexPath.row])
            }
        }
    }
}

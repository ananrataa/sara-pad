//
//  Area5ViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 26/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation


class Area5ViewController: UIViewController {
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var timeSlider: DesignableSlider!
    @IBOutlet weak var progressTime: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    @IBOutlet weak var btnVolume: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    
    let qPlayer = AVQueuePlayer()
    
    var timeObserverToken: Any?
    
    var playerLayer: AVPlayerLayer!
    var isAd: Bool = false
    var mediaPath: String
    var json:[String:Any]
    var roomId: String
    var ad: SPApiAdsItem?
    var mediaTitle: String
    var resumeFromLinkAd = false
    var isAdsTapped = false
    
    lazy var volumeSlider: UISlider = {
        let sl = UISlider(frame: .zero)
        sl.value = 0.5
        sl.minimumTrackTintColor = UIColor(hexString: "#FF9300")
        sl.maximumTrackTintColor = UIColor.lightGray
        if let vol = UserDefaults.standard.value(forKey: DEFAULT_VOLUME) as? Float {
            sl.value = min(1, max(0, vol))
        } else {
            sl.value = 0.5
            UserDefaults.standard.set(0.5, forKey: DEFAULT_VOLUME)
            UserDefaults.standard.synchronize()
        }
        
        sl.alpha = 0
        return sl
    }()
    @objc init(roomId: String, mediaPath: String, json: [String:Any], title: String) {
        self.roomId = roomId
        self.mediaPath = mediaPath
        self.json = json
        self.mediaTitle = title
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        LightTheme.shared.applyNavigationBar(vc: self)
        self.title = mediaTitle
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        controlView.layer.borderColor = UIColor.darkGray.cgColor
        controlView.layer.borderWidth = 1
        controlView.layer.cornerRadius = 7
        
        btnSkip.alpha = 0
        btnSkip.layer.cornerRadius = 7
        btnSkip.layer.borderWidth = 0.5
        btnSkip.layer.borderColor = UIColor(hexString: "#f2f2f2").cgColor
        
        btnPause.layer.borderColor = UIColor.white.cgColor
        btnPause.layer.borderWidth = 0.5
        btnPause.layer.cornerRadius = btnPause.frame.height/2
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if resumeFromLinkAd {
            resumeFromLinkAd.toggle()
            if !qPlayer.isPlaying {
                qPlayer.play()
                btnSkip.alpha = 1
            } else {
                skipAds(btnSkip)
            }
        } else {
            loadVideo()
        }
        self.view.setNeedsLayout()
        SVProgressHUD.dismiss()
    }
    func loadVideo() {
        //check Area 5, Video
        if self.ad == nil {
            let ads = SPRoomAds(json: json)
            self.ad = ads.area5.first
        }
        //check Area 6, Audio
        if self.ad == nil {
            let ads = SPRoomAds(json: json)
            self.ad = ads.area6.first
        }
        if let ad = self.ad {
            if let url = URL(string: ad.adsImageUrl) {
                let asset = AVURLAsset(url: url)
                let item = AVPlayerItem(asset: asset)
                qPlayer.insert(item, after: qPlayer.items().last)
                isAd = true
            }
        }
        //Video
        if let url = URL(string: mediaPath) {
            let asset = AVURLAsset(url: url)
            let item = AVPlayerItem(asset: asset)
            qPlayer.insert(item, after: qPlayer.items().last)
        }
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { (ntf) in
            if self.isAd || self.qPlayer.items().count == 1 {
                self.isAd = false
                self.btnSkip.alpha = 0
            } else {
                self.done()
            }
        }
        
        playerLayer = AVPlayerLayer(player: qPlayer)
        videoView.layer.addSublayer(playerLayer)
        qPlayer.volume = volumeSlider.value
        qPlayer.play()
        
        //set Timer for skip button
        if let ad = ad, ad.skip, isAd {
            DispatchQueue.main.asyncAfter(deadline: .now() + ad.duration1) {
                self.btnSkip.alpha = 1
            }
        }
        
        //set TimeObserver to update reading progress
        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        timeSlider.value = 0
        timeObserverToken = qPlayer.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) {
            [weak self] time in
            
            if let totalCMTime = self?.qPlayer.currentItem?.duration,
                let curCMTime = self?.qPlayer.currentItem?.currentTime() {
                //timeSlider progress
                DispatchQueue.main.async {
                    self?.timeSlider.value = Float(curCMTime.seconds/totalCMTime.seconds)
                    self?.progressTime.text = CMTimeGetSeconds(curCMTime).stringFromTimeIntervalMMss()
                    if totalCMTime.isValid && curCMTime.isValid && totalCMTime != CMTime.indefinite {
                        self?.remainTime.text = "-\((CMTimeGetSeconds(totalCMTime - curCMTime)).stringFromTimeIntervalMMss())"
                    }
                    
                    //if curItem not Ads hide skip button
                    if let btn = self?.btnSkip, btn.alpha > 0 {
                        if self?.qPlayer.items().count == 1 {
                            self?.isAd = false
                            btn.alpha = 0
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let playerLayer = playerLayer {
            playerLayer.frame = videoView.bounds
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let p = touch.location(in: view)
            let x = btnVolume.frame.origin.x + 2*btnVolume.frame.width
            let y = controlView.frame.origin.y
            guard p.y < y else {return}
            if isAd, let ad = ad, URL(string: ad.linkUrl) != nil, p.x > x {
                qPlayer.pause()
                resumeFromLinkAd = true
                let vc = WebOSViewController(urlPath: ad.linkUrl, title: ad.adsName)
                let nav = UINavigationController(rootViewController: vc)
                self.present(nav, animated: true, completion: nil)
                isAdsTapped = true
                return
            }
            
            super.touchesBegan(touches, with: event)
            
            guard p.y < controlView.frame.origin.y else {return}
            
            guard volumeSlider.alpha == 0 else {
                if p.x > x {
                    UIView.animate(withDuration: 1) {
                        self.volumeSlider.alpha = 0
                        let alpha:CGFloat = 1
                        self.stackView.alpha = alpha
                        self.controlView.alpha = alpha
                        self.navigationController?.navigationBar.alpha = alpha
                    }
                }
                return
            }
            UIView.animate(withDuration: 1) {
                let alpha:CGFloat = self.stackView.alpha == 0 ? 1 : 0
                self.stackView.alpha = alpha
                self.controlView.alpha = alpha
                self.navigationController?.navigationBar.alpha = alpha
            }
        }
    }
    
    func stopPlayer() {
        if qPlayer.isPlaying {
            qPlayer.pause()
            qPlayer.rate = 0.0
            qPlayer.replaceCurrentItem(with: nil)
        }
    }
    @objc func done() {
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        stopPlayer()
        if let token = timeObserverToken {
            qPlayer.removeTimeObserver(token)
            timeObserverToken = nil
        }
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func pause(_ sender: UIButton) {
        if qPlayer.isPlaying {
            qPlayer.pause()
            UIView.animate(withDuration: 1) {
                sender.setImage(#imageLiteral(resourceName: "play.png"), for: .normal)
            }
        } else {
            qPlayer.play()
            UIView.animate(withDuration: 1) {
                sender.setImage(#imageLiteral(resourceName: "pause.png"), for: .normal)
            }
        }
    }
    @IBAction func forward(_ sender: UIButton) {
        guard let duration  = qPlayer.currentItem?.duration else {
            return
        }
        let playerCurrentTime = CMTimeGetSeconds(qPlayer.currentTime())
        let newTime = playerCurrentTime + 15
        
        if newTime < (CMTimeGetSeconds(duration) - 15) {
            
            let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
            qPlayer.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            
        }
    }
    
    @IBAction func backward(_ sender: UIButton) {
        let playerCurrentTime = CMTimeGetSeconds(qPlayer.currentTime())
        let newTime = max(0, playerCurrentTime - 15)
        let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        qPlayer.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    @IBAction func volumeTapped(_ sender: UIButton) {
        if volumeSlider.frame == .zero {
            let btnFrame = sender.frame
            let x = btnFrame.origin.x + (btnFrame.width/2 - 100)
            let y = btnFrame.origin.y - 100
            let rect = CGRect(x: x , y: y, width: 200, height: 10)
            volumeSlider.frame = rect
            view.addSubview(volumeSlider)
            volumeSlider.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2))
            volumeSlider.addTarget(self, action: #selector(volumeChanged), for: .valueChanged)
        }
        
        UIView.animate(withDuration: 1) {
            self.volumeSlider.alpha = 1
            let alpha:CGFloat = 0
            self.stackView.alpha = alpha
            self.controlView.alpha = alpha
            self.navigationController?.navigationBar.alpha = alpha
        }
    }
    
    @objc func volumeChanged() {
        qPlayer.volume = volumeSlider.value
        let x = volumeSlider.value
        switch x {
        case 0:
            btnVolume.setImage(#imageLiteral(resourceName: "mute.png"), for: .normal)
        case _ where x < 0.65 :
            btnVolume.setImage(#imageLiteral(resourceName: "volume_mid.png"), for: .normal)
        default:
            btnVolume.setImage(#imageLiteral(resourceName: "volume.png"), for: .normal)
        }
        UserDefaults.standard.set(x, forKey: DEFAULT_VOLUME)
        UserDefaults.standard.synchronize()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 1) {
                self.volumeSlider.alpha = 0
            }
        }
    }
    
    @IBAction func skipAds(_ sender: UIButton) {
        isAd = false
        btnSkip.alpha = 0
        qPlayer.advanceToNextItem()
    }
}


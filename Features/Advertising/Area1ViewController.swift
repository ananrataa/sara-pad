//
//  Area1ViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 19/9/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit



class Area1ViewController: UIViewController, AlertHandler {
    @IBOutlet weak var adImageView: UIImageView!
    var clickAds: Bool = false
    let btnClose: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 32))
        btn.setTitle(String.localizedString(key: "close"), for: .normal)
        btn.titleLabel?.font = UIFont(name: "\(sukhumvitLightFont)", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .light)
        btn.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        btn.tintColor = .white
        btn.layer.cornerRadius = btn.frame.height/2
        btn.layer.borderColor = btn.tintColor.cgColor
        btn.layer.borderWidth = 0.5
        return btn
    }()
    var json:[[String:Any]]
    var ad: SPApiAdsItem!
    @objc init(json: AnyObject) {
        if let data = json as? [NSDictionary] as? [[String:Any]] {
            self.json = data
        } else {
            self.json = []
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClose.addTarget(self, action: #selector(done), for: .touchUpInside)
        if json.count > 0 {
            ad = SPApiAdsItem(json: json[0])
            guard let url = URL(string: ad.adsImageUrl) else {return}
            do {
                let data = try Data(contentsOf: url)
                adImageView.image = UIImage(data: data)
                let tap = UITapGestureRecognizer(target: self, action: #selector(linkUrlTouched(sender:)))
                adImageView.addGestureRecognizer(tap)
                
            } catch  {
                return
            }
        }
        SVProgressHUD.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        TransparentTheme.shared.applyNavigationBar(vc: self)
        clickAds = false
        // hide status bar
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindow.Level.statusBar + 1
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if let ad = ad {
            DispatchQueue.main.asyncAfter(deadline: .now() + ad.duration1) {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.btnClose)
                //Another 5 sec -> close
                DispatchQueue.main.asyncAfter(deadline: .now() + ad.duration2 - ad.duration1, execute: { [weak self] in
                    self?.done()
                })
            }
        } else {
            done()
        }
    }
    
    @IBAction func done() {
        if clickAds {
            return
        }
        self.dismiss(animated: true,completion: {
            // show status bar
            DispatchQueue.main.async {
                if let window = UIApplication.shared.keyWindow {
                    window.windowLevel = UIWindow.Level.normal
                }
            }
        })
    }
    
    @objc func linkUrlTouched(sender: UITapGestureRecognizer) {
        if navigationItem.rightBarButtonItem == nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.btnClose)
        }
        if URL(string: ad.linkUrl) != nil {
            clickAds = true
            let vc = WebOSViewController(urlPath: ad.linkUrl, title: "")
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

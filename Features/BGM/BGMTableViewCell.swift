//
//  BGMTableViewCell.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/15/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class BGMTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var btnPreview: UIButton!
    @IBOutlet weak var btnSelection: UIButton!
    
    //Service songs
    var curTrack: SPBGMTrack? {
        didSet {
            refreshTrack()
        }
    }
    //Device songs
    var curSongItem: DeviceSongItem? {
        didSet {
            refreshSong()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        btnPreview.addBorder(width: 0.5, color: .clear, radius: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func refreshTrack() {
        if let track = curTrack {
            lblTitle.text = track.trackName
            lblArtist.text = track.trackId
            btnPreview.layer.borderColor = UIColor.clear.cgColor
            btnPreview.setImage(#imageLiteral(resourceName: "note30"), for: .normal)
            btnSelection.setImage(nil, for: .normal)
        }
    }
    func refreshSong() {
        if let songItem = curSongItem {
            lblTitle.text = songItem.title
            lblArtist.text = songItem.artist
            btnPreview.layer.borderColor = UIColor.clear.cgColor
            btnPreview.setImage(#imageLiteral(resourceName: "note30"), for: .normal)
            let image = songItem.selected ? #imageLiteral(resourceName: "check-box") : #imageLiteral(resourceName: "uncheck-box")
            btnSelection.setImage(image, for: .normal)
        }
    }
}

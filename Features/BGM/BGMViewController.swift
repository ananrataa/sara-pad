//
//  BGMViewController.swift
//  SARA-PAD
//
//  Created by mbox imac on 6/15/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import RxCocoa
import RxSwift

struct DeviceSongItem {
    let albumTitle: String
    let trackNumber: Int
    let title: String
    let artist: String
    let trackDuration :TimeInterval
    let persistentID: UInt64
    var selected: Bool
}
enum BgmMode {
    case Cloud
    case Device
}
protocol BGMViewDelegate: class {
    func bgmViewDidSelectedCloundSongs(playLists:[SPBGMPlayList])
    func bgmViewDidSelectedDeviceSongs(songs:[DeviceSongItem])
}
@available(iOS 11.0, *)
class BGMViewController: UIViewController, AlertHandler {
    weak var delegate: BGMViewDelegate?
    var deviceSongs: [DeviceSongItem] = [] {
        didSet {
            let selectedSongs = deviceSongs.filter{$0.selected}
            let songsDuration:TimeInterval = (selectedSongs.map{$0.trackDuration}).reduce(0, +)
            let songsCount = selectedSongs.count
            let selectedText = Bundle.main.localizedString(forKey: "Selected songs", value: nil, table: nil)
            let songsText = Bundle.main.localizedString(forKey: "songs", value: nil, table: nil)
            let hrsText = Bundle.main.localizedString(forKey: "hrs", value: nil, table: nil)
            let str = "\(selectedText) (\(songsCount) \(songsText), \(songsDuration.stringFromTimeInterval()) \(hrsText))"
            btnSelectionInfo.setTitle(str, for: .normal)
        }
    }
    var myMediaPlayer = MPMusicPlayerController.applicationQueuePlayer
    let cellId = "track_cell"
    let disposeBag = DisposeBag()
    var roomId: String
    var cloudPlayLists: [SPBGMPlayList] = [] {
        didSet {
            DispatchQueue.main.async {[unowned self] in
                self.tableView.reloadData()
                let selectedPlayList = self.cloudPlayLists.filter{$0.selected}
                let songsDuration = (selectedPlayList.map{Double($0.duration) ?? 0}).reduce(0.0, +)
                let songsCount = (selectedPlayList.map{$0.tracks.count}).reduce(0, +)
                let selectedText = Bundle.main.localizedString(forKey: "Selected songs", value: nil, table: nil)
                let songsText = Bundle.main.localizedString(forKey: "songs", value: nil, table: nil)
                let minsText = Bundle.main.localizedString(forKey: "mins", value: nil, table: nil)
                let str = "\(selectedText) (\(songsCount) \(songsText), \(songsDuration) \(minsText))"
                self.btnSelectionInfo.setTitle(str, for: .normal)
            }
        }
    }
    var player: AVPlayer = AVPlayer()
    var curButton: UIButton?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSelectionInfo: UIButton!
    @IBOutlet weak var segmentSongSource: UISegmentedControl!
    
    init(roomId: String) {
        //2a49571e-0c34-44ff-bd22-0a432fe69dd2
        self.roomId = roomId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.alpha = 1
        title = "Background Music"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        let att = [NSAttributedString.Key.font: UIFont(name: mainFont, size: 17)!]
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(att, for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(att, for: .normal)
        
        tableView.register(UINib.init(nibName: "BGMTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        
        btnSelectionInfo.addBorder(width: 1, color: .clear, radius: 7)
        tableView.tableFooterView = UIView()
        let audioSession = AVAudioSession.sharedInstance()
        do
        {
//            try audioSession.setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playback), mode: .duckOthers)
            try audioSession.setCategory(.playback, mode: .moviePlayback, options: [.duckOthers])
            try audioSession.setActive(true)
        }
        catch
        {
            print("AVAudioSession cannot be set")
        }
        //set device name in segmentController
        segmentSongSource.setTitle(UIDevice.current.name, forSegmentAt: 1)
        //Room data
        loadRoomPlayList()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let audioSession = AVAudioSession.sharedInstance()
        do
        {
//            try audioSession.setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playback), mode: .defaultToSpeaker)
            try audioSession.setCategory(.playback, mode: .default, options: [.defaultToSpeaker])
            try audioSession.setActive(true)
        }
        catch
        {
            print("AVAudioSession cannot be set")
        }
    }
    @IBAction func selectSource(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if self.cloudPlayLists.count == 0 {
                self.loadRoomPlayList()
            }
        } else {
            if self.deviceSongs.count == 0 {
                self.loadDeviceSongs()
            }
        }
    }
    
    @objc func done() {
        selectedInfo()
        navigationController?.popViewController(animated: true)
    }
    func selectedInfo() {
        if segmentSongSource.selectedSegmentIndex == 0 {
            let songs = cloudPlayLists.filter{$0.selected}
            if songs.count > 0 {
                self.delegate?.bgmViewDidSelectedCloundSongs(playLists:songs)
            }
        } else {
            let songs = deviceSongs.filter{$0.selected}
            if songs.count > 0 {
                delegate?.bgmViewDidSelectedDeviceSongs(songs: songs)
            }
        }
    }
    @IBAction func btnSelectionInfoTap(_ sender: Any) {
        selectedInfo()
        done()
    }
    @objc func cancel() {
        navigationController?.popViewController(animated: true)
    }
    func loadRoomPlayList() {
        let deviceCode = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        //Get RoomDetail
        RoomAPI.shared.deviceRoomDetail(memberId: deviceCode, roomId: roomId) {[unowned self] (roomDetail, _) in
            if let roomDetail = roomDetail {
                DispatchQueue.main.async {
                    self.segmentSongSource.setTitle(roomDetail.roomName, forSegmentAt: 0)
                }
            }
        }
        
        RoomAPI.shared.roomBGM(deviceCode: deviceCode, roomId: roomId) {[weak self] (json) in
            if let json = json as? [[String:Any]] {
                let roomBGM = SPBGM(json: json)
                self?.cloudPlayLists = roomBGM.playLists
            }
            if self?.cloudPlayLists.count == 0 {
                DispatchQueue.main.async {
                    let str = self?.segmentSongSource.titleForSegment(at: 0) ?? "Cloud"
                    self?.infoAlert(message: "No songs in \(str)", duration: 2)
                    self?.segmentSongSource.selectedSegmentIndex = 1
                    self?.loadDeviceSongs()
                }
            }
        }
    }
    @IBAction func selectAllTracks(_ sender: Any) {
        if segmentSongSource.selectedSegmentIndex == 1 {
            var songs: [DeviceSongItem] = []
            for var item in deviceSongs {
                item.selected = true
                songs.append(item)
            }
            deviceSongs = songs
        } else {
            for index in 0..<cloudPlayLists.count {
                var playList = cloudPlayLists[index]
                playList.selected = true
                cloudPlayLists[index] = playList
            }
        }
        tableView.reloadData()
    }
    
    @IBAction func selectNone(_ sender: Any) {
        if segmentSongSource.selectedSegmentIndex == 1 {
            var songs: [DeviceSongItem] = []
            for var item in deviceSongs {
                item.selected = false
                songs.append(item)
            }
            deviceSongs = songs
        } else {
            for index in 0..<cloudPlayLists.count {
                var playList = cloudPlayLists[index]
                playList.selected = false
                cloudPlayLists[index] = playList
            }
        }
        tableView.reloadData()
    }
    
    func loadDeviceSongs() {
        MPMediaLibrary.requestAuthorization { (status) in
            DispatchQueue.main.async {
                if status == .authorized {
                    self.getSongItems()
                } else {
                    self.presentAlert(withTitle: "Access Denied", message: "Sara+Pad can't access your music, Please check permissions.")
                }
            }
        }
    }
    private func getSongItems(){
        if let songListItem = MPMediaQuery.songs().items {
            for song in songListItem  {
                let songItem = DeviceSongItem(albumTitle: song.albumTitle ?? "",
                                              trackNumber: song.albumTrackNumber,
                                              title: song.title ?? "",
                                              artist: song.artist ?? "",
                                              trackDuration: song.playbackDuration,
                                              persistentID: song.persistentID, selected: false)
                deviceSongs.append(songItem)
            }
            deviceSongs.sort { (s1, s2) -> Bool in
                if s1.artist != s2.artist {
                    return s1.artist < s2.artist
                } else if s1.albumTitle != s2.albumTitle {
                    return s1.albumTitle < s2.albumTitle
                } else if s1.trackNumber != s2.trackNumber {
                    return s1.trackNumber < s2.trackNumber
                } else {
                    return s1.title < s2.title
                }
            }
            self.tableView.reloadData()
        }
    }
}

@available(iOS 11.0, *)
extension BGMViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return segmentSongSource.selectedSegmentIndex == 0 ? cloudPlayLists.count:1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segmentSongSource.selectedSegmentIndex == 0 ? cloudPlayLists[section].tracks.count:deviceSongs.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return segmentSongSource.selectedSegmentIndex == 0 ? 40 : 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if segmentSongSource.selectedSegmentIndex == 1 { return nil}
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        headerView.backgroundColor = UIColor(hexString: "#ffd299")
        let image = cloudPlayLists[section].selected ? #imageLiteral(resourceName: "check-box") : #imageLiteral(resourceName: "uncheck-box")
        let btnCheck = UIButton(image: image, target: self, action: #selector(selectPlayList(_:)))
        btnCheck.tag = section
        let lblTitle = UILabel()
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17)
        let lblDuration = UILabel()
        lblDuration.font = UIFont.systemFont(ofSize: 12)
        lblDuration.textAlignment = .right
        lblDuration.textColor = .gray
        
        headerView.addSubview(lblTitle)
        headerView.addSubview(lblDuration)
        headerView.addSubview(btnCheck)
        
        headerView.addConstraintsWithFormat(format: "H:|-12-[v0(20)]-12-[v1]-8-[v2]-12-|", views: btnCheck, lblTitle, lblDuration)
        headerView.addConstraintsWithFormat(format: "V:|[v0]|", views: lblTitle)
        headerView.addConstraintsWithFormat(format: "V:|[v0]|", views: lblDuration)
        headerView.addConstraintsWithFormat(format: "V:|-10-[v0]-10-|", views: btnCheck)
        if segmentSongSource.selectedSegmentIndex == 0 {
            let playList = cloudPlayLists[section]
            lblTitle.text = playList.bgmName
            lblDuration.text = playList.duration
        } else {
            lblTitle.text = "Total \(deviceSongs.count) songs on current device"
            let totalTimeInterval = deviceSongs.reduce(0) { (sum, item) -> TimeInterval in
                sum + item.trackDuration
            }
            lblDuration.text = "(\(totalTimeInterval.stringFromTimeInterval()))"
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BGMTableViewCell
        cell.btnPreview.tag = indexPath.row
        cell.btnPreview.removeTarget(self, action: #selector(previewSong(_:)), for: .touchUpInside)
        cell.btnPreview.removeTarget(self, action: #selector(previewTrack(_:)), for: .touchUpInside)
        if segmentSongSource.selectedSegmentIndex == 0 {
            let track = cloudPlayLists[indexPath.section].tracks[indexPath.row]
            cell.btnPreview.addTarget(self, action: #selector(previewTrack(_:)), for: .touchUpInside)
            cell.curTrack = track
        } else {
            let songItem = deviceSongs[indexPath.row]
            cell.btnPreview.addTarget(self, action: #selector(previewSong(_:)), for: .touchUpInside)
            cell.curSongItem = songItem
            cell.btnSelection.tag = indexPath.row
            cell.btnSelection.addTarget(self, action: #selector(selectDeviceSong(_:)), for: .touchUpInside)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentSongSource.selectedSegmentIndex == 1 {
            var song = deviceSongs[indexPath.row]
            song.selected = !song.selected
            deviceSongs[indexPath.row] = song
            if let cell = tableView.cellForRow(at: indexPath) as? BGMTableViewCell {
                let image = song.selected ? #imageLiteral(resourceName: "check-box") : #imageLiteral(resourceName: "uncheck-box")
                cell.btnSelection.setImage(image, for: .normal)
            }
        }
    }
    @objc func selectDeviceSong(_ sender: UIButton) {
        var song = deviceSongs[sender.tag]
        song.selected = !song.selected
        deviceSongs[sender.tag] = song
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    @objc func selectPlayList(_ sender: UIButton) {
        let index = sender.tag
        var playList = cloudPlayLists[index]
        playList.selected = !playList.selected
        if playList.selected {
            sender.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
        }
        cloudPlayLists[index] = playList
    }
    @objc func previewTrack(_ sender: UIButton?) {
            if player.isPlaying && sender == curButton {
                player.pause()
                tableView.reloadData()
            } else if
                let cell = sender?.superview?.superview as? BGMTableViewCell,
                let track = cell.curTrack,
                let url = URL(string: track.trackUrl) {
                if let btn = curButton {
                    btn.setImage(#imageLiteral(resourceName: "note30"), for: .normal)
                    btn.layer.borderColor = UIColor.clear.cgColor
                }
                player = AVPlayer(url: url)
                player.play()
                curButton = sender
                sender?.layer.borderColor = UIColor.lightGray.cgColor
                sender?.setImage(#imageLiteral(resourceName: "stop30"), for: .normal)
            }
    }
    
    @objc func previewSong(_ sender: UIButton?) {
        if myMediaPlayer.currentPlaybackRate > 0 {
            myMediaPlayer.stop()
            tableView.reloadData()
        } else {
            let id = deviceSongs[(sender?.tag)!].persistentID
            if let curItem = (MPMediaQuery.songs().items?.filter{$0.persistentID == id})?.first {
                let mediaCollection = MPMediaItemCollection(items: [curItem])
                myMediaPlayer.setQueue(with: mediaCollection)
                myMediaPlayer.nowPlayingItem = mediaCollection.items[0]
                myMediaPlayer.prepareToPlay()
                myMediaPlayer.play()
                curButton = sender
                sender?.layer.borderColor = UIColor.lightGray.cgColor
                sender?.setImage(#imageLiteral(resourceName: "stop30"), for: .normal)
            }
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

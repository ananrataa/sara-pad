//
//  StartBookReadingViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 2/8/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit
import RxCocoa
import RxSwift
import AVFoundation

enum BookReadingStatus {
    case play
    case pause
    case stop
    case navigate
    case config
}

@available(iOS 11.0, *)
class StartBookReadingViewController: UIViewController, AlertHandler {
    let isEng:Bool = Locale.preferredLanguages[0] == "en"
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var svPlayControl: UIStackView!
    @IBOutlet weak var timeSlider: DesignableSlider!
    @IBOutlet weak var progressTime: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    @IBOutlet weak var timeStackView: UIStackView!
    @IBOutlet weak var btnRestart: UIButton!
    @IBOutlet weak var timeBackgroundView: UIViewX!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var curPlayerStatus: BookReadingStatus = .stop {
        didSet {
            btnPause.setImage(curPlayerStatus == .play ? #imageLiteral(resourceName: "pause40x40") : #imageLiteral(resourceName: "play40x40"), for: .normal)
        }
    }
    var currentPauseTime: CMTime?
    var tvStop: TimeInterval!
    var deltaTime: TimeInterval!
    
    var pdfDocument: PDFDocument!
    var voicePlayer: AVPlayer?
    var curVoiceIndex: Int = 0
    var lastTime: Double = 0
    let disposeBag = DisposeBag()
    var timeObserverToken: Any?
    var mediaId: String
    var mediaList: SPSelectedMediaVoices
    var voices: [SPMediaVoice]
    var totalVoices: [SPMediaVoice] = []
    var startPage: PDFPage?
    var endPage: PDFPage?
    var startInfo: SPMediaVoicePageInfo?
    var curPage: PDFPage?
    var isChapterChanged: Bool = false
    var isLastChapter: Bool = false
    var shouldResumePlay: Bool = false
    var helpInfo = true
    let lblDuration: UILabel = {
        let font = UIFont(name: sukhumvitLightFont, size: 15)
        let lbl = DarkTheme.shared.createLabel(with: font, alignment: .right)
        return lbl
    }()
    var curVoice: SPMediaVoice!
    var selectedItem: SPSelectedMediaVoices?
    var curPageInfos: [SPMediaVoicePageInfo] = []
    var timer: Timer!
    var pageChanged: ((PDFPage, SPSelectedMediaVoices)->())?
    //Reading time
    var ntfResign: NSObjectProtocol?
    var ntfEnter: NSObjectProtocol?
    var pageStart: DispatchTime?
    var memberId: String = {
        var id = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            id = DBManager.selectMemberID()
        }
        return id
    }()
    init(mediaId: String, mediaList: SPSelectedMediaVoices) {
        self.mediaId = mediaId
        self.mediaList = mediaList
        self.voices = mediaList.voices
        self.totalVoices = voices
        self.startPage = mediaList.startPage
        self.endPage = mediaList.endPage
        super.init(nibName: nil, bundle: nil)
        
        // this is needed to the audio can play even when the "silent/vibrate" toggle is on
        let session = AVAudioSession.sharedInstance()
//        try? session.setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playback))
        try? session.setCategory(.playback, mode: .default, options: [.duckOthers])
        try? session.setActive(true)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        
        LightTheme.shared.applyNavigationBar(vc: self)
        let barTitleFont = UIFont(name: sukhumvitLightFont, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .light)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: barTitleFont, NSAttributedString.Key.foregroundColor: LightTheme.shared.barTitleTint]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "reading_config"), style: .done, target: self, action: #selector(config))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if selectedItem == nil {
            setControlView()
            setup()
        } else if let voiceSelected = selectedItem, voiceSelected.source >= 0 {
            mediaList = voiceSelected
            if voiceSelected.source == 0 {
                //record voices by Page
                if voiceSelected.voices.count > 0 {
                    clearPlayer()
                    totalVoices = voiceSelected.voices
                    voices = totalVoices
                    startPage = voiceSelected.startPage
                    endPage = voiceSelected.endPage
                    startFirstPageReading()
                }
            } else if voiceSelected.source == 1 {
                //synthesizer voices
                clearPlayer()
                if let pdf = pdfDocument {
                    let vc = RobotReadingViewController(mediaId: mediaId, pdfDoc: pdf, mediaInfo: voiceSelected)
                    navigationController?.pushViewController(vc, animated: false)
                    navigationController?.viewControllers[0].removeFromParent()
                }
            }
        } else {
            if shouldResumePlay {
                pausePlay(btnPause)
                shouldResumePlay = false
            }
        }
        selectedItem = nil
        if pageStart == nil {
            pageStart = DispatchTime.now()
        }
        
        //Reading time observer
        self.ntfResign = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("resign active")
                if let page = self?.pdfView.currentPage {
                    self?.saveReadingData(page: page)
                    self?.pageStart = nil
                }
            }
        }
        self.ntfEnter = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("enter active")
                if self?.pageStart == nil {
                    self?.pageStart = DispatchTime.now()
                }
            }
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if curPlayerStatus != .config {
            if let callBack = self.pageChanged, let page = pdfView.currentPage {
                callBack(page, mediaList)
            }
            NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: nil)
            UIApplication.shared.isIdleTimerDisabled = false
            clearPlayer()
        }
        //save current reading page
        if let page = pdfView.currentPage {
            saveReadingData(page: page)
            pageStart = nil
        }
        
        //remover observer
        if let ntf = ntfResign {
            NotificationCenter.default.removeObserver(ntf)
        }
        if let ntf = ntfEnter {
            NotificationCenter.default.removeObserver(ntf)
        }

    }
    func resume(voiceSelected: SPSelectedMediaVoices) {
        if shouldResumePlay {
            pausePlay(btnPause)
            shouldResumePlay = false
        }
        if voiceSelected.source == 0 {
            //record voices by Page
            if voiceSelected.voices.count > 0 {
                totalVoices = voiceSelected.voices
                voices = totalVoices
                startPage = voiceSelected.startPage
                endPage = voiceSelected.endPage
                startFirstPageReading()
            }
        } else if voiceSelected.source == 1 {
            //record voices by Chapter
            if voiceSelected.voices.count > 0 {
                totalVoices = voiceSelected.voices
                voices = totalVoices
                startPage = nil
                startFirstPageReading()
            }
        } else {
            //synthesizer voices
        }
    }
    
    func setup() {
        btnRestart.alpha = 0
        lblPage.layer.cornerRadius = 7
        
        //Loaf PDF document start read first chapter
        if let media = DB.getMedia(id: mediaId) {
            self.title = media.mediaTitle ?? ""
            MediaModel.shared.createMediaPdfObservable(id: mediaId)
                .subscribe(onNext: {[weak self] (pdfDoc) in
                    if let pdfDoc = pdfDoc {
                        self?.pdfDocument = pdfDoc
                        self?.pdfView.document = pdfDoc
                        self?.pdfView.autoScales = true
                        self?.pdfView.displayMode = .singlePageContinuous
                        self?.pdfView.displayDirection = .horizontal
                        self?.pdfView.usePageViewController(true, withViewOptions: [convertFromUIPageViewControllerOptionsKey(UIPageViewController.OptionsKey.interPageSpacing): 8])
                        self?.startFirstPageReading()
                    }
                    SVProgressHUD.dismiss()
                }).disposed(by: disposeBag)
        }
        
        //pdfView Notification
        NotificationCenter.default.addObserver(forName: .PDFViewPageChanged, object: nil, queue: nil) {[weak self] (_) in
            if let page = self?.pdfView.currentPage {
                //save reading time & reset start reading time
                self?.saveReadingData(page: page)
                self?.pageStart = DispatchTime.now()
                
                if let curPage = self?.curPage, let totalPages = self?.pdfDocument.pageCount {
                    if page == curPage {
                        self?.lblPage.text = "  🗣 " + String.localizedString(key: "Page") + " \(page.pageNo + 1)/\(totalPages)  "
                    } else {
                        self?.lblPage.text = "  " + String.localizedString(key: "Page") + " \(page.pageNo + 1)/\(totalPages)  "
                    }
                }
                if self?.helpInfo ?? false, let curPage = self?.curPage, curPage != page {
                    self?.helpInfo = false
                    if self?.isEng ?? false {
                        self?.infoAlert(message: "Double taps to go back to current speaking page.", duration: 2)
                    } else {
                        self?.infoAlert(message: "🖕🏻 2 ครั้ง เพื่อกลับสู่หน้าที่กำลังอ่าน", duration: 2)
                    }
                }
            }
        }
    }
    
    func startFirstPageReading() {
        if let n = startPage?.pageNo,
            let nn = voices.firstIndex(where: {(n+1 >= $0.fromPage) && n+1 <= $0.toPage}) {
            let info = (voices[nn].timePages.filter({$0.page >= (n+1) && $0.pageLength > 2}).first) ?? voices[nn].timePages.last
            curVoice = voices[nn]
            startInfo = info
            voices = Array(totalVoices.dropFirst(nn+1))
            //curPageInfos will be set in seek time function. / flip page here
            if let page = pdfDocument.page(at: (info?.page)! - 1) {
                pdfView.go(to: page)
            }
        } else if voices.count > 0 {
            curVoice = voices.removeFirst()
            startInfo = curVoice.timePages.first
        }
        playBookReading(curVoice.voiceUrl)
        
        if svPlayControl.alpha == 0 {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations:
                {[weak self] in
                    self?.svPlayControl.alpha = 1
                    self?.lblPage.alpha = 1
                }, completion: {[weak self] (_) in
                    self?.btnRestart.alpha = 0
            })
        }
    }
    
    //MARK:- Seek Page
    @IBAction func changePage(_ sender: DesignableSlider) {
        if let totalCMTime = self.voicePlayer?.currentItem?.duration {
            let time = totalCMTime.seconds * Double(sender.value)
            seekTime(time)
        }
    }
    
    func seekTime(_ seekTime: Double) {
        if let n = curVoice.timePages.firstIndex(where: {seekTime <= $0.seconds}) {
            curPageInfos = Array(curVoice.timePages.dropFirst(n))
            if curPageInfos.count > 0 {
                let curInfo = curPageInfos.removeFirst()
                lastTime = curInfo.seconds
                let timeScale = voicePlayer?.currentTime().timescale
                voicePlayer?.seek(to: CMTime.init(seconds: curInfo.seconds, preferredTimescale: timeScale!), completionHandler:
                    {[weak self] (finished) in
                        if finished {
                            if let timer = self?.timer {
                                timer.invalidate()
                                self?.timer = nil
                            }
                            self?.setPageTimer(pageInfo: curInfo)
                        }
                })
            }
        }
    }
    
    //Observer when AVPlayer is really start
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let avPlayer = voicePlayer, object as AnyObject? === voicePlayer {
            if keyPath == "status" {
                if avPlayer.status == .readyToPlay {
                    voicePlayer?.rate = 1.0
                    avPlayer.play()
                }
            } else if keyPath == "timeControlStatus" {
                if avPlayer.timeControlStatus == .playing {
                    SVProgressHUD.dismiss()
                    enableControlButtons(enabled: true)
                    curPlayerStatus = .play
                    isLastChapter = (voices.count == 0)
                    if let info = startInfo {
                        seekTime(info.seconds)
                        startInfo = nil
                    }
                    if isChapterChanged {
                        isChapterChanged = false
                        if self.curPageInfos.count > 1 {
                            let info = self.curPageInfos.removeFirst()
                            self.lastTime = info.seconds
                            self.setPageTimer(pageInfo: info)
                        }
                    }
                    //set TimeObserver to update reading progress
                    let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
                    timeSlider.value = 0
                    self.title = curVoice.voiceName
                    timeObserverToken = voicePlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) {
                        [weak self] time in
                        
                        if let totalCMTime = self?.voicePlayer?.currentItem?.duration,
                            let curCMTime = self?.voicePlayer?.currentItem?.currentTime() {
                            //timeSlider progress
                            DispatchQueue.main.async {
                                self?.timeSlider.value = Float(curCMTime.seconds/totalCMTime.seconds)
                                self?.progressTime.text = CMTimeGetSeconds(curCMTime).stringFromTimeIntervalMMss()
                                if totalCMTime.isValid && curCMTime.isValid && totalCMTime != CMTime.indefinite {
                                    self?.remainTime.text = "-\((CMTimeGetSeconds(totalCMTime - curCMTime)).stringFromTimeIntervalMMss())"
                                }
                            }
                        }
                    }
                    
                    SVProgressHUD.dismiss()
                }
            } else if keyPath == "rate" {
                if avPlayer.rate > 0 {
                    curPlayerStatus = .play
                } else {
                    curPlayerStatus = .stop
                }
            }
        }
    }
}

//MARK:- Play Chapter
@available(iOS 11.0, *)
extension StartBookReadingViewController {
    func playBookReading(_ strUrl:String) {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        stopReading()
        guard let url = URL.init(string: strUrl)
            else {
                print("Bgm Cloud url error")
                SVProgressHUD.dismiss()
                return
        }
        let playerItem:AVPlayerItem = AVPlayerItem(url: url)
        voicePlayer = AVPlayer(playerItem: playerItem)
        //Check if AVPlayer is really start
        voicePlayer?.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)
        voicePlayer?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
        voicePlayer?.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
        
        
        //remove previous observer
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        //Chapter End
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: nil)
        {[weak self] (_) in
            //progressView
            self?.timeSlider.value = (self?.timeSlider.maximumValue)!
            
            if let token = self?.timeObserverToken {
                self?.voicePlayer?.removeTimeObserver(token)
                self?.timeObserverToken = nil
            }
            if let timer = self?.timer {
                timer.invalidate()
                self?.timer = nil
            }
            self?.remainTime.text = "--:--"
            if (self?.voices.first) != nil {
                DispatchQueue.main.async {
                    SVProgressHUD.show(withStatus: "Loading...")
                }
                self?.curVoice = self?.voices.removeFirst()
                self?.curPageInfos = (self?.curVoice.timePages)!
                self?.isChapterChanged = true
                self?.playBookReading((self?.curVoice.voiceUrl)!)
            }
            //Check if is lastChapter
            if (self?.isLastChapter)! {
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations:
                    {[weak self] in
                        self?.svPlayControl.alpha = 0
                        self?.lblPage.alpha = 0
                        self?.timeStackView.alpha = 0
                        self?.timeBackgroundView.alpha = 0
                    }, completion: {[weak self] (_) in
                        UIView.animate(withDuration: 0.3, animations: {
                            self?.btnRestart.alpha = 1
                        })
                        SVProgressHUD.showInfo(withStatus: String.localizedString(key: "read_complete"))
                        self?.isLastChapter = false
                })
            }
        }
    }
    
    func stopReading() {
        if let voicePlayer = voicePlayer, voicePlayer.isPlaying {
            voicePlayer.pause()
            voicePlayer.rate = 0.0
            voicePlayer.replaceCurrentItem(with: nil)
        }
    }
    
    private func setPageTimer(pageInfo: SPMediaVoicePageInfo) {
        if let pdfPage = pdfDocument.page(at: pageInfo.page - 1) {
            curPage = pdfPage
            self.pdfView.go(to: pdfPage)
        }
        if let info = self.curPageInfos.first {
            tvStop = Date().timeIntervalSinceReferenceDate
            timer = Timer.scheduledTimer(withTimeInterval: info.seconds - lastTime, repeats: false, block:
                {[weak self] (timer) in
                    self?.lastTime = info.seconds
                    if (self?.curPageInfos.count ?? 0) > 0, let nextInfo = self?.curPageInfos.removeFirst() {
                        self?.setPageTimer(pageInfo: nextInfo)
                    }
            })
        }
    }
}
//MARK:- Puase/Play/Next/Back/Restart
@available(iOS 11.0, *)
extension StartBookReadingViewController {
    func setControlView() {
        //time slider
        timeSlider.minimumTrackTintColor = UIColor(hexString: ThemeTemplate.getThemeSet3())
        //Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleControl))
        pdfView.addGestureRecognizer(tapGesture)
        let dblGesture = UITapGestureRecognizer(target: self, action: #selector(currentSpeakingPage))
        dblGesture.numberOfTapsRequired = 2
        pdfView.addGestureRecognizer(dblGesture)
    }
    @objc func toggleControl() {
        guard btnRestart.alpha == 0 else {return}
        if svPlayControl.alpha == 0 {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations:
                {[weak self] in
                    self?.svPlayControl.alpha = 1
                    self?.lblPage.alpha = 1
                }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations:
                {[weak self] in
                    self?.svPlayControl.alpha = 0
                    self?.lblPage.alpha = 0
                }, completion: nil)
        }
    }
    @objc func currentSpeakingPage() {
        if let page = curPage {
            pdfView.go(to: page)
            toggleControl()
        }
    }
    func enableControlButtons(enabled: Bool) {
        btnBack.isEnabled = enabled
        btnNext.isEnabled = enabled
    }
    
    @IBAction func back(_ sender: Any) {
        //Previous chapter
        let n1 = totalVoices.count - 1
        let n2 = voices.count + 1
        if n1 - n2 >= 0 {
            enableControlButtons(enabled: false)
            //add previous voice -> voices
            voices = Array(totalVoices.dropFirst(n1-n2))
            clearPlayer()
            curVoice = voices.removeFirst()
            curPageInfos = curVoice.timePages
            isChapterChanged = true
            playBookReading(curVoice.voiceUrl)
        } else {
            SVProgressHUD.showInfo(withStatus: String.localizedString(key: "first_chapter_info"))
        }
    }
    @IBAction func next(_ sender: Any) {
        //Next chapter
        if voices.count > 0 {
            enableControlButtons(enabled: false)
            clearPlayer()
            curVoice = voices.removeFirst()
            curPageInfos = curVoice.timePages
            isChapterChanged = true
            playBookReading(curVoice.voiceUrl)
        } else {
            SVProgressHUD.showInfo(withStatus: String.localizedString(key: "last_chapter_info"))
        }
    }
    
    @IBAction func pausePlay(_ sender: UIButton) {
        if currentPauseTime != nil {
            //resume timer
            if let info = self.curPageInfos.first {
                timer = Timer.scheduledTimer(withTimeInterval: info.seconds - lastTime - deltaTime, repeats: false, block:
                    {[weak self] (timer) in
                    self?.lastTime = info.seconds
                    if (self?.curPageInfos.count ?? 0) > 0, let nextInfo = self?.curPageInfos.removeFirst() {
                        self?.setPageTimer(pageInfo: nextInfo)
                    }
                })
            }
            
            voicePlayer?.play()
            currentPauseTime = nil
            sender.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
            curPlayerStatus = .play
        } else {
            //stop current timer
            if let timer = timer {
                timer.invalidate()
                self.timer = nil
            }
            if let tvStop = tvStop {
                deltaTime = Date().timeIntervalSinceReferenceDate - tvStop
                currentPauseTime = voicePlayer?.currentTime()
            }
            voicePlayer?.pause()
            tvStop = Date().timeIntervalSinceReferenceDate
            sender.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
            curPlayerStatus = .pause
        }
    }
    
    @IBAction func restart(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {[weak self] in
            self?.btnRestart.alpha = 0
        }) {[weak self] (_) in
            UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseIn, animations: {[weak self] in
                self?.timeStackView.alpha = 1
                self?.timeBackgroundView.alpha = 1
                self?.svPlayControl.alpha = 1
            }, completion: nil)
        }
        voices = totalVoices
        isLastChapter = false
        if voices.count > 0 {
            curVoice = voices.removeFirst()
            curPageInfos = curVoice.timePages
            isChapterChanged = true
            playBookReading(curVoice.voiceUrl)
        }
    }
}

//MARK:- Media Selection
@available(iOS 11.0, *)
extension StartBookReadingViewController {
    func mediaDidSelect(item: SPSelectedMediaVoices) {
        selectedItem = item
    }
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    @objc func config() {
        self.title = ""
        if curPlayerStatus == .play {
            pausePlay(btnPause)
            shouldResumePlay = true
        }
        curPlayerStatus = .config
        let page = curPage ?? pdfDocument.page(at: 0)
        let selectMediaVC = SelectMediaVoiceViewController(mediaId: mediaId, pdfDoc: pdfDocument, startPage: page!)
        selectMediaVC.selectMediaCallback = mediaDidSelect
        navigationController?.pushViewController(selectMediaVC, animated: true)
    }
    func clearPlayer() {
        stopReading()
        if let token = timeObserverToken {
            voicePlayer?.removeTimeObserver(token)
            timeObserverToken = nil
        }
        if let timer = timer {
            timer.invalidate()
            self.timer = nil
        }
        //Remove observer
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        if let player = voicePlayer {
            player.removeObserver(self, forKeyPath: "status")
            player.removeObserver(self, forKeyPath: "timeControlStatus")
            player.removeObserver(self, forKeyPath: "rate")
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIPageViewControllerOptionsKey(_ input: UIPageViewController.OptionsKey) -> String {
	return input.rawValue
}

extension StartBookReadingViewController {
    func saveReadingData(page: PDFPage) {
        //actual page number
        let actualPageNumber = SP.shared.actualPageNumber(page: page) + 1
        //compute time reading in seconds
        if let start = self.pageStart {
            let seconds = Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
            print("Page-\(actualPageNumber): \(seconds) seconds")
            
            let data = ReadingData()
            data.memberId = memberId
            data.mediaId = mediaId
            data.date = Date().getDateString(with: "yyyyMMdd")
            data.page = actualPageNumber//index 1 base
            data.duration = Double(Int(1000*seconds))/1000
            if data.duration > 0 {
                DB.saveReadingData(data: data)
            }
        }
    }
}

//
//  RobotReadingViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 22/8/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit
import RxSwift
import RxCocoa

@available(iOS 11.0, *)
class RobotReadingViewController: UIViewController, AlertHandler {
    let isEng:Bool = Locale.preferredLanguages[0] == "en"
    
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var svVoiceControl: UIStackView!
    
    let disposeBag = DisposeBag()
    var sourceText: String = ""
    var targetText: String = ""
    var speechSynthesizer: AVSpeechSynthesizer = AVSpeechSynthesizer()
    var newMediaInfo: SPSelectedMediaVoices?
    var textComponents:[String] = []
    var shouldResumePlay = false
    var helpInfo = true
    var curPage: PDFPage?
    var scrollingPage: PDFPage?
    
    var mediaId: String
    var pdfDoc: PDFDocument
    var mediaInfo: SPSelectedMediaVoices
    var readingStatus: BookReadingStatus = .stop
    var lastRect: CGRect!
    var pageChanged: ((PDFPage, SPSelectedMediaVoices)->())?
    //Reading time
    var ntfResign: NSObjectProtocol?
    var ntfEnter: NSObjectProtocol?
    var pageStart: DispatchTime?
    var memberId: String = {
        var id = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            id = DBManager.selectMemberID()
        }
        return id
    }()
    
    init(mediaId: String, pdfDoc: PDFDocument, mediaInfo: SPSelectedMediaVoices) {
        self.mediaId = mediaId
        self.pdfDoc = pdfDoc
        self.mediaInfo = mediaInfo
        super.init(nibName: nil, bundle: nil)
        // this is needed to the audio can play even when the "silent/vibrate" toggle is on
        let session = AVAudioSession.sharedInstance()
//        try? session.setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playback))
        try? session.setCategory(.playback, mode: .default)
        try? session.setActive(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        speechSynthesizer.delegate = self
        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let source = newMediaInfo, source.source >= 0 {
            mediaInfo = source
            newMediaInfo = nil
            if source.source == 0 {
                speechSynthesizer.delegate = nil
                speechSynthesizer.stopSpeaking(at: .immediate)
                let vc = StartBookReadingViewController(mediaId: mediaId, mediaList: source)
                navigationController?.pushViewController(vc, animated: false)
                navigationController?.viewControllers[0].removeFromParent()
            } else {
                btnPause.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
                if let n = source.startPage?.pageNo, let page = pdfDoc.page(at: n) {
                    curPage = page
                    readingStatus = .navigate
                    prepareText(page: page)
                    pdfView.go(to: page)
                }
            }
        } else if shouldResumePlay {
            shouldResumePlay = false
            speechSynthesizer.continueSpeaking()
            btnPause.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
        }
        if pageStart == nil {
            pageStart = DispatchTime.now()
        }
        
        //Reading time observer
        self.ntfResign = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("resign active")
                if let page = self?.pdfView.currentPage {
                    self?.saveReadingData(page: page)
                    self?.pageStart = nil
                }
            }
        }
        self.ntfEnter = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: nil) {[weak self] (_) in
            if self != nil {
                print("enter active")
                if self?.pageStart == nil {
                    self?.pageStart = DispatchTime.now()
                }
            }
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if readingStatus != .config {
            if let callBack = self.pageChanged, let page = pdfView.currentPage {
                callBack(page, mediaInfo)
            }
            NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: nil)
            speechSynthesizer.delegate = nil
            speechSynthesizer.pauseSpeaking(at: .immediate)
            speechSynthesizer.stopSpeaking(at: .immediate)
            
            //set back idleTime
            UIApplication.shared.isIdleTimerDisabled = false
            
            //save current reading page
            if let page = pdfView.currentPage {
                saveReadingData(page: page)
                pageStart = nil
            }
        }
        
        //remover observer
        if let ntf = ntfResign {
            NotificationCenter.default.removeObserver(ntf)
        }
        if let ntf = ntfEnter {
            NotificationCenter.default.removeObserver(ntf)
        }

    }
    
    func setup() {
        UIApplication.shared.isIdleTimerDisabled = true
        
        LightTheme.shared.applyNavigationBar(vc: self)
        let barTitleFont = UIFont(name: sukhumvitLightFont, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .light)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: barTitleFont, NSAttributedString.Key.foregroundColor: LightTheme.shared.barTitleTint]
        if let media = DB.getMedia(id: mediaId) {
            self.title = media.mediaTitle
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "reading_config"), style: .done, target: self, action: #selector(config))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        pdfView.document = pdfDoc
        pdfView.autoScales = true
        pdfView.displayMode = .singlePageContinuous
        pdfView.displayDirection = .horizontal
        pdfView.usePageViewController(true, withViewOptions: [convertFromUIPageViewControllerOptionsKey(UIPageViewController.OptionsKey.interPageSpacing):8])
        
        lblPage.layer.cornerRadius = 7
        //pdfView Notification
        NotificationCenter.default.addObserver(forName: .PDFViewPageChanged, object: nil, queue: nil) {[weak self] (_) in
            if let page = self?.pdfView.currentPage {
                //save reading time & reset start reading time
                self?.saveReadingData(page: page)
                self?.pageStart = DispatchTime.now()

                if let curPage = self?.curPage, let totalPages = self?.pdfDoc.pageCount {
                    if page == curPage {
                        self?.lblPage.text = "  🗣 " + String.localizedString(key: "Page") + " \(page.pageNo + 1)/\(totalPages)  "
                    } else {
                        self?.lblPage.text = "  " + String.localizedString(key: "Page") + " \(page.pageNo + 1)/\(totalPages)  "
                    }
                }
                
                self?.scrollingPage = page
                if self?.helpInfo ?? false, let curPage = self?.curPage, curPage != page {
                    self?.helpInfo = false
                    if self?.isEng ?? false {
                        self?.infoAlert(message: "Double taps to go back to current speaking page.", duration: 2)
                    } else {
                        self?.infoAlert(message: "🖕🏻 2 ครั้ง เพื่อกลับสู่หน้าที่กำลังอ่าน", duration: 2)
                    }
                }
            }
        }
        
        //Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleControl))
        pdfView.addGestureRecognizer(tapGesture)
        let dblGesture = UITapGestureRecognizer(target: self, action: #selector(currentSpeakingPage))
        dblGesture.numberOfTapsRequired = 2
        pdfView.addGestureRecognizer(dblGesture)
        
        if let page = mediaInfo.startPage {
            curPage = page
            prepareText(page: page)
            pdfView.go(to: page)
        }
    }
   
    @objc func done() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func config() {
        readingStatus = .config
        self.title = ""
        speechSynthesizer.pauseSpeaking(at: .immediate)
        btnPause.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
        shouldResumePlay = true
        let page = curPage ?? pdfDoc.page(at: 0)
        let selectMediaVC = SelectMediaVoiceViewController(mediaId: mediaId, pdfDoc: pdfDoc, startPage: page!, language: mediaInfo.language)
        selectMediaVC.selectMediaCallback = configCallback
        navigationController?.pushViewController(selectMediaVC, animated: true)
    }
    func configCallback(source: SPSelectedMediaVoices) {
        newMediaInfo = source
    }
}

@available(iOS 11.0, *)
extension RobotReadingViewController: AVSpeechSynthesizerDelegate {
    func readingPage(text: String) {
        lastRect = .zero
        speechSynthesizer.stopSpeaking(at: .immediate)
        let speechUtterance = AVSpeechUtterance(string:text)
        speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate
        speechUtterance.pitchMultiplier = 1
        speechUtterance.volume = 1.0
        speechUtterance.voice =  AVSpeechSynthesisVoice(language: mediaInfo.language)
        speechSynthesizer.speak(speechUtterance)
        currentSpeakingPage()
        DispatchQueue.main.async { [weak self] in
            self?.btnForward.isEnabled = true
            self?.btnBackward.isEnabled = true
        }
    }
    
    @IBAction func previous(_ sender: Any) {
        guard let n = curPage?.pageNo, n > 0 else {
            return
        }
        if let lastPage = pdfDoc.page(at: n - 1) {
            readingStatus = .navigate
            curPage = lastPage
            prepareText(page: lastPage)
        }
        
    }
    
    @IBAction func pause(_ sender: Any) {
        if readingStatus == .pause {
            if let scrollPage = scrollingPage, let curPage = curPage, scrollPage != curPage {
                self.curPage = scrollPage
                readingStatus = .navigate
                prepareText(page: scrollPage)
            } else {
                speechSynthesizer.continueSpeaking()
                readingStatus = .play
            }
        } else if readingStatus == .play {
            speechSynthesizer.pauseSpeaking(at: .immediate)
            readingStatus = .pause
        }
    }
    
    @IBAction func next(_ sender: Any) {
        guard let n = curPage?.pageNo, n < pdfDoc.pageCount - 1 else {
            return
        }
        if let nextPage = pdfDoc.page(at: n + 1) {
            readingStatus = .navigate
            curPage = nextPage
            prepareText(page: nextPage)
        }
    }
    @objc func toggleControl() {
        if svVoiceControl.alpha == 0 {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations:
                {[weak self] in
                    self?.svVoiceControl.alpha = 1
                    self?.lblPage.alpha = 1
                }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations:
                {[weak self] in
                    self?.svVoiceControl.alpha = 0
                    self?.lblPage.alpha = 0
                }, completion: nil)
        }
    }
    @objc func currentSpeakingPage() {
        if let page = curPage {
            pdfView.go(to: page)
//            toggleControl()
        }
    }
}

@available(iOS 11.0, *)
extension RobotReadingViewController {
    func prepareText(page: PDFPage) {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        //check if exceeds 1,500 chars
        DispatchQueue.main.async { [weak self] in
            self?.btnForward.isEnabled = false
            self?.btnBackward.isEnabled = false
        }
        targetText = ""
        sourceText = (page.attributedString?.string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)) ?? ""
        sourceText = String(cString: sourceText.cString(using: String.Encoding.utf8)!, encoding: String.Encoding.utf8)!
        
        sourceText = sourceText.replacingOccurrences(of: "\"", with: " ")

        
        
        
        
        if sourceText.count <= 0 {
            sourceText = Locale.preferredLanguages[0] == "th" ? "หน้านี้ว่าง หรือ เป็นรูปภาพ" : "This page is blank or only picture."
        }
        let sourceKey = sourceText.detectLanguage() ?? "auto"
        if sourceKey == mediaInfo.language {
            targetText = sourceText
            self.readingPage(text: self.targetText)
            SVProgressHUD.dismiss()
        } else {
            textComponents = sourceText.pairs(every: 1500)
            if textComponents.count > 0 {
                googleTranslate(text: textComponents.removeFirst()) {(finished) in
                    
                }
            }
        }
    }
    
    func googleTranslate(text: String, completion: @escaping (Bool)->()) {
        if InternetConnection.isConnectedToNetwork(){
            let sourceKey = text.detectLanguage() ?? "auto"
            
            GoogleModel.shared.createGoogleTranslationObservable(text: text, source: sourceKey, target: mediaInfo.language)
                .subscribe(onNext: {[unowned self] (value) in
                    self.targetText += value
                    //check if more text
                    if self.textComponents.count > 0 {
                        completion(false)
                        let text = self.textComponents.removeFirst()
                        self.googleTranslate(text: text){_ in }
                    } else {
                        //Refresh source & target
                        completion(true)
                        
                        self.readingPage(text: self.targetText)
                        SVProgressHUD.dismiss()
                    }
                }).disposed(by: disposeBag)
        }
        else{
            SVProgressHUD.showError(withStatus: "No internet connection!")
            completion(false)
        }
    }
}

@available(iOS 11.0, *)
extension RobotReadingViewController {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        btnPause.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
        readingStatus = .play
        SVProgressHUD.dismiss()
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        guard readingStatus != .navigate else {return}
        if let page = curPage, let lastPage = mediaInfo.endPage, page.pageNo < lastPage.pageNo {
            if let nextPage = pdfDoc.page(at: page.pageNo + 1) {
                curPage = nextPage
                prepareText(page: nextPage)
            }
        } else {
            speechSynthesizer.stopSpeaking(at: .immediate)
            SVProgressHUD.showSuccess(withStatus: String.localizedString(key: "read_complete"))
        }
        btnPause.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance) {
        btnPause.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
        readingStatus = .pause
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        btnPause.setImage(#imageLiteral(resourceName: "play40x40"), for: .normal)
        readingStatus = .stop
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance) {
        btnPause.setImage(#imageLiteral(resourceName: "pause40x40"), for: .normal)
        readingStatus = .play
    }
    
//    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
//        if let page = curPage, let pdf = pdfView.document, let text = utterance.speechString.substring(with: characterRange) {
//            let selections = pdf.findString(String(text), withOptions: .literal)
//            pdfView.setCurrentSelection(nil, animate: true)
//            for sel in selections {
//                if sel.pages.contains(page) {
//                    let rect = sel.bounds(for: page)
//                    //check if rect > last rect
//                    if rect > lastRect {
//                        pdfView.setCurrentSelection(sel, animate: true)
//                        lastRect = rect
//                    } else if lastRect == .zero {
//                        lastRect = rect
//                    }
//
//                    break
//                }
//            }
//        }
//
//    }
}

extension String {
    func substring(with nsrange: NSRange) -> Substring? {
        guard let range = Range(nsrange, in: self) else { return nil }
        return self[range]
    }
}

extension CGRect {
    static func > (lhs: CGRect, rhs: CGRect) -> Bool {
        if lhs.origin.y.isEqual(to: rhs.origin.y) {
            return (lhs.origin.x + lhs.width) > (rhs.origin.x + rhs.width)
        } else {
            return lhs.origin.y < rhs.origin.y
        }
    }
}

import CoreGraphics
@available(iOS 11.0, *)
extension RobotReadingViewController {
    func test() {
        guard let media = DB.getMedia(id: mediaId), let url = URL(string: media.fileUrl ?? "") else {
            return
        }
        
        let operatorTableRef = CGPDFOperatorTableCreate()
//        CGPDFOperatorTableSetCallback(operatorTableRef!, "BT") { (scanner, info) in
//            print("Begin text object")
//        }
//        CGPDFOperatorTableSetCallback(operatorTableRef!, "ET") { (scanner, info) in
//            print("End text object")
//        }
//        CGPDFOperatorTableSetCallback(operatorTableRef!, "Tf") { (scanner, info) in
//            print("Select font")
//        }
//        CGPDFOperatorTableSetCallback(operatorTableRef!, "Tj") { (scanner, info) in
//            print("Show text")
//            var text: CGPDFStringRef?
//            withUnsafeMutablePointer(to: &text, { (p) -> () in
//                let r = CGPDFScannerPopString(scanner, p)
//                if r {
//                    let string = String(cString: CGPDFStringGetBytePtr(p.pointee!)!)
//                    print(string)
//                }
//            })
//        }
        CGPDFOperatorTableSetCallback(operatorTableRef!, "TJ") { (scanner, info) in
            print("Show text, allowing individual glyph positioning")
            var pa: CGPDFArrayRef?
            withUnsafeMutablePointer(to: &pa, { (ppa) -> () in
                let r = CGPDFScannerPopArray(scanner, ppa)
                print("TJ", r)
                if r {
                    let count = CGPDFArrayGetCount(ppa.pointee!)
                    var j = 0
                    for i in 0..<count {
                        var str: CGPDFStringRef?
                        let r = CGPDFArrayGetString(ppa.pointee!, i, &str)
                        if r {
                            let string = String(cString: CGPDFStringGetBytePtr(str!)!)
                            print(string, i, j)
                            j += 1
                        }
                    }
                }
            })
        }
        
        var myDoc: CGPDFDocument!
        let cfUrl: CFURL = CFBridgingRetain(url) as! CFURL
        myDoc = CGPDFDocument(cfUrl)
        if !myDoc.isUnlocked, let password = Utility.generatePassword(media.originalFileName ?? "") {
            myDoc.unlockWithPassword(password)
        }
        let numOfPages = myDoc.numberOfPages
        for k in 0..<numOfPages {
            let myPage = myDoc.page(at: k)
            let stream = CGPDFContentStreamCreateWithPage (myPage!);
            let scanner = CGPDFScannerCreate(stream, operatorTableRef, nil)
            CGPDFScannerScan(scanner)
            CGPDFScannerRelease(scanner)
            CGPDFContentStreamRelease(stream)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIPageViewControllerOptionsKey(_ input: UIPageViewController.OptionsKey) -> String {
	return input.rawValue
}

extension RobotReadingViewController {
    func saveReadingData(page: PDFPage) {
        //actual page number
        let actualPageNumber = SP.shared.actualPageNumber(page: page) + 1
        //compute time reading in seconds
        if let start = self.pageStart {
            let seconds = Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
            print("Page-\(actualPageNumber): \(seconds) seconds")
            
            let data = ReadingData()
            data.memberId = memberId
            data.mediaId = mediaId
            data.date = Date().getDateString(with: "yyyyMMdd")
            data.page = actualPageNumber//index 1 base
            data.duration = Double(Int(1000*seconds))/1000
            if data.duration > 0 {
                DB.saveReadingData(data: data)
            }
        }
    }
}

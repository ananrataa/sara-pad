//
//  RobotVoiceTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
class RobotVoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var mediaName: UILabel!
    @IBOutlet weak var smOption: UISegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblCurrentLanguage: UILabel!
    @IBOutlet weak var btnSelectLanguage: UIButton!
    @IBOutlet weak var btnSelectAll: UIButton!
    
    var curKey = "th"
    var pageVC: PageSelectionViewController?
    var chapterVC: ChapterSelectionViewController?
    var pdfDoc: PDFDocument!
    var startPage: PDFPage?
    var endPage: PDFPage?
    var curMediaVoices: [SPMediaVoice] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 6
        bgView.backgroundColor = UIColor(white: 0.975, alpha: 1)
        bgView.layer.shadowOpacity = 0.25
        bgView.layer.shadowOffset = .init(width: 3, height: 3)
        
        imgView.layer.shadowOpacity = 0.25
        imgView.layer.shadowOffset = .init(width: 3, height: 3)

        //Language
        lblLanguage.text = String.localizedString(key: "choose_language")
        
//        playButton.layer.shadowOpacity = 0.25
//        playButton.layer.shadowOffset = .init(width: 3, height: 3)

        //segment control
        let font = UIFont(name: sukhumvitLightFont, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: .light)
        smOption.setTitleTextAttributes([NSAttributedString.Key.font:font], for: .normal)
        //scrollView
        scrollView.addBorder(width: 0.1, color: .clear, radius: 3)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func readOption(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if let pdf = pdfDoc, let startPage = startPage {
                setPageSelection(pdfDoc: pdf, startPage: startPage)
            }
        } else {
            setChapterSelection()
        }
        btnSelectAll.isHidden = sender.selectedSegmentIndex == 0 ? true : false
    }
    
    @IBAction func selectAllChapters(_ sender: UIButton) {
        if let vc = chapterVC {
            let isSelectAll = vc.selectionRows.count == vc.mediaVoices.count
            sender.setImage(!isSelectAll ? #imageLiteral(resourceName: "select_all") : #imageLiteral(resourceName: "select_none"), for: .normal)
            let n = vc.mediaVoices.count
            vc.selectionRows = []
            if !isSelectAll {
                for row in 0..<n {
                    let indexPath = IndexPath(row: row, section: 0)
                    vc.selectionRows.append(indexPath)
                }
            }
            vc.tableView.reloadData()
        }
        
    }
    
    func setPageSelection(pdfDoc: PDFDocument, startPage: PDFPage) {
        self.smOption.selectedSegmentIndex = 0
        self.pdfDoc = pdfDoc
        self.startPage = startPage
        scrollView.removeAllSubviews()
        let rect = scrollView.bounds
        pageVC = PageSelectionViewController(pdfDocument: pdfDoc, curPage: startPage)
        pageVC?.view.frame = rect
        scrollView.addSubview((pageVC?.view)!)
    }
    
    func setChapterSelection() {
        if let pdf = pdfDoc, let vc = pageVC {
            startPage = vc.startPage
            endPage = vc.endPage
            scrollView.removeAllSubviews()
            let rect = scrollView.bounds
            chapterVC = ChapterSelectionViewController(pdfDoc: pdf, media: curMediaVoices)
            chapterVC?.view.frame = rect
            scrollView.addSubview((chapterVC?.view)!)
        }
    }
    
}


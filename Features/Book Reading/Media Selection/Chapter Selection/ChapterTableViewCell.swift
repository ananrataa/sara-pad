//
//  ChapterTableViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 17/8/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class ChapterTableViewCell: UITableViewCell {
    @IBOutlet weak var lblChapter: UILabel!
    @IBOutlet weak var lblReader: UILabel!
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var cardView: UIViewX!
    
    var mediaVoice: SPMediaVoice? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func preview(_ sender: Any) {
    }
    
    
    func refreshView() {
        if let mv = mediaVoice {
            lblChapter.text = mv.voiceName
            lblReader.text = mv.reader
            lblPage.text = mv.page
            lblDuration.text = TimeInterval(exactly: mv.duration.toDouble()*60)?.stringFromTimeIntervalMMss()
        }
    }
}

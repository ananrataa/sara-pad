//
//  ChapterSelectionViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 15/8/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
class ChapterSelectionViewController: UIViewController {
    let cellId = "chapter_cell"
    
    @IBOutlet weak var tableView: UITableView!
    var pdfDoc: PDFDocument
    var mediaVoices: [SPMediaVoice]
    var selectionRows: [IndexPath] = []
    init(pdfDoc: PDFDocument, media: [SPMediaVoice]) {
        self.pdfDoc = pdfDoc
        self.mediaVoices = media
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib.init(nibName: "ChapterTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

}

@available(iOS 11.0, *)
extension ChapterSelectionViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaVoices.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ChapterTableViewCell
        
        let mv = mediaVoices[indexPath.row]
        if selectionRows.contains(indexPath) {
            cell.cardView.backgroundColor = UIColor(hexString: "#8DC248")
            cell.cardView.alpha = 1
        } else {
            cell.cardView.backgroundColor = .white
            cell.cardView.alpha = 0.5
        }
        cell.mediaVoice = mv
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let n = selectionRows.firstIndex(of: indexPath) {
            selectionRows.remove(at: n)
        } else {
            selectionRows.append(indexPath)
        }
        tableView.reloadData()
//        if let cell = tableView.cellForRow(at: indexPath) as? ChapterTableViewCell {
//            if let rows = tableView.indexPathsForSelectedRows, rows.contains(indexPath) {
//                cell.cardView.backgroundColor = UIColor(hexString: "#8DC248")
//                cell.cardView.alpha = 1
//            } else {
//                cell.cardView.backgroundColor = .white
//                cell.cardView.alpha = 0.5
//            }
//        }
    }
    
    
}

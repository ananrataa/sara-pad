//
//  PageSelectionViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 15/8/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
class PageSelectionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtPageFrom: UITextField!
    @IBOutlet weak var txtPageTo: UITextField!
    @IBOutlet weak var thumbFrom: UIImageView!
    @IBOutlet weak var thumbTo: UIImageView!
    @IBOutlet weak var lblFromPage: UILabel!
    @IBOutlet weak var lblToPage: UILabel!
    @IBOutlet weak var svHeightConstraint: NSLayoutConstraint!
    
    var lvFrom: ListItemTableViewController?
    var lvTo: ListItemTableViewController?
    var pages: [String] = []
    var pdfDocument: PDFDocument
    var startPage: PDFPage {
        didSet {
            txtPageFrom.text = String.localizedString(key: "Page") + " \(startPage.pageNo + 1)"
            thumbFrom.image = startPage.thumbnail(of: thumbFrom.frame.size, for: .mediaBox)
            view.sendSubviewToBack(thumbFrom)
        }
    }
    var endPage: PDFPage {
        didSet {
            txtPageTo.text = String.localizedString(key: "Page") + " \(endPage.pageNo + 1)"
            thumbTo.image = endPage.thumbnail(of: thumbTo.frame.size, for: .mediaBox)
            view.sendSubviewToBack(thumbTo)
        }
    }
    init(pdfDocument: PDFDocument, curPage: PDFPage) {
        self.pdfDocument = pdfDocument
        self.startPage = curPage
        self.endPage = pdfDocument.page(at: pdfDocument.pageCount - 1)!
        let strP = String.localizedString(key: "Page")
        let n = pdfDocument.pageCount
        pages = Array(1...n).map{strP + " \($0)"}
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.modelName.contains("iPhone SE") {
            svHeightConstraint.constant = 150
        } else {
            svHeightConstraint.constant = 192
        }
    }

    
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if thumbFrom.image == nil {
            txtPageFrom.text = String.localizedString(key: "Page") + " \(startPage.pageNo + 1)"
            thumbFrom.image = startPage.thumbnail(of: thumbFrom.frame.size, for: .mediaBox)
        }
        if thumbTo.image == nil {
            txtPageTo.text = String.localizedString(key: "Page") + " \(endPage.pageNo + 1)"
            thumbTo.image = endPage.thumbnail(of: thumbTo.frame.size, for: .mediaBox)
        }
        
        if lvFrom == nil {
            lblFromPage.text = String.localizedString(key: "Start")
            SP.shared.setTextFieldRightButton(image: #imageLiteral(resourceName: "text_img_triangle"), controller: self, textField: txtPageFrom, action: #selector(selectPageFrom))
            lvFrom = SP.shared.setListView(contentView: view, items: pages, textField: txtPageFrom)
            lvFrom?.delegate = self
            lvFrom?.curIndex = startPage.pageNo
        }
        
        if lvTo == nil {
            lblToPage.text = String.localizedString(key: "Stop")
            SP.shared.setTextFieldRightButton(image: #imageLiteral(resourceName: "text_img_triangle"), controller: self, textField: txtPageTo, action: #selector(selectPageTo))
            lvTo = SP.shared.setListView(contentView: view, items: pages, textField: txtPageTo)
            lvTo?.delegate = self
            lvTo?.curIndex = endPage.pageNo
        }
        
        //set listItem frame
        lvFrom?.viewRect = (txtPageFrom.superview?.convert(txtPageFrom.frame, to: view))!
        lvTo?.viewRect = (txtPageTo.superview?.convert(txtPageTo.frame, to: view))!
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let no = textField.text?.toInt(), no < pdfDocument.pageCount, no >= 0, let page = pdfDocument.page(at: no - 1) {
            if textField == txtPageFrom {
                startPage = page
                lvFrom?.curIndex = page.pageNo
            } else {
                endPage = page
                lvTo?.curIndex = page.pageNo
            }
        } else {
            textField.text = ""
            let str = "\n1 - \(pdfDocument.pageCount)"
            SVProgressHUD.showError(withStatus: String.localizedString(key: "page_input_error") + str)
            if textField == txtPageFrom {
                txtPageFrom.text = String.localizedString(key: "Page") + " \(startPage.pageNo + 1)"
            } else {
                txtPageTo.text = String.localizedString(key: "Page") + " \(endPage.pageNo + 1)"
            }
        }
    }
    @IBAction func toPageBack(_ sender: Any) {
        if let n = lvTo?.curIndex, n > 0, let page = pdfDocument.page(at: n-1) {
            endPage = page
            lvTo?.curIndex = n-1
        }
    }
    @IBAction func toPageNext(_ sender: Any) {
        if let n = lvTo?.curIndex, n < pdfDocument.pageCount - 1, let page = pdfDocument.page(at: n+1) {
            endPage = page
            lvTo?.curIndex = n+1
        }
    }
    @IBAction func fromPageBack(_ sender: Any) {
        if let n = lvFrom?.curIndex, n > 0, let page = pdfDocument.page(at: n-1) {
            startPage = page
            lvFrom?.curIndex = n-1
        }
    }
    @IBAction func fromPageNext(_ sender: Any) {
        if let n = lvFrom?.curIndex, n < pdfDocument.pageCount - 1, let page = pdfDocument.page(at: n+1) {
            startPage = page
            lvFrom?.curIndex = n+1
        }
    }
    
}

@available(iOS 11.0, *)
extension PageSelectionViewController: ListItemDelegate {
    func listItemSelected(listItem: ListItemTableViewController, index: Int) {
        if let page = pdfDocument.page(at: index) {
            if lvFrom == listItem {
                startPage = page
            } else {
                endPage = page
            }
        }
        listItem.tableView.isHidden = true
    }
    
    @objc func selectPageFrom() {
        lvFrom?.expand()
        lvFrom?.curIndex = startPage.pageNo
    }
    @objc func selectPageTo() {
        lvTo?.curIndex = endPage.pageNo
        lvTo?.expand()
        
    }
}

//
//  SelectMediaVoiceViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import AVFoundation
import SDWebImage
import PDFKit

@available(iOS 11.0, *)
class SelectMediaVoiceViewController: UIViewController, AlertHandler {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startReading: UIButton!
    
    let isEng:Bool = Locale.preferredLanguages[0] == "en"
    let cellId = "media_voice"
    let robotCellId = "robot_voice"
    var player: AVPlayer = AVPlayer()
    let disposeBag = DisposeBag()
    var mediaId: String
    var mediaVoices: [SPMediaVoiceList] = []
    var selectedIndexPaths: [IndexPath] = []
    var pages:[[String]] = []
    var timer: Timer?
    var selectMediaCallback: ((SPSelectedMediaVoices)->())?
    var curIndexPath: IndexPath?
    var pdfDocument: PDFDocument
    var curPage: PDFPage
    var curLanguageKey: String
    
    init(mediaId: String, pdfDoc: PDFDocument, startPage: PDFPage, language: String? = "th") {
        self.mediaId = mediaId
        self.pdfDocument = pdfDoc
        self.curPage = startPage
        self.curLanguageKey = language ?? "th"
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        setupUI()
        
        tableView.register(UINib.init(nibName: "MediaVoiceTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        tableView.register(UINib.init(nibName: "RobotVoiceTableViewCell", bundle: nil), forCellReuseIdentifier: robotCellId)
        
        MediaModel.shared.createMediaVoicesObservable(mediaId: mediaId)
            .subscribe(onNext: {[weak self] (items) in
                self?.mediaVoices = items
                self?.tableView.reloadData()
                SVProgressHUD.dismiss()
            }).disposed(by: disposeBag)
    }

    
    func setupUI() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.localizedString(key: "Cancel"), style: .plain, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: #selector(settings))
        
        LightTheme.shared.applyNavigationBar(vc: self)
        if isEng {
            navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "book_reading_title_en"))
        } else {
            navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "book_reading_title_th"))
        }
        //Reading button
        startReading.setTitle(String.localizedString(key: "Start Reading"), for: .normal)
        startReading.layer.borderColor = UIColor(named: "sp_darkGray")?.cgColor
        startReading.layer.cornerRadius = startReading.frame.height/2
        startReading.layer.borderWidth = 0.75
        startReading.layer.shadowOpacity = 0.25
        startReading.layer.shadowOffset = .init(width: 3, height: 3)
    }
    
    @objc func done() {
        dismiss(animated: true, completion: nil)
    }
    @objc func cancel() {
        if let callBack = selectMediaCallback {
            callBack(SPSelectedMediaVoices(source: -1, startPage: nil, endPage: nil, voices: [], language: curLanguageKey))
        }
        navigationController?.popViewController(animated: true)
    }
    @objc func settings() {
        
    }
    @objc func previewTrack(_ sender: UIButton?) {
        if player.isPlaying {
            if let timer = timer {
                timer.invalidate()
            }
            player.pause()
            tableView.reloadData()
        } else {
            let voiceSet = mediaVoices[(sender?.tag)!]
            if let track = voiceSet.mediaVoices.first {
                let url = URL(string: track.voiceUrl)
                player = AVPlayer(url: url!)
                player.play()
                sender?.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
                timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false, block: {
                    [weak self] (time) in
                    time.invalidate()
                    self?.player.pause()
                    self?.tableView.reloadData()
                })
            }
        }
    }
    
    @IBAction func startReading(_ sender: UIButton) {
        guard let indexPath = curIndexPath else {
            startReading.jitter()
            return
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? MediaVoiceTableViewCell, let callBack = selectMediaCallback {
            if cell.smOption.selectedSegmentIndex == 0,
                let indexPath = curIndexPath,
                let p1 = cell.pageVC?.startPage,
                let p2 = cell.pageVC?.endPage {
                
                let voices = mediaVoices[indexPath.row].mediaVoices                
                let item = SPSelectedMediaVoices(source: indexPath.section, startPage: p1, endPage: p2, voices: voices, language: curLanguageKey)
                callBack(item)
            }
            if cell.smOption.selectedSegmentIndex == 1,
                let indexPath = curIndexPath,
                let vc = cell.chapterVC {
                
                if vc.selectionRows.count == 0 {
                    startReading.jitter()
                    self.errorAlert(message: String.localizedString(key: "select voice"), duration: 2) {}
                    return
                } else {
                    var voices:[SPMediaVoice] = []
                    let voiceSet = mediaVoices[indexPath.row].mediaVoices
                    vc.selectionRows.forEach({ (index) in
                        voices.append(voiceSet[index.row])
                    })
                    
                    let item = SPSelectedMediaVoices(source: indexPath.section, startPage: nil, endPage: nil, voices: voices, language: curLanguageKey)
                    callBack(item)
                }
                
            }
            navigationController?.popViewController(animated: true)
        } else if let cell = tableView.cellForRow(at: indexPath) as? RobotVoiceTableViewCell, let callBack = selectMediaCallback {
            if cell.smOption.selectedSegmentIndex == 0,
                let indexPath = curIndexPath,
                let p1 = cell.pageVC?.startPage,
                let p2 = cell.pageVC?.endPage {
                let item = SPSelectedMediaVoices(source: indexPath.section, startPage: p1, endPage: p2, voices: [], language: curLanguageKey)
                callBack(item)
            }
            if cell.smOption.selectedSegmentIndex == 1,
                let indexPath = curIndexPath {
                let item = SPSelectedMediaVoices(source: indexPath.section, startPage: nil, endPage: nil, voices: [], language: curLanguageKey)
                callBack(item)
            }
            navigationController?.popViewController(animated: true)
        }
    }
}

@available(iOS 11.0, *)
extension SelectMediaVoiceViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? mediaVoices.count : 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath == curIndexPath ? 400 : 110// 115
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: robotCellId, for: indexPath) as! RobotVoiceTableViewCell
            cell.btnSelectAll.isHidden = true
            cell.mediaName.text = isEng ? "Synthesizer Voice" : "เสียงสังเคราะห์"
            cell.imgView.image = #imageLiteral(resourceName: "robot")
            cell.smOption.alpha = 0
            cell.btnSelectLanguage.addTarget(self, action: #selector(languageSelection(_:)), for: .touchUpInside)
            cell.lblCurrentLanguage.text = " " + SP.shared.getLanguageString(key: curLanguageKey)
            let touch = UITapGestureRecognizer(target: self, action: #selector(languageSelection(_:)))
            cell.lblCurrentLanguage.addGestureRecognizer(touch)
            if let path = curIndexPath, path == indexPath {
                cell.smOption.alpha = 1
                cell.bgView.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1).withAlphaComponent(0.1)
                cell.setPageSelection(pdfDoc: pdfDocument, startPage: curPage)
            } else {
                cell.smOption.alpha = 0
                cell.bgView.backgroundColor = .white
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MediaVoiceTableViewCell
            cell.btnSelectAll.isHidden = true
            let item = mediaVoices[indexPath.row]
            cell.mediaName.text = item.listName
            if isEng {
                cell.reader.text = "\(item.mediaVoices.count) chapter" + (item.mediaVoices.count > 1 ? "s" : "")
            } else {
                cell.reader.text = "\(item.mediaVoices.count) บท"
            }
                
            cell.pages.text = "\(item.totalPages())"
            cell.duration.text = item.totalDuration().stringFromTimeInterval()
            cell.imgView.image = #imageLiteral(resourceName: "human")
            if let path = curIndexPath, path == indexPath {
                cell.smOption.alpha = 1
                cell.bgView.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1).withAlphaComponent(0.1)
                cell.curMediaVoices = item.mediaVoices
                cell.setPageSelection(pdfDoc: pdfDocument, startPage: curPage)
            } else {
                cell.smOption.alpha = 0
                cell.bgView.backgroundColor = .white
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let path = curIndexPath {
            if path == indexPath {
                curIndexPath = nil
            } else {
                curIndexPath = indexPath
            }
        } else {
            curIndexPath = indexPath
        }
        tableView.reloadData()
        if indexPath.section == 1 {
            tableView.scrollToNearestSelectedRow(at: .bottom, animated: true)
        }
    }
}

@available(iOS 11.0, *)
extension SelectMediaVoiceViewController {
    @objc func languageSelection(_ sender: Any) {
        if curIndexPath != IndexPath(row: 0, section: 1) {
            curIndexPath = IndexPath(row: 0, section: 1)
            tableView.reloadData()
            tableView.scrollToNearestSelectedRow(at: .bottom, animated: true)
        }
        
        let langVC = LanguageViewController(curKey: curLanguageKey)
        langVC.selectionCallback = languageSelected
        langVC.modalPresentationStyle = .overCurrentContext
        self.present(langVC, animated: true, completion: nil)
    }
    
    func languageSelected(key: String) {
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? RobotVoiceTableViewCell {
            curLanguageKey = key
            cell.lblCurrentLanguage.text = " " + SP.shared.getLanguageString(key: key)
        }
    }
}

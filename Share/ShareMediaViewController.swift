//
//  ShareMediaViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 14/12/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class ShareMediaViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var stackView: UIStackView!
    
    let images: [UIImage] = [#imageLiteral(resourceName: "app_facebook") , #imageLiteral(resourceName: "app_line"), #imageLiteral(resourceName: "app_twitter"), #imageLiteral(resourceName: "app_mail"), #imageLiteral(resourceName: "app_copy")]
//    let images: [UIImage] = [#imageLiteral(resourceName: "share-facebook-icon.png") , #imageLiteral(resourceName: "share-line-icon.png"), #imageLiteral(resourceName: "share-twitter-icon.png"), #imageLiteral(resourceName: "share-email-icon.png"), #imageLiteral(resourceName: "copy-icon.png")]
    var shareSelected: ((Int)->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.alpha = 0
        collectionView.register(ShareCell.self, forCellWithReuseIdentifier: ShareCell.identification)
        buttonCancel.tintColor = UIColor(named: "sp_blue")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        blurView.alpha = 0.9
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let p = touches.first?.location(in: view) else {return}
        if p.y < stackView.frame.origin.y {
            dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true) {
            
        }
    }
    
}

extension ShareMediaViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShareCell.identification, for: indexPath) as! ShareCell
        
        cell.shareImageView.image = images[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dismiss(animated: true) {
            if let shareSelected = self.shareSelected {
                shareSelected(indexPath.row)
            }
        }
    }
}

class ShareCell: BaseCell {
    let shareImageView: UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleAspectFill
        imgV.tintColor = UIColor(white: 0.75, alpha: 1)
        return imgV
    }()
    
    override func setup() {
        self.addSubview(shareImageView)
        
        shareImageView.pin(to: self)
    }
}

//
//  RoomProfileLogo.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 29/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RoomProfileLogo : NSObject

+(void)setRoomProfileLogo:(UIImage *)img;
+(UIImage *)getRoomProfileLogo;

@end

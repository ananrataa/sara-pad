//
//  RoomProfileLogo.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 29/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "RoomProfileLogo.h"
#import <UIKit/UIKit.h>

@implementation RoomProfileLogo

UIImage *logo;

+(void)setRoomProfileLogo:(UIImage *)img{
    logo = img;
}

+(UIImage *)getRoomProfileLogo{
    return logo;
}


@end

//
//  Notification.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 27/12/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPNotification : NSObject{
    
    NSString *notificationID;
    NSString *notificationTitle;
    NSString *notificationBody;
    NSString *notificationSound;
    int notificationBadge;
    NSDictionary *notificationUserInfo;
    NSString *notificationAttachment;
    NSString *notificationLaunchImageName;
    int notificationDateTimePresent;
    BOOL notificationIsRepeat;
    
    /*
     title: String containing the primary reason for the alert.
     
     subtitle: String containing an alert subtitle (if required)
     
     body: String containing the alert message text
    
     badge: Number to show on the app’s icon.
     
     sound: A sound to play when the alert is delivered. Use UNNotificationSound.default() or create a custom sound from a file.
     launchImageName: name of a launch image to use if your app is launched in response to a notification.
     
     userInfo: A dictionary of custom info to pass in the notification
     
     attachments: An array of UNNotificationAttachment objects. Use to include audio, image or video content.
     */
}


-(void)NotificationWithIdentifier:(NSString*)identifier
                            Title:(NSString*)title
                             Body:(NSString*)body
                            Sound:(NSString*)sound
                            Badge:(int)badge
                         UserInfo:(NSDictionary*)userInfo
                       Attachment:(NSString*)attachment
                      TimePresent:(int)timePresent
                           Repeat:(BOOL)repeat;

-(NSString*)getNotificationIdentifier;
-(NSString*)getNotificationTitle;
-(NSString*)getNotificationBody;
-(NSString*)getNotificationSound;
-(int)getNotificationBadge;
-(NSDictionary*)getNotificationUserInfo;
-(NSString*)getNotificationAttachment;
-(int)getNotificationTimePresent;
-(BOOL)getNotificationIsRepeat;


@end

//
//  Notification.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 27/12/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SPNotification.h"

@implementation SPNotification

-(void)NotificationWithIdentifier:(NSString*)identifier
                            Title:(NSString*)title
                             Body:(NSString*)body
                            Sound:(NSString*)sound
                            Badge:(int)badge
                         UserInfo:(NSDictionary*)userInfo
                       Attachment:(NSString*)attachment
                      TimePresent:(int)timePresent
                           Repeat:(BOOL)repeat{
    
    notificationID = identifier;
    notificationTitle = title;
    notificationBody = body;
    notificationSound = sound;
    notificationBadge = badge;
    notificationUserInfo = userInfo;
    notificationAttachment = attachment;
    notificationIsRepeat = repeat;
    notificationDateTimePresent = timePresent;
    
}

-(NSString*)getNotificationIdentifier{
    return notificationID;
}
-(NSString*)getNotificationTitle{
    return notificationTitle;
}
-(NSString*)getNotificationBody{
    return notificationBody;
}
-(NSString*)getNotificationSound{
    return notificationSound;
}
-(int)getNotificationBadge{
    return notificationBadge;
}
-(NSDictionary*)getNotificationUserInfo{
    return notificationUserInfo;
}
-(NSString*)getNotificationAttachment{
    return notificationAttachment;
}
-(int)getNotificationTimePresent{
    return notificationDateTimePresent;
}
-(BOOL)getNotificationIsRepeat{
    return notificationIsRepeat;
}

@end

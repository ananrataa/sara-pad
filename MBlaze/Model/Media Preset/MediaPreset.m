//
//  MediaPreset.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 14/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaPreset.h"

@implementation MediaPreset


-(void)MediaPreset:(NSString*)subjectID MemberID:(NSString*)memberID ROOMID:(NSString*)roomID ROOMNAME:(NSString*)roomName SUBJECTCODE:(NSString*)subjectCode SUBJECTNAME:(NSString*)subjectName DESP:(NSString*)desp SECTIONCODE:(NSString*)sectionCode YEAR:(NSString*)year SEMESTER:(NSString*)semester UPDATEDATE:(NSString*)updateDate{
    
    SUBJECTID = subjectID;
    MEMBERID = memberID;
    ROOMID = roomID;
    ROOMNAME = roomName;
    SUBJECTCODE = subjectCode;
    SUBJECTNAME = subjectName;
    DESP = desp;
    SECTIONCODE = sectionCode;
    YEAR = year;
    SEMESTER = semester;
    UPDATEDATE = updateDate;
}

-(NSString*)subjectID{
    return SUBJECTID;
}

-(NSString*)memberID{
    return MEMBERID;
}

-(NSString*)roomID{
    return ROOMID;
}
-(NSString*)roomName{
    return ROOMNAME;
}
-(NSString*)subjectCode{
    return SUBJECTCODE;
}
-(NSString*)subjectName{
    return SUBJECTNAME;
}
-(NSString*)desp{
    return DESP;
}
-(NSString*)sectionCode{
    return SECTIONCODE;
}
-(NSString*)year{
    return YEAR;
}
-(NSString*)semester{
    return SEMESTER;
}
-(NSString*)updateDate{
    return UPDATEDATE;
}

@end

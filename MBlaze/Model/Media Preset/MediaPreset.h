//
//  MediaPreset.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 14/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaPreset : NSObject {
    
    NSString *SUBJECTID;
    NSString *MEMBERID;
    NSString *ROOMID;
    NSString *ROOMNAME;
    NSString *SUBJECTCODE;
    NSString *SUBJECTNAME;
    NSString *DESP;
    NSString *SECTIONCODE;
    NSString *YEAR;
    NSString *SEMESTER;
    NSString *UPDATEDATE;
}

-(void)MediaPreset:(NSString*)subjectID MemberID:(NSString*)memberID ROOMID:(NSString*)roomID ROOMNAME:(NSString*)roomName SUBJECTCODE:(NSString*)subjectCode SUBJECTNAME:(NSString*)subjectName DESP:(NSString*)desp SECTIONCODE:(NSString*)sectionCode YEAR:(NSString*)year SEMESTER:(NSString*)semester UPDATEDATE:(NSString*)updateDate;

-(NSString*)subjectID;
-(NSString*)memberID;
-(NSString*)roomID;
-(NSString*)roomName;
-(NSString*)subjectCode;
-(NSString*)subjectName;
-(NSString*)desp;
-(NSString*)sectionCode;
-(NSString*)year;
-(NSString*)semester;
-(NSString*)updateDate;

@end

//
//  FilterList.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/9/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "FilterList.h"

@implementation FilterList

NSMutableArray *arrayOfFilter;

+(void)FilterList:(NSMutableArray*)filterList{
    arrayOfFilter = filterList;
}

+(NSMutableArray*)getFilterList{
    return  arrayOfFilter;
}

@end

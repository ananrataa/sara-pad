//
//  FilterList.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/9/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterList : NSObject


+(void)FilterList:(NSMutableArray*)filterList;
+(NSMutableArray*)getFilterList;

@end

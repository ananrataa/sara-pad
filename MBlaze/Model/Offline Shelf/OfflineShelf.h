//
//  OfflineShelf.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/28/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfflineShelf : NSObject{
    
    NSString* MediaID;
    NSString* MemberID;
    NSString* ShelfID;
    NSString* LibraryID;
    NSString* LibraryName;
    NSString* MediaTitle;
    NSString* MediaAuthor;
    NSString* MediaType;
    NSString* MediaCoverURL;
    NSString* MediaCoverPath;
    NSString* MediaFilePath;
    NSString* MediaFileUrl;
    NSString* DateExp;
    NSString* DateBorrow;
    NSString* MediaOriginalName;
    NSString* SubjectID;
    //NSString* notificationIdentifier;

}


-(void)OfflineShelf:(NSString*)mediaID
           MemberID:(NSString*)memberID
            ShelfID:(NSString*)shelfID
          LibraryID:(NSString*)libID
        LibraryName:(NSString*)libName
         MediaTitle:(NSString*)mediaTitle
        MediaAuthor:(NSString*)mediaAuthor
          MediaType:(NSString*)mediaType
  MediaCoverFileURL:(NSString*)mCoverURL
 MediaCoverFilePath:(NSString*)mCoverPath
      MediaFilePath:(NSString*)mFilePath
       MediaFileURL:(NSString*)mFileUrl
         DateBorrow:(NSString*)dateBorrow
            DateExp:(NSString*)dateExp
  MediaOriginalName:(NSString*)mediaOriginalName
          SubjectID:(NSString*)subjectID;
//NotificationIdentifier:(NSString*)identifier;


-(NSString*)getMediaID;
-(NSString*)getShelfID;
-(NSString*)getMemberID;
-(NSString*)getLibraryID;
-(NSString*)getLibraryName;
-(NSString*)getMediaTitle;
-(NSString*)getMediaAuthor;
-(NSString*)getMediaType;
-(NSString*)getMediaCoverURL;
-(NSString*)getMediaCoverPath;
-(NSString*)getMediaFilePath;
-(NSString*)getDateExp;
-(NSString*)getMediaFileUrl;
-(NSString*)getDateBorrow;
-(NSString*)getMediaOriginalName;
-(NSString*)getSubjectID;
//-(NSString*)getNotificationIdentifier;


+(void)onlyOfflineModeEnable:(BOOL)isOfflineMode;
+(BOOL)isOnlyOfflineModeEnable;



@end

//
//  OfflineShelf.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/28/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "OfflineShelf.h"

@implementation OfflineShelf

BOOL isOnlyOfflineMode;

-(void)OfflineShelf:(NSString*)mediaID
           MemberID:(NSString*)memberID
            ShelfID:(NSString*)shelfID
          LibraryID:(NSString*)libID
        LibraryName:(NSString*)libName
         MediaTitle:(NSString*)mediaTitle
        MediaAuthor:(NSString*)mediaAuthor
          MediaType:(NSString*)mediaType
  MediaCoverFileURL:(NSString*)mCoverURL
 MediaCoverFilePath:(NSString*)mCoverPath
      MediaFilePath:(NSString*)mFilePath
       MediaFileURL:(NSString*)mFileUrl
         DateBorrow:(NSString*)dateBorrow
            DateExp:(NSString*)dateExp
  MediaOriginalName:(NSString*)mediaOriginalName
          SubjectID:(NSString*)subjectID{
//NotificationIdentifier:(NSString*)identifier
//{
    MediaID = mediaID;
    ShelfID = shelfID;
    MemberID = memberID;
    LibraryID = libID;
    LibraryName = libName;
    MediaTitle = mediaTitle;
    MediaAuthor = mediaAuthor;
    MediaType = mediaType;
    MediaCoverPath = mCoverPath;
    MediaFilePath = mFilePath;
    MediaFileUrl = mFileUrl;
    DateBorrow = dateBorrow;
    DateExp = dateExp;
    MediaOriginalName = mediaOriginalName;
    SubjectID = subjectID;
    //notificationIdentifier = identifier;
}

-(NSString*)getMediaID{
    return MediaID;
}

-(NSString*)getShelfID{
    return ShelfID;
}
-(NSString*)getMemberID{
    return MemberID;
}
-(NSString*)getLibraryID{
   return LibraryID;
}
-(NSString*)getLibraryName{
    return LibraryName;
}
-(NSString*)getMediaTitle{
    return MediaTitle;
}
-(NSString*)getMediaAuthor{
    return MediaAuthor;
}

-(NSString*)getMediaType{
    return MediaType;
}

-(NSString*)getMediaCoverURL{
    return MediaCoverURL;
}

-(NSString*)getMediaCoverPath{
    return MediaCoverPath;
}
-(NSString*)getMediaFilePath{
    return MediaFilePath;
}
-(NSString*)getDateExp{
    return DateExp;
}

-(NSString*)getMediaFileUrl{
    return MediaFileUrl;
}
-(NSString*)getDateBorrow{
    return DateBorrow;
}

-(NSString*)getMediaOriginalName{
    return MediaOriginalName;
}

-(NSString*)getSubjectID{
    return SubjectID;
}

//-(NSString*)getNotificationIdentifier{
//    return notificationIdentifier;
//}

+(void)onlyOfflineModeEnable:(BOOL)isOfflineMode{
    isOnlyOfflineMode = isOfflineMode;
}
+(BOOL)isOnlyOfflineModeEnable{
    return isOnlyOfflineMode;
}

@end

//
//  HistoryModel.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryModel : NSObject{
    
    NSString *MemberID;
    NSString *CoverURL;
    NSString *LibraryID;
    NSString *LibraryName;
    NSString *CategoryID;
    NSString *CategoryName;
    NSString *MediaType;
    NSString *MediaID;
    NSString *MediaTitle;
    NSString *MediaAuthor;
    NSString *WatchedDate;
    BOOL IsReturn;
}


-(void)HistoryModelWithLibraryID:(NSString*)library_id
                     LibraryName:(NSString*)library_name
                      CategoryID:(NSString*)cate_id
                    CategoryName:(NSString*)cate_name
                       MediaType:(NSString*)mType
                         MediaID:(NSString*)mID
                      MediaTitle:(NSString*)mTitle
                     MediaAuthor:(NSString*)mAuthor
                     WatchedDate:(NSString*)wDate
                        isReturn:(BOOL)isReturn;

-(void)HistoryWithMediaID:(NSString*)media_id
                 MemberID:(NSString*)member_id
                LibraryID:(NSString*)library_id
               MediaTitle:(NSString*)media_title
              MediaAuthor:(NSString*)media_author
             CategoryName:(NSString*)category_name
                 CoverURL:(NSString*)cover_url
                     Date:(NSString*)date
                 isReturn:(BOOL)isReturn;

-(NSString*)getMediaID;
-(NSString*)getLibraryName;
-(NSString*)getMemberID;
-(NSString*)getCoverUrl;
-(NSString*)getLibraryID;
-(NSString*)getCategoryID;
-(NSString*)getCategoryName;
-(NSString*)getMediaType;
-(NSString*)getMediaTitle;
-(NSString*)getMediaAuthor;
-(NSString*)getWatchedDate;
-(BOOL)getIsReturn;

@end

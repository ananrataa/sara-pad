//
//  HistoryModel.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "HistoryModel.h"

@implementation HistoryModel

-(void)HistoryModelWithLibraryID:(NSString*)library_id
                     LibraryName:(NSString*)library_name
                      CategoryID:(NSString*)cate_id
                    CategoryName:(NSString*)cate_name
                       MediaType:(NSString*)mType
                         MediaID:(NSString*)mID
                      MediaTitle:(NSString*)mTitle
                     MediaAuthor:(NSString*)mAuthor
                     WatchedDate:(NSString*)wDate
                        isReturn:(BOOL)isReturn{
    LibraryID = library_id;
    LibraryName = library_name;
    CategoryID = cate_id;
    CategoryName = cate_name;
    MediaType = mType;
    MediaID = mID;
    MediaTitle = mTitle;
    MediaAuthor = mAuthor;
    WatchedDate = wDate;
    IsReturn = isReturn;
}

-(void)HistoryWithMediaID:(NSString*)media_id
                 MemberID:(NSString*)member_id
                LibraryID:(NSString*)library_id
               MediaTitle:(NSString*)media_title
              MediaAuthor:(NSString*)media_author
             CategoryName:(NSString*)category_name
                 CoverURL:(NSString*)cover_url
                     Date:(NSString*)date
                 isReturn:(BOOL)isReturn{
    MediaID = media_id;
    MemberID = member_id;
    LibraryID = library_id;
    MediaTitle = media_title;
    MediaAuthor = media_author;
    CategoryName = category_name;
    CoverURL = cover_url;
    WatchedDate = date;
    IsReturn = isReturn;
    
}


-(NSString*)getLibraryID{
    return LibraryID;
}

-(NSString*)getLibraryName{
    return LibraryName;
}

-(NSString*)getCategoryID{
    return CategoryID;
}
-(NSString*)getCategoryName{
    return CategoryName;
}
-(NSString*)getMediaType{
    return MediaType;
}
-(NSString*)getMediaID{
    return MediaID;
}
-(NSString*)getMediaTitle{
    return MediaTitle;
}
-(NSString*)getMediaAuthor{
    return MediaAuthor;
}
-(NSString*)getWatchedDate{
    return WatchedDate;
}

-(NSString*)getMemberID{
    return MemberID;
}
-(NSString*)getCoverUrl{
    return CoverURL;
}

-(BOOL)getIsReturn {
    return IsReturn;
}
@end

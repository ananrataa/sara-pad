//
//  Room.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/7/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Room.h"

@implementation Room

NSMutableArray* arrayOfRoom;
NSString *currRoomID;

//-(void)Room:(NSString*)_default
//MessaageText:(NSString*)message
//   ROOMCODE:(NSString*)room_code
//     ROOMID:(NSString*)room_id
//   ROOMNAME:(NSString*)room_name
//     SYMBOL:(NSString*)symbol{
//    
//    default_ = _default;
//    message_ = message;
//    room_code_ = room_code;
//    room_id_ = room_id;
//    room_name_ = room_name;
//    symbol_ = symbol;
//}


-(void)Room:(NSString*)_roomID
   RoomCode:(NSString*)_roomCode
   RoomName:(NSString*)_roomName
     Symbol:(NSString*)_symbol
    Default:(NSString*)_default
 RoomTypeID:(NSString*)_roomTypeID
RoomTypeCode:(NSString*)_roomTypeCode
RoomTypeName:(NSString*)_roomTypeName
MessaageText:(NSString*)_message
      Index:(NSString*)_index
DateRegister:(NSString*)_date{
    
    
    default_ = _default;
    message_ = _message;
    room_code_ = _roomCode;
    room_id_ = _roomID;
    room_name_ = _roomName;
    symbol_ = _symbol;
    room_type_id = _roomTypeID;
    room_type_code = _roomTypeCode;
    room_type_name = _roomTypeName;
    room_date=_date;
    room_index = _index;
    
}


-(void)setRoom:(NSMutableArray *)arr_rooms{
    arrayOfRoom = arr_rooms;
}

-(NSMutableArray*)getRooms{
    return arrayOfRoom;
}

-(NSString*)getDefault{
    return default_;
}
-(NSString*)getRoomCode{
   return room_code_;
}
-(NSString*)getRoomID{
    return room_id_;
}

-(NSString*)getMessaageText{
    return message_;
}
-(NSString*)getRoomName{
    return room_name_;
}
-(NSString*)getSymbol{
    return symbol_;
}

-(NSString*)getRoomTypeID{
    return room_type_id;
}
-(NSString*)getRoomTypeCode{
    return room_type_code;
}
-(NSString*)getRoomTypeName{
    return room_type_name;
}

-(NSString*)getRoomIndex{
    return room_index;
}
-(NSString*)getRoomDateRegister{
    return room_date;
}

///current room id
//-(void)setCurrentRoomID:(NSString*)rID{
//    currRoomID = rID;
//    NSLog(@"CurrentRoomID : %@",currRoomID);
//}
//
//-(NSString*)getCurrentRoomID{
//    return currRoomID;
//}

@end

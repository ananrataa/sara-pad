//
//  Room.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/7/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Room : NSObject{
    NSString*default_;
    NSString*message_;
    NSString*room_code_;
    NSString*room_id_;
    NSString*room_name_;
    NSString*symbol_;
    NSString*room_type_code;
    NSString*room_type_id;
    NSString*room_type_name;
    NSString*room_index;
    NSString*room_date;
    
}

//-(void)Room:(NSString*)_default
//MessaageText:(NSString*)message
//   ROOMCODE:(NSString*)room_code
//     ROOMID:(NSString*)room_id
//   ROOMNAME:(NSString*)room_name
//     SYMBOL:(NSString*)symbol;

-(void)Room:(NSString*)_roomID
   RoomCode:(NSString*)_roomCode
   RoomName:(NSString*)_roomName
     Symbol:(NSString*)_symbol
    Default:(NSString*)_default
 RoomTypeID:(NSString*)_roomTypeID
RoomTypeCode:(NSString*)_roomTypeCode
RoomTypeName:(NSString*)_roomTypeName
MessaageText:(NSString*)_message
      Index:(NSString*)_index
DateRegister:(NSString*)_date;

-(void)setRoom:(NSMutableArray *)arr_rooms;

//-(void)setCurrentRoomID:(NSString*)rID;

-(NSMutableArray*)getRooms;

-(NSString*)getDefault;
-(NSString*)getMessaageText;
-(NSString*)getRoomCode;
-(NSString*)getRoomID;
-(NSString*)getRoomName;
-(NSString*)getSymbol;
-(NSString*)getRoomTypeID;
-(NSString*)getRoomTypeCode;
-(NSString*)getRoomTypeName;
-(NSString*)getRoomIndex;
-(NSString*)getRoomDateRegister;

//-(NSString*)getCurrentRoomID;

@end

//
//  Downloader.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 12/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Downloader : NSObject

-(void)startDownloadFile:(NSDictionary *)fileMetaData URL:(NSURL*)url;

@end

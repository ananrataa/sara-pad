//
//  Downloader.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 12/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Downloader.h"

@implementation Downloader

-(void)startDownloadFile:(NSDictionary *)fileMetaData URL:(NSURL*)url{
    
    
    
}

-(void)prepare4DownloadMedia:(NSString*)strURL{
    ///encode url ///
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:strURL];
    //NSLog(@"URL download file : %@",url);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:(id) self delegateQueue: nil];
        //[NSURLSession sharedSession];
         NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:url];
        [downloadTask resume];
        
    });
    
}

@end

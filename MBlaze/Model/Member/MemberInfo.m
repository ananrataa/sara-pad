//
//  Member.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/1/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MemberInfo.h"

@implementation MemberInfo

BOOL isSignIn;

-(void)MemberInfo:(NSString*) memberID
      SigninName:(NSString*) loginName
         Password:(NSString*) memberPass
      DisplayName:(NSString*) memberName
            Email:(NSString*) memberEmail
        Telephone:(NSString*)memberTel
      DateOfBirth:(NSString*) memberDateOfBirth
       ProfileUrl:(NSString*) memberProfileUrl
       LastSignin:(NSString*) memberLastSignin
              Etc:(NSString*) memberEtc{
    
    
    MEMBERID = memberID;
    MemberLoginName = loginName;
    PASSWORD = memberPass;
    MemberDisplayName = memberName;
    EMAIL = memberEmail;
    TELEPHONE = memberTel;
    DateOfBirth = memberDateOfBirth;
    ProfileURL = memberDateOfBirth;
    LastSignin = memberLastSignin;
    Etc = memberEtc;
}

-(NSString *)getMemberID{
    return MEMBERID;
}
-(NSString *)getUserLogin{
    return MemberLoginName;
}
-(NSString *)getPassword{
    return PASSWORD;
}
-(NSString *)getDisplayName{
    return MemberDisplayName;
}
-(NSString *)getEmail{
    return EMAIL;
}

-(NSString *)getTelephone{
    return TELEPHONE;
}

-(NSString *)getDateOfBirth{
    return DateOfBirth;
}

-(NSString *)getLastSignin{
    return LastSignin;
}

-(NSString *)getProfileURL{
    return ProfileURL;
}
-(NSString *)getEtc{
    return Etc;
}

+(void)setIsSignIn:(BOOL)isSignin{
    isSignIn = isSignin;
}

+(BOOL)isSignin{
    return isSignIn;
}

@end

//
//  MemberInfo.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/1/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MemberInfo : NSObject{
    NSString* MEMBERID;
    NSString* MemberLoginName;
    NSString* PASSWORD;
    NSString* MemberDisplayName;
    NSString* EMAIL;
    NSString* TELEPHONE;
    //    NSString* USERPW;
    //    NSString* SECRETCODE;
    //    NSString* INSTANCESIGN;
    //    NSString* SOLUTIONID;
    NSString* LastSignin;
    //    NSString* LimitDayOfRenting;
    //    NSString* LimitMediaOfRenting;
    //    NSString* SocialID;
    //    NSString* SocialTokenID;
    NSString *DateOfBirth;
    NSString *ProfileURL;
    NSString *Etc;
    
    //BOOL isSignIn;
}
/*
 "MEMBERID TEXT PRIMARY KEY DEFAULT NULL, "
 "USERNAME TEXT DEFAULT NULL, "
 "PASSWORD TEXT DEFAULT NULL, "
 "MEMBERNAME TEXT DEFAULT NULL, "
 "EMAIL TEXT DEFAULT NULL, "
 "USERPW TEXT DEFAULT NULL, "
 "SECRETCODE TEXT DEFAULT NULL, "
 "INSTANCESIGN TEXT DEFAULT NULL, "
 "SOLUTIONID TEXT DEFAULT NULL, "
 "LastSignin TEXT DEFAULT NULL, "
 "LimitDayOfRenting TEXT DEFAULT NULL, "
 "LimitMediaOfRenting TEXT DEFAULT NULL
 */

-(void)MemberInfo:(NSString*) memberID
     SigninName:(NSString*) loginName
     Password:(NSString*) memberPass
   DisplayName:(NSString*) memberName
        Email:(NSString*) memberEmail
        Telephone:(NSString*)memberTel
       DateOfBirth:(NSString*) memberDateOfBirth
  ProfileUrl:(NSString*) memberProfileUrl
  LastSignin:(NSString*) memberLastSignin
   Etc:(NSString*) memberEtc;

-(NSString *)getMemberID;
-(NSString *)getUserLogin;
-(NSString *)getPassword;
-(NSString *)getDisplayName;
-(NSString *)getEmail;
-(NSString *)getTelephone;
-(NSString *)getDateOfBirth;
//-(NSString *)getUSERPW;
//-(NSString *)getSECRETCODE;
//-(NSString *)getINSTANCESIGN;
//-(NSString *)getSOLUTIONID;
-(NSString *)getLastSignin;
//-(NSString *)getLimitDayOfRenting;
//-(NSString *)getLimitMediaOfRenting;
//-(NSString *)getSocialID;
//-(NSString *)getSocialTokenID;
-(NSString *)getProfileURL;
-(NSString *)getEtc;

+(void)setIsSignIn:(BOOL)isSignin;
+(BOOL)isSignin;

@end

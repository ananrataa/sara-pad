//
//  ThemeTemplate.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ThemeTemplate.h"
#import "ConstantValues.h"
#import "Utility.h"

@implementation ThemeTemplate
NSString *themeSet1;
NSString *themeSet2;
NSString *themeSet3;
NSString *themeSet4;
NSString *themeSet5;
NSString *themeSet6;
NSString *themeSet7;

+(void)saveTheme:(NSDictionary *)dict RoomID:(NSString*)roomId{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString;
    if (jsonData){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        jsonString = [NSString stringWithFormat:@"[%@]",jsonString];
    }
    else
        jsonString = @"";
    
    //NSLog(@"Your JSON String is %@", jsonString);
    
    [Utility createNewDirectory:ThemeTmeplateFolderName isSupportDirectory:false];
    NSString *fileAtPath = [[Utility documentPath] stringByAppendingPathComponent:[ThemeTmeplateFolderName stringByAppendingPathComponent:[NSString stringWithFormat:@"Theme_%@.txt", roomId]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:fileAtPath]) {
        [fileManager removeItemAtPath:fileAtPath error:nil];
    }
    [[jsonString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:YES];
}

+(void)loadTheme:(NSString *)roomId;{
    NSString *fileAtPath = [[Utility documentPath] stringByAppendingPathComponent:[ThemeTmeplateFolderName stringByAppendingPathComponent:[NSString stringWithFormat:@"Theme_%@.txt", roomId]]];
    
    NSData *data = [NSData dataWithContentsOfFile:fileAtPath];
    if([data length] > 0){
        NSError *err = nil;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
        if (!err && [array count] > 0) {
            NSDictionary *dict = [array objectAtIndex:0];
            //NSLog(@"Theme : %@",[dict objectForKey:@"Set1"]);
            themeSet1 = [dict objectForKey:@"Set1"];
            themeSet2 = [dict objectForKey:@"Set2"];
            themeSet3 = [dict objectForKey:@"Set3"];
            themeSet4 = [dict objectForKey:@"Set4"];
            themeSet5 = [dict objectForKey:@"Set5"];
            themeSet6 = [dict objectForKey:@"Set6"];
            themeSet7 = [dict objectForKey:@"Set7"];
        }
        else
            [self setDefaultTheme];
        
    }
    else
        [self setDefaultTheme];
    
}

+(void)setDefaultTheme{
//    themeSet1 = @"#0066cc";
//    themeSet2 = @"#ffffff";
//    themeSet3 = @"#3399ff";
//    themeSet4 = @"#001a33";
//    themeSet5 = @"#939597";
//    themeSet6 = @"#d44c27";
//    themeSet7 = @"#0066ff";
    
    themeSet1 = @"#ffffff";
    themeSet2 = @"#383232";
    themeSet3 = @"#d44c27";
    themeSet4 = @"#ffffff";
    themeSet5 = @"#939597";
    themeSet6 = @"#d44c27";
    themeSet7 = @"#323232";
}

+(NSString*)getThemeSet1{
    return themeSet1;
}
+(NSString*)getThemeSet2{
    return themeSet2;
}
+(NSString*)getThemeSet3{
    return themeSet3;
}
+(NSString*)getThemeSet4{
    return themeSet4;
}
+(NSString*)getThemeSet5{
    return themeSet5;
}
+(NSString*)getThemeSet6{
    return themeSet6;
}
+(NSString*)getThemeSet7{
    return themeSet7;
}

@end

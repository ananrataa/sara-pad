//
//  ThemeTemplate.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemeTemplate : NSObject

+(void)saveTheme:(NSDictionary *)dict RoomID:(NSString*)roomId;

+(void)loadTheme:(NSString *)roomId;

+(void)setDefaultTheme;

+(NSString*)getThemeSet1;
+(NSString*)getThemeSet2;
+(NSString*)getThemeSet3;
+(NSString*)getThemeSet4;
+(NSString*)getThemeSet5;
+(NSString*)getThemeSet6;
+(NSString*)getThemeSet7;

@end

//
//  MediaFileCache.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaFileCache : NSObject{
    
    NSData *mImage;
    NSString *mID;
}


-(void)MediaFileCacheWithMediaID:(NSString*)ID NSData:(NSData*)imageData;
-(NSData*)getCoverImageMedia;
-(NSString*)getMediaID;

@end

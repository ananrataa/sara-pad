//
//  MediaFileCache.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaFileCache.h"


@implementation MediaFileCache

-(void)MediaFileCacheWithMediaID:(NSString*)ID NSData:(NSData*)imageData{
    
    mID = ID;
    mImage = imageData;
    
}

-(NSData*)getCoverImageMedia{
    return mImage;
}

-(NSString*)getMediaID{
    return mID;
}

@end

//
//  Ads.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Ads.h"

@implementation Ads

-(void)AdsFullScreen:(NSString*)ads_id
             AdsName:(NSString*)ads_name
         AdsImageUrl:(NSString*)url
          AdsLinkUrl:(NSString*)link_url
             AdsDESP:(NSString*)ads_desp
           AdsRoomID:(NSString*)room_id{
    
    AdsID = ads_id;
    AdsName = ads_name;
    AdsImageUrl = url;
    AdsDESP = ads_desp;
    AdsRoomID = room_id;
    AdsLinkUrl = link_url;
}


-(NSString*)getAdsID{
    return AdsID;
}
-(NSString*)getAdsName{
    return AdsName;
}
-(NSString*)getAdsImageUrl{
   return AdsImageUrl;
}
-(NSString*)getAdsDESP{
    return AdsDESP;
}
-(NSString*)getAdsRoomID{
    return AdsRoomID;
}

-(NSString*)getAdsLinkUrl{
    return AdsLinkUrl;
}


@end

//
//  Ads.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ads : NSObject{

    NSString *AdsID;
    NSString *AdsName;
    NSString *AdsImageUrl;
    NSString *AdsDESP;
    NSString *AdsRoomID;
    NSString *AdsLinkUrl;
}

-(void)AdsFullScreen:(NSString*)ads_id
             AdsName:(NSString*)ads_name
         AdsImageUrl:(NSString*)url
          AdsLinkUrl:(NSString*)link_url
             AdsDESP:(NSString*)ads_desp
           AdsRoomID:(NSString*)room_id;

-(NSString*)getAdsID;
-(NSString*)getAdsName;
-(NSString*)getAdsImageUrl;
-(NSString*)getAdsDESP;
-(NSString*)getAdsRoomID;
-(NSString*)getAdsLinkUrl;

@end

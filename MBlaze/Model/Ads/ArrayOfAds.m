//
//  ArrayOfAds.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/30/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ArrayOfAds.h"

@implementation ArrayOfAds

NSMutableArray *ArrFullScreenAds;
NSMutableArray *ArrSliderAds;

+(void)setArrayFullScreenAds:(NSMutableArray *)arrFullAds{
    ArrFullScreenAds = [NSMutableArray arrayWithArray:arrFullAds];
}

+(NSMutableArray *)getArrayFullScreenAds{
    return ArrFullScreenAds;
}

+(void)setArraySlideAds:(NSMutableArray *)arrSlideAds{
    ArrSliderAds = [NSMutableArray arrayWithArray:arrSlideAds];
}

+(NSMutableArray *)getArrayArraySlideAds{
    return ArrSliderAds;
}


@end

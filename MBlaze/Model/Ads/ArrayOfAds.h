//
//  ArrayOfAds.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/30/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArrayOfAds : NSObject

+(void)setArrayFullScreenAds:(NSMutableArray *)arrFullAds;

+(NSMutableArray *)getArrayFullScreenAds;

+(void)setArraySlideAds:(NSMutableArray *)arrSlideAds;

+(NSMutableArray *)getArrayArraySlideAds;

@end

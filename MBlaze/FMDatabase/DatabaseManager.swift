//
//  DatabaseManager.swift
//  MPAD Plus
//
//  Created by MBOX Multimedia Co., Ltd. on 10/27/2559 BE.
//  Copyright © 2559 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
class DatabaseManager {
    
    private let dbFileName = "database.sqlite"
    private var database:FMDatabase!
    
    init() {
        openDatabase()
    }
    
    func openDatabase() {
        
//        let dbPath = Bundle.main.path(forResource: dbFileName, ofType: "sqlite")// path of your file in bundle
        
        let dbPath = HTTPDownloadFile.fileInDocumentsDirectory(filename: dbFileName)
        let database = FMDatabase(path: dbPath)
        
        /* Open database read-only. */
        if (!(database?.open(withFlags: 1))!) {
            if HTTPDownloadFile.deleteFile(fileName: CentralController.DBVersion){
                //print("Delete file version success)")
            }
            //print("Could not open database at \(dbPath).")
        } else {
            self.database = database;
        }
    }
    
    func closeDatabase() {
        if (database != nil) {
            database.close()
        }
    }
    
    func query(queryString:String) -> [String] {
        //let sql = "select * from tb_song where fl_songid='601004' "
        var index = 0
        var dataArray:[String] = []
        
        if let db = database, let q = db.executeQuery(queryString, withArgumentsIn: nil) {
            while q.next() {
                let data = q.string(forColumn: "fl_singername")
                if data != nil && data != ""{
                    dataArray.append(data!)
                    index += 1
                }
                
                //print("\(index).\(data)")
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    /////// singers //////////

    func loadSinger(queryString:String,startAtRow:Int,totalRow:Int) -> Array<String> {
        //print("Start Index : \(startAtRow) , Total : \(totalRow)")
        
        let sql = queryString + " limit " + String(totalRow)
        var index = 0
        var dataArray:Array<String> = []
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                
                if(index >= startAtRow){
                    //print("------- Index : \(index) ----------")
                    let data = q.string(forColumn: "fl_singer")
                    if data != nil && data != ""{
                        dataArray.append(data!)
                        //print("\(index) -> \(data)")
                    }
                }
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    /////// song of singer //////////
    func loadSongOfSinger(queryString:String,startAtRow:Int,totalRow:Int) -> Array<Array<String>> {
        
        let sql = queryString + " limit " + String(totalRow)
        var index = 0
        var dataArray:Array<Array<String>>=[]
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                
                if(index >= startAtRow){
                    //print("------- Index : \(index) ----------")
                    let data_id = q.string(forColumn: "fl_song_id")
                    let song_name = q.string(forColumn: "fl_song_name")
                    let singer = q.string(forColumn: "fl_singer")
                    let hdd = q.string(forColumn: "fl_hdd")
                    let pic_path = q.string(forColumn: "fl_pic_path")
                    let data:Array<String> = [data_id!, song_name!, singer!,hdd!,pic_path!]
                    dataArray.append(data)
                    
                   // print("Song of Singer : \(dataArray)")
                   

                }
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    /////// aistit image path //////////
    
    func loadArtistImagePath(queryString:String,startAtRow:Int,totalRow:Int) -> Array<Array<String>> {
        
        let sql = queryString + " limit " + String(totalRow)
        var index = 0
        var dataArray:Array<Array<String>> = []
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                
                if(index >= startAtRow){
                    //print("------- Index : \(index) ----------")
                    let singer = q.string(forColumn: "fl_singer")
                    let img_path = q.string(forColumn: "fl_pic_path")
                    if singer != "" /*&&  img_path != ""*/ {
                    dataArray.append([singer!,img_path!])
                    }
                    //print("\(img_path)")
                }
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }

    /////// songs //////////
    func loadSong(queryString:String,startAtRow:Int,totalRow:Int) -> Array<Array<String>> {
        
       // print("SQL :\(queryString)")
        
        let sql = queryString + " limit " + String(totalRow)
        var index = 0
        var dataArray:Array<Array<String>>=[]
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                
                if(index >= startAtRow){
                    //print("------- Index : \(index) ----------")
                    let data_id = q.string(forColumn: "fl_song_id")
                    let song_name = q.string(forColumn: "fl_song_name")
                    let singer = q.string(forColumn: "fl_singer")
                    let hdd = q.string(forColumn: "fl_hdd")
                    let pic_path = q.string(forColumn: "fl_pic_path")
                    let data:Array<String> = [data_id!, song_name!, singer!, hdd!, pic_path!]
                    dataArray.append(data)
                    
                    // print("Song of Singer : \(dataArray)")
                    
                    
                }
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    
    ////// type of song ////
    func loadTypeOfSong(queryString:String) -> Array<Array<String>> {
        
        var index = 0
        var dataArray:Array<Array<String>> = []
    
        if let db = database, let q = db.executeQuery(queryString, withArgumentsIn: nil) {
            while q.next() {
                
                //print("------- Index : \(index) ----------")
                let data_id = q.string(forColumn: "fl_cat_id")
                let data_th = q.string(forColumn: "fl_th_name")
                let data_en = q.string(forColumn: "fl_en_name")
                if data_th != nil && data_th != ""{
                    let data:Array<String> = [data_id!, data_th!, data_en!]
                    dataArray.append(data)
                    
                    //print("\(index) -> \(data_th) , \(data_en)")
                }
                
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    
    /////// search  //////////
    func searching(queryString:String,startAtRow:Int,totalRow:Int) -> Array<Array<String>> {
        
        let sql = queryString + " limit " + String(totalRow)
        var index = 0
        var dataArray:Array<Array<String>>=[]
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                
                if(index >= startAtRow){
                    //print("------- Index : \(index) ----------")
                    let data_id = q.string(forColumn: "fl_song_id")
                    let singer_name =  q.string(forColumn: "fl_singer")
                    let song_name = q.string(forColumn: "fl_song_name")
                    let hdd = q.string(forColumn: "fl_hdd")
                    let pic_path = q.string(forColumn: "fl_pic_path")
                    let data:Array<String> = [data_id!, song_name!, singer_name!,hdd!,pic_path!]
                    dataArray.append(data)
                    
                    // print("Song of Singer : \(dataArray)")
                    
                    
                }
                index += 1
            }
            
            return dataArray
            
        }
        
        return dataArray
    }
    
    
    func checkFavoriteSong(sql:String) -> Array<String> {
        
        var dataArray:Array<String> = []
        
        if let db = database, let q = db.executeQuery(sql, withArgumentsIn: nil) {
            while q.next() {
                let data_id = q.string(forColumn: "fl_song_id")
                let singer_name =  q.string(forColumn: "fl_singer")
                let song_name = q.string(forColumn: "fl_song_name")
                let hdd = q.string(forColumn: "fl_hdd")
                let pic_path = q.string(forColumn: "fl_pic_path")
                
                dataArray = [data_id!, song_name!, singer_name!,hdd!,pic_path!]
                // print("Song of Singer : \(dataArray)")
            }
        }
        return dataArray
    }
    

}

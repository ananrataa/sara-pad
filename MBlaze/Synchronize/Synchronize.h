//
//  Synchronize.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Synchronize : NSObject{
    
    NSUserDefaults *userDefault;
    NSString *memberID;
}

-(void)syncMediaCloudShelf:(NSString *)memberID;
-(void)syncMediaPreset:(NSString *) memberID;
-(void)writeMediaToMyShelf:(NSDictionary *)dictJson ShelfID:(NSString *)shelfID;

@end

//
//  Synchronize.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Synchronize.h"
#import "ConstantValues.h"
#import "ServiceAPI.h"
#import "Utility.h"
#import "DBManager.h"
#import "Member.h"
#import "OfflineShelf.h"

@implementation Synchronize

-(instancetype)init{
    userDefault = [NSUserDefaults standardUserDefaults];
    return self;
}

#pragma mark ----- sync Media on Cloud ----
-(void)syncMediaCloudShelf:(NSString *)memberID{
    if(!([userDefault integerForKey:LastSyncCloudMedia] > 0 ) ||
       [userDefault integerForKey:LastSyncCloudMedia] < [[Utility currentDate] integerValue]){
        ServiceAPI *api = [[ServiceAPI alloc] init];
        [api syncMemberCloudMedia:@"syncMediaOnCloud" MemberID:memberID];
    }
    
}


#pragma mark ---- sync Preset ------
-(void)syncMediaPreset:(NSString *) memberID{
    //||[userDefault integerForKey:LastSyncPresetMedia] < [[Utility currentDate] integerValue]
    if(!([userDefault integerForKey:LastSyncPresetMedia] > 0 ) ||
       [userDefault integerForKey:LastSyncPresetMedia] < [[Utility currentDate] integerValue]){
        ServiceAPI *api = [[ServiceAPI alloc] init];
        [api syncMediasPreset:@"SyncMediaPreset" MemberID:memberID];
    }
}



-(void)writeMediaToMyShelf:(NSDictionary *)dictJson ShelfID:(NSString *)shelfID{
    if ([shelfID isEqualToString:PresetShelfId]) {
        [DBManager createMediaPresetTable];
        for( NSDictionary *level1 in ((NSArray*)dictJson)){
            NSString *folderName = [NSString stringWithFormat:@"%@/%@",sarapadFloderName,[level1 objectForKey:@"ROOMID"]];
            //crate dir ///
            [Utility createNewDirectory:folderName isSupportDirectory:FALSE];
            
            for (NSDictionary *presetItems in [level1 objectForKey:@"PresetItems"]) {
                //NSLog(@"%@",[presetItems objectForKey:@"SUBJECTNAME"]);
                MediaPreset *preSet = [[MediaPreset alloc] init];
                [preSet MediaPreset:[presetItems objectForKey:@"SUBJECTID"]
                             ROOMID:[presetItems objectForKey:@"ROOMID"]
                           ROOMNAME:[presetItems objectForKey:@"ROOMNAME"]
                        SUBJECTCODE:[presetItems objectForKey:@"SUBJECTCODE"]
                        SUBJECTNAME:[presetItems objectForKey:@"SUBJECTNAME"]
                               DESP:[presetItems objectForKey:@"DESP"]
                        SECTIONCODE:[presetItems objectForKey:@"SECTIONCODE"]
                               YEAR:[presetItems objectForKey:@"YEAR"]
                           SEMESTER:[presetItems objectForKey:@"SEMESTER"]
                         UPDATEDATE:[presetItems objectForKey:@"UPDATEDATE"]];
                [DBManager insertMediaPreset:preSet];
                
                for (NSDictionary *mediaItems in [presetItems objectForKey:@"MediaItems"]) {
                    //NSLog(@"%@",[mediaItems objectForKey:@"MEDIANAME"]);
                    
                    //dowload Media Cover
                    NSString *mediaID = [mediaItems objectForKey:@"MEDIAID"];
                    NSString* coverFileAtPath = [self downloadMediaCover:mediaID Folder:folderName Url:[mediaItems objectForKey:@"MediaCoverUrl"]];
                
                    //create MediaID for preset
                    mediaID = [Utility encodeMediaIDPreset:[mediaItems objectForKey:@"MEDIAID"]];
                    
                    NSString *subjectName = [NSString stringWithFormat:@"%@ : %@",[presetItems objectForKey:@"SUBJECTCODE"],[presetItems objectForKey:@"SUBJECTNAME"]];
                    
                    OfflineShelf *myShelf = [[OfflineShelf alloc] init];
                    [myShelf OfflineShelf:mediaID
                                 MemberID:memberID
                                  ShelfID:[presetItems objectForKey:@"ROOMID"]
                                LibraryID:[presetItems objectForKey:@"SUBJECTID"]
                              LibraryName:subjectName
                               MediaTitle:[mediaItems objectForKey:@"MEDIANAME"]
                              MediaAuthor:[mediaItems objectForKey:@"AUTHOR"]
                                MediaType:[mediaItems objectForKey:@"MEDIAKIND"]
                        MediaCoverFileURL:[mediaItems objectForKey:@"MediaCoverUrl"]
                       MediaCoverFilePath:coverFileAtPath
                            MediaFilePath:@""
                             MediaFileURL:@""
                               DateBorrow:@""
                                  DateExp:[Utility covertDateToFormat:[mediaItems objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                        MediaOriginalName:@""
                                SubjectID:[presetItems objectForKey:@"SUBJECTID"]];
                    [DBManager insertOfflineShelf:myShelf isPreset:YES];
                }
            }
        }
    }
    else if ([shelfID isEqualToString:MemberShelfId]){
        NSLog(@"dict : %@",dictJson);
        for(NSDictionary *roomData in ((NSArray*)dictJson)){
            //NSLog(@"RoomID : %@",[roomData objectForKey:@"ROOMID"]);
            for (NSDictionary *mediaItems in [roomData objectForKey:@"MediaItems"]) {
                
                //dowload Media Cover
                NSString *folderName = [NSString stringWithFormat:@"%@/%@",sarapadFloderName,[roomData objectForKey:@"ROOMID"]];
                NSString *mediaID = [mediaItems objectForKey:@"MEDIAID"];
                NSString* coverFileAtPath = [self downloadMediaCover:mediaID Folder:folderName Url:[mediaItems objectForKey:@"MediaCoverUrl"]];
                
                OfflineShelf *myShelf = [[OfflineShelf alloc] init];
                //insert Metadata (file not download yet!!)
                [myShelf OfflineShelf:[mediaItems objectForKey:@"MEDIAID"]
                             MemberID:memberID
                              ShelfID:MemberShelfId
                            LibraryID:[roomData objectForKey:@"ROOMID"]
                          LibraryName:[roomData objectForKey:@"ROOMNAME"]
                           MediaTitle:[mediaItems objectForKey:@"MEDIANAME"]
                          MediaAuthor:[mediaItems objectForKey:@"AUTHOR"]
                            MediaType:[mediaItems objectForKey:@"MEDIAKIND"]
                    MediaCoverFileURL:[mediaItems objectForKey:@"MediaCoverUrl"]
                   MediaCoverFilePath:coverFileAtPath
                        MediaFilePath:@""
                         MediaFileURL:@""
                           DateBorrow:[Utility covertDateToFormat:[mediaItems objectForKey:@"RENTDATE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                              DateExp:[Utility covertDateToFormat:[mediaItems objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                    MediaOriginalName:@""
                            SubjectID:@""];
                
                [DBManager syncForInsertUpdateMemberCloudMedia:myShelf];
            }
            
        }
    }
    
}

-(NSString*)downloadMediaCover:(NSString *)mediaID Folder:(NSString*)folderName Url:(NSString*)_url{
    NSString* coverImgThumbName = [NSString stringWithFormat:@"%@_%@_thumb.png",memberID,mediaID];
    NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:coverImgThumbName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath: docPath]) {
        NSString* url = [_url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        if ([Utility isImage:imgData]) {
            [Utility saveMediaCoverImageFromData:imgData withFileName:coverImgThumbName inDirectory:folderName];
        }
    }
    
   // NSString* coverFileAtPath = [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
    
    return [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
}


@end

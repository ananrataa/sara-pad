//
//  SettingTCV.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 11/9/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SettingTCV.h"
#import "Utility.h"
#import "ChangePasswordVC.h"
#import "ConstantValues.h"
#import "AppDelegate.h"
#import "MemberInfo.h"
#import "HexColor.h"
#import "ThemeTemplate.h"

@interface SettingTCV ()

@end

@implementation SettingTCV

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"Setting"]];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    if([MemberInfo isSignin])
        optionArray = @[[Utility NSLocalizedString:@"ChangeLanguage"],
                    [Utility NSLocalizedString:@"ChangePassword"]];
    else
        optionArray = @[[Utility NSLocalizedString:@"ChangeLanguage"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [optionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
    
    UILabel *lb = (UILabel*)[cell viewWithTag:201];
    lb.text = optionArray[indexPath.row];
    lb.tag = indexPath.row;
    lb.font = [UIFont fontWithName:mainFont size:17.f];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self changeLanguage:indexPath];
    }
    else if(indexPath.row == 1){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        ChangePasswordVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)changeLanguage:(NSIndexPath *)indexPath{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Utility NSLocalizedString:@"SelectLanguage"]
                                                                   message:nil
                                                    preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *thaiAction = [UIAlertAction actionWithTitle:[self getMessageText:[NSMutableString stringWithString: @"th"] Message:[NSMutableString stringWithString: @"🇹🇭 ภาษาไทย"]]
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              [self prepareToChangeLanguage:@"th"];
                                                              
                                                          }];
    UIAlertAction *englishAction = [UIAlertAction actionWithTitle:[self getMessageText:@"en" Message:@"﻿🇺🇸 English"]
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               
                                                               [self prepareToChangeLanguage:@"en"];
                                                           }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
    
    [alert addAction:thaiAction];
    [alert addAction:englishAction];
    [alert addAction:cancelAction];
    
    if(IPAD){
        alert.popoverPresentationController.sourceView = self.tableView;
        alert.popoverPresentationController.sourceRect = [self.tableView rectForRowAtIndexPath:indexPath];
    }

    [self presentViewController:alert animated:YES completion:nil];
}

-(NSString *)getMessageText:(NSString *)code Message:(NSString *)text{
    NSString *currLanguage = [((NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"])[0] lowercaseString];
    if([currLanguage caseInsensitiveCompare:code] == NSOrderedSame)
        return [NSString stringWithFormat:@"%@ %@",text,[Utility NSLocalizedString:@"CurrUsing"]];
    
    else
        return text;
}

-(void)prepareToChangeLanguage:(NSString*)codeLang{
    NSString *currLanguage = [((NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"])[0] lowercaseString];
    if([currLanguage caseInsensitiveCompare:codeLang] != NSOrderedSame){
        // set AppleLanguages
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObjects:codeLang, nil]  forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] setValue:[codeLang uppercaseString]  forKey:AppLanguageKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate relaunchApplication];
    }
}



@end

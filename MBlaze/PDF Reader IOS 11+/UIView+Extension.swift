//
//  MusicBackgroundUI+Extension.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 13/6/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

extension UIView {
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.size.width, height:  self.bounds.size.height)
        
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 99999
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)
        
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = self.viewWithTag(99999){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
    
    func presentMusicBackgourndPlayerControl(_ btnPlay:UIButton, btnStop:UIButton) {
        removeMusicPlayerControl()
        
        let playerControlView = UIView()
        playerControlView.frame = CGRect(x: 5, y: 20, width: 110, height: 50)
        playerControlView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        playerControlView.tag = 787878
        playerControlView.clipsToBounds = true
        playerControlView.layer.cornerRadius = 17
        
        btnPlay.frame = CGRect(x: 10, y: 5, width: 40, height: 40)
        btnPlay.setImage(#imageLiteral(resourceName: "pause-bgm"), for: .normal)
        
        btnStop.frame = CGRect(x: 60, y: 5, width: 40, height: 40)
        btnStop.setImage(#imageLiteral(resourceName: "stop-bgm"), for: .normal)
        
        playerControlView.addSubview(btnPlay)
        playerControlView.addSubview(btnStop)
        
        self.addSubview(playerControlView)
    }
    
    func removeMusicPlayerControl() {
        if let playerControlView = self.viewWithTag(787878){
            playerControlView.removeFromSuperview()
        }
    }
    
    func musicPlayerControlSlideLeftToDisappear() {
        if let mView = self.viewWithTag(787878){
            UIView.animate(withDuration: 0.55, delay: 0.0, options: .transitionFlipFromRight, animations: {
                mView.frame = CGRect(x: mView.frame.origin.x, y: mView.frame.origin.y, width: -mView.frame.size.width, height: mView.frame.size.height)
            }, completion: { (_) in
                mView.removeFromSuperview()
            })
        }
        
    }
    
    var isMusicPlayerControlShowing:Bool {
        if self.viewWithTag(787878) != nil{
            print("view is alive")
            return true
        }
        print("view is dead")
        return false
    }
    
}



//
//  Annotation.swift
//  ios11PDFKitObjectiveC
//
//  Created by MBOX Multimedia Co., Ltd. on 24/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import UIKit
import PDFKit


@available(iOS 11.0, *)
class Annotation{
    let pdfDocument:PDFDocument
    let colorAnnotation:UIColor
    
    init(_ pdfDocument:PDFDocument) {
        self.pdfDocument =  pdfDocument
        
        if UserDefaults.standard.string(forKey: "ColorPicker") != nil{
            self.colorAnnotation = UIColor.init(hexString: UserDefaults.standard.string(forKey: "ColorPicker")!)
        }
        else{
            self.colorAnnotation = .yellow
        }
       
    }
    
    //strikeOut
    func strikeOut(_ selections: [PDFSelection], page:PDFPage, tag: Int? = 1) -> [NSDictionary] {
        var jsonArray:[NSDictionary] = [NSDictionary]()
        selections.forEach({ selection in
            let highlight = PDFAnnotation(bounds: selection.bounds(for: page), forType: .strikeOut, withProperties: nil)
            highlight.color = self.colorAnnotation
            highlight.endLineStyle = .square
            page.addAnnotation(highlight)
            //print("----- add highlight ------")
            let pageNumber = SP.shared.actualPageNumber(page: page)
            let dictJson = ["pageNumber":pageNumber,"annotationList":[
                "type":"strikeOut",
                "tag":tag!,
                "X":CGFloat(highlight.bounds.origin.x),
                "Y":CGFloat(highlight.bounds.origin.y),
                "W":CGFloat(highlight.bounds.size.width),
                "H":CGFloat(highlight.bounds.size.height)]] as [String : Any] as NSDictionary
            
            jsonArray.append(dictJson as NSDictionary)
            //isPdfEdited = true;
            //saveAnnotationToFile(0)
        })
        //print("highlight point :",jsonArray)
        return jsonArray
    }
    
    
    //underline
    func underline(_ selections:[PDFSelection], page:PDFPage, tag: Int? = 1) -> [NSDictionary] {
        var jsonArray:[NSDictionary] = [NSDictionary]()
        selections.forEach({ selection in
            let highlight = PDFAnnotation(bounds: selection.bounds(for: page), forType: .underline, withProperties: nil)
            highlight.userName = "\(tag ?? 0)"
            highlight.color = self.colorAnnotation
            highlight.endLineStyle = .square
            page.addAnnotation(highlight)
            //print("----- add highlight ------")
            let pageNumber = SP.shared.actualPageNumber(page: page)
            let dictJson = ["pageNumber":pageNumber,"annotationList":[
                "type":"underline",
                "tag":tag!,
                "X":CGFloat(highlight.bounds.origin.x),
                "Y":CGFloat(highlight.bounds.origin.y),
                "W":CGFloat(highlight.bounds.size.width),
                "H":CGFloat(highlight.bounds.size.height)]] as [String : Any] as NSDictionary
            
            jsonArray.append(dictJson as NSDictionary)
            //isPdfEdited = true;
            //saveAnnotationToFile(0)
        })
        //print("highlight point :",jsonArray)
        return jsonArray
    }
    
    //highlight
    func highlight(_ selections: [PDFSelection], page:PDFPage, tag: Int? = 1) -> [NSDictionary] {
        var jsonArray:[NSDictionary] = [NSDictionary]()
        selections.forEach({ selection in
            let highlight = PDFAnnotation(bounds: selection.bounds(for: page), forType: .highlight, withProperties: nil)
            highlight.color = self.colorAnnotation
            highlight.endLineStyle = .square
            highlight.userName = "\(tag ?? 0)"
            page.addAnnotation(highlight)
            //print("----- add highlight ------")
            let pageNumber = SP.shared.actualPageNumber(page: page)
            let dictJson = ["pageNumber":pageNumber,"annotationList":[
                "color":self.colorAnnotation.hexCode,
                "type":"highlight",
                "tag":tag!,
                "X":CGFloat(highlight.bounds.origin.x),
                "Y":CGFloat(highlight.bounds.origin.y),
                "W":CGFloat(highlight.bounds.size.width),
                "H":CGFloat(highlight.bounds.size.height)]] as [String : Any] as NSDictionary
            
            jsonArray.append(dictJson as NSDictionary)
            //isPdfEdited = true;
            //saveAnnotationToFile(0)
        })
        //print("highlight point :",jsonArray)
        return jsonArray
    }
    
    /*/unhighlight
    func unHighlight(_ selection:[PDFSelection], page:PDFPage, currentAnnotation:[NSDictionary]) -> [NSDictionary] {
        var highlightArray = [PDFAnnotation]()
        var currentAnnotationJson = currentAnnotation
        selection.forEach({ selection in
            highlightArray.append(PDFAnnotation(bounds: selection.bounds(for: page), forType: .highlight, withProperties: nil))
        })
        
        page.annotations.forEach({ annotation in
            //print("----- unhighlight ------")
            for highlight in highlightArray {
                let errorPointX = CGFloat(highlight.bounds.origin.x)/CGFloat(annotation.bounds.origin.x)
                let errorPointY = CGFloat(highlight.bounds.origin.y)/CGFloat(annotation.bounds.origin.y)
                let errorWidth = CGFloat(highlight.bounds.size.width)/CGFloat(annotation.bounds.size.width)
                let errorHeight = CGFloat(highlight.bounds.size.height)/CGFloat(annotation.bounds.size.height)
                
                if errorPointX <= 1.0 && errorPointY <= 1.0 && errorWidth <= 1.0 && errorHeight <=  1.0 {
                    page.removeAnnotation(annotation)
                    let dictionary = ["pageNumber":(self.pdfDocument.index(for: page)),"annotationList":[
                        "type":"highlight",
                        "X":CGFloat(highlight.bounds.origin.x),
                        "Y":CGFloat(highlight.bounds.origin.y),
                        "W":CGFloat(highlight.bounds.size.width),
                        "H":CGFloat(highlight.bounds.size.height)]] as [String : Any]
                    
                    if let index = currentAnnotationJson.index(where: {$0 == dictionary as NSDictionary}){
                        //print("------- found object  ------")
                        currentAnnotationJson.remove(at: index)
                    }
                    
                }
            }
        })
        
        return currentAnnotationJson
    }*/
    
    
    /*/clear all highlight
    func clearAllHighlight(_ page:PDFPage, currentAnnotation:[NSDictionary]) -> [NSDictionary] {
        var currentAnnotationJson = currentAnnotation
        page.annotations.forEach({ annotation in
            page.removeAnnotation(annotation)
        })
        
        for annotation in currentAnnotationJson{
            //find index's object in array
            //let index = alphabets.index(where: {$0 == "A"})
            if let index = currentAnnotationJson.index(where: {$0 == annotation}), annotation.object(forKey: "pageNumber") as! Int == (self.pdfDocument.index(for: page)) {
                currentAnnotationJson.remove(at: index)
                // self.saveAnnotationToFile(0)
            }
        }
        
        return currentAnnotationJson
    }*/
    
    
    func setAnnotation(currentAnnotationArray:[NSDictionary], pdfView:PDFView) {
        for dic in currentAnnotationArray{
            let annotationListDic = dic["annotationList"] as! NSDictionary
            let pageNumber = dic["pageNumber"] as! Int
            let annotationType = (annotationListDic["type"] as? String ?? "").lowercased()
            if  let page = pdfView.currentPage, pageNumber == SP.shared.actualPageNumber(page: page), annotationType == "highlight" ||
                annotationType == "underline" || annotationType == "strikeout" {
                
                let pointX = CGFloat(annotationListDic["X"] as! CGFloat)
                let pointY =  CGFloat(annotationListDic["Y"] as! CGFloat)
                let pointW =  CGFloat(annotationListDic["W"] as! CGFloat)
                let pointH =  CGFloat(annotationListDic["H"] as! CGFloat)
                
//                let pageBounds = page.bounds(for: .mediaBox)
//                let scX:CGFloat = pageBounds.width/1043
//                let scY:CGFloat = pageBounds.height/1497
//                let mirror = CGAffineTransform(scaleX: scX,y: -scY)
//                let translate = CGAffineTransform(translationX: 0,y: pageBounds.size.height)
//                let concatenated = mirror.concatenating(translate)
//                let origin = CGPoint(x: pointX, y: pointY).applying(concatenated)
//                let bounds = CGRect(origin: origin, size: .init(width: pointW*scX, height: pointH*scY))
//                let bounds = pageBounds
                let bounds = CGRect(x: pointX, y: pointY, width: pointW, height: pointH)
                var anotation = PDFAnnotation()
                var color:UIColor = .yellow
                if annotationType == "highlight" {
                    if let colorString = annotationListDic["color"] as? String{
                        color =  UIColor.init(hexString: colorString)
                    }
                    
                    anotation = PDFAnnotation(bounds: bounds, forType:.highlight, withProperties: nil)
                    anotation.color = color
                    anotation.endLineStyle = .square
                }
               
                if annotationType == "underline" {
                    if let colorString = annotationListDic["color"] as? String{
                        color =  UIColor.init(hexString: colorString)
                    }
                    anotation = PDFAnnotation(bounds: bounds, forType: .underline, withProperties: nil)
                    anotation.color = color
                    anotation.endLineStyle = .square
                }
                
                if annotationType == "strikeout" {
                    if let colorString = annotationListDic["color"] as? String {
                        color =  UIColor.init(hexString: colorString)
                    }
                    anotation = PDFAnnotation(bounds: bounds, forType: .strikeOut, withProperties: nil)
                    anotation.color = color
                    anotation.endLineStyle = .square
                }
            
                if let tag = annotationListDic["tag"] as? Int {
                    anotation.userName = "\(tag)"
                }
                guard let page = pdfView.currentPage else {return}
                page.addAnnotation(anotation)
            }
        }
    }
}
//MARK:- Ink Annotation
@available(iOS 11.0, *)
extension Annotation {
    //Draw inkAnnotation from Json dict return value to current PDFPage Tuple info for editting
    func setInkAnnotation(jsonDict:[NSDictionary], pdfView:PDFView) -> [(page: Int, tag: Int, color: String, width: CGFloat, points:[CGPoint])] {
        var inkAnnotation:[(page: Int, tag: Int, color: String, width: CGFloat, points:[CGPoint])] = []
        if let page = pdfView.currentPage {
            //filter only current page
            let curPageNo = SP.shared.actualPageNumber(page: page)
            let jsonPage = jsonDict.filter{($0["pageNumber"] as? Int) ?? 0 == curPageNo}
            for pageInfo in jsonPage {
                if let newInk = self.path2Annotation(pageInfo, page: page) {
                    page.addAnnotation(newInk)
                    if let pathnfo = pageInfo["annotationList"] as? NSDictionary,
                        let tag = pathnfo["tag"] as? Int,
                        let color = pathnfo["color"] as? String,
                        let width = pathnfo["width"] as? CGFloat,
                        let points = pathnfo["points"] as? [[String:CGFloat]] {
                        let pointArray = points.map{CGPoint(x: $0["X"]!, y: $0["Y"]!)}
                        inkAnnotation.append((page: curPageNo, tag: tag, color: color, width: width, points: pointArray))
                    }
                }
            }
        }
        return inkAnnotation
    }
    
    func path2Annotation(_ info: NSDictionary, page: PDFPage) -> PDFAnnotation? {
        guard
            let inkDict = info["annotationList"] as? [String:Any],
            let brushColor = inkDict["color"] as? String,
            let brushWidth = inkDict["width"] as? CGFloat,
            let points = inkDict["points"] as? [[String:CGFloat]], points.count > 1
            else {
                return nil
        }
        var inkAnnotation: PDFAnnotation?
        let path = UIBezierPath()        
        let p0 = CGPoint(x: points[0]["X"]!, y: points[0]["Y"]!)
        path.move(to: p0)
        for i in 1..<points.count {
            let p = CGPoint(x: points[i]["X"]!, y: points[i]["Y"]!)
            path.addLine(to: p)
        }
        let pageBounds = page.bounds(for: .mediaBox)
//        let scX:CGFloat = pageBounds.width/1043
//        let scY:CGFloat = pageBounds.height/1497
//        let mirror = CGAffineTransform(scaleX: scX,y: -scY)
//        let translate = CGAffineTransform(translationX: 0,y: pageBounds.size.height)
//        let concatenated = mirror.concatenating(translate)
//        path.apply(concatenated)
        
        let b = PDFBorder()
        b.lineWidth = brushWidth*1.5
        inkAnnotation = PDFAnnotation(bounds: pageBounds, forType: .ink, withProperties:nil)
        inkAnnotation?.add(path)
        inkAnnotation?.border = b
        inkAnnotation?.color = UIColor(hexString: brushColor).withAlphaComponent(inkAlpha)
        return inkAnnotation
    }
    func clearAllInkAnnotations(page: PDFPage) {
        let pageInkAnnotations = page.annotations.filter{($0.type ?? "") == "Ink" }
        for an in pageInkAnnotations {
            page.removeAnnotation(an)
        }
    }
    //Path -> Json, Json -> Path
    //Ink Drawing make Json
    func inkPath2Json(_ pathInfo: [(page: Int, tag: Int, color: String, width: CGFloat, points: [CGPoint])]) -> [NSDictionary] {
        var jsonArray:[NSDictionary] = []
        for linePath in pathInfo {
            var pointArray: [NSDictionary] = []
            for point in linePath.points {
                let pointDict:NSDictionary =
                    ["X": point.x,
                     "Y": point.y]
                pointArray.append(pointDict)
            }
            let lineDict = ["tag": linePath.tag,
                            "color": linePath.color,
                            "width": linePath.width,
                            "points": pointArray] as [String : Any] as NSDictionary
            let dictJson = ["pageNumber": linePath.page,
                            "annotationList": lineDict] as [String : Any] as NSDictionary
            jsonArray.append(dictJson)
        }
        return jsonArray
    }
}

//MARK:- Check if Annotations touched
@available(iOS 11.0, *)
extension Annotation {
    func getSelectedAnnotations(at p: CGPoint, in annotationArray: [NSDictionary]) -> NSDictionary? {
        for jsDict in annotationArray {
            if let dict = jsDict["annotationList"] as? NSDictionary {
                if let x = dict["X"] as? CGFloat,
                    let y = dict["Y"] as? CGFloat,
                    let w = dict["W"] as? CGFloat,
                    let h = dict["H"] as? CGFloat {
                    let rect = CGRect(x: x, y: y, width: w, height: h)
                    if rect.contains(p) {
                        return jsDict
                    }
                }
            }
        }
        return nil
    }
}

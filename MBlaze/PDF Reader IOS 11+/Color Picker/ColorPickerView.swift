//
//  ColorPickerView.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 25/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit


class SuggestColorPickerView: UIView {
    
    @IBOutlet weak var btnSuggest1: UIButton!
    @IBOutlet weak var btnSuggest2: UIButton!
    @IBOutlet weak var btnSuggest3: UIButton!
    @IBOutlet weak var btnSuggest4: UIButton!
    @IBOutlet weak var btnSuggest5: UIButton!
    @IBOutlet weak var btnSuggest6: UIButton!
    
    @IBOutlet weak var btnSuggest7: UIButton!
    @IBOutlet weak var btnSuggest8: UIButton!
    @IBOutlet weak var btnSuggest9: UIButton!
    @IBOutlet weak var btnSuggest10: UIButton!
    @IBOutlet weak var btnSuggest11: UIButton!
    @IBOutlet weak var btnSuggest12: UIButton!
    
    
    class func instanceFromNib() -> UIView {
            return UINib(nibName: "SuggestColorPickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        }
    
}


public class ColorPickerView: UIView, ChromaColorPickerDelegate {
 
    var displayView:UIView!
    var colorPicker: ChromaColorPicker!
    var currentColor:UIColor!
    open var delegate: ColorPickerDeletegate?
    
   public func getColorAnnotation() -> UIColor{
        if UserDefaults.standard.string(forKey: "ColorPicker") != nil{
            return UIColor.init(hexString: UserDefaults.standard.string(forKey: "ColorPicker")!)
        }
        else{
            return .yellow
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(hexString: "#424242")
        self.clipsToBounds = true
        self.layer.cornerRadius = 9
        
        let btnSize = 22
        let btnClose = UIButton.init(frame: CGRect(x: 12, y: 12, width: btnSize, height: btnSize))
        btnClose.setImage(UIImage(named: "close-white"), for: .normal)
        btnClose.addTarget(self, action: #selector(dismissButtonDidTapped(_:)), for: .touchUpInside)
        
        let btnDone = UIButton.init(frame: CGRect(x: Int(frame.size.width)-(btnSize+12), y: 12, width: btnSize, height: btnSize))
        btnDone.setImage(UIImage(named: "checked"), for: .normal)
        btnDone.addTarget(self, action: #selector(doneButtonDidTapped(_:)), for: .touchUpInside)
        
        let displayWidth = Int(frame.size.width-(20*2))
        displayView = UIView(frame: CGRect(x: 20, y: 40, width: displayWidth, height: displayWidth))
        //displayView.backgroundColor = .black
       
        /* Calculate relative size and origin in bounds */
        let pickerSize = CGSize(width: displayView.bounds.width, height: displayView.bounds.width)
        let pickerOrigin = CGPoint(x: displayView.bounds.midX - pickerSize.width/2, y: displayView.bounds.midY - pickerSize.height/2)
        
        //set current color
        currentColor = UIColor.init(hexString: UserDefaults.standard.string(forKey: "ColorPicker")!)
        
        /* Create Color Picker */
        colorPicker = ChromaColorPicker(frame: CGRect(origin: pickerOrigin, size: pickerSize))
        colorPicker.delegate = self
        
        /* Customize the view (optional) */
        colorPicker.padding = 0
        colorPicker.stroke = 15 //stroke of the rainbow circle
        colorPicker.currentAngle = Float.pi
        colorPicker.adjustToColor(currentColor != nil ? currentColor:.yellow)
        
       // print("current color:\(String(describing: currentColor.hexCode))")
        
        /* Customize for grayscale (optional) */
        colorPicker.supportsShadesOfGray = true // false by default
        //colorPicker.colorToggleButton.grayColorGradientLayer.colors = [UIColor.lightGray.cgColor, UIColor.gray.cgColor] // You can also override gradient colors
        
        colorPicker.hexLabel.isHidden = true
        colorPicker.handleLine.isHidden = true
        
        /* Don't want an element like the shade slider? Just hide it: */
        //colorPicker.shadeSlider.hidden = true
        
        displayView.addSubview(colorPicker)
        
        //suggest color
        let suggestColorView = SuggestColorPickerView.instanceFromNib() as! SuggestColorPickerView
        suggestColorView.frame = CGRect(x: 20, y: (95+20+262), width: Int(frame.size.width)-(40), height: 95)
        suggestColorView.backgroundColor = .clear
        addTarget( suggestColorView.btnSuggest1)
        addTarget( suggestColorView.btnSuggest2)
        addTarget( suggestColorView.btnSuggest3)
        addTarget( suggestColorView.btnSuggest4)
        addTarget( suggestColorView.btnSuggest5)
        addTarget( suggestColorView.btnSuggest6)
        addTarget( suggestColorView.btnSuggest7)
        addTarget( suggestColorView.btnSuggest8)
        addTarget( suggestColorView.btnSuggest9)
        addTarget( suggestColorView.btnSuggest10)
        addTarget( suggestColorView.btnSuggest11)
        addTarget( suggestColorView.btnSuggest12)
    
        self.addSubview(btnDone)
        self.addSubview(btnClose)
        self.addSubview(displayView)
        self.addSubview(suggestColorView)
        
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    public func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        //print("selected color :\(color.hexCode)")
        currentColor = color
    }
    
    private func addTarget(_ sender:UIButton){
        sender.clipsToBounds = true
        sender.layer.cornerRadius = 8
        sender.layer.borderWidth = 1
        sender.layer.borderColor = UIColor.white.cgColor
         sender.addTarget(self, action: #selector(suggestColorDidSelected(_:)), for: .touchUpInside)
    }
    
    @objc func suggestColorDidSelected(_ sender:UIButton){
        //print("suggest color selected:\(String(describing: sender.backgroundColor?.hexCode))")
        currentColor = sender.backgroundColor!
        colorPicker.adjustToColor(sender.backgroundColor!)
    }
    
    @objc func doneButtonDidTapped(_ sender:UIButton){
        self.delegate?.didSelectedColor(currentColor)
    }
    
    @objc func dismissButtonDidTapped(_ sender:UIButton){
        self.delegate?.dismissColorPicker()
    }
}


public protocol ColorPickerDeletegate {
    func didSelectedColor(_ color:UIColor)
    func dismissColorPicker()
}


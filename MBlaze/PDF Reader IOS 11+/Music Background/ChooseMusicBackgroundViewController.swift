//
//  ChooseMusicBackgroundViewController.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 30/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import SwiftyJSON
import AVKit


class  CloudMusicBackgroundTableRowCell: UITableViewCell {
    //cloudBGMCell
    @IBOutlet weak var trackTitleLabel: UILabel!
    @IBOutlet weak var previewTrackButton: UIButton!
    
}

class MusicBackgroundTableRowCell: UITableViewCell {
    
    @IBOutlet weak var imageChecked: UIImageView!
    @IBOutlet weak var labelSongTitle: UILabel!
    
    @IBOutlet weak var labelArtistTitle: UILabel!
    
    @IBOutlet weak var labelTrackDuration: UILabel!
    
}

class SongItem {
    var title: String
    var artist: String
    var trackDuration :TimeInterval
    var persistentID: UInt64
    
    init(_ title:String, artist:String, trackDuration:TimeInterval, persistentID:UInt64) {
        self.title = title
        self.artist = artist
        self.trackDuration = trackDuration
        self.persistentID = persistentID
    }
    
}

protocol MusicBackgroundDelegate {
    //func didSongChoosed(_ songlist:[MPMediaItem])
    func didLocalSongChoosed(_ songlist:[MPMediaItem])
    func didCloudSongChoosed(_ cloudList:[String])
}

@available(iOS 11.0, *)
class ChooseMusicBackgroundViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var strURLToLoadMusicBackground:String?
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var buttonChoosed: UIButton!
    
    @IBOutlet weak var buttonSaraPadSong:UIButton!
    @IBOutlet weak var buttonDeviceSong:UIButton!
    
    var arrayIndexChoosedSaraPadSong = [Int]()
    var arraySaraPadSongItem = [JSON]()
    
    var arrayIndexChoosedDeviceSong = [Int]()
    var arrayOnDeviceSongItem = [SongItem]()
    var isChooseSongOnDevice = false
    
    var currentURLSelected:String?
    var currentCellSelected:CloudMusicBackgroundTableRowCell?
    var sectionItemHide:NSMutableDictionary = NSMutableDictionary()
    
    var bgmPreviewPlayer:AVPlayer?
    
    open var delegate:MusicBackgroundDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Music Background"
        
        do {
//            try AVAudioSession.sharedInstance().setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playback))
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.duckOthers])
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        // let navigationHeight = navigationController?.navigationBar.frame.size.height;
        // let navigationImageSize = CGSize(width: navigationHeight!-20, height: navigationHeight!-20)
        
        let selectALL = UIBarButtonItem(image:#imageLiteral(resourceName: "select-all"), style: .plain, target: self, action: #selector(chooseAllSongTapping(_:)))
        let clear = UIBarButtonItem(image:#imageLiteral(resourceName: "clear-select"), style: .plain, target: self, action: #selector(clearSelectAllSongTapping(_:)))
        navigationItem.leftBarButtonItems = [selectALL, clear]
        
        let closeButton = UIBarButtonItem(image: UIImage.init(named: "close_black"), style: .done, target: self, action: #selector(closeBarButton(_:)))
        closeButton.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = closeButton
        
        buttonChoosed.layer.cornerRadius = 9
        buttonChoosed.setTitle("Done", for: .normal)
        
        buttonSaraPadSong.clipsToBounds = true
        buttonSaraPadSong.layer.cornerRadius = 9
        buttonSaraPadSong.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        buttonDeviceSong.clipsToBounds = true
        buttonDeviceSong.layer.cornerRadius = 9
        buttonDeviceSong.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        //update button UI
        updateHeaderAppearanceSelected()
        
        self.mTableView.register(UINib(nibName: "CloudMusicBackgroundTableRowCell", bundle: nil), forCellReuseIdentifier: "cloudBGMCell")
        
        self.mTableView.register(UINib(nibName: "LocalMusicBackgroundTableRowCell", bundle: nil), forCellReuseIdentifier: "localBGMCell")
        
        //load BMG List
        loadCloudMusic()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func CloudMusicTapping(_ sender: Any) {
        currentCellSelected = nil
        arrayIndexChoosedSaraPadSong.removeAll()
        arrayIndexChoosedDeviceSong.removeAll()
        isChooseSongOnDevice = false
        updateTotalSong()
        updateHeaderAppearanceSelected()
        self.mTableView.reloadData()
        self.mTableView.scrollsToTop = true
       // loadCloudMusic()
    }
    
    @IBAction func LocalMusicTapping(_ sender: Any) {
        stopPreviewAudio()
        arrayIndexChoosedSaraPadSong.removeAll()
        arrayIndexChoosedDeviceSong.removeAll()
        isChooseSongOnDevice = true
        updateTotalSong()
        accessSongOnDevice()
        updateHeaderAppearanceSelected()
    }
    
    
    @objc func closeBarButton(_ sender:UIBarButtonItem){
        stopPreviewAudio()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func chooseAllSongTapping(_ sender:UIBarButtonItem){
        if isChooseSongOnDevice {
            arrayIndexChoosedDeviceSong.removeAll()
            for index in 0..<arrayOnDeviceSongItem.count {
                arrayIndexChoosedDeviceSong.append(index)
            }
        }
        else{
            arrayIndexChoosedSaraPadSong.removeAll()
            for index in 0..<arraySaraPadSongItem.count {
                arrayIndexChoosedSaraPadSong.append(index)
            }
        }
        
        self.mTableView.reloadData()
        updateTotalSong()
        
    }
    
    @objc func clearSelectAllSongTapping(_ sender:UIBarButtonItem){
        removeAllSongChoosed()
    }
    
    func removeAllSongChoosed() {
        arrayIndexChoosedSaraPadSong.removeAll()
        arrayIndexChoosedDeviceSong.removeAll()
        self.mTableView.reloadData()
        updateTotalSong()
    }
    
    @IBAction func songChoosedTapping(_ sender: UIButton) {
        stopPreviewAudio()
        if isChooseSongOnDevice &&  arrayIndexChoosedDeviceSong.count > 0 {
            var songSelectedlist = [UInt64]()
            for index in self.arrayIndexChoosedDeviceSong {
                songSelectedlist.append(arrayOnDeviceSongItem[index].persistentID)
            }
            
            var songListItem = [MPMediaItem]()
            for song in MPMediaQuery.songs().items! {
                if songSelectedlist.contains(where: {$0 == song.persistentID}){
                    songListItem.append(song)
                    // print("Song : \(String(describing: song.title)) \n Artist : \(String(describing: song.artist))")
                }
            }
            
            delegate?.didLocalSongChoosed(songListItem)
            self.dismiss(animated: true, completion: nil)
        }
            
        else if arrayIndexChoosedSaraPadSong.count > 0 {
            var cloudPlaylistURL:[String] = [String]()
            
            for index in self.arrayIndexChoosedSaraPadSong {
                let songItem = arraySaraPadSongItem[index].dictionary!["ITEMS"]?.array
                for trackURL in songItem! {
                    //print("url : \(trackURL["TRACKURL"].stringValue)")
                    cloudPlaylistURL.append(trackURL["TRACKURL"].stringValue)
                }
            }
            delegate?.didCloudSongChoosed(cloudPlaylistURL)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isChooseSongOnDevice ? 1 : arraySaraPadSongItem.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isChooseSongOnDevice ? arrayOnDeviceSongItem.count : (arraySaraPadSongItem[section].dictionary!["ITEMS"]?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var mCell:UITableViewCell?
        if isChooseSongOnDevice {
            let cell = tableView.dequeueReusableCell(withIdentifier: "localBGMCell", for: indexPath) as! MusicBackgroundTableRowCell
            
            let songItem = arrayOnDeviceSongItem[indexPath.row]
            
            cell.imageChecked.image = #imageLiteral(resourceName: "uncheck-box")
            if arrayIndexChoosedDeviceSong.contains(indexPath.row){
                cell.imageChecked.image = #imageLiteral(resourceName: "check-box")
            }
            cell.labelSongTitle.text = songItem.title
            cell.labelArtistTitle.text = songItem.artist
            cell.labelTrackDuration.text = songItem.trackDuration.stringFromTimeInterval()
            
            mCell = cell
        }
            
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cloudBGMCell", for: indexPath) as! CloudMusicBackgroundTableRowCell
            
            let songItem = arraySaraPadSongItem[indexPath.section].dictionary!["ITEMS"]?.array
            cell.trackTitleLabel.text = songItem![indexPath.row].dictionary!["TRACKNAME"]?.stringValue
            cell.accessibilityValue = songItem![indexPath.row].dictionary!["TRACKURL"]?.stringValue
            cell.tag = indexPath.row
             
            if currentCellSelected != nil && cell.accessibilityValue == currentURLSelected {
                cell.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-pause-button"), for: .normal)
            }
            else {
                cell.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-play-button"), for: .normal)
            }
        
            mCell = cell
        }
        
        return mCell!
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !isChooseSongOnDevice {
            let sectionView = UINib(nibName: "MusicBackgroundHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(choosePlaylist(_:)))
            tap.accessibilityValue = String.init(section)
            sectionView.isUserInteractionEnabled = true
            sectionView.addGestureRecognizer(tap)
            
            //imageView Select
            let imageViewChoosed = sectionView.viewWithTag(100) as! UIImageView
            if arrayIndexChoosedSaraPadSong.contains(section){
                imageViewChoosed.image = #imageLiteral(resourceName: "check-box")
            }
            else{
                imageViewChoosed.image = #imageLiteral(resourceName: "uncheck-box")
            }
            
            // playlist name
            let strPlaylist =  (arraySaraPadSongItem[section].dictionary!["BGMNAME"]?.stringValue)! + " (" + (arraySaraPadSongItem[section].dictionary!["DURATION"]?.stringValue)! + " min)"
            
            let playListLabel = sectionView.viewWithTag(101) as! UILabel
            playListLabel.text = strPlaylist
            
            //set accessibilityLabel = H --> at storyboard
            let dropdownButton = sectionView.viewWithTag(102) as! UIButton
            dropdownButton.addTarget(self, action: #selector(showHideCloudTrackItems(_:)), for: .touchUpInside)
            dropdownButton.tag = section
            
            if sectionItemHide.count == 0  || (sectionItemHide.object(forKey: String.init(section)) == nil){
                sectionItemHide.setValue(dropdownButton.accessibilityLabel!, forKey: String.init(section))
            }
            //print("check : \(sectionItemHide.allValues)")
            //print("0 sectionItemHide : \(sectionItemHide)")
            
            if sectionItemHide.object(forKey: String.init(section)) as! String == "H" {
                dropdownButton.setImage(#imageLiteral(resourceName: "arrow-black-down"), for: .normal)
            }
            else {
                dropdownButton.setImage(#imageLiteral(resourceName: "arrow-black-up"), for: .normal)
            }
            
            return sectionView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isChooseSongOnDevice {
            return 50
        }
        else {
            if (sectionItemHide.count > 0) && (sectionItemHide.object(forKey: String.init(indexPath.section)) as! String != "H"){
                return 50
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isChooseSongOnDevice ? 0 : 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isChooseSongOnDevice {
            let cell = tableView.cellForRow(at: indexPath) as! MusicBackgroundTableRowCell
            if arrayIndexChoosedDeviceSong.contains(indexPath.row){
                cell.imageChecked.image = #imageLiteral(resourceName: "uncheck-box")
                if let index = arrayIndexChoosedDeviceSong.firstIndex(of: indexPath.row){
                    arrayIndexChoosedDeviceSong.remove(at: index)
                }
            }
            else{
                cell.imageChecked.image = #imageLiteral(resourceName: "check-box")
                arrayIndexChoosedDeviceSong.append(indexPath.row)
            }
            
            updateTotalSong()
        }
        else {
            let cell = tableView.cellForRow(at: indexPath) as! CloudMusicBackgroundTableRowCell
            prepareToplayMusicPreview(cell)
            
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func choosePlaylist(_ sender:UITapGestureRecognizer){
        let identifier = Int(sender.accessibilityValue!)
        //print("Tap secion : \(String(describing: identifier))")
        if arrayIndexChoosedSaraPadSong.contains(identifier!){
            if let index = arrayIndexChoosedSaraPadSong.firstIndex(of: identifier!){
                arrayIndexChoosedSaraPadSong.remove(at: index)
            }
        }
        else{
            arrayIndexChoosedSaraPadSong.append(identifier!)
        }
        updateTotalSong()
        self.mTableView.reloadSections(IndexSet(integer: identifier!), with: .automatic)
    }
    
    @objc func showHideCloudTrackItems(_ sender:UIButton){
        if sectionItemHide.object(forKey: String.init(sender.tag)) as! String == "H" {
            //sender.accessibilityLabel = "S"
            sender.setImage(#imageLiteral(resourceName: "arrow-black-up"), for: .normal)
            sectionItemHide.setValue("S", forKey: String.init(sender.tag))
        }
        else {
            //sender.accessibilityLabel = "H"
            sender.setImage(#imageLiteral(resourceName: "arrow-black-down"), for: .normal)
            sectionItemHide.setValue("H", forKey: String.init(sender.tag))
        }
        
        self.mTableView.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
    }
    
    
    private func updateHeaderAppearanceSelected(){
        
        if !isChooseSongOnDevice {
            buttonSaraPadSong?.backgroundColor = .darkGray
            buttonSaraPadSong?.setTitleColor(.white, for: .normal)
            
            buttonDeviceSong?.backgroundColor = .lightGray
            buttonDeviceSong?.setTitleColor(.black, for: .normal)
        }
            
        else {
            buttonDeviceSong?.backgroundColor = .darkGray
            buttonDeviceSong?.setTitleColor(.white, for: .normal)
            
            buttonSaraPadSong?.backgroundColor = .lightGray
            buttonSaraPadSong?.setTitleColor(.black, for: .normal)
        }
    }
    
    private func prepareToplayMusicPreview(_ cell:CloudMusicBackgroundTableRowCell){
        if  cell.previewTrackButton.currentImage == UIImage.init(named: "bgm-preview-pause-button"){
            stopPreviewAudio()
            cell.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-play-button"), for: .normal)
            currentURLSelected = ""
            currentCellSelected = nil
        }
        else {
            // cell.accessibilityValue
            stopPreviewAudio()
            if currentCellSelected != nil && cell != currentCellSelected{
                currentCellSelected?.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-play-button"), for: .normal)
            }
            currentCellSelected = cell
            currentURLSelected = cell.accessibilityValue!
            
            guard let url = URL.init(string: cell.accessibilityValue!)
                else {
                    print("Preview Bgm Cloud url error")
                    return
            }
            
            let playerItem:AVPlayerItem = AVPlayerItem(url: url)
            bgmPreviewPlayer = AVPlayer(playerItem: playerItem)
            bgmPreviewPlayer?.rate = 1.0
            bgmPreviewPlayer?.play()
            cell.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-pause-button"), for: .normal)
        }
    }
    
    private func stopPreviewAudio() {
        if bgmPreviewPlayer != nil && (bgmPreviewPlayer?.isPlaying)! {
            bgmPreviewPlayer?.pause()
            bgmPreviewPlayer?.rate = 0.0
            
            if currentCellSelected != nil{
                currentCellSelected?.previewTrackButton.setImage(#imageLiteral(resourceName: "bgm-preview-play-button"), for: .normal)
            }
        }
    }
    
    //Mark : Access song on device
    private func accessSongOnDevice(){
        if !(arrayOnDeviceSongItem.count > 0) {
            MPMediaLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    if status == .authorized {
                        SVProgressHUD.show()
                        self.getSongItems()
                    } else {
                        self.presentAlert(withTitle: "Access Denied", message: "Sara+Pad can't access your music, Please check permissions.")
                    }
                }
            }
        }
        else{
            self.mTableView.reloadData()
            self.mTableView.scrollsToTop = true
            
        }
        
    }
    
    private func getSongItems(){
        if let songListItem = MPMediaQuery.songs().items {
            for song in songListItem  {
                let songItem = SongItem.init(song.title!, artist: song.artist != nil ? song.artist! : "", trackDuration: song.playbackDuration, persistentID: song.persistentID)
                arrayOnDeviceSongItem.append(songItem)
                //print("Song : \(songItem.title) \n Artist : \(songItem.artist) \n Time : \(songItem.playbackDuration)")
            }
            
            SVProgressHUD.dismiss()
            self.mTableView.reloadData()
            self.mTableView.scrollsToTop = true
        }
        
    }
    
    private func updateTotalSong(){
        if isChooseSongOnDevice {
            let queue = DispatchQueue(label: "updateTotalSong")
            queue.async {
                //coding
                var timeTrack = TimeInterval()
                for index in self.arrayIndexChoosedDeviceSong {
                    timeTrack = timeTrack + self.arrayOnDeviceSongItem[index].trackDuration
                }
                
                DispatchQueue.main.async {
                    //update ui
                    let btnTitle = "Done (" + String(self.arrayIndexChoosedDeviceSong.count) + " song, " + timeTrack.stringFromTimeInterval() + " min)"
                    self.buttonChoosed.setTitle(btnTitle, for: .normal)
                }
            }
        }
        else {
            var totalDuration:Double = 0.00
            for index in self.arrayIndexChoosedSaraPadSong {
                //print("\((arraySaraPadSongItem[index].dictionary!["DURATION"]?.stringValue))")
                let playlistTime = Double((arraySaraPadSongItem[index].dictionary!["DURATION"]?.stringValue)!)
                totalDuration = totalDuration + playlistTime!
                
                // print("\(totalDuration)")
            }
            let btnTitle = "Done (" + String(self.arrayIndexChoosedSaraPadSong.count) + " song, " + String(totalDuration) + " min)"
            self.buttonChoosed.setTitle(btnTitle, for: .normal)
        }
    }
    
    
    func loadCloudMusic() {
        if let strUrl = strURLToLoadMusicBackground {
            SVProgressHUD.show()
            
            NotificationCenter.default.addObserver(self, selector: #selector(loadMusicBackgroundResponse(_:)), name: NSNotification.Name(rawValue: "LoadBGM"), object: nil)
            ServiceAsynchronousRequest.call(strUrl: strUrl, notificationName: "LoadBGM", isAllowHeader: true)
        }
    }
    
    @objc func loadMusicBackgroundResponse(_ notification:Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LoadBGM"), object: nil)
        SVProgressHUD.dismiss()
        if notification.userInfo!["Data"] != nil {
            if let jsonArray = (notification.userInfo!["Data"] as! JSON).array{
                //print("BGN List : \(jsonArray)")
                arraySaraPadSongItem = jsonArray
                self.mTableView.reloadData()
                self.mTableView.scrollsToTop = true
            }
        }
    }
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

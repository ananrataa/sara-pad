//
//  NoteTextView.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 16/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    convenience init(android c: Int) {
        let r = CGFloat((c>>16)&0xFF)/255.0
        let g = CGFloat((c>>8)&0xFF)/255.0
        let b = CGFloat((c)&0xFF)/255.0
        let a = CGFloat((c>>24)&0xFF)/255.0
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}


public class BookMarkNoteView : UIView {
    
    var cancelBtn:UIButton?
    var saveBtn:UIButton?
    var textLength:UILabel?
    var bookmarkPage:UILabel?
    var bookmarkTextView:UITextView?
    
    final let BUTTON_WIDTH:Double  = 70
    final let BUTTON_HEIGHT:Double = 25
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        self.backgroundColor = UIColor.init(hexString: "#f5f5f5")
        self.layer.cornerRadius = 5
        
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath;
        
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: 40))
        topView.backgroundColor = UIColor.init(hexString: "#e6e6e6")
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 5
        
        let reminder = UILabel(frame: CGRect(x: 8+BUTTON_WIDTH, y: 7.5, width: Double.init(self.frame.size.width)-(16+BUTTON_WIDTH+BUTTON_WIDTH), height: BUTTON_HEIGHT))
        reminder.text = "Note Text"
        reminder.textColor = .black
        reminder.font = UIFont.systemFont(ofSize: 15)
        reminder.textAlignment = .center
        
        cancelBtn = UIButton(frame: CGRect(x: 8, y: 7.5, width: BUTTON_WIDTH, height: BUTTON_HEIGHT))
        cancelBtn?.setTitle("Cancel", for: .normal)
        cancelBtn?.setTitleColor(.black, for: .normal)
        cancelBtn?.setTitleColor(.lightGray, for: .highlighted)
        cancelBtn?.contentHorizontalAlignment = .center
        cancelBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        cancelBtn?.clipsToBounds = true
        cancelBtn?.layer.cornerRadius = 10
        cancelBtn?.backgroundColor = UIColor.init(hexString: "#B8B8B8")
        self.addSubview(cancelBtn!)
        
        saveBtn = UIButton(frame: CGRect(x: Double.init(self.frame.size.width)-(BUTTON_WIDTH+8), y: 7.5, width: BUTTON_WIDTH, height: BUTTON_HEIGHT))
        saveBtn?.setTitle("Save", for: .normal)
        saveBtn?.setTitleColor(.darkText, for: .normal)
        saveBtn?.setTitleColor(.lightGray, for: .highlighted)
        saveBtn?.contentHorizontalAlignment = .center
        saveBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        saveBtn?.clipsToBounds = true
        saveBtn?.layer.cornerRadius = 10
        saveBtn?.backgroundColor = UIColor.init(hexString: "#B8B8B8")
        self.addSubview(saveBtn!)
        
        bookmarkPage = UILabel(frame: CGRect(x:10, y: Double.init(topView.frame.size.height+5), width: 200, height: 20))
        bookmarkPage?.backgroundColor = .clear
        bookmarkPage?.textAlignment = .left
        bookmarkPage?.text = "หน้าที่: ";
        bookmarkPage?.textColor = .darkGray
        bookmarkPage?.font = UIFont.systemFont(ofSize: 13)
        
        textLength = UILabel(frame: CGRect(x:Double.init(topView.frame.size.width-110), y: Double.init(topView.frame.size.height+5), width: 100, height: 20))
        textLength?.backgroundColor = .clear
        textLength?.textAlignment = .right
        textLength?.text = "0/250"
        textLength?.textColor = .darkGray
        textLength?.font = UIFont.systemFont(ofSize: 13)
        
        let bookmarkYpoint = Double.init((textLength?.frame.origin.y)!) + Double.init((textLength?.frame.size.height)!) + 5
        
        bookmarkTextView = UITextView(frame: CGRect(x: 5, y: Double.init(bookmarkYpoint), width: Double.init(frame.size.width-10), height: Double.init(frame.size.height)-(bookmarkYpoint+5)))
        bookmarkTextView?.backgroundColor = .white
        bookmarkTextView?.textAlignment = .left
        bookmarkTextView?.textColor = .black
        bookmarkTextView?.font = UIFont.systemFont(ofSize: 15)
        bookmarkTextView?.autocorrectionType = .no
        
        topView.addSubview(cancelBtn!)
        topView.addSubview(reminder)
        topView.addSubview(saveBtn!)
        
        self.addSubview(topView)
        self.addSubview(bookmarkPage!)
        self.addSubview(textLength!)
        self.addSubview(bookmarkTextView!)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

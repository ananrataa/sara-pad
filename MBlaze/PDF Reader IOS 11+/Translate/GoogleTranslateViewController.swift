//
//  GoogleTranslateViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 16/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

extension UIImage {
    func resizeImage(_ newSize: CGSize) -> UIImage? {
        func isSameSize(_ newSize: CGSize) -> Bool {
            return size == newSize
        }
        
        func scaleImage(_ newSize: CGSize) -> UIImage? {
            func getScaledRect(_ newSize: CGSize) -> CGRect {
                let ratio   = max(newSize.width / size.width, newSize.height / size.height)
                let width   = size.width * ratio
                let height  = size.height * ratio
                return CGRect(x: 0, y: 0, width: width, height: height)
            }
            
            func _scaleImage(_ scaledRect: CGRect) -> UIImage? {
                UIGraphicsBeginImageContextWithOptions(scaledRect.size, false, 0.0);
                draw(in: scaledRect)
                let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
                UIGraphicsEndImageContext()
                return image
            }
            return _scaleImage(getScaledRect(newSize))
        }
        
        return isSameSize(newSize) ? self : scaleImage(newSize)!
    }
}

/*
@available(iOS 11.0, *)
class GoogleTranslateViewController: UITableViewController,LanguageListTranslateDelegate {

    var originalStr:String?
    var resultStr:String?

    let cellReuseIdentifier = "googleTranslateCell"
    let headerCellHeight:CGFloat = 50
    var cellRowHeight:CGFloat?
    
    var srcButton:UIButton?
    var destButton:UIButton?
    var translateButton:UIButton?
    
    var refreshTranslateResult:UITextView?
    
    var currentSourceLng:String?
    var currentDstLng:String?
    
    var keySourceLng:String!
    var keyDstLng:String!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Translate"
        
        let navigationHeight = navigationController?.navigationBar.frame.size.height;
        let navigationImageSize = CGSize(width: navigationHeight!-20, height: navigationHeight!-20)
        
        let backButton = UIBarButtonItem(image: UIImage.init(named: "arrow_back_black")?.resizeImage(navigationImageSize), style: .done, target: self, action: #selector(backBarButton(_:)))
        backButton.imageInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
        backButton.tintColor = UIColor.black
        navigationItem.leftBarButtonItem = backButton
        
        if let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"), let lngDict = NSDictionary(contentsOfFile: path){
            
            currentSourceLng = lngDict.object(forKey: keySourceLng) as? String
            currentDstLng = (lngDict.object(forKey: keyDstLng) as? String)
        }
        
        cellRowHeight = (tableView.frame.size.height - (20 + headerCellHeight + (self.navigationController?.navigationBar.frame.size.height)!))/2
       
        tableView.separatorColor = UIColor.clear
        tableView.isScrollEnabled = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backBarButton(_ sender:UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        let textView = cell.viewWithTag(101) as! UITextView
        textView.clipsToBounds = true
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.black.cgColor
        textView.text = indexPath.row == 0 ? originalStr : resultStr
        
        let textViewFrame = textView.frame
        let frameRect = CGRect(x: textViewFrame.origin.x, y: textViewFrame.origin.y, width: self.tableView.frame.size.width-32, height: (cellRowHeight!-32))
        textView.frame = frameRect
        
        if indexPath.row == 1 {
            refreshTranslateResult = textView
        }
        
        cell.addSubview(textView)
        
        cell.selectionStyle = .none
        
        tableView.separatorColor = UIColor.clear
        tableView.isScrollEnabled = false
    
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let headerView:UIView = Bundle.main.loadNibNamed("translateHeaderCell", owner: self, options: nil)?.first as? UIView else {
            // xib not loaded, or its top view is of the wrong type
            return nil
        }
        
        let sourceLanguage = headerView.viewWithTag(101) as! UIButton
        sourceLanguage.addTarget(self, action: #selector(self.sourceLanguage(_:)), for:  .touchUpInside)
        let destinationLanguage = headerView.viewWithTag(103) as! UIButton
        destinationLanguage.addTarget(self, action: #selector(self.destinationLanguage(_:)), for:  .touchUpInside)
        
        let translateLanguage = headerView.viewWithTag(104) as! UIButton
        translateLanguage.addTarget(self, action: #selector(self.translateLanguage(_:)), for:  .touchUpInside)
        translateLanguage.layer.cornerRadius = 9
        
        srcButton = sourceLanguage
        destButton = destinationLanguage
        translateButton = translateLanguage
        
        srcButton?.setTitle(currentSourceLng, for: .normal)
        destButton?.setTitle(currentDstLng, for: .normal)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return cellRowHeight!
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerCellHeight
    }
    
    
    @objc func sourceLanguage(_ sender:UIButton){
        sugueToLanguageDictionary(0)
    }
    
    @objc func destinationLanguage(_ sender:UIButton){
        sugueToLanguageDictionary(1)
    }
    
    @objc func translateLanguage(_ sender:UIButton){
        translate(originalStr!, srcKey: keySourceLng, destkey: keyDstLng)
    }

    func sugueToLanguageDictionary(_ flag:Int) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let languageDictionaryVC:LanguageListTranslateTableView = mainStoryboard.instantiateViewController(withIdentifier: "LanguageListTranslateTableView") as! LanguageListTranslateTableView
        languageDictionaryVC.currentSourceLng = keySourceLng
        languageDictionaryVC.currentDstLng = keyDstLng
        languageDictionaryVC.flag = flag
        languageDictionaryVC.delegate = self
        self.navigationController?.pushViewController(languageDictionaryVC, animated: true)
        
    }
    
    func didSelectedLanguage(_ selectedLngKey: String, selectedLngName: String, flag: Int) {
       // print("Key Lng : " + selectedLngKey );print("Name Lng : " + selectedLngName )
        if flag == 0 {
            keySourceLng = selectedLngKey
            srcButton?.setTitle(selectedLngName, for: .normal)
        }
        else{
            keyDstLng = selectedLngKey
            destButton?.setTitle(selectedLngName, for: .normal)
        }
    }

    @objc func translate(_ srcText:String , srcKey:String, destkey:String){
        SVProgressHUD.show(withStatus: "Translating...")
        
        if InternetConnection.isConnectedToNetwork(){
        
            let param = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" + srcKey + "&tl=" + destkey + "&ie=UTF-8&oe=UTF-8&dt=t&q=" + srcText
        
            print("param :",param)
            
            NotificationCenter.default.addObserver(self, selector: #selector(googleTranslate(_:)), name: NSNotification.Name(rawValue: "GoogleTranslate"), object: nil)
            ServiceAsynchronousRequest.call(strUrl: param,notificationName:"GoogleTranslate", isAllowHeader: false )
            //print("translate : ",jsonResult.description)
        }
        else{
            SVProgressHUD.dismiss()
            presentAlert(withTitle: "Failed", message: "Unable to connect to the Internet")
        }
    }
    
    @objc func googleTranslate(_ notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GoogleTranslate"), object: nil)
        var translateResult = ""
        if notification.userInfo!["Data"] != nil {
            if let jsonArray = (notification.userInfo!["Data"] as! JSON).array,(jsonArray.count) > 0,
                let arrayTranslate = jsonArray[0].array, arrayTranslate.count > 0 {
                //print("translate array :",arrayTranslate.count)
                for arrayTranslate1 in arrayTranslate{
                    let strTranslate = arrayTranslate1.array![0].stringValue
                    print("translate :",strTranslate)
                    translateResult += strTranslate
                }
                
            }
           // print("\n\n translateResult :",translateResult)
            resultStr = translateResult
            refreshTranslateResult?.text = translateResult
            SVProgressHUD.dismiss()
        }
        else{
            SVProgressHUD.dismiss()
            if notification.userInfo!["error"] != nil{
                presentAlert(withTitle: "Failed", message: notification.userInfo!["error"] as! String)
            }
        }
        
    }

}
*/

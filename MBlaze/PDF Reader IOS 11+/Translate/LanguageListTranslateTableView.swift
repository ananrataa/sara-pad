//
//  LanguageListTranslateTableView.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 19/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class LanguageListTranslateTableView: UITableViewController {
    
    weak var delegate:LanguageListTranslateDelegate?
    
    let cellReuseIdentifier = "languageListCell"
    var flag:Int? //flag 0=SourceLng, 1=DestinationLng
    var currentSourceLng:String?
    var currentDstLng:String?
    var myLngDict:NSMutableDictionary?
    var myLngKeyArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Choose Language"
        
        let navigationHeight = navigationController?.navigationBar.frame.size.height;
        let navigationImageSize = CGSize(width: navigationHeight!-20, height: navigationHeight!-20)
        
        let dummyView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        dummyView.backgroundColor = UIColor.clear
        let backButton = UIBarButtonItem(customView: dummyView)
        backButton.tintColor = UIColor.clear
        navigationItem.leftBarButtonItem = backButton
        
        let closeButton = UIBarButtonItem(image: UIImage.init(named: "close_black")?.resizeImage(navigationImageSize), style: .done, target: self, action: #selector(closeBarButton(_:)))
        closeButton.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = closeButton
        
        //print("Flag" + String.init(describing: flag))
        if let path = Bundle.main.path(forResource: "translateLanguageDictionary", ofType: "stringsdict"), let lngDict = NSDictionary(contentsOfFile: path){
            myLngDict = NSMutableDictionary(dictionary: lngDict)
            if flag == 1{
                myLngDict?.removeObject(forKey: currentSourceLng!)
            }
            myLngKeyArray = (myLngDict?.allKeys as! [String]).sorted(by: <)
            //print("Key : " + String.init(describing: myLngKeyArray.description))
            
            tableView.reloadData()
        }
       // print("Language Dic :" + String.init(describing: myLngDictArray?.description))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func closeBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myLngKeyArray.count
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        
        let keyLng =  myLngKeyArray[indexPath.row]
        let nameLng = myLngDict?.object(forKey: keyLng)
        
        let lngLabel = cell.viewWithTag(101) as! UILabel
        lngLabel.text = nameLng as? String
        
        let checkedLabel = cell.viewWithTag(102) as! UILabel
        checkedLabel.isHidden = true
        if (flag == 0 && keyLng == currentSourceLng) || (flag == 1 && keyLng == currentDstLng) {
            checkedLabel.isHidden = false
        }
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keyLng =  myLngKeyArray[indexPath.row]
        let nameLng = myLngDict?.object(forKey: keyLng)
         delegate?.didSelectedLanguage(keyLng, selectedLngName: nameLng as! String, flag: flag!)
        self.navigationController?.popViewController(animated: true)
        
        //tableView.deleteRows(at: [indexPath], with: .automatic)
      }

}

@available(iOS 11.0, *)
protocol LanguageListTranslateDelegate : class {
    func didSelectedLanguage(_ selectedLngKey:String, selectedLngName:String, flag:Int)
}

//
//  FileDataManager.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 14/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class FileDataManager {
    
    let paths:URL
    
    init() {
        paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func read(filePath: String) -> String {
        let readFileAtPath = paths.appendingPathComponent(filePath)
        //print("read file at path: \(readFileAtPath)")
        var str = "NO DATA"
        do {
            str = try String(contentsOf: readFileAtPath, encoding: .utf8)
        }
        catch {
            print("PDFReaderViewController : failed to read file")
        }
        
        return str
    }
    
    func write(filePath: String, value: String) {
        let writeFileAtPath:URL = paths.appendingPathComponent(filePath)
        //print("Write file to path :",writeFileAtPath)
        
        //check File directory
        let pdfDir = paths.appendingPathComponent("PDFAnotation")
        if !directoryExistsAtPath(pdfDir.path) {
            do {
                try FileManager.default.createDirectory(atPath: pdfDir.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
        
        if FileManager.default.fileExists(atPath: writeFileAtPath.absoluteString){
            do{
                try FileManager.default.removeItem(at: writeFileAtPath)
            }
            catch{
                print("PDFReaderViewController : failed to remove annotation file")
            }
        }
        
        do{
            try value.write(to: writeFileAtPath, atomically: true, encoding: .utf8)
        } catch {
            print("PDFReaderViewController : failed to write file")
            print(error.localizedDescription)
        }
    }
    
    
    func readAnnotationFromFile(readFileAtPath:String) -> ([NSDictionary],[Int],[NSDictionary],[NSDictionary]) {
        let jsonText =  self.read(filePath: readFileAtPath)
        //print("get Annotation : " + jsonText)
        var currentAnnotationArray:[NSDictionary] = [NSDictionary]()
        var currentBookMarkArray:[Int] = [Int]()
        var currentBookNoteArray:[NSDictionary] = [NSDictionary]()
        var currentInkArray:[NSDictionary] = []
        if jsonText != "NO DATA"{
            let data = jsonText.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            
            do{
                let jsonObj = try JSON(data: data)
                
                if jsonObj["mEditorModelList_IOS"].arrayObject != nil{
                    currentAnnotationArray =  jsonObj["mEditorModelList_IOS"].arrayObject as! [NSDictionary]
                }
                
                if jsonObj["mBookMarkAtPage"].arrayObject != nil{
                    //get book mark
                    currentBookMarkArray = jsonObj["mBookMarkAtPage"].arrayObject as! [Int]
                }
                
                if jsonObj["mBookNoteAtPage"].arrayObject != nil{
                    //get book note
                    currentBookNoteArray = jsonObj["mBookNoteAtPage"].arrayObject  as! [NSDictionary]
                }
                if let dictArray = jsonObj["mInkPathAtPage"].arrayObject as? [NSDictionary] {
                    currentInkArray = dictArray
                }
            }catch {
                print(error.localizedDescription)
            }
        }
        
        return (currentAnnotationArray,currentBookMarkArray,currentBookNoteArray,currentInkArray)
    }
    
    
    func saveAnnotationFile(Highlight jsonHighlight:[NSDictionary],
                            BookMark jsonBookmark:[Int],
                            Note jsonNote:[NSDictionary],
                            Ink jsonInk:[NSDictionary],
                            SaveFileToPath path:String) {
        let backgroundWork = DispatchQueue(label: "saveAnnotationToFile")
        backgroundWork.async{
            // create json array to save//
            let annatattionJsonStr = self.arrayDictionaryToJsonString(jsonHighlight)
            let bookmarkJsonStr = String.init(describing: jsonBookmark)
            let booknoteJsonStr = self.arrayDictionaryToJsonString(jsonNote)
            let inkJsonStr = self.arrayDictionaryToJsonString(jsonInk)
            let jsonObjectStr =
                "{\"mEditorModelList_IOS\":" + annatattionJsonStr + "," +
                    "\"mBookMarkAtPage\":" + bookmarkJsonStr + "," +
                    "\"mBookNoteAtPage\":"  + booknoteJsonStr + "," +
                    "\"mInkPathAtPage\":"  + inkJsonStr + "}"
            
            // print("Save : " + jsonObjectStr)
            
            //write to file
            FileDataManager.init().write(filePath: path, value: jsonObjectStr)
        }
    }
    
    func saveAnnotationFileAndUploadFile(Highlight jsonHighlight:[NSDictionary],
                                         BookMark jsonBookmark:[Int],
                                         Note jsonNote:[NSDictionary],
                                         Ink jsonInk:[NSDictionary],
                                         SaveFileToPath path:String,
                                         url:String,allowToSync:Bool,
                                         lastModifiedDate:Date) {
        
        let backgroundWork = DispatchQueue(label: "saveAnnotationToFile")
        backgroundWork.async{
            // create json array to save//
            let annatattionJsonStr = self.arrayDictionaryToJsonString(jsonHighlight)
            let bookmarkJsonStr = String.init(describing: jsonBookmark)
            let booknoteJsonStr = self.arrayDictionaryToJsonString(jsonNote)
            let inkJsonStr = self.arrayDictionaryToJsonString(jsonInk)
            
            let jsonObjectStr =
                "{\"mEditorModelList_IOS\":" + annatattionJsonStr + ","
                    + "\"mBookMarkAtPage\":" + bookmarkJsonStr + ","
                    + "\"mBookNoteAtPage\":" + booknoteJsonStr + ","
                    + "\"mInkPathAtPage\":" + inkJsonStr + "}"
            
            // print("Save : " + jsonObjectStr)
            
            //write to file
            FileDataManager.init().write(filePath: path, value: jsonObjectStr)
            
            //upload to server
            if let newModifiedDate = self.lastModified(path: path){
                var isDescending = false
                switch newModifiedDate.compare(lastModifiedDate) {
                case .orderedAscending:
                    isDescending = false
                case .orderedSame:
                    isDescending = false
                case .orderedDescending:
                    isDescending = true
                }
                
                //print("new date:\(newModifiedDate)")
                //print("pre date:\(lastModifiedDate)")
                
                if isDescending && allowToSync {
                    DispatchQueue.main.async {
                        print("upload to server")
                        ServiceAsynchronousRequest.makePostCall(notificationName: "UploadFile", strUrl: url,filepath: path)
                    }
                }
            }
        }
    }
    
    func encodeBookNoteToArrayDictionary(_ jsonObject:[String:String]) -> [NSDictionary]{
        var dictArray = [NSDictionary]()
        for (key,value) in jsonObject {
            dictArray.append(["pageNumber":key, "note":value as String])
        }
        return dictArray
    }
    
    
    func arrayDictionaryToJsonString(_ arrDict:[NSDictionary]) -> String{
        let jsonData: NSData
        var jsonString: String = ""
        do {
            jsonData = try JSONSerialization.data(withJSONObject: arrDict, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
        }catch{
            print ("JSON Failure")
        }
        
        return jsonString
    }
    
    //    private func  uploadAnnotation(url:String,filePath:String,allowToSync:Bool, lastModifiedDate:NSDate)  {
    //        //+ PDFAnotationFolderName + "/" + annotationKey+".txt"
    //        if let newModifiedDate = lastModified(path: filePath),lastModifiedDate != newModifiedDate{
    //            ServiceAsynchronousRequest.call(strUrl: url,notificationName: "UploadAnnotation")
    //        }
    //
    //
    //    }
    
    func lastModified(path: String) -> Date? {
        let fileUrl = self.paths.appendingPathComponent(path)
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: fileUrl.path)
            return attr[FileAttributeKey.modificationDate] as? Date
        } catch {
        
            return Date()
        }
        
    }
    
    func removeFileAtPath(PdfFile pdfUrlPath:URL, AnnotationFile annotationPath:String) {
        // let removePdfFileAtPath = self.paths.appendingPathComponent(pdfPath)
        let removeAnnotationFileAtPath = self.paths.appendingPathComponent(annotationPath)
        let lastOpenPageKey = removeAnnotationFileAtPath.deletingPathExtension().lastPathComponent
        
        UserDefaults.standard.removeObject(forKey: lastOpenPageKey)
        UserDefaults.standard.synchronize()
        
        do{
            try FileManager.default.removeItem(at: pdfUrlPath)
            try FileManager.default.removeItem(at: removeAnnotationFileAtPath)
        }
        catch{
            print("PDFReaderViewController : failed to remove annotation file")
        }
    }
    
    fileprivate func directoryExistsAtPath(_ path: String) -> Bool {
        var isDirectory = ObjCBool(true)
        let exists = FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
        return exists && isDirectory.boolValue
    }
}

//
//  PDFReaderViewController.swift
//  ios11PDFKitObjectiveC
//
//  Created by MBOX Multimedia Co., Ltd. on 18/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//        let annotation = PDFAnnotation.init(bounds: CGRect(x: 50, y: 50, width: 50, height: 50), forType: .freeText, withProperties: nil)
//        annotation.backgroundColor = .blue
//        page.addAnnotation(annotation)
//
//let bounds = CGRect(x: 20.0, y: 20.0, width: 200.0, height: 200.0)
//let annotation = PDFAnnotation(bounds: bounds, forType: .widget, withProperties: nil)
//annotation.widgetFieldType = .text
//annotation.backgroundColor = .yellow
//annotation.font = .systemFont(ofSize: 18)
//annotation.widgetStringValue = "Test!"
//
//page.addAnnotation(annotation)

import Foundation
import UIKit
import PDFKit
import MessageUI
import UIKit.UIGestureRecognizerSubclass
import Accounts
import SwiftyJSON
import MediaPlayer
import AVFoundation

protocol PDFReaderViewControllerDelegate {
    func didSaveDocument()
}

extension String {
    func detectLanguage() -> String? {
        let length = self.utf16.count
        let languageCode = CFStringTokenizerCopyBestStringLanguage(self as CFString, CFRange(location: 0, length: length)) as String?
        return languageCode
    }
}

extension UIViewController {
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

@available(iOS 11.0, *)
@objc class PDFReaderViewController :UIViewController, UIPopoverPresentationControllerDelegate, PDFViewDelegate, SearchViewControllerDelegate, ThumbnailGridViewControllerDelegate, OutlineViewControllerDelegate, BookmarkViewControllerDelegate, UITextViewDelegate, MusicBackgroundDelegate, MPMediaPickerControllerDelegate {

    @objc var roomId: String!
    @objc var pdfDocument: PDFDocument?
    @objc var annotationFilePath:String!
    
    @objc var strURLToUploadAnnatationFile:String?
    @objc var strURLToLoadMusicBackground:String?
    
    @objc var flagAllowToUploadAnnatationFile:String?
    @objc var flagRemoveFileWhenExist:String?
    @objc var flagIsPreviewFile:String?
    
    var isAllowToUploadAnnatationFile = false
    var isRemoveFileWhenExist = false
    
    
    var lastOpenPageKey: String!
    var saveDocumentdelegate: PDFReaderViewControllerDelegate?
    
   
    var currentAnnotationArray:[NSDictionary] = [NSDictionary]()
    var currentBookMarkArray:[Int] = [Int]()
    var currentBookNoteArray:[NSDictionary] = [NSDictionary]()
    
    var currentModifiedDate:Date?
    
    var arrayTranslateLngKey = [String]()
    
    var bookmarkNote:BookMarkNoteView?
    var textMarkRemind:UITextView!
    var textMarkLength:UILabel!
    
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var pdfThumbnailViewContainer: UIView!
    @IBOutlet weak var pdfThumbnailView: PDFThumbnailView!
    @IBOutlet private weak var pdfThumbnailViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelContainer: UIView!
    @IBOutlet weak var pageNumberLabel: UILabel!
    @IBOutlet weak var pageNumberLabelContainer: UIView!
    
    let tableOfContentsToggleSegmentedControl = UISegmentedControl(items: [#imageLiteral(resourceName: "Grid"), #imageLiteral(resourceName: "List"), #imageLiteral(resourceName: "Bookmark-N")])
    @IBOutlet weak var thumbnailGridViewConainer: UIView!
    @IBOutlet weak var outlineViewConainer: UIView!
    @IBOutlet weak var bookmarkViewConainer: UIView!
    
    var bookmarkButton: UIBarButtonItem!
    var booknoteButton: UIBarButtonItem!
    
    var searchNavigationController: UINavigationController?
    
    let barHideOnTapGestureRecognizer = UITapGestureRecognizer()
    let pdfViewGestureRecognizer = PDFViewGestureRecognizer()
    
    //bgmLocal Player
    let bgmLocalPlayer = MPMusicPlayerController.applicationQueuePlayer
    let btnMusicPlay = UIButton()
    let btnMusicStop = UIButton()
    var isBgmLocalPlaying = false
    //bgmCloud Player
    var bgmCloudPlayer:AVPlayer?
    var bgmCloudIndexPlaying:Int = 0
    var bgmCloudList:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barHideOnTapGestureRecognizer.addTarget(self, action: #selector(gestureRecognizedToggleVisibility(_:)))
        view.addGestureRecognizer(barHideOnTapGestureRecognizer)
        
        tableOfContentsToggleSegmentedControl.tintColor = .black
        tableOfContentsToggleSegmentedControl.selectedSegmentIndex = 0
        tableOfContentsToggleSegmentedControl.addTarget(self, action: #selector(toggleTableOfContentsView(_:)), for: .valueChanged)
        
        pdfView.autoScales = true
        pdfView.displayMode = .singlePage
        pdfView.displayDirection = .horizontal
        pdfView.usePageViewController(true, withViewOptions: [convertFromUIPageViewControllerOptionsKey(UIPageViewController.OptionsKey.interPageSpacing): 20])
        
        pdfView.addGestureRecognizer(pdfViewGestureRecognizer)
        
        pdfView.document = pdfDocument
        
        isAllowToUploadAnnatationFile = flagAllowToUploadAnnatationFile=="1" ? true:false
        isRemoveFileWhenExist = flagRemoveFileWhenExist=="1" ?  true:false
        
        //set default Color Picker
        if UserDefaults.standard.string(forKey: "ColorPicker") == nil {
            UserDefaults.standard.set(UIColor.yellow.hexCode, forKey: "ColorPicker")
            UserDefaults.standard.synchronize()
        }

        //GO TO LAST OPEN PAGE
        lastOpenPageKey = annotationFilePath
        //print("last Open Page Key :\(lastOpenPageKey)")
        var goToPage:Int = UserDefaults.standard.integer(forKey: lastOpenPageKey)
        goToPage = goToPage<=0 ? 0:goToPage
        //print("get currentPage : \(goToPage)")
        pdfView.go(to: (pdfDocument?.page(at: goToPage))!)
        
        currentModifiedDate = FileDataManager.init().lastModified(path: annotationFilePath)
        //print("currentModifiedDate : \(String(describing: currentModifiedDate))")
        
        pdfThumbnailView.layoutMode = .horizontal
        pdfThumbnailView.pdfView = pdfView
        
        titleLabel.text = pdfDocument?.documentAttributes?["Title"] as? String
        titleLabelContainer.layer.cornerRadius = 4
        pageNumberLabelContainer.layer.cornerRadius = 4
        
        //get Annotation Json
        //print("Annotation file path :",annotationFilePath)
        (currentAnnotationArray,currentBookMarkArray,currentBookNoteArray,currentInkArray) = FileDataManager.init().readAnnotationFromFile(readFileAtPath: annotationFilePath)
        
        let menu = UIMenuController.shared
        menu.setTargetRect(CGRect.zero, in: self.view)
        menu.setMenuVisible(true, animated: true)
        menu.arrowDirection = .down
        
        let highLight = UIMenuItem(title: "Highlight", action: #selector(PDFReaderViewController.highlightTextSelected))
        let underlineItem = UIMenuItem(title: "Underline", action: #selector(PDFReaderViewController.underline))
        let strikeOutItem = UIMenuItem(title: "Strike Out", action: #selector(PDFReaderViewController.strikeOut))
        
        let translateItem = UIMenuItem(title: "Translate", action: #selector(PDFReaderViewController.translate))
       
        let eraseItem = UIMenuItem(title: "Erase", action: #selector(PDFReaderViewController.unHighlightTextSelected))
        let clearItem = UIMenuItem(title: "Clear", action: #selector(PDFReaderViewController.clearHighlightOnCurrentPage))
        
        //add MenuItems to MenuController
        //don't forget add fucntion(canPerformAction)
        menu.menuItems = [highLight,underlineItem,strikeOutItem,translateItem,eraseItem,clearItem]
        
        resume()
        
       
        
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        //print("Action :",action.description)
        if action == #selector(PDFReaderViewController.clearHighlightOnCurrentPage) ||
            action == #selector(PDFReaderViewController.unHighlightTextSelected) ||
            action == #selector(PDFReaderViewController.highlightTextSelected) ||
            action == #selector(PDFReaderViewController.translate) ||
            action == #selector(PDFReaderViewController.underline) ||
            action == #selector(PDFReaderViewController.strikeOut){
            return true
        }
        else{
            return false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pdfViewPageChanged(_:)), name: .PDFViewPageChanged, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        adjustThumbnailViewHeight()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (context) in
            self.adjustThumbnailViewHeight()
        }, completion: nil)
    }
    
    private func adjustThumbnailViewHeight() {
        self.pdfThumbnailViewHeightConstraint.constant = 44 + self.view.safeAreaInsets.bottom
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ThumbnailGridViewController {
            viewController.pdfDocument = pdfDocument
            viewController.delegate = self
        } else if let viewController = segue.destination as? OutlineViewController {
            viewController.pdfDocument = pdfDocument
            viewController.delegate = self
        } else if let viewController = segue.destination as? BookmarkViewController {
            viewController.pdfDocument = pdfDocument
            viewController.annotationFilePath =  annotationFilePath
            viewController.delegate = self
        }
    }
    
    func addAnnotationToPdfPage(){
        Annotation.init(self.pdfDocument!).setAnnotation(currentAnnotationArray: currentAnnotationArray, pdfView: self.pdfView!)
        pageInkPathData = Annotation.init(self.pdfDocument!).setInkAnnotation(jsonDict: currentInkArray, pdfView: pdfView)
    }
    
    @objc func highlightTextSelected() {
        let selections = pdfView.currentSelection?.selectionsByLine()
        guard let page = pdfView.currentPage else {return}
        
        let jsonArray = Annotation.init(self.pdfDocument!).highlight(selections!, page: page)
        if jsonArray.count > 0 {
            currentAnnotationArray += jsonArray
        }
        //save to file
        saveAnnotationToFile()
    }
    
    @objc func unHighlightTextSelected() {
//        let selections = pdfView.currentSelection?.selectionsByLine()
//        //guard let page = selections?.first?.pages.first  else { return }
//        guard let page = pdfView.currentPage else {return}
//        self.currentAnnotationArray = Annotation.init(pdfDocument!).unHighlight(selections!, page: page, currentAnnotation: self.currentAnnotationArray)
//        //save to file
//        saveAnnotationToFile()
    }
    
    @objc func clearHighlightOnCurrentPage(){
//        guard let page = pdfView.currentPage else {return}
//        self.currentAnnotationArray = Annotation.init(pdfDocument!).clearAllHighlight(page, currentAnnotation: self.currentAnnotationArray)
//        //save to file
//       saveAnnotationToFile()
        
    }
    
    @objc func underline(){
        let selections = pdfView.currentSelection?.selectionsByLine()
        guard let page = pdfView.currentPage else {return}
        
        let jsonArray = Annotation.init(self.pdfDocument!).underline(selections!, page: page)
        if jsonArray.count > 0 {
            currentAnnotationArray += jsonArray
        }
        //save to file
        saveAnnotationToFile()
    }
    
    @objc func strikeOut(){
        let selections = pdfView.currentSelection?.selectionsByLine()
        guard let page = pdfView.currentPage else {return}
        
        let jsonArray = Annotation.init(self.pdfDocument!).strikeOut(selections!, page: page)
        if jsonArray.count > 0 {
            currentAnnotationArray += jsonArray
        }
        //save to file
        saveAnnotationToFile()
    }

    
    @objc func translate(){
        SVProgressHUD.show(withStatus: "Translate...")
        if InternetConnection.isConnectedToNetwork(){
            var strSelections:String = (pdfView.currentSelection?.attributedString)!.string
            strSelections = String(cString: strSelections.cString(using: String.Encoding.utf8)!, encoding: String.Encoding.utf8)!
            
            strSelections = strSelections.replacingOccurrences(of: "\"", with: " ")
            // print("detect language :",strSelections.detectLanguage())
            
            let strDetected = strSelections.detectLanguage() ?? "auto"
            
            var param = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=th&ie=UTF-8&oe=UTF-8&dt=t&q=" + strSelections
            arrayTranslateLngKey = ["auto","th"]
            
            if strDetected != "und" && strDetected == "th" {
                param = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=th&tl=en&ie=UTF-8&oe=UTF-8&dt=t&q=" + strSelections
                arrayTranslateLngKey = ["th","en"]
            }
            else if strDetected != "und" {
                param = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" + strDetected + "&tl=th&ie=UTF-8&oe=UTF-8&dt=t&q=" + strSelections
                arrayTranslateLngKey = [strDetected,"th"]
            }
            print("param :",param)
            
            NotificationCenter.default.addObserver(self, selector: #selector(googleTranslate(_:)), name: NSNotification.Name(rawValue: "GoogleTranslate"), object: nil)
            ServiceAsynchronousRequest.call(strUrl: param, notificationName:"GoogleTranslate", isAllowHeader:false)
            //print("translate : ",jsonResult.description)
        }
        else{
            SVProgressHUD.dismiss()
            presentAlert(withTitle: "Failed", message: "Unable to connect to the Internet")
            
        }
        
    }
    
    @objc func googleTranslate(_ notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GoogleTranslate"), object: nil)
        var translateResult = ""
        if notification.userInfo!["Data"] != nil {
            if let jsonArray = (notification.userInfo!["Data"] as! JSON).array,(jsonArray.count) > 0,
                let arrayTranslate = jsonArray[0].array, arrayTranslate.count > 0 {
                //print("translate array :",arrayTranslate.count)
                for arrayTranslate1 in arrayTranslate{
                    let strTranslate = arrayTranslate1.array![0].stringValue
                    print("translate :",strTranslate)
                    translateResult += strTranslate
                }
                
            }
            //print("\n\n translateResult :",translateResult)
            
            SVProgressHUD.dismiss()
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let googleTranslateVC:GoogleTranslateViewController = mainStoryboard.instantiateViewController(withIdentifier: "GoogleTranslateViewController") as! GoogleTranslateViewController
//            googleTranslateVC.originalStr = (pdfView.currentSelection?.attributedString)!.string
//            googleTranslateVC.resultStr = translateResult
//            
//            googleTranslateVC.keySourceLng = arrayTranslateLngKey[0]
//            googleTranslateVC.keyDstLng = arrayTranslateLngKey[1]
//            
//            let selfNavigation:UINavigationController = UINavigationController(rootViewController: googleTranslateVC)
//            self.present(selfNavigation, animated: true, completion: nil)
        }
        else{
            SVProgressHUD.dismiss()
            if notification.userInfo!["error"] != nil{
                presentAlert(withTitle: "Failed", message: notification.userInfo!["error"] as! String)
            }
        }
        
    }
    
    //Mark Ink Annotation
    func addInkAnnotation() {
        //pdfView.isUserInteractionEnabled = false
        //pdfView.isMultipleTouchEnabled = true
        
//        let rect = CGRect(x: 0.0, y: 0.0, width: 300.0, height: 70.0)
//        let annotation = PDFAnnotation(bounds: rect, forType: .ink, withProperties: nil)
//        annotation.backgroundColor = .blue
//
//        let pathRect = pdfView.bounds
//        let path = UIBezierPath(rect: pathRect)
//        annotation.add(path)
//
//        // Add annotation to the page
//         guard let page = pdfView.currentPage else {return}
//        pdfView.document?.page(at: (pdfDocument?.index(for: page))!)?.addAnnotation(annotation)
    }
    

    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func searchViewController(_ searchViewController: PdfSearchViewController, didSelectSearchResult selection: PDFSelection) {
        selection.color = .yellow
        pdfView.currentSelection = selection
        pdfView.go(to: selection)
        showBars()
    }
    
    func thumbnailGridViewController(_ thumbnailGridViewController: ThumbnailGridViewController, didSelectPage page: PDFPage) {
        resume()
        pdfView.go(to: page)
    }
    
    func outlineViewController(_ outlineViewController: OutlineViewController, didSelectOutlineAt destination: PDFDestination) {
        resume()
        pdfView.go(to: destination)
    }
    
    func bookmarkViewController(_ bookmarkViewController: BookmarkViewController, didSelectPage page: PDFPage) {
        resume()
        pdfView.go(to: page)
    }
    
    private func resume() {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow_back_black"), style: .plain, target: self, action: #selector(back(_:)))
        let tableOfContentsButton = UIBarButtonItem(image: #imageLiteral(resourceName: "List"), style: .plain, target: self, action: #selector(showTableOfContents(_:)))
        //        let actionButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showActionMenu(_:))) ///share & print
        navigationItem.leftBarButtonItems = [backButton, tableOfContentsButton]
        
       // let colorWheel = UIBarButtonItem(image: #imageLiteral(resourceName: "color-wheel"), style: .plain, target: self, action: #selector(colorWheel(_:)))
        
        booknoteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "notetext"), style: .plain, target: self, action: #selector(markRemindButtonTapped(_:)))
        booknoteButton.isEnabled = false
        
          bookmarkButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Bookmark-N"), style: .plain, target: self, action: #selector(addOrRemoveBookmark(_:)))
        
        let textToSpeech = UIBarButtonItem(image: #imageLiteral(resourceName: "voice-speaker-black"), style: .plain, target: self, action: #selector(textToSpeech(_:)))
        
        let musicPlayer = UIBarButtonItem(image: #imageLiteral(resourceName: "music-black"), style: .plain, target: self, action: #selector(playMusic(_:)))
        
        let brightnessButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Brightness"), style: .plain, target: self, action: #selector(showAppearanceMenu(_:)))
       
        let searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style: .plain, target: self, action: #selector(showSearchView(_:)))
    
        navigationItem.rightBarButtonItems = [searchButton,brightnessButton,musicPlayer,textToSpeech,bookmarkButton,booknoteButton]
        UIBarButtonItem.appearance().tintColor = UIColor.black
        
        pdfThumbnailViewContainer.alpha = 1
        
        pdfView.isHidden = false
        titleLabelContainer.alpha = 0//1
        pageNumberLabelContainer.alpha = 1
        thumbnailGridViewConainer.isHidden = true
        outlineViewConainer.isHidden = true
        
        barHideOnTapGestureRecognizer.isEnabled = true
        
        addAnnotationToPdfPage()
        updateBookmarkStatus()
        updatePageNumberLabel()
    }
    
    private func showTableOfContents() {
        view.exchangeSubview(at: 0, withSubviewAt: 1)
        view.exchangeSubview(at: 0, withSubviewAt: 2)
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow_back_black"), style: .plain, target: self, action: #selector(back(_:)))
        let tableOfContentsToggleBarButton = UIBarButtonItem(customView: tableOfContentsToggleSegmentedControl)
        let resumeBarButton = UIBarButtonItem(title: NSLocalizedString("Resume", comment: ""), style: .plain, target: self, action: #selector(resume(_:)))
        navigationItem.leftBarButtonItems = [backButton, tableOfContentsToggleBarButton]
        navigationItem.rightBarButtonItems = [resumeBarButton]
        
        pdfThumbnailViewContainer.alpha = 0
        
        toggleTableOfContentsView(tableOfContentsToggleSegmentedControl)
        
        barHideOnTapGestureRecognizer.isEnabled = false
    }
    
    @objc func resume(_ sender: UIBarButtonItem) {
        resume()
    }
    
    @objc func back(_ sender: UIBarButtonItem) {
        let currentPage:Int = (pdfDocument?.index(for: pdfView.currentPage!))!
        UserDefaults.standard.set(currentPage, forKey: lastOpenPageKey)
        UserDefaults.standard.synchronize()
        
        FileDataManager.init().saveAnnotationFileAndUploadFile(Highlight: currentAnnotationArray,
                                                               BookMark: currentBookMarkArray,
                                                               Note: currentBookNoteArray,
                                                               Ink: currentInkArray,
                                                               SaveFileToPath: annotationFilePath,
                                                               url: strURLToUploadAnnatationFile!,
                                                               allowToSync: isAllowToUploadAnnatationFile,
                                                               lastModifiedDate: currentModifiedDate!)
        
        if isRemoveFileWhenExist{
            FileDataManager.init().removeFileAtPath(PdfFile:(pdfDocument?.documentURL)!,AnnotationFile: annotationFilePath!)
        }
        
        self.dismiss(animated:true, completion: nil)
        //navigationController?.popViewController(animated: true)
    }
    
    @objc func showTableOfContents(_ sender: UIBarButtonItem) {
        showTableOfContents()
    }
    
    //    @objc func showActionMenu(_ sender: UIBarButtonItem) {
    //        if let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: ActionMenuViewController.self)) as? ActionMenuViewController {
    //            viewController.modalPresentationStyle = .popover
    //            viewController.preferredContentSize = CGSize(width: 300, height: 88)
    //            viewController.popoverPresentationController?.barButtonItem = sender
    //            viewController.popoverPresentationController?.permittedArrowDirections = .up
    //            viewController.popoverPresentationController?.delegate = self
    //            viewController.delegate = self
    //            present(viewController, animated: true, completion: nil)
    //        }
    //    }
    
    @objc func showAppearanceMenu(_ sender: UIBarButtonItem) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: AppearanceViewController.self)) as? AppearanceViewController {
            viewController.modalPresentationStyle = .popover
            viewController.preferredContentSize = CGSize(width: 300, height: 44)
            viewController.popoverPresentationController?.barButtonItem = sender
            viewController.popoverPresentationController?.permittedArrowDirections = .up
            viewController.popoverPresentationController?.delegate = self
            present(viewController, animated: true, completion: nil)
        }
    }
    
    @objc func showSearchView(_ sender: UIBarButtonItem) {
        if let searchNavigationController = self.searchNavigationController {
            present(searchNavigationController, animated: true, completion: nil)
        } else if let navigationController = storyboard?.instantiateViewController(withIdentifier: String(describing: PdfSearchViewController.self)) as? UINavigationController,
            let searchViewController = navigationController.topViewController as? PdfSearchViewController {
            searchViewController.pdfDocument = pdfDocument!
            searchViewController.delegate = self
            present(navigationController, animated: true, completion: nil)

            searchNavigationController = navigationController
        }
    }
    
    @objc func colorWheel(_ sender: UIBarButtonItem){
        
        /* colorPicker = ColorPickerView.init(frame: CGRect(x: 20, y: 80, width: Int(self.view.bounds.size.width-40), height: Int(self.view.bounds.size.height-(80+50))))
         colorPicker?.delegate = self
         
         //self.view.isUserInteractionEnabled = false
         UIApplication.shared.keyWindow?.addSubview(colorPicker!)
         hideBars()
         */
    }
    
    @objc func textToSpeech(_ sender: UIBarButtonItem){
    }
    
    @objc func playMusic(_ sender: UIBarButtonItem){
//        let musicBackgroundVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseMusicBackgroundViewController") as! ChooseMusicBackgroundViewController
//        musicBackgroundVC.delegate = self
//        musicBackgroundVC.strURLToLoadMusicBackground = strURLToLoadMusicBackground
        
//        let selfNavigation:UINavigationController = UINavigationController(rootViewController: musicBackgroundVC)
//        self.present(selfNavigation, animated: true, completion: nil)
        
        let bgm = BGMViewController(roomId: roomId ?? "")
        let nav = UINavigationController(rootViewController: bgm)
        present(nav, animated: true, completion: nil)
    }
    
    func didLocalSongChoosed(_ songlist: [MPMediaItem]) {
        //print("song count : \(songlist.count)")
        
        stopBgmCloudMusic()
        stopBgmLocalMusic()
    
        if songlist.count > 0 {
            //let bgmLocalPlayer = MPMusicPlayerController.applicationQueuePlayer
            // Add a notification observer for MPMusicPlayerControllerNowPlayingItemDidChangeNotification that fires a method when the track changes (to update track info label)
            bgmLocalPlayer.beginGeneratingPlaybackNotifications()
            
            //            NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ViewController.updateNowPlayingInfo), name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: nil)
            
            let coll = MPMediaItemCollection(items: songlist)
            bgmLocalPlayer.setQueue(with: coll)
            bgmLocalPlayer.nowPlayingItem = coll.items[0]
            bgmLocalPlayer.prepareToPlay()
            bgmLocalPlayer.play()
            bgmLocalPlayer.accessibilityValue = "play"
        
            isBgmLocalPlaying = true
        }
    }
    
    func didCloudSongChoosed(_ cloudList: [String]) {
        //print("cloud list: \(cloudList)")
        if cloudList.count > 0 {
            bgmCloudList = cloudList
            bgmCloudIndexPlaying = 0
            prepareToPlayBgmCloudMusic(cloudList[0])
        }
    }
    
    
    @objc func playpauseMusicTapping(_ sender:UIButton){
        if isBgmLocalPlaying {
            if bgmLocalPlayer.playbackState == MPMusicPlaybackState.playing {
                bgmLocalPlayer.pause()
                bgmLocalPlayer.accessibilityValue = "pause"
                btnMusicPlay.setImage(#imageLiteral(resourceName: "play-bgm"), for: .normal)
            }
            else  if bgmLocalPlayer.playbackState == MPMusicPlaybackState.paused {
                bgmLocalPlayer.play()
                bgmLocalPlayer.accessibilityValue = "play"
                btnMusicPlay.setImage(#imageLiteral(resourceName: "pause-bgm"), for: .normal)
            }
        }
        else {
             if bgmCloudPlayer != nil && (bgmCloudPlayer?.isPlaying)! {
                bgmCloudPlayer?.pause()
                bgmCloudPlayer?.rate = 0.0
                bgmCloudPlayer?.accessibilityValue = "pause"
                btnMusicPlay.setImage(#imageLiteral(resourceName: "play-bgm"), for: .normal)
            }
             else if  bgmCloudPlayer != nil && !(bgmCloudPlayer?.isPlaying)!{
                bgmCloudPlayer?.rate = 1.0
                bgmCloudPlayer?.play()
                bgmCloudPlayer?.accessibilityValue = "play"
                btnMusicPlay.setImage(#imageLiteral(resourceName: "pause-bgm"), for: .normal)
            }
        }
        
    }
    
    @objc func stopMusicTapping(_ sender:UIButton){
        if isBgmLocalPlaying {
            stopBgmLocalMusic()
        }
        else {
            stopBgmCloudMusic()

        }
        
         self.view.musicPlayerControlSlideLeftToDisappear()
    }
    
    
    private func prepareToPlayBgmCloudMusic(_ strUrl:String){
        stopBgmCloudMusic()
        stopBgmLocalMusic()

        guard let url = URL.init(string: strUrl)
            else {
                print("Bgm Cloud url error")
                return
        }
        //print("url : \(url)")
        let playerItem:AVPlayerItem = AVPlayerItem(url: url)
        bgmCloudPlayer = AVPlayer(playerItem: playerItem)
        bgmCloudPlayer?.rate = 1.0
        bgmCloudPlayer?.play()
        bgmCloudPlayer?.accessibilityValue = "play"
        
        NotificationCenter.default.addObserver(self, selector: #selector(PDFReaderViewController.bgmCloudDidPlayToEndTime), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        isBgmLocalPlaying = false
    }
    
    private func stopBgmCloudMusic() {
        if bgmCloudPlayer != nil && (bgmCloudPlayer?.isPlaying)! {
            bgmCloudPlayer?.pause()
            bgmCloudPlayer?.rate = 0.0
            bgmCloudPlayer?.accessibilityValue = "stop"
        }
    }
    
    private func stopBgmLocalMusic(){
        if bgmLocalPlayer.playbackState == MPMusicPlaybackState.playing ||
            bgmLocalPlayer.playbackState == MPMusicPlaybackState.paused {
            bgmLocalPlayer.stop()
            bgmLocalPlayer.accessibilityValue = "stop"
        }
    }
    
    @objc func bgmCloudDidPlayToEndTime(){
        //print("play did end, --> play next")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        if bgmCloudList.count > 0 && bgmCloudIndexPlaying < bgmCloudList.count {
            bgmCloudIndexPlaying += 1
            prepareToPlayBgmCloudMusic(bgmCloudList[bgmCloudIndexPlaying])
        }
    }

    func didSelectedColor(_ color: UIColor) {
        print("didSelectedColor: \(color.hexCode)")
        UserDefaults.standard.set(color.hexCode, forKey: "ColorPicker")
        UserDefaults.standard.synchronize()
        hideColorPickerView()
    }
    
    func dismissColorPicker() {
        hideColorPickerView()
    }
    
    func hideColorPickerView(){
//        colorPicker?.removeFromSuperview()
//        colorPicker  = nil
        showBars()
    }
    
    @objc func addOrRemoveBookmark(_ sender: UIBarButtonItem) {
        if let currentPage = pdfView.currentPage,
            let pageIndex = pdfDocument?.index(for: currentPage) {
            if let index = currentBookMarkArray.firstIndex(of: pageIndex) {
                currentBookMarkArray.remove(at: index)
                bookmarkButton.image = #imageLiteral(resourceName: "Bookmark-N")
                booknoteButton.isEnabled = false
                
                if let currentPage = pdfView.currentPage,
                    let pageIndex = pdfDocument?.index(for: currentPage) {
                    if let index = currentBookNoteArray.firstIndex(where: {$0.allKeys as! [String] == [String(pageIndex)]}){
                        print("------- found object  ------")
                        currentBookNoteArray.remove(at: index)
                        //print("af delete bokkmark at index =",index)
                        //print(String.init(describing: currentBookNoteArray))
                        
                    }
                }
                
                saveAnnotationToFile()
                
            } else {
                currentBookMarkArray = (currentBookMarkArray + [pageIndex]).sorted()
                bookmarkButton.image = #imageLiteral(resourceName: "Bookmark-P")
                booknoteButton.isEnabled = true
                
                addBookNoteView(OpenNoteForEdit: false)
            }
        }
    }
    
    @objc func cancelBookmarkReminder(sender:UIButton) {
        textMarkRemind.resignFirstResponder()
        
        saveAnnotationToFile()
        
        bookmarkNote?.removeFromSuperview()
        bookmarkNote = nil
        pdfView.isUserInteractionEnabled = true
        
    }
    
    @objc func saveBookmarkReminder(sender:UIButton) {
        textMarkRemind.resignFirstResponder()
        if textMarkRemind.text.count > 0 {
            SVProgressHUD.show()
            if let currentPage = pdfView.currentPage,
                let pageIndex = pdfDocument?.index(for: currentPage) {
                currentBookNoteArray.append([String(pageIndex):textMarkRemind.text ?? ""])
            }

            SVProgressHUD.dismiss()
        }
        
        saveAnnotationToFile()
        
        booknoteButton.isEnabled = true
        bookmarkNote?.removeFromSuperview()
        bookmarkNote = nil
        pdfView.isUserInteractionEnabled = true
        updateBookmarkStatus()
    }
    
    @objc func markRemindButtonTapped(_ sender: UIBarButtonItem){
        addBookNoteView(OpenNoteForEdit: true)
    }
    
    @objc func closeMarkReminderView(_:UIButton){
        bookmarkNote?.removeFromSuperview()
        bookmarkNote = nil
        pdfView.isUserInteractionEnabled = true
    }
    
    @objc func editMarkReminderText(sender:UIButton) {
        if sender.tag == 1 {
            bookmarkNote?.saveBtn?.tag = 2
            sender.setTitle("Edit", for: .normal)
            textMarkRemind.isEditable = false
            textMarkRemind.resignFirstResponder()
            if textMarkRemind.text.count > 0 {
                 self.view.activityStartAnimating(activityColor: .white, backgroundColor: UIColor.black.withAlphaComponent(0.8))
                
                if let currentPage = pdfView.currentPage,
                    let pageIndex = pdfDocument?.index(for: currentPage),
                    let index = currentBookNoteArray.firstIndex(where: {$0.allKeys as! [String] == [String(pageIndex)]}){
                    currentBookNoteArray.remove(at: index)
                    currentBookNoteArray.append([String(pageIndex):textMarkRemind.text ?? ""])
                }
                
                SVProgressHUD.dismiss()
            }
            
            saveAnnotationToFile()
            
            bookmarkNote?.removeFromSuperview()
            bookmarkNote = nil
            pdfView.isUserInteractionEnabled = true
        }
        else {
            bookmarkNote?.saveBtn?.tag = 1
            sender.setTitle("Save", for: .normal)
            textMarkRemind.isEditable = true
            textMarkRemind.becomeFirstResponder()
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count < 250{
            return true
        }
        else if text.count == 0 && range.length > 0{
            return true
        }
        
        return false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textMarkLength.text = String.init(describing: textView.text.count) + "/250"
    }
    
    @objc func toggleTableOfContentsView(_ sender: UISegmentedControl) {
        pdfView.isHidden = true
        titleLabelContainer.alpha = 0
        pageNumberLabelContainer.alpha = 0
        
        if tableOfContentsToggleSegmentedControl.selectedSegmentIndex == 0 {
            thumbnailGridViewConainer.isHidden = false
            outlineViewConainer.isHidden = true
            bookmarkViewConainer.isHidden = true
        } else if tableOfContentsToggleSegmentedControl.selectedSegmentIndex == 1 {
            thumbnailGridViewConainer.isHidden = true
            outlineViewConainer.isHidden = false
            bookmarkViewConainer.isHidden = true
        } else {
            thumbnailGridViewConainer.isHidden = true
            outlineViewConainer.isHidden = true
            bookmarkViewConainer.isHidden = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateBookmark"), object: nil, userInfo: nil)
        }
    }
    
    @objc func pdfViewPageChanged(_ notification: Notification) {
        if pdfViewGestureRecognizer.isTracking {
            hideBars()
        }
        addAnnotationToPdfPage()
        updateBookmarkStatus()
        updatePageNumberLabel()
    }
    
    @objc func gestureRecognizedToggleVisibility(_ gestureRecognizer: UITapGestureRecognizer) {
        if let navigationController = navigationController {
            if navigationController.navigationBar.alpha > 0 {
                hideBars()
            } else {
                showBars()
            }
        }
    }
    
    private func saveAnnotationToFile(){
        FileDataManager.init().saveAnnotationFile(Highlight: currentAnnotationArray, BookMark: currentBookMarkArray, Note: currentBookNoteArray, Ink: currentInkArray, SaveFileToPath: annotationFilePath)
    }
    
    private func updateBookmarkStatus() {
        if let currentPage = pdfView.currentPage,
            let index:Int = pdfDocument?.index(for: currentPage) {
            bookmarkButton.image = currentBookMarkArray.contains(index) ? #imageLiteral(resourceName: "Bookmark-P") : #imageLiteral(resourceName: "Bookmark-N")
            booknoteButton.isEnabled = false
            if let _ = currentBookNoteArray.firstIndex(where: {$0.allKeys as! [String] == [String(index)]}){
                booknoteButton.isEnabled = true
            }
        }
    }
    
    private func updatePageNumberLabel() {
        if let currentPage = pdfView.currentPage, let index = pdfDocument?.index(for: currentPage), let pageCount = pdfDocument?.pageCount {
            pageNumberLabel.text = String(format: "%d/%d", index + 1, pageCount)
        } else {
            pageNumberLabel.text = nil
        }
    }
    
    private func showBars() {
        //removeMusicPlayerControl
        self.view.removeMusicPlayerControl()
        
        if let navigationController = navigationController {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                navigationController.navigationBar.alpha = 1
                self.pdfThumbnailViewContainer.alpha = 1
                self.titleLabelContainer.alpha = 0//1
                self.pageNumberLabelContainer.alpha = 1
                self.inkButton.alpha = 0
            }
        }
    }
    
    private func hideBars() {
        if let navigationController = navigationController {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                navigationController.navigationBar.alpha = 0
                self.pdfThumbnailViewContainer.alpha = 0
                self.titleLabelContainer.alpha = 0
                self.pageNumberLabelContainer.alpha = 0
                self.inkButton.alpha = 1
            }
        }
        
        //presentMusicBackgourndPlayerControl
        if (bgmLocalPlayer.accessibilityValue == "play" ||
            bgmLocalPlayer.accessibilityValue == "pause") ||
            (bgmCloudPlayer != nil && (bgmCloudPlayer?.accessibilityValue == "pause" || bgmCloudPlayer?.accessibilityValue == "play")){

                self.view.presentMusicBackgourndPlayerControl(btnMusicPlay, btnStop: btnMusicStop)
                btnMusicPlay.addTarget(self, action: #selector(playpauseMusicTapping(_:)), for: .touchUpInside)
                btnMusicStop.addTarget(self, action: #selector(stopMusicTapping(_:)), for: .touchUpInside)
        }
    }
    
    private func addBookNoteView(OpenNoteForEdit isEditMode:Bool){
        if bookmarkNote==nil{
            let bookmarkWidth = self.view.frame.size.width-20
            let bookmarkFrame = CGRect(x: 10, y: 44+10, width: bookmarkWidth, height: (bookmarkWidth*3)/4)
            bookmarkNote = BookMarkNoteView(frame: bookmarkFrame)
            
            bookmarkNote?.cancelBtn?.addTarget(self, action: #selector(cancelBookmarkReminder(sender:)), for: .touchUpInside)
           
            bookmarkNote?.bookmarkPage?.text = "Page " + String.init(describing: (pdfDocument?.index(for: pdfView.currentPage!))! + 1)
            
            textMarkRemind = bookmarkNote?.bookmarkTextView
            textMarkRemind.delegate = self
            textMarkRemind.isEditable = true
            textMarkLength = bookmarkNote?.textLength
            
            if isEditMode{
                bookmarkNote?.saveBtn?.setTitle("Edit", for: .normal)
                bookmarkNote?.saveBtn?.tag = 2
                bookmarkNote?.saveBtn?.addTarget(self, action: #selector(editMarkReminderText(sender:)), for: .touchUpInside)
                
                if let currentPage = pdfView.currentPage,
                    let pageIndex = pdfDocument?.index(for: currentPage) {
                     if let index = currentBookNoteArray.firstIndex(where: {$0.allKeys as! [String] == [String(pageIndex)]}){
                        textMarkRemind.text = currentBookNoteArray[index].value(forKey: String(pageIndex)) as? String
                    }
                }
                textMarkRemind.isEditable = false
                textMarkLength = bookmarkNote?.textLength
            }
            else{
                 bookmarkNote?.saveBtn?.addTarget(self, action: #selector(saveBookmarkReminder(sender:)), for: .touchUpInside)
                textMarkRemind.becomeFirstResponder()
            }
            
            UIApplication.shared.keyWindow?.addSubview(bookmarkNote!)
            pdfView.isUserInteractionEnabled = false
        }
    }
    //MARK:- Ink Annotation
    var currentInkArray:[NSDictionary] = []
    var pageInkPathData:[(page: Int, tag: Int, color: String, width: CGFloat, points:[CGPoint])] = []
    var redoPathData: [(page: Int, color: String, width: CGFloat, points:[CGPoint])] = []
    var pathPoints:[CGPoint] = []
    var pageInkAnnotations:[[PDFAnnotation]] = [] {
        didSet {
            undoButton.isEnabled = pageInkAnnotations.count > 0
        }
    }
    var redoPaths:[[PDFAnnotation]] = [] {
        didSet {
            redoButton.isEnabled = redoPaths.count > 0
        }
    }
    var drawPath: UIBezierPath?
    var lastPoint: CGPoint = .zero
    var curPDFPage: PDFPage!
    var pathInkAnnotations:[PDFAnnotation] = [] //keep annotation for each line
    var penButtons:[UIButton] = []
    var colorButtons:[UIButton] = []
    var undoButton: UIButton!
    var redoButton: UIButton!
    var brushSize:[CGFloat] = [0.1, 2.5, 5, 10]
    let inkColor: [UIColor] = [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.231372549, green: 0.4862745098, blue: 0.9490196078, alpha: 1), #colorLiteral(red: 0.4745098039, green: 0.8235294118, blue: 0.4549019608, alpha: 1), #colorLiteral(red: 0.9568627451, green: 0.8078431373, blue: 0.3294117647, alpha: 1), #colorLiteral(red: 0.8666666667, green: 0.2588235294, blue: 0.2745098039, alpha: 1)]//"#FFFFFF","#000000","#3B7CF2","#79D274","#F4CE54","#DD4246"
    let dfPenColor = "df_color"
    let dfPenWidth = "df_width"
    lazy var brushColor:UIColor = {
        if let i = UserDefaults.standard.value(forKey: dfPenColor) as? Int, i >= 0 {
            return inkColor[min(5, i)]
        } else {
            //set initial color = black
            UserDefaults.standard.set(1, forKey: dfPenColor)
            UserDefaults.standard.synchronize()
        }
        return inkColor[1]
    }()
    lazy var brushWidth:CGFloat = {
        if let i = UserDefaults.standard.value(forKey: dfPenWidth) as? Int, i >= 0 {
            return brushSize[i]
        }
        return brushSize[0]
    }()
    let inkMenuView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(hexString: "#E8E8E8")
        v.tintColor = .gray
        v.isHidden = true
        return v
    }()
    let inkButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "pen"), for: .normal)
        btn.layer.cornerRadius = 18
        btn.layer.borderWidth = 1
        btn.alpha = 0
        return btn
    }()
}

class PDFViewGestureRecognizer: UIGestureRecognizer {
    var isTracking = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        isTracking = true
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        isTracking = false
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        isTracking = false
    }
}

@available(iOS 11.0, *)
extension PDFReaderViewController : PDFReaderViewControllerDelegate{
    func didSaveDocument() {
        self.dismiss(animated: true, completion: nil)
    }
}






// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIPageViewControllerOptionsKey(_ input: UIPageViewController.OptionsKey) -> String {
	return input.rawValue
}

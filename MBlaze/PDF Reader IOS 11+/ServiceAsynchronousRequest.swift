//
//  ServiceAsynchronousRequest.swift
//  SaraPadReader
//
//  Created by MBOX Multimedia Co., Ltd. on 15/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON


public class ServiceAsynchronousRequest{
    
    class func call(strUrl : String, notificationName:String, isAllowHeader:Bool) {
        // Set up the URL request
        let strUrlEncode = strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: strUrlEncode!) else {
            print("Error: cannot create URL")
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: nil, userInfo: ["error":"url error"])
            }
            return
        }
        
        let request : NSMutableURLRequest = NSMutableURLRequest()
        // [request addValue:API_KEY forHTTPHeaderField:@"API_KEY"];
        if isAllowHeader {
            request.addValue("IE11bHRpbWVkaWEgIA0KIA0KICANCiBEaWdpdGFsIENvbnRlbnRzIA0K", forHTTPHeaderField: "API_KEY")
        }
        request.url = url
        request.httpMethod = "GET"
        request.timeoutInterval = 30
        request.cachePolicy = .useProtocolCachePolicy
        
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            // check for any errors
            guard error == nil else {
                print(error!)
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: nil, userInfo: ["error":error!])
                }
                return
            }
            // make sure we got data
            guard data != nil else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: nil, userInfo: ["error":"Did not receive data"])
                }
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                let result = try JSON(data: data! as Data)
                //print("json : \(result)")
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: nil, userInfo: ["Data":result])
                }
            } catch  {
                print("error trying to convert data to JSON")
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: nil, userInfo: ["error":"Convert data to JSON"])
                }
                return
            }
        })
        
        task.resume()
    }
    
    class func makePostCall(notificationName:String, strUrl:String, filepath:String) {
        
        //print("url:",strUrl)
        //print("file path :",filepath)
        
        let strUrlEncode = strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: strUrlEncode!) else {
            print("Error: cannot create URL")
            return
        }
        
        let boundary = "unique-consistent-string"
        // Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=" + boundary
        
        do{
            let urlPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filepath)
            let fileName = urlPath.lastPathComponent
            let strData = try String(contentsOf: urlPath, encoding: String.Encoding.utf8)
            
            // post body
            let bodyData = NSMutableData()
            bodyData.append(("--" + boundary + "\r\n").data(using: String.Encoding.utf8)!)
            bodyData.append(("Content-Disposition: form-data; name=\"File\"; filename=\"" + fileName + "\"\r\n").data(using: String.Encoding.utf8)!)
            bodyData.append(("Content-Type: text/plain\r\n\r\n").data(using: String.Encoding.utf8)!)
            bodyData.append(strData.data(using: String.Encoding.utf8)!)
            bodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
            bodyData.append(("--" + boundary + "\r\n").data(using: String.Encoding.utf8)!)
            
            // the content-length
            let postLength = String(bodyData.length)
            
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.url = url
            request.httpMethod = "POST"
            request.timeoutInterval = 30
            request.cachePolicy = .useProtocolCachePolicy
            request.addValue("IE11bHRpbWVkaWEgIA0KIA0KICANCiBEaWdpdGFsIENvbnRlbnRzIA0K", forHTTPHeaderField: "API_KEY")
            request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
            request.addValue(contentType, forHTTPHeaderField: "content-type")
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
            request.httpBody = bodyData as Data
            
            // set up the session
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                guard error == nil else {
                    print(error!)
                    return
                }
                
                if let htppResponse = response as? HTTPURLResponse{
                    print("htpp status: \(htppResponse.statusCode)")
                }
                
                guard let data = data else {
                    print("Error: did not receive data")
                    return
                }
                do{
                    print("upload result :",try JSON(data: data))
                    
                }catch{}
                
                // parse the result as JSON, since that's what the API provides
                
            })
            
            task.resume()
        }
        catch{
           return
        }
    }
     
     /*func makeDeleteCall() {
     let firstTodoEndpoint: String = "https://jsonplaceholder.typicode.com/todos/1"
     var firstTodoUrlRequest = URLRequest(url: URL(string: firstTodoEndpoint)!)
     firstTodoUrlRequest.httpMethod = "DELETE"
     
     let session = URLSession.shared
     
     let task = session.dataTask(with: firstTodoUrlRequest) {
     (data, response, error) in
     guard let _ = data else {
     print("error calling DELETE on /todos/1")
     return
     }
     print("DELETE ok")
     }
     task.resume()
     } */
    
}

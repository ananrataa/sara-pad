//
//  CustomActivityIndicator.m
//
//  Created by Nong.Preeyawal
//  Copyright (c) 2015 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "CustomActivityIndicator.h"
#import "HexColor.h"

@implementation CustomActivityIndicator

#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

+ (void) presentActivityIndicatorView:(NSString *) processName {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.userInteractionEnabled = NO;
    
    // Instantiate the nib content without any reference to it.
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomActivityIndicator" owner:nil options:nil];
    
    // Find the view among nib contents (not too hard assuming there is only one view in it).
    UIView *acView = [nibContents objectAtIndex:0];
    
    if (IPAD)
        acView = [nibContents objectAtIndex:1];
    
    acView.layer.cornerRadius = 5;
    acView.layer.shadowOpacity = 0.8;
    acView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    UILabel *mLabel = acView.subviews.lastObject;
    mLabel.text = processName;
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    acView.center = CGPointMake(screenSize.width/2, screenSize.height/2);
    
    if (acView != nil) {
        [window addSubview:acView];
    }
}


+ (void) presentActivityIndicatorView {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.userInteractionEnabled = NO;
    
    // Instantiate the nib content without any reference to it.
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomActivityIndicator" owner:nil options:nil];
    
    // Find the view among nib contents (not too hard assuming there is only one view in it).
    UIView *acView = [nibContents objectAtIndex:2];
    
    acView.layer.cornerRadius = 5;
    acView.layer.shadowOpacity = 0.8;
    acView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    acView.center = CGPointMake(screenSize.width/2, screenSize.height/2);
    
    if (acView != nil) {
        [window addSubview:acView];
    }
}


+(void)presentToastView:(NSString*)toastTitle isSuccess:(BOOL)success{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.userInteractionEnabled = NO;
    
    // Instantiate the nib content without any reference to it.
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomActivityIndicator" owner:nil options:nil];
    
    // Find the view among nib contents (not too hard assuming there is only one view in it).
    UIView *acView = [nibContents objectAtIndex:3];
    
    UIView *mView = acView.subviews.firstObject;
    
    UIImageView *mImageView = mView.subviews.firstObject;
    UILabel *mLabel = mView.subviews.lastObject;
    mLabel.text = toastTitle;
    
    if (!success) {
        mImageView.image = [UIImage imageNamed:@"alert-error"];
        mLabel.textColor = [HexColor SKColorHexString:@"D80027"];
        
    }
    else{
        mImageView.image = [UIImage imageNamed:@"alert-success"];
        mLabel.textColor = [HexColor SKColorHexString:@"FFD700"];
    }
    
    acView.layer.cornerRadius = 5;
    acView.layer.shadowOpacity = 0.8;
    acView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    acView.center = CGPointMake(screenSize.width/2, screenSize.height/2);
    
    if (acView != nil) {
        [window addSubview:acView];
    }
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2.f * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [self dismissActivityIndicatorView];
    });
}

+ (void) dismissActivityIndicatorView {
    for (UIView *subview in [[[UIApplication sharedApplication] keyWindow] subviews]) {
        if (subview.tag == 9999) {
            [subview removeFromSuperview];
            
            [[UIApplication sharedApplication] keyWindow].userInteractionEnabled = YES;
            break;
        }
    }
}

@end

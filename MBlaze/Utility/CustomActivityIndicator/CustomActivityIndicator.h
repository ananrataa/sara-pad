//
//  CustomActivityIndicator.h
//
//  Created by Nong.Preeyawal
//  Copyright (c) 2015 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomActivityIndicator : UIView

+ (void) presentActivityIndicatorView:(NSString *) processName;

+ (void) presentActivityIndicatorView;

+ (void) dismissActivityIndicatorView;

+(void)presentToastView:(NSString*)toastTitle isSuccess:(BOOL)success;

@end

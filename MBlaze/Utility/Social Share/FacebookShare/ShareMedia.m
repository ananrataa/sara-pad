//
//  FaceBookShare.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/4/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//content.contentURL = [NSURL URLWithString:@"http://dev.sara-pad.com:88/DigiLibDashboard/FacebookAppLinkTest/mediaID=eb543d61-080c-4074-a0b1-02878d3bca6a&roomID=3b125216-c64f-4593-a1bc-9a5e558adab8.html"]; //ล้านนา
//content.contentURL = [NSURL URLWithString:@"http://dev.sara-pad.com:88/DigiLibDashboard/FacebookAppLinkTest/mediaID=7c68cedb-5a94-453c-9c81-023bb3a584b3&roomID=3b125216-c64f-4593-a1bc-9a5e558adab8.html"]; //เที่ยว 10 เส้นทางวัฒนธรรมท่องประวัติศาสตร์
//

#import "ShareMedia.h"

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import "CustomActivityIndicator.h"
#import "SVProgressHUD.h"

@implementation ShareMedia

- (void)shareMediaToFacebook:(id)_self URL:(NSString*) url{
    //NSLog(@"Url : %@",url);
    //    if ([FBSDKAccessToken currentAccessToken]){
    //        //token available : EAAWG899ElswBACZB5SAgJyQhYazZAZBpXkifcZB8bwv38Gg8tMdTwSPZCpdzcZCF6BvcP5QsrtAEvRczYZCrfhAHxxbE5unsZCZARR8aREACvMS5Oenj2dxe3XlNLhkVtu8evJIGcgYI0XYk6I7xaxBapMRURoSoUKblK6kbvQoKJxZCtF4eakj2gXZCRgmbTDXF09clD7VYK4wFZBf2RwZBMLejz3ey0OfZAZA0KJFMENyjZCyWNQZDZD
    //        NSLog(@"token available : %@",[[FBSDKAccessToken currentAccessToken] tokenString]);
    //    }

    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL =  [NSURL URLWithString:url];
    //Hashtags
    //You can specify a single hashtag to appear with a shared photo, link, or video. This hashtag also appears in the Share dialog, and people have the the opportunity to remove it before publishing.
    content.hashtag = [FBSDKHashtag hashtagWithString:@"#SaraPad"];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = _self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeShareSheet;
    if (![dialog canShow]) {
       dialog.mode = FBSDKShareDialogModeWeb;
        //NSLog(@"shareDialog can't Show");
    }
    [dialog show];
    
   // [CustomActivityIndicator dismissActivityIndicatorView];
    
    NSError *error;
    if (![dialog validateWithError:&error]) {
        // check the error for explanation of invalid content
        NSLog(@"shareDialog error : %@",[error description]);
    }
    
    if ([dialog show]) {
        NSLog(@"shareDialog is Showing ");
    } else {
        [SVProgressHUD showErrorWithStatus:@"ไม่สามารถแชร์\nไม่พบ Facebook App บนเครื่อง"];
    }
}

- (void)shareMediaToLine:(NSString*) url{
    //share to Line
    NSString *strUrl = [NSString stringWithFormat:@"line://msg/text/?%@",url];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl] options:@{} completionHandler:nil];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl] options:@{} completionHandler:^(BOOL success) {
        if (!success) {
            [SVProgressHUD showErrorWithStatus:@"ไม่สามารถแชร์\nไม่พบ Line App บนเครื่อง"];
        }
    }];
}

- (void)shareMediaToTwitter:(NSString*) url{
    //Share to Twitter
    NSString *strUrl = [NSString stringWithFormat:@"http://twitter.com/share?url=%@&hashtags=%@",url,@"SaraPad"];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSLog(@"%@",strUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl] options:@{} completionHandler:^(BOOL success) {
        if (!success) {
            [SVProgressHUD showErrorWithStatus:@"ไม่สามารถแชร์\nไม่พบ Twitter App บนเครื่อง"];
        }
    }];

}

@end

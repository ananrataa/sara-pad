//
//  FaceBookShare.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/4/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareMedia : NSObject

- (void)shareMediaToFacebook:(id)_self URL:(NSString*) url;

- (void)shareMediaToLine:(NSString*) url;

- (void)shareMediaToTwitter:(NSString*) url;
@end

//
//  AsyncTask.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 8/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AsyncTask.h"

@implementation AsyncTask

{
    void(^callback)(id someValue);
}

- (id)initWithCallback:(void(^)(id someValue))handler
{
    self = [super init];
    if (self)
    {
        callback = handler;
    }
    return self;
}

- (void)runAsyncProcess
{
    // Run async process
    callback(@"Value being passed to block");
    callback = nil;
}


@end

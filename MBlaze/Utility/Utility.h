//
//  Utility.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "SPNotification.h"
#import "MemberInfo.h"

@interface Utility : NSObject

+ (NSString *) documentPath;

+(NSString*)deviceName;

+(float)deviceDisplaySize;

+(BOOL)isImage:(NSData *)data;

+ (void) removeFileInDirectory:(NSString *)filePath;

+ (void) removeAllFilesInDirectory: (NSString *) dirName;

+(void)readFileInDir:(NSString*)dir;

+ (void) createNewDirectory:(NSString *) dirName isSupportDirectory:(BOOL) isSupportDir;

+(void) saveMediaFile:(NSString *) filePath FileData:(NSData*)fileData;

//+ (UIImage *) changeImageScaleWithImage:(UIImage *)image scaledToFillSize:(CGSize)size;

+(UIImage *)rescaleImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+(NSString *)covertNsNullToNSString:(NSString*)str;

+(void)loadMediaCoverImageFromURL:(NSString*)url OrHaveNSData:(NSData *)data withFileName:(NSString *) imageName inDirectory:(NSString *) directoryPath;

+ (UIImage *) loadImageWithFilePath:(NSString *) fileFullPath defaultImage:(UIImage *) defaultImage;

+(void) saveMediaCoverImageFromData:(NSData *) fileData withFileName:(NSString *) imageName inDirectory:(NSString *) directoryPath;

+(void)setMediaRating:(NSString*)rate ImageView1:(UIImageView*)imgV1 ImageView2:(UIImageView*)imgV2 ImageView3:(UIImageView*)imgV3 ImageView4:(UIImageView*)imgV4 ImageView5:(UIImageView*)imgV5;

+(void)setMediaTypeIcon:(UIImageView*)imageV MediaType:(NSString*)type;

//+(void)showAlertViewController:(NSString*)title Message:(NSString*)message;

+(void)showAlertViewController:(NSString*)title Message:(NSString*)message Controller:(UIViewController*)vc;

+ (int)calculateLabelHeight:(NSString*)str UILabelDefaultWidth:(float)defaultWidth UILabelDefaultHeight:(float)defaultHeight Font:(float)fontSize;

+ (int)lineCountForLabel:(UILabel *)label;

+(NSString*)encodeURL:(NSString*)url;

+(NSString*)currentDate;

+(NSString*)currentDateTimeWithFormatter:(NSString*)strFormatter;

+(NSString*)getDateTimeForNotification:(NSString*)strDate InputFormat:(NSString*)inputFormat OutputFormat:(NSString*)outputFormat;

+(NSString*)covertDateToFormat:(NSString*)strDate InputFormat:(NSString*)inputFormat OutputFormat:(NSString*)outputFormat;

+(void)showAlertControllerForSignout:(UIViewController*)self_;

+(void)prepare4SignOut:(UIViewController*)self_;

+(NSArray*)checkUserLimitDayAndMedia;

+(void)checkToRemoveExpiredMediaOnShelf;

+(void)removeExpiredMediaOnShelf:(NSString *)mediaId CoverPath:(NSString*)coverPath FilePath:(NSString*)filePath MemberID:(NSString *)memberId RoomID:(NSString *)roomID;

+(BOOL)isValidePDF:(NSData *)pdfData;

+(BOOL)isServiceResponseError:(NSDictionary *)jsonString;

+(BOOL)isJsonDictionaryNotNull:(NSDictionary *)jsonDic;

+(float)bounceHeightForLoadMore;

//+(NSString *)getDeviceMedelName;

+(BOOL)connectedToNetwork;

+(NSString *)generatePassword:(NSString*)fileName;

+(NSString *)NSLocalizedString:(NSString *)stringName;

+(void)setFont:(UIFont *)font WithFontSize:(float)size;

+(void)addShadow:(UIView*)view WithShadowRadius:(float)shadowRadius;

+(NSString *)getLocalizeFilterName:(NSString *)filter_id;

+(NSDictionary *)getDeviceLanguage;

+(void)setBookExpireNotification:(SPNotification *)notification;

+(void)updateBookExpireNotification:(NSArray *)identifiers;

+(int)dateToSecond:(NSString *)startDate EndDate:(NSString*)endDate DateFormat:(NSString*)inputFormat;

+(void)writeLogFile:(NSString *)logText;

+(NSString*)readLogFile;

+(int)calaulateCollectionItemInRow;

+(float)fontSize:(float)originalSize;

+(void)writePdfAnotationJson:(NSString *)fileName JsonString:(NSString*)jsonStr;

+(NSString *)readPdfAnotationJson:(NSString *)fileName;

+(void) deletePdfAnotationJson:(NSString *)fileName;

+(void)getListFiles:(NSString*)dirName;

+(NSString *)encodeMediaIDPreset:(NSString *)mediaID;

+(NSString *)decodeMediaIDPreset:(NSString *)mediaIdPreset;

+(CGSize)getWidthOfNSString:(NSString *)str FontSize:(UIFont*)font;

+(CGSize)calculateNavigationbarImageSize:(float)navigationbarHeight isLeft:(BOOL)isLeft;

+(void)viewAnimationSlideUpAndDown:(UIView*)targetView YPoint:(float)yPoint;

+(void)setUnderlineToUILabel:(UILabel*)label Text:(NSString*)str;

+(BOOL) validateEmailFormat:(NSString *)checkString;

+(void)insertMemberInfo:(MemberInfo *)member VC:(UIViewController*)vc;

extern BOOL const isFullScreen;




@end

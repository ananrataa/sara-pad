//
//  ConstantValues.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ConstantValues.h"

@implementation ConstantValues

float default_navigation_height = 44.f;
float default_tabbar_height = 49.f;

NSString *KeyUID = @"UID";

NSString *KeyCuurrentUsedLribaryID = @"CuurrentUsedLribary";

NSString *KeyMBZSolutionId = @"MBZSolutionId";

NSString *KeyDeviceCode = @"DeviceCode";

NSString *mainFont = @"Sukhumvit Set"; // @"HelveticaNeue";

NSString *sukhumvitFont =@"Sukhumvit Set";
NSString *sukhumvitLightFont =@"SukhumvitSet-Light";
NSString *sukhumvitMediumFont =@"SukhumvitSet-Medium";

NSString *mainFontHeader = @"SukhumvitSet-Light"; //@"HelveticaNeue-Medium";

NSString *mediatypeId = @"6A18570F-8A45-4C56-89A1-D6ECC21C69AB";

NSString *sarapadFloderName = @"Sarapad_Media_Offline";

NSString *temporyFloderName = @"TemporyFile";

NSString *DecryptKey = @"Mbvcxzasdfgtre4567890IUYTREWQJhkIUftrdcvbMkl76512qA43EDRfuUjhygB";

int MaximunFilesDownload = 10;

int MaximunDateForBorrow = 10;

NSString *UnknownError = @"UnknownError";

NSString *KeyMediaTypeName = @"Public";

NSString *PublicShelfIdMediaOffline = @"####";

NSString *NotificationTokenID = @"NotificationTokenID";

NSString *MediaExpNotificationHeader = @"MediaExpire";

NSString *MediaFollowNotificationHeader = @"MediaFollow";

NSString *UserExpiredNotificationHeader = @"UserExpired";

NSString *MediaPresetNotificationHeader = @"MediaPreset";

NSString *RoomRegisterHeader = @"RegisterRoom";

NSString *MergeNotiSender = @"UserMerged";

NSString *MergeNotiReceiever = @"UserMergedRefresh";

NSString *TabbarIndex = @"TabbarIndex";

NSString *AppLanguageKey = @"AppLanguageKey";

NSString *PDFAnnotationFolderName = @"PDFAnotation";

NSString *PublicShelfId = @"3";

NSString *MemberShelfId = @"1";

NSString *PresetShelfId = @"2";

NSString *SaleShelfId = @"0";

NSString *AutoSyncPdfAnnatationKey = @"SyncPdfAnnatationKey";

NSString *ThemeTmeplateFolderName = @"Theme Tmeplate";


NSString *LatestAppVersionKey = @"Latest AppVersion";

NSString *LastSyncPresetMedia = @"LastSyncPreset";

NSString *LastSyncCloudMedia = @"LastSyncCloudMedia";

NSString *LastOpenShelf = @"LastOpenShelf";

NSString *LastLeaveApp = @"LastLeaveApp";

NSString *MediaPresetNoExpireDate = @"99999999999999";

NSString *MediaPresetToRemove = @"0";

NSString *DeepAppLinkHeader = @"DeepAppLink";

NSString *UserSigninRequire = @"SigninRequire";

NSString *HelpUrl = @"https://file.sara-pad.com/DigiLibDashboard/help.html";

NSString *TermOfServiceUrl = @"https://sara-pad.com/term.html";

NSString *PrivacyPolicyUrl = @"https://file.sara-pad.com/digilibdashboard/Lincense/LicenseIOS.html"; // @"https://demo.sara-pad.com/digilibdashboard/Lincense/LicenseIOS.html";

NSString *CurrentAppVersion = @"1.8.1";

//Test
//NSString *ShareMediaUrl = @"https://dev.sara-pad.com/linkgen/l.aspx?Q=";
//NSString *ServerIp = @"https://dev.sara-pad.com/";
//NSString *hostIpCustomAPI = @"https://dev.sara-pad.com/CustomerAPI/";
//NSString *hostIpRoomAPI = @"https://dev.sara-pad.com/RoomAPI/";

//OLD API server
NSString *ShareMediaUrl = @"https://l.sara-pad.com/l.aspx?Q=";
//NSString *ServerIp = @"https://demo.sara-pad.com/";
//NSString *hostIpCustomAPI = @"https://demo.sara-pad.com/CustomerAPI/";
//NSString *hostIpRoomAPI = @"https://demo.sara-pad.com/RoomAPI/";


//New API server
NSString *hostIpCustomAPI = @"https://custapi.sara-pad.com/";
NSString *hostIpRoomAPI = @"https://roomapi.sara-pad.com/";


NSString *ProductionServerIp = @"https://demo.sara-pad.com/";

BOOL isRedeem = NO;

@end



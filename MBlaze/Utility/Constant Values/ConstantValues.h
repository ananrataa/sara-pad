//
//  ConstantValues.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstantValues : NSObject

extern  float default_navigation_height;
extern  float default_tabbar_height;

extern  NSString *KeyUID;

extern  NSString *KeyMBZSolutionId;

extern  NSString *KeyDeviceCode;

extern  NSString *mainFont;

extern  NSString *sukhumvitFont;
extern NSString *sukhumvitLightFont;
extern NSString *sukhumvitMediumFont;

extern  NSString *mainFontHeader;

extern  NSString *mediatypeId;

extern  NSString *KeyCuurrentUsedLribaryID;

extern  NSString *sarapadFloderName;

extern  NSString *temporyFloderName;

extern  NSString *DecryptKey;

extern  int MaximunFilesDownload;

extern  int MaximunDateForBorrow;

extern  NSString *KeyMediaTypeName;

extern  NSString *PublicShelfIdMediaOffline;

extern  NSString *NotificationTokenID;

extern  NSString *MediaExpNotificationHeader;

extern  NSString *MediaFollowNotificationHeader;

extern  NSString *UserExpiredNotificationHeader;

extern  NSString *MediaPresetNotificationHeader;

extern NSString *RoomRegisterHeader;

extern NSString *MergeNotiSender;

extern NSString *MergeNotiReceiever;

extern  NSString *TabbarIndex;

extern  NSString *AppLanguageKey;

extern  NSString *PDFAnnotationFolderName;

extern  NSString *PublicShelfId;

extern  NSString *MemberShelfId;

extern  NSString *PresetShelfId;

extern NSString *SaleShelfId;

extern  NSString *AutoSyncPdfAnnatationKey;

extern  NSString *ThemeTmeplateFolderName;

extern  NSString *CurrentAppVersion;

extern  NSString *LatestAppVersionKey;

extern  NSString *LastSyncPresetMedia;

extern  NSString *LastSyncCloudMedia;

extern  NSString *LastOpenShelf;

extern  NSString *LastLeaveApp;

extern  NSString *MediaPresetNoExpireDate;

extern  NSString *MediaPresetToRemove;

extern NSString *UserSigninRequire;

extern NSString *DeepAppLinkHeader;

extern NSString *ServerIp;

extern NSString *HelpUrl;

extern NSString *PrivacyPolicyUrl;

extern NSString *TermOfServiceUrl;

extern NSString *hostIpCustomAPI;

extern NSString *hostIpRoomAPI;

extern NSString *ShareMediaUrl;

///error
extern  NSString *UnknownError;


//Redeem
extern BOOL isRedeem;
@end

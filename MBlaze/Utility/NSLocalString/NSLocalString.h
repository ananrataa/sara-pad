//
//  NSLocalString.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLocalString : NSObject


+(NSString*)getCurrentLanguage;

@end

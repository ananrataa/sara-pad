//
//  ServiceAPI.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 6/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestfulServices.h"

@interface ServiceAPI : NSObject {
    
    RestfulServices *service;
    NSString *deviceCode;
}

extern NSString *KindOfRoomTypeDefault;
extern NSString *KindOfRoomTypeTag;
extern NSString *KindOfRoomTypeAuthor;
extern NSString *KindOfRoomTypePublisher;

-(void)browsMediaDetail:(NSString *)notificationName MediaID:(NSString *)mediaID RoomId:(NSString *)roomId LimtItem:(int)limit IsPreset:(BOOL)isPreset;

-(void)getAds:(NSString *)notificationName RoomId:(NSString *)roomId AdsType:(int)adsType IsSystemOwner:(NSString*)isSystemOwner;
-(void)getTypeAds:(NSString *)notificationName RoomId:(NSString *)roomId AdsType:(NSString *)adsType mediaId:(NSString*)mediaId;

-(void)mediaKindOfRoom:(NSString *)notificationName RoomId:(NSString *)roomId Language:(NSString *) lng CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID AppVersion:(NSString*)version Type:(int)intType Value:(NSString*)strValue;

-(void)categorieMediaItemPreview:(NSString *)notificationName RoomId:(NSString *)roomId MediaKind:(NSString *)mediaKind AppVersion:(NSString*)version;

-(void)mediaItems:(NSString *)notificationName RoomId:(NSString *)roomId CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID MediaKind:(NSString*)mediaKind SortBy:(int)sort LoadIndex:(int)loadIndex AppVersion:(NSString*)version;

-(void)roomsDetail:(NSString *)notificationName RoomID:(NSString *)roomID memberID:(NSString*)memberID;

-(void)mediaHistory:(NSString *)notificationName HistoryType:(int)type LoadIndex:(int)loadIndex;

-(void)changePassword:(NSString *)notificationName MemberID:(NSString *)memberID CurrPasswprd:(NSString *)currPass NewPassWord:(NSString *)newPass;

-(void)signin:(NSString *)notificationName UserName:(NSString *)userName Password:(NSString *)password;

-(void)forgetMemberPassword:(NSString *)notificationName Email:(NSString *)eMail;

-(void)libraries:(NSString *)notificationName index:(NSInteger)index;

-(void)mediasSuggested:(NSString *)notificationName RoomID:(NSString *)roomID MediaID:(NSString *)mediaID MediaKind:(NSString *)mediaKind;

-(void)mediaPreviewImage:(NSString *)notificationName MediaID:(NSString *)mediaID;

-(void)mediaFollow:(NSString *)notificationName MediaID:(NSString *)mediaID FollowType:(int)type;

-(void)mediaDownload:(NSString *)notificationName MediaID:(NSString *)mediaID RoomID:(NSString*) roomID SubjectID:(NSString*)sujectID;

-(void)mediaVote:(NSString *)notificationName MediaID:(NSString *)mediaID Score:(int)score;

-(void)mediaSearchTag:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind TagValue:(NSString*)tagValue SortBy:(int)sort LoadIndex:(int)loadIndex;

-(void)mediaSearchAuthor:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind AuthorName:(NSString*)authorName SortBy:(int)sort LoadIndex:(int)loadIndex;

-(void)mediaSearchPublisher:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind PublisherId:(NSString*)publisherId SortBy:(int)sort LoadIndex:(int)loadIndex;

-(void)mediaSearch:(NSString *)notificationName RoomID:(NSString *)roomID SearchFlag:(int)flag CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID MediaKind:(NSString *)kind TextSearch:(NSString *)text LoadIndex:(int)loadIndex;

-(void)findHistory:(NSString *)notificationName SearchKeyword:(NSString*)searchText RoomID:(NSString*)roomID;

-(void)browsMediaDetailSearch:(NSString *)notificationName RoomID:(NSString *)roomID MediaID:(NSString*)mediaID SearchText:(NSString *)searchText LimitItem:(int)limitItems;

-(void)categoryList:(NSString *)notificationName RoomID:(NSString *)roomID LoadIndex:(int)loadIndex;

-(void)limitRent:(NSString *)notificationName LimitItem:(int)limitItem LimitDay:(int)limitDay;

-(void)annotationFile:(NSString *)notificationName MemberID:(NSString *)memberID;

-(void)mediaReturn:(NSString *)notificationName MemberID:(NSString*)memberID RoomID:(NSString*)roomID MediaID:(NSString*)mediaID;

-(void)mediasLibrary:(NSString *)notificationName MediaKind:(NSString*)mediaKind LoadIndex:(int)loadDataIndex;

-(void)appVersion:(NSString *)notificationName;

-(void)getMembersDetailAsync:(NSString *)notificationName MemberID:(NSString *)memberID;

-(void)getToken:(NSString *)notificationName MemberID:(NSString *)memberID;

-(void)deviceRegister:(NSString *)notificationName MemberID:(NSString *)memberID DeviceDetail:(NSString *)deviceDetail MonitorSize:(float)mSize  MonitorType:(NSString*)mType NotificationTokenID:(NSString*)tokenID;

-(void)roomList:(NSString *)notificationName MemberID:(NSString *)memberID;

-(void)updateNotificationTokenID:(NSString *)notificationName TokenID:(NSString *)token;

-(void)logingOut:(NSString *)notificationName MemberID:(NSString*)memberId;

-(void)syncMediasPreset:(NSString *)notificationName MemberID:(NSString*)memberId;

-(void)syncMemberCloudMedia:(NSString *)notificationName MemberID:(NSString*)memberId;

-(void)isMemberRemainLogin:(NSString *)notificationName MemberID:(NSString*)memberId;

-(void)registerSaraPad:(NSString *)notificationName isByEmail:(BOOL)isEmail info:(NSDictionary*)dictInfo;

-(void)updateMemberProFile:(NSString *)notificationName info:(NSDictionary*)dictInfo;

-(void)textToSpeechByGoogleApi:(NSString *)notificationName Text:(NSString*)str destLanguage:(NSString*)lnguage;

-(void)MediaGenerateShareCode:(NSString *)notificationName FucntionName:(NSString*)fucntionName  UserID:(NSString*)userID MediaID:(NSString*)mediaID RoomID:(NSString*)roomID;

-(void)MediaShareCode:(NSString *)notificationName ShareID:(NSString*)shareID;

-(void)RemoveAnnotationFile:(NSString*)notificationName JsonString:(NSString*)json;

-(void)ShareAnnotationFile:(NSString*)notificationName FilePath:(NSString*)filePath Username:(NSString*)username;

-(void)cancelHttpService;

-(void)memberBag:(NSString *)notificationName MemberID:(NSString*)memberId;

-(void)saleShelf:(NSString *)notificationName MemberID:(NSString*)memberId;
@end

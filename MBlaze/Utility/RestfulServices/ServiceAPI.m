//
//  ServiceAPI.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 6/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ServiceAPI.h"
#import "ConstantValues.h"

@implementation ServiceAPI

NSString *KindOfRoomTypeDefault = @"0";
NSString *KindOfRoomTypeTag = @"1";
NSString *KindOfRoomTypeAuthor = @"2";
NSString *KindOfRoomTypePublisher = @"3";

-(instancetype)init{
    self = [super init];
    service = [[RestfulServices alloc] init];
    deviceCode = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
    return self;
}

-(void)browsMediaDetail:(NSString *)notificationName MediaID:(NSString *)mediaID RoomId:(NSString *)roomId LimtItem:(int)limit IsPreset:(BOOL)isPreset{
    //Old
//    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/BrowsMediaDetail?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i&isPreset=%@",hostIpRoomAPI,deviceCode,roomId,mediaID,limit,isPreset? @"true" : @"false"];
    //New
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/BrowsMediaDetail_new?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i",hostIpRoomAPI,deviceCode,roomId,mediaID,limit];

    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)getAds:(NSString *)notificationName RoomId:(NSString *)roomId AdsType:(int)adsType IsSystemOwner:(NSString*)isSystemOwner{
     NSString *param = [NSString stringWithFormat:@"%@api/DeviceAds/GetAds?deviceCode=%@&RoomId=%@&adsType=%i&isSystemOwner=%@",hostIpRoomAPI,deviceCode,roomId,adsType,isSystemOwner];
     [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)getTypeAds:(NSString *)notificationName RoomId:(NSString *)roomId AdsType:(NSString *)adsType mediaId:(NSString*)mediaId {
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceAds/GetAds_New?deviceCode=%@&roomID=%@&AdsType=%@&MediaID=%@",hostIpRoomAPI,deviceCode,roomId,adsType, mediaId];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}
//-(void)loadSlideAds:(NSString *)notificationName RoomId:(NSString *)roomId{
//     NSString *param = [NSString stringWithFormat:@"%@api/DeviceAds/GetAds?deviceCode=%@&RoomId=%@&adsType=%i&isSystemOwner=%@",hostIpRoomAPI,deviceCode,roomId,2,@"false"];
//     [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
//}
-(void)mediaKindOfRoom:(NSString *)notificationName RoomId:(NSString *)roomId Language:(NSString *) lng CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID AppVersion:(NSString*)version Type:(int)intType Value:(NSString*)strValue{
    //api/DeviceMedia/MediaKindOfRoom?deviceCode={deviceCode}&roomId={roomId}&catId={catId}&subId={subId}&lang={lang}&version={version}&kindType={kindType}&findValue={findValue}
    
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaKindOfRoom?deviceCode=%@&roomId=%@&catId=&subId=&lang=%@&version=%@&kindType=%i&findValue=%@",hostIpRoomAPI,deviceCode,roomId,lng,version,intType,strValue];
    
    if ([cateID length] > 0 && [subCateID length] > 0)
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaKindOfRoom?deviceCode=%@&roomId=%@&catId=%@&subId=%@&lang=%@&version=%@&kindType=%i&findValue=%@",hostIpRoomAPI,deviceCode,roomId,cateID,subCateID,lng,version,intType,strValue];
    else if ([cateID length] > 0)
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaKindOfRoom?deviceCode=%@&roomId=%@&catId=%@&subId=&lang=%@&version=%@&kindType=%i&findValue=%@",hostIpRoomAPI,deviceCode,roomId,cateID,lng,version,intType,strValue];
     else if ([subCateID length] > 0)
         param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaKindOfRoom?deviceCode=%@&roomId=%@&catId=&subId=%@&lang=%@&version=%@&kindType=%i&findValue=%@",hostIpRoomAPI,deviceCode,roomId,subCateID,lng,version,intType,strValue];
    
   // NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)categorieMediaItemPreview:(NSString *)notificationName RoomId:(NSString *)roomId MediaKind:(NSString *)mediaKind AppVersion:(NSString*)version{
    
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/CategorieMediaItemPreview?deviceCode=%@&roomId=%@&mediatypeId=%@&mediaKind=%@&CategoriePane=10&MediaItemVol=10&version=%@",hostIpRoomAPI,deviceCode,roomId,mediatypeId,mediaKind,version];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)mediaItems:(NSString *)notificationName RoomId:(NSString *)roomId CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID MediaKind:(NSString*)mediaKind SortBy:(int)sort LoadIndex:(int)loadIndex AppVersion:(NSString*)version{
    
    NSString *  param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=&subId=&sort=%i&loadIndex=%i&version=%@",hostIpRoomAPI,deviceCode,roomId,mediaKind,sort,loadIndex,version];
    
    if ([cateID length] > 0 && [subCateID length] > 0)
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&sort=%i&loadIndex=%i&version=%@",hostIpRoomAPI,deviceCode,roomId,mediaKind,cateID,subCateID,sort,loadIndex,version];
    
    else if([subCateID length] > 0)
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=&subId=%@&sort=%i&loadIndex=%i&version=%@",hostIpRoomAPI,deviceCode,roomId,mediaKind,subCateID,sort,loadIndex,version];
    
   else if([cateID length] > 0)
       param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=&sort=%i&loadIndex=%i&version=%@",hostIpRoomAPI,deviceCode,roomId,mediaKind,cateID,sort,loadIndex,version];
    
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)roomsDetail:(NSString *)notificationName RoomID:(NSString *)roomID memberID:(NSString*)memberID {
//    NSString *param = [NSString stringWithFormat:@"%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@",hostIpRoomAPI,deviceCode,roomID];
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@&memberID=%@",hostIpRoomAPI,deviceCode,roomID, memberID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- MediaHistory -----
-(void)mediaHistory:(NSString *)notificationName HistoryType:(int)type LoadIndex:(int)loadIndex{
//    NSString *x = @"api/DeviceMedia/MediaHistory_ne/Applications/Android File Transfer.appw?deviceCode={deviceCode}&HisType={HisType}&startDate={startDate}&endDate={endDate}&loadIndex={loadIndex}&isWeb={isWeb}"

    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaHistory_new?deviceCode=%@&HisType=%i&startDate=&endDate=&loadIndex=%i",hostIpRoomAPI,deviceCode,type,loadIndex];

//    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaHistory?deviceCode=%@&HisType=%i&loadIndex=%i",hostIpRoomAPI,deviceCode,type,loadIndex];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


#pragma mark ----------- ChangePassword -------
-(void)changePassword:(NSString *)notificationName MemberID:(NSString *)memberID CurrPasswprd:(NSString *)currPass NewPassWord:(NSString *)newPass{
    
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/ChangePassword?deviceCode=%@&memberID=%@&password=%@&newPassword=%@",hostIpCustomAPI,deviceCode,memberID,currPass,newPass];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----------  Signin --------
-(void)signin:(NSString *)notificationName UserName:(NSString *)userName Password:(NSString *)password{
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/Signin?emailOrUsername=%@&password=%@&deviceCode=%@",hostIpCustomAPI,userName,password,deviceCode];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark --------- ForgetMemberPassword --------
-(void)forgetMemberPassword:(NSString *)notificationName Email:(NSString *)eMail{
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/ForgetMemberPassword?emailAddress=%@",hostIpCustomAPI,eMail];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"STRING" HttpMethod:@"POST"];
}

#pragma mark ------- Libraries ---------
-(void)libraries:(NSString *)notificationName index:(NSInteger)index {
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/Libraries_new?deviceCode=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,index];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ------- MediasSuggested -----
-(void)mediasSuggested:(NSString *)notificationName RoomID:(NSString *)roomID MediaID:(NSString *)mediaID MediaKind:(NSString *)mediaKind{
       //pi/DeviceMedia/MediasSuggested?deviceCode={deviceCode}&roomId={roomId}&mediaId={mediaId}&mediaKind={mediaKind}
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediasSuggested?deviceCode=%@&roomId=%@&mediaId=%@&mediaKind=%@",hostIpRoomAPI,deviceCode,roomID,mediaID,mediaKind];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark --------- MediaPreviewImage ------
-(void)mediaPreviewImage:(NSString *)notificationName MediaID:(NSString *)mediaID{
     //api/DeviceMedia/MediaPreviewImage?deviceCode={deviceCode}&mediaId={mediaId}
     //fot test param = @"http://dev.sara-pad.com:88/RoomAPI/api/DeviceMedia/MediaPreviewImage?deviceCode=01FBB3E6-E0A8-4600-B92C-07945722C8EA&mediaId=5ADF806E-EFAC-4559-9FB4-000872DB793B";
    
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaPreviewImage?deviceCode=%@&mediaId=%@",hostIpRoomAPI,deviceCode,mediaID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- MediaFollow -----
-(void)mediaFollow:(NSString *)notificationName MediaID:(NSString *)mediaID FollowType:(int)type{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaFollow?deviceCode=%@&mediaId=%@&followType=%i",hostIpRoomAPI,deviceCode,mediaID,type];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ------- MediaDownload ----
-(void)mediaDownload:(NSString *)notificationName MediaID:(NSString *)mediaID RoomID:(NSString*) roomID SubjectID:(NSString*)sujectID{
    NSString *param;
    /*
    if ([sujectID length] > 0) {
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaDownload?deviceCode=%@&MediaId=%@&RoomId=%@&SubjectId=%@",hostIpRoomAPI,deviceCode,mediaID,roomID,sujectID];
    }
    else{
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaDownload?deviceCode=%@&MediaId=%@&RoomId=%@",hostIpRoomAPI,deviceCode,mediaID,roomID];
    }
    */
    NSString *newApi = @"%@api/DeviceMedia/MediaDownload_new?deviceCode=%@&MediaId=%@&RoomId=%@&SubjectId=%@";
    param = [NSString stringWithFormat:newApi, hostIpRoomAPI, deviceCode, mediaID, roomID, sujectID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


#pragma  mark --------- MediaVote -------
-(void)mediaVote:(NSString *)notificationName MediaID:(NSString *)mediaID Score:(int)score{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaVote?deviceCode=%@&MediaId=%@&ScoreVote=%i",hostIpRoomAPI,deviceCode,mediaID,score];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark -------- MediaSearch by Tag -------
-(void)mediaSearchTag:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind TagValue:(NSString*)tagValue SortBy:(int)sort LoadIndex:(int)loadIndex{
    //api/DeviceMedia/MediaSearchTag?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&findValue={findValue}&sort={sort}&loadIndex={loadIndex}
    NSString * param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearchTag?deviceCode=%@&roomId=%@&mediaKind=%@&findValue=%@&sort=%i&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,tagValue,sort,loadIndex];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
    
}

#pragma mark ------ MediaSearch by Author --------
-(void)mediaSearchAuthor:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind AuthorName:(NSString*)authorName SortBy:(int)sort LoadIndex:(int)loadIndex{
    //api/DeviceMedia/MediaSearchAuthor?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&findValue={findValue}&sort={sort}&loadIndex={loadIndex}
    NSString * param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearchAuthor?deviceCode=%@&roomId=%@&mediaKind=%@&findValue=%@&sort=%i&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,authorName,sort,loadIndex];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
    
}

#pragma mark ------ MediaSearch by Publisher ------
-(void)mediaSearchPublisher:(NSString *)notificationName RoomID:(NSString *)roomID MediaKind:(NSString*)kind PublisherId:(NSString*)publisherId SortBy:(int)sort LoadIndex:(int)loadIndex{
    //api/DeviceMedia/MediaSearchPublisher?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&publisherId={publisherId}&sort={sort}&loadIndex={loadIndex}
    NSString * param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearchPublisher?deviceCode=%@&&roomId=%@&mediaKind=%@&publisherId=%@&sort=%i&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,publisherId,sort,loadIndex];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
    
}

#pragma mark ------- MediaSearch -------
-(void)mediaSearch:(NSString *)notificationName RoomID:(NSString *)roomID SearchFlag:(int)flag CategoryID:(NSString *)cateID SubCatrgory:(NSString *)subCateID MediaKind:(NSString *)kind TextSearch:(NSString *)text LoadIndex:(int)loadIndex{
    //api/DeviceMedia/MediaSearch?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&catId={catId}&subId={subId}&findValue={findValue}&loadIndex={loadIndex}
    
    //api/DeviceMedia/MediaSearchRoomMember?deviceCode={deviceCode}&mediaKind={mediaKind}&catId={catId}&subId={subId}&findValue={findValue}&loadIndex={loadIndex}
    
    //api/DeviceMedia/MediaSearchTotal?deviceCode={deviceCode}&mediaKind={mediaKind}&catId={catId}&subId={subId}&findValue={findValue}&loadIndex={loadIndex}
    NSString *param;
    switch (flag) {
        case 0:
            if([cateID length] > 0 && subCateID > 0)
                param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearch?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=%@&findValue=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,cateID,subCateID,text,loadIndex];
            else if ([cateID length] > 0)
                param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearch?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=&findValue=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,cateID,text,loadIndex];
            else
                param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearch?deviceCode=%@&roomId=%@&mediaKind=%@&catId=&subId=%@&findValue=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,kind,subCateID,text,loadIndex];
            break;
        case 1:
            param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearchRoomMember?deviceCode=%@&mediaKind=%@&catId=&subId=&findValue=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,kind,text,loadIndex];
            break;
        case 2:
            param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaSearchTotal?deviceCode=%@&mediaKind=%@&catId=&subId=&findValue=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,kind,text,loadIndex];
        default:
            break;
    }
    
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- FindHistory (send user's search keyword to server)------
-(void)findHistory:(NSString *)notificationName SearchKeyword:(NSString*)searchText RoomID:(NSString*)roomID{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/FindHistory?findHistory=%@&roomId=%@",hostIpRoomAPI,searchText,roomID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- BrowsMediaDetailSearch -----
-(void)browsMediaDetailSearch:(NSString *)notificationName RoomID:(NSString *)roomID MediaID:(NSString*)mediaID SearchText:(NSString *)searchText LimitItem:(int)limitItems{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/BrowsMediaDetailSearch?deviceCode=%@&RoomId=%@&MediaId=%@&findHistory=%@&ItemLimitMember=%i",hostIpRoomAPI,deviceCode,roomID,mediaID,searchText,limitItems];
    //NSLog(@"Param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark -------- DeviceMedia ----
-(void)categoryList:(NSString *)notificationName RoomID:(NSString *)roomID LoadIndex:(int)loadIndex{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/Categorie?deviceCode=%@&roomId=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,roomID,loadIndex];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ------- LimitRent ------
-(void)limitRent:(NSString *)notificationName LimitItem:(int)limitItem LimitDay:(int)limitDay{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/LimitRent?deviceCode=%@&itemLimit=%i&dayLimit=%i",hostIpRoomAPI,deviceCode,limitItem,limitDay];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ------ Annotation File List ------
-(void)annotationFile:(NSString *)notificationName MemberID:(NSString *)memberID{
    //api/DeviceMedia/AnnotationItem?deviceCode={deviceCode}&memberId={memberId}
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/AnnotationItem?deviceCode=%@&memberId=%@",hostIpRoomAPI,deviceCode,memberID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- MediaReturn ----
-(void)mediaReturn:(NSString *)notificationName MemberID:(NSString*)memberID RoomID:(NSString*)roomID MediaID:(NSString*)mediaID{
    NSString *param;
    if ([memberID isEqualToString:deviceCode])
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaReturn?deviceCode=%@&MediaId=%@&RoomId=%@&MemberId=",hostIpRoomAPI,deviceCode,mediaID,roomID];
    else
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaReturn?deviceCode=%@&MediaId=%@&RoomId=%@&MemberId=%@",hostIpRoomAPI,deviceCode,mediaID,roomID,memberID];
    
    //NSLog(@"prepare4ReturnMedia : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)mediasLibrary:(NSString *)notificationName MediaKind:(NSString*)mediaKind LoadIndex:(int)loadDataIndex{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediasLibrary?deviceCode=%@&mediaKind=%@&loadIndex=%i",hostIpRoomAPI,deviceCode,mediaKind,loadDataIndex];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- AppVersion ---
-(void)appVersion:(NSString *)notificationName{
    //old version NSString *param = [NSString stringWithFormat:@"%@api/AppVersion/AppVersion?os=ios",hostIpRoomAPI];
    NSString *param = [NSString stringWithFormat:@"%@api/AppVersion/AppVersionV1?os=ios&version=%@",hostIpRoomAPI,CurrentAppVersion];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"STRING" HttpMethod:@"GET"];
    
    
}

#pragma  mark ---- GetMembersDetailAsync ----
-(void)getMembersDetailAsync:(NSString *)notificationName MemberID:(NSString *)memberID{
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/GetMembersDetailAsync?MemberId=%@",hostIpCustomAPI,memberID];
    //NSLog(@"Members Details param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ---- GetToken ----
-(void)getToken:(NSString *)notificationName MemberID:(NSString *)memberID{
    NSString *param = [NSString stringWithFormat:@"%@api/Authen/GetToken?MemberId=%@&isSave=%@",hostIpCustomAPI,memberID,@"true"];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"STRING" HttpMethod:@"GET"];
}

#pragma mark ------ DeviceRegister ---
-(void)deviceRegister:(NSString *)notificationName MemberID:(NSString *)memberID DeviceDetail:(NSString *)deviceDetail MonitorSize:(float)mSize  MonitorType:(NSString*)mType NotificationTokenID:(NSString*)tokenID{
    
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/DeviceRegister?memberId=%@&deviceCode=%@&deviceDetail=%@&monitorSize=%.1f&monitorType=%@&token=%@",hostIpCustomAPI,memberID,deviceCode,deviceDetail,mSize,mType,tokenID];
    //NSLog(@"deviceRegister param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"STRING" HttpMethod:@"POST"];
}

#pragma mark ----- room list ------
-(void)roomList:(NSString *)notificationName MemberID:(NSString *)memberID{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceRoom/Rooms?deviceCode=%@&memberId=",hostIpRoomAPI,deviceCode];
    
    if ([memberID length] > 0) {
        param = [NSString stringWithFormat:@"%@api/DeviceRoom/Rooms?deviceCode=%@&memberId=%@",hostIpRoomAPI,deviceCode,memberID];
    }
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ---- UpdateToken -----
-(void)updateNotificationTokenID:(NSString *)notificationName TokenID:(NSString *)token{
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/UpdateToken?deviceCode=%@&token=%@",hostIpCustomAPI,deviceCode,token];
    //NSLog(@"token param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ---- LogingOut -----
-(void)logingOut:(NSString *)notificationName MemberID:(NSString*)memberId{
    NSString *param;
    if ([memberId length] > 0)
        param = [NSString stringWithFormat:@"%@api/DevicePlatform/LogingOut?deviceCode=%@&memberID=%@",hostIpCustomAPI,deviceCode,memberId];
    else
        param = [NSString stringWithFormat:@"%@api/DevicePlatform/LogingOut?deviceCode=%@&memberID=",hostIpCustomAPI,deviceCode];

    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


#pragma  mark ----- sync Media Preset ------
-(void)syncMediasPreset:(NSString *)notificationName MemberID:(NSString*)memberId{
    //api/DeviceMedia/MediasPresetShelf?deviceCode=F47CB341-1CE9-4F12-BC83-C9800DA3D5CD&memberId=4B494ED5-D498-4157-9C1E-14A02259F8FC
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediasPresetShelf?deviceCode=%@&memberId=%@",hostIpRoomAPI,deviceCode,memberId];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


#pragma mark ------ sync Media on Member's Cloud ------
-(void)syncMemberCloudMedia:(NSString *)notificationName MemberID:(NSString*)memberId{
    //api/DeviceMedia/MediasMemberShelf?deviceCode={deviceCode}&memberId={memberId}
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediasMemberShelf?deviceCode=%@&memberId=%@",hostIpRoomAPI,deviceCode,memberId];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- Member Remain Login ---
-(void)isMemberRemainLogin:(NSString *)notificationName MemberID:(NSString*)memberId{
    //api/DevicePlatform/isSignin?memberID={memberID}&deviceCode={deviceCode}
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/isSignin?memberID=%@&deviceCode=%@",hostIpCustomAPI,memberId,deviceCode];
    //NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

#pragma mark ----- Register -----------
-(void)registerSaraPad:(NSString *)notificationName isByEmail:(BOOL)isEmail info:(NSDictionary*)dictInfo{

    if (isEmail) {
        //api/DevicePlatform/mailRegister?mailRegis={mailRegis}&mailPass={mailPass}
        NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/mailRegister?mailRegis=%@&mailPass=%@",hostIpCustomAPI,[dictInfo objectForKey:@"EMAIL"],[dictInfo objectForKey:@"PASSWORD"]];
        //NSLog(@"param : %@",param);
        [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
    }
    else{
        //api/DevicePlatform/AhutenLoginUserID?DisPlayname={DisPlayname}&deviceCode={deviceCode}&urlIMG={urlIMG}&tokenAuthen={tokenAuthen}&userID={userID}
        NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/AhutenLoginUserID?DisPlayname=%@&deviceCode=%@&urlIMG=%@&tokenAuthen=%@&userID=%@",hostIpCustomAPI,[dictInfo objectForKey:@"DISPLAYNAME"],deviceCode,[dictInfo objectForKey:@"ProfileImageURL"],[dictInfo objectForKey:@"AuthenToken"],[dictInfo objectForKey:@"USERID"]];
        //NSLog(@"param : %@",param);
        [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
    }
}

-(void)updateMemberProFile:(NSString *)notificationName info:(NSDictionary*)dictInfo{
    //api/DevicePlatform/UpdateProFile?memberID={memberID}&birthdate={birthdate}&Fname={Fname}&eMail={eMail}
    NSString *birthDay = @"";
    if ([dictInfo objectForKey:@"DateOfBirth"]) {
        birthDay = [dictInfo objectForKey:@"DateOfBirth"];
    }
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/UpdateProFile?memberID=%@&birthdate=%@&Fname=%@&eMail=%@",hostIpCustomAPI,[dictInfo objectForKey:@"MEMBERID"],birthDay,[dictInfo objectForKey:@"FName"],[dictInfo objectForKey:@"EMAIL"]];
  //  NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)textToSpeechByGoogleApi:(NSString *)notificationName Text:(NSString*)str destLanguage:(NSString*)lnguage{
    NSString *param = [NSString stringWithFormat:@"https://translate.google.com/translate_tts?ie=UTF-8&q=%@&tl=%@&client=tw-ob",str,lnguage];
     [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)MediaGenerateShareCode:(NSString *)notificationName FucntionName:(NSString*)fucntionName  UserID:(NSString*)userID MediaID:(NSString*)mediaID RoomID:(NSString*)roomID{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaGenQ?F=%@&U=%@&M=%@&L=%@",hostIpRoomAPI,fucntionName,userID,mediaID,roomID];
   // NSLog(@"param : %@",param);
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"STRING" HttpMethod:@"GET"];
}

-(void)MediaShareCode:(NSString *)notificationName ShareID:(NSString*)shareID{
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaListQ?C=%@",hostIpRoomAPI,shareID];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


-(void)RemoveAnnotationFile:(NSString*)notificationName JsonString:(NSString*)json{
    //api/DeviceMedia/AnnotationRemove?deviceCode={deviceCode}
    NSString *url = [NSString stringWithFormat:@"%@api/DeviceMedia/AnnotationRemove?deviceCode=%@",hostIpRoomAPI,deviceCode];
    [service uploadJsonStringToServer:notificationName Url:url JsonString:json];
}

-(void)ShareAnnotationFile:(NSString*)notificationName FilePath:(NSString*)filePath Username:(NSString*)username{
//api/DeviceMedia/AnnotationShare?userName={userName}&filePath={filePath}
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/AnnotationShare?userName=%@&filePath=%@",hostIpRoomAPI,username,filePath];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}


-(void)cancelHttpService{
    [service cancelServiceTask];
}

#pragma mark ----- Payment -----------
-(void)memberBag:(NSString *)notificationName MemberID:(NSString*)memberId {
    NSString *param = [NSString stringWithFormat:@"%@api/DevicePlatform/Bag_Member?memberID=%@&DeviceCode=%@",hostIpCustomAPI, memberId, deviceCode];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}
#pragma mark ----- SaleShelf -----------
-(void)saleShelf:(NSString *)notificationName MemberID:(NSString*)memberId {
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediasSaleShelf?deviceCode=%@&memberId=%@",hostIpRoomAPI, deviceCode, memberId];
    [service sendHTTPRequest:param NotificationName:notificationName ReturnType:@"JSON" HttpMethod:@"GET"];
}
@end

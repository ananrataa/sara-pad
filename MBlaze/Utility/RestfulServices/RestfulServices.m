//
//  RestfulServices.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/25/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "RestfulServices.h"
#import "Utility.h"
#import "ConstantValues.h"

@implementation RestfulServices

//NSString *hostIpCustomAPI = @"https://dev.sara-pad.com:88/CustomerAPI/";
//NSString *hostIpRoomAPI = @"https://dev.sara-pad.com:88/RoomAPI/";

//NSString *hostIpCustomAPI = @"https://demo.sara-pad.com:88/CustomerAPI/";
//NSString *hostIpRoomAPI = @"https://demo.sara-pad.com:88/RoomAPI/";

#define API_KEY @"IE11bHRpbWVkaWEgIA0KIA0KICANCiBEaWdpdGFsIENvbnRlbnRzIA0K"

-(void)sendHTTPRequest:(NSString*)strURL NotificationName:(NSString *)NotiName ReturnType:(NSString*)returntype HttpMethod:(NSString*)method{
    
    NotificattionName = NotiName;
    
    NSString *str_url = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    dispatch_queue_t connectivityThread = dispatch_queue_create("th.co.mbox.MBlaze", NULL);
    dispatch_async(connectivityThread, ^{
        BOOL isInternet = NO;
        //while (!isInternet){
        isInternet =[Utility connectedToNetwork];
        if (!isInternet)
        {
            //NSLog(@"internet not connected");
             dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NotiName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"You’re not connected to the internet.",@"error", nil]];
             });
        }
        else
        {
            // NSLog(@"internet connected");
            //strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                mTimer = [NSTimer scheduledTimerWithTimeInterval:32.0f
                                                          target:self
                                                        selector:@selector(connectServiceTimeout)
                                                        userInfo:nil
                                                         repeats:NO];
                
                NSURL *url = [NSURL URLWithString:str_url];
                //NSLog(@"URL : %@",url);
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:30.0f];
                [request addValue:API_KEY forHTTPHeaderField:@"API_KEY"];
                [request setHTTPMethod:method];
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
                //[NSURLSession sharedSession];
                
                task = [session dataTaskWithRequest:request completionHandler:
                        ^(NSData *data, NSURLResponse *response, NSError *error) {
                            [self sendDataResult:data NSURLResponse:response NSError:error NotificationName:NotiName ReturnType:returntype];
                        }];
                
                
                [task resume];
            });
        }
        // }
    });
    
    
    
}

-(void)uploadFileToServer:(NSString *)NotiName Url:(NSString*)strURL FilePath:(NSString*)filePath{
     NotificattionName = NotiName;
    
    NSString *boundary = @"unique-consistent-string";
    // Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    
    NSError *error;
    
    
    NSMutableData *body = [NSMutableData data];
    NSString* textStr = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    else{
        // add params (all params are strings)
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"File",filePath.lastPathComponent] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: text/plain\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[textStr dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    // the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    
    //NSLog(@"---------- body string --------");
    //NSLog(@"%@",[[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding]);
    
    mTimer = [NSTimer scheduledTimerWithTimeInterval:35.0f
                                              target:self
                                            selector:@selector(connectServiceTimeout)
                                            userInfo:nil
                                             repeats:NO];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    //[request setAllHTTPHeaderFields:headers];
    [request addValue:API_KEY forHTTPHeaderField:@"API_KEY"];
    [request addValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [request addValue:contentType forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:body];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
   
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    //NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    [self sendDataResult:data NSURLResponse:response NSError:error NotificationName:NotiName ReturnType:@"JSON"];
                                                }];
    [dataTask resume];
}

-(void)uploadJsonStringToServer:(NSString *)NotiName Url:(NSString*)strURL JsonString:(NSString*)jsonString{
    NotificattionName = NotiName;
    
    NSData *bodyData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    // the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[bodyData length]];
    
    //NSLog(@"---------- body string --------");
    //NSLog(@"%@",[[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding]);
    
    mTimer = [NSTimer scheduledTimerWithTimeInterval:35.0f
                                              target:self
                                            selector:@selector(connectServiceTimeout)
                                            userInfo:nil
                                             repeats:NO];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    //[request setAllHTTPHeaderFields:headers];
    [request addValue:API_KEY forHTTPHeaderField:@"API_KEY"];
    [request addValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [request addValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:bodyData];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    //NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    [self sendDataResult:data NSURLResponse:response NSError:error NotificationName:NotiName ReturnType:@"JSON"];
                                                }];
    [dataTask resume];
}

/*-(void)NSURLSessionDataTaskResult:(NSHTTPURLResponse *) response NotificationName:(NSString*)notificationName{
    NSInteger statusCode = [response statusCode];
     NSLog(@"dataTaskWithRequest HTTP status code: %ld", (long)statusCode);
    if ([response statusCode] != 200) {
        NSLog(@"dataTaskWithRequest HTTP status code: %ld", (long)statusCode);
        [self cancelTimer];
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[Utility NSLocalizedString:@"ServerError"],@"error", nil]];
    }
    else{
        [self cancelTimer];
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[Utility NSLocalizedString:@"UploadFileSuccess"],@"success", nil]];
    }
}*/

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    //NSLog(@"Progress Total Sent:%lli , Total Size:%lli",totalBytesSent,totalBytesExpectedToSend);
    
}

-(void) connection:(NSURLConnection * ) connection didFailWithError:(NSError *)error {
    
    NSLog(@"connection error : %@",error.description);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificattionName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription],@"error", nil]];
}


-(NSString *)decodeNSString:(NSString *)text{
    if ([text length] > 0 && ![text isEqualToString:@""]){
        NSData *newdata = [text dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        return [[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    }
    else
        return text;
    
}

-(void)connectServiceTimeout{
    //  dispatch_async(dispatch_get_main_queue(), ^{
    [self cancelServiceTask];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificattionName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[Utility NSLocalizedString:@"ConnectServiceF"],@"error", nil]];
    
    [self cancelTimer];
    
    // });
}

-(void)cancelServiceTask{
    isCancelTask = YES;
    [task cancel];
}

-(void)cancelTimer{
    [mTimer invalidate];
    mTimer = nil;
}

-(void)sendDataResult:(NSData *)data NSURLResponse:(NSURLResponse *)response NSError:(NSError *)error NotificationName:(NSString *)NotiName ReturnType:(NSString*)returntype{
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        // handle HTTP errors here
        if ([response isKindOfClass:[NSHTTPURLResponse class]] && [NotiName length] > 0) {
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            if (statusCode != 200) {
                NSLog(@"dataTaskWithRequest HTTP status code: %ld", (long)statusCode);
                [self cancelTimer];
                [[NSNotificationCenter defaultCenter] postNotificationName:NotiName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[Utility NSLocalizedString:@"ServerError"],@"error", nil]];
            }
            else{
                if (error) {
                    // NSLog(@"1.ERROR : %@", [error localizedDescription]);
                    
                    [self cancelTimer];
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotiName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription],@"error", nil]];
                    
                } else {
                    if ([returntype isEqualToString:@"STRING"]) {
                        [self cancelTimer];
                        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotiName object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:myString,@"Response", nil]];
                    }
                    else if ([returntype isEqualToString:@"JSON"]){
                        [self cancelTimer];
                        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:0
                                                                                   error:NULL];
                        
                        //NSLog(@"--- NSDATA : %@ -----",response);
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotiName object:nil userInfo:response];
                        
                    }
                }
                
            }
        }
        
    });
}

@end

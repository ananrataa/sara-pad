//
//  RestfulServices.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/25/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestfulServices : NSObject<NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>{
    NSTimer *mTimer;
    BOOL isCancelTask;
    NSString *NotificattionName;
    NSURLSessionDataTask *task;
}

//extern NSString *hostIpCustomAPI;
//extern NSString *hostIpRoomAPI;

-(void)sendHTTPRequest:(NSString*)strURL NotificationName:(NSString *)NotiName ReturnType:(NSString*)returntype HttpMethod:(NSString*)method;

-(void)uploadFileToServer:(NSString *)NotiName Url:(NSString*)strURL FilePath:(NSString*)filePath;

-(void)uploadJsonStringToServer:(NSString *)NotiName Url:(NSString*)strURL JsonString:(NSString*)jsonString;

-(void)cancelServiceTask;

@end

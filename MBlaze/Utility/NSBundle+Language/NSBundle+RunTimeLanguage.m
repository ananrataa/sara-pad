//
//  NSBundle.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 17/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

// NSBundle+RunTimeLanguage.m
#import "NSBundle+RunTimeLanguage.h"

@implementation NSBundle (RunTimeLanguage)

+(NSString *)runTimeLocalizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName
{
    NSArray *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *path= [[NSBundle mainBundle] pathForResource:lang[0] ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    NSString *localizedString=[languageBundle localizedStringForKey:key value:key table:nil];
    //NSLog(@"LocalizedString : %@",localizedString);
    return localizedString;
}
@end

//
//  Utility.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/21/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Utility.h"
#import <sys/utsname.h>
#include "math.h"

#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>

#import "DBManager.h"
#import "SplashScreen.h"
#import "MemberInfo.h"

#import "ConstantValues.h"

#import "Reachability.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#import "CustomActivityIndicator.h"

#import <UserNotifications/UserNotifications.h>
#import "SPNotification.h"

#import "NSBundle+RunTimeLanguage.h"

#import "ServiceAPI.h"

#import <LineSDK/LineSDK.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation Utility

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

BOOL const isFullScreen = YES;

UIViewController *_self;

#define IMAGE_MAX_WIDTH 480.f

+ (NSString *) documentPath {
    // Path to the application's "~/Documents" directory
    NSURL *pathURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
    return [pathURL path];
}

+ (NSString *) applicationSupportPath {
    // Path to the application's "~/Documents" directory
    NSURL *pathURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] firstObject];
    
    return [pathURL path];
}

+ (void) createNewDirectory:(NSString *) dirName isSupportDirectory:(BOOL) isSupportDir {
    NSString *pathDir = [self documentPath];
    
    if (isSupportDir) {
        pathDir = [self applicationSupportPath];
    }
    
    NSError *error = nil;
    NSString *dataPath = [pathDir stringByAppendingPathComponent:dirName];
    NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:attr error:&error];
        
        if (error) {
            NSLog(@"Fail to creating directory path: %@", [error localizedDescription]);
        } else {
            NSLog(@"%@ directory are created.", dirName);
        }
    }
}

+ (void) removeAllFilesInDirectory: (NSString *) dirName {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [[self documentPath] stringByAppendingPathComponent:dirName];
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
        if (!success || error)
            NSLog(@"Fail to remove file path: %@", [error localizedDescription]);
        else
            NSLog(@"Success to remove file path: %@", directory);
    }
}

+(void) removeFileInDirectory:(NSString *)filePath{
    NSError *error = nil;
    NSString * path = [[Utility documentPath] stringByAppendingPathComponent:filePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (error)
            NSLog(@"Fail to remove file path: %@", [error localizedDescription]);
        else
            NSLog(@"Success to remove file path: %@", path);
    }
}

+(void)readFileInDir:(NSString*)dir{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [[Utility documentPath] stringByAppendingPathComponent:dir];
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        NSLog(@"File: %@", file);
    }
}


+(void) saveMediaFile:(NSString *) filePath FileData:(NSData*)fileData{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        // Perform async operation
        
        if (fileData) {
            NSError *error;
            NSString* docPath = [[Utility documentPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",filePath]];
            [fileData writeToFile:docPath options:NSAtomicWrite error:&error];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error)
                    NSLog(@"save file Failed: %@", [error localizedDescription]);
                else
                    NSLog(@"save file Successfully : %@",docPath);
            });
        }
        
    });
    
}


+ (UIImage *) thumbnailFromVideoAtURL:(NSURL *) url {
    AVAsset *asset = [AVAsset assetWithURL:url];
    
    //  Get thumbnail at the very start of the video
    CMTime thumbnailTime = [asset duration];
    thumbnailTime.value = 1;
    
    //  Get image from the video at the given time
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    float imgFixWidth = 180.0f;
    float ratio = imgFixWidth/thumbnail.size.width;
    
    UIImage *resizeThumbnail = [self rescaleImage:thumbnail scaledToSize:CGSizeMake(imgFixWidth, thumbnail.size.height*ratio)];
    
    CGImageRelease(imageRef);
    
    return resizeThumbnail;
}

/*+ (UIImage *) changeImageScaleWithImage:(UIImage *)image scaledToFillSize:(CGSize)size
{
    //This code is for just for change image scale not for resize You have to set CGSize as your image /////width and hight so image will not stretch and it arrange at the middle.
     //cr. http:stackoverflow.com/questions/4712329/how-to-resize-the-image-programmatically-in-objective-c-in-iphone
 

    CGFloat scale = MAX(size.width/image.size.width, size.height/image.size.height);
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    CGRect imageRect = CGRectMake((size.width - width)/2.0f,
                                  (size.height - height)/2.0f,
                                  width,
                                  height);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    [image drawInRect:imageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}*/


+(UIImage *)rescaleImage:(UIImage *)image scaledToSize:(CGSize)size{
    /*This code is for just for change image scale not for resize You have to set CGSize as your image width and hight so image will not stretch and it arrange at the middle.
     cr. http://stackoverflow.com/questions/4712329/how-to-resize-the-image-programmatically-in-objective-c-in-iphone
     */
    
    CGFloat scale = MAX(size.width/image.size.width, size.height/image.size.height);
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    CGRect imageRect = CGRectMake((size.width - width)/2.0f,
                                  (size.height - height)/2.0f,
                                  width,
                                  height);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    [image drawInRect:imageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
}

+(NSString*)deviceName{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *machineName = [NSString stringWithCString:systemInfo.machine
                                               encoding:NSUTF8StringEncoding];
    
    //MARK: More official list is at
    //http://theiphonewiki.com/wiki/Models
    //MARK: You may just return machineName. Following is for convenience
    
    NSDictionary *commonNamesDictionary =
    @{
      @"i386":     @"i386 Simulator",
      @"x86_64":   @"x86_64 Simulator",
      
      @"iPhone1,1":    @"iPhone",
      @"iPhone1,2":    @"iPhone 3G",
      @"iPhone2,1":    @"iPhone 3GS",
      @"iPhone3,1":    @"iPhone 4",
      @"iPhone3,2":    @"iPhone 4(Rev A)",
      @"iPhone3,3":    @"iPhone 4(CDMA)",
      @"iPhone4,1":    @"iPhone 4S",
      @"iPhone5,1":    @"iPhone 5(GSM)",
      @"iPhone5,2":    @"iPhone 5(GSM+CDMA)",
      @"iPhone5,3":    @"iPhone 5c(GSM)",
      @"iPhone5,4":    @"iPhone 5c(GSM+CDMA)",
      @"iPhone6,1":    @"iPhone 5s(GSM)",
      @"iPhone6,2":    @"iPhone 5s(GSM+CDMA)",
      
      @"iPhone7,1":    @"iPhone 6+(GSM+CDMA)",
      @"iPhone7,2":    @"iPhone 6(GSM+CDMA)",
      
      @"iPhone8,1":    @"iPhone 6S(GSM+CDMA)",
      @"iPhone8,2":    @"iPhone 6S+(GSM+CDMA)",
      @"iPhone8,4":    @"iPhone SE(GSM+CDMA)",
      @"iPhone9,1":    @"iPhone 7(GSM+CDMA)",
      @"iPhone9,2":    @"iPhone 7+(GSM+CDMA)",
      @"iPhone9,3":    @"iPhone 7(GSM+CDMA)",
      @"iPhone9,4":    @"iPhone 7+(GSM+CDMA)",
      
      @"iPad1,1":  @"iPad",
      @"iPad2,1":  @"iPad 2(WiFi)",
      @"iPad2,2":  @"iPad 2(GSM)",
      @"iPad2,3":  @"iPad 2(CDMA)",
      @"iPad2,4":  @"iPad 2(WiFi Rev A)",
      @"iPad2,5":  @"iPad Mini 1G (WiFi)",
      @"iPad2,6":  @"iPad Mini 1G (GSM)",
      @"iPad2,7":  @"iPad Mini 1G (GSM+CDMA)",
      @"iPad3,1":  @"iPad 3(WiFi)",
      @"iPad3,2":  @"iPad 3(GSM+CDMA)",
      @"iPad3,3":  @"iPad 3(GSM)",
      @"iPad3,4":  @"iPad 4(WiFi)",
      @"iPad3,5":  @"iPad 4(GSM)",
      @"iPad3,6":  @"iPad 4(GSM+CDMA)",
      
      @"iPad4,1":  @"iPad Air(WiFi)",
      @"iPad4,2":  @"iPad Air(GSM)",
      @"iPad4,3":  @"iPad Air(GSM+CDMA)",
      
      @"iPad5,3":  @"iPad Air 2 (WiFi)",
      @"iPad5,4":  @"iPad Air 2 (GSM+CDMA)",
      
      @"iPad4,4":  @"iPad Mini 2G (WiFi)",
      @"iPad4,5":  @"iPad Mini 2G (GSM)",
      @"iPad4,6":  @"iPad Mini 2G (GSM+CDMA)",
      
      @"iPad4,7":  @"iPad Mini 3G (WiFi)",
      @"iPad4,8":  @"iPad Mini 3G (GSM)",
      @"iPad4,9":  @"iPad Mini 3G (GSM+CDMA)",
      
      @"iPod1,1":  @"iPod 1st Gen",
      @"iPod2,1":  @"iPod 2nd Gen",
      @"iPod3,1":  @"iPod 3rd Gen",
      @"iPod4,1":  @"iPod 4th Gen",
      @"iPod5,1":  @"iPod 5th Gen",
      @"iPod7,1":  @"iPod 6th Gen",
      };
    
    NSString *deviceName = commonNamesDictionary[machineName];
    
    if (deviceName == nil) {
        deviceName = machineName;
    }
    
    return deviceName;
}

+(float)deviceDisplaySize{
    
    CGSize scrrenSize = [[UIScreen mainScreen] nativeBounds].size;
    float width = scrrenSize.width;
    float height = scrrenSize.height;
    
    if (width == 640 && height == 1136){
        return 4.0;
    }
    else if (width == 750 && height == 1334){
        return 4.7;
    }
    else if (width == 1242 && height == 2208){
        return 5.5;
    }
    else if (width == 1536 && height == 2048){
        return 7.9;
    }
    else if (width == 1668 && height == 2224 ){
        return 10.5;
    }
    else if(width == 2048 &&  height == 2732){
        return 12.9;
    }
    else if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return 10.5;
    }
    else
        return 3.5;
}


-(void)loadImageFromURL:(NSString*)url DefaultImage:(UIImage*)defaultImage ImageView:(UIImageView*)imageView{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSData * imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        if (imgData) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [imageView setImage:[UIImage imageWithData:imgData]];
            });
        }
        else{
            dispatch_sync(dispatch_get_main_queue(), ^{
                [imageView setImage:defaultImage];
            });
        }
        
    });
    
}

+(BOOL)isImage:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return YES;//@"image/jpeg";
        case 0x89:
            return YES;; // @"image/png";
        case 0x47:
            return YES; //@"image/gif";
        case 0x49:
        case 0x4D:
            return YES; //@"image/tiff";
    }
    return NO;
}


+(NSString *)covertNsNullToNSString:(NSString*)str{
    return [str isEqual:[NSNull null]] || str == nil? @"":str;
}


+(void)setMediaRating:(NSString*)rate ImageView1:(UIImageView*)imgV1 ImageView2:(UIImageView*)imgV2 ImageView3:(UIImageView*)imgV3 ImageView4:(UIImageView*)imgV4 ImageView5:(UIImageView*)imgV5{
    
    if ([rate intValue] == 5) {
        imgV1.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV2.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV3.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV4.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV5.image = [UIImage imageNamed:@"iconvote-yellow"];
    }
    else if ([rate intValue] == 4) {
        imgV1.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV2.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV3.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV4.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV5.image = [UIImage imageNamed:@"iconvote-gray"];
        
    }
    else if ([rate intValue] == 3) {
        imgV1.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV2.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV3.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV4.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV5.image = [UIImage imageNamed:@"iconvote-gray"];
    }
    else if ([rate intValue] == 2) {
        imgV1.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV2.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV3.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV4.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV5.image = [UIImage imageNamed:@"iconvote-gray"];
    }
    else if ([rate intValue] == 1) {
        imgV1.image = [UIImage imageNamed:@"iconvote-yellow"];
        imgV2.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV3.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV4.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV5.image = [UIImage imageNamed:@"iconvote-gray"];
    }
    else{
        imgV1.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV2.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV3.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV4.image = [UIImage imageNamed:@"iconvote-gray"];
        imgV5.image = [UIImage imageNamed:@"iconvote-gray"];
    }
}

+(void)setMediaTypeIcon:(UIImageView*)imageV MediaType:(NSString*)type{
    ///doc = D , video = V, Audio = A, Magazine = M, ePub = E ///
    NSString *imageName = @"";
    type = [type lowercaseString];
    imageV.hidden = NO;
    if ([type isEqualToString:@"d"]) {
        imageName = @"iconbook";
    }
    else if ([type isEqual:@"v"]) {
        imageName = @"iconvideo";
    }
    else if ([type isEqual:@"a"]) {
        imageName = @"iconaudio";
    }
    else if ([type isEqual:@"m"]) {
        imageName = @"iconmagazine";
    }
    else if ([type isEqual:@"e"]) {
        imageName = @"iconepub";
    }
    else if ([type isEqual:@"yu"]) {
        imageName = @"icon_youtube";
    }
    else if ([type isEqual:@"ep"]) {
        imageName = @"icon_pdf";
    }
    else if ([type isEqual:@"el"]) {
        imageName = @"icon_link";
    }
    else if ([type isEqual:@"es"]) {
        imageName = @"icon_sound";
    }
    else if ([type isEqual:@"ev"]) {
        imageName = @"icon_video";
    }
    else if ([type isEqual:@"t"]) {
        imageName = @"icon_material";
    }
    else if ([type isEqual:@"cr"]) {
        imageName = @"icon_tutor";
    }
    else{
        imageV.hidden = YES;
    }
    
    imageV.image = [UIImage imageNamed:imageName];
    
}

+(NSString *)getLocalizeFilterName:(NSString *)filter_id{
    NSString *localName;
    filter_id = [filter_id lowercaseString];
    if ([filter_id isEqualToString:@"d"]) {
        localName = @"BookKind";
    }
    else if ([filter_id isEqual:@"v"]) {
        localName = @"VideoKind";
    }
    else if ([filter_id isEqual:@"a"]) {
        localName = @"AudioKind";
    }
    else if ([filter_id isEqual:@"m"]) {
        localName = @"MagazineKind";
    }
    else if ([filter_id isEqual:@"e"]) {
        localName = @"EpubKind";
    }
    else {
        localName = @"AllKind";
    }
    
    return [self NSLocalizedString:localName];
}

+(void)showAlertViewController:(NSString*)title Message:(NSString*)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {}];
    
    [alert addAction:okButton];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
    //[self presentViewController:alert animated:YES completion:nil];
    
}

+(void)showAlertViewController:(NSString*)title Message:(NSString*)message Controller:(UIViewController*)vc{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                        
                               }];
    
    [alert addAction:okButton];
  //  [vc presentViewController:alert animated:YES completion:nil];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
    //[self presentViewController:alert animated:YES completion:nil];
    
}


+(int)calculateLabelHeight:(NSString*)str UILabelDefaultWidth:(float)defaultWidth UILabelDefaultHeight:(float)defaultHeight Font:(float)fontSize {
    CGSize constrain = CGSizeMake(defaultWidth, FLT_MAX);
    CGSize size = [str sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] constrainedToSize:constrain lineBreakMode:NSLineBreakByCharWrapping];
    
    //NSLog(@"lineCountForLabel : %i",(int)ceil(size.height));
    
    return (ceil(size.height))>defaultHeight? ceil(size.height):defaultHeight;
}


+ (int)lineCountForLabel:(UILabel *)label {
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByCharWrapping];
    
    //NSLog(@"lineCountForLabel : %i",(int)ceil(size.height / label.font.lineHeight));
    
    return ceil(size.height / label.font.lineHeight);
}


+(void)loadMediaCoverImageFromURL:(NSString*)url OrHaveNSData:(NSData *)data withFileName:(NSString *) imageName inDirectory:(NSString *) directoryPath{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        if ([self isImage:data]) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self saveMediaCoverImageFromData:data withFileName:imageName inDirectory:directoryPath];
            });
            
        }
        else{
            NSString* url_ = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url_]];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([self isImage:imgData]) {
                    [self saveMediaCoverImageFromData:imgData withFileName:imageName inDirectory:directoryPath];
                }
            });
            
        }
        
    });
}

+(void) saveImageFromUIImage:(UIImage *) image withFileName:(NSString *) imageName inDirectory:(NSString *) directoryPath {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        // Perform async operation
        if (image) {
            CGSize imgSize = image.size;
            UIImage *resizeImage;
            if (imgSize.width > IMAGE_MAX_WIDTH)
                resizeImage = [self rescaleImage:image scaledToSize:CGSizeMake(IMAGE_MAX_WIDTH, imgSize.height*(IMAGE_MAX_WIDTH/imgSize.width))];
            else
                resizeImage = image;
            
            if (resizeImage) {
                NSError *error;
                
                NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]]];
                
                [UIImagePNGRepresentation(resizeImage) writeToFile:docPath options:NSAtomicWrite error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error)
                        NSLog(@"Image Save Failed: %@", [error localizedDescription]);
//                    else
//                        NSLog(@"Image Save Successfully : %@",imageName);
                });
            }
        }
    });
    
}

+(void) saveMediaCoverImageFromData:(NSData *) fileData withFileName:(NSString *) imageName inDirectory:(NSString *) directoryPath {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        // Perform async operation
        
        if ([fileData length] > 0) {
            UIImage *image = [UIImage imageWithData:fileData];
            CGSize imgSize = image.size;
            UIImage *resizeImage;
            if (imgSize.width > IMAGE_MAX_WIDTH)
                resizeImage = [self rescaleImage:image scaledToSize:CGSizeMake(IMAGE_MAX_WIDTH, imgSize.height*(IMAGE_MAX_WIDTH/imgSize.width))];
            else
                resizeImage = image;
            
            image = nil;
            
            if (resizeImage) {
                NSError *error;
                
                NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]]];
                
                [UIImagePNGRepresentation(resizeImage) writeToFile:docPath options:NSAtomicWrite error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error)
                        NSLog(@"Image Save Failed: %@", [error localizedDescription]);
//                    else{
//                        NSLog(@"Image Save Successfully : %@",imageName);
//                    }
                });
            }
        }
    });
    
}


//+ (UIImage *) imageWithImage:(UIImage *) image scaledToSize:(CGSize) newSize {
//    //UIGraphicsBeginImageContext(newSize);
//    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
//    // Pass 1.0 to force exact pixel size.
//
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
//    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    return newImage;
//}


+ (UIImage *) loadImageWithFilePath:(NSString *) fileFullPath defaultImage:(UIImage *) defaultImage{
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileFullPath])
        return [UIImage imageWithContentsOfFile:fileFullPath];
    else
        return defaultImage;
}

+(NSString*)encodeURL:(NSString*)url{
    return [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}


+(NSString*)currentDate{
    //NSLocale *enLocale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[formatter setLenient:<#(BOOL)#>];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    //NSLog(@"Current Date : %@",dateTime);
    return dateTime;
}

+(NSString*)currentDateTimeWithFormatter:(NSString*)strFormatter{
    //NSLocale *enLocale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:strFormatter];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[formatter setLenient:<#(BOOL)#>];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    //NSLog(@"Current Date : %@",dateTime);
    return dateTime;
}

+(NSString*)getDateTimeForNotification:(NSString*)strDate InputFormat:(NSString*)inputFormat OutputFormat:(NSString*)outputFormat{
    ///input format : 20170907104426 @"yyyyMMddHHmmss"
    NSDateFormatter *dateInputFormat = [[NSDateFormatter alloc] init];
    [dateInputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateInputFormat setDateFormat:inputFormat];
    //[dateInputFormat setTimeZone:gmt];
    NSDate *dte = [dateInputFormat dateFromString:strDate];
    //NSLog(@"input Date : %@", [dte description]);
    
    ///output format @"yyyyMMdd"
    NSDateFormatter *dateOutputFormat = [[NSDateFormatter alloc] init];
    [dateOutputFormat setDateFormat:outputFormat];
    [dateOutputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
    //[dateOutputFormat setTimeZone:gmt];
    
    //NSLog(@"output date : %@",[dateOutputFormat stringFromDate:dte]);
    
    return [dateOutputFormat stringFromDate:dte];
}

+(NSString*)covertDateToFormat:(NSString*)strDate InputFormat:(NSString*)inputFormat OutputFormat:(NSString*)outputFormat{
//    NSLog(@"intput date : %@",strDate);
//    NSLog(@"input format : %@",inputFormat);
//    NSLog(@"out format : %@",outputFormat);
    // convert to date 2017-09-01T14:49:34.2613318+07:00
    //[dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSS'+07:00"];
    //test
    
    ///NSTimeZone *gmt = [NSTimeZone localTimeZone];
   
    ///input format : 20170907104426 @"yyyyMMddHHmmss"
    if ([strDate isEqualToString:@"99999999999999"]) {
        return @"-";
//        strDate = @"99990101010101";
    }
    NSDateFormatter *dateInputFormat = [[NSDateFormatter alloc] init];
    [dateInputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateInputFormat setDateFormat:inputFormat];
    //[dateInputFormat setTimeZone:gmt];
    NSDate *dte = [dateInputFormat dateFromString:strDate];
    //NSLog(@"input Date : %@", [dte description]);
    
    ///output format @"yyyyMMdd"
    NSDateFormatter *dateOutputFormat = [[NSDateFormatter alloc] init];
    [dateOutputFormat setDateFormat:outputFormat];
    [dateOutputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[dateOutputFormat setTimeZone:gmt];
    
    //NSLog(@"output date : %@",[dateOutputFormat stringFromDate:dte]);
    
    return [dateOutputFormat stringFromDate:dte];
}

#pragma mark ------- prepare to SignOut ----------
+(void)showAlertControllerForSignout:(UIViewController*)self_{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ %@ ?",[Utility NSLocalizedString:@"Youwant"],[Utility NSLocalizedString:@"SignOut"]]
                                                                  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* signout = [UIAlertAction
                              actionWithTitle:[Utility NSLocalizedString:@"SignOut"]
                              style:UIAlertActionStyleDestructive
                              handler:^(UIAlertAction * action)
                              {
                                  [self prepare4SignOut:self_];
                              }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    
    [alert addAction:signout];
    [alert addAction:cancel];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
}

+(void)prepare4SignOut:(UIViewController*)self_{
    [CustomActivityIndicator presentActivityIndicatorView];
    _self = self_;
    NSString *deviceCode = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
    NSString *memberId = [DBManager selectMemberID];
    [self logoutService:deviceCode MemberID:[memberId length] > 0? memberId:@""];
}


#pragma mark ------- Logout Service ----------
+(void)logoutService:(NSString*)deviceCode MemberID:(NSString*)memberId{
    //api/DevicePlatform/LogingOut?deviceCode={deviceCode}&memberID={memberID}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutServiceResponse:) name:@"Logout" object:nil];
    ServiceAPI *api = [[ServiceAPI alloc] init];
    [api logingOut:@"Logout" MemberID:memberId];
}

+(void)logoutServiceResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Logout" object:nil];
    //NSLog(@"Logout : %@",notification.userInfo);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        if ([[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200) {
            [DBManager deleteAllRowInMemberTable];
            [DBManager deleteAllRowInLibraryTable];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:LastLeaveApp];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:AutoSyncPdfAnnatationKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //cancel Login by Line App
            [[LineSDKLogin sharedInstance] canLoginWithLineApp];
            //Logout user from facebook
            FBSDKLoginManager *fbManager = [[FBSDKLoginManager alloc] init];
            [fbManager logOut];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            SplashScreen *splash = (SplashScreen*)[storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
            _self.view.window.rootViewController = splash;
        }
        else
            [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"MessageText"] Controller:(UIViewController*)self];
    }
    
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
     _self = nil;
}

#pragma  mark ---- check Media Limit & Rent Limit -------
+(NSArray*)checkUserLimitDayAndMedia{
    if ([DBManager isMember]) {
        NSArray *info =  [NSArray arrayWithArray:[DBManager selectUserDayAndMediaLimit]];
        if ([info count] >= 2) {
            return info;
        }
    }
    return [NSArray arrayWithObjects:[NSString stringWithFormat:@"%i",MaximunDateForBorrow],
                [NSString stringWithFormat:@"%i",MaximunFilesDownload],nil];
}


+(void)getListFiles:(NSString*)dirName{
    //----- LIST ALL FILES -----
    //NSLog(@"LISTING ALL FILES FOUND AT : %@",dirName);
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [[self documentPath] stringByAppendingPathComponent:dirName];
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {
        NSLog(@"File : %@", file);
    }
}

+(void)checkToRemoveExpiredMediaOnShelf{
        //NSMutableArray *expMedia = [DBManager selectExpireMediaOnShelf];
    for (NSArray *expMedia in [DBManager selectExpireMediaOnShelf]) {
        if ([expMedia count] >= 5) {
            [self removeExpiredMediaOnShelf:expMedia[0] CoverPath:expMedia[1] FilePath:expMedia[2] MemberID:expMedia[3] RoomID:expMedia[4]];
        }
    }
    
}
+(void)removeExpiredMediaOnShelf:(NSString *)mediaId CoverPath:(NSString*)coverPath FilePath:(NSString*)filePath MemberID:(NSString *)memberId RoomID:(NSString *)roomID{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
       // NSLog(@"File path : %@",filePath);
        if ([filePath length] > 0 && [DBManager checkFileAvailableToRemove:mediaId FilePath:filePath]){
            // delete media file
            [self removeFileInDirectory:filePath];
        }
        if ([coverPath length] > 0 && [DBManager checkFileAvailableToRemove:mediaId FilePath:coverPath]){
            //delete thumbnail file
            [self removeFileInDirectory:coverPath];
        }
        // delete row
        [DBManager removeMediaOnShelf:mediaId];
        
        //delete PDF Annotation
        NSString *fileAtPath = [NSString stringWithFormat:@"%@/Annotation_%@_%@_%@.txt",PDFAnnotationFolderName,memberId,mediaId,roomID];
        if ([fileAtPath length] > 0)
            [self deletePdfAnotationJson:fileAtPath];
        // delete last open page
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:fileAtPath];
        
        //update notification
        [self updateBookExpireNotification:@[mediaId]];
        
         dispatch_async(dispatch_get_main_queue(), ^{
             [[NSNotificationCenter defaultCenter] postNotificationName:mediaId object:[NSDictionary dictionaryWithObjectsAndKeys:mediaId,@"MediaID", nil]];
         });
        
        
    });
}


// Validate PDF using NSData
+(BOOL)isValidePDF:(NSData *)pdfData {
    BOOL isPDF = false;
    if (pdfData.length >= 1024 ) {
        int startMetaCount = 4, endMetaCount = 5;
        // check pdf data is the NSData with embedded %PDF & %%EOF
        NSData *startPDFData = [NSData dataWithBytes:"%PDF" length:startMetaCount];
        NSData *endPDFData = [NSData dataWithBytes:"%%EOF" length:endMetaCount];
        // startPDFData, endPDFData data are the NSData with embedded in pdfData
        NSRange startRange = [pdfData rangeOfData:startPDFData options:0 range:NSMakeRange(0, 1024)];
        NSRange endRange = [pdfData rangeOfData:endPDFData options:0 range:NSMakeRange(0, pdfData.length)];
        
        if (startRange.location != NSNotFound && startRange.length == startMetaCount && endRange.location != NSNotFound && endRange.length == endMetaCount ) {
            // This assumes the start & end PDFData doesn't have a specific range in file pdf data
            isPDF = true;
            
        } else  {
            isPDF = false;
        }
    }
    NSLog(@"----- isPDF : %d -----",isPDF);
    return isPDF;
}


+(BOOL)isServiceResponseError:(NSDictionary *)jsonString{
    if ([jsonString isKindOfClass:[NSDictionary class]] && [[jsonString objectForKey:@"error"] isKindOfClass:[NSString class]]) {
        NSLog(@"isServiceResponseError : %@",[jsonString objectForKey:@"error"]);
        return YES;
    }
    else
        return NO;
}

+(BOOL)isJsonDictionaryNotNull:(NSDictionary *)jsonDic{
    if (([jsonDic isKindOfClass:[NSDictionary class]] || [jsonDic isKindOfClass:[NSMutableArray class]] || [jsonDic isKindOfClass:[NSArray class]])
        && ![jsonDic isEqual:[NSNull null]] && [jsonDic count]>0){
        //        if ([jsonDic isKindOfClass:[NSDictionary class]] && [[jsonDic objectForKey:@"MessaageText"] length] > 0) {
        //            return NO;
        //        }
        return YES;
    }
    else
        return NO;
}


+(float)bounceHeightForLoadMore{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return 180.f;
    }
    else{
        if([[UIScreen mainScreen]bounds].size.height >= 568)
            return 100.f;
        else
            return 140.f;
    }
}

+(NSString *)getErrorNameFromHttpStatusCode:(int)statusCode{
    NSString *errorName;
    
    switch (statusCode) {
        case 400:
            errorName = @"The server cannot or will not process the request due to an apparent client error.";
            break;
        case 401:
            errorName = @"Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided.";
        default:
            break;
    }
    return errorName;
}

+(BOOL)connectedToNetwork {
    Reachability* reachability = [Reachability reachabilityWithHostName:@"google.com"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable){
        //NSLog(@"------- Internet No Connect --------");
        return NO;
    }
    else if (remoteHostStatus == ReachableViaWWAN){
        //NSLog(@"------- Internet ReachableViaWWAN Connect --------");
        return TRUE;
    }
    else if (remoteHostStatus == ReachableViaWiFi){
        //NSLog(@"------- Internet ReachableViaWiFi Connect --------");
        return TRUE;
    }
    return NO;
}


+(NSString *)generatePassword:(NSString*)fileName{
    if (![fileName isEqual:nil] && [fileName length] > 0) {
        fileName = [fileName stringByDeletingPathExtension];
        NSArray *temp = [fileName componentsSeparatedByString:@"-"];
        //NSLog(@"generatePassword : %@",temp);
        NSMutableString *pass = [[NSMutableString alloc] init];
        for(int i=0; i < [temp count]-1; i++){
            NSString *str = [temp[i] substringToIndex:2];
            //NSLog(@"Pass : %@",str);
            [pass appendString:str];
        }
        
        //NSLog(@"Password : %@",pass);
        return [pass description];
    }
    
    return nil;
    
}

+(NSString *)NSLocalizedString:(NSString *)stringName{
    return  [NSBundle runTimeLocalizedStringForKey:stringName value:nil  table:nil];
}

+(void)setFont:(UIFont *)font WithFontSize:(float)size{
    font = [UIFont fontWithName:mainFont size:size];
}

+(void)addShadow:(UIView*)view WithShadowRadius:(float)shadowRadius{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    view.layer.shadowOpacity = 0.7f;
    view.layer.shadowRadius = shadowRadius;
    view.layer.shadowPath = shadowPath.CGPath;
}


+(NSDictionary *)getDeviceLanguage{
    //NSLog(@"DeviceLanguage :%@",[NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]]);
    return [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
}


+(void)setBookExpireNotification:(SPNotification *)notification{
//    NSLog(@"===== notificationBookCloseToExpire ======");
//    NSLog(@"title : %@",[notification getNotificationTitle]);
//    NSLog(@"body : %@",[notification getNotificationBody]);
//    NSLog(@"Time Present : %i",[notification getNotificationTimePresent]);
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = [notification getNotificationTitle];
    content.body = [notification getNotificationBody];
    content.badge = [NSNumber numberWithInt:[notification getNotificationBadge]];
    content.sound = [UNNotificationSound defaultSound];
    content.userInfo = [notification getNotificationUserInfo];
    
    // Configure the trigger for Show Notification.
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:[notification getNotificationTimePresent]];
    NSDateComponents *triggerDate = [[NSCalendar currentCalendar]
                                     components:NSCalendarUnitYear +
                                     NSCalendarUnitMonth + NSCalendarUnitDay +
                                     NSCalendarUnitHour + NSCalendarUnitMinute +
                                     NSCalendarUnitSecond fromDate:date];
    
    UNCalendarNotificationTrigger* trigger = [UNCalendarNotificationTrigger
                                              triggerWithDateMatchingComponents:triggerDate repeats:NO];
    // Create the request object.
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[notification getNotificationIdentifier] content:content trigger:trigger];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%@", error.localizedDescription);
        } else {
            NSLog(@"Notification added.");
        }
    }];
    
    [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
        //NSLog(@"count : %lu",(unsigned long)requests.count);
        if (requests.count>0) {
            for (UNNotificationRequest * pendingRequest in requests) {
                NSLog(@"%@", pendingRequest.identifier);
            }
        }
        
    }];
}

#pragma mark ------ Update & Remove Notification atfer Book be removed
+(void)updateBookExpireNotification:(NSArray *)identifiers{
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
        //NSLog(@"count : %lu",(unsigned long)requests.count);
        if (requests.count>0) {
            for (UNNotificationRequest * pendingRequest in requests) {
                if ([identifiers containsObject:pendingRequest.identifier]) {
                    //NSLog(@"pendingRequest identifier : %@",pendingRequest.identifier);
                    [center removePendingNotificationRequestsWithIdentifiers:@[pendingRequest.identifier]];
                }
            }
        }
        
    }];
}

+(int)dateToSecond:(NSString *)startDate EndDate:(NSString*)endDate DateFormat:(NSString*)inputFormat{
    //1 day = 86400 sec
//    expDate =    @"20180113143600";
//    NSLog(@"1 borrow date : %@",borrowDate);
//    NSLog(@"1 exp date : %@",expDate);
    NSDateFormatter *dateInputFormat = [[NSDateFormatter alloc] init];
    [dateInputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateInputFormat setDateFormat:inputFormat];
    NSDate *start = [dateInputFormat dateFromString:startDate];
    NSDate *end = [dateInputFormat dateFromString:endDate];
    
    NSTimeInterval interval = [end timeIntervalSinceDate:start];
    //return sec
    return (int)interval;
}

+(void)writeLogFile:(NSString *)logText{
    NSData *data = [logText dataUsingEncoding:NSUTF8StringEncoding];
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/sara-pad-log.txt",
                          documentsDirectory];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        // Add the text at the end of the file.
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:data];
        [fileHandler closeFile];
    } else {
        // Create the file and write text to it.
        [data writeToFile:filePath atomically:YES];
    }
}

+(NSString*)readLogFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/sara-pad-log.txt",
                          documentsDirectory];
    if([[NSData dataWithContentsOfFile:filePath] length] > 0)
        return [NSString stringWithContentsOfFile:filePath];
    else
        return @"NO data";
}


+(void)writePdfAnotationJson:(NSString *)fileName JsonString:(NSString*)jsonStr{
    [self createNewDirectory:PDFAnnotationFolderName isSupportDirectory:FALSE];
    NSString *fileAtPath = [[self documentPath] stringByAppendingPathComponent:fileName];
    NSLog(@"write annotation file Path : %@",fileAtPath);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:fileAtPath]) {
        [fileManager removeItemAtPath:fileAtPath error:nil];
        // Add the text at the end of the file.
//        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileAtPath];
//        [fileHandler seekToEndOfFile];
//        [fileHandler writeData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]];
//        [fileHandler closeFile];
    } //else {
//        // Create the file and write text to it.
//        [[jsonStr dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:YES];
//    }
    [[jsonStr dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:YES];
}

+(NSString *)readPdfAnotationJson:(NSString *)fileName{
     NSString *fileAtPath = [[self documentPath] stringByAppendingPathComponent:fileName];
    //NSLog(@"read annotation file Path : %@",fileAtPath);
    if([[NSData dataWithContentsOfFile:fileAtPath] length] > 0){
        NSError *error;
        NSString *fileContents = [NSString stringWithContentsOfFile:fileAtPath encoding:NSUTF8StringEncoding error:&error];
        //NSLog(@"contents: %@", fileContents);
        if (error){
            NSLog(@"Error reading file: %@", error.localizedDescription);
            return @"NO DATA";
        }
        else
            return fileContents;
    }
    else
        return @"NO DATA";
}

+(void) deletePdfAnotationJson:(NSString *)fileName{
     NSString *fileAtPath = [[self documentPath] stringByAppendingPathComponent:fileName];
    
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:&error];
//    if (error)
//        NSLog(@"Fail to remove file path: %@", [error localizedDescription]);
//    else
//        NSLog(@"Success to remove file path: %@", fileAtPath);
}

+(int)calaulateCollectionItemInRow{
    float size = [self deviceDisplaySize];
    if (size >= 10)
        return 5;
    else if (size == 7.9)
        return 4;
    else
        return 3;
}

+(float)fontSize:(float)originalSize{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return originalSize + 2.f;
    return 17.f;
}

+(NSString *)encodeMediaIDPreset:(NSString *)mediaID{
    return [NSString stringWithFormat:@"%@-Preset",mediaID];
}
+(NSString *)decodeMediaIDPreset:(NSString *)mediaIdPreset{
    //NSLog(@"bf : %@",mediaIdPreset);
    //NSLog(@"af : %@",[mediaIdPreset substringToIndex:[mediaIdPreset length]-7]);
    return [mediaIdPreset substringToIndex:[mediaIdPreset length]-7];
}

+(CGSize)getWidthOfNSString:(NSString *)str FontSize:(UIFont*)font{
    return [str sizeWithAttributes:@{NSFontAttributeName: font}];
}

+(void)clearAllFileInApp{
    [self removeAllFilesInDirectory:sarapadFloderName];
    [self removeAllFilesInDirectory:temporyFloderName];
}

+(CGSize)calculateNavigationbarImageSize:(float)navigationbarHeight isLeft:(BOOL)isLeft{
    if (isLeft)
        return CGSizeMake(navigationbarHeight-8, navigationbarHeight-8);//navigationbarHeight-20;
    
    else
        return CGSizeMake(navigationbarHeight-25, navigationbarHeight-25);
}

+(void)viewAnimationSlideUpAndDown:(UIView*)targetView YPoint:(float)yPoint{
    //slide up is slide to y point
    [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        [targetView setFrame:CGRectMake(0.f, yPoint - targetView.frame.size.height, targetView.frame.size.width, targetView.frame.size.height)];
    } completion:nil];
}

+(void)setUnderlineToUILabel:(UILabel*)label Text:(NSString*)str{
    NSMutableAttributedString *roomName = [[NSMutableAttributedString alloc] initWithString:str];
    [roomName addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, roomName.length)];
    label.attributedText = roomName;
}

+(BOOL) validateEmailFormat:(NSString *)checkString
{
    checkString = [checkString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(void)insertMemberInfo:(MemberInfo *)member VC:(UIViewController*)vc{
    [DBManager insertMemberInfo:member];
    [MemberInfo setIsSignIn:YES];
    
    //reset for sync Member Cloud Shelf
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:LastSyncCloudMedia];
    [userDefault synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashScreen *splash = (SplashScreen*)[storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
    vc.view.window.rootViewController = splash;
}


#pragma GCC diagnostic pop

@end

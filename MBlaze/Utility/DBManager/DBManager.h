//
//  DBManager.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "Room.h"
#import "HistoryModel.h"
#import "OfflineShelf.h"
#import "MediaPreset.h"
#import "MemberInfo.h"

@interface DBManager : NSObject

+ (FMResultSet*)getTableSchema:(NSString*)tableName;
+(BOOL)columnExists:(NSString*)columnName inTableWithName:(NSString*)tableName;
//+(void)addColumnToTable;
+(void)upgradeDatabase;

+(void)createMemberInfoTable;
+(void)insertMemberInfo:(MemberInfo*)member;
+(NSArray *)selecttMemberMenuInfo;
+(NSDictionary*)selectMemberInfo;
+(BOOL)isMember;
+(NSArray*)selectUserDayAndMediaLimit;
+(NSString*)selectMemberID;
+(NSString*)selectMemberPassword;
+(void)updatePassword:(NSString *)password MemberID:(NSString *)memID;

+(void)createRoomInfoTable;
+(void)insertRoomInfo:(Room *)roomDic;
+(Room*)selectDefaultLibraryInfo;
+(NSMutableArray*)selectLibraryList;
+(NSString *)selectLibraryNameByID:(NSString*)libID;
+(NSString *)selectLibraryDateRegisterByID:(NSString*)libID;
+(NSArray *)selectLibraryPicAndName:(NSString*)libID;

+(void)createBookmarkRemindTable;
+(void)insertBookmarkReminder:(NSDictionary *)dictionary;
+(NSString *)selectMarkRemindByPageIndex:(int)pageIndex MediaID:(NSString *)mediaId;
+(void)deleteMarkRemindAtPageIndex:(int)pageIndex MediaID:(NSString *)mediaId;
+(BOOL)isHaveMarkRemindAtPageIndex:(int)pageIndex MediaID:(NSString *)mediaId;
+(void)deleteMarkRemindWhenMediaExp:(NSString*)mediaID;
+(void)selectAllMarkRemindByMediaID:(NSString *)mediaId;

//+(void)createHistoryTable;
//+(void)insertHistoryInfo:(HistoryModel *)hData;
//+(NSMutableArray*)selectHistoryInfo;

+(void)deleteAllRowInMemberTable;
+(void)deleteAllRowInLibraryTable;
+(void)deleteAllRowInHistoryTable;
+(void)deleteAllRowInMyShelfTable;
+(void)deleteAllRowInPresetTable;

+(void)createOfflineShelfTable;
+(void)insertOfflineShelf:(OfflineShelf *)shelfData isPreset:(BOOL)isPreset;
+(void)syncForInsertUpdateMemberCloudMedia:(OfflineShelf *)shelfData;

+(NSMutableArray *)selectLibraryNameOnMyShelfId:(NSString*)shelfId MemberID:(NSString*)memberID;
+(NSMutableArray *)selectExpireMediaOnShelf;
+(NSArray *)selectMediaOnShelfByMediaID:(NSString *)mediaID;
+(NSMutableArray *)selectPresetMediaOnMyShelfId:(NSString *)roomID MemberID:(NSString*)memberID;

+(BOOL)isMediaExpired:(NSString*)mediaID;
+(BOOL)isHaveUserMediaOnShelf;
+(BOOL)checkFileAvailableToRemove:(NSString*)mediaID FilePath:(NSString*)path;

+(void)removeMediaOnShelf:(NSString*)mediaID;
+(void)removeMediaOnShelfFilePath:(NSString*)mediaID;

+(NSString*)selectMediaFilePath:(NSString*)mediaID;

+(NSInteger)selectTotalMediaOnShelf:(NSString*)memberID;
+(void)recheckMediaExpirOnShelfAtDirectoryName:(NSString*)memberID InDirectoryPath:(NSString*)dirpath LibraryID:(NSString*)roomID;
+(NSMutableArray *)selectMediaOnMyShelfId:(NSString *)shelfId MemberID:(NSString*)memberID;
+(int)getCountAvailableShelf;


+(void)createMediaPresetTable;
+(void)insertMediaPreset:(MediaPreset *)preSet;
+(void)selectMediaPresetBySubjectID:(NSString*)subjectID SelectAllRow:(BOOL)selectAll;
//+(NSMutableArray *)selectPresetSubjectItem:(NSString*)subjectID;
+(NSMutableArray *)selectPresetRoomItems:(NSString*)memberID;
+(NSMutableArray *)selectPresetSubjectItems:(NSString*)roomID;
+(MediaPreset *)selectMediaPresetInfoBySubjectId:(NSString*)subjectId;
+(NSMutableDictionary *)selectMediaPresetInfoByRoomId:(NSString*)roomId;
+(BOOL)isMediaPresetRoomExist:(NSString *)roomId;

+(void)dropMemberTable;
+(void)dropLibraryTable;
+(void)dropHistoryTable;
+(void)dropMyShelfTable;
+(void)dropPresetTable;


@end

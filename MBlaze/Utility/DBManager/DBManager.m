//
//  DBManager.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "DBManager.h"
#import "Room.h"
#import "HistoryModel.h"
#import "Utility.h"
#import "ConstantValues.h"
#import "MemberInfo.h"
#import "MediaPreset.h"
#import "MemberInfo.h"

@implementation DBManager

#define DATABASE_NAME @"SaraPad.sqlite"

#define MemberInfoTable  @"MEMBER_INFO_TABLE"
#define RoomInfoTable  @"ROOM_INFO_TABLE"
#define HistoryTable @"HISTORY_TABLE"
#define OfflineShelfTable @"OfflineShelf_TABLE"
#define MediaPresetTable @"MediaPreset_TABLE"

#define MediaBookmarksTable @"MediaBookmarks_TABLE"

#define DATABASE_VERSION 1.0

#pragma mark - get Database path
+(NSString *) databasePath{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    return  [documentsDir   stringByAppendingPathComponent:DATABASE_NAME];
}

+(BOOL) openDB:(FMDatabase *)database{
    return [database open];
}

+(void) closeDB:(FMDatabase *)database{
    [database close];
}

/*
 get table schema: result colums: cid[INTEGER], name,type [STRING], notnull[INTEGER], dflt_value[],pk[INTEGER]
 */
+ (FMResultSet*)getTableSchema:(NSString*)tableName {
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        //result colums: cid[INTEGER], name,type [STRING], notnull[INTEGER], dflt_value[],pk[INTEGER]
        FMResultSet *tabeInfo = [database executeQuery:[NSString stringWithFormat: @"pragma table_info('%@')", tableName]];
        
        return tabeInfo;
    }
    [database close];
    
    return nil;
}

+(BOOL)columnExists:(NSString*)columnName inTableWithName:(NSString*)tableName {
    BOOL returnBool = NO;
    tableName  = [tableName lowercaseString];
    columnName = [columnName lowercaseString];
    
    FMResultSet *rs = [self getTableSchema:tableName];
    //check if column is present in table schema
    while ([rs next]) {
        if ([[[rs stringForColumn:@"name"] lowercaseString] isEqualToString:columnName]) {
            returnBool = YES;
            break;
        }
    }
    //If this is not done FMDatabase instance stays out of pool
    [rs close];

    return returnBool;
}

+(void)addColumnToTable:(NSString*)checkCulumn Table:(NSString*)tableName Sql:(NSString*)sql{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        BOOL existColumn = [self columnExists:checkCulumn inTableWithName:tableName];
        if (!existColumn) {
            [database executeUpdate:sql];
        }
        //BOOL result = [database executeUpdate:script];
        //NSLog(@"addColumnToTable -->> %@ -->> %s",OfflineShelfTable,result ? "YES" : "NO");
    }
    [database close];
}

#pragma mark ----- upgrade database ----
+(void)upgradeDatabase{
    // add SubjectID column at OfflineShelfTable
    NSString *script = [NSString stringWithFormat:@"ALTER TABLE %@ ADD SubjectID TEXT DEFAULT NULL ",OfflineShelfTable];
    [self addColumnToTable:@"SubjectID" Table:OfflineShelfTable Sql:script];
    
//    //add new column at MemberInfoTable
//    //SocialID, SocialTokenID, ProfileURL
//    for (NSString *columnID in @[@"SocialID",@"SocialTokenID",@"ProfileURL"]) {
//        script = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ TEXT DEFAULT NULL ",MemberInfoTable,columnID];
//        [self addColumnToTable:columnID Table:MemberInfoTable Sql:script];
//    }
    
}

#pragma mark ----- Member Info ----
+(void)createMemberInfoTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "MEMBERID TEXT PRIMARY KEY DEFAULT NULL, "
                                  "USERNAME TEXT DEFAULT NULL, "
                                  "PASSWORD TEXT DEFAULT NULL, "
                                  "EMAIL TEXT DEFAULT NULL, "
                                  "TELEPHONE TEXT DEFAULT NULL, "
                                  "DISPLAYNAME TEXT DEFAULT NULL, "
                                  "DATEOFBIRTH TEXT DEFAULT NULL, "
                                  "PROFILEURL TEXT DEFAULT NULL, "
                                  "LastSignin TEXT DEFAULT NULL, "
                                  "ETC TEXT DEFAULT NULL ) ",MemberInfoTable];
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",MemberInfoTable,result ? "YES" : "NO");
    }
    [database close];

}

+(void)insertMemberInfo:(MemberInfo *)member{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
         NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MEMBERID) FROM %@ where MEMBERID='%@'",MemberInfoTable,[member getMemberID]]];
        
        if (rowCount > 0) {
            ///// update ///
            NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET DISPLAYNAME='%@', EMAIL='%@', TELEPHONE='%@', DATEOFBIRTH='%@', LastSignin='%@', ETC='%@' where MEMBERID='%@'",MemberInfoTable,[member getDisplayName],[member getEmail],[member getTelephone],[member getDateOfBirth],[member getLastSignin],[member getEtc],[member getMemberID]];
            
            [database executeUpdate:sql,nil];
//            BOOL result = [database executeUpdate:sql,nil];
//            NSLog(@"Update -->> %@ -->> %s",MemberInfoTable,result ? "YES" : "NO");
        } else {
            //// insert ////
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (MEMBERID, USERNAME, PASSWORD, EMAIL, TELEPHONE, DISPLAYNAME, DATEOFBIRTH, PROFILEURL, LastSignin, ETC) VALUES ('%@',  '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",MemberInfoTable,[member getMemberID],[member getUserLogin],[member getPassword],[member getEmail],[member getTelephone],[member getDisplayName],[member getDateOfBirth],[member getProfileURL],[member getLastSignin],[member getEtc]];
             [database executeUpdate:sql, nil];
//            BOOL result = [database executeUpdate:sql, nil];
//            NSLog(@"Insert -->> %@ -->> %s -->> ID : %@",MemberInfoTable,result ? "YES" : "NO",[member getMemberID]);
        }
        
        
    }
    
    [database close];

}

+(NSArray *)selecttMemberMenuInfo{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSArray *arraData;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT DISPLAYNAME,EMAIL,LastSignin FROM %@ limit 1",MemberInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            arraData = [NSArray arrayWithObjects:[results stringForColumn:@"DISPLAYNAME"],[results stringForColumn:@"EMAIL"],[results stringForColumn:@"LastSignin"], nil];
            //NSLog(@"--- member id : %@ ----",[results stringForColumn:@"MEMBERID"]);
            //NSLog(@"--- member name : %@ ----",[results stringForColumn:@"USERNAME"]);
            
        }
    }
    [database close];
    
    if (!([arraData count]>0)) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy, HH:mm:ss"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        //NSLog(@"Current Date : %@",[formatter stringFromDate:[NSDate date]]);
       arraData = [NSArray arrayWithObjects:[Utility NSLocalizedString:@"NoName"],@" - ",[formatter stringFromDate:[NSDate date]], nil];
    }
    
    return arraData;
}

+(NSDictionary*)selectMemberInfo{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSDictionary *memberInfoDic = [[NSDictionary alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ limit 1",MemberInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {

            memberInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:
                             [results stringForColumn:@"MEMBERID"],@"MEMBERID",
                             [results stringForColumn:@"USERNAME"],@"USERNAME",
                             [results stringForColumn:@"EMAIL"],@"EMAIL",
                             [results stringForColumn:@"TELEPHONE"],@"TELEPHONE",
                             [results stringForColumn:@"DISPLAYNAME"],@"DISPLAYNAME",
                             [results stringForColumn:@"DATEOFBIRTH"],@"DATEOFBIRTH",
                             [results stringForColumn:@"PROFILEURL"],@"PROFILEURL",
                             [results stringForColumn:@"LastSignin"],@"LastSignin",
                             [results stringForColumn:@"ETC"],@"ETC",nil];

            //NSLog(@"--- member id : %@ ----",[results stringForColumn:@"MEMBERID"]);
            //NSLog(@"--- member name : %@ ----",[results stringForColumn:@"USERNAME"]);

        }
    }
    [database close];

    return memberInfoDic;
}

+(BOOL)isMember{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isMember = NO;
    if ([self openDB:database]){
         NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MEMBERID) FROM %@ ",MemberInfoTable]];
        if (rowCount > 0) {
            isMember = YES;
        }
//        ///get member count ///
//        FMResultSet *mCountResults = [database executeQuery:[NSString stringWithFormat:@"SELECT count(*) as mCount from %@ ",MemberInfoTable]];
//        while([mCountResults next]) {
//            //NSLog(@"mCount : %@",[mCountResults stringForColumn:@"mCount"]);
//            isMember = [[mCountResults stringForColumn:@"mCount"] intValue]>0 ? YES:NO;
//        }
    }
    [database close];
    
    return isMember;
}

+(NSArray*)selectUserDayAndMediaLimit{
    
//    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
//    NSArray * limit;
//    if ([self openDB:database]){
//        NSString* sql = [NSString stringWithFormat:@"SELECT LimitDayOfRenting, LimitMediaOfRenting FROM %@ ",MemberInfoTable];
//        FMResultSet *results = [database executeQuery:sql];
//        while([results next]) {
//            limit = [NSArray arrayWithObjects:![[results stringForColumn:@"LimitDayOfRenting"] isEqualToString:@"0"]? [results stringForColumn:@"LimitDayOfRenting"] : [NSString stringWithFormat:@"%i",MaximunDateForBorrow],
//                     ![[results stringForColumn:@"LimitMediaOfRenting"] isEqualToString:@"0"]? [results stringForColumn:@"LimitMediaOfRenting"] : [NSString stringWithFormat:@"%i",MaximunFilesDownload], nil];
//        }
//    }
//    [database close];
//
//    return limit;
    return [NSArray arrayWithObjects:[NSString stringWithFormat:@"%i",MaximunDateForBorrow],[NSString stringWithFormat:@"%i",MaximunFilesDownload], nil];
}

+(NSString*)selectMemberPassword{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString * memberPass;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT PASSWORD FROM %@ ",MemberInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            memberPass = [results stringForColumn:@"PASSWORD"];
        }
    }
    [database close];
    
    return memberPass;
    
}

+(void)updatePassword:(NSString *)password MemberID:(NSString *)memID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET PASSWORD='%@' where MEMBERID='%@'",MemberInfoTable,password,memID];
        [database executeUpdate:sql,nil];
        //BOOL result = [database executeUpdate:sql,nil];
        //NSLog(@"Update Password -->> %@ -->> %s",MemberInfoTable,result ? "YES" : "NO");
    }
    [database close];
}

+(NSString*)selectMemberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString * memberID;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT MEMBERID FROM %@ ",MemberInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
           memberID = [results stringForColumn:@"MEMBERID"];
        }
    }
    [database close];
    
    return memberID;

}

#pragma mark ------- ROOM INFO Table --------
+(void)createRoomInfoTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "ROOMID TEXT PRIMARY KEY DEFAULT NULL, "
                                  "ROOMCODE TEXT DEFAULT NULL, "
                                  "ROOMNAME TEXT DEFAULT NULL, "
                                  "ROOMTYPECODE TEXT DEFAULT NULL, "
                                  "ROOMTYPEID TEXT DEFAULT NULL, "
                                  "ROOMTYPENAME TEXT DEFAULT NULL, "
                                  "SYMBOL TEXT DEFAULT NULL, "
                                  "MessaageText TEXT DEFAULT NULL, "
                                  "rDEFAULT TEXT DEFAULT NULL, "
                                  "ROOMINDEX TEXT DEFAULT NULL, "
                                  "DATE TEXT DEFAULT NULL) ",RoomInfoTable];
        
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",RoomInfoTable,result ? "YES" : "NO");
    }
    [database close];
    
}


+(void)insertRoomInfo:(Room *)roomDic{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
 
            //// insert ////
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (ROOMID, ROOMCODE, ROOMNAME, ROOMTYPECODE, ROOMTYPEID, ROOMTYPENAME, SYMBOL, MessaageText, rDEFAULT, ROOMINDEX, DATE) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",RoomInfoTable,
                             [roomDic getRoomID],
                             [roomDic getRoomCode],
                             [roomDic getRoomName],
                             [roomDic getRoomTypeCode],
                             [roomDic getRoomTypeID],
                             [roomDic getRoomTypeName],
                             [roomDic getSymbol],
                             [roomDic getMessaageText],
                             [roomDic getDefault],
                             [roomDic getRoomIndex],
                             [roomDic getRoomDateRegister]];
            
            ////NSLog(@"sql : %@",sql);
            [database executeUpdate:sql, nil];
            //BOOL result = [database executeUpdate:sql, nil];
            //NSLog(@"Room Name : %@",[roomDic getRoomName]);
            //NSLog(@"Insert -->> %@ -->> %s -->> ID : %@",RoomInfoTable,result ? "YES" : "NO",[roomDic getRoomID]);
    }
    [database close];
    
}

+(Room*)selectDefaultLibraryInfo{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    Room *roomInfoDic = [[Room alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT ROOMID, ROOMCODE, ROOMNAME, ROOMTYPECODE, ROOMTYPEID, ROOMTYPENAME, SYMBOL, MessaageText, rDEFAULT, ROOMINDEX, DATE FROM %@ where max(cast(DATE as INTEGER)) limit 1",RoomInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            [roomInfoDic Room:[results stringForColumn:@"ROOMID"]
                     RoomCode:[results stringForColumn:@"ROOMCODE"]
                     RoomName:[results stringForColumn:@"ROOMNAME"]
                       Symbol:[results stringForColumn:@"SYMBOL"]
                      Default:[results stringForColumn:@"rDEFAULT"]
                   RoomTypeID:[results stringForColumn:@"ROOMTYPEID"]
                 RoomTypeCode:[results stringForColumn:@"ROOMTYPECODE"]
                 RoomTypeName:[results stringForColumn:@"ROOMTYPENAME"]
                 MessaageText:[results stringForColumn:@"MessaageText"]
                        Index:[results stringForColumn:@"ROOMINDEX"]
                 DateRegister:[results stringForColumn:@"DATE"]];
        
            //NSLog(@"--- room id : %@ ----",[results stringForColumn:@"ROOMID"]);
            //NSLog(@"--- room name : %@ ----",[results stringForColumn:@"ROOMNAME"]);
            
        }
    }
    [database close];
    
    return roomInfoDic;
}

+(NSMutableArray*)selectLibraryList{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfLib = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@",RoomInfoTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            Room *roomInfo = [[Room alloc] init];
            [roomInfo Room:[results stringForColumn:@"ROOMID"]
                  RoomCode:[results stringForColumn:@"ROOMCODE"]
                  RoomName:[results stringForColumn:@"ROOMNAME"]
                    Symbol:[results stringForColumn:@"SYMBOL"]
                   Default:[results stringForColumn:@"rDEFAULT"]
                RoomTypeID:[results stringForColumn:@"ROOMTYPEID"]
              RoomTypeCode:[results stringForColumn:@"ROOMTYPECODE"]
              RoomTypeName:[results stringForColumn:@"ROOMTYPENAME"]
              MessaageText:[results stringForColumn:@"MessaageText"]
                     Index:[results stringForColumn:@"ROOMINDEX"]
              DateRegister:[results stringForColumn:@"DATE"]];
            
            //NSLog(@"--- room id : %@ ----",[results stringForColumn:@"ROOMID"]);
            //NSLog(@"--- room name : %@ ----",[results stringForColumn:@"ROOMNAME"]);
            
            [arrayOfLib addObject:roomInfo];
            
        }
    }
    [database close];
    
    return arrayOfLib;
}


+(NSString *)selectLibraryNameByID:(NSString*)libID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString *name;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT ROOMNAME FROM %@ where ROOMID='%@' ",RoomInfoTable,libID];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            name = [results stringForColumn:@"ROOMNAME"];
            //NSLog(@"--- room id : %@ ----",[results stringForColumn:@"ROOMID"]);
            //NSLog(@"--- room name : %@ ----",[results stringForColumn:@"ROOMNAME"]);
            
        }
    }
    [database close];
    
    return name;

}

+(NSString *)selectLibraryDateRegisterByID:(NSString*)libID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString *date;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT ROOMNAME, DATE FROM %@ where ROOMID='%@' ",RoomInfoTable,libID];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            date = [results stringForColumn:@"DATE"];
            //NSLog(@"--- room name : %@ ----",[results stringForColumn:@"ROOMNAME"]);
            //NSLog(@"--- room date regist : %@ ----",[results stringForColumn:@"DATE"]);
            
        }
    }
    [database close];
    
    return date;
    
}

+(NSArray *)selectLibraryPicAndName:(NSString*)libID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSArray *arrOfLib = nil;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT ROOMNAME, SYMBOL FROM %@ where ROOMID='%@' ",RoomInfoTable,libID];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            arrOfLib = [NSArray arrayWithObjects:[results stringForColumn:@"ROOMNAME"],[results stringForColumn:@"SYMBOL"], nil];
            //NSLog(@"--- room id : %@ ----",[results stringForColumn:@"SYMBOL"]);
            //NSLog(@"--- room name : %@ ----",[results stringForColumn:@"ROOMNAME"]);
            
        }
    }
    [database close];
    
    return arrOfLib;

}

#pragma mark ------- History -------------
+(void)createHistoryTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "MediaID TEXT PRIMARY KEY DEFAULT NULL, "
                                  "MemberID TEXT DEFAULT NULL, "
                                  "LibraryID TEXT DEFAULT NULL, "
                                  "MediaTitle TEXT DEFAULT NULL, "
                                  "MediaAuthor TEXT DEFAULT NULL, "
                                  "CategoryName TEXT DEFAULT NULL, "
                                  "CoverUrl TEXT DEFAULT NULL, "
                                  "Date TEXT DEFAULT NULL) ",HistoryTable];
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",HistoryTable,result ? "YES" : "NO");
    }
    [database close];
}

+(void)insertHistoryInfo:(HistoryModel *)hData{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        
         NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MediaID) FROM %@ where MediaID='%@' ",HistoryTable,[hData getMediaID]]];
        if (rowCount > 0) {
            ///// update ///
            NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET MemberID='%@', LibraryID='%@', MediaTitle='%@', MediaAuthor='%@', CategoryName='%@', CoverUrl='%@', Date='%@' Where MediaID='%@' ",HistoryTable,
                             [hData getMemberID],
                             [hData getLibraryID],
                             [hData getMediaTitle],
                             [hData getMediaAuthor],
                             [hData getCategoryName],
                             [hData getCoverUrl],
                             [hData getWatchedDate],
                             [hData getMediaID]];
            
            [database executeUpdate:sql,nil];
           // BOOL result = [database executeUpdate:sql,nil];
            //NSLog(@"Update -->> %@ -->> %s",HistoryTable,result ? "YES" : "NO");
        }
        else{
            //// insert ////
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (MediaID, MemberID, LibraryID, MediaTitle, MediaAuthor, CategoryName, CoverUrl, Date) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",HistoryTable,
                             [hData getMediaID],
                             [hData getMemberID],
                             [hData getLibraryID],
                             [hData getMediaTitle],
                             [hData getMediaAuthor],
                             [hData getCategoryName],
                             [hData getCoverUrl],
                             [hData getWatchedDate]];
            [database executeUpdate:sql, nil];
            //BOOL result = [database executeUpdate:sql, nil];
            //NSLog(@"Insert -->> %@ -->> %s -->> ID : %@",HistoryTable,result ? "YES" : "NO",[hData getMediaID]);
        }
    
    }
    
    [database close];
    
}

+(NSMutableArray*)selectHistoryInfo{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *hArray = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT MediaID, MemberID, LibraryID, MediaTitle, MediaAuthor, CategoryName, CoverUrl, Date FROM %@",HistoryTable];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            HistoryModel *history = [[HistoryModel alloc] init];
            [history HistoryWithMediaID:[results stringForColumn:@"MediaID"]
                               MemberID:[results stringForColumn:@"MemberID"]
                              LibraryID:[results stringForColumn:@"LibraryID"]
                             MediaTitle:[results stringForColumn:@"MediaTitle"]
                            MediaAuthor:[results stringForColumn:@"MediaAuthor"]
                           CategoryName:[results stringForColumn:@"CategoryName"]
                               CoverURL:[results stringForColumn:@"CoverUrl"]
                                   Date:[results stringForColumn:@"Date"]
                               isReturn:NO];
            
            [hArray addObject:history];
            //NSLog(@"--- MemberID : %@ ----",[results stringForColumn:@"MediaID"]);
            //NSLog(@"--- MediaTitle : %@ ----",[results stringForColumn:@"MediaTitle"]);
            
        }
    }
    [database close];

    return hArray;
}

#pragma mark ------- Offline Shelf ----------
+(void)createOfflineShelfTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "MediaID TEXT PRIMARY KEY DEFAULT NULL, "
                                  "MemberID TEXT DEFAULT NULL, "
                                  "ShelfID TEXT DEFAULT NULL, "
                                  "LibraryID TEXT DEFAULT NULL, "
                                  "LibraryName TEXT DEFAULT NULL, "
                                  "MediaTitle TEXT DEFAULT NULL, "
                                  "MediaAuthor TEXT DEFAULT NULL, "
                                  "MediaType TEXT DEFAULT NULL, "
                                  "CoverURL TEXT DEFAULT NULL, "
                                  "CoverPath TEXT DEFAULT NULL, "
                                  "FileContentPath TEXT DEFAULT NULL, "
                                  "FileContentUrl TEXT DEFAULT NULL, "
                                  "DateBorrow TEXT DEFAULT NULL, "
                                  "DateExp TEXT DEFAULT NULL, "
                                  "OriginalFileName TEXT DEFAULT NULL, "
                                  "SubjectID TEXT DEFAULT NULL) ",OfflineShelfTable];
        
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",OfflineShelfTable,result ? "YES" : "NO");
    }
    [database close];
}

+(void)insertOfflineShelf:(OfflineShelf *)shelfData isPreset:(BOOL)isPreset{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        //NSLog(@"Row count : %lu",(unsigned long)count);
        if (isPreset) {
             NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MediaID) FROM %@ where MediaID='%@' and ShelfID ='2' ",OfflineShelfTable,[shelfData getMediaID]]];
            
            NSString *sql;
            if (rowCount>0 && [[shelfData getDateExp] isEqualToString:MediaPresetToRemove]) {
                [database executeUpdate:[NSString stringWithFormat:@"Delete FROM %@ where MediaID='%@' and ShelfID ='2' ",OfflineShelfTable,[shelfData getMediaID]]];
                //check for remove Preset Subject
                 FMResultSet *result = [database executeQuery:[NSString stringWithFormat:@"SELECT count(*) as mCount FROM %@ where ShelfID ='2' and SubjectID='%@' ",OfflineShelfTable,[shelfData getSubjectID]]];
                while ([result next]) {
                    //NSLog(@"count :%ld",(long)[[result stringForColumn:@"mCount"] integerValue]);
                    if (!([[result stringForColumn:@"mCount"] integerValue] > 0)) {
                        //delete preset subject data
                        [database executeUpdate:[NSString stringWithFormat:@"Delete FROM %@ where SUBJECTID='%@' ",MediaPresetTable,[shelfData getSubjectID]]];
                    }
                }
            }
            else if (rowCount>0){
                if ([[shelfData getDateBorrow] length] > 0 && [[shelfData getMediaFilePath] length] > 0) {
                    sql = [NSString stringWithFormat:@"UPDATE %@ SET CoverPath='%@', FileContentPath='%@', FileContentUrl='%@', DateBorrow='%@', DateExp='%@', OriginalFileName='%@' Where MediaID='%@' ",
                           OfflineShelfTable,
                           [shelfData getMediaCoverPath],
                           [shelfData getMediaFilePath],
                           [shelfData getMediaFileUrl],
                           [shelfData getDateBorrow],
                           [shelfData getDateExp],
                           [shelfData getMediaOriginalName],
                           [shelfData getMediaID]];
                    [database executeUpdate:sql,nil];
                }
                else{
                    sql = [NSString stringWithFormat:@"UPDATE %@ SET MemberID='%@', ShelfID='%@', LibraryID='%@' , LibraryName='%@', MediaTitle='%@' , MediaAuthor='%@' , MediaType='%@' , CoverURL='%@' , CoverPath='%@' , DateExp='%@',  SubjectID='%@' Where MediaID='%@' ",
                           OfflineShelfTable,
                           [shelfData getMemberID],
                           [shelfData getShelfID],
                           [shelfData getLibraryID],
                           [shelfData getLibraryName],
                           [shelfData getMediaTitle],
                           [shelfData getMediaAuthor],
                           [shelfData getMediaType],
                           [shelfData getMediaCoverURL],
                           [shelfData getMediaCoverPath],
                           [shelfData getDateExp],
                           [shelfData getSubjectID],
                           [shelfData getMediaID]];
                    [database executeUpdate:sql,nil];
                }
                
            }
            else if (![[shelfData getDateExp] isEqualToString:MediaPresetToRemove]){
                [self insert:shelfData FMDatabase:database];
            }
        }
        else{
             NSUInteger rowCount_ = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MediaID) FROM %@ where MediaID='%@' and (ShelfID !='2') ",OfflineShelfTable,[shelfData getMediaID]]];
            if (rowCount_>0) {
                NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET MemberID='%@', ShelfID='%@', LibraryID='%@' , LibraryName='%@', MediaTitle='%@' , MediaAuthor='%@' , MediaType='%@' , CoverURL='%@' , CoverPath='%@', FileContentPath='%@', FileContentUrl='%@', DateBorrow='%@', DateExp='%@', OriginalFileName='%@' , SubjectID='%@' Where MediaID='%@' ",
                                 OfflineShelfTable,[shelfData getMemberID],
                                 [shelfData getShelfID],[shelfData getLibraryID],
                                 [shelfData getLibraryName],[shelfData getMediaTitle],
                                 [shelfData getMediaAuthor],[shelfData getMediaType],
                                 [shelfData getMediaCoverURL],[shelfData getMediaCoverPath],
                                 [shelfData getMediaFilePath],[shelfData getMediaFileUrl],
                                 [shelfData getDateBorrow],[shelfData getDateExp],
                                 [shelfData getMediaOriginalName],[shelfData getSubjectID],
                                 [shelfData getMediaID]];
                [database executeUpdate:sql,nil];
            }
            else{
                [self insert:shelfData FMDatabase:database];
            }
        }
    }
    [database close];
}

+(void)insert:(OfflineShelf*)shelfData FMDatabase:(FMDatabase*)database{
    // insert //
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (MediaID, MemberID, ShelfID, LibraryID, LibraryName, MediaTitle, MediaAuthor, MediaType, CoverURL, CoverPath, FileContentPath, FileContentUrl, DateBorrow, DateExp, OriginalFileName, SubjectID) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",OfflineShelfTable,
                     [shelfData getMediaID],
                     [shelfData getMemberID],
                     [shelfData getShelfID],
                     [shelfData getLibraryID],
                     [shelfData getLibraryName],
                     [shelfData getMediaTitle],
                     [shelfData getMediaAuthor],
                     [shelfData getMediaType],
                     [shelfData getMediaCoverURL],
                     [shelfData getMediaCoverPath],
                     [shelfData getMediaFilePath],
                     [shelfData getMediaFileUrl],
                     [shelfData getDateBorrow],
                     [shelfData getDateExp],
                     [shelfData getMediaOriginalName],
                     [shelfData getSubjectID]];
    [database executeUpdate:sql, nil];
    //BOOL result = [database executeUpdate:sql, nil];
    //NSLog(@"Insert -->> %@ -->> %s",OfflineShelfTable,result ? "YES" : "NO");
}

+(void)syncForInsertUpdateMemberCloudMedia:(OfflineShelf *)shelfData{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(MediaID) FROM %@ where MediaID='%@'",OfflineShelfTable,[shelfData getMediaID]]];
        //NSLog(@"Row count : %lu",(unsigned long)count);
        //int currDate = [[Utility currentDate] intValue];
        if (rowCount>0) {
            FMResultSet *results = [database executeQuery:[NSString stringWithFormat:@"SELECT DateBorrow, DateExp FROM %@ where MediaID='%@' ",OfflineShelfTable,[shelfData getMediaID]]];
            while ([results next]) {
                if ([[shelfData getDateExp] intValue] > [[results stringForColumn:@"DateExp"] intValue] || [[shelfData getDateBorrow] intValue] > [[results stringForColumn:@"DateBorrow"] intValue]) {
                    //NSLog(@"update");
                    ///// update ///
                    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET DateBorrow='%@', DateExp='%@' Where MediaID='%@' ",OfflineShelfTable,[shelfData getDateBorrow],[shelfData getDateExp],[shelfData getMediaID]];
                    [database executeUpdate:sql,nil];
                    
                }
            }
            
        }
        else if (!(rowCount>0)){
            [self insert:shelfData FMDatabase:database];
        }
    }
    [database close];
    
}

+(int)getCountAvailableShelf{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    int shelfCount = 0;
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT count(DISTINCT ShelfID) as mCount FROM %@ where cast(DateExp as INTEGER) >= %d ",OfflineShelfTable,(int)currDate];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"shelfCount : %@",[results stringForColumn:@"mCount"]);
           shelfCount = [[results stringForColumn:@"mCount"] intValue];
        }
    }
    [database close];
    
    return shelfCount;
}

+(NSMutableArray *)selectExpireMediaOnShelf{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfExpBook = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT MediaID, MemberID, CoverPath, FileContentPath, LibraryID FROM %@ where cast(DateExp as INTEGER) < %d",OfflineShelfTable,(int)currDate];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            
            [arrayOfExpBook addObject:@[[results stringForColumn:@"MediaID"],
                                        [results stringForColumn:@"CoverPath"],
                                        [results stringForColumn:@"FileContentPath"],
                                        [results stringForColumn:@"MemberID"],
                                        [results stringForColumn:@"LibraryID"]]];
            
//            NSLog(@"--------- ExpireMediaOnShelf -----------");
//            NSLog(@"--- MediaTitle : %@ ----",[results stringForColumn:@"MediaTitle"]);
//            NSLog(@"--- LibraryName : %@ ----",[results stringForColumn:@"LibraryName"]);
//            NSLog(@"--- DateExp : %@ ----",[results stringForColumn:@"DateExp"]);
        }
    }
    [database close];
    
    return arrayOfExpBook;
}

+(NSArray *)selectMediaOnShelfByMediaID:(NSString *)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSArray *arrayOfExpBook = [[NSArray alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT  MediaID, MemberID, CoverPath, FileContentPath, LibraryID FROM %@ where MediaID = '%@' ",OfflineShelfTable,mediaID];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            arrayOfExpBook = @[[results stringForColumn:@"MediaID"],
                               [results stringForColumn:@"CoverPath"],
                               [results stringForColumn:@"FileContentPath"],
                               [results stringForColumn:@"MemberID"],
                               [results stringForColumn:@"LibraryID"]];
            
            //            NSLog(@"--------- ExpireMediaOnShelf -----------");
            //            NSLog(@"--- MediaTitle : %@ ----",[results stringForColumn:@"MediaTitle"]);
            //            NSLog(@"--- LibraryName : %@ ----",[results stringForColumn:@"LibraryName"]);
            //            NSLog(@"--- DateExp : %@ ----",[results stringForColumn:@"DateExp"]);
        }
    }
    [database close];
    
    return arrayOfExpBook;
}

+(BOOL)isMediaExpired:(NSString*)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isExp = NO;
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT MediaID, CoverPath, FileContentPath, MediaTitle, DateExp  FROM %@ where MediaID='%@' and cast(DateExp as INTEGER) < '%i'",OfflineShelfTable,mediaID,currDate];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            isExp = YES;
            //NSLog(@"--- MediaTitle : %@ ----",[results stringForColumn:@"MediaTitle"]);
            //NSLog(@"--- LibraryName : %@ ----",[results stringForColumn:@"LibraryName"]);
            //NSLog(@"--- Media is Exp >> DateExp : %@ ----",[results stringForColumn:@"DateExp"]);
        }
    }
    [database close];

    return isExp;
}

+(NSMutableArray *)selectMediaOnMyShelfId:(NSString *)shelfId MemberID:(NSString*)memberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfMedia = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString *userID = memberID;
        if ([shelfId isEqualToString:PublicShelfId])
            userID = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where ShelfID='%@' and MemberID='%@' and cast(DateExp as INTEGER) >= %d order by DateExp desc",OfflineShelfTable,shelfId,userID,(int)currDate];
       //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            OfflineShelf *myShelf = [[OfflineShelf alloc] init];
            [myShelf OfflineShelf:[results stringForColumn:@"MediaID"]
                         MemberID:[results stringForColumn:@"MemberID"]
                          ShelfID:[results stringForColumn:@"ShelfID"]
                        LibraryID:[results stringForColumn:@"LibraryID"]
                      LibraryName:[results stringForColumn:@"LibraryName"]
                       MediaTitle:[results stringForColumn:@"MediaTitle"]
                      MediaAuthor:[results stringForColumn:@"MediaAuthor"]
                        MediaType:[results stringForColumn:@"MediaType"]
                MediaCoverFileURL:[results stringForColumn:@"CoverURL"]
               MediaCoverFilePath:[results stringForColumn:@"CoverPath"]
                    MediaFilePath:[results stringForColumn:@"FileContentPath"]
                     MediaFileURL:[results stringForColumn:@"FileContentUrl"]
                       DateBorrow:[results stringForColumn:@"DateBorrow"]
                          DateExp:[results stringForColumn:@"DateExp"]
                MediaOriginalName:[results stringForColumn:@"OriginalFileName"]
                        SubjectID:[results stringForColumn:@"SubjectID"]];
            
            [arrayOfMedia addObject:myShelf];
            
            //NSLog(@"MediaTitle : %@", [results stringForColumn:@"MediaTitle"]);
        }
    }
    [database close];
    
    return arrayOfMedia;
}

+(NSMutableArray *)selectPresetMediaOnMyShelfId:(NSString *)roomID MemberID:(NSString*)memberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfMedia = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where ShelfID='%@' and MemberID='%@' and (cast(DateExp as INTEGER) >= %d or DateExp='%@') order by DateExp desc",OfflineShelfTable,roomID,memberID,(int)currDate,MediaPresetNoExpireDate];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            OfflineShelf *myShelf = [[OfflineShelf alloc] init];
            [myShelf OfflineShelf:[results stringForColumn:@"MediaID"]
                         MemberID:[results stringForColumn:@"MemberID"]
                          ShelfID:[results stringForColumn:@"ShelfID"]
                        LibraryID:[results stringForColumn:@"LibraryID"]
                      LibraryName:[results stringForColumn:@"LibraryName"]
                       MediaTitle:[results stringForColumn:@"MediaTitle"]
                      MediaAuthor:[results stringForColumn:@"MediaAuthor"]
                        MediaType:[results stringForColumn:@"MediaType"]
                MediaCoverFileURL:[results stringForColumn:@"CoverURL"]
               MediaCoverFilePath:[results stringForColumn:@"CoverPath"]
                    MediaFilePath:[results stringForColumn:@"FileContentPath"]
                     MediaFileURL:[results stringForColumn:@"FileContentUrl"]
                       DateBorrow:[results stringForColumn:@"DateBorrow"]
                          DateExp:[results stringForColumn:@"DateExp"]
                MediaOriginalName:[results stringForColumn:@"OriginalFileName"]
                        SubjectID:[results stringForColumn:@"SubjectID"]];
            
            [arrayOfMedia addObject:myShelf];
            
            //NSLog(@"MediaTitle : %@", [results stringForColumn:@"MediaTitle"]);
            //NSLog(@"OriginalFileName : %@", [results stringForColumn:@"OriginalFileName"]);
        }
    }
    [database close];
    
    return arrayOfMedia;
}

+(NSString*)selectMediaFilePath:(NSString*)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString *mediaPath = @"";
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        //NSString *userID = memberID;
        //userID = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
        
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where MediaID='%@' and cast(DateExp as INTEGER) >= %d ",OfflineShelfTable,mediaID,(int)currDate];
        
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            mediaPath = [results stringForColumn:@"FileContentURL"];
            // NSLog(@"ShelfID : %@", [results stringForColumn:@"ShelfID"]);
            //NSLog(@"LibraryName : %@", [results stringForColumn:@"LibraryName"]);
        }
    }
    [database close];
    
    return mediaPath;
}

+(NSMutableArray *)selectLibraryNameOnMyShelfId:(NSString*)shelfId MemberID:(NSString*)memberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfLibName = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        int currDate = [[Utility currentDate] intValue];
        NSString *userID = memberID;
        if ([shelfId isEqualToString:@"0"])
            userID = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
        NSString* sql = [NSString stringWithFormat:@"SELECT MediaTitle, ShelfID,LibraryName,LibraryID FROM %@ where ShelfID='%@' and MemberID='%@' and cast(DateExp as INTEGER) >= %d group by LibraryID order by LibraryName COLLATE NOCASE",OfflineShelfTable,shelfId,userID,(int)currDate];
        
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            [arrayOfLibName addObject:@[[results stringForColumn:@"ShelfID"],
                                        [results stringForColumn:@"LibraryID"],
                                        [results stringForColumn:@"LibraryName"]]];
            
            //NSLog(@"ShelfID : %@", [results stringForColumn:@"ShelfID"]);
           // NSLog(@"LibraryName : %@", [results stringForColumn:@"LibraryName"]);
            //NSLog(@"MediaTitle : %@",[results stringForColumn:@"MediaTitle"]);
        }
    }
    [database close];
    
    return arrayOfLibName;
}

+(NSInteger)selectTotalMediaOnShelf:(NSString*)memberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSInteger mediaNumber = 0;
    if ([self openDB:database]){
        NSInteger currDate = [[Utility currentDate] integerValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT count(*) as mCount FROM %@ where (MemberID='%@' or MemberID='%@') and cast(DateExp as INTEGER) >= %d",OfflineShelfTable,memberID,[[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode],(int)currDate];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"mCount : %@",[results stringForColumn:@"mCount"]);
            mediaNumber = [[results stringForColumn:@"mCount"] integerValue];
        }
    }
    [database close];

    return mediaNumber;
}

+(BOOL)isHaveUserMediaOnShelf{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isHave = NO;
    if ([self openDB:database]){
        NSInteger currDate = [[Utility currentDate] integerValue];
        NSString* sql = [NSString stringWithFormat:@"SELECT count(*) as mCount FROM %@ where MemberID='%@' and cast(DateExp as INTEGER) >= %d",OfflineShelfTable,[[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode],(int)currDate];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            if([[results stringForColumn:@"mCount"] integerValue] > 0)
                isHave = YES;
            else
                isHave = NO;
        }
    }
    [database close];

    return isHave;
}

+(void)recheckMediaExpirOnShelfAtDirectoryName:(NSString*)memberID InDirectoryPath:(NSString*)dirpath LibraryID:(NSString*)roomID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSInteger mediaNumber = 0;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT count(*) as mCount FROM %@ where (MemberID='%@' or MemberID='%@') and LibraryID='%@' ",OfflineShelfTable,memberID,[[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode],roomID];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"mCount : %@",[results stringForColumn:@"mCount"]);
            mediaNumber = [[results stringForColumn:@"mCount"] integerValue];
            
//            NSLog(@"--- MediaTitle : %@ ----",[results stringForColumn:@"MediaTitle"]);
//            NSLog(@"--- MediaID : %@ ----",[results stringForColumn:@"MediaID"]);
//            NSLog(@"--- DateExp : %@ ----",[results stringForColumn:@"DateExp"]);
//            NSLog(@"--- FileContentPath : %@ ----",[results stringForColumn:@"FileContentPath"]);
//            NSLog(@"--- CoverPath : %@ ----",[results stringForColumn:@"CoverPath"]);
        }
    }
    [database close];
    
    if (mediaNumber == 0) {
        [Utility removeAllFilesInDirectory:dirpath];
    }
}

+(void)removeMediaOnShelf:(NSString*)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"DELETE FROM %@ where MediaID='%@' ",OfflineShelfTable,mediaID];
        [database executeUpdate:sql, nil];
        //NSLog(@"Delete MediaOnshelf -->> %s -->> ID : %@",result ? "YES" : "NO",mediaID);
    
        //delete bookmark
        //result = NO;
        //result=
        [database executeUpdate:[NSString stringWithFormat:@"Delete From %@ where MediaID='%@'  ",MediaBookmarksTable,mediaID], nil];
        //NSLog(@"Delete Row in %@ ------> %@",MediaBookmarksTable,result ? @"YES" : @"NO");
    }
    [database close];
}

+(void)removeMediaOnShelfFilePath:(NSString*)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET FileContentPath='' Where MediaID='%@' ",OfflineShelfTable,mediaID];
        [database executeUpdate:sql,nil];
    }
    [database close];
}

+(BOOL)checkFileAvailableToRemove:(NSString*)mediaID FilePath:(NSString*)path{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isAvailable = YES;
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT MediaID FROM %@ Where (FileContentPath='%@' or CoverPath='%@') and MediaID!='%@' ",OfflineShelfTable,path,path,mediaID];
        FMResultSet *results = [database executeQuery:sql];
        if ([results next]) {
            isAvailable = NO;
        }
    }
    [database close];
    
    return isAvailable;
}



#pragma mark -------- MediaBookmarks Table --------
+(void)createBookmarkRemindTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                                  "PageIndex TEXT DEFAULT NULL, "
                                  "MediaID TEXT DEFAULT NULL, "
                                  "MarkText TEXT DEFAULT NULL) ",MediaBookmarksTable];
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",MediaBookmarksTable,result ? "YES" : "NO");
    }
    [database close];
}

+(void)insertBookmarkReminder:(NSDictionary *)dictionary{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        
        NSUInteger rowCount = [database intForQuery:[NSString stringWithFormat:@"SELECT COUNT(PageIndex) FROM %@ where PageIndex='%@' and MediaID='%@' ",MediaBookmarksTable,[dictionary objectForKey:@"PageIndex"],[dictionary objectForKey:@"MediaID"]]];
        
        if (rowCount > 0) {
            ///// update ///
            NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET MarkText='%@' where PageIndex='%@' and MediaID='%@' ",MediaBookmarksTable,[dictionary objectForKey:@"MarkText"],[dictionary objectForKey:@"PageIndex"],[dictionary objectForKey:@"MediaID"]];
            [database executeUpdate:sql,nil];
            //BOOL result = [database executeUpdate:sql,nil];
            //NSLog(@"Update -->> %@ -->> %s",MediaBookmarksTable,result ? "YES" : "NO");
        }
        else{
            //// insert ////
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (PageIndex, MediaID, MarkText) VALUES ('%@', '%@', '%@')",MediaBookmarksTable,
                             [dictionary objectForKey:@"PageIndex"],
                             [dictionary objectForKey:@"MediaID"],
                             [dictionary objectForKey:@"MarkText"]];
            [database executeUpdate:sql, nil];
            //BOOL result = [database executeUpdate:sql, nil];
            //NSLog(@"Insert -->> %@ -->> %s ",MediaBookmarksTable,result ? "YES" : "NO");
        }
        
    }
    
    [database close];
}

+(void)selectAllMarkRemindByMediaID:(NSString *)mediaId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where MediaID='%@' ",MediaBookmarksTable,mediaId];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"MarkText : %@",[results stringForColumn:@"MarkText"]);
           // NSLog(@"Page Index : %@",[results stringForColumn:@"PageIndex"]);
            // markText = [results stringForColumn:@"MarkText"];
        }
    }
    [database close];
}

+(NSString *)selectMarkRemindByPageIndex:(int)pageIndex MediaID:(NSString *)mediaId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSString *markText;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT MarkText FROM %@ where PageIndex='%@' and MediaID='%@' ",MediaBookmarksTable,[NSString stringWithFormat:@"%i",pageIndex],mediaId];
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"mCount : %@",[results stringForColumn:@"mCount"]);
            markText = [results stringForColumn:@"MarkText"];
        }
    }
    [database close];
    
    return markText;
}

+(void)deleteMarkRemindAtPageIndex:(int)pageIndex MediaID:(NSString *)mediaId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        BOOL result = NO;
        result=[database executeUpdate:[NSString stringWithFormat:@"Delete From %@ where MediaID='%@' and PageIndex='%@' ",MediaBookmarksTable,mediaId,[NSString stringWithFormat:@"%i",pageIndex]], nil];
        //NSLog(@"Delete Row in %@ ------> %@",MediaBookmarksTable,result ? @"YES" : @"NO");
    }
    
    [database close];
}

+(BOOL)isHaveMarkRemindAtPageIndex:(int)pageIndex MediaID:(NSString *)mediaId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isMark = NO;
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT PageIndex FROM %@ where PageIndex='%@' and MediaID='%@' ",MediaBookmarksTable,[NSString stringWithFormat:@"%i",pageIndex],mediaId];
        //NSLog(@"sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            //NSLog(@"Is have mark");
            isMark = YES;
        }
    }
    [database close];
    
    return isMark;
}

+(void)deleteMarkRemindWhenMediaExp:(NSString*)mediaID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        BOOL result = NO;
        result=[database executeUpdate:[NSString stringWithFormat:@"Delete From %@ where MediaID='%@'  ",MediaBookmarksTable,mediaID], nil];
        //NSLog(@"Delete Row in %@ ------> %@",MediaBookmarksTable,result ? @"YES" : @"NO");
    }
    
    [database close];
}



#pragma mark ------- Media Preset ----------
+(void)createMediaPresetTable{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        NSString* createScript = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( "
                                  "SUBJECTID TEXT PRIMARY KEY DEFAULT NULL, "
                                  "MEMBERID TEXT DEFAULT NULL, "
                                  "ROOMID TEXT DEFAULT NULL, "
                                  "ROOMNAME TEXT DEFAULT NULL, "
                                  "SUBJECTCODE TEXT DEFAULT NULL, "
                                  "SUBJECTNAME TEXT DEFAULT NULL, "
                                  "DESP TEXT DEFAULT NULL, "
                                  "SECTIONCODE TEXT DEFAULT NULL, "
                                  "YEAR TEXT DEFAULT NULL, "
                                  "SEMESTER TEXT DEFAULT NULL, "
                                  "UPDATEDATE TEXT DEFAULT NULL) ",MediaPresetTable];
        
        [database executeUpdate:createScript];
        //BOOL result = [database executeUpdate:createScript];
        //NSLog(@"Create -->> %@ -->> %s",OfflineShelfTable,result ? "YES" : "NO");
    }
    [database close];
    
}

+(void)insertMediaPreset:(MediaPreset *)preSet{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        
        FMResultSet *presetMedia = [database executeQuery:[NSString stringWithFormat:@"SELECT UPDATEDATE FROM %@ where SUBJECTID='%@' limit 1",MediaPresetTable,[preSet subjectID]]];
        if ([presetMedia next]) {
            ///// update ///
            if([[preSet updateDate] intValue] > [[presetMedia stringForColumn:@"UPDATEDATE"] intValue]){
                NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET MEMBERID='%@', ROOMID='%@', ROOMNAME='%@', SUBJECTCODE='%@', SUBJECTNAME='%@', DESP='%@', SECTIONCODE='%@', YEAR='%@', SEMESTER='%@', UPDATEDATE='%@'  Where SUBJECTID='%@' ",MediaPresetTable,[preSet memberID],[preSet roomID],[preSet roomName],[preSet subjectCode],[preSet subjectName],[preSet desp],[preSet sectionCode],[preSet year],[preSet semester],[preSet updateDate],[preSet subjectID]];
                [database executeUpdate:sql,nil];
            }
        }
        else{
            //// insert ////
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (SUBJECTID, MEMBERID,  ROOMID, ROOMNAME, SUBJECTCODE, SUBJECTNAME, DESP, SECTIONCODE, YEAR, SEMESTER, UPDATEDATE) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",MediaPresetTable,
                             [preSet subjectID],
                             [preSet memberID],
                             [preSet roomID],
                             [preSet roomName],
                             [preSet subjectCode],
                             [preSet subjectName],
                             [preSet desp],
                             [preSet sectionCode],
                             [preSet year],
                             [preSet semester],
                             [preSet updateDate]];
            //[database executeUpdate:sql, nil];
            BOOL result = [database executeUpdate:sql, nil];
            NSLog(@"Insert -->> %@ -->> %s -->> SUBJECTID : %@",MediaPresetTable,result ? "YES" : "NO",[preSet subjectID]);
        }
    }
    [database close];
}

/*+(NSMutableArray *)selectPresetSubjectItem:(NSString*)subjectID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *arrayOfLibName = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where SUBJECTID='%@' ",OfflineShelfTable,subjectID];
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            [arrayOfLibName addObject:@[[results stringForColumn:@"SUBJECTID"],
                                        [results stringForColumn:@"ROOMID"],
                                        [results stringForColumn:@"ROOMNAME"],
                                        [results stringForColumn:@"SUBJECTCODE"],
                                        [results stringForColumn:@"SUBJECTNAME"],
                                        [results stringForColumn:@"DESP"],
                                        [results stringForColumn:@"SECTIONCODE"],
                                        [results stringForColumn:@"YEAR"],
                                        [results stringForColumn:@"SEMESTER"],
                                        [results stringForColumn:@"UPDATEDATE"]]];
            
            // NSLog(@"ShelfID : %@", [results stringForColumn:@"ShelfID"]);
            //NSLog(@"LibraryName : %@", [results stringForColumn:@"LibraryName"]);
        }
    }
    [database close];
    
    return arrayOfLibName;
}*/

+(void)selectMediaPresetBySubjectID:(NSString*)subjectID SelectAllRow:(BOOL)selectAll{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        //[database executeUpdate:[NSString stringWithFormat:@"Update %@ SET DateExp='9' where ShelfID!='0' and ShelfID!='1' ",OfflineShelfTable]];
        
        //[database executeUpdate:[NSString stringWithFormat:@"Delete FROM %@ ",OfflineShelfTable]];
        //[database executeUpdate:[NSString stringWithFormat:@"Delete FROM %@ ",MediaPresetTable]];
        
        //NSLog(@"--------- MediaOnShelf -----------");
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM %@ where ShelfID ='2' ",OfflineShelfTable];
        // where ShelfID !='0' and ShelfID !='1'
        //NSLog(@"Sql : %@",sql);
        FMResultSet *results = [database executeQuery:sql];
        while([results next]) {
            NSLog(@"MediaID : %@",[results stringForColumn:@"MediaID"]);
            NSLog(@"MediaTitle : %@",[results stringForColumn:@"MediaTitle"]);
            NSLog(@"ShelfID : %@",[results stringForColumn:@"ShelfID"]);
            NSLog(@"DateBorrow : %@",[results stringForColumn:@"DateBorrow"]);
            NSLog(@"DateExp : %@",[results stringForColumn:@"DateExp"]);
            NSLog(@"--------------------");
        }

        
        
//        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ where SUBJECTID='%@' ",MediaPresetTable,subjectID];
//        if (selectAll) {
//            sql = [NSString stringWithFormat:@"SELECT * FROM %@ ",MediaPresetTable];
//        }
//
//        FMResultSet *preset = [database executeQuery:sql];
//        while ([preset next]) {
//            NSLog(@"SUBJECTNAME : %@",[preset stringForColumn:@"SUBJECTNAME"]);
//            NSLog(@"SUBJECTCODE : %@",[preset stringForColumn:@"SUBJECTCODE"]);
//        }
    }
    [database close];
    
}

+(void)clearMediaPresetOnMyShelf{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ where where ShelfID ='2' ",OfflineShelfTable];
        FMResultSet *preset = [database executeQuery:sql];
        while ([preset next]) {
            [Utility removeExpiredMediaOnShelf:[preset stringForColumn:@"MediaID"]
                                     CoverPath:[preset stringForColumn:@"CoverPath"]
                                      FilePath:[preset stringForColumn:@"FileContentPath"]
                                      MemberID:[preset stringForColumn:@"MemberID"]
                                        RoomID:[preset stringForColumn:@"ShelfID"]];
           
            NSLog(@"MediaTitle : %@",[preset stringForColumn:@"MediaTitle"]);
        }
        
        [database executeUpdate:[NSString stringWithFormat:@"Delete FROM %@ where ShelfID ='2' ",OfflineShelfTable]];
    }
    [database close];
    
}

+(NSMutableArray *)selectPresetRoomItems:(NSString*)memberID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *presetArray = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT ROOMID,ROOMNAME,SUBJECTID FROM %@ where MEMBERID='%@'  group by ROOMID order by ROOMNAME COLLATE NOCASE ",MediaPresetTable,memberID];
        FMResultSet *preset = [database executeQuery:sql];
    
        while ([preset next]) {
            [presetArray addObject:@[[preset stringForColumn:@"SUBJECTID"],
                                     [preset stringForColumn:@"ROOMID"],
                                     [preset stringForColumn:@"ROOMNAME"]]];
            
            //NSLog(@"SUBJECTNAME : %@",[preset stringForColumn:@"ROOMNAME"]);
            //NSLog(@"SUBJECTCODE : %@",[preset stringForColumn:@"ROOMID"]);
        }
    }
    [database close];
    
    return presetArray;
}

+(NSMutableArray *)selectPresetSubjectItems:(NSString*)roomID{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableArray *presetArray = [[NSMutableArray alloc] init];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT ROOMID, SUBJECTID, SUBJECTNAME, SUBJECTCODE FROM %@ where ROOMID='%@' order by ROOMNAME COLLATE NOCASE ",MediaPresetTable,roomID];
        FMResultSet *preset = [database executeQuery:sql];
        
        while ([preset next]) {
            NSString *subjectName = [NSString stringWithFormat:@"%@ : %@",[preset stringForColumn:@"SUBJECTCODE"],[preset stringForColumn:@"SUBJECTNAME"]];
           
            [presetArray addObject:@[[preset stringForColumn:@"ROOMID"],
                                     [preset stringForColumn:@"SUBJECTID"],
                                     subjectName]];
            
            //NSLog(@"SUBJECTNAME : %@",[preset stringForColumn:@"SUBJECTID"]);
            //NSLog(@"SUBJECTCODE : %@",subjectName);
        }
    }
    [database close];
    
    return presetArray;
}

+(NSMutableDictionary *)selectMediaPresetInfoByRoomId:(NSString*)roomId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    NSMutableDictionary *mulDictPreset = [[NSMutableDictionary alloc] init];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ where ROOMID='%@' ",MediaPresetTable,roomId];
        FMResultSet *preset = [database executeQuery:sql];
        while ([preset next]) {
             MediaPreset *presetInfo = [[MediaPreset alloc] init];
            [presetInfo MediaPreset:[preset stringForColumn:@"SUBJECTID"]
                           MemberID:[preset stringForColumn:@"MEMBERID"]
                             ROOMID:[preset stringForColumn:@"ROOMID"]
                           ROOMNAME:[preset stringForColumn:@"ROOMNAME"]
                        SUBJECTCODE:[preset stringForColumn:@"SUBJECTCODE"]
                        SUBJECTNAME:[preset stringForColumn:@"SUBJECTNAME"]
                               DESP:[preset stringForColumn:@"DESP"]
                        SECTIONCODE:[preset stringForColumn:@"SECTIONCODE"]
                               YEAR:[preset stringForColumn:@"YEAR"]
                           SEMESTER:[preset stringForColumn:@"SEMESTER"]
                         UPDATEDATE:[preset stringForColumn:@"UPDATEDATE"]];
            
            //NSLog(@"SUBJECTNAME : %@",[preset stringForColumn:@"SUBJECTNAME"]);
            //NSLog(@"SUBJECTID : %@",[preset stringForColumn:@"SUBJECTID"]);
            [mulDictPreset setValue:presetInfo forKey:[preset stringForColumn:@"SUBJECTID"]];
        }
    }
    [database close];
    
    return mulDictPreset;
}

+(MediaPreset *)selectMediaPresetInfoBySubjectId:(NSString*)subjectId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    MediaPreset *presetInfo = [[MediaPreset alloc] init];
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ where SUBJECTID='%@' ",MediaPresetTable,subjectId];
        FMResultSet *preset = [database executeQuery:sql];
        while ([preset next]) {
            [presetInfo MediaPreset:[preset stringForColumn:@"SUBJECTID"]
                           MemberID:[preset stringForColumn:@"MEMBERID"]
                             ROOMID:[preset stringForColumn:@"ROOMID"]
                           ROOMNAME:[preset stringForColumn:@"ROOMNAME"]
                        SUBJECTCODE:[preset stringForColumn:@"SUBJECTCODE"]
                        SUBJECTNAME:[preset stringForColumn:@"SUBJECTNAME"]
                               DESP:[preset stringForColumn:@"DESP"]
                        SECTIONCODE:[preset stringForColumn:@"SECTIONCODE"]
                               YEAR:[preset stringForColumn:@"YEAR"]
                           SEMESTER:[preset stringForColumn:@"SEMESTER"]
                         UPDATEDATE:[preset stringForColumn:@"UPDATEDATE"]];
            
            //NSLog(@"SUBJECTNAME : %@",[preset stringForColumn:@"SUBJECTNAME"]);
            //NSLog(@"SUBJECTID : %@",[preset stringForColumn:@"SUBJECTID"]);
        }
    }
    [database close];
    
    return presetInfo;
}

+(BOOL)isMediaPresetRoomExist:(NSString *)roomId{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    BOOL isExist = NO;
    if ([self openDB:database]){
        NSString *sql = [NSString stringWithFormat:@"SELECT ROOMID FROM %@ where ROOMID='%@' ",MediaPresetTable,roomId];
        FMResultSet *preset = [database executeQuery:sql];
        while ([preset next]) {
            isExist = YES;
        }
    }
    [database close];
    
    return isExist;
}


#pragma mark ------ delete all row in Table -------

+(void)deleteAllRowInMemberTable{
    [self deleteAllRowInTable:MemberInfoTable];
}

+(void)deleteAllRowInLibraryTable{
    [self deleteAllRowInTable:RoomInfoTable];
}

+(void)deleteAllRowInHistoryTable{
    [self deleteAllRowInTable:HistoryTable];
}

+(void)deleteAllRowInMyShelfTable{
    [self deleteAllRowInTable:OfflineShelfTable];
}

+(void)deleteAllRowInPresetTable{
    [self deleteAllRowInTable:MediaPresetTable];
}

+(void)deleteAllRowInTable:(NSString*)tableName{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        BOOL result = NO;
        result=[database executeUpdate:[NSString stringWithFormat:@"Delete From  %@ ",tableName], nil];
        //NSLog(@"Delete all Row in %@ ------> %@",tableName,result ? @"YES" : @"NO");
    }
    
    [database close];
}



#pragma mark ------ drop table ----------
+(void)dropMemberTable{
    [self dropTable:MemberInfoTable];
}

+(void)dropLibraryTable{
    [self dropTable:RoomInfoTable];
}

+(void)dropHistoryTable{
    [self dropTable:HistoryTable];
}

+(void)dropMyShelfTable{
    [self dropTable:OfflineShelfTable];
}

+(void)dropPresetTable{
    [self dropTable:MediaPresetTable];
}

+(void)dropTable:(NSString*)tableName{
    FMDatabase *database = [FMDatabase databaseWithPath:[self databasePath]];
    if ([self openDB:database]){
        BOOL result = NO;
        result=[database executeUpdate:[NSString stringWithFormat:@"DROP TABLE IF EXISTS %@ ",tableName], nil];
        //NSLog(@"Drop %@ ------> %@",tableName,result ? @"YES" : @"NO");
    }
    
     [database close];
}




@end

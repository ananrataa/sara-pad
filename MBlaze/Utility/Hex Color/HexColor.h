//
//  HexColor.h
//
//  Created by Nong.Preeyawal
//  Copyright (c) 2015 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ANDROID_COLOR(c) [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:((c)&0xFF)/255.0  alpha:((c>>24)&0xFF)/255.0]
@interface HexColor : NSObject

+ (UIColor *) SKColorHexString:(NSString *) hexString;

@end

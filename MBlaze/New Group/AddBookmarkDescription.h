//
//  AddBookmarkDescription.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/27/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBookmarkDescription : UIView

@property (strong,nonatomic)UIButton *cancelBtn;
@property (strong,nonatomic)UIButton *saveBtn;
@property (strong,nonatomic)UILabel *textLength;
@property (strong,nonatomic)UILabel *bookmarkPage;
@property (strong,nonatomic)UITextView *bookmarkTextView;

//@property (strong,nonatomic)UIImageView *imageBookmark;
//@property (strong,nonatomic)UILabel *labelBookmark;


@end

//
//  UIViewBookmarkReminder.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "UIViewBookmarkReminder.h"
#import "HexColor.h"
#import "Utility.h"

@implementation UIViewBookmarkReminder

#define BUTTON_WIDTH 70.f
#define BUTTON_HEIGHT 36.f
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        UIView *navigateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 48.0f)];
        navigateView.backgroundColor = [HexColor SKColorHexString:@"A9A9A9"];
        
        self.backBtn = [[UIButton alloc] initWithFrame:CGRectMake(8.f, 6.f, BUTTON_WIDTH, BUTTON_HEIGHT)];
        [self.backBtn setTitle:[Utility NSLocalizedString:@"Back"] forState:UIControlStateNormal];
        [self.backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.backBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        self.backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        self.saveBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-BUTTON_WIDTH, 6.f, BUTTON_WIDTH, BUTTON_HEIGHT)];
        [self.saveBtn setTitle:[Utility NSLocalizedString:@"Edit"] forState:UIControlStateNormal];
        [self.saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.saveBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
         self.saveBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        
        UILabel *reminder = [[UILabel alloc] initWithFrame:CGRectMake(8.f+8.f+BUTTON_WIDTH, 6.f, self.frame.size.width-(8.f+BUTTON_WIDTH+BUTTON_WIDTH), BUTTON_HEIGHT)];
        reminder.text = [Utility NSLocalizedString:@"Reminder"];
        reminder.textColor = [UIColor whiteColor];
        reminder.font = [UIFont systemFontOfSize:20.f];
        reminder.textAlignment = NSTextAlignmentCenter;
        
        self.bookmarkTextView =  [[UITextView alloc] initWithFrame:CGRectMake(8.f, 48.0f, frame.size.width-16.f, frame.size.height-48.f)];
        self.bookmarkTextView.backgroundColor = [HexColor SKColorHexString:@"999900"];
        self.bookmarkTextView.font = [UIFont systemFontOfSize:[Utility fontSize:16.f]];
        [self.bookmarkTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
        self.bookmarkTextView.text = [NSMutableString stringWithString: @"Many years ago, when computers supported maximum 256 different colors, a list of 216 \"Web Safe Colors\" was suggested as a Web standard (reserving 40 fixed system colors). This is not important now, since most computers can display millions of different colors. This 216 hex values cross-browser color palette was created to ensure that all computers would display the colors correctly when running a 256 color ********** Many years ago, when computers supported maximum 256 different colors, a list of 216 \"Web Safe Colors\" was suggested as a Web standard (reserving 40 fixed system colors). This is not important now, since most computers can display millions of different colors. This 216 hex values cross-browser color palette was created to ensure that all computers would display the colors correctly when running a 256 color"];
        
        [navigateView addSubview:self.backBtn];
        [navigateView addSubview:reminder];
        [navigateView addSubview:self.saveBtn];
        
        [self addSubview:navigateView];
        [self addSubview:self.bookmarkTextView];
        
        //[self.bookmarkTextView becomeFirstResponder];
    }
    
    return self;
}

@end

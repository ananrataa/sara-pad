//
//  UIViewBookmarkReminder.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewBookmarkReminder : UIView <UITextViewDelegate>

@property (strong,nonatomic)UIButton *backBtn;
@property (strong,nonatomic)UIButton *saveBtn;
@property (strong,nonatomic)UITextView *bookmarkTextView;

@end

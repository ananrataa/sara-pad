//
//  AddBookmarkDescription.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/27/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AddBookmarkDescription.h"
#import "HexColor.h"
#import "Utility.h"
#import "ConstantValues.h"

@implementation AddBookmarkDescription

#define BUTTON_WIDTH 70.f
#define BUTTON_HEIGHT 25.f

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = YES;
        self.backgroundColor = [HexColor SKColorHexString:@"f5f5f5"];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 5.f;
        
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
        self.layer.shadowOpacity = 0.5f;
        self.layer.shadowPath = shadowPath.CGPath;
        
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 40.f)];
        topView.backgroundColor = [HexColor SKColorHexString:@"e6e6e6"];
        topView.clipsToBounds = YES;
        topView.layer.cornerRadius = 5.f;
        
        UILabel *reminder = [[UILabel alloc] initWithFrame:CGRectMake(8.f+BUTTON_WIDTH, 7.5f, self.frame.size.width-(16.f+BUTTON_WIDTH+BUTTON_WIDTH), BUTTON_HEIGHT)];
        reminder.text = [Utility NSLocalizedString:@"Reminder"];
        reminder.textColor = [UIColor blackColor];
        reminder.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:15.f]];
        reminder.textAlignment = NSTextAlignmentCenter;
        
        
        self.cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(8.f, 7.5f, BUTTON_WIDTH, BUTTON_HEIGHT)];
        [self.cancelBtn setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        self.cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.cancelBtn.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:12.f]];
        self.cancelBtn.clipsToBounds = YES;
        self.cancelBtn.layer.cornerRadius = 10.f;
        self.cancelBtn.backgroundColor = [HexColor SKColorHexString:@"B8B8B8"];
        [self addShadow:self.cancelBtn];
        
        self.saveBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-(BUTTON_WIDTH+8.f), 7.5f, BUTTON_WIDTH, BUTTON_HEIGHT)];
        [self.saveBtn setTitle:[Utility NSLocalizedString:@"Save"] forState:UIControlStateNormal];
        [self.saveBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [self.saveBtn setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
        self.saveBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.saveBtn.titleLabel.font = [UIFont fontWithName:mainFont size:12.f];
        self.saveBtn.clipsToBounds = YES;
        self.saveBtn.layer.cornerRadius = 10.f;
        self.saveBtn.backgroundColor = [HexColor SKColorHexString:@"B8B8B8"];
        [self addShadow:self.saveBtn];
        
        self.bookmarkPage = [[UILabel alloc] initWithFrame:CGRectMake(10, topView.frame.size.height+5, 200, 20)];
        self.bookmarkPage.backgroundColor = [UIColor clearColor];
        self.bookmarkPage.textAlignment = NSTextAlignmentLeft;
        self.bookmarkPage.text = [NSMutableString stringWithString: @"หน้าที่: 1000"];
        self.bookmarkPage.textColor = [UIColor darkGrayColor];
        self.bookmarkPage.font = [UIFont fontWithName:mainFont size:[Utility fontSize:13.f]];
        
        self.textLength = [[UILabel alloc] initWithFrame:CGRectMake(topView.frame.size.width-110, topView.frame.size.height+5, 100, 20)];
        self.textLength.backgroundColor = [UIColor clearColor];
        self.textLength.textAlignment = NSTextAlignmentRight;
        self.textLength.text = [NSMutableString stringWithString: @"0/250"];
        self.textLength.textColor = [UIColor darkGrayColor];
        self.textLength.font = [UIFont fontWithName:mainFont size:[Utility fontSize:13.f]];
        
        float bookmarkYpoint = self.textLength.frame.origin.y+self.textLength.frame.size.height+5;
        self.bookmarkTextView = [[UITextView alloc] initWithFrame:CGRectMake(5,bookmarkYpoint, frame.size.width-10, frame.size.height-(bookmarkYpoint+5))];
        self.bookmarkTextView.backgroundColor = [UIColor whiteColor];
        self.bookmarkTextView.textAlignment = NSTextAlignmentLeft;
        self.bookmarkTextView.textColor = [UIColor blackColor];
        self.bookmarkTextView.font = [UIFont fontWithName:mainFont size:[Utility fontSize:15.f]];
        [self.bookmarkTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
        
        [topView addSubview:self.cancelBtn];
        [topView addSubview:reminder];
        [topView addSubview:self.saveBtn];
        
        [self addSubview:topView];
        [self addSubview:self.bookmarkPage];
        [self addSubview:self.textLength];
        [self addSubview:self.bookmarkTextView];
    }
    
    return self;
}

-(void)addShadow:(UIButton*)view{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowRadius = 5.f;
    view.layer.shadowPath = shadowPath.CGPath;
}

@end

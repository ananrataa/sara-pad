//
//	ReaderMainToolbar.m
//	Reader v2.8.6
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011-2015 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "ReaderConstants.h"
#import "ReaderMainToolbar.h"
#import "ReaderDocument.h"

#import <MessageUI/MessageUI.h>

#import "DBManager.h"
#import "HexColor.h"
#import "UIViewBookmarkReminder.h"
#import "Utility.h"

@implementation ReaderMainToolbar
{
	UIButton *markButton;
    //UIButton *markRemind;
    
    NSInteger currentPage;
    NSString *guid;
    NSString *mediaID;
    
    UIViewBookmarkReminder *markRemindView;

	UIImage *markImageN;
	UIImage *markImageY;
}

#pragma mark - Constants

#define BUTTON_X 8.0f
#define BUTTON_Y 8.0f

#define BUTTON_SPACE 8.0f
#define BUTTON_HEIGHT 30.0f

#define BUTTON_FONT_SIZE 16.0f
#define TEXT_BUTTON_PADDING 24.0f

#define ICON_BUTTON_WIDTH 40.0f

#define TITLE_FONT_SIZE 17.0f
#define TITLE_HEIGHT 28.0f

#pragma mark - Properties

@synthesize delegate;

#pragma mark - ReaderMainToolbar instance methods

- (instancetype)initWithFrame:(CGRect)frame
{
	return [self initWithFrame:frame document:nil mediaID:nil];
}

- (instancetype)initWithFrame:(CGRect)frame document:(ReaderDocument *)document mediaID:(NSString*)media_id
{
    assert(document != nil); // Must have a valid ReaderDocument
    
    if ((self = [super initWithFrame:frame]))
    {
        CGFloat viewWidth = self.bounds.size.width; // Toolbar view width
        
        guid = document.guid;
        mediaID = media_id;
        ///NSLog(@"check add media id : %@",mediaID);
        
        UIImage *buttonH = nil;
        UIImage *buttonN = nil;
        
        //BOOL largeDevice = ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad);
        
        const CGFloat buttonSpacing = BUTTON_SPACE; const CGFloat iconButtonWidth = ICON_BUTTON_WIDTH;
        
        CGFloat titleX = BUTTON_X; CGFloat titleWidth = (viewWidth - (titleX + titleX));
        
#if (READER_STANDALONE == FALSE) // Option
        
        NSString *doneButtonText = [Utility NSLocalizedString:@"Back"];
        
        UIFont *doneButtonFont = [UIFont boldSystemFontOfSize:BUTTON_FONT_SIZE];
        CGSize size = [Utility getWidthOfNSString:doneButtonText FontSize:doneButtonFont];
        
        // Values are fractional -- you should take the ceilf to get equivalent values
        CGSize doneButtonSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
        
        CGFloat doneButtonWidth = (doneButtonSize.width + TEXT_BUTTON_PADDING);
        
        UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        doneButton.frame = CGRectMake(BUTTON_X, BUTTON_Y, doneButtonWidth, BUTTON_HEIGHT);
        [doneButton setTitleColor:[UIColor colorWithWhite:0.0f alpha:1.0f] forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor colorWithWhite:1.0f alpha:1.0f] forState:UIControlStateHighlighted];
        [doneButton setTitle:doneButtonText forState:UIControlStateNormal]; doneButton.titleLabel.font = doneButtonFont;
        [doneButton addTarget:self action:@selector(doneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [doneButton setBackgroundImage:buttonH forState:UIControlStateHighlighted];
        [doneButton setBackgroundImage:buttonN forState:UIControlStateNormal];
        doneButton.autoresizingMask = UIViewAutoresizingNone;
        //doneButton.backgroundColor = [UIColor grayColor];
        doneButton.exclusiveTouch = YES;
        
        [self addSubview:doneButton];
        
        titleX += (doneButtonWidth + buttonSpacing); titleWidth -= (doneButtonWidth + buttonSpacing);
        
#endif // end of READER_STANDALONE Option
        
        CGFloat rightButtonX = viewWidth; // Right-side buttons start X position
        
#if (READER_BOOKMARKS == TRUE) // Option
        
        rightButtonX -= (iconButtonWidth + buttonSpacing); // Position
        
        /// add bookmark
        UIButton *flagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        flagButton.frame = CGRectMake(rightButtonX, BUTTON_Y, iconButtonWidth, BUTTON_HEIGHT);
        [flagButton setImage:[UIImage imageNamed:@"Reader-Mark-N"] forState:UIControlStateNormal];
        [flagButton addTarget:self action:@selector(markButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [flagButton setBackgroundImage:buttonH forState:UIControlStateHighlighted];
        [flagButton setBackgroundImage:buttonN forState:UIControlStateNormal];
        flagButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        //flagButton.backgroundColor = [UIColor grayColor];
        flagButton.exclusiveTouch = YES;
        
        [self addSubview:flagButton]; titleWidth -= (iconButtonWidth + buttonSpacing);
        
        markButton = flagButton; markButton.enabled = NO; markButton.tag = NSIntegerMin;
        
        markImageN = [UIImage imageNamed:@"Reader-Mark-N"]; // N image
        markImageY = [UIImage imageNamed:@"Reader-Mark-Y"]; // Y image
        
#endif // end of READER_BOOKMARKS Option
        
#if (READER_ENABLE_THUMBS == TRUE) // Option
        
        rightButtonX -= iconButtonWidth;
        
        //// list of thumbnail page ///
        UIButton *thumbsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        thumbsButton.frame = CGRectMake(rightButtonX, BUTTON_Y, iconButtonWidth, BUTTON_HEIGHT);
        [thumbsButton setImage:[UIImage imageNamed:@"Reader-Thumbs"] forState:UIControlStateNormal];
        [thumbsButton addTarget:self action:@selector(thumbsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [thumbsButton setBackgroundImage:buttonH forState:UIControlStateHighlighted];
        [thumbsButton setBackgroundImage:buttonN forState:UIControlStateNormal];
        thumbsButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        //thumbsButton.backgroundColor = [UIColor blueColor];
        thumbsButton.exclusiveTouch = YES;
        
        [self addSubview:thumbsButton];
        
        titleX += (iconButtonWidth + buttonSpacing); titleWidth -= (iconButtonWidth + buttonSpacing);
        
#endif // end of READER_ENABLE_THUMBS Option
        
        rightButtonX -= iconButtonWidth;
        
        self.bookmarkRemind = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bookmarkRemind.frame = CGRectMake(rightButtonX, BUTTON_Y, iconButtonWidth, BUTTON_HEIGHT);
        [self.bookmarkRemind setImage:[UIImage imageNamed:@"bookmarkReminder"] forState:UIControlStateNormal];
        [self.bookmarkRemind setBackgroundImage:buttonH forState:UIControlStateHighlighted];
        [self.bookmarkRemind setBackgroundImage:buttonN forState:UIControlStateNormal];
        self.bookmarkRemind.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [self addSubview:self.bookmarkRemind];
        
       // markRemind = self.bookmarkRemind;
       self.bookmarkRemind.hidden = YES;
        
        /*if (largeDevice == YES) // Show document filename in toolbar
         {
         CGRect titleRect = CGRectMake(titleX, BUTTON_Y, titleWidth, TITLE_HEIGHT);
         
         UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleRect];
         
         titleLabel.textAlignment = NSTextAlignmentCenter;
         titleLabel.font = [UIFont systemFontOfSize:TITLE_FONT_SIZE];
         titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
         titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
         titleLabel.textColor = [UIColor colorWithWhite:0.0f alpha:1.0f];
         titleLabel.backgroundColor = [UIColor clearColor];
         titleLabel.adjustsFontSizeToFitWidth = YES;
         titleLabel.minimumScaleFactor = 0.75f;
         titleLabel.text = [document.fileName stringByDeletingPathExtension];
         #if (READER_FLAT_UI == FALSE) // Option
         titleLabel.shadowColor = [UIColor colorWithWhite:0.75f alpha:1.0f];
         titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
         #endif // end of READER_FLAT_UI Option
         
         [self addSubview:titleLabel];
         }*/
    }
    
    return self;
}



- (void)setBookmarkState:(BOOL)state CurrentPageNumber:(NSInteger)pageNumber
{
#if (READER_BOOKMARKS == TRUE) // Option
	if (state != markButton.tag) // Only if different state
	{
		if (self.hidden == NO) // Only if toolbar is visible
		{
			UIImage *image = (state ? markImageY : markImageN);

			[markButton setImage:image forState:UIControlStateNormal];
		}

		markButton.tag = state; // Update bookmarked state tag
        self.bookmarkRemind.hidden = YES;
        currentPage = pageNumber;
        //NSLog(@"set Page number : %ld",(long)currentPage);
        //check bookmark have mark remind
        if (state && [DBManager isHaveMarkRemindAtPageIndex:(int)currentPage MediaID:mediaID]) {
            self.bookmarkRemind.hidden = !state;
        }
        
        ///delete mark text remind
        if (!state) {
            [DBManager deleteMarkRemindAtPageIndex:(int)currentPage MediaID:mediaID];
        }
	}

	if (markButton.enabled == NO)
        markButton.enabled = YES;
    
    currentPage = pageNumber;

#endif // end of READER_BOOKMARKS Option
}

- (void)updateBookmarkImage
{
#if (READER_BOOKMARKS == TRUE) // Option

	if (markButton.tag != NSIntegerMin) // Valid tag
	{
		BOOL state = markButton.tag; // Bookmarked state

		UIImage *image = (state ? markImageY : markImageN);

		[markButton setImage:image forState:UIControlStateNormal];
        
        self.bookmarkRemind.hidden = YES;
        
        //check bookmark have mark remind
        //NSLog(@"update Page number : %ld",(long)currentPage);
        if (state && [DBManager isHaveMarkRemindAtPageIndex:(int)currentPage MediaID:mediaID]) {
             self.bookmarkRemind.hidden = !state;
        }
        
        ///delete mark text remind
        if (!state) {
            [DBManager deleteMarkRemindAtPageIndex:(int)currentPage MediaID:mediaID];
        }
	}

	if (markButton.enabled == NO)
        markButton.enabled = YES;

#endif // end of READER_BOOKMARKS Option
}

- (void)hideToolbar
{
	if (self.hidden == NO)
	{
		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.alpha = 0.0f;
			}
			completion:^(BOOL finished)
			{
				self.hidden = YES;
			}
		];
	}
}

- (void)showToolbar
{
	if (self.hidden == YES)
	{
		[self updateBookmarkImage]; // First

		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.hidden = NO;
				self.alpha = 1.0f;
			}
			completion:NULL
		];
	}
}

#pragma mark - UIButton action methods

- (void)doneButtonTapped:(UIButton *)button
{
	[delegate tappedInToolbar:self doneButton:button];
}

- (void)thumbsButtonTapped:(UIButton *)button
{
	[delegate tappedInToolbar:self thumbsButton:button];
}

- (void)markButtonTapped:(UIButton *)button
{
	[delegate tappedInToolbar:self markButton:button];
}

//-(void)markRemindButtonTapped:(UIButton *)button
//{
//    if(markRemindView == nil){
//         CGRect bookmarkFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
//        markRemindView = [[UIViewBookmarkReminder alloc] initWithFrame:bookmarkFrame];
//
//    }
//    else{
//        [markRemindView removeFromSuperview];
//        markRemindView = nil;
//    }
//}
//
//-(BOOL)isShowMarkReminder{
//    return markRemindView!=nil? YES:NO;
//}

@end

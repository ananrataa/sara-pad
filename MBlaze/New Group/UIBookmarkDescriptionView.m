//
//  UIBookmarkDescriptionView.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/19/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "UIBookmarkDescriptionView.h"
#import "HexColor.h"
#import "Utility.h"

@implementation UIBookmarkDescriptionView

#define BUTTON_WIDTH 70.f
#define BUTTON_HEIGHT 36.f
-(instancetype)initWithFrame:(CGRect)frame{
     if (self = [super initWithFrame:frame]) {
         self.clipsToBounds = YES;
         self.backgroundColor = [UIColor whiteColor];
    
         UIView *navigateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 48.0f)];
         navigateView.backgroundColor = [HexColor SKColorHexString:@"A9A9A9"];
         
         self.backBtn = [[UIButton alloc] initWithFrame:CGRectMake(8.f, 6.f, BUTTON_WIDTH, BUTTON_HEIGHT)];
         [self.backBtn setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
         [self.backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [self.backBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
         self.backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
         
         self.saveBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-(BUTTON_WIDTH+8.f), 6.f, BUTTON_WIDTH, BUTTON_HEIGHT)];
         [self.saveBtn setTitle:[Utility NSLocalizedString:@"Save"] forState:UIControlStateNormal];
         [self.saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [self.saveBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
         self.saveBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
         
         UILabel *reminder = [[UILabel alloc] initWithFrame:CGRectMake(8.f+8.f+BUTTON_WIDTH, 6.f, self.frame.size.width-(8.f+BUTTON_WIDTH+BUTTON_WIDTH), BUTTON_HEIGHT)];
         reminder.text = [Utility NSLocalizedString:@"Reminder"];
         reminder.textColor = [UIColor whiteColor];
         reminder.font = [UIFont systemFontOfSize:20.f];
         reminder.textAlignment = NSTextAlignmentCenter;
         
         self.textLength = [[UILabel alloc] initWithFrame:CGRectMake(0, 48.0f, frame.size.width-8.f, 25.f)];
         self.textLength.backgroundColor = [UIColor whiteColor];
         self.textLength.font = [UIFont systemFontOfSize:[Utility fontSize:15.f]];
         self.textLength.textAlignment = NSTextAlignmentRight;
         self.textLength.text = [NSMutableString stringWithString: @"0/250"];
         self.textLength.textColor = [UIColor darkGrayColor];
         
         self.bookmarkTextView =  [[UITextView alloc] initWithFrame:CGRectMake(8.f, 48.0f + 25.f + 8.f, frame.size.width-16.f, frame.size.height-(48.0f + 25.f + 8.f))];
         self.bookmarkTextView.backgroundColor = [UIColor whiteColor];
         self.bookmarkTextView.font = [UIFont systemFontOfSize:[Utility fontSize:16.f]];
         [self.bookmarkTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
         self.bookmarkTextView.text = @"";
         self.bookmarkTextView.editable = NO;
         self.textLength.textColor = [UIColor blackColor];
         
         [navigateView addSubview:self.backBtn];
         [navigateView addSubview:reminder];
         [navigateView addSubview:self.saveBtn];
         
         [self addSubview:navigateView];
         [self addSubview:self.textLength];
         [self addSubview:self.bookmarkTextView];
     }
    
    return self;
}

@end

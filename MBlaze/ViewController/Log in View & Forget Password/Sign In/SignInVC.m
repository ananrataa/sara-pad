//
//  SignInVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SignInVC.h"
#import "SVProgressHUD.h"
#import "HexColor.h"
#import "DBManager.h"
#import "Utility.h"
#import "ConstantValues.h"
#import "SplashScreen.h"
#import "MemberInfo.h"
#import "RegisterMemberVC.h"
#import "ForgotPasswordVC.h"
#import "ThemeTemplate.h"
#import "AddMoreMemberInfoVC.h"
#import "SARA_PAD-Swift.h"

@interface SignInVC ()

@end

@implementation SignInVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:NULL forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
    titleVC.text = [Utility NSLocalizedString:@"SigninTitle"];
    
    if ([UIDevice.modelName containsString:@"iPad"]) {
        CGFloat w = [[UIScreen mainScreen] bounds].size.width;
        leftConstraint.constant = (w - 315)/2;
        rightConstraint.constant = (w - 315)/2;
    }
    
    userDefault = [NSUserDefaults standardUserDefaults];
    api = [[ServiceAPI alloc] init];
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
    
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:dummyView];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveViewController:)];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    
    /// user name ///
    inputUserName.delegate = self;
    inputUserName.text = @"";
    inputUserName.textColor = [UIColor whiteColor];
    inputUserName.placeholder = [Utility NSLocalizedString:@"user_email"];
    [inputUserName setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    inputUserName.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:inputUserName.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear = [inputUserName valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    /// password ///
    inputPassword.delegate = self;
    inputPassword.text = @"";
    inputPassword.textColor = [UIColor whiteColor];
    inputPassword.placeholder = [Utility NSLocalizedString:@"password"];
    [inputPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    inputPassword.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:inputPassword.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear1 = [inputPassword valueForKey:@"clearButton"];
    [btnClear1 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    [signInButton setTitle:[Utility NSLocalizedString:@"SignIn"] forState:UIControlStateNormal];
    signInButton.layer.cornerRadius = 18.f;
    [signInButton addTarget:self action:@selector(signInTapping:) forControlEvents:UIControlEventTouchUpInside];
    [signInButton setBackgroundColor:[HexColor SKColorHexString:@"d44c27"]];
    
    [forgotPasswordButton setTitle:[Utility NSLocalizedString:@"Forgot_Password"] forState:UIControlStateNormal];
    [forgotPasswordButton addTarget:self action:@selector(forgotPasswordTapping:) forControlEvents:UIControlEventTouchUpInside];
    
    [registerButton setTitle:[Utility NSLocalizedString:@"RegisterTitle"] forState:UIControlStateNormal];
    registerButton.layer.cornerRadius = 18.f;
    registerButton.layer.borderWidth = 1.0f;
    registerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    registerButton.backgroundColor = [UIColor clearColor];
    [registerButton addTarget:self action:@selector(registerMember:) forControlEvents:UIControlEventTouchUpInside];
    
    lbSkipSignin.hidden = YES;

    //UITextField
    UIColor *tint = [UIColor colorWithWhite:0.8 alpha:1];
    [inputUserName setLeftImageWithImage:[UIImage imageNamed:@"user"] tint:tint];
    [inputPassword setLeftImageWithImage:[UIImage imageNamed:@"password"] tint:tint];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapSkipSignin:(UITapGestureRecognizer *) recognizer{
     [self prepareToLeaveViewController];
}

-(void)leaveViewController:(UIButton*)sender{
    [self prepareToLeaveViewController];
}

-(void)prepareToLeaveViewController{
    [inputUserName resignFirstResponder];
    [inputPassword resignFirstResponder];
    
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    [self.tabBarController.tabBar setHidden:NO];
    if (self.isOwnNavigationBar)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}



- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == inputUserName) {
        [inputPassword becomeFirstResponder];
    }
    else if (textField == inputPassword){
        //NSLog(@"----- prepare for Sign in --------");
        if ([[inputUserName text] length] > 0)
            [self prepare4Signin:[inputUserName text]
                        Password:[inputPassword text]];
    }
    return YES;
}

#pragma mark --------- Register Member ---------
-(void)registerMember:(UIButton *)sender{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    RegisterMemberVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterMemberVC"];
    [vc setIsOwnNavigationBar:self.isOwnNavigationBar? YES:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --------- forgot Password ---------
-(void)forgotPasswordTapping:(UIButton *)sender{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    ForgotPasswordVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVC"];
    [vc setIsOwnNavigationBar:self.isOwnNavigationBar? YES:NO];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark --------- sign in ---------
-(void)signInTapping:(UIButton *)sender{
    //NSLog(@"----- prepare for Sign in --------");
    if ([[inputUserName text] length] > 0) {
        [self prepare4Signin:[inputUserName text]
                    Password:[inputPassword text]];
    } else {
        [lblError setText:[Utility NSLocalizedString:@"signinFailed"]];
        [sender jitter];
        [lblError flash];
    }
    
}

#pragma markkkk ------------------ Sign in --------------------------------
-(void)prepare4Signin:(NSString*)uID Password:(NSString*)password{
    //api/DevicePlatform/Signin?emailOrUsername={emailOrUsername}&password={password}&deviceCode={deviceCode}
    
    [inputUserName resignFirstResponder];
    [inputPassword resignFirstResponder];
    
    [SVProgressHUD show];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signinResponse:) name:@"Signin" object:nil];
   
    [api signin:@"Signin" UserName:uID Password:password];
}

-(void)signinResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    //NSLog(@"Signin Response : %@",[notification.userInfo description]);
    [SVProgressHUD dismiss];
    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]) {
        
        [DBManager deleteAllRowInMemberTable];
        [DBManager deleteAllRowInLibraryTable];
        
        if ([[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0) {
            [SVProgressHUD dismiss];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
            AddMoreMemberInfoVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMoreMemberInfoVC"];
            [vc setMemberID:[notification.userInfo objectForKey:@"MEMBERID"]];
            [vc setPassword:[inputPassword text]];
            [vc setEmail:[notification.userInfo objectForKey:@"INSTANCESIGN"]];
            [vc setUserNameSigin:[notification.userInfo objectForKey:@"USERNAME"]];
            [vc setProfileUrl:@""];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
             //insert member info
            MemberInfo *member = [[MemberInfo alloc] init];
            [member MemberInfo:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEMBERID"]]
                    SigninName:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"USERNAME"]]
                      Password:[inputPassword text]
                   DisplayName:@""
                         Email:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"INSTANCESIGN"]]
                     Telephone:@""
                   DateOfBirth:@""
                    ProfileUrl:@""
                    LastSignin:@""
                           Etc:@""];
            
            [Utility insertMemberInfo:member VC:self];
        }
    } else {
        [MemberInfo setIsSignIn:NO];
        [lblError setText:[Utility NSLocalizedString:@"signinFailed"]];
        [signInButton jitter];
        [lblError flash];
    }
}

#pragma mark ------ facebookRegister ----------
- (IBAction)facebookRegisterTapping:(id)sender{
    //Logout user from facebook
    FBSDKLoginManager *fbManager = [[FBSDKLoginManager alloc] init];
    [fbManager logOut];
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        [self fetchuserinfo];
    } else {
        //FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [fbManager setDefaultAudience:FBSDKDefaultAudienceEveryone];
        [fbManager logInWithPublishPermissions:@[@"publish_actions"]//@[@"public_profile", @"email"]
                               fromViewController:self
                                          handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                              if (error) {
                                                  NSLog(@"Process error");
                                              }
                                              else if ([result.declinedPermissions containsObject:@"publish_actions"]) {
                                                  NSLog(@"declinedPermissions");
                                              }
                                              else if (result.isCancelled) {
                                                  NSLog(@"Cancelled");
                                              } else {
                                                  NSLog(@"Logged in");
                                                  [self fetchuserinfo];
                                              }
                                          }];
    }
}

-(void)fetchuserinfo {
    if ([FBSDKAccessToken currentAccessToken]){
        //token available : EAAWG899ElswBACZB5SAgJyQhYazZAZBpXkifcZB8bwv38Gg8tMdTwSPZCpdzcZCF6BvcP5QsrtAEvRczYZCrfhAHxxbE5unsZCZARR8aREACvMS5Oenj2dxe3XlNLhkVtu8evJIGcgYI0XYk6I7xaxBapMRURoSoUKblK6kbvQoKJxZCtF4eakj2gXZCRgmbTDXF09clD7VYK4wFZBf2RwZBMLejz3ey0OfZAZA0KJFMENyjZCyWNQZDZD
        //NSLog(@"token available : %@",[[FBSDKAccessToken currentAccessToken] tokenString]);
        FBSDKGraphRequest *fbRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}];
        [SVProgressHUD dismiss];
        [fbRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error){
                NSDictionary *dictResult = (NSDictionary*)result;
                NSString *strProfileImageUrl = [[[dictResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                
                dictRegistInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [[FBSDKAccessToken currentAccessToken] tokenString],@"AuthenToken",
                                  [dictResult objectForKey:@"id"],@"USERID",
                                  [dictResult objectForKey:@"name"],@"DISPLAYNAME",
                                  strProfileImageUrl,@"ProfileImageURL", nil];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialRegisterResponse:) name:@"RegisterSaraPad" object:nil];
                
                [api registerSaraPad:@"RegisterSaraPad" isByEmail:NO info:dictRegistInfo];
                
            }
            else{
                NSLog(@"error %@",error);
            }
        }];
    }
}

#pragma mark ----- lineRegisterTapping ------
- (IBAction)lineRegisterTapping:(id)sender{
    //cancel Login by Line App
//    [[LineSDKLogin sharedInstance] canLoginWithLineApp];

    // Set the LINE Login Delegate
    [LineSDKLogin sharedInstance].delegate = self;
    NSArray *permissions = @[@"profile", @"openid", @"email"];
    [[LineSDKLogin sharedInstance] startLoginWithPermissions:permissions];
//    [[LineSDKLogin sharedInstance] startLogin];
}

#pragma mark LineSDKLoginDelegate

- (void)didLogin:(LineSDKLogin *)login
      credential:(LineSDKCredential *)credential
         profile:(LineSDKProfile *)profile
           error:(NSError *)error {
    
    [SVProgressHUD dismiss];
    if (error) {
        // Login failed with an error. Use the error parameter to identify the problem.
        NSLog(@"Error: %@", error.localizedDescription);
        [self showAlertViewController:@"Error" Message:error.localizedDescription];
    }
    else {
        
        // Login success. Extracts the access token, user profile ID, display name, status message, and profile picture.
        NSURL * pictureURL = profile.pictureURL;
        NSString * pictureUrlString;
        // If the user does not have a profile picture set, pictureURL will be nil
        if (pictureURL) {
            pictureUrlString = profile.pictureURL.absoluteString;
        }
        
        dictRegistInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                          credential.accessToken.accessToken,@"AuthenToken",
                          profile.userID,@"USERID",
                          profile.displayName,@"DISPLAYNAME",
                          pictureUrlString,@"ProfileImageURL", nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialRegisterResponse:) name:@"RegisterSaraPad" object:nil];
        
        [api registerSaraPad:@"RegisterSaraPad" isByEmail:NO info:dictRegistInfo];
    }
}

-(void)showAlertViewController:(NSString*)title Message:(NSString*)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:msg
                                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark ------ Social Response -----
-(void)socialRegisterResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RegisterSaraPad" object:nil];
   // NSLog(@"Register Response : %@",[notification.userInfo description]);
    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        if ([[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
            AddMoreMemberInfoVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMoreMemberInfoVC"];
            [vc setMemberID:[notification.userInfo objectForKey:@"MEMBERID"]];
            [vc setProfileUrl:[dictRegistInfo objectForKey:@"ProfileImageURL"]];
            [vc setDisplayName:[dictRegistInfo objectForKey:@"DISPLAYNAME"]];
            [vc setPassword:@""];
            [vc setUserNameSigin:@""];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else if (![[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0){
            //insert member info
            MemberInfo *member = [[MemberInfo alloc] init];
            [member MemberInfo:[notification.userInfo objectForKey:@"MEMBERID"]
                    SigninName:@""
                      Password:@""
                   DisplayName:[Utility covertNsNullToNSString:[dictRegistInfo objectForKey:@"DISPLAYNAME"]]
                         Email:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"INSTANCESIGN"]]
                     Telephone:@""
                   DateOfBirth:@""
                    ProfileUrl:[Utility covertNsNullToNSString:[dictRegistInfo objectForKey:@"ProfileImageURL"]]
                    LastSignin:@""
                           Etc:@""];
            [Utility insertMemberInfo:member VC:self];
            
        }
        else
            [self showAlertViewController:@"Error" Message:[Utility NSLocalizedString:@"RegisterError"]];
        
    }
    else
        [self showAlertViewController:@"Error" Message:[Utility NSLocalizedString:@"RegisterError"]];
}

@end

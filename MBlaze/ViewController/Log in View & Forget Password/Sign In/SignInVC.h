//
//  SignInVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <LineSDK/LineSDK.h>

@class UITextFieldX;

@interface SignInVC : UIViewController <UITextFieldDelegate,LineSDKLoginDelegate>{

    __weak IBOutlet  UITextField *inputUserName;
    __weak IBOutlet  UITextField *inputPassword;
    __weak IBOutlet  UIButton *signInButton;
    __weak IBOutlet  UIButton *forgotPasswordButton;
    __weak IBOutlet UILabel *lblError;
    
   // __weak IBOutlet  UIView *signInView;
    
    __weak IBOutlet  UIButton *registerButton;

    __weak IBOutlet UILabel *titleVC;
    
    __weak IBOutlet UILabel *lbSkipSignin;
    
    __weak IBOutlet  UIButton *fbButtonRegister;
    __weak IBOutlet  UIButton *lineButtonRegister;
    
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    
    __weak IBOutlet NSLayoutConstraint *rightConstraint;
    ServiceAPI *api;
    
    NSUserDefaults *userDefault;
    
    NSDictionary *dictRegistInfo;
}

@property (nonatomic, assign)BOOL isOwnNavigationBar;


- (IBAction)facebookRegisterTapping:(id)sender;
- (IBAction)lineRegisterTapping:(id)sender;


@end

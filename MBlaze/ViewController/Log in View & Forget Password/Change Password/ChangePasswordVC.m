//
//  ChangePasswordVC.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/28/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "Utility.h"
#import "HexColor.h"
#import "ConstantValues.h"
#import "DBManager.h"
#import "ThemeTemplate.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:NULL forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
    UIFont *font = [UIFont fontWithName:mainFont size:21];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                    NSFontAttributeName:font
                                                                    };
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    self.title = [Utility NSLocalizedString:@"settings"];
    titleVC.text = [Utility NSLocalizedString:@"ChangePassTitle"];
    despTitle.text = [Utility NSLocalizedString:@"ChangePassDesc"];
    
    if ([UIDevice.modelName containsString:@"iPad"]) {
        CGFloat w = [[UIScreen mainScreen] bounds].size.width;
        leftConstraint.constant = (w - 315)/2;
        rightConstraint.constant = (w - 315)/2;
    }
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveViewController:)];
    [leftBarButton setTintColor:[UIColor whiteColor]];
    //leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    
    //navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveToRootViewController:)];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapping:)];
    tap.delegate = (id)self;
    [self.view addGestureRecognizer:tap];
    
    //curr pass
    textFieldCurrPassword.delegate = self;
    textFieldCurrPassword.text = @"";
    textFieldCurrPassword.textColor = [UIColor whiteColor];
    textFieldCurrPassword.placeholder = [Utility NSLocalizedString:@"Curr_Password"];
    [textFieldCurrPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldCurrPassword.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:textFieldCurrPassword.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear0 = [textFieldCurrPassword valueForKey:@"clearButton"];
    [btnClear0 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    /// new pass ///
    textFieldNewPassword.delegate = self;
    textFieldNewPassword.text = @"";
    textFieldNewPassword.textColor = [UIColor whiteColor];
    textFieldNewPassword.placeholder = [Utility NSLocalizedString:@"New_Password"];
    [textFieldNewPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldNewPassword.clearButtonMode = UITextFieldViewModeAlways;
     [Utility setFont:textFieldNewPassword.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear = [textFieldNewPassword valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    /// confirm password ///
    textFieldComfirmPassword.delegate = self;
    textFieldComfirmPassword.text = @"";
    textFieldComfirmPassword.textColor = [UIColor whiteColor];
    textFieldComfirmPassword.placeholder = [Utility NSLocalizedString:@"Confirm_Password"];
    [textFieldComfirmPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldComfirmPassword.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:textFieldComfirmPassword.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear1 = [textFieldComfirmPassword valueForKey:@"clearButton"];
    [btnClear1 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    [OkButton setTitle:[Utility NSLocalizedString:@"Submit_Review"] forState:UIControlStateNormal];
    OkButton.clipsToBounds = YES;
    OkButton.layer.cornerRadius = 18.f;
    [OkButton addTarget:self action:@selector(changePasswordTapping:) forControlEvents:UIControlEventTouchUpInside];
    [OkButton setBackgroundColor:[HexColor SKColorHexString:@"d44c27"]];
    [Utility setFont:OkButton.titleLabel.font WithFontSize:20.f];
    
    
    UIColor *tint = [UIColor colorWithWhite:0.8 alpha:1];
    [textFieldCurrPassword setLeftImageWithImage:[UIImage imageNamed:@"password"] tint:tint];
    [textFieldNewPassword setLeftImageWithImage:[UIImage imageNamed:@"new_password"] tint:tint];
    [textFieldComfirmPassword setLeftImageWithImage:[UIImage imageNamed:@"new_password"] tint:tint];
    
    api = [[ServiceAPI alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [textFieldCurrPassword becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)viewTapping:(UITapGestureRecognizer *)recgnizer{
    [textFieldCurrPassword resignFirstResponder];
    [textFieldNewPassword resignFirstResponder];
    [textFieldComfirmPassword resignFirstResponder];
}

-(void)leaveToRootViewController:(UIButton*)sender{
    [self prepare4LeaveToRootViewController:1];
}

-(void)prepare4LeaveToRootViewController:(int)flag{
    [textFieldCurrPassword resignFirstResponder];
    [textFieldNewPassword resignFirstResponder];
    [textFieldComfirmPassword resignFirstResponder];
    
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    if (flag == 0)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)leaveViewController:(UIButton*)sender{
     [self prepare4LeaveToRootViewController:0];
}

-(void)changePasswordTapping:(UIButton *)sender{
    if ([[textFieldCurrPassword text] length] > 0  && [[textFieldNewPassword text] length] > 0 && [[textFieldComfirmPassword text] length] > 0){
        if ([textFieldNewPassword.text isEqualToString:textFieldComfirmPassword.text]) {
            [textFieldCurrPassword resignFirstResponder];
            [textFieldNewPassword resignFirstResponder];
            [textFieldComfirmPassword resignFirstResponder];
            [self validatePassword];
        } else {
            [self alertError:[Utility NSLocalizedString:@"PassNotMatch"]];
        }
    } else {
        [self alertError:[Utility NSLocalizedString:@"AddMoreInfoError"]];
    }
}
-(void)alertError:(NSString*)message {
    [lblError setText:message];
    [OkButton jitter];
    [lblError flash];
}
#pragma UITextField delegate
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldCurrPassword) {
        [textFieldNewPassword becomeFirstResponder];
    }
    if (textField == textFieldNewPassword) {
        [textFieldComfirmPassword becomeFirstResponder];
    }
    else if (textField == textFieldComfirmPassword){
        if ([[textFieldCurrPassword text] length] > 0  && [[textFieldNewPassword text] length] > 0 && [[textFieldComfirmPassword text] length] > 0){
            [textField resignFirstResponder];
            [self validatePassword];
        }
           
    }
    return YES;
}

-(void)validatePassword{
    ///validate curr pass
    if ([[textFieldCurrPassword text] isEqualToString:[DBManager selectMemberPassword]]) {
        if ([[textFieldNewPassword text] isEqualToString:[textFieldComfirmPassword text]]) {
            //validate password lenght
            if ([[textFieldNewPassword text] length] >= 8) {
                ///validate spacial character
                if(![self checkSpacialCharacter:[textFieldNewPassword text]] && ![self stringContainsEmoji:[textFieldNewPassword text]])
                    [self prepare4ChangePassword:[textFieldCurrPassword text] ConfirmPassword:[textFieldNewPassword text]];
                else{
                    [self alertError:[Utility NSLocalizedString:@"PasswordFormatError"]];
                }
            }
            else{
                [self alertError:[Utility NSLocalizedString:@"PasswordTooShot"]];
            }
            
        }
        else{
            [self alertError:[Utility NSLocalizedString:@"NewPasswordNotMatch"]];
        }
    }
    else{
        [self alertError:[Utility NSLocalizedString:@"CurrPasswordNotMatch"]];
    }
}


-(BOOL)checkSpacialCharacter:(NSString*)str{
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:=_{}[],.<>?\\/|\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if ([str.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        //NSLog(@"contains special characters or number");
        return YES;
    }
    else
        return NO;
}

-(BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    
    return returnValue;
}



-(void)prepare4ChangePassword:(NSString *)currPass ConfirmPassword:(NSString *)newPass{
    //api/DevicePlatform/ChangePassword?deviceCode={deviceCode}&memberID={memberID}&password={password}&newPassword={newPassword}
    memberID = [DBManager selectMemberID];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePasswordResult:) name:@"ChangePassword_" object:nil];
    [api changePassword:@"ChangePassword_" MemberID:memberID CurrPasswprd:currPass NewPassWord:newPass];
    
}

-(void)changePasswordResult:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangePassword_" object:nil];
    //NSLog(@"changePassword Result : %@",[notification.userInfo description]);    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///update new password to DB
        [DBManager updatePassword:[textFieldNewPassword text] MemberID:memberID];
        [SVProgressHUD showSuccessWithStatus:[Utility NSLocalizedString:@"ChangePasswordSuccess"]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
        });
    }
    else{
        [self alertError:[Utility NSLocalizedString:@"ChangePasswordError"]];
    }
    
}

@end

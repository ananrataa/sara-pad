//
//  ChangePasswordVC.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/28/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface ChangePasswordVC : UIViewController <UITextFieldDelegate>{
    
    
    //__weak IBOutlet UILabel *vcSubTitleLabel;
   // __weak IBOutlet UILabel *vcTitleLabel;
    
    __weak IBOutlet UITextField *textFieldCurrPassword;
    __weak IBOutlet UITextField *textFieldNewPassword;
    __weak IBOutlet UITextField *textFieldComfirmPassword;
    __weak IBOutlet UIButton *OkButton;
    
  //  __weak IBOutlet UIButton *exitButton;

    __weak IBOutlet UILabel *titleVC;
    __weak IBOutlet UILabel *despTitle;
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    
    __weak IBOutlet NSLayoutConstraint *rightConstraint;
    __weak IBOutlet UILabel *lblError;
    ServiceAPI *api;
    
    NSString *memberID;
    
}

@end

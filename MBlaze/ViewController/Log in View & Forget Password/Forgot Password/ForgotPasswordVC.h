//
//  ForgotPasswordVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface ForgotPasswordVC : UIViewController <UITextFieldDelegate>{
    
    __weak IBOutlet  UITextField *inputEmail;
    __weak IBOutlet  UIButton *OkButton;
    __weak IBOutlet UILabel *lblError;
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    __weak IBOutlet NSLayoutConstraint *rightConstraint;
    
    ServiceAPI *api;
    
    __weak IBOutlet UILabel *titleVC;
    __weak IBOutlet UILabel *despTitle;
    
}
@property (nonatomic, assign)BOOL isOwnNavigationBar;

@end

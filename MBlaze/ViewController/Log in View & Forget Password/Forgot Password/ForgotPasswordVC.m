//
//  ForgotPasswordVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/18/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "CustomActivityIndicator.h"
#import "ConstantValues.h"
#import "Utility.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "SARA_PAD-Swift.h"
#import "SVProgressHUD.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:NULL forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    titleVC.text = [Utility NSLocalizedString:@"Forgot_Password"];
    despTitle.text = [Utility NSLocalizedString:@"SubTitleForgotPass"];
    
    if ([UIDevice.modelName containsString:@"iPad"]) {
        CGFloat w = [[UIScreen mainScreen] bounds].size.width;
        leftConstraint.constant = (w - 315)/2;
        rightConstraint.constant = (w - 315)/2;
    }
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveViewController:)];
    [leftBarButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveToRootViewController:)];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    inputEmail.delegate = self;
    inputEmail.text = @"";
    inputEmail.textColor = [UIColor whiteColor];
    inputEmail.placeholder = [Utility NSLocalizedString:@"EmailForgotPass"];
    [inputEmail setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    inputEmail.clearButtonMode = UITextFieldViewModeAlways;
     [Utility setFont:inputEmail.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear = [inputEmail valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    OkButton.layer.cornerRadius = 18.f;
    [OkButton setTitle:[Utility NSLocalizedString:@"OK"] forState:UIControlStateNormal];
    [OkButton addTarget:self action:@selector(gogetPasswordTapping:) forControlEvents:UIControlEventTouchUpInside];
    [OkButton setBackgroundColor:[HexColor SKColorHexString:@"d44c27"]];
    [Utility setFont:OkButton.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
    
    UIImageView *imgView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email"]];
    imgView1.frame = CGRectMake(0, 0, 30, 30);
    [imgView1 setTintColor:[UIColor colorWithWhite:0.8 alpha:1]];
    [imgView1 setContentMode:UIViewContentModeScaleAspectFill];
    [inputEmail setLeftView:imgView1];
    [inputEmail setLeftViewMode:UITextFieldViewModeAlways];
    
    api = [[ServiceAPI alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [inputEmail becomeFirstResponder];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leaveToRootViewController:(UIButton*)sender{
    [inputEmail resignFirstResponder];
    
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    [self.tabBarController.tabBar setHidden:NO];
    if (self.isOwnNavigationBar)
        [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)leaveViewController:(UIButton*)sender{
    [inputEmail resignFirstResponder];
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   // NSLog(@"textFieldShouldReturn:");
    
    if ([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0)
        [self prepare4RequestPassword:textField.text];
    
    return YES;
}


#pragma mark ---------- forgot password --------
-(void)gogetPasswordTapping:(UIButton*)sender{
    if ([[inputEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0) {
        [self prepare4RequestPassword:inputEmail.text];
    } else {
        lblError.text = [Utility NSLocalizedString:@"SubTitleForgotPass"];
        [OkButton jitter];
        [lblError flash];
    }
}


-(void)prepare4RequestPassword:(NSString *)email{
    //api/DevicePlatform/ForgetMemberPassword?emailAddress={emailAddress}
    [SVProgressHUD show];
    if ([Utility validateEmailFormat:email]) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getForgotPasswordResponse:) name:@"ForgotPassword" object:nil];
        
        [api forgetMemberPassword:@"ForgotPassword" Email:email];
        
    } else {
        [SVProgressHUD dismiss];
        lblError.text = [Utility NSLocalizedString:@"EmailFailed"];
        [OkButton jitter];
        [lblError flash];
    }
        
}

-(void)getForgotPasswordResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForgotPassword" object:nil];
    //NSLog(@"forgot password response : %@",[notification.userInfo objectForKey:@"Response"]);
    
    if ([[notification.userInfo objectForKey:@"Response"] isEqualToString:@"null"]) {
        [SVProgressHUD showSuccessWithStatus:[Utility NSLocalizedString:@"forget_email_success"]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self leaveToRootViewController:NULL];
        });
    } else {
        [SVProgressHUD showErrorWithStatus:[notification.userInfo objectForKey:@"Response"]];
    }
}


@end

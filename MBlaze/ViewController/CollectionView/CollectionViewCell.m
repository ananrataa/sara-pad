//
//  CollectionViewCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/1/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.mediaTypePic.clipsToBounds = YES;
    self.mediaTypePic.layer.cornerRadius = self.mediaTypePic.frame.size.height/2;
    
    self.mediaCoverImageView.layer.cornerRadius = 7;
    self.mediaCoverImageView.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
    self.mediaCoverImageView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.mediaCoverImageView.layer.borderWidth = 0.35;
    
    
    //Border
    self.bgView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.bgView.layer.borderWidth = 0.35;
    self.bgView.layer.cornerRadius = 7;
    self.bgView.layer.masksToBounds = YES;
    
    //shadow
    self.contentView.layer.shadowOpacity = 0.35;
    self.contentView.layer.cornerRadius = 7;
    self.contentView.layer.shadowOffset = CGSizeMake(1, 2);
}

@end

//
//  OfflineMediaCollectionCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/30/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "OfflineMediaCollectionCell.h"

@implementation OfflineMediaCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.mediaTypePic.clipsToBounds = YES;
    self.mediaTypePic.layer.cornerRadius = self.mediaTypePic.frame.size.height/2;
}

@end

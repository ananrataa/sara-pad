//
//  OfflineMediaCollectionCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/30/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfflineMediaCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mediaCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *mediaTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mediaAuthorLabel;

@property (weak, nonatomic) IBOutlet UIImageView *mediaTypePic;
@property (weak, nonatomic) IBOutlet UIImageView *videoImagePic;
@property (weak, nonatomic) IBOutlet UILabel *mediaExpLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@end

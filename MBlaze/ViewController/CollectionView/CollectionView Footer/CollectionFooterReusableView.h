//
//  CollectionFooterReusableView.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 16/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionFooterReusableView : UICollectionReusableView

@end

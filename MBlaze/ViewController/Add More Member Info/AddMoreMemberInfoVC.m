//
//  AddMoreMemberInfoVC.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/4/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.

#import "ThemeTemplate.h"
#import "AddMoreMemberInfoVC.h"
#import "HexColor.h"
#import "Utility.h"
#import "DBManager.h"
#import "MemberInfo.h"
#import "SplashScreen.h"
#import "ConstantValues.h"
#import "MemberInfo.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface AddMoreMemberInfoVC ()

@end

@implementation AddMoreMemberInfoVC

#define strDateFormate @"dd/MM/yyyy"

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveViewController:)];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBarButton;

    
    if ([UIDevice.modelName containsString:@"iPad"]) {
        CGFloat w = [[UIScreen mainScreen] bounds].size.width;
        leftConstraint.constant = (w - 315)/2;
        rightConstraint.constant = (w - 315)/2;
    }
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:dummyView];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    titleVC.text = [Utility NSLocalizedString:@"AddMoreInfoTitle"];
    
    // textField Nickname //
    textFieldNickname.delegate = self;
    textFieldNickname.text = @"";
    textFieldNickname.textColor = [UIColor whiteColor];
    textFieldNickname.placeholder = [Utility NSLocalizedString:@"DisplayName"];
    [textFieldNickname setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldNickname.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:textFieldNickname.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear = [textFieldNickname valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];

    // textField Email ///
    textFieldEmail.delegate = self;
    textFieldEmail.text = @"";
    textFieldEmail.textColor = [UIColor whiteColor];
    textFieldEmail.placeholder = [Utility NSLocalizedString:@"email"];
    [textFieldEmail setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldEmail.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:textFieldEmail.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear1 = [textFieldEmail valueForKey:@"clearButton"];
    [btnClear1 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    // textField Date of Birth ///
    textFieldDateOfBirth.delegate = self;
    textFieldDateOfBirth.text = @"";
    textFieldDateOfBirth.textColor = [UIColor whiteColor];
    textFieldDateOfBirth.placeholder = [Utility NSLocalizedString:@"DateOfBirth"];
    [textFieldDateOfBirth setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textFieldDateOfBirth.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:textFieldDateOfBirth.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *btnClear2 = [textFieldDateOfBirth valueForKey:@"clearButton"];
    [btnClear2 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    [btnSend setTitle:[Utility NSLocalizedString:@"OK"] forState:UIControlStateNormal];
    btnSend.layer.cornerRadius = 18.f;
    [btnSend addTarget:self action:@selector(sendInfoToService:) forControlEvents:UIControlEventTouchUpInside];
    [btnSend setBackgroundColor:[HexColor SKColorHexString:@"d44c27"]];
    
    //UITextField
    UIColor *tint = [UIColor colorWithWhite:0.8 alpha:1];
    [textFieldNickname setLeftImageWithImage:[UIImage imageNamed:@"user"] tint:tint];
    [textFieldEmail setLeftImageWithImage:[UIImage imageNamed:@"email"] tint:tint];
    [textFieldDateOfBirth setLeftImageWithImage:[UIImage imageNamed:@"birth_calendar"] tint:tint];
    
    api = [[ServiceAPI alloc] init];
    
    [self memberDetail];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)leaveViewController:(UIButton*)sender{
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    [self.tabBarController.tabBar setHidden:NO];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark --------- get member Detail --------------
-(void)memberDetail{
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMemberDetailResponse:) name:@"MemberDetail" object:nil];
    [api getMembersDetailAsync:@"MemberDetail" MemberID:self.memberID];
}

-(void)getMemberDetailResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberDetail" object:nil];
    //NSLog(@"Member Detail Response : %@",[notification.userInfo description]);
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
         NSDictionary *memberInfo = [notification.userInfo objectForKey:@"Member"];
        if ([Utility isJsonDictionaryNotNull:memberInfo]) {
            
            if ([[Utility covertNsNullToNSString:[memberInfo objectForKey:@"EMAIL"]] length] > 0 && [Utility validateEmailFormat:[memberInfo objectForKey:@"EMAIL"]]) {
                
                textFieldEmail.text = [memberInfo objectForKey:@"EMAIL"];
            }
            
            if ([[Utility covertNsNullToNSString:[memberInfo objectForKey:@"MEMBER_FNAME"]] length] > 0) {
                textFieldNickname.text = [memberInfo objectForKey:@"MEMBER_FNAME"];
            }
            
             NSString *dateOfBirth = [Utility covertDateToFormat:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"BRTHDATE"]] InputFormat:@"yyyy-MM-dd'T'HH:mm:ss" OutputFormat:@"dd/MM/yyyy"];
            
            if ([dateOfBirth length] > 0) {
                textFieldDateOfBirth.text = dateOfBirth;
            }
        }
    }
    
    [SVProgressHUD dismiss];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == textFieldNickname){
        [textFieldEmail becomeFirstResponder];
    }
    
    if(textField == textFieldNickname){
        [textFieldDateOfBirth becomeFirstResponder];
    }
     return YES;
}


- (IBAction)showDatePicker:(id)sender {
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    NSString *maxDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    // alloc/init your date picker, and (optional) set its initial date
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datePicker setDate:[dateFormatter dateFromString:maxDateString]];
    [datePicker setCalendar:[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian]];
    [datePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];

    // converting string to date
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerSelectDone:)];
    [toolBar setItems:@[flexibleSpace,btnDone] animated:YES];
    [toolBar setBarStyle:UIBarStyleDefault];
    
    [textFieldDateOfBirth setInputAccessoryView:toolBar];
    [textFieldDateOfBirth setInputView:datePicker];
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{

    textFieldDateOfBirth.text = [[self getDateFormatter] stringFromDate:datePicker.date];
}

-(void)datePickerSelectDone:(UIBarButtonItem *)sender{
    [textFieldDateOfBirth resignFirstResponder];
}


-(void)sendInfoToService:(UIButton *) sender{
    [textFieldNickname resignFirstResponder];
    [textFieldEmail resignFirstResponder];
    [textFieldDateOfBirth resignFirstResponder];
    [SVProgressHUD show];
    if ([[textFieldNickname text] length] > 0 && [[textFieldEmail text] length] > 0 && [Utility validateEmailFormat:[textFieldEmail text]]) {
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMemberInfoResponse:) name:@"UpdateMemberInfo" object:nil];
        
        NSString *strDateOfBirth = [Utility covertDateToFormat:[textFieldDateOfBirth text] InputFormat:strDateFormate OutputFormat:@"yyyy-MM-dd"];
        
        dateOfBirth = [Utility covertDateToFormat:[textFieldDateOfBirth text] InputFormat:strDateFormate OutputFormat:@"dd/MM/yyyy"];
        
        NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.memberID,@"MEMBERID",[textFieldNickname text],@"FName",[textFieldEmail text],@"EMAIL",strDateOfBirth,@"DateOfBirth", nil];
        
        self.displayName = [textFieldNickname text];
        self.email = [textFieldEmail text];
        
        [api updateMemberProFile:@"UpdateMemberInfo" info:dictInfo];
        
    }
    else{
        if (![Utility validateEmailFormat:[textFieldEmail text]]) {
            [self alertErrorMessage:@"Email ไม่ถูกต้อง"];
        } else {
            [self alertErrorMessage:[Utility NSLocalizedString:@"AddMoreInfoError"]];
        }
    }
    
}
-(void)alertErrorMessage:(NSString*)message {
    [SVProgressHUD dismiss];
    [lblError setText:message];
    [btnSend jitter];
    [lblError flash];
}
-(void)updateMemberInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateMemberInfo" object:nil];
    
     [SVProgressHUD dismiss];
    
     if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
         
         if (![[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0) {
            //insert member info
             MemberInfo *member = [[MemberInfo alloc] init];
             [member MemberInfo:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEMBERID"]]
                     SigninName:self.userNameSigin
                       Password:self.password
                    DisplayName:self.displayName
                          Email:self.email
                      Telephone:@""
                    DateOfBirth:dateOfBirth
                     ProfileUrl:self.profileUrl
                     LastSignin:@""
                            Etc:@""];
             [Utility insertMemberInfo:member VC:self];
             [self leaveViewController:NULL];
         } else if ([notification.userInfo objectForKey:@"MessageText"]) {
             [self alertErrorMessage:[notification.userInfo objectForKey:@"MessageText"]];
         }else {
             [self alertErrorMessage:[Utility NSLocalizedString:@"ToastFailed"]];
         }
     } else {
         [self alertErrorMessage:[Utility NSLocalizedString:@"ToastFailed"]];
     }
}


-(NSDateFormatter*)getDateFormatter{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = strDateFormate;
    [dateFormatter setCalendar:[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    return dateFormatter;
}

@end

//
//  AddMoreMemberInfoVC.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 26/4/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface AddMoreMemberInfoVC : UIViewController<UITextFieldDelegate>{
    
    __weak IBOutlet UILabel *titleVC;
    __weak IBOutlet UITextField *textFieldNickname;
    __weak IBOutlet UITextField *textFieldEmail;
     __weak IBOutlet UITextField *textFieldDateOfBirth;
    
    __weak IBOutlet UIButton *btnSend;
    __weak IBOutlet UILabel *lblError;
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    __weak IBOutlet NSLayoutConstraint *rightConstraint;
    
    ServiceAPI *api;
    
    UIDatePicker *datePicker;
    
    NSUserDefaults *userDefault;
    
    NSString *dateOfBirth;
    
}

- (IBAction)showDatePicker:(id)sender;

@property(strong,nonatomic)NSString* memberID;
@property(strong,nonatomic)NSString* password;
@property(strong,nonatomic)NSString* userNameSigin;
@property(strong,nonatomic)NSString* email;
@property(strong,nonatomic)NSString* displayName;
@property(strong,nonatomic)NSString* profileUrl;

@end

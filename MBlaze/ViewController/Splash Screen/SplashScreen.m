//
//  ViewController.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SplashScreen.h"
#import "ConstantValues.h"
#import "DBManager.h"
#import "Utility.h"
#import "Room.h"
#import "Ads.h"
#import "ArrayOfAds.h"
#import "MemberInfo.h"
#import "OfflineShelf.h"
#import "ThemeTemplate.h"
#import "SDImageCache.h"
#import "SARA_PAD-Swift.h"

@interface SplashScreen ()

@end

@implementation SplashScreen

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    NSLog(@"Device Code : %@",deviceCode);
    api = [[ServiceAPI alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showStatusBar:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self checkAppVersion];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self showStatusBar:YES];
}
-(void)reloadSplashScreen{
    [self showStatusBar:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppVersion" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberRemainLogin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberDetail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TokenID" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DeviceRegister" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Rooms" object:nil];
    [self checkAppVersion];
}
-(void)showStatusBar:(BOOL)show {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        if (window) {
            if (show) {
                window.windowLevel = UIWindowLevelNormal;
            } else {
                window.windowLevel = UIWindowLevelStatusBar + 1;
            }
        }
    });
}
#pragma mark ----- check app version ------
-(void)checkAppVersion{
    [self needsUpdate];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkAppVersionResponse:) name:@"AppVersion" object:nil];
//    [api appVersion:@"AppVersion"];
}

-(void) needsUpdate {
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    if (data) {
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([lookup[@"resultCount"] integerValue] == 1) {
            NSString* appStoreVersion = lookup[@"results"][0][@"version"];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            NSArray *appStoreVersionArray = [appStoreVersion componentsSeparatedByString:@"."];
            NSArray *currentVersionArray = [currentVersion componentsSeparatedByString:@"."];
            NSMutableString *appStoreVersionValue = [NSMutableString stringWithString:@""];
            NSMutableString *currentVersionValue = [NSMutableString stringWithString:@""];
            for (NSString *n in appStoreVersionArray) {
                [appStoreVersionValue appendString:n];
            }
            for (NSString *n in currentVersionArray) {
                [currentVersionValue appendString:n];
            }
            if ([currentVersionValue intValue] >= [appStoreVersionValue intValue]) {
                [userDefault setValue:CurrentAppVersion forKey:LatestAppVersionKey];
                [userDefault synchronize];
                [self appVersionCleared];
            } else {
                [self showAlertViewToUpdateApplication];
            }
        } else {
            [self appVersionCleared];
        }
    } else {
        [self appVersionCleared];
    }
}
-(void)appVersionCleared {
    isMember = [DBManager isMember];
    memberInfo = [[NSDictionary alloc] init];
    if (isMember) {
        //[self checkMemberRemainLogin];
        NSLog(@"is Member");
        memberInfo = [DBManager selectMemberInfo];
        [MemberInfo setIsSignIn:YES];
        memberID = [memberInfo objectForKey:@"MEMBERID"];
        [self memberDetail];
    } else{
        NSLog(@"is User");
        memberInfo = [NSDictionary dictionaryWithObjectsAndKeys:[userDefault objectForKey:KeyDeviceCode],@"MEMBERID", nil];
        [MemberInfo setIsSignIn:NO];
        [self deviceRegister];
    }
}
-(void)checkAppVersionResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppVersion" object:nil];
    NSLog(@"AppVersion Response : %@",[notification.userInfo description]);
    if ([Utility isServiceResponseError:notification.userInfo])
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
    else{
        //Response
        if ([Utility isJsonDictionaryNotNull:notification.userInfo]){
            BOOL versionUptodate = [[notification.userInfo objectForKey:@"Response"] boolValue];
            if (!versionUptodate) {
                 [self showAlertViewToUpdateApplication];
            } else{
                [userDefault setValue:CurrentAppVersion forKey:LatestAppVersionKey];
                [userDefault synchronize];
                
                isMember = [DBManager isMember];
                memberInfo = [[NSDictionary alloc] init];
                if (isMember) {
                   //[self checkMemberRemainLogin];
                    NSLog(@"is Member");
                    memberInfo = [DBManager selectMemberInfo];
                    [MemberInfo setIsSignIn:YES];
                    memberID = [memberInfo objectForKey:@"MEMBERID"];
                    [self memberDetail];
                } else{
                    NSLog(@"is User");
                    memberInfo = [NSDictionary dictionaryWithObjectsAndKeys:[userDefault objectForKey:KeyDeviceCode],@"MEMBERID", nil];
                    [MemberInfo setIsSignIn:NO];
                    [self deviceRegister];
                }
            }
        }
    }
}

#pragma mark ----- Member Remain Login ------
-(void)checkMemberRemainLogin{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isMemberLoginResponse:) name:@"MemberRemainLogin" object:nil];
    [api isMemberRemainLogin:@"MemberRemainLogin" MemberID:memberID];
}

-(void)isMemberLoginResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberRemainLogin" object:nil];
    if ([Utility isServiceResponseError:notification.userInfo])
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
    
    else if ([Utility isJsonDictionaryNotNull:notification.userInfo] && [[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200 && [[notification.userInfo objectForKey:@"MessageText"] isEqualToString:@"is Signin !"]){
        
        NSLog(@"is Member");
        memberInfo = [DBManager selectMemberInfo];
        [MemberInfo setIsSignIn:YES];
        memberID = [memberInfo objectForKey:@"MEMBERID"];
        [self memberDetail];
        //[self prepare4Signin:[memberInfo objectForKey:@"USERNAME"] Password:[memberInfo objectForKey:@"PASSWORD"]];
    }
    else{
        NSLog(@"is User");
        [DBManager deleteAllRowInMemberTable];
        [DBManager deleteAllRowInLibraryTable];
        
        memberInfo = [NSDictionary dictionaryWithObjectsAndKeys:[userDefault objectForKey:KeyDeviceCode],@"MEMBERID", nil];
        [MemberInfo setIsSignIn:NO];
        [self deviceRegister];
    }
}


#pragma mark --------- get member Detail --------------
-(void)memberDetail{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMemberDetailResponse:) name:@"MemberDetail" object:nil];
    [api getMembersDetailAsync:@"MemberDetail" MemberID:memberID];
}

-(void)getMemberDetailResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberDetail" object:nil];
    //NSLog(@"Member Detail Response : %@",[notification.userInfo description]);
    if ([Utility isServiceResponseError:notification.userInfo])
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
    
    else{
        
        if ([Utility isJsonDictionaryNotNull:notification.userInfo]){
            NSDictionary *memberInfo = [notification.userInfo objectForKey:@"Member"];
            NSString *lastSigin = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"Lastlogin"]];
            if ([Utility isJsonDictionaryNotNull:memberInfo]) {
                //Update Member info
                NSString *dateOfBirth = [Utility covertDateToFormat:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"BRTHDATE"]] InputFormat:@"yyyy-MM-dd'T'HH:mm:ss" OutputFormat:@"dd/MM/yyyy"];
                
                MemberInfo *member = [[MemberInfo alloc] init];
                [member MemberInfo:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"MEMBERID"]]
                        SigninName:nil
                          Password:nil
                       DisplayName:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"MEMBER_FNAME"]]
                             Email:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"EMAIL"]]
                         Telephone:[Utility covertNsNullToNSString:[memberInfo objectForKey:@"PHONENUMBER"]]
                       DateOfBirth:dateOfBirth
                        ProfileUrl:@""
                        LastSignin:lastSigin
                               Etc:@""];
                [DBManager insertMemberInfo:member];
                //CoreData
                MemberData *memberData = [DB addMemberWithInfo:memberInfo];
                if (memberData) {
                    memberData.lastLogin = lastSigin;
                    [DB saveData];
                }
            }
            
            [self deviceRegister];
        } else {
            [self showAlertViewController:nil Message:[Utility NSLocalizedString:@"SomethingWrong"]];
        }
    }
}

#pragma mark ---------- get Token ID -----------------
-(void)getTokenID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTokenIDRespones:) name:@"TokenID" object:nil];
    [api getToken:@"TokenID" MemberID:memberID];
}

-(void)getTokenIDRespones:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TokenID" object:nil];
    //NSLog(@"get Token ID Response : %@",[notification.userInfo description]);
    [self deviceRegister];
}

#pragma markk ---------------- DeviceRegister ------------------
-(void)deviceRegister{
    //api/DevicePlatform/DeviceRegister?memberId={memberId}&deviceCode={deviceCode}&deviceDetail={deviceDetail}
    
    NSString *model = [Utility deviceName];
    NSString *osVersion = [NSString stringWithFormat:@"%.1f",[[[UIDevice currentDevice] systemVersion] floatValue]];
    float monitorSize = [Utility deviceDisplaySize];
    NSString *monitorType = @"S";
    if (monitorSize < 5.0)
        monitorType = @"M";
    else if (monitorSize >= 5.0)
         monitorType = @"L";
    
    // = @"L";//monitorSize>=4.7f? @"L" : @"M";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceRegisterResponse:) name:@"DeviceRegister" object:nil];
    
    NSString *mmID = [[memberInfo objectForKey:@"MEMBERID"] isEqualToString:deviceCode]? @"":[memberInfo objectForKey:@"MEMBERID"];
    
    NSString *notiTokenID = [[userDefault objectForKey:NotificationTokenID] length] > 0? [userDefault objectForKey:NotificationTokenID]:@"\"\"";
    
    NSString *deviceDetails = [NSString stringWithFormat:@"%@,%@,%@,%@",@"Apple",model,@"IOS",osVersion];
    [api deviceRegister:@"DeviceRegister" MemberID:mmID DeviceDetail:deviceDetails MonitorSize:monitorSize MonitorType:monitorType NotificationTokenID:notiTokenID];
}

-(void)deviceRegisterResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DeviceRegister" object:nil];
    //NSLog(@"deviceRegister Response : %@",[notification.userInfo objectForKey:@"Response"]);
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
    }
    else{
        [self getRooms];
    }
}

#pragma mark ----------- get Rooms -----------------
-(void)getRooms{
    
    NSString *mID = [[memberInfo objectForKey:@"MEMBERID"] isEqualToString:deviceCode]? @"":[memberInfo objectForKey:@"MEMBERID"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRoomsResponse:) name:@"Rooms" object:nil];
    [api roomList:@"Rooms" MemberID:mID];
}

-(void)getRoomsResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Rooms" object:nil];
    //NSLog(@"get Rooms Response : %@",[notification.userInfo description]);
    
    currRoomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
    }
    else if ([Utility isJsonDictionaryNotNull:notification.userInfo]){
        
    
        [OfflineShelf onlyOfflineModeEnable:NO];
        [DBManager deleteAllRowInLibraryTable];
        //coreData delete rooms
        [DB clearRooms];
        
        
        NSString *mID = [[memberInfo objectForKey:@"MEMBERID"] isEqualToString:deviceCode]? @"":[memberInfo objectForKey:@"MEMBERID"];
        for (NSDictionary *obj in notification.userInfo) {
            /// set new room default by service
            if ([currRoomID length] > 0) {
                NSString *dateRegist = [DBManager selectLibraryDateRegisterByID:currRoomID];
                if (![[Utility covertNsNullToNSString:[obj objectForKey:@"DATE"]] isEqualToString:@""] &&
                    ([[obj objectForKey:@"DATE"] intValue] > [dateRegist intValue])) {
                    
                    //// set new default room id
                    [userDefault setValue:[obj objectForKey:@"ROOMID"] forKey:KeyCuurrentUsedLribaryID];
                    [userDefault synchronize];
                }
            }
            else{
                // set new default room id
                [userDefault setValue:[obj objectForKey:@"ROOMID"] forKey:KeyCuurrentUsedLribaryID];
                [userDefault synchronize];
            }
            
            Room *room = [[Room alloc] init];
            [room Room:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMID"]]
              RoomCode:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMCODE"]]
              RoomName:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMNAME"]]
                Symbol:[Utility covertNsNullToNSString:[obj objectForKey:@"SYMBOL"]]
               Default:[Utility covertNsNullToNSString:[obj objectForKey:@"DEFAULT"]]
            RoomTypeID:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMTYPEID"]]
          RoomTypeCode:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMTYPECODE"]]
          RoomTypeName:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMTYPENAME"]]
          MessaageText:[Utility covertNsNullToNSString:[obj objectForKey:@"MessaageText"]]
                 Index:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMINDEX"]]
          DateRegister:[Utility covertNsNullToNSString:[obj objectForKey:@"DATE"]]];
            
            [DBManager insertRoomInfo:room];
            //CoreData
            RoomData *roomData = [DB addRoomWithInfo:obj curRoom:nil];
            //Room's Theme
            if ([[obj objectForKey:@"THEME"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *themObj = [NSDictionary dictionaryWithDictionary:[obj objectForKey:@"THEME"]];
                NSDictionary *themeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET1"]],@"Set1",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET2"]],@"Set2",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET3"]],@"Set3",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET4"]],@"Set4",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET5"]],@"Set5",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET6"]],@"Set6",
                                           [Utility covertNsNullToNSString:[themObj objectForKey:@"SET7"]],@"Set7",nil];
                [ThemeTemplate saveTheme:themeDict RoomID:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMID"]]];
                
                //Save CoreData
//                MemberData *member = [DB getMemberWithId:mID];
//                RoomData *roomData = [member addRoomWithInfo:obj];
                if (roomData) {
                    roomData.thememBarBackground = [themeDict valueForKey: @"Set1"];
                    roomData.themeBarForeground = [themeDict valueForKey: @"Set2"];
                    roomData.themeButtonBackground = [themeDict valueForKey: @"Set3"];
                    roomData.themeButtonForeground = [themeDict valueForKey: @"Set4"];
                    roomData.themeTint = [themeDict valueForKey: @"Set6"];
                    [DB saveData];
                }
            }
        }
        
    }
    
    [ThemeTemplate loadTheme:[userDefault objectForKey:KeyCuurrentUsedLribaryID]];
    [self manageOfflineShelf];
}

#pragma mark --------- Manage Media On Offline Shelf -----------
-(void)manageOfflineShelf{
    //clear all cache image
    NSInteger imgCacheSize = (long)[[SDImageCache sharedImageCache] findDiskUsage];
    //NSLog(@"Image Cache Size : %ld",imgCacheSize);
    if (imgCacheSize > 250000000) {
        [[SDImageCache sharedImageCache]clearMemory];
        [[SDImageCache sharedImageCache]clearDisk];
    }
    
    

    // remove move media expired///
//    [Utility checkToRemoveExpiredMediaOnShelf];
   
    NSString *mID = [[memberInfo objectForKey:@"MEMBERID"] isEqualToString:deviceCode]? @"":[memberInfo objectForKey:@"MEMBERID"];
    NSString *currRoomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    
//    [DBManager recheckMediaExpirOnShelfAtDirectoryName:mID InDirectoryPath:[NSString stringWithFormat:@"%@/%@",sarapadFloderName,currRoomID] LibraryID:currRoomID];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SplashScreen" object:nil];
    
}

-(void)showAlertViewController:(NSString*)title Message:(NSString*)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* tryButton = [UIAlertAction
                                actionWithTitle:[Utility NSLocalizedString:@"Try_Again"]
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction * action)
                                {
                                    [OfflineShelf onlyOfflineModeEnable:YES];
                                    [self checkAppVersion];
                                    
                                }];
    
    UIAlertAction* offlineMode = [UIAlertAction
                                  actionWithTitle:[Utility NSLocalizedString:@"Offline_Mode"]
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [OfflineShelf onlyOfflineModeEnable:YES];
                                      [ThemeTemplate setDefaultTheme];
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"SplashScreen" object:nil];
                                  }];

    [alert addAction:tryButton];
    [alert addAction:offlineMode];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)showAlertViewToUpdateApplication {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"A New version is available. Please update"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
//    UIAlertAction* cancelButton = [UIAlertAction
//                                actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
//                                style:UIAlertActionStyleDestructive
//                                handler:^(UIAlertAction * action)
//                                {
//                                }];
//    
    UIAlertAction* updateButton = [UIAlertAction
                                  actionWithTitle:[Utility NSLocalizedString:@"Update"]
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      NSString *url = @"itms-apps://itunes.apple.com/app/id1308779366";
                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:NULL];
                                  }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"later"]
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [self appVersionCleared];
                                                   }];
    [alert addAction:updateButton];
    [alert addAction:cancle];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

@end

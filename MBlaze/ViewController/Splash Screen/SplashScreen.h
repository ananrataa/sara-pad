//
//  ViewController.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "ServiceAPI.h"

@interface SplashScreen : UIViewController {
    //__weak UITabBarController *tabBarController;
    //NSArray *tabBarItems;
    
     Utility *utility;
    
    float tabbar_height;
    
     NSUserDefaults *userDefault;
     ServiceAPI *api;
    
     NSDictionary *memberInfo;
     NSMutableDictionary *memberDic;
    
     BOOL isMember;
    
     NSString *solutionId;
     NSString *memberID;
     NSString *deviceCode;
     NSString *currRoomID;

    
//    __weak IBOutlet UIActivityIndicatorView *spiner;
//    __weak IBOutlet UILabel *labelAlert;
    
//    __weak IBOutlet UIImageView *imageBackgroundSlide;
//    __weak IBOutlet UIImageView *sarapadLogoImage;
//
//    __weak IBOutlet NSLayoutConstraint *imageBgSlideConstraint;
    
   __weak IBOutlet UIImageView *imageSplashScreen;
}



@end


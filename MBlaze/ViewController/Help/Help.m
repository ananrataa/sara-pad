//
//  Help.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 11/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Help.h"
#import "SVProgressHUD.h"
#import "Utility.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "ConstantValues.h"

@interface Help ()

@end

@implementation Help

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"Help"]];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
    
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:dummyView];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    [self prepare4LoadWebPage];
}

-(void)dismissViewController:(id)sender{
    if (mWebView != nil)
        [mWebView stopLoading];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepare4LoadWebPage{
    errorView.hidden = YES;
    
    [SVProgressHUD show];
    
    NSURL *url = [NSURL URLWithString:HelpUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    mWebView.delegate = self;
    [mWebView loadRequest:urlRequest];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [SVProgressHUD dismiss];
    errorLabel.text = [Utility NSLocalizedString:@"WebPageOpenTitle"];
    errorView.hidden = NO;
   
}

-(void)reloadWebPage:(UIButton *)sender{
    [self prepare4LoadWebPage];
}

@end

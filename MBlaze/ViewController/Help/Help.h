//
//  Help.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 11/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Help : UIViewController <UIWebViewDelegate>{
    
    __weak IBOutlet UIWebView *mWebView;
    
    __weak IBOutlet UILabel *errorLabel;
    
    __weak IBOutlet UIButton *reloadWebPageBtn;
    
    __weak IBOutlet UIView *errorView;
    
    //UIBarButtonItem *leftBarButton;
}

@end

//
//  LibraryDetailVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaFileCache.h"
#import "ServiceAPI.h"
@interface LibraryDetailVC : UICollectionViewController{
    NSString *deviceCode;
    
    UIBarButtonItem *leftBarButton;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    //NSData *libraryProfilePicData;
    
    //MediaFileCache *mCache;
    //NSMutableDictionary *mediaCacheDic;
    
    NSInteger infoLabelHeight;
    NSInteger emailLabelHeight;
    NSInteger telLabelHeight;
    NSInteger tagLabelHeight;
    
    NSMutableArray *arrayOfMedia;
    NSArray *courseMedias;
    NSArray *filteredMedia;
    
    BOOL isLoadingMore;
    BOOL isViewFirstLoad;
    BOOL isNoMoreToLoad;
    int loadTimeIndex;
    
    ServiceAPI *api;
    
    UIColor *bgColor;
    UIColor *fgColor;
    //NSMutableDictionary *imageUrlCache;
}

@property (nonatomic, assign)BOOL isOwnNavigationBar;
@property (strong, nonatomic)NSString* libraryTitle;
@property (strong, nonatomic)NSString* libraryID;
@property (strong, nonatomic)NSDictionary *jsonLibraryInfo;
@property (strong, nonatomic)NSArray *jsonMediaItems;

@end

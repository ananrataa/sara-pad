//
//  LibraryDetailVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "LibraryDetailVC.h"
#import "Utility.h"
#import "CollectionViewCell.h"
#import "LibraryInfoViewCell.h"
#import "CollectionHeaderView2.h"
#import "ConstantValues.h"
#import "CustomActivityIndicator.h"
#import "HexColor.h"
#import "UIImageView+WebCache.h"
#import "ThemeTemplate.h"
#import "DBManager.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface LibraryDetailVC ()

@end

@implementation LibraryDetailVC

#define cellMediaIdentifier  @"mediaContentCell"
#define cellInfoIdentifier  @"libraryInfoCell"
#define cellHeaderIdentifier @"collectionViewHeader2"
#define cellHeaderId @"cell_header"
#define infoViewMinHeight 270

@synthesize libraryID;
@synthesize libraryTitle;
@synthesize jsonLibraryInfo;
@synthesize jsonMediaItems;

BOOL isCourse;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Register Header cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionHeaderView2" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LibMediaCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderId];

    /////change collectionView Layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(8,8,0,8);
    layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
    layout.minimumInteritemSpacing = 5;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:layout];
    
    leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[UIColor blackColor]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    
    arrayOfMedia = [[NSMutableArray alloc] init];
    
    isViewFirstLoad = YES;
    isLoadingMore = NO;
    
    api = [[ServiceAPI alloc] init];
    loadTimeIndex = 1;
    
    [SVProgressHUD show];
    [self setTheme];
    [self calculateTextLabelHeight];
    [self getMediaItemsFromJson];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationItem setTitle:libraryTitle];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)setTheme{
    //Theme
    NSDictionary *theme = [jsonLibraryInfo objectForKey:@"THEME"];
    if ([Utility isJsonDictionaryNotNull:theme]) {
        //update Theme for Media
        bgColor = [HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET1"]]];
        fgColor = [HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET2"]]];
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:fgColor, NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
        self.navigationController.navigationBar.barTintColor = bgColor;
        
        [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET2"]]]];
    }
    
}

-(void)leftTopBarTapping:(id)sender{
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    if (self.isOwnNavigationBar)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
    
    //[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return (isCourse)? [courseMedias count] : [filteredMedia count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self.collectionView registerNib:[UINib nibWithNibName:@"LibraryInfoViewCell" bundle:nil] forCellWithReuseIdentifier:cellInfoIdentifier];
        
        LibraryInfoViewCell *cell = (LibraryInfoViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellInfoIdentifier forIndexPath:indexPath];
        
        
        ////Title Label
        cell.libraryInfoTitleLabel.text = [Utility NSLocalizedString:@"Info"];
        cell.libraryEmailTitleLabel.text = [Utility NSLocalizedString:@"Email"];
        cell.libraryTelTitleLabel.text = [Utility NSLocalizedString:@"Contact"];
        cell.libraryTagTitleLabel.text = [Utility NSLocalizedString:@"Tag"];
        
        cell.libraryInfoTitleBodyLabel.text = [[jsonLibraryInfo objectForKey:@"DESP"] length]>0? [jsonLibraryInfo objectForKey:@"DESP"] : [NSMutableString stringWithString: @"-  "];
        cell.libraryInfoTitleBodyLabel.numberOfLines = 5;
        
        cell.libraryEmailBodyLabel.text = [[jsonLibraryInfo objectForKey:[NSMutableString stringWithString: @"EMAIL"]] length]>0? [jsonLibraryInfo objectForKey:@"EMAIL"] : [NSMutableString stringWithString: @"-  "];
        cell.libraryEmailBodyLabel.numberOfLines = 2;
        
        cell.libraryTelBodyLabel.text = [NSMutableString stringWithString: [NSMutableString stringWithString: [self getTelephoneNumber]]];
        cell.libraryTelBodyLabel.numberOfLines = 2;
        
        cell.libraryTagBodyLabel.text = [[jsonLibraryInfo objectForKey:@"TAG"] length]>0? [jsonLibraryInfo objectForKey:@"TAG"] : @"-";
        cell.libraryTagBodyLabel.numberOfLines = 3;
        
        //[cell.libraryInfoProfilePic setImage:[UIImage imageNamed:@"default_library"]];
        cell.libraryInfoProfilePic.contentMode = UIViewContentModeScaleToFill;
        
        NSString *imgUrl = [jsonLibraryInfo objectForKey:@"SYMBOL"];
        //[cell.libraryInfoProfilePic sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
        [cell.libraryInfoProfilePic sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]  options:0 progress:nil completed:nil];
        
        //Member status
        NSInteger memStatus = [[jsonLibraryInfo valueForKey:@"MemSTT"] integerValue];
        NSString *regTitle = @"";
        switch (memStatus) {
            case 0:
                //Member
                [cell.registButton addTarget:self action:@selector(alreadyMember) forControlEvents:UIControlEventTouchUpInside];
                regTitle = [Utility NSLocalizedString:@"status_Member"];
                bgColor = [HexColor SKColorHexString:@"#8DC248"];
                fgColor = [UIColor blackColor];
                break;
            case 1:
            case 2:
                //Not Member
                //Guest
                [cell.registButton addTarget:self action:@selector(librayApply) forControlEvents:UIControlEventTouchUpInside];
                regTitle = [Utility NSLocalizedString:@"apply_now"];
                bgColor = [HexColor SKColorHexString:@"#30ABDF"];
                fgColor = [UIColor whiteColor];
                break;
            case 3:
                //Wait
                [cell.registButton addTarget:self action:@selector(waitStatus) forControlEvents:UIControlEventTouchUpInside];
                regTitle = [Utility NSLocalizedString:@"status_wait"];
                bgColor = [HexColor SKColorHexString:@"#CA1E7B"];
                fgColor = [UIColor whiteColor];
                break;
            default:
                break;
        }
        [cell.contentView setBackgroundColor:[bgColor colorWithAlphaComponent:0.15]];
        
        [cell.registButton setTitleColor:fgColor forState:UIControlStateNormal];
        cell.registButton.backgroundColor = bgColor;
        [cell.registButton setTitle:regTitle forState:UIControlStateNormal];
        cell.registButton.clipsToBounds = YES;
        cell.registButton.layer.cornerRadius = 7.f;
        
        UIView *uiView = [[UIView alloc] init];
        uiView.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = uiView;
        
        [cell setSelected:NO];
        return cell;
        
    } else {
        NSArray *medias = (isCourse)? [NSArray arrayWithArray:courseMedias] : [NSArray arrayWithArray:filteredMedia];
        [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellMediaIdentifier];
        
        CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
        NSDictionary *itemDic = [medias objectAtIndex:indexPath.row];
            
        NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
        
        [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"] options:0 progress:nil completed:nil];
        
        cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
        cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
        
        [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
        
        [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
        
        cell.videoImagePic.hidden = YES;
        [cell setSelected:YES];
        UIView *uiView = [[UIView alloc] init];
        uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
        [cell setSelectedBackgroundView:uiView];
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section != 0){
        [SVProgressHUD show];
        
        //// load data /////
        NSArray *medias = (isCourse)? [NSArray arrayWithArray:courseMedias] : [NSArray arrayWithArray:filteredMedia];
        NSDictionary *itemDic = [medias objectAtIndex:indexPath.row];
        [self loadMediaDetail:[itemDic objectForKey:@"MEDIAID"] RoomId:[itemDic objectForKey:@"MEDIA_ROOMID"]];
    }
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        float height =  infoViewMinHeight+infoLabelHeight+emailLabelHeight+telLabelHeight+tagLabelHeight + 44;
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width-16, height);
    }
    NSString *modelName = [UIDevice modelName];
    int n = [modelName containsString:@"iPad"] ? 4 : ([modelName containsString:@"iPhone 5"] ? 2 : 3);
    collectionCellWidth = (self.collectionView.frame.size.width - (n+1)*12)/n;
    collectionCellHeight = collectionCellWidth*4/3 + 90;

    return CGSizeMake(collectionCellWidth, collectionCellHeight);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionHeader && indexPath.section != 0) {
        if (courseMedias.count > 0) {
            LibMediaCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderId forIndexPath:indexPath];
            headerView.onSegmentSelected = ^(NSInteger index) {
                isCourse = (index == 0);
                [self.collectionView reloadData];
            };
            return headerView;
        } else {
            CollectionHeaderView2 *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderIdentifier forIndexPath:indexPath];
            headerView.backgroundColor = [UIColor whiteColor];
            
            UILabel *headerName = (UILabel*)[headerView.subviews firstObject];
            headerName.text =[Utility NSLocalizedString:@"Media"];
            headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
            
            UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
            seeMoreBtn.hidden = YES;
            return headerView;
        }
    } else {
        return [[UICollectionReusableView alloc] init];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"contentOffset y >> %.2f",(float)self.tableView.contentOffset.y);
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore && !isViewFirstLoad){
        isLoadingMore = YES;
        [SVProgressHUD showWithStatus:@"Loading..."];
        loadTimeIndex ++;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.8f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self loadMediaItemsInLibrary];
        });
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section != 0)
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 40.f);
    else
        return CGSizeMake(0.f, 0.f);
}

-(void)calculateTextLabelHeight{
    infoLabelHeight = [self calculateChatMessageHeight:[NSString stringWithFormat:@"information : %@",[jsonLibraryInfo objectForKey:@"DESP"]]];
    emailLabelHeight = [self calculateChatMessageHeight:[NSString stringWithFormat:@"อีเมล์ : %@",[jsonLibraryInfo objectForKey:@"EMAIL"]]];
    telLabelHeight = [self calculateChatMessageHeight:[NSString stringWithFormat:@"เบอร์โทรศัพท์ : %@",[self getTelephoneNumber]]];
    tagLabelHeight = [self calculateChatMessageHeight:[NSString stringWithFormat:@"Tag : %@",[jsonLibraryInfo objectForKey:@"TAG"]]];
}

- (float) calculateChatMessageHeight:(NSString *) chatText {
    chatText = [chatText length] > 0 ? chatText:@"XOXOXO";
    CGRect textRect = [chatText boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width-40, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:[Utility fontSize:18.f]]} context:nil];
    //NSLog(@"text : %@",chatText);
    //NSLog(@"Height : %.2f",textRect.size.height);
    return (textRect.size.height - 20.5);
}

-(NSString *)getTelephoneNumber {
    NSString *tel = @"";
    if([[jsonLibraryInfo objectForKey:@"TEL1"] length] > 0){
        tel = [jsonLibraryInfo objectForKey:@"TEL1"];
    }
    
    if ([[jsonLibraryInfo objectForKey:@"TEL2"] length] > 0){
        if ([tel length] > 0)
            tel = [NSString stringWithFormat:@"%@, %@",tel,[jsonLibraryInfo objectForKey:@"TEL2"]];
        else
            tel = [jsonLibraryInfo objectForKey:@"TEL2"];
    }
    if ([[jsonLibraryInfo objectForKey:@"TEL3"] length] > 0) {
        if ([tel length] > 0)
            tel = [NSString stringWithFormat:@"%@, %@",tel,[jsonLibraryInfo objectForKey:@"TEL3"]];
        else
            tel = [jsonLibraryInfo objectForKey:@"TEL3"];
    }
    return [tel length]>0? tel:@"-  ";
}

-(void)getMediaItemsFromJson{
    // NSLog(@"MediaItemsFromJson : %@",[jsonMediaItems description]);
    if(![Utility isServiceResponseError:jsonMediaItems] && [Utility isJsonDictionaryNotNull:jsonMediaItems]){
        for (NSDictionary *mediaItem in jsonMediaItems) {
            NSMutableDictionary *mulDicItem = [[NSMutableDictionary alloc] init];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                          forKey:@"MEDIAID"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]]
                          forKey:@"MEDIANAME"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                          forKey:@"AUTHOR"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                          forKey:@"POPULARVOTE"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                          forKey:@"MediaCoverUrl"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                          forKey:@"MEDIAKIND"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"ROOMID"]]
                          forKey:@"MEDIA_ROOMID"];
            
            [arrayOfMedia addObject:mulDicItem];
        }
        //filter medias
        NSPredicate *coursePredicate = [NSPredicate predicateWithFormat:@"MEDIAKIND == %@", @"CR"];
        courseMedias = [NSArray arrayWithArray:[arrayOfMedia filteredArrayUsingPredicate:coursePredicate]];
        NSPredicate *mediaPredicate = [NSPredicate predicateWithFormat:@"MEDIAKIND != %@", @"CR"];
        filteredMedia = [NSArray arrayWithArray:[arrayOfMedia filteredArrayUsingPredicate:mediaPredicate]];
        if (isViewFirstLoad && courseMedias.count > 0) {
            isCourse = YES;
        }
        [self.collectionView reloadData];
    }
    isViewFirstLoad = NO;
    isLoadingMore = NO;
    [SVProgressHUD dismiss];
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    if (loadTimeIndex == 1) {
        [arrayOfMedia removeAllObjects];
    }
    if (isNoMoreToLoad) {
        [SVProgressHUD dismiss];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsResponse:) name:@"MediaItemsInLibrary" object:nil];
        [api mediaItems:@"MediaItemsInLibrary" RoomId:libraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:loadTimeIndex AppVersion:CurrentAppVersion];
    }
}

-(void)loadMediaItemsResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    //NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    if ([Utility isServiceResponseError:notification.userInfo]){
        isViewFirstLoad = NO;
        isLoadingMore = NO;
        isNoMoreToLoad = YES;
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        jsonMediaItems = (NSArray*) notification.userInfo;
        [self getMediaItemsFromJson];
    } else {
        isViewFirstLoad = NO;
        isLoadingMore = NO;
        isNoMoreToLoad = YES;
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"no_more_data"] Controller:(UIViewController*)self];
    }
}
#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)mediaID  RoomId:(NSString *)room_id{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:room_id LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
//        MediaDetailTVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        //vc.libraryID = libraryID;
//        //vc.libraryName = libraryTitle;
//        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        vc.jsonData = notification.userInfo;
//        vc.rootNavigationController = self;
//
//        [self.navigationController pushViewController:vc animated:YES];
        self.title = @"";
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
}

-(void)alreadyMember {
    [SVProgressHUD showInfoWithStatus:@"คุณเป็นสมาชิกแล้ว"];
}
-(void)librayApply {
    NSDictionary *memberInfo = [DBManager selectMemberInfo];
    NSString *memberID = [memberInfo objectForKey:@"MEMBERID"];
    if (memberID == NULL) {
        [SVProgressHUD showInfoWithStatus:[Utility NSLocalizedString:@"register_warning"]];
    } else {
        [[RoomAPI shared] postLibraryApplyWithRoomId:libraryID memberId:memberID completion:^(NSDictionary<NSString *,id> *json) {
            NSInteger responseCode = [[json valueForKey:@"MessageCode"] integerValue];
            if (responseCode == 200) {
                NSString *path = [json valueForKey:@"MessageText"];
                NSString *titleText = [NSString stringWithFormat:@"%@: %@", [Utility NSLocalizedString:@"Library"], libraryTitle];
                WebOSViewController *vc = [[WebOSViewController alloc] initWithUrlPath:path title: titleText];
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [self presentViewController:nav animated:YES completion:NULL];
            } else {
                NSString * error = [json valueForKey:@"MessageText"];
                [SVProgressHUD showErrorWithStatus:error];
            }
        }];
    }
}
-(void)waitStatus {
    [SVProgressHUD showInfoWithStatus:@"ได้รับการสมัครของคุณแล้ว\nอยู่ระหว่างดำเนินการ\nกรุณาเช็คเมล์ของคุณ"];
}
@end

//
//  LibraryDetails.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaFileCache.h"

@interface LibraryDetails : UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate>{
    UIBarButtonItem *leftBarButton;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    MediaFileCache *mCache;
    NSMutableDictionary *mediaCacheDic;
    
    NSInteger infoLabelHeight;
    NSInteger emailLabelHeight;
    NSInteger telLabelHeight;
    NSInteger tagLabelHeight;
    
    NSMutableArray *arrayOfMedia;
}



@property (strong, nonatomic)NSString* libraryTitle;
@property (strong, nonatomic)NSString* libraryID;
@property (strong, nonatomic)NSDictionary *jsonData;
@end

//
//  LibraryDetails.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "LibraryDetails.h"
#import "Utility.h"
#import "LibraryDetailsCell.h"
#import "TableViewCell.h"
#import "ConstantValues.h"
#import "CollectionViewCell.h"
#import "CustomActivityIndicator.h"

@interface LibraryDetails ()

@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@end

@implementation LibraryDetails

@synthesize libraryID;
@synthesize libraryTitle;
@synthesize jsonData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:libraryTitle];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName :
                                                                        [UIColor blackColor]};
    
    /// calculate collection Cell Width & Height
    collectionCellWidth = ((self.tableView.frame.size.width-32)/3);
    collectionCellHeight =  ((collectionCellWidth*4)/3)+90;
    
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    CGSize navigationImageSize = CGSizeMake(navigationHeight-16, navigationHeight-16);
    
    UIView *bgViewNav = [[UIView alloc] initWithFrame:CGRectMake(0.0f, navigationHeight-2, [[UIScreen mainScreen] bounds].size.width, 2)];
    bgViewNav.backgroundColor = [UIColor blackColor];
    [self.navigationController.navigationBar addSubview:bgViewNav];
    
    leftBarButton = [[UIBarButtonItem alloc] initWithImage:[Utility rescaleImage:[UIImage imageNamed:@"arrow_back_black"] scaledToSize:navigationImageSize] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[UIColor blackColor]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 10);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    arrayOfMedia = [[NSMutableArray alloc] init];
    
    mCache = [[MediaFileCache alloc] init];
    mediaCacheDic = [[NSMutableDictionary alloc] init];
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leftTopBarTapping:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)getLibraryDetailFromJsonString{
 
    //// calculate height
    //mediaNameLabelHeight = [self calculateChatMessageHeight:[dictionaryOfMedail objectForKey:@"MEDIANAME"]];
    //mediaAuthorLabelHeight = [self calculateChatMessageHeight:[dictionaryOfMedail objectForKey:@"AUTHOR"]];
    //mediaCategoryLabelHeight = [self calculateChatMessageHeight:[dictionaryOfMedail objectForKey:@"SUBCATEGORIESNAME"]];
    //mediaAbstractLabelHeight = [self calculateChatMessageHeight:[dictionaryOfMedail objectForKey:@"ABSTRACT"]];
    
}


- (float) calculateChatMessageHeight:(NSString *) chatText {
    CGRect textRect = [chatText boundingRectWithSize:CGSizeMake(self.tableView.frame.size.width-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.f]} context:nil];
    //NSLog(@"Height : %.2f",textRect.size.height);
    return textRect.size.height;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        [self.tableView registerNib:[UINib nibWithNibName:@"LibraryDetailsCell" bundle:nil] forCellReuseIdentifier:@"libraryInfoCell"];
        LibraryDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"libraryInfoCell" forIndexPath:indexPath];
        
        return cell;
    }
    else{
        
        TableViewCell *cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier"];
        
        /////change collectionView Layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(8, 8, 0, 8);
        layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
        layout.minimumLineSpacing = 8;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        [cell.collectionView setCollectionViewLayout:layout];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }

}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section==1) {
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CollectionHeaderView" owner:self options:nil] objectAtIndex:0];
        float headerHeight = 40.0f;
        headerView.frame = CGRectMake(0.0f, 0.0f, [[UIScreen mainScreen] bounds].size.width, headerHeight);
        headerView.backgroundColor = [UIColor whiteColor];
        
        UILabel *headerName = (UILabel*)[headerView.subviews firstObject];
        headerName.text = @"หนังสือ";
        headerName.font = [UIFont fontWithName:mainFont size:20.0f];
        
        UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
        seeMoreBtn.hidden = YES;
        //        [seeMoreBtn setTitle:@"See more >>" forState:UIControlStateNormal];
        //        seeMoreBtn.titleLabel.font = [UIFont fontWithName:mainFont size:12.0f];
        //        [seeMoreBtn setTintColor:[UIColor blackColor]];
        //        seeMoreBtn.accessibilityLabel = headerName.text; //**//
        //        [seeMoreBtn addTarget:self action:@selector(seeMoreTapping:) forControlEvents:UIControlEventTouchUpInside];
        return headerView;
    }
    else
        return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1)
        return 50.0f;
    else
        return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return 245.f;
    }
    else
        return self.tableView.frame.size.height - (265.f+50.f);
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(TableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
        NSInteger index = cell.collectionView.indexPath.row;
        
        CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
        [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
    }
    
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return [arrayOfMedia count];
    
    return 20;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
//    NSDictionary *categoryDic = [arrayOfMedia objectAtIndex:collectionView.tag];
//    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
//    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
//    
//    [cell.mediaCoverImageView setImage:[UIImage imageNamed:@"default_media_cover.png"]];
//    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
//    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
//    
//    mCache = [mediaCacheDic objectForKey:[itemDic objectForKey:@"MEDIAID"]];
//    if ([Utility isImage:[mCache getCoverImageMedia]]) {
//        //NSLog(@"Image Data : %lu",(unsigned long)[[mCache getCoverImageMedia] length]);
//        [cell.mediaCoverImageView setImage:[UIImage imageWithData:[mCache getCoverImageMedia]]];
//    }
//    else{
//        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//        dispatch_async(queue, ^{
//            NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
//            NSData * imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
//            if ([Utility isImage:imgData]) {
//                MediaFileCache *cache = [[MediaFileCache alloc] init];
//                [cache MediaFileCacheWithMediaID:[itemDic objectForKey:@"MEDIAID"] NSData:imgData];
//                [mediaCacheDic setValue:cache forKey:[itemDic objectForKey:@"MEDIAID"]];
//                
//                dispatch_sync(dispatch_get_main_queue(), ^{
//                    [cell.mediaCoverImageView setImage:[UIImage imageWithData:imgData]];
//                    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"spider_man_three_ver7.jpg"]];
//                    
//                });
//            }
//        });
//        
//    }
//    
//    
//    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
//    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
//    
//    [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
//    
//    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
//    
//    if ([[[itemDic objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"])
//        cell.videoImagePic.hidden = NO;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [UIColor lightGrayColor];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    [CustomActivityIndicator presentActivityIndicatorView:@"loading..."];
//    //// load data ////
//    NSDictionary *categoryDic = [arrayOfMedia objectAtIndex:collectionView.tag];
//    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
//    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
//    //// add temp data
//    arrayOfMediaDetail = [NSMutableArray arrayWithObjects:[itemDic objectForKey:@"MEDIAID"],curUsedLibraryID,[itemDic objectForKey:@"MEDIAKIND"],[itemDic objectForKey:@"MEDIANAME"], nil];
//    
//    [self loadMediaDetail:curUsedLibraryID MediaID:[itemDic objectForKey:@"MEDIAID"]];
//    
//    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    
    AFIndexedCollectionView *collectionView = (AFIndexedCollectionView *)scrollView;
    NSInteger index = collectionView.indexPath.row;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
}



@end

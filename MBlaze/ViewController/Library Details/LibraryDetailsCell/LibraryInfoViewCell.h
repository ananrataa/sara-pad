//
//  LibraryInfoViewCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LibraryInfoViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *libraryInfoProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *libraryInfoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryInfoTitleBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryEmailTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryEmailBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryTelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryTelBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryTagTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryTagBodyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelWidthConstraint;

@property (weak, nonatomic) IBOutlet UIButton *registButton;


@end

//
//  LibraryInfoViewCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "LibraryInfoViewCell.h"
#import "Utility.h"
#import "ConstantValues.h"
@implementation LibraryInfoViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat r = self.libraryInfoProfilePic.frame.size.height/2;
    self.libraryInfoProfilePic.layer.cornerRadius = r;
//    self.libraryInfoProfilePic.layer.shadowOpacity = 0.25;
//    self.libraryInfoProfilePic.layer.shadowOffset = CGSizeMake(2, 2);
    
    self.contentView.layer.cornerRadius = 7;
}

@end

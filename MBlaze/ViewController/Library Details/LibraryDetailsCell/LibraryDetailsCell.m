//
//  LibraryDetailsCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "LibraryDetailsCell.h"

@implementation LibraryDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.libraryInfoProfilePic.clipsToBounds = YES;
    self.layer.cornerRadius = self.libraryInfoProfilePic.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

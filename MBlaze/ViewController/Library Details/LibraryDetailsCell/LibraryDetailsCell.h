//
//  LibraryDetailsCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/6/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibraryDetailsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *libraryInfoProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *libraryInfoTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryInfoTitleBodyLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryEmailTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryEmailBodyLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryTelTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryTelBodyLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryTagTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryTagBodyLabel;


@end

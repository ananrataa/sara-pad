//
//  LeftSlideMenuVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/25/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "LeftSlideMenuVC.h"
#import "ConstantValues.h"

#import "UserInfoTVCell.h"
#import "UserLibraryTVCell.h"
#import "UserMoreSettingTVCell.h"

#import "HexColor.h"
#import "AppDelegate.h"
#import "HistoryTVC.h"
#import "MemberInfo.h"
#import "Room.h"
#import "DBManager.h"
#import "Utility.h"
#import "SigninVC.h"
#import "OfflineShelf.h"
#import "ThemeTemplate.h"
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface LeftSlideMenuVC () {
    NSString* curBagValue ;
}
@end

@implementation LeftSlideMenuVC

#define infoCellIndentifier @"userInfoCell"
#define userLibraryCellIndentifier @"userLibraryCell"
#define userMoreSettingCellIndentifier @"userMoreSettingCell"

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundColor = [[HexColor SKColorHexString:@"0e0f20"] colorWithAlphaComponent:0.1f];
    
    isSignin = [MemberInfo isSignin];
   // NSLog(@"+++++++++++ is sign in : %d ++++++++ ",isSignin);
    if (isSignin) {
        myLibraryList = [[NSMutableArray alloc] init];
        myLibraryList = [DBManager selectLibraryList];
        MemberData *member = [DB getMemberInfo];
        NSArray *memberRooms = [NSArray arrayWithObject:member.memberRooms];
        NSLog(@"rooms: %lu", (unsigned long)memberRooms.count);
    }
    curBagValue = @"";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isSignin) {
        NSString *memberId = [DBManager selectMemberID];
        ServiceAPI *api = [[ServiceAPI alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCustomBag:) name:@"MemberBag" object:nil];
        [api memberBag:@"MemberBag" MemberID:memberId];
    }
    //Redeem
    [self createRedeemButton];
}
-(void) createRedeemButton {
    CGFloat h = self.view.frame.size.height;
    CGFloat max =  [[UIScreen mainScreen] bounds].size.width * 0.65;
    CGFloat buttonHeight = 40;
    max = MIN(max, 265);
    UIFont *font = [UIFont fontWithName:mainFont size:16];
    UIButton *redeemButton = [[UIButton alloc] initWithFrame:CGRectMake(0.25*max, h - 2*buttonHeight, 0.5*max, buttonHeight)];
    [redeemButton.titleLabel setFont:font];
    [redeemButton setTintColor:[UIColor whiteColor]];
    [redeemButton setTitle:@"🎗 Redeem" forState:UIControlStateNormal];
    redeemButton.layer.borderColor = [UIColor whiteColor].CGColor;
    redeemButton.layer.borderWidth = 0.35;
    redeemButton.layer.cornerRadius = 22;
    [redeemButton addTarget:self action:@selector(redeemCode) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:redeemButton];
}

-(void)loadCustomBag:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberBag" object:nil];
    curBagValue = @"ℹ️";
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        NSDictionary *info = notification.userInfo;
        if (info) {
            NSNumber *code = [info valueForKey:@"MessageCode"];
            NSInteger value = [[info valueForKey:@"MessageText"] integerValue];
            if ([code intValue] == 200) {
                NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
                [fmt setPositiveFormat:@"#,##0"];
                curBagValue = [fmt stringFromNumber:[NSNumber numberWithInteger:value]];
            }
        }
    }
    [self.tableView reloadData];
    
}

-(void)redeemCode {
    isRedeem = YES;
    if (!isSignin) {
        [self dismissLeftMenuView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Signin" object:nil];
    } else {
        [self dismissLeftMenuView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Redeem" object:nil];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return isSignin? 7 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSignin) {
        if (section == 1)
            return [myLibraryList count];
        else
            return 1;
    } else {
        return 5;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIFont *font17 = [UIFont fontWithName:mainFont size:17];
    if (isSignin) {
        if (indexPath.section == 0) {
            
            [tableView registerNib:[UINib nibWithNibName:@"UserInfoTVCell" bundle:nil] forCellReuseIdentifier:infoCellIndentifier];
            
            UserInfoTVCell *cell = (UserInfoTVCell*)[tableView dequeueReusableCellWithIdentifier:infoCellIndentifier forIndexPath:indexPath];
            cell.backgroundColor = [UIColor redColor];
            
            NSArray *info = [DBManager selecttMemberMenuInfo];
            cell.userName.text = info[0];
            cell.userEmailName.text = info[1];
            
            cell.lastSigninLabel.text = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"LastSignin"],[info[2] length] > 0 ? info[2]:@"-"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //Bag
            cell.lblBag.text = curBagValue;
            return cell;
        }
        else if (indexPath.section == 1){
            [tableView registerNib:[UINib nibWithNibName:@"UserLibraryTVCell" bundle:nil] forCellReuseIdentifier:userLibraryCellIndentifier];
            
            UserLibraryTVCell *cell = (UserLibraryTVCell*)[tableView dequeueReusableCellWithIdentifier:userLibraryCellIndentifier forIndexPath:indexPath];
            cell.backgroundColor = [UIColor blackColor];
            
            Room *room = [myLibraryList objectAtIndex:indexPath.row];
            
            [cell.libraryPic sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[room getSymbol]] placeholderImage:[UIImage imageNamed:@"default_library.png"] options:0 progress:nil completed:nil];
            
            
            NSString* roomName = [NSString stringWithFormat:@"%@",[room getRoomName]];
            cell.libraryName.textColor = [HexColor SKColorHexString:@"b0b5b7"];
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyCuurrentUsedLribaryID] isEqualToString:[room getRoomID]]){
                cell.libraryPic.layer.borderColor = [HexColor SKColorHexString:@"32CD32"].CGColor;
                cell.libraryPic.layer.borderWidth = 2.75;
                [[cell contentView] setBackgroundColor: [[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
            } else {
                cell.libraryPic.layer.borderWidth = 1.25;
                cell.libraryPic.layer.borderColor = UIColor.lightGrayColor.CGColor;
                [[cell contentView] setBackgroundColor: [UIColor blackColor]];
            }
            
            cell.libraryName.text = roomName;
            cell.accessibilityLabel = [room getRoomID];
            
            UIView *customColorView = [[UIView alloc] init];
            customColorView.backgroundColor = [UIColor darkGrayColor];
            cell.selectedBackgroundView = customColorView;
            
            return cell;
        }
        else{
            
            [tableView registerNib:[UINib nibWithNibName:@"UserMoreSettingTVCell" bundle:nil] forCellReuseIdentifier:userMoreSettingCellIndentifier];
            
            UserMoreSettingTVCell *cell = (UserMoreSettingTVCell*)[tableView dequeueReusableCellWithIdentifier:userMoreSettingCellIndentifier forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            
            CGRect sizeRect = [UIScreen mainScreen].bounds;
            NSInteger separatorHeight = 1;
            UIView * additionalSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height-separatorHeight,sizeRect.size.width,separatorHeight)];
            additionalSeparator.backgroundColor = [[HexColor SKColorHexString:@"e5e5e5"] colorWithAlphaComponent:0.35];
            [cell addSubview:additionalSeparator];
            
            NSString *labelName;
            switch (indexPath.section) {
                case 2:
                    labelName = [Utility NSLocalizedString:@"History"];
                    break;
                case 3:
                    labelName = [Utility NSLocalizedString:@"Setting"];
                    break;
                case 4:
                    labelName = [Utility NSLocalizedString:@"Help"];
                    break;
                case 5:
                    labelName = [Utility NSLocalizedString:@"SignOut"];
                    break;
               case 6:
                    labelName = [Utility NSLocalizedString:@"About"];
                    break;
                    
                default:
                    break;
            }
            
            cell.settingName.text = labelName;
            cell.settingName.textColor = [UIColor whiteColor];
            cell.settingName.font = font17;
            
            
            UIView *customColorView = [[UIView alloc] init];
            customColorView.backgroundColor = [UIColor darkGrayColor];
            cell.selectedBackgroundView = customColorView;
            
            return cell;
        }

    }
    else{
        ///// not Sign in yet //////
        
        [tableView registerNib:[UINib nibWithNibName:@"UserMoreSettingTVCell" bundle:nil] forCellReuseIdentifier:userMoreSettingCellIndentifier];
        
        UserMoreSettingTVCell *cell = (UserMoreSettingTVCell*)[tableView dequeueReusableCellWithIdentifier:userMoreSettingCellIndentifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        
        CGRect sizeRect = [UIScreen mainScreen].bounds;
        NSInteger separatorHeight = 1;
        UIView * additionalSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height-separatorHeight,sizeRect.size.width,separatorHeight)];
        additionalSeparator.backgroundColor = [[HexColor SKColorHexString:@"e5e5e5"] colorWithAlphaComponent:0.35];
        [cell addSubview:additionalSeparator];
        
        NSString *labelName;
        switch (indexPath.row) {
            case 0:
                labelName = [Utility NSLocalizedString:@"SignIn"];
                break;
            case 1:
                labelName = [Utility NSLocalizedString:@"History"];
                break;
                
            case 2:
                labelName = [Utility NSLocalizedString:@"Setting"];
                break;
                
            case 3:
                labelName = [Utility NSLocalizedString:@"Help"];
                break;
                
            case 4:
                labelName = [Utility NSLocalizedString:@"About"];
                break;
                
            default:
                break;
        }
        
        cell.settingName.text = labelName;
        cell.settingName.textColor = [UIColor whiteColor];
        cell.settingName.font = font17;
        cell.userInteractionEnabled = YES;
        
        if (indexPath.row == 1 && [OfflineShelf isOnlyOfflineModeEnable]) {
            cell.settingName.textColor = [UIColor lightGrayColor];
             cell.userInteractionEnabled = NO;
        }
        
        UIView *customColorView = [[UIView alloc] init];
        customColorView.backgroundColor = [UIColor darkGrayColor];
        cell.selectedBackgroundView = customColorView;
        
        return cell;
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0)
        return isSignin? 110 : 50;
    else
        return 50;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 1){
        float headerHeight = 40.f;
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, tableView.frame.size.width, headerHeight)];
        headerView.backgroundColor = [UIColor blackColor];
        
        float headerNameHeight = 30.f;
        UILabel * headerName = [[UILabel alloc] initWithFrame:CGRectMake(8.f, (headerHeight/2)-(headerNameHeight/2), tableView.frame.size.width, headerNameHeight)];
        headerName.text = [myLibraryList count]>1 ?[Utility NSLocalizedString:@"MyLibraries"]:[Utility NSLocalizedString:@"MyLibrary"];
        headerName.textColor = [UIColor whiteColor];
        headerName.font = [UIFont fontWithName:sukhumvitMediumFont size:[Utility fontSize:17.f]];
        //[Utility setFont:headerName.font  WithFontSize:17.f];
        
        [headerView addSubview:headerName];
        
        return headerView;
    }
    else
        return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1)
        return 40;
    else
        return 0;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [SVProgressHUD show];
    }
    return indexPath;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isSignin) {
        if (indexPath.section == 0) {
            [SVProgressHUD show];
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MemberProfile" object:nil];
        }
        else if (indexPath.section == 1) {
            UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            ///// set Current Library by User ///
            [[NSUserDefaults standardUserDefaults] setValue:cell.accessibilityLabel forKey:KeyCuurrentUsedLribaryID];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSRange range = NSMakeRange(1, 1);
            NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
            [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
            
            [ThemeTemplate loadTheme:cell.accessibilityLabel];
            
            [self dismissLeftMenuView];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeLibrary" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:cell.accessibilityLabel,@"libraryID", nil]];
        }
        else if (indexPath.section == 2){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WatchHistory" object:nil];
        }
        else if (indexPath.section == 3){
            [self dismissLeftMenuView];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"Setting" object:nil];
            
        }
        else if (indexPath.section == 4){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Help" object:nil];
        }
        else if (indexPath.section == 5){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SignOut" object:nil];
        }
        else if (indexPath.section == 6){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"About" object:nil];
        }

    }
    else{
        if (indexPath.row == 0) {
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Signin" object:nil];
        }
        else if(indexPath.row == 1){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WatchHistory" object:nil];
        }
        else if(indexPath.row == 2){
            [self dismissLeftMenuView];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"Setting" object:nil];
        }
        else if(indexPath.row == 3){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Help" object:nil];
        }
        else if(indexPath.row == 4){
            [self dismissLeftMenuView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"About" object:nil];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)memberBagTapped {
    [self dismissLeftMenuView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MemberBagTapped" object:NULL];
}
-(void)dismissLeftMenuView{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

@end

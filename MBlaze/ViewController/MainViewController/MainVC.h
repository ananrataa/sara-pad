//
//  MainViewController.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/25/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AMSlideMenuMainViewController.h"
#import "Utility.h"

@interface MainVC : AMSlideMenuMainViewController{
    
    UITabBarController *tabBarController;
    NSArray *tabBarItems;

    float tabbar_height;
    
    NSUserDefaults *userDefault;
}

@end

//
//  MainViewController.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/25/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MainVC.h"
#import "ConstantValues.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
        userDefault = [NSUserDefaults standardUserDefaults];
    
        tabbar_height = [userDefault floatForKey:@"TABBAR_HEIGHT"];
        if (tabbar_height <= 0)
            tabbar_height = default_tabbar_height;
    
        tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
        tabBarController.selectedIndex = 0;
        tabBarController.delegate = (id) self;
    
        tabBarItems = [tabBarController tabBar].items;
        tabBarController.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
        CGSize newSize = CGSizeMake(tabbar_height-8,tabbar_height-8);
    
        [tabBarItems[0] setImage:[[Utility rescaleImage:[UIImage imageNamed:@"header"] scaledToSize:newSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tabBarItems[1] setImage:[[Utility rescaleImage:[UIImage imageNamed:@"header"] scaledToSize:newSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tabBarItems[2] setImage:[[Utility rescaleImage:[UIImage imageNamed:@"header"] scaledToSize:newSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tabBarItems[3] setImage:[[Utility rescaleImage:[UIImage imageNamed:@"header"] scaledToSize:newSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tabBarItems[4] setImage:[[Utility rescaleImage:[UIImage imageNamed:@"header"] scaledToSize:newSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)viewWillAppear:(BOOL)animated{
    [self presentViewController:tabBarController animated:YES completion:nil];
}


-(void)configureLeftMenuButton:(UIButton *)button{
    
    CGRect frame = button.frame;
    frame.origin = (CGPoint){0,0};
    frame.size = (CGSize){40,40};
    button.frame = frame;
    
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
}

-(CGFloat)leftMenuWidth{
    return 180;
}


-(NSString *) segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath{
    NSString* identifier;
    switch (indexPath.row) {
        case 0:
            identifier = @"firstLeftSegue";
            break;
            
        case 1:
            identifier = @"secondLeftSegue";
            break;
            
        default:
            break;
    }
    
    return identifier;
}



@end

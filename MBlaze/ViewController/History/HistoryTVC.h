//
//  HistoryTVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryModel.h"
#import "ServiceAPI.h"

@protocol HistoryDelegate <NSObject>

-(void)historyDidSelected:(NSString*) mediaId roomId:(NSString*) roomId;

@end

@interface HistoryTVC : UITableViewController{
    
    //UIBarButtonItem * leftBarButton;
    
    NSMutableArray *arrayOfHistory;
    NSMutableArray *arrayOfFilter;
    
    HistoryModel *mHistory;
    
    ServiceAPI *api;
   
    int loadTimeIndex;
    int historyType;
    int previousHistoryType;
    int xScrollViewContentOffset;
    
    BOOL isLoadingMore;
    //BOOL loadMoreEnable;
    UIActivityIndicatorView *loadMoreActivityIndicator;
    BOOL showLoadingMore;
    
    BOOL isNoMore;
}

@property(nonatomic, unsafe_unretained) id<HistoryDelegate> delegate;

-(void)loadMediaHistory;
-(void)setHistoryType:(int) type;
@end

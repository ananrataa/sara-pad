//
//  HistoryTVCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAutor;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak,nonatomic) IBOutlet UILabel *mediaTitle;
@property (weak,nonatomic) IBOutlet UILabel *mediaAuthor;
@property (weak,nonatomic) IBOutlet UILabel *mediaCategory;
@property (weak,nonatomic) IBOutlet UILabel *watchedDate;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;


-(void)setCoverImage: (UIImage*) image;

@end

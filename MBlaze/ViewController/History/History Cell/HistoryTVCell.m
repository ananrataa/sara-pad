//
//  HistoryTVCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "HistoryTVCell.h"

@implementation HistoryTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.coverImageView.layer.cornerRadius = 5.0f;
    self.coverImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.coverImageView.layer.shadowOpacity = 0.35;
    self.coverImageView.layer.shadowOffset = CGSizeMake(1, 2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCoverImage: (UIImage*) image {
    [self.coverImageView setImage:image];
}
@end

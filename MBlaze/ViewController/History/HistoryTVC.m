//
//  HistoryTVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "HistoryTVC.h"
#import "Utility.h"
#import "HistoryTVCell.h"
#import "ConstantValues.h"
//#import "MediaDetailTVC.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "ConstantValues.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface HistoryTVC ()

@end

@implementation HistoryTVC

#define historyCellIndentifier @"historyCell"
#define readingCellIndentifier @"readingCell"
#define Rented_HistoryType 0
#define Viewed_HistoryType 1
#define Voted_HistoryType 2
#define Following_HistoryType 3
#define Reading_HistoryType 4

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"HistoryTVCell" bundle:nil] forCellReuseIdentifier:historyCellIndentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReadingTableViewCell" bundle:nil] forCellReuseIdentifier:readingCellIndentifier];
    
    loadTimeIndex = 1;
    arrayOfHistory = [[NSMutableArray alloc] init];
    arrayOfFilter =[NSMutableArray arrayWithObjects:
                    @[[Utility NSLocalizedString:@"Viewed"],@Viewed_HistoryType],
                    @[[Utility NSLocalizedString:@"Rented"],@Rented_HistoryType],
                    @[[Utility NSLocalizedString:@"Reading"],@Reading_HistoryType],
                    @[[Utility NSLocalizedString:@"Voted"],@Voted_HistoryType],
                    @[[Utility NSLocalizedString:@"Following"],@Following_HistoryType],  nil];
    
    historyType = Viewed_HistoryType;
    previousHistoryType = Viewed_HistoryType;
    
    api = [[ServiceAPI alloc] init];
    
    self.view.userInteractionEnabled = NO;
    [self loadMediaHistory];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"History"]];
}
-(void)loadMediaHistory {
    ///api/DeviceMedia/MediaHistory?deviceCode={deviceCode}&HisType={HisType}&loadIndex={loadIndex}
    //http://192.168.0.98/RoomAPI/api/DeviceMedia/MediaHistory?deviceCode=8F9968E6-28B4-4DCB-BF58-7D1DEA25E166&HisType=0&loadIndex=1
    if (api == nil)
        api = [[ServiceAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaHistoryResponse:) name:@"MediaHistory" object:nil];
    [api mediaHistory:@"MediaHistory" HistoryType:historyType LoadIndex:loadTimeIndex];
    
}

-(void)mediaHistoryResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaHistory" object:nil];
    // NSLog(@"media History Response : %@",[notification.userInfo description]);
    
    if (previousHistoryType != historyType)
        [arrayOfHistory removeAllObjects];
    previousHistoryType = historyType;
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo]){
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
        if (isLoadingMore) loadTimeIndex--;
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        [arrayOfHistory removeAllObjects];
        for (NSDictionary *dicData in notification.userInfo) {
            HistoryModel *model = [[HistoryModel alloc] init];
            BOOL isReturn = ![[dicData valueForKey:@"ISRENT"] boolValue];
            [model HistoryModelWithLibraryID:[Utility covertNsNullToNSString:[dicData objectForKey:@"ROOMID"]]
                                 LibraryName:@""
                                  CategoryID:@""
                                CategoryName:[Utility covertNsNullToNSString:[dicData objectForKey:@"CATEGORIESNAME"]]
                                   MediaType:[Utility covertNsNullToNSString:[dicData objectForKey:@"MEDIAKIND"]]
                                     MediaID:[Utility covertNsNullToNSString:[dicData objectForKey:@"MEDIAID"]]
                                  MediaTitle:[Utility covertNsNullToNSString:[dicData objectForKey:@"MEDIANAME"]]
                                 MediaAuthor:[Utility covertNsNullToNSString:[dicData objectForKey:@"AUTHOR"]]
                                 WatchedDate:[Utility covertNsNullToNSString:[Utility covertDateToFormat:[dicData objectForKey:@"DATE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"dd/MM/yyyy"]]
                                    isReturn:isReturn];
            
            
            [arrayOfHistory addObject:model];
        }
        //[self.tableView reloadData];
    } else {
        [SVProgressHUD showInfoWithStatus:[Utility NSLocalizedString:@"UnknownError"]];
        isNoMore = YES;
         if (isLoadingMore) loadTimeIndex--;
    }
    
    [self.tableView reloadData];
    self.view.userInteractionEnabled = YES;
    
    float loadMoreIndicatorYpoint = self.tableView.contentSize.height>=self.tableView.frame.size.height? self.tableView.contentSize.height:self.tableView.frame.size.height;
    [[self.view viewWithTag:909] removeFromSuperview];
    ///load more IndicatorView
    UIView *loadMoreIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
    loadMoreIndicatorView.frame = CGRectMake(0,loadMoreIndicatorYpoint, [[UIScreen mainScreen] bounds].size.width, 50);
    [loadMoreIndicatorView setBackgroundColor:[UIColor clearColor]];
    loadMoreIndicatorView.tag = 909;
    loadMoreActivityIndicator = nil;
    loadMoreActivityIndicator = [[loadMoreIndicatorView subviews] objectAtIndex:0];
    [loadMoreActivityIndicator setHidesWhenStopped:YES];
    [self.tableView addSubview:loadMoreIndicatorView];
    
    if (loadMoreActivityIndicator.isAnimating) {
        [loadMoreActivityIndicator stopAnimating];
    }
    if (isLoadingMore) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height) animated:YES];
    }
    isLoadingMore =NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dismissViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayOfHistory count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryTVCell *cell = [tableView dequeueReusableCellWithIdentifier:historyCellIndentifier forIndexPath:indexPath];
    mHistory = (HistoryModel*)[arrayOfHistory objectAtIndex:indexPath.row];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *image = [DB getMediaCoverImage:[mHistory getMediaID]];
        [cell setCoverImage:image];
//    });
    cell.lblAutor.text = [Utility NSLocalizedString:@"Author"];
    cell.lblCategory.text = [Utility NSLocalizedString:@"CategoryName"];
    cell.lblDate.text = [Utility NSLocalizedString:@"Date"];
    
    cell.mediaTitle.text = [NSString stringWithFormat:@"%i. %@",(int)indexPath.row+1,[mHistory getMediaTitle]];
    
    cell.mediaAuthor.text = [mHistory getMediaAuthor];
    cell.mediaCategory.text = [mHistory getCategoryName];
    cell.watchedDate.text = [mHistory getWatchedDate];
    
    ////add separator line
    CGRect sizeRect = [UIScreen mainScreen].bounds;
    NSInteger separatorHeight = 1.5f;
    UIView * additionalSeparator = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-separatorHeight,sizeRect.size.width,separatorHeight)];
    additionalSeparator.backgroundColor = [UIColor lightGrayColor];
    
    //Borrow-Return
    if (historyType == 0) {
        [cell.statusImageView setAlpha:1];
        if ([mHistory getIsReturn]) {
            //Green
            [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.3765 green:0.7373 blue:0.4235 alpha:0.35]];
            [cell.statusImageView setImage:[UIImage imageNamed:@"book_in"]];
        } else {
            //Red
            [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.8824 green:0.2863 blue:0.2196 alpha:0.35]];
            [cell.statusImageView setImage:[UIImage imageNamed:@"book_out"]];
        }
    } else {
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        [cell.statusImageView setAlpha:0];
    }
    [cell addSubview:additionalSeparator];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (historyType == 4) {
        return tableView.frame.size.height - 50.f;
    } else {
        return 136.5f;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (historyType != 4) {
        mHistory = (HistoryModel*)[arrayOfHistory objectAtIndex:indexPath.row];
        [self loadMediaDetail:[mHistory getLibraryID] MediaID:[mHistory getMediaID]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"contentOffset y >> %.2f",(float)self.tableView.contentOffset.y);
    
    ///load more
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore) {
        [loadMoreActivityIndicator startAnimating];
        isLoadingMore = YES;
        showLoadingMore = NO;
    }
    if (!isNoMore) {
        if (scrollView.contentOffset.y <= (self.tableView.contentSize.height - self.tableView.bounds.size.height + 50) && isLoadingMore  && !showLoadingMore){
            [self.tableView setContentOffset:CGPointMake(0,(self.tableView.contentSize.height - self.tableView.bounds.size.height + 50)) animated:YES];
            showLoadingMore = YES;
            self.view.userInteractionEnabled = NO;
            [SVProgressHUD showWithStatus:@"Loading..."];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                self.view.userInteractionEnabled = NO;
                loadTimeIndex ++;
                [self loadMediaHistory];
                
            });
        }
    } else {
        [SVProgressHUD dismiss];
        [loadMoreActivityIndicator stopAnimating];
    }
}

#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)libraryID MediaID:(NSString*)mediaID{
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        self.title = @"";
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [mHistory getLibraryID];
        [self.delegate historyDidSelected:mediaId roomId:roomId];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}

#pragma mark ------------ Filter Tapping ----------
-(void)filterMediaTapping:(UIButton*)sender{
    int n = [sender.accessibilityLabel intValue];
    if (n != historyType) {
        [SVProgressHUD showWithStatus:@"Loading..."];
        historyType = n;
        self.view.userInteractionEnabled = NO;
        loadTimeIndex = 1;
        [self loadMediaHistory];
    }
}

-(void)setHistoryType:(int) type {
    switch (type) {
        case 0:
            historyType = Viewed_HistoryType;
            break;
        case 1:
            historyType = Rented_HistoryType;
            break;
        case 3:
            historyType = Voted_HistoryType;
        default:
            historyType = Following_HistoryType;
            break;
    }
    isNoMore = NO;
    loadTimeIndex = 1;
    [self loadMediaHistory];
}
@end

//
//  MediaCoverCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaCoverCell.h"

@implementation MediaCoverCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

    self.dowloadMediaButton.layer.cornerRadius = 8.f;
    
    self.preViewMediaButton.layer.cornerRadius = 8.f;
    self.followMediaButton.layer.cornerRadius = 8.f;
    self.followMediaButton.backgroundColor = [UIColor lightGrayColor];
    
    self.shareMediaButton.layer.cornerRadius = 8.f;
    
    self.dowloadMediaButton.layer.shadowOpacity = 0.25;
    self.dowloadMediaButton.layer.shadowOffset = CGSizeMake(2, 2);
    self.preViewMediaButton.layer.shadowOpacity = 0.25;
    self.preViewMediaButton.layer.shadowOffset = CGSizeMake(2, 2);
    self.followMediaButton.layer.shadowOpacity = 0.25;
    self.followMediaButton.layer.shadowOffset = CGSizeMake(2, 2);
    self.shareMediaButton.layer.shadowOpacity = 0.25;
    self.shareMediaButton.layer.shadowOffset = CGSizeMake(2, 2);

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

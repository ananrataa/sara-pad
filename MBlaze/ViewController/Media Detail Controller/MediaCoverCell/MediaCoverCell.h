//
//  MediaCoverCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaCoverCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mediaCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *mediaTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *mediaRate1;
@property (weak, nonatomic) IBOutlet UIImageView *mediaRate2;
@property (weak, nonatomic) IBOutlet UIImageView *mediaRate3;
@property (weak, nonatomic) IBOutlet UIImageView *mediaRate4;
@property (weak, nonatomic) IBOutlet UIImageView *mediaRate5;

@property (weak, nonatomic) IBOutlet UIButton *dowloadMediaButton;
@property (weak, nonatomic) IBOutlet UIButton *preViewMediaButton;
@property (weak, nonatomic) IBOutlet UIButton *followMediaButton;
@property (weak, nonatomic) IBOutlet UIButton *shareMediaButton;




@property (weak, nonatomic) IBOutlet UIImageView *mediaTypeImage;
@property (weak, nonatomic) IBOutlet UIButton *mediaCoverImageListBtn;



@end

//
//  MediaDetailTVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaDetailTVC.h"
#import "Utility.h"
#import "ConstantValues.h"
#import "HexColor.h"
#import "MediaCoverCell.h"
#import "MediaDetailCell.h"
#import "MediaReviewCell.h"
#import "TableViewCell.h"
#import "CollectionViewCell.h"
#import "CustomActivityIndicator.h"
#import "CustomActivityIndicator.h"
#import "OfflineShelf.h"
#import "DBManager.h"
#import "MemberInfo.h"
#import "ArrayOfAds.h"
#import "Ads.h"
#import "SPNotification.h"
#import "ZMImageSliderViewController.h"
#import <PDFKit/PDFKit.h>
#import "UIImageView+WebCache.h"
#import "ThemeTemplate.h"
#import "MediaListCollection.h"
#import "ShareMedia.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface MediaDetailTVC () {
    SPMedia *spMedia;
    NSString *curShelfId;
}
@end

@implementation MediaDetailTVC

@synthesize mediaID;
//@synthesize libraryID;
//@synthesize libraryName;
@synthesize mediaType;
@synthesize mediaTitle;
@synthesize jsonData;
@synthesize subjectID;
@synthesize isMediaPreset;

#define tilteNameFontSize 20.f
#define bodyNameFontSize 17.f

#define abstractHeight 105.f
#define abstractLabelBottonConstraint 10.f
//#define seeMoreDropDownHeight 25.f

#define isIPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    spMedia = [[SPMedia alloc] initWithJson:jsonData];
    subjectID = @"";
    [self.navigationItem setTitle:mediaTitle];
    [self.tabBarController.tabBar setHidden:YES];
    UIFont *font = [UIFont fontWithName:mainFont size:21];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor],
                                                                    NSFontAttributeName:font
                                                                    };
    /// calculate collection Cell Width & Height
    collectionCellWidth = (([[UIScreen mainScreen] bounds].size.width)/[Utility calaulateCollectionItemInRow]);
    collectionCellHeight =  ((collectionCellWidth*4)/3)+90;
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
 
    leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[UIColor blackColor]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    //navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
//    rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(rightTopBarTapping:)];
//    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
//    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    [SVProgressHUD show];
    
    deviceCode = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];
    memberID = deviceCode;
    isMember = NO;
    if([MemberInfo isSignin] || [DBManager isMember]){
        memberID = [DBManager selectMemberID];
        isMember = YES;
    }
    
    dictionaryOfMedia = [[NSMutableDictionary alloc] init];
    arrayOfHeader = [NSMutableArray arrayWithObjects:@"",[Utility NSLocalizedString:@"Media_Details"],[Utility NSLocalizedString:@"Suggestion"],[Utility NSLocalizedString:@"Review"], nil];
    
    api = [[ServiceAPI alloc] init];
    
    arrayOfMediaSuggestion = [[NSMutableArray alloc] init];
    //mCache = [[MediaFileCache alloc] init];
   // mediaCacheDic = [[NSMutableDictionary alloc] init];
    coverImageData = nil;
    reviewScore = 0;
    isFollow = NO;
    
    //[Utility removeAllFilesInDirectory:sarapadFloderName];
    
    [self getMediaDetailFromJson];
    [self loadMediaSuggestion];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([dictionaryOfMedia count] > 0){
        NSRange range = NSMakeRange(0, 1);
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)leftTopBarTapping:(id)sender{
    [self prepareToDismissVC:NO];
}

-(void)rightTopBarTapping:(id)sender{
    [self prepareToDismissVC:YES];
}

-(void)prepareToDismissVC:(BOOL)dismissToRootView {
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    
    
    if (self.isOwnNavigationBar)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    else
        if(dismissToRootView)
            [self.navigationController popToRootViewControllerAnimated:YES];
        else
            [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -------- get Medai Detail --------
-(void)getMediaDetailFromJson{
    //NSLog(@"json Media Detail : %@",jsonData);
    
    if (![Utility isServiceResponseError:jsonData] && [Utility isJsonDictionaryNotNull:jsonData]) {
       
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"MEDIACODE"]] forKey:@"MEDIACODE"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"AUTHOR"]] forKey:@"AUTHOR"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"MEDIANAME"]] forKey:@"MEDIANAME"];
        
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"ROOMID"]] forKey:@"ROOMID"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"ROOMNAME"]] forKey:@"ROOMNAME"];
        
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"CATEGORIESID"]] forKey:@"CATEGORIESID"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"CATEGORIESNAME"]] forKey:@"SUBCATEGORIESNAME"];
        //[dictionaryOfMedia setValue:[self generateCategoryName:[Utility covertNsNullToNSString:[jsonData objectForKey:@"CATEGORIESNAME"]] SubCategoryName:[Utility covertNsNullToNSString:[jsonData objectForKey:@"SUBCATEGORIESNAME"]]] forKey:@"SUBCATEGORIESNAME"];
       
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"POPULARVOTE"]] forKey:@"POPULARVOTE"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"MEDIAKIND"]] forKey:@"MEDIAKIND"];
        
         [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"ABSTRACT"]] forKey:@"ABSTRACT"];
        [dictionaryOfMedia setValue:[self generateTag:[Utility covertNsNullToNSString:[jsonData objectForKey:@"TAG"]]] forKey:@"TAG"];
        
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"isDownload"]] forKey:@"isDownload"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"RENTLIMITDAY"]] forKey:@"RENTLIMITDAY"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"isMember"]] forKey:@"isMember"];
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"isPublic"]] forKey:@"isPublic"];
        
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"ANNOTATION"]] forKey:@"ANNOTATION"];
    
        //PublisherName
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"PUBLISHERNAME"]] forKey:@"PublisherName"];
        //PUBLISHERID
         [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"PUBLISHERID"]] forKey:@"PublisherID"];
        
        //ครั้งที่พิมพ์ (Version)
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"VOLUME"]] forKey:@"Version"];
        //จำนวนที่พิมพ์ (Amount)
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"AMOUNT"]] forKey:@"Amount"];
        //ปีที่พิมพ์ (Year)
        [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[jsonData objectForKey:@"VERSION"]] forKey:@"Year"];
        
        //media type info
        NSDictionary *mediaTypeDic = [jsonData objectForKey:@"MASMEDIATYPE"];
        if ([Utility isJsonDictionaryNotNull:mediaTypeDic]) {
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaTypeDic objectForKey:@"MEDIATYPEID"]] forKey:@"MEDIATYPEID"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaTypeDic objectForKey:@"MEDIATYPENAME"]] forKey:@"MEDIATYPENAME"];
            
            // mediaType = [Utility covertNsNullToNSString:[mediaTypeDic objectForKey:@"MEDIATYPEID"]];
        }
        // cover info
        NSDictionary *mediaCoverDic = [jsonData objectForKey:@"CoverFileURLs"];
        if ([Utility isJsonDictionaryNotNull:mediaCoverDic]) {
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaCoverDic objectForKey:@"URL"]] forKey:@"COVERURL"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaCoverDic objectForKey:@"FileExten"]] forKey:@"COVERFILEEXTEN"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaCoverDic objectForKey:@"FileSizeMB"]] forKey:@"COVERFILESIZE"];
        }
        //file info
        NSDictionary *mediaFileDic = [jsonData objectForKey:@"ContentFileURLs"];
        if ([Utility isJsonDictionaryNotNull:mediaFileDic]) {
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaFileDic objectForKey:@"URL"]] forKey:@"FILEURL"];
            //[dictionaryOfMedia setValue:@"https://www.bigcamera.co.th/media/catalog/file/c/a/catalog-8-2017.pdf" forKey:@"FILEURL"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaFileDic objectForKey:@"FileExten"]] forKey:@"FILEFILEEXTEN"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaFileDic objectForKey:@"FileSizeMB"]] forKey:@"FILEFILESIZE"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaFileDic objectForKey:@"PAGE"]] forKey:@"PAGE"];
            
            ///get Original file name from FILEURL
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[[mediaFileDic objectForKey:@"URL"] lastPathComponent]] forKey:@"OriginalFileName"];
            
        }
        
        // preview info
        NSDictionary *mediaPreviewDic = [jsonData objectForKey:@"DemoFileURLs"];
        if ([Utility isJsonDictionaryNotNull:mediaPreviewDic]) {
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaPreviewDic objectForKey:@"URL"]] forKey:@"PREVIEWURL"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaPreviewDic objectForKey:@"FileExten"]] forKey:@"PREVIEWFILEEXTEN"];
            [dictionaryOfMedia setValue:[Utility covertNsNullToNSString:[mediaPreviewDic objectForKey:@"FileSizeMB"]] forKey:@"PREVIEWFILESIZE"];
        }
        
        //Theme
        NSDictionary *theme = [jsonData objectForKey:@"THEME"];
        if ([Utility isJsonDictionaryNotNull:theme]) {
            //update Theme for Media
            UIFont *font = [UIFont fontWithName:mainFont size:21];
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET2"]]], NSFontAttributeName:font};
            self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET1"]]];
            
            [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[Utility covertNsNullToNSString:[theme objectForKey:@"SET2"]]]];
            
            NSDictionary *themeDict = [NSDictionary dictionaryWithObjectsAndKeys:
//                [Utility covertNsNullToNSString:[theme objectForKey:@"SET1"]],@"SET1",
//                [Utility covertNsNullToNSString:[theme objectForKey:@"SET2"]],@"SET2",
                [Utility covertNsNullToNSString:[theme objectForKey:@"SET3"]],@"SET3",
                [Utility covertNsNullToNSString:[theme objectForKey:@"SET4"]],@"SET4",nil];
//                [Utility covertNsNullToNSString:[theme objectForKey:@"SET5"]],@"SET5",
//                [Utility covertNsNullToNSString:[theme objectForKey:@"SET6"]],@"SET6",
//                [Utility covertNsNullToNSString:[theme objectForKey:@"SET7"]],@"SET7"
                                      
            [dictionaryOfMedia setValue:themeDict forKey:@"THEME"];
        }
        
        //isFirst use for Download Media Booking
        //isFirst = [[Utility covertNsNullToNSString:[jsonData objectForKey:@"isFirst"]] boolValue];
        libraryName = [Utility covertNsNullToNSString:[jsonData objectForKey:@"ROOMNAME"]];
        reviewScore = [[Utility covertNsNullToNSString:[jsonData objectForKey:@"SCOREVOTE"]] intValue];
        isFollow = [[Utility covertNsNullToNSString:[jsonData objectForKey:@"isFollow"]] boolValue];
        libraryID = [Utility covertNsNullToNSString:[jsonData objectForKey:@"ROOMID"]];
        mediaID = [Utility covertNsNullToNSString:[jsonData objectForKey:@"MEDIAID"]];
        
        //// floder name ///
        folderName = [NSString stringWithFormat:@"%@/%@",sarapadFloderName,libraryID];
        tempFloderName = [NSString stringWithFormat:@"%@/%@",temporyFloderName,libraryID];
        
    }
    
    //// calculate height
    mediaNameLabelHeight = [self calculateTextHeight:[dictionaryOfMedia objectForKey:@"MEDIANAME"]];
    
    mediaAuthorLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Author"],[dictionaryOfMedia objectForKey:@"AUTHOR"]]];
    mediaCategoryLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"CategoryName"],[dictionaryOfMedia objectForKey:@"SUBCATEGORIESNAME"]]];
     mediaPublisherLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Publisher"],[dictionaryOfMedia objectForKey:@"PublisherName"]]];
    mediaLibraryLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Library"],libraryName]];
    mediaCodeLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"MediaCode"],[dictionaryOfMedia objectForKey:@"MEDIACODE"]]];
    tagLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"Tag : %@",[dictionaryOfMedia objectForKey:@"TAG"]]];
    mediaAbstractLabelHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Abstract"],[dictionaryOfMedia objectForKey:@"ABSTRACT"]]];

    [self.tableView reloadData];
    
    
    [SVProgressHUD dismiss];
}

- (float) calculateTextHeight:(NSString *) chatText {
    chatText = [chatText length] > 0 ? chatText:@"XOXOXO";
    CGRect textRect = [chatText boundingRectWithSize:CGSizeMake(self.tableView.frame.size.width-45, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]]} context:nil];
    //    NSLog(@"Text : %@",chatText);
    //    NSLog(@"Height : %.2f",textRect.size.height);
    return textRect.size.height;
}

-(NSMutableArray *)generateTag:(NSString *)tagStr{
    return [NSMutableArray arrayWithArray:[tagStr componentsSeparatedByString:@","]];
}

-(NSString *) generateCategoryName:(NSString *)cateName SubCategoryName:(NSString *)subCateName{
    //NSLog(@"Category Text : %@",cateName);
    //NSLog(@"SubCategoryText : %@",subCateName);
    NSString *CateName = @"";
    if ([cateName length] > 0) {
        CateName = cateName;
    }
    if ([subCateName length] > 0) {
        if ([CateName length] > 0)
            CateName = [CateName stringByAppendingString:[NSString stringWithFormat:@", %@",subCateName]];
        else
            CateName = subCateName;
    }
    return CateName;
}

-(BOOL)isFileAlreadyDownload:(NSString*)fileExten{
    ///check file already dowload //
    if([MemberInfo isSignin]){
        BOOL isHaveFile = NO;
        /// member shelf
        filePath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.%@",memberID,mediaID,[fileExten lowercaseString]]]];
        isHaveFile = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        
        if(!isHaveFile){
            ///user shelf
            filePath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.%@",deviceCode,mediaID,[fileExten lowercaseString]]]];
            isHaveFile = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        }
        //NSLog(@"---- media have already download : %d ------",isHaveFile);
        NSString *mmediaID = isMediaPreset? [Utility encodeMediaIDPreset:mediaID]:mediaID;
        BOOL isHaveFilePath = [[DBManager selectMediaFilePath:mmediaID] length] > 0? YES:NO;
        
        return isHaveFile && isHaveFilePath ;
    }
    
    else{
        /// un sign in ---> user shelf
        filePath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.%@",deviceCode,mediaID,[fileExten lowercaseString]]]];
        //        NSLog(@"check file already dowload at path : %@",filePath);
        //        fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        //        NSLog(@"---- media have already download : %d ------",[[NSFileManager defaultManager] fileExistsAtPath:filePath]);
        
        NSString *mmediaID = isMediaPreset? [Utility encodeMediaIDPreset:mediaID]:mediaID;
        BOOL isHaveFilePath = [[DBManager selectMediaFilePath:mmediaID] length] > 0? YES:NO;
        
        return [[NSFileManager defaultManager] fileExistsAtPath:filePath] && isHaveFilePath;
    }
}

-(BOOL)isMediaAlreadyExpired:(NSString*)fileExten{
    ////check & delete if Media is Expired
    if ([DBManager isMediaExpired:mediaID]) {
        
        NSArray* expMedia = [DBManager selectMediaOnShelfByMediaID:mediaID];
        if ([expMedia count] >= 4) {
            [Utility removeExpiredMediaOnShelf:expMedia[0] CoverPath:expMedia[1] FilePath:expMedia[2] MemberID:expMedia[3] RoomID:expMedia[4]];
        }
        
        return YES;
    }
    
    return NO;
}


#pragma mark -------- Media suggestion ----------
-(void)loadMediaSuggestion{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaSuggestionResponse:) name:@"MediaSuggestion" object:nil];
    [api mediasSuggested:@"MediaSuggestion" RoomID:libraryID MediaID:mediaID MediaKind:@""];
}

-(void)loadMediaSuggestionResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaSuggestion" object:nil];
    // NSLog(@"Media Suggestion Response : %@",[notification.userInfo description]);
    
    if (![notification.userInfo isEqual:[NSNull null]] && [notification.userInfo count]>0){
        for (NSDictionary *mediaItem in notification.userInfo) {
            NSMutableDictionary *itemDic = [[NSMutableDictionary alloc] init];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                       forKey:@"MEDIAID"];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]] forKey:@"MEDIANAME"];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                       forKey:@"AUTHOR"];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                       forKey:@"POPULARVOTE"];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                       forKey:@"MediaCoverUrl"];
            [itemDic setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                       forKey:@"MEDIAKIND"];
            [arrayOfMediaSuggestion addObject:itemDic];
            
        }
        
        [self.tableView reloadData];
    }
    [SVProgressHUD dismiss];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        [self.tableView registerNib:[UINib nibWithNibName:@"MediaCoverCell" bundle:nil] forCellReuseIdentifier:@"mediaCoverCell"];
        MediaCoverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mediaCoverCell" forIndexPath:indexPath];
        
        //[cell.mediaCoverImageView sd_setImageWithURL:[NSURL URLWithString:[dictionaryOfMedia objectForKey:@"COVERURL"]] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
        
        [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[dictionaryOfMedia objectForKey:@"COVERURL"]] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"] options:0 progress:nil completed:nil];
        
        //media cover image list button
        [cell.mediaCoverImageListBtn addTarget:self action:@selector(coverListImageFullScreenCoverImage:) forControlEvents:UIControlEventTouchUpInside];
        
        //media type
        [Utility setMediaTypeIcon:cell.mediaTypeImage MediaType:[dictionaryOfMedia objectForKey:@"MEDIAKIND"]];
        
        //title
        mediaName = cell.mediaTitleLabel;
        cell.mediaTitleLabel.text = mediaTitle;
        [Utility setFont:cell.mediaTitleLabel.font WithFontSize:tilteNameFontSize];
        
        // rate
        [Utility setMediaRating:[dictionaryOfMedia objectForKey:@"POPULARVOTE"] ImageView1:cell.mediaRate1 ImageView2:cell.mediaRate2 ImageView3:cell.mediaRate3 ImageView4:cell.mediaRate4 ImageView5:cell.mediaRate5];
        
        
        //remove observer Target for download button
        
        [cell.dowloadMediaButton removeTarget:self action:@selector(downloadButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        [cell.dowloadMediaButton removeTarget:self action:@selector(openMediaTapping:) forControlEvents:UIControlEventTouchUpInside];
        [cell.dowloadMediaButton removeTarget:self action:@selector(playVideoButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        //if ([[dictionaryOfMedia objectForKey:@"MEDIATYPENAME"] isEqualToString:KeyMediaTypeName]){
        // is Public
        
        if ([[[dictionaryOfMedia objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"] || [[[dictionaryOfMedia objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"a"]){
            // is Video or audio //
            [cell.dowloadMediaButton setTitle:[Utility NSLocalizedString:@"Play"] forState:UIControlStateNormal];
            cell.dowloadMediaButton.accessibilityLabel = [dictionaryOfMedia objectForKey:@"FILEURL"];
            mediaPath = [dictionaryOfMedia objectForKey:@"FILEURL"];
            if ([[dictionaryOfMedia objectForKey:@"isDownload"] intValue] == 1 && [[dictionaryOfMedia objectForKey:@"FILEURL"] length] > 0) {
                cell.dowloadMediaButton.backgroundColor = [HexColor SKColorHexString:@"3399FF"];
                [cell.dowloadMediaButton addTarget:self action:@selector(playVideoButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
                cell.dowloadMediaButton.userInteractionEnabled = YES;
            }
            else{
                cell.dowloadMediaButton.backgroundColor = [UIColor darkGrayColor];
                cell.dowloadMediaButton.userInteractionEnabled = NO;
            }
        } else {
            // is Document //
            fileExists = [self isFileAlreadyDownload:[dictionaryOfMedia objectForKey:@"FILEFILEEXTEN"]];
            if(fileExists && ![self isMediaAlreadyExpired:[dictionaryOfMedia objectForKey:@"FILEFILEEXTEN"]]){
                // is already download //
                cell.dowloadMediaButton.backgroundColor = [HexColor SKColorHexString:@"f47530"];
                [cell.dowloadMediaButton setTitle:[Utility NSLocalizedString:@"Open"] forState:UIControlStateNormal];
                cell.dowloadMediaButton.accessibilityLabel = [dictionaryOfMedia objectForKey:@"OriginalFileName"];
                [cell.dowloadMediaButton addTarget:self action:@selector(openMediaTapping:) forControlEvents:UIControlEventTouchUpInside];
            }
            else{
                //check can download ///
                if ([[dictionaryOfMedia objectForKey:@"isDownload"] intValue] == 1 && [[dictionaryOfMedia objectForKey:@"FILEURL"] length] > 0) {
                    cell.dowloadMediaButton.backgroundColor = [HexColor SKColorHexString:@"f47530"];
                    [cell.dowloadMediaButton setTitle:[Utility NSLocalizedString:@"Download"] forState:UIControlStateNormal];
                    cell.dowloadMediaButton.accessibilityLabel = [dictionaryOfMedia objectForKey:@"FILEURL"];
                    [cell.dowloadMediaButton addTarget:self action:@selector(downloadButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
                    cell.dowloadMediaButton.userInteractionEnabled = YES;
                }
                else{
                    cell.dowloadMediaButton.backgroundColor = [UIColor whiteColor];
                    cell.dowloadMediaButton.layer.borderWidth = .5f;
                    cell.dowloadMediaButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    [cell.dowloadMediaButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                    [cell.dowloadMediaButton setTitle:[Utility NSLocalizedString:@"Download"] forState:UIControlStateNormal];
                    cell.dowloadMediaButton.userInteractionEnabled = NO;
                }
                
                
            }
        }
        
        // preview
        cell.preViewMediaButton.userInteractionEnabled = YES;
        cell.preViewMediaButton.backgroundColor = [HexColor SKColorHexString:@"3399FF"];
        [cell.preViewMediaButton setTitle:[Utility NSLocalizedString:@"Example"] forState:UIControlStateNormal];
        [cell.preViewMediaButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.preViewMediaButton.accessibilityLabel = [dictionaryOfMedia objectForKey:@"PREVIEWURL"];
        [cell.preViewMediaButton addTarget:self action:@selector(previewButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        if(!([[dictionaryOfMedia objectForKey:@"PREVIEWURL"] length] > 0)){
            cell.preViewMediaButton.userInteractionEnabled = NO;
            cell.preViewMediaButton.layer.borderWidth = 0.5f;
            cell.preViewMediaButton.backgroundColor = [UIColor whiteColor];
            cell.preViewMediaButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [cell.preViewMediaButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
        //follow
        //cell.followMediaButton.hidden = YES;
        // && [[dictionaryOfMedia objectForKey:@"isPublic"] integerValue] == 1
        if ([MemberInfo isSignin]) {
            cell.followMediaButton.hidden = NO;
            [cell.followMediaButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            if (isFollow){
                cell.followMediaButton.backgroundColor = [HexColor SKColorHexString:@"505050"];
                [cell.followMediaButton setTitle:[Utility NSLocalizedString:@"Unfollow"] forState:UIControlStateNormal];
                cell.followMediaButton.accessibilityLabel = @"1";
            }
            else{//[Utility NSLocalizedString:@"ChangePassword"]@"follow", @"")
                cell.followMediaButton.backgroundColor = [HexColor SKColorHexString:@"00a299"];
                [cell.followMediaButton setTitle:[Utility NSLocalizedString:@"Follow"] forState:UIControlStateNormal];
                cell.followMediaButton.accessibilityLabel = @"0";
            }
            
            [cell.followMediaButton addTarget:self action:@selector(followButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            cell.followMediaButton.backgroundColor = [UIColor whiteColor];
            cell.followMediaButton.layer.borderWidth = 0.5f;
            cell.followMediaButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [cell.followMediaButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [cell.followMediaButton setTitle:[Utility NSLocalizedString:@"Follow"] forState:UIControlStateNormal];
            cell.followMediaButton.userInteractionEnabled = NO;
        }
        
        //share Media
        [cell.shareMediaButton setTitle:[Utility NSLocalizedString:@"ShareMedia"] forState:UIControlStateNormal];
        [cell.shareMediaButton addTarget:self action:@selector(shareMediaButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        [Utility setFont:cell.dowloadMediaButton.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
        [Utility setFont:cell.preViewMediaButton.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
        [Utility setFont:cell.followMediaButton.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1){
        // Media Details ///////
        NSString * mediaCellName = [[[[NSLocale preferredLanguages] firstObject] lowercaseString] isEqualToString:@"en"]? @"MediaDetailCellEN":@"MediaDetailCellTH";
        [self.tableView registerNib:[UINib nibWithNibName:mediaCellName bundle:nil] forCellReuseIdentifier:mediaCellName];
        MediaDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:mediaCellName forIndexPath:indexPath];
        
        // Author
        // To underline text in UILable
        if ([[dictionaryOfMedia objectForKey:@"AUTHOR"] length] > 0 ) {
            [Utility setUnderlineToUILabel:cell.authorLabel Text:[dictionaryOfMedia objectForKey:@"AUTHOR"]];
            
            UITapGestureRecognizer *authorTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(authorTapping:)];
            authorTap.accessibilityLabel = cell.authorLabel.text;
            [cell.authorLabel addGestureRecognizer:authorTap];
            cell.authorLabel.userInteractionEnabled = YES;
        }
        else{
            cell.libraryLabel.text = [NSMutableString stringWithString: @"-    "];
        }
        cell.authorLabel.numberOfLines = 5;
        [Utility setFont:cell.authorLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        // Category Name
        // To underline text in UILable
        if ([[dictionaryOfMedia objectForKey:@"SUBCATEGORIESNAME"] length] > 0 ) {
            
             [Utility setUnderlineToUILabel:cell.categoryLabel Text:[dictionaryOfMedia objectForKey:@"SUBCATEGORIESNAME"]];
            
            UITapGestureRecognizer *cateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(categoryTapping:)];
            [cell.categoryLabel addGestureRecognizer:cateTap];
            cell.categoryLabel.userInteractionEnabled = YES;
        }
        else{
            cell.categoryLabel.text = [NSMutableString stringWithString: @"-    "];
        }
        
        cell.categoryLabel.numberOfLines = 5;
        [Utility setFont:cell.categoryLabel.font WithFontSize:[Utility fontSize:17.f]];
       
        // Page Count
        cell.pageCountLabel.text = [NSString stringWithFormat:@"%@ %@",[dictionaryOfMedia objectForKey:@"PAGE"],[Utility NSLocalizedString:@"Page"]];
        cell.pageCountLabel.numberOfLines = 1;
        //cell.pageCountLabel.font = [UIFont systemFontOfSize:17.f];
        [Utility setFont:cell.pageCountLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //file size
        cell.fileSizeLabel.text =  [NSString stringWithFormat:@"%.2f %@",(float)[[dictionaryOfMedia objectForKey:@"FILEFILESIZE"] integerValue]/1048576,[Utility NSLocalizedString:@"Megabyte"]];
        cell.fileSizeLabel.numberOfLines = 1;
        //cell.fileSizeLabel.font = [UIFont systemFontOfSize:17.f];
        [Utility setFont:cell.fileSizeLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //Publisher
        // To underline text in UILable
        if ([[dictionaryOfMedia objectForKey:@"PublisherName"] length] > 0 ) {
            
            [Utility setUnderlineToUILabel:cell.publisherLabel Text:[dictionaryOfMedia objectForKey:@"PublisherName"]];
            
            UITapGestureRecognizer *publisherTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(publisherTapping:)];
            [cell.publisherLabel addGestureRecognizer:publisherTap];
            cell.publisherLabel.userInteractionEnabled = YES;
        }
        else{
            cell.publisherLabel.text = [NSMutableString stringWithString: @"-    "];
        }
        
        cell.publisherLabel.numberOfLines = 5;
        [Utility setFont:cell.publisherLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //year
        cell.yearLabel.text = [[dictionaryOfMedia objectForKey:@"Year"] length] > 3 ? [dictionaryOfMedia objectForKey:@"Year"]:[NSMutableString stringWithString: @"-    "];
        cell.yearLabel.numberOfLines = 1;
        [Utility setFont:cell.yearLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //version
        cell.versionLabel.text = [[dictionaryOfMedia objectForKey:@"version"] intValue] > 0 ? [dictionaryOfMedia objectForKey:@"version"]:[NSMutableString stringWithString: @"-    "];
        cell.versionLabel.numberOfLines = 1;
        [Utility setFont:cell.versionLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //amount
        cell.amountLabel.text = [[dictionaryOfMedia objectForKey:@"Amount"] intValue] > 0 ? [dictionaryOfMedia objectForKey:@"Amount"]:[NSMutableString stringWithString: @"-    "];
        cell.amountLabel.numberOfLines = 1;
        [Utility setFont:cell.amountLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //Library
        // To underline text in UILable
        if ([[dictionaryOfMedia objectForKey:@"ROOMNAME"] length] > 0 ) {
            
            [Utility setUnderlineToUILabel:cell.libraryLabel Text:[dictionaryOfMedia objectForKey:@"ROOMNAME"]];
            
            UITapGestureRecognizer *roomTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadLibraryInfo:)];
            [cell.libraryLabel addGestureRecognizer:roomTap];
            cell.libraryLabel.userInteractionEnabled = YES;
        }
        else{
            cell.libraryLabel.text = [NSMutableString stringWithString: [NSMutableString stringWithString: @"-    "]];
        }
        
        cell.libraryLabel.numberOfLines = 5;
        [Utility setFont:cell.libraryLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        //Media Code
        cell.mediaCode.text = [[dictionaryOfMedia objectForKey:@"MEDIACODE"] length] > 0 ? [dictionaryOfMedia objectForKey:@"MEDIACODE"]:[NSMutableString stringWithString: [NSMutableString stringWithString: @"-    "]];
        cell.mediaCode.numberOfLines = 1;
        [Utility setFont:cell.mediaCode.font WithFontSize:[Utility fontSize:17.f]];
        
        //tag
        [[cell viewWithTag:6969] removeFromSuperview];
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, cell.tagView.frame.size.width, 30)];
        scrollView.tag = 6969;
        [scrollView setShowsHorizontalScrollIndicator:NO];
        [scrollView setShowsVerticalScrollIndicator:NO];
        float pointX = 0;
        for (NSString *tag in [dictionaryOfMedia objectForKey:@"TAG"]) {
            NSString *tagStr = [NSString stringWithFormat:@"#%@",tag];
            CGRect rect = CGRectMake(pointX, 0, 0, 30);
            UILabel *tagLabel = [[UILabel alloc] initWithFrame:rect];
            tagLabel.text = tagStr;
            tagLabel.font = cell.libraryLabel.font;
            tagLabel.clipsToBounds = YES;
            tagLabel.layer.cornerRadius = 7.f;
            tagLabel.backgroundColor = [HexColor SKColorHexString:@"#0EB5F7"];
            tagLabel.textAlignment = NSTextAlignmentCenter;
            
            UITapGestureRecognizer *tagTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tagTapping:)];
            tagTap.accessibilityLabel = tagStr;
            tagTap.accessibilityValue = tag;
            [tagLabel addGestureRecognizer:tagTap];
            tagLabel.userInteractionEnabled = YES;
        
            CGSize labelSize =  [Utility getWidthOfNSString:tagStr FontSize:tagLabel.font];
            
            [tagLabel setFrame:CGRectMake(pointX, 0, labelSize.width+5, 30)];
            
            pointX = pointX + labelSize.width+5 + 5;
            
            [scrollView addSubview:tagLabel];
        }
        [scrollView setContentSize:CGSizeMake(pointX, 21)];
        [cell.tagView addSubview:scrollView];
    
        //Abstract
        abstractLabel = cell.detailBodyLabel;
        abstractLabel.layer.cornerRadius = 4.f;
        abstractLabel.layer.borderWidth = 0.5f;
        abstractLabel.layer.borderColor = abstractLabel.backgroundColor.CGColor;
        cell.detailBodyLabel.text = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Abstract"],[dictionaryOfMedia objectForKey:@"ABSTRACT"]];
        cell.detailBodyLabel.numberOfLines = isSeeMoreDetails? 0 : 3;
       
        seeMoreDetailsBtn = cell.seeMoreDetailButton;
        seeMoreDetailsBtn.hidden = mediaAbstractLabelHeight>105? NO:YES;
        UIImage *image = isSeeMoreDetails? [UIImage imageNamed:@"up-arrow"]:[UIImage imageNamed:@"down-arrow"];
        [seeMoreDetailsBtn setImage:image forState:UIControlStateNormal];
        
        [seeMoreDetailsBtn addTarget:self action:@selector(seeMoreDetailTapping:) forControlEvents:UIControlEventTouchUpInside];
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 2){
        // Media Suggestion ///////
        TableViewCell *cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier"];
        
        //change collectionView Layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(5,8,0,8);
        layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
        layout.minimumLineSpacing = 5;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [cell.collectionView setCollectionViewLayout:layout];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else{
        ///// Review ///////
        [self.tableView registerNib:[UINib nibWithNibName:@"MediaReviewCell" bundle:nil] forCellReuseIdentifier:@"mediaReviewCell"];
        MediaReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mediaReviewCell" forIndexPath:indexPath];
        
        [cell.reviewTextDesc setText:[Utility NSLocalizedString:@"ReviewTextDesc"]];
        [cell.submitReview setTitle:[Utility NSLocalizedString:@"Submit_Review"] forState:UIControlStateNormal];
        
        //if([[dictionaryOfMedia objectForKey:@"isDownload"] intValue] == 1 && [[dictionaryOfMedia objectForKey:@"FILEURL"] length] > 0){
        if (isMember) {
            UIImage *imageGray = [UIImage imageNamed:@"iconvote-gray"];
            UIImage *imageYellow = [UIImage imageNamed:@"iconvote-yellow"];
            [cell.reviewRate1 addTarget:self action:@selector(starRateReviewTapping:) forControlEvents:UIControlEventTouchUpInside];
            [cell.reviewRate1 setImage:(reviewScore >=1)? imageYellow:imageGray forState:UIControlStateNormal];
            cell.reviewRate1.tag = 1;
            
            [cell.reviewRate2 addTarget:self action:@selector(starRateReviewTapping:) forControlEvents:UIControlEventTouchUpInside];
            [cell.reviewRate2 setImage:(reviewScore >=2)? imageYellow:imageGray forState:UIControlStateNormal];
            cell.reviewRate2.tag = 2;
            
            [cell.reviewRate3 addTarget:self action:@selector(starRateReviewTapping:) forControlEvents:UIControlEventTouchUpInside];
            [cell.reviewRate3 setImage:(reviewScore >=3)? imageYellow:imageGray forState:UIControlStateNormal];
            cell.reviewRate3.tag = 3;
            
            [cell.reviewRate4 addTarget:self action:@selector(starRateReviewTapping:) forControlEvents:UIControlEventTouchUpInside];
            [cell.reviewRate4 setImage:(reviewScore >=4)? imageYellow:imageGray forState:UIControlStateNormal];
            cell.reviewRate4.tag = 4;
            
            [cell.reviewRate5 addTarget:self action:@selector(starRateReviewTapping:) forControlEvents:UIControlEventTouchUpInside];
            [cell.reviewRate5 setImage:(reviewScore >=5)? imageYellow:imageGray forState:UIControlStateNormal];
            cell.reviewRate5.tag = 5;
                        
            NSDictionary *theme = [dictionaryOfMedia objectForKey:@"THEME"];
            
            [cell.submitReview setBackgroundColor:[HexColor SKColorHexString:[theme objectForKey:@"SET3"]]];
            [cell.submitReview setTitleColor:[HexColor SKColorHexString:[theme objectForKey:@"SET4"]] forState:UIControlStateNormal];
            [cell.submitReview addTarget:self action:@selector(submitReview:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        cell.reviewRate1.userInteractionEnabled = isMember;
        cell.reviewRate2.userInteractionEnabled = isMember;
        cell.reviewRate3.userInteractionEnabled = isMember;
        cell.reviewRate4.userInteractionEnabled = isMember;
        cell.reviewRate5.userInteractionEnabled = isMember;
        cell.submitReview.userInteractionEnabled = isMember;
        if (!isMember) {
            cell.submitReview.backgroundColor = [cell.submitReview.backgroundColor colorWithAlphaComponent:0.35];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section > 0) {
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CollectionHeaderView" owner:self options:nil] objectAtIndex:0];
        float headerHeight = 40.0f;
        headerView.frame = CGRectMake(0.0f, 0.0f, [[UIScreen mainScreen] bounds].size.width, headerHeight);
        headerView.backgroundColor = [UIColor whiteColor];
        
        UILabel *headerName = (UILabel*)[headerView.subviews firstObject];
        headerName.text = [arrayOfHeader objectAtIndex:section];
        headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
        //[Utility setFont:headerName.font WithFontSize:17.f];
        
        UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
        seeMoreBtn.hidden = YES;
        //        [seeMoreBtn setTitle:@"See more >>" forState:UIControlStateNormal];
        //        seeMoreBtn.titleLabel.font = [UIFont fontWithName:mainFont size:12.0f];
        //        [seeMoreBtn setTintColor:[UIColor blackColor]];
        //        seeMoreBtn.accessibilityLabel = headerName.text; //**//
        //        [seeMoreBtn addTarget:self action:@selector(seeMoreTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        if (section != 2)
            seeMoreBtn.hidden = YES;
        
        return headerView;
        
    }
    else
        return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0)
        return 0.0f;
    else
        return 40.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
//        return 315.0f;
//        mediaName.numberOfLines = 0;
        [mediaName sizeToFit];
        CGRect rect = mediaName.frame;
        return rect.origin.y + rect.size.height + 40;
    }
    else if (indexPath.section == 2){
        //collectionCellHeight + margin Top
        if ([arrayOfMediaSuggestion count] > 0)
            return collectionCellHeight + 8.f;
        
        return 0.0f;
    }
    else if (indexPath.section == 1){
        if (abstractLabel) {
            if (isSeeMoreDetails) {
                abstractLabel.numberOfLines = 0;
                [abstractLabel sizeToFit];
                CGRect rect = abstractLabel.frame;
                return rect.origin.y + rect.size.height + 60.f;
            }
        }
        return 450.f;
    }
    else
        return 200.0f;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(TableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
        NSInteger index = cell.collectionView.indexPath.row;
        
        CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
        [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
    }
    
}

//-(void)setUnderlineToUILabel:(UILabel*)label Text:(NSString*)str{
//    NSMutableAttributedString *roomName = [[NSMutableAttributedString alloc] initWithString:str];
//    [roomName addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, roomName.length)];
//   label.attributedText = roomName;
//}


-(void)seeMoreDetailTapping:(UIButton *)sender{
    isSeeMoreDetails = !isSeeMoreDetails;
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
}

-(void)categoryTapping:(UIGestureRecognizer *)sender{
    [SVProgressHUD show];
    [self searchMediaByCategory:[dictionaryOfMedia objectForKey:@"CATEGORIESID"] RoomID:[dictionaryOfMedia objectForKey:@"ROOMID"]];
}

-(void)tagTapping:(UIGestureRecognizer *)sender{
    [SVProgressHUD show];
    tagLabel = sender.accessibilityLabel;
    tagValue = sender.accessibilityValue;
    [self searchMediaListByTag:sender.accessibilityValue];
}

-(void)authorTapping:(UIGestureRecognizer *)sender{
    [SVProgressHUD show];
    tagLabel = sender.accessibilityLabel;
    [self searchMediaByAuthor:sender.accessibilityLabel];
}

-(void)publisherTapping:(UIGestureRecognizer *)sender{
    [SVProgressHUD show];
    tagLabel = [dictionaryOfMedia objectForKey:@"PublisherName"];
    [self searchMediaByPublisher:[dictionaryOfMedia objectForKey:@"PublisherID"]];
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayOfMediaSuggestion count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *itemDic = [arrayOfMediaSuggestion objectAtIndex:indexPath.row];
    
    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"default_media_cover.png"]];
    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
    
    //NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
   // [cell.mediaCoverImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
    
     [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[itemDic objectForKey:@"MediaCoverUrl"]] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"] options:0 progress:nil completed:nil];
    
    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
    [Utility setFont:cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    
    [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
    
    cell.videoImagePic.hidden = YES;
    //    if ([[[itemDic objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"])
    //        cell.videoImagePic.hidden = NO;
    
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.selectedBackgroundView = uiView;
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"collectionView didSelectItemAtIndexPath");
    // NSLog(@"------------ index : %li --------------",(long)indexPath.row );
    
    [SVProgressHUD show];
    
    NSDictionary *itemDic = [arrayOfMediaSuggestion objectAtIndex:indexPath.row];
    [self loadMediaSuggestionDetail:[itemDic objectForKey:@"MEDIAID"]];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    
    AFIndexedCollectionView *collectionView = (AFIndexedCollectionView *)scrollView;
    NSInteger index = collectionView.indexPath.row;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
}


#pragma mark --------- Show Full Photo Cover -------
-(void)coverListImageFullScreenCoverImage:(UIButton *)sender{
    //load list url
    //api/DeviceMedia/MediaPreviewImage?deviceCode={deviceCode}&mediaId={mediaId}
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(coverListImageResponse:) name:@"CoverListImage" object:nil];
    [api mediaPreviewImage:@"CoverListImage" MediaID:mediaID];
}

-(void)coverListImageResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CoverListImage" object:nil];
    [SVProgressHUD dismiss];
    if([(NSArray *)notification.userInfo isKindOfClass:[NSArray class]] && [(NSArray *)notification.userInfo count] > 0){
        // NSLog(@"cover List Image : %@",[(NSArray *)notification.userInfo description]);
        ZMImageSliderViewController *controller = [[ZMImageSliderViewController alloc] initWithOptions:0 imageUrls:(NSArray *)notification.userInfo];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else{
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"ConnectServiceF"] Controller:self];
    }
}

#pragma mark ---------- follow & unfollow media ----------
-(void)followButtonTapping:(UIButton*)sender{
    [SVProgressHUD show];
    //api/DeviceMedia/MediaFollow?deviceCode={deviceCode}&mediaId={mediaId}&followType={followType}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(followMediaResponse:) name:@"FollowMedia" object:nil];
    [api mediaFollow:@"FollowMedia" MediaID:mediaID FollowType:[sender.accessibilityLabel intValue]];
}

-(void)followMediaResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FollowMedia" object:nil];
    //NSLog(@"followMediaResponse : %@",[notification.userInfo description]);
    [SVProgressHUD dismiss];
    if([[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200 ){
        isFollow = !isFollow;
        [self.tableView reloadData];
    }
    else{
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"SomethingWrong"] Controller:self];
    }
    
    
}

#pragma mark ---------- preview media ----------
-(void)previewButtonTapping:(UIButton*)sender{
    ////crate dir ///
    [Utility createNewDirectory:tempFloderName isSupportDirectory:FALSE];
    isDownlaodingPreview = YES;
    [self createCustomProgressDownload];
    fileExtension = [[sender.accessibilityLabel lastPathComponent] pathExtension];
    //NSLog(@"file Extension : %@",fileExtension);
    [self prepare4DownloadMedia:sender.accessibilityLabel];
}

#pragma marl -------- play Video -----------
-(void)playVideoButtonTapping:(UIButton*)sender{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryAdsResponse:) name:@"getTypeAdsResponse" object:nil];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *roomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    if ([[[dictionaryOfMedia objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"a"]) {
        [api getTypeAds:@"getTypeAdsResponse" RoomId:roomID AdsType:@"Audio" mediaId:self.mediaID];
    } else if ([[[dictionaryOfMedia objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"]) {
        [api getTypeAds:@"getTypeAdsResponse" RoomId:roomID AdsType:@"Video" mediaId:self.mediaID];
    }
}

-(void)loadLibraryAdsResponse:(NSNotification *)notification  {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getTypeAdsResponse" object:nil];
    if ([notification.userInfo isKindOfClass:[NSDictionary class]]) {
        NSDictionary *json = [NSDictionary dictionaryWithDictionary:notification.userInfo];
//        [[SP shared] saveVideoAdsWithJson:json];
        //Open Area5 Ads
        NSString *roomID = [[NSUserDefaults standardUserDefaults] objectForKey:KeyCuurrentUsedLribaryID];
        Area5ViewController *vc = [[Area5ViewController alloc] initWithRoomId:roomID mediaPath:mediaPath json: json title: mediaTitle];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:NULL];
    }
}

#pragma mark ---- downlod media ------------
-(void)downloadButtonTapping:(UIButton*)sender{
    ///check Limit Day & Limit Media ///
    
    if (spMedia.isBuy && spMedia.priceList.count > 0) {
        PurchaseViewController *vc = [[PurchaseViewController alloc] initWithMedia:spMedia];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.startDownload = ^{
            [self bookingMediaBeforeDownload];
        };
        [self presentViewController:vc animated:YES completion:NULL];
    } else {
        [SVProgressHUD show];
        [self bookingMediaBeforeDownload];
    }
}

#pragma mark ------ Open Media ---------
-(void)openMediaTapping:(UIButton*)sender{
    //filePath = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.%@",memberID,mediaID,sender.accessibilityLabel]]];
    [SVProgressHUD showWithStatus:@"Loading..."];
    double delayInSeconds = 0.5; // set the time
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self prepare4ReadPDF:sender.accessibilityLabel UserID:memberID MediaID:mediaID RoomID:libraryID];
    });
}

-(void)cancelDownloadMedia:(UIButton *)sender{
    isUserCancenDownload = YES;
    [downloadTask cancel];
    downloadTask = nil;
    //[self comfirmMediaDownload:@"false"];
    [self dismissCustomProgressDownload];
}

-(void)dismissCustomProgressDownload{
    if(!isFileWriting){
        if (customProgressDownload != nil) {
            [customProgressDownload removeFromSuperview];
            customProgressDownload = nil;
        }
    }
}

-(void)prepare4DownloadMedia:(NSString*)strURL{
    ///encode url ///
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:strURL];
    //NSLog(@"URL download file : %@",url);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:(id) self delegateQueue: nil];
        //[NSURLSession sharedSession];
        downloadTask = [session dataTaskWithURL:url];
        [downloadTask resume];
        
    });
    
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    _downloadSize = [response expectedContentLength];
    _dataToDownload=[[NSMutableData alloc]init];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        downloadProgressLabel.text = [NSString stringWithFormat:@"0.00 MB/%.2f MB",_downloadSize/1048576];
    });
    
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    //NSLog(@"Receive data :%lu , Total Size:%f",(unsigned long)[_dataToDownload length],_downloadSize);
    
    [_dataToDownload appendData:data];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        downloadProgressLabel.text = [NSString stringWithFormat:@"%.2f MB/%.2f MB",(double)([_dataToDownload length]/1048576),(double)(_downloadSize/1048576)];
    });
    
    
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error{
    //NSLog(@"---- didBecomeInvalidWithError : %@ ------",error.description);
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self dismissCustomProgressDownload];
        _dataToDownload = nil;
        if (error){
            /// send confirm Dowlaod///
            //[self comfirmMediaDownload:@"false"];
            if (!isUserCancenDownload)
                [Utility showAlertViewController:[Utility NSLocalizedString:@"DownloadFileFailed"] Message:error.description Controller:(UIViewController*)self];
        }
        
    });
    
}

- (void)URLSession:(NSURLSession *)session task:(nonnull NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error{
    //NSLog(@"---- didCompleteWithError : %@ ------",error.description);
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        if (!error) {
            if(isDownlaodingPreview){
                downloadProgressLabel.text = [Utility NSLocalizedString:@"loading"];
                NSString *fileName = [NSString stringWithFormat:@"%@_%@_temp.%@",memberID,mediaID,fileExtension];
                [Utility createNewDirectory:tempFloderName isSupportDirectory:FALSE];
                [self saveData2File:_dataToDownload FileName:fileName DirectoryPath:tempFloderName];
            }
            else{
                downloadProgressLabel.text = [Utility NSLocalizedString:@"Saving"];
                /// send confirm Dowlaod///
                //[self comfirmMediaDownload:@"true"];
                
                NSString *fileName = [NSString stringWithFormat:@"%@_%@.%@",memberID,mediaID,fileExtension];
                [self saveData2File:_dataToDownload FileName:fileName DirectoryPath:folderName];
            }
        }
        else{
            _dataToDownload = nil;
            [self dismissCustomProgressDownload];
            /// send confirm Dowlaod///
            //[self comfirmMediaDownload:@"false"];
            
            if (!isUserCancenDownload)
                [Utility showAlertViewController:[Utility NSLocalizedString:@"DownloadFileFailed"] Message:error.description Controller:(UIViewController*)self];
        }
    });
    
}

-(void)saveData2File:(NSData *)fileData FileName:(NSString *)fileName DirectoryPath:(NSString*)dirPath{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        // Perform async operation
        isFileWriting = YES;
        if ([[[[fileName lastPathComponent] pathExtension] lowercaseString] isEqualToString:@"pdf"] && [fileData length] > 0) {
            
            NSError *error;
            
            filePath  = [[Utility documentPath] stringByAppendingPathComponent:[dirPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]]];
            
            BOOL isFileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            if (isFileExists) {
                [Utility removeFileInDirectory:filePath];
            }
            
            [fileData writeToFile:filePath options:NSAtomicWrite error:&error];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error){
                    //NSLog(@"Save Failed: %@", [error localizedDescription]);
                    _dataToDownload = nil;
                    [self dismissCustomProgressDownload];
                    [Utility showAlertViewController:[Utility NSLocalizedString:@"SaveFailed"] Message:error.description Controller:(UIViewController*)self];
                }
                else{
                    //NSLog(@"Save Successfully at path: %@",filePath);
                    //fileExists = YES;
                    if (isDownlaodingPreview){
                        _dataToDownload = nil;
                        isFileWriting = NO;
                        [self dismissCustomProgressDownload];
                    
                        [self prepare4ReadPDF:nil UserID:nil MediaID:nil RoomID:nil];
                    }
                    else{
                       NSString* mediaFileAtPath = [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", [filePath lastPathComponent]]];
                        
                        [self insertMediaDatatoShelf:YES MediaFilePath:mediaFileAtPath];
                        
                        _dataToDownload = nil;
                        isFileWriting = NO;
                        
                        NSString *mmemberID;
                    
                        if ([[dictionaryOfMedia objectForKey:@"isMember"] boolValue] == 1)
                            mmemberID = memberID;
                        else
                            mmemberID = deviceCode;
                        
                        //set Notification for Media Close Exp
                        //1 day = 86400 sec
                        int timePresentNotification = [Utility dateToSecond:[jsonDataDownloadFileResult objectForKey:@"RENTDATE"] EndDate:[jsonDataDownloadFileResult objectForKey:@"RENTEXPIRE"] DateFormat:@"yyyyMMddHHmmss"]-86400;
                        
                        NSString *NotificationBody = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"ExpDate"],[Utility getDateTimeForNotification:[jsonDataDownloadFileResult objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"EEEE d MMM yyyy"]];
                        
                        //NSLog(@"Body Notification : %@",NotificationBody);
                        
                        SPNotification *notification = [[SPNotification alloc] init];
                        [notification NotificationWithIdentifier:mediaID
                                                           Title:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Book"],[dictionaryOfMedia objectForKey:@"MEDIANAME"]]
                                                            Body:NotificationBody
                                                           Sound:@"default"
                                                           Badge:1
                                                        UserInfo:[NSDictionary dictionaryWithObjectsAndKeys:MediaExpNotificationHeader,@"Header", nil]
                                                      Attachment:nil
                                                     TimePresent:timePresentNotification
                                                          Repeat:NO];
                        
                        if(!isMediaPreset){
                            //remove pre-noti
                            [Utility updateBookExpireNotification:@[mediaID]];
                            [Utility setBookExpireNotification:notification];
                        }
                        
                        // load Annotation file
                        //NSLog(@"Annotation file : %@",[dictionaryOfMedia objectForKey:@"ANNOTATION"]);
                        if ([[dictionaryOfMedia objectForKey:@"ANNOTATION"] length] > 0 && [[dictionaryOfMedia objectForKey:@"isMember"] boolValue] == 1) {
                            downloadProgressLabel.text = [Utility NSLocalizedString:@"DownloadAnnotationFile"];
                            NSData *annotationData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionaryOfMedia objectForKey:@"ANNOTATION"]]];
                            NSString *strAnnotation = [[NSString alloc] initWithData:annotationData encoding:NSUTF8StringEncoding];
                            NSString *fileName = [NSString stringWithFormat:@"%@/Annotation_%@_%@_%@.txt",PDFAnnotationFolderName,mmemberID,mediaID,libraryID];
                            [Utility writePdfAnotationJson:fileName JsonString:strAnnotation];
                            
                        }
                        [self dismissCustomProgressDownload];
                        [self.tableView reloadData];
                    }
                }
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"Media is not allow to load");
                _dataToDownload = nil;
                isFileWriting = NO;
                if (customProgressDownload != nil) {
                    [customProgressDownload removeFromSuperview];
                    customProgressDownload = nil;
                }
                [Utility showAlertViewController:[Utility NSLocalizedString:@"SaveFailed"] Message:@"Media is not allow to Download" Controller:(UIViewController*)self];
            });
            
        }
    });
    
}

-(void)insertMediaDatatoShelf:(BOOL)downloadFileSuccess MediaFilePath:(NSString *)mediaFileAtPath {
    
    [dictionaryOfMedia setValue:[jsonData objectForKey:@"MEDIANAME"] forKey:@"MEDIANAME"];
    if(isMediaPreset && downloadFileSuccess){
        NSString *mediaIdPreset = [Utility encodeMediaIDPreset:mediaID];
        OfflineShelf *myShelf = [[OfflineShelf alloc] init];
        NSString *borrowDate = [Utility covertDateToFormat:[jsonDataDownloadFileResult objectForKey:@"RENTDATE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"];
        NSString *expDate = [Utility covertDateToFormat:[jsonDataDownloadFileResult objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"];
        if (expDate == nil) {
            NSString *year = [borrowDate substringToIndex:4];
            NSInteger expYear = [year integerValue] + 1;
            expDate = [NSString stringWithFormat:@"%li%@", (long)expYear, [borrowDate substringFromIndex:4]];
        }
        [myShelf OfflineShelf:mediaIdPreset
                     MemberID:nil
                      ShelfID:nil
                    LibraryID:nil
                  LibraryName:nil
                   MediaTitle:nil
                  MediaAuthor:nil
                    MediaType:nil
            MediaCoverFileURL:nil
           MediaCoverFilePath:[jsonDataDownloadFileResult objectForKey:@"CoverFilePath"]
                MediaFilePath:mediaFileAtPath
                 MediaFileURL:[dictionaryOfMedia objectForKey:@"FILEURL"]
                   DateBorrow:borrowDate
                      DateExp:expDate
            MediaOriginalName:[dictionaryOfMedia objectForKey:@"OriginalFileName"]
                    SubjectID:nil];
   
        [DBManager insertOfflineShelf:myShelf isPreset:YES];
        
    }
   else if(!isMediaPreset){
        NSString *mmemberID;
        NSString *mShelfID;
        
        if ([[dictionaryOfMedia objectForKey:@"isMember"] boolValue] == 1) {
            mmemberID = memberID;
            mShelfID = curShelfId;
        }
        else{
            mmemberID = deviceCode;
            mShelfID = PublicShelfId;
        }
        
        OfflineShelf *myShelf = [[OfflineShelf alloc] init];
            [myShelf OfflineShelf:mediaID
                         MemberID:mmemberID
                          ShelfID:curShelfId
                        LibraryID:libraryID
                      LibraryName:[dictionaryOfMedia objectForKey:@"ROOMNAME"]
                       MediaTitle:[dictionaryOfMedia objectForKey:@"MEDIANAME"]
                      MediaAuthor:[dictionaryOfMedia objectForKey:@"AUTHOR"]
                        MediaType:mediaType
                MediaCoverFileURL:[dictionaryOfMedia objectForKey:@"COVERURL"]
               MediaCoverFilePath:[jsonDataDownloadFileResult objectForKey:@"CoverFilePath"]
                    MediaFilePath:downloadFileSuccess? mediaFileAtPath:@""
                     MediaFileURL:[dictionaryOfMedia objectForKey:@"FILEURL"]
                       DateBorrow:[Utility covertDateToFormat:[jsonDataDownloadFileResult objectForKey:@"RENTDATE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                          DateExp:[Utility covertDateToFormat:[jsonDataDownloadFileResult objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                MediaOriginalName:[dictionaryOfMedia objectForKey:@"OriginalFileName"]
                        SubjectID:@""];
        [DBManager insertOfflineShelf:myShelf isPreset:NO];

    }
}


#pragma mark ------------ Review -------------
-(void)submitReview:(UIButton *)sender{
    [SVProgressHUD show];
    
    [self prepare4SubmitReviewMedia];
}

-(void)setStarRateReviewScoreImage:(int)score ArrayOfUIButton:(NSArray*)arrBtn{
    for (int i=0; i< score; i++) {
        UIButton *btn = (UIButton*)arrBtn[i];;
        [btn setImage:[UIImage imageNamed:@"iconvote"] forState:UIControlStateNormal];
    }
}

-(void)starRateReviewTapping:(UIButton *)sender{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    MediaReviewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    UIButton *btn1 = [cell viewWithTag:1];
    UIButton *btn2 = [cell viewWithTag:2];
    UIButton *btn3 = [cell viewWithTag:3];
    UIButton *btn4 = [cell viewWithTag:4];
    UIButton *btn5 = [cell viewWithTag:5];
    
    if (sender.tag == 1) {
        
        if ([[btn1 imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"iconvote-yellow"]]) {
            ////not voted //
            [btn1 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 0;
        }
        else{
            // vote ///
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 1;
        }
        
    }
    else if (sender.tag == 2) {
        if ([[btn2 imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"iconvote-yellow"]]) {
            ////not voted //
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 1;
        }
        else{
            /// vote ///
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 2;
        }
    }
    else if (sender.tag == 3) {
        if ([[btn3 imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"iconvote-yellow"]]) {
            ////not voted //
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 2;
            
        }
        else{
            /// vote ///
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 3;
        }
        
    }
    else if (sender.tag == 4) {
        
        if ([[btn4 imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"iconvote-yellow"]]) {
            ////not voted //
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            
            reviewScore = 3;
        }
        else{
            /// vote ///
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 4;
        }
        
    }
    else if (sender.tag == 5) {
        
        if ([[btn5 imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"iconvote-yellow"]]) {
            ////not voted //
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-gray"] forState:UIControlStateNormal];
            
            reviewScore = 4;
        }
        else{
            /// vote ///
            [btn1 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn4 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            [btn5 setImage:[UIImage imageNamed:@"iconvote-yellow"] forState:UIControlStateNormal];
            
            reviewScore = 5;
        }
    }
}


#pragma -------- Prepare for xRead PDF File --------
-(void)prepare4ReadPDF:(NSString*)origiFileName UserID:(NSString*)userID MediaID:(NSString*)mediaId RoomID:(NSString *)roomID{
    roomID = (roomID == NULL)? @"" : roomID;
    [[SP shared] loadPdfAdsWithRoomId:roomID mediaId:mediaID completion:^(ADPdf *ads) {
        if (ads != NULL) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                if ([[[filePath lastPathComponent] pathExtension] caseInsensitiveCompare:@"pdf"] == NSOrderedSame) {
                    if (@available(iOS 11.0, *)) {
                        NSURL *url = [NSURL fileURLWithPath:filePath];
                        PDFDocument *pdfDoc = [[PDFDocument alloc] initWithURL:url];
                        NSString *pdfPassword = @"";
                        if([pdfDoc isLocked]){
                            pdfPassword = [Utility generatePassword:origiFileName];
                            [pdfDoc unlockWithPassword:pdfPassword];
                        }
                        NSArray *arrMediaID = [[[filePath stringByDeletingPathExtension] lastPathComponent] componentsSeparatedByString:@"_"];
                        BOOL isAllowToUploadAnnatationFile = ([[dictionaryOfMedia objectForKey:@"isMember"] boolValue] == 1 &&
                                                              [[NSUserDefaults standardUserDefaults] boolForKey:AutoSyncPdfAnnatationKey] &&
                                                              ![deviceCode isEqualToString:userID])? YES : NO;
                        
                        isDownlaodingPreview = NO;
                        
                        NSString *filePath = [NSString stringWithFormat:@"%@/Annotation_%@_%@_%@.txt",PDFAnnotationFolderName,memberID,arrMediaID[1],libraryID];
                        NSString *strUrl = [NSString stringWithFormat:@"%@api/DeviceMedia/UploadAnnotation",hostIpRoomAPI];
                        PdfAnnotationInfo *info = [[PdfAnnotationInfo alloc] initWithRoomId:roomID
                                                                                    mediaId:mediaId
                                                                                pdfDocument:pdfDoc
                                                                                   filePath:filePath
                                                                                 uploadPath:strUrl
                                                                           removeFileOnExit:isDownlaodingPreview
                                                                                  canUpload:isAllowToUploadAnnatationFile
                                                                                  isPreview:isDownlaodingPreview
                                                                                     pdfURL:url
                                                                                   password:pdfPassword
                                                                                        ads:ads];
                        PdfViewController *pdfVC = [[PdfViewController alloc] initWithInfo: info];
                        UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:pdfVC];
                        [self presentViewController:nv animated:YES completion:nil];
                    }
                }
            }
            else{
                [SVProgressHUD dismiss];
                [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:[Utility NSLocalizedString:@"UnOpenFile"] Controller:(UIViewController*)self];
            }
        } else {
            [SVProgressHUD showInfoWithStatus:@"Cannot open file."];
        }
            
    }];
    
}

#pragma mark - ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissReaderViewController" object:nil];
    
    if (isDownlaodingPreview && [[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *arrMediaID = [[[filePath stringByDeletingPathExtension] lastPathComponent] componentsSeparatedByString:@"_"];

        [Utility removeFileInDirectory:filePath];
        //delete PDF Annotation
        NSString *fileAtPath = [NSString  stringWithFormat:@"Annotation_%@_%@_%@.txt",memberID,arrMediaID[1],libraryID];
        if ([fileAtPath length] > 0)
            [Utility removeFileInDirectory:fileAtPath];
        // delete last open page
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"lastOpenPage_%@",arrMediaID[1]]];
        //delete book mark
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"bookmark_%@",arrMediaID[1]]];
        
        isDownlaodingPreview = NO;
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 11.0) {
        self.tabBarController.delegate = (id) self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark ------------- Check Media before Download File ------
-(void)bookingMediaBeforeDownload{
    //api/DeviceMedia/MediaDownload?deviceCode={deviceCode}&MediaId={MediaId}&RoomId={RoomId}
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkMediaBeforeDownloadResponse:) name:@"CheckMediaDownload" object:nil];
//    [api mediaDownload:@"CheckMediaDownload" MediaID:mediaID RoomID:libraryID SubjectID:subjectID];
    
    [[RoomAPI shared] mediaDownloadWithMediaId:mediaID roomId:libraryID completion:^(SPMediaDownload *data) {
        if (data) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                ////load Cover ///
                NSString* coverImgThumbName = [NSString stringWithFormat:@"%@_%@_thumb.png",memberID,mediaID];
                NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:coverImgThumbName]];
                if (![[NSFileManager defaultManager] fileExistsAtPath: docPath]) {
                    [Utility loadMediaCoverImageFromURL:spMedia.coverFile.url
                                           OrHaveNSData:coverImageData
                                           withFileName:coverImgThumbName
                                            inDirectory:folderName];
                }
                
                NSString* coverFileAtPath = [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
                
                jsonDataDownloadFileResult = [NSDictionary dictionaryWithObjectsAndKeys:
                                              data.expDate,@"RENTEXPIRE",
                                              data.rentDate,@"RENTDATE",
                                              data.shelfId,@"shelfId",
                                              coverFileAtPath,@"CoverFilePath", nil];
                curShelfId = data.shelfId;
                [self insertMediaDatatoShelf:NO MediaFilePath:@""];
                
                /// star download //
                //crate dir ///
                [Utility createNewDirectory:folderName isSupportDirectory:FALSE];
                isDownlaodingPreview = NO;
                //isFirst = NO;
                [self createCustomProgressDownload];
                
                fileExtension = [[[dictionaryOfMedia objectForKey:@"FILEURL"] pathExtension] lowercaseString];
                //NSLog(@"file Extension : %@",fileExtension);
                [self prepare4DownloadMedia:[dictionaryOfMedia objectForKey:@"FILEURL"]];
            });
        }
    }];
}

-(void)checkMediaBeforeDownloadResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CheckMediaDownload" object:nil];
    [SVProgressHUD dismiss];
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ////load Cover ///
        NSString* coverImgThumbName = [NSString stringWithFormat:@"%@_%@_thumb.png",memberID,mediaID];
        NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:coverImgThumbName]];
        if (![[NSFileManager defaultManager] fileExistsAtPath: docPath]) {
            [Utility loadMediaCoverImageFromURL:[dictionaryOfMedia objectForKey:@"COVERURL"]
                                   OrHaveNSData:coverImageData
                                   withFileName:coverImgThumbName
                                    inDirectory:folderName];
        }
        
        NSString* coverFileAtPath = [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
        
        jsonDataDownloadFileResult = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [notification.userInfo objectForKey:@"RENTEXPIRE"],@"RENTEXPIRE",
                                      [notification.userInfo objectForKey:@"RENTDATE"],@"RENTDATE",
                                      coverFileAtPath,@"CoverFilePath", nil];
        curShelfId = [notification.userInfo objectForKey:@"TYPESHEF"];
        [self insertMediaDatatoShelf:NO MediaFilePath:@""];
        
        /// star download //
        //crate dir ///
        [Utility createNewDirectory:folderName isSupportDirectory:FALSE];
        isDownlaodingPreview = NO;
        //isFirst = NO;
        [self createCustomProgressDownload];
        
        fileExtension = [[[dictionaryOfMedia objectForKey:@"FILEURL"] pathExtension] lowercaseString];
        //NSLog(@"file Extension : %@",fileExtension);
        [self prepare4DownloadMedia:[dictionaryOfMedia objectForKey:@"FILEURL"]];
    }
    else{
        //// can not Download File ///
        [Utility showAlertViewController:[Utility NSLocalizedString:@"DownloadFileFailed"] Message:@"Media is not allow to Download" Controller:(UIViewController*)self];
    }
    
}
#pragma mark --------- create customProgressDownload --------
-(void)createCustomProgressDownload{
    isUserCancenDownload = NO;
    customProgressDownload = [[[NSBundle mainBundle] loadNibNamed:@"CustomProgressDownload" owner:self options:nil] objectAtIndex:isIPAD?1:0];
    customProgressDownload.frame = [[UIScreen mainScreen] bounds];
    
    UIView *subView0 = [customProgressDownload.subviews objectAtIndex:0];
    subView0.clipsToBounds = YES;
    subView0.layer.cornerRadius = 12.f;
    
    UIView *subView1 = [subView0.subviews objectAtIndex:0];
    cancelDownloadFile = [subView1.subviews objectAtIndex:2];
    cancelDownloadFile.exclusiveTouch = YES;
    cancelDownloadFile.userInteractionEnabled = YES;
    cancelDownloadFile.clipsToBounds = YES;
    cancelDownloadFile.layer.cornerRadius = 10.f;
    cancelDownloadFile.backgroundColor = [UIColor clearColor];
    cancelDownloadFile.layer.borderWidth = .5f;
    cancelDownloadFile.layer.borderColor = [HexColor SKColorHexString:@"E6E6E6"].CGColor;
    [cancelDownloadFile setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
    [cancelDownloadFile addTarget:self action:@selector(cancelDownloadMedia:) forControlEvents:UIControlEventTouchUpInside];
    
    downloadProgressLabel = [subView1.subviews objectAtIndex:0];
    downloadProgressLabel.text = @"";

    [[[UIApplication sharedApplication] keyWindow] addSubview:customProgressDownload];
}

#pragma mark ------------- Load Media Detail --------
-(void)loadMediaSuggestionDetail:(NSString*)suggest_mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaSuggestionDetail:) name:@"MediaSuggestionDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    [api browsMediaDetail:@"MediaSuggestionDetail" MediaID:suggest_mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaSuggestionDetail:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaSuggestionDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        mediaID = [notification.userInfo objectForKey:@"MEDIAID"];
        mediaType = [notification.userInfo objectForKey:@"MEDIAKIND"];;
        mediaTitle = [notification.userInfo objectForKey:@"MEDIANAME"];;
        
        jsonData = notification.userInfo;
        
        [self viewDidLoad];
        [self.tableView setContentOffset:CGPointZero animated:YES];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
}

#pragma mark ------------ Media Review ------------
-(void)prepare4SubmitReviewMedia{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(submitReviewMediaResponse:) name:@"ReviewMedia" object:nil];
    [api mediaVote:@"ReviewMedia" MediaID:mediaID Score:reviewScore];
}

-(void)submitReviewMediaResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReviewMedia" object:nil];
    //NSLog(@"submit Review Media Response : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if([notification.userInfo isKindOfClass:[NSString class]]){
        [Utility showAlertViewController:nil Message:[notification.userInfo description] Controller:(UIViewController*)self];
    }
    
}


#pragma mark --------- Search Media by Category ------
-(void)searchMediaByCategory:(NSString *)categoryID RoomID:(NSString *)roomID{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchMediaByCategoryResponse:) name:@"MediaItemInCategory" object:nil];
    [api mediaItems:@"MediaItemInCategory" RoomId:roomID CategoryID:categoryID SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}

-(void)searchMediaByCategoryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemInCategory" object:nil];
    //NSLog(@"get More Media Items : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        MediaListCollection *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaListCollection"];
        vc.usingLibraryID = [dictionaryOfMedia objectForKey:@"ROOMID"];
        vc.navigationTitle = [dictionaryOfMedia objectForKey:@"SUBCATEGORIESNAME"];
        vc.categoryID = [dictionaryOfMedia objectForKey:@"CATEGORIESID"];
        vc.subCategoryID = @"";
        vc.jsonData = notification.userInfo;
        vc.kindOfRoomKeyValue = @[KindOfRoomTypeDefault,@""];
        //[self prepareToDismissVC];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}

#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo:(UIGestureRecognizer *)recogn{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    [api roomsDetail:@"LibraryDetail" RoomID:libraryID memberID:memberID];
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    //NSLog(@"libraryDetailResponse : %@",notification.userInfo);
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        ///set Library Info to ViewController
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        libraryDetailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = libraryName;
        libraryDetailVC.libraryID = libraryID;
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        
        /// load items in Library
        [self loadMediaItemsInLibrary];
    }
    else{
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
    
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    [api mediaItems:@"MediaItemsInLibrary" RoomId:libraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];

}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = notification.userInfo;
       // [self prepareToDismissVC];
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
       
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
}


#pragma mark --------- Search by Tag -------
-(void)searchMediaListByTag:(NSString *)tag{
    //api/DeviceMedia/MediaSearchTag?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&findValue={findValue}&loadIndex={loadIndex}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaByResponse:) name:@"MediaItemByTag" object:nil];
    kindOfRoomKeyValue = @[KindOfRoomTypeTag,tag];
    [api mediaSearchTag:@"MediaItemByTag" RoomID:libraryID MediaKind:@"" TagValue:tag SortBy:0 LoadIndex:1];
}

#pragma mark -------- Search by Author -------
-(void)searchMediaByAuthor:(NSString*)auhorName{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaByResponse:) name:@"MediaItemByAuthor" object:nil];
    kindOfRoomKeyValue = @[KindOfRoomTypeAuthor,auhorName];
    [api mediaSearchAuthor:@"MediaItemByAuthor" RoomID:libraryID MediaKind:@"" AuthorName:auhorName SortBy:0 LoadIndex:1];
}

#pragma mark -------- Search by Publisher -------
-(void)searchMediaByPublisher:(NSString*)publisherId{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaByResponse:) name:@"MediaItemByPublisher" object:nil];
    kindOfRoomKeyValue = @[KindOfRoomTypePublisher,publisherId];
    [api mediaSearchPublisher:@"MediaItemByPublisher" RoomID:(NSString *)libraryID MediaKind:@"" PublisherId:publisherId SortBy:0 LoadIndex:1];
}

-(void)loadMediaByResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemByTag" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemByAuthor" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemByPublisher" object:nil];
    
    //NSLog(@"Media list By Tag : %@",notification.userInfo);
    //tagLabel , tagValue
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        MediaListCollection *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaListCollection"];
        vc.usingLibraryID = [dictionaryOfMedia objectForKey:@"ROOMID"];
        vc.navigationTitle = tagLabel;
        vc.categoryID = @"";
        vc.subCategoryID = @"";
        vc.jsonData = (NSArray*)notification.userInfo;
        vc.kindOfRoomKeyValue = kindOfRoomKeyValue;
        
        //[self prepareToDismissVC];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
}


#pragma mark ------- show Share Media View -----
-(void)shareMediaButtonTapping:(UIButton*)sender{
    if (shareView == nil) {
        shareView = [[[NSBundle mainBundle] loadNibNamed:@"ShareMediaUIView" owner:self options:nil] objectAtIndex:0];
        [shareView setFrame:CGRectMake(0.f, self.view.frame.size.height, self.view.frame.size.width, 237)];
        
        UILabel *tilte = (UILabel*)[shareView.subviews objectAtIndex:1];
        tilte.text = [Utility NSLocalizedString:@"Share"];
        
        UIScrollView *scrollView = (UIScrollView*)[shareView.subviews objectAtIndex:0];
        [scrollView setScrollEnabled:YES];
        scrollView.frame = CGRectMake(0, 0, self.view.frame.size.height, 100);
        scrollView.contentSize = CGSizeMake(((15+60)*6)+15, 100);
        scrollView.contentOffset = CGPointMake(0,0);
        
        int btnTag = 0;
        for (UIView *subview in scrollView.subviews) {
            UIButton *btnshare = (UIButton*)[subview.subviews objectAtIndex:0];
            btnshare.tag = btnTag;
            [btnshare addTarget:self action:@selector(btnTappingToShareMedia:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *lbShare = (UILabel*)[subview.subviews objectAtIndex:1];
            switch (btnTag) {
                case 0:
                    lbShare.text = [Utility NSLocalizedString:@"facebook"];
                    break;
                case 1:
                    lbShare.text = [Utility NSLocalizedString:@"line"];
                    break;
                case 2:
                    lbShare.text = [Utility NSLocalizedString:@"twitter"];
                    break;
                case 3:
                    lbShare.text = [Utility NSLocalizedString:@"email"];
                    break;
                case 4:
                    lbShare.text = [Utility NSLocalizedString:@"copy"];
                    break;
                case 5:
                    lbShare.text = [Utility NSLocalizedString:@"more"];
                    break;
                    
                default:
                    break;
            }
            
            btnTag = btnTag+1;
        }
        
        UIButton *btnCancelShare = (UIButton*)[shareView.subviews lastObject];
        [btnCancelShare setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
        [btnCancelShare addTarget:self action:@selector(shareMediaButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:shareView];
        [self.tableView setScrollEnabled:NO];
        
        [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            [shareView setFrame:CGRectMake(0.f, self.view.frame.size.height - shareView.frame.size.height, self.view.frame.size.width, 237)];
        } completion:nil];
    }
    else{
        [self dismissShareMediaView];
       
    }
}

-(void)dismissShareMediaView{
    [self.tableView setScrollEnabled:YES];
    
    [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        [shareView setFrame:CGRectMake(0.f, self.view.frame.size.height, self.view.frame.size.width, 237)];
    } completion:^(BOOL finished){
        [shareView removeFromSuperview];
        shareView = nil;
    }];
    
}

-(void)btnTappingToShareMedia:(UIButton *)sender{
    [self dismissShareMediaView];
    [SVProgressHUD show];
    shareMediaType = sender.tag;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareMediaGenerateID:) name:@"MediaGenQ" object:nil];
    [api MediaGenerateShareCode:@"MediaGenQ" FucntionName:@"media" UserID:memberID MediaID:mediaID RoomID:libraryID];
}

-(void)shareMediaGenerateID:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaGenQ" object:nil];
    [SVProgressHUD dismiss];
    if ([Utility isJsonDictionaryNotNull:notification.userInfo]) {
        //Response
        NSString *link2share = [NSString stringWithFormat:@"%@%@",ShareMediaUrl,[notification.userInfo objectForKey:@"Response"]];
        
        ShareMedia *share = [[ShareMedia alloc] init];
        
        if (shareMediaType == 0) {
            //share to facebook
            //[SVProgressHUD show];
            [share shareMediaToFacebook:self URL:link2share];
        }
        else if (shareMediaType == 1) {
            //share to line
            [share shareMediaToLine:link2share];
        }
        else if (shareMediaType == 2) {
            //shate to twitter
            [share shareMediaToTwitter:link2share];
        }
        else if (shareMediaType == 3) {
            //share to email
            if ([MFMailComposeViewController canSendMail]) {
                //Your code will go here
                MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
                mailVC.mailComposeDelegate = self;
                [mailVC setSubject:[NSString stringWithFormat:@"%@ (Sara+Pad Application)",[dictionaryOfMedia objectForKey:@"MEDIANAME"]]];
                [mailVC setMessageBody:link2share isHTML:NO];
                [self presentViewController:mailVC animated:YES completion:nil];
                
            } else {
                //This device cannot send email
                [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastFailed"] isSuccess:NO];
            }
            
        }
        else if (shareMediaType == 4) {
            [self dismissShareMediaView];
            //copy link
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = link2share;
        
            [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastSuccess"] isSuccess:YES];
        }
        
        else if (shareMediaType == 5){
            NSArray *objectsToShare = @[link2share];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo,
                                           UIActivityTypeMessage,
                                           UIActivityTypeCopyToPasteboard];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
            [self dismissShareMediaView];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *strToast = @"ToastSuccess";
    BOOL boolToast = YES;
    BOOL isAllowToShow = YES;
    switch (result) {
        case MFMailComposeResultSent:
            //Email sent
            break;
        case MFMailComposeResultSaved:
            //Email saved
            break;
        case MFMailComposeResultCancelled:
            //Handle cancelling of the email
            strToast = @"ToastFailed";
            boolToast = NO;
            isAllowToShow = NO;
            break;
        case MFMailComposeResultFailed:
            //Handle failure to send.
            strToast = @"ToastFailed";
            boolToast = NO;
            break;
        default:
            //A failure occurred while completing the email
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if(isAllowToShow)
    [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:strToast] isSuccess:boolToast];
}

@end

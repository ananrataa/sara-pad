//
//  MediaReviewCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaReviewCell.h"

@implementation MediaReviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.submitReview.layer.cornerRadius = 7.f;
    self.submitReview.layer.shadowOpacity = 0.25;
    self.submitReview.layer.shadowOffset = CGSizeMake(2, 2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  MediaReviewCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaReviewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *reviewRate1;
@property (weak, nonatomic) IBOutlet UIButton *reviewRate2;
@property (weak, nonatomic) IBOutlet UIButton *reviewRate3;
@property (weak, nonatomic) IBOutlet UIButton *reviewRate4;
@property (weak, nonatomic) IBOutlet UIButton *reviewRate5;

@property (weak, nonatomic) IBOutlet UIButton *submitReview;

@property (weak,nonatomic) IBOutlet UILabel *reviewTextDesc;

@end

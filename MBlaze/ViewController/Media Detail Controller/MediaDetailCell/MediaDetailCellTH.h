//
//  MediaDetailCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaDetailCellTH : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *pageCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *fileSizeLabel;
//@property (strong, nonatomic) IBOutlet UILabel *detailTitleLabel;
//@property (strong, nonatomic) IBOutlet UILabel *detailBodyLabel;
//
//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextView *detailBodyLabel;

@property (strong, nonatomic) IBOutlet UILabel *TitileAuthorLabel;
@property (strong, nonatomic) IBOutlet UILabel *TitileaCategoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *TitilePageCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *TitileFileSizeLabel;

@property (strong, nonatomic) IBOutlet UIButton *seeMoreDetailButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *abstractBottonConstraint;


@property (strong, nonatomic) IBOutlet UILabel *TitlePublisherLabel;
@property (strong, nonatomic) IBOutlet UILabel *TitleLibraryLabel;
@property (strong, nonatomic) IBOutlet UILabel *publisherLabel;
@property (strong, nonatomic) IBOutlet UILabel *libraryLabel;





@end

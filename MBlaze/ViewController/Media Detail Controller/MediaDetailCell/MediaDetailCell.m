//
//  MediaDetailCellEN.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 4/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaDetailCell.h"

@implementation MediaDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

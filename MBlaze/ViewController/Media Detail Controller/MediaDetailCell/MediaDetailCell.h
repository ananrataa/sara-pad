//
//  MediaDetailCellEN.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 4/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaDetailCell : UITableViewCell


//@property (strong, nonatomic) IBOutlet UILabel *TitileAuthorLabel;
//@property (strong, nonatomic) IBOutlet UILabel *TitileaCategoryLabel;
//@property (strong, nonatomic) IBOutlet UILabel *TitilePageCountLabel;
//@property (strong, nonatomic) IBOutlet UILabel *TitileFileSizeLabel;
//@property (strong, nonatomic) IBOutlet UILabel *TitlePublisherLabel;
//@property (strong, nonatomic) IBOutlet UILabel *TitleLibraryLabel;


@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *pageCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *fileSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *publisherLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *libraryLabel;

@property (weak, nonatomic) IBOutlet UILabel *mediaCode;

@property (weak, nonatomic) IBOutlet UIView *tagView;

@property (weak, nonatomic) IBOutlet UILabel *detailBodyLabel;


//@property (strong, nonatomic) IBOutlet UILabel *detailTitleLabel;
//@property (strong, nonatomic) IBOutlet UILabel *detailBodyLabel;
//
//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//@property (strong, nonatomic) IBOutlet UITextView *detailBodyLabel;




@property (weak, nonatomic) IBOutlet UIButton *seeMoreDetailButton;

//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *abstractBottonConstraint;





@end

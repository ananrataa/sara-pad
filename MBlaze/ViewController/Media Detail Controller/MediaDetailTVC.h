//
//  MediaDetailTVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import "CustomProgressDownload.h"
#import "ServiceAPI.h"
#import "MediaFileCache.h"

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "LibraryDetailVC.h"
#import <MessageUI/MessageUI.h>


@interface MediaDetailTVC : UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate,MFMailComposeViewControllerDelegate>{
    
    UIBarButtonItem *leftBarButton;
    UIBarButtonItem * rightBarButton;
    
    NSMutableArray *arrayOfHeader;
    NSMutableDictionary *dictionaryOfMedia;
    NSMutableArray *arrayOfMediaSuggestion;
    
    NSDictionary *jsonDataDownloadFileResult;
    
    float collectionCellWidth;
    float collectionCellHeight;
   // float navigationHeight;
    //float statusBarHeight;
    
    BOOL fileExists;
    BOOL isDownlaodingPreview;
    
    BOOL isMember;
    
    NSString* libraryID;
    NSString *folderName;
    NSString *filePath;
    NSString *tempFloderName;
    NSString *tempPath;
    NSString *mediaPath;
    
    NSString *fileExtension;
    
    //MediaFileCache *mCache;
    
    BOOL isFileWriting;
    double _downloadSize;
    NSMutableData *_dataToDownload;
    
    UIView *customProgressDownload;
    UILabel *downloadProgressLabel;
    UIButton *cancelDownloadFile;
    
    NSData *coverImageData;
    
    NSInteger mediaNameLabelHeight;
    NSInteger mediaAuthorLabelHeight;
    NSInteger mediaCategoryLabelHeight;
    NSInteger mediaPublisherLabelHeight;
    NSInteger mediaLibraryLabelHeight;
    NSInteger tagLabelHeight;
    NSInteger mediaAbstractLabelHeight;
    NSInteger mediaCodeLabelHeight;

    BOOL isSeeMoreDetails;
    UIButton *seeMoreDetailsBtn;
    
    //NSArray *arrayOfDayAndMediaLimit;
    //NSMutableDictionary *mediaCacheDic;
    
    NSString *deviceCode;
    NSString *libraryName;
    NSString *memberID;
    
    BOOL isFollow;
    //BOOL isFirst;
    
    int reviewScore;
    NSInteger mediaDownloaId;
    
    NSURLSessionDataTask *downloadTask;
    BOOL isUserCancenDownload;
    
    NSString *tagLabel;
    NSString *tagValue;
    
    ReaderDocument *document;
    ServiceAPI *api;
    LibraryDetailVC *libraryDetailVC;
    
    UIView *shareView;
    
    NSArray *kindOfRoomKeyValue;
    
    NSInteger shareMediaType;
    
    UILabel *mediaName;
    UILabel *abstractLabel;
    //__weak NSDictionary *jsonData;
    
}

@property (strong, nonatomic) NSMutableDictionary *contentOffsetDictionary;
@property (strong, nonatomic)NSString* mediaID;
@property (nonatomic, assign)BOOL isMediaPreset;
@property (strong, nonatomic)NSString* subjectID;
//@property (strong, nonatomic)NSString* libraryName;
@property (strong, nonatomic)NSString* mediaType;
@property (strong, nonatomic)NSString* mediaTitle;
@property (strong, nonatomic)NSDictionary *jsonData;
@property (nonatomic, assign)BOOL isOwnNavigationBar;
@property (strong, nonatomic)UIViewController *rootNavigationController;

@end

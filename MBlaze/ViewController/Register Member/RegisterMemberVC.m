//
//  RegisterMemberVC.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "RegisterMemberVC.h"
#import "Utility.h"
#import "ConstantValues.h"
//#import "CustomActivityIndicator.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <LineSDK/LineSDK.h>

#import "HexColor.h"
#import "ThemeTemplate.h"
#import "AddMoreMemberInfoVC.h"
#import "MemberInfo.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"


@interface RegisterMemberVC ()

@end

@implementation RegisterMemberVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:NULL forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.tabBar setHidden:YES];
    [self.tabBarController.tabBar setTranslucent:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
    if ([UIDevice.modelName containsString:@"iPad"]) {
        CGFloat w = [[UIScreen mainScreen] bounds].size.width;
        leftConstraint.constant = (w - 315)/2;
        rightConstraint.constant = (w - 315)/2;
    }
    
    TitleVC.text = [Utility NSLocalizedString:@"RegisterTitle"];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveViewController:)];
    [leftBarButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white"] style:UIBarButtonItemStyleDone target:self action:@selector(leaveToRootViewController:)];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    
    //email
    emailTextField.delegate = self;
    emailTextField.text = @"";
    emailTextField.textColor = [UIColor whiteColor];
    emailTextField.placeholder = [Utility NSLocalizedString:@"email"];
    [emailTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    emailTextField.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:emailTextField.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *emailClear = [emailTextField valueForKey:@"clearButton"];
    [emailClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    // password ///
    passwordTextField.delegate = self;
    passwordTextField.text = @"";
    passwordTextField.textColor = [UIColor whiteColor];
    passwordTextField.placeholder = [Utility NSLocalizedString:@"password"];
    [passwordTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    passwordTextField.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:passwordTextField.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *passwordClear = [passwordTextField valueForKey:@"clearButton"];
    [passwordClear setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    // confirm password ///
    confirmPasswordTextField.delegate = self;
    confirmPasswordTextField.text = @"";
    confirmPasswordTextField.textColor = [UIColor whiteColor];
    confirmPasswordTextField.placeholder = [Utility NSLocalizedString:@"Confirm_Password"];
    [confirmPasswordTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    confirmPasswordTextField.clearButtonMode = UITextFieldViewModeAlways;
    [Utility setFont:confirmPasswordTextField.font WithFontSize:[Utility fontSize:17.f]];
    
    UIButton *passwordClear1 = [confirmPasswordTextField valueForKey:@"clearButton"];
    [passwordClear1 setImage:[UIImage imageNamed:@"textField_clear"] forState:UIControlStateNormal];
    
    registerButton.layer.cornerRadius = 18.f;
    [registerButton setTitle:[Utility NSLocalizedString:@"register"] forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(emailRegister:) forControlEvents:UIControlEventTouchUpInside];
    
    orTextLabel.text = [Utility NSLocalizedString:@"or"];
    
    //[Utility setUnderlineToUILabel:skipRegister Text:[Utility NSLocalizedString:@"SkipRegister"]];
    
    //cancel Login by Line App
    [[LineSDKLogin sharedInstance] canLoginWithLineApp];
    
    //Logout user from facebook
    FBSDKLoginManager *fbManager = [[FBSDKLoginManager alloc] init];
    [fbManager logOut];
    
    api = [[ServiceAPI alloc] init];
    
    
    //UITextField
    UIColor *tint = [UIColor colorWithWhite:0.8 alpha:1];
    [emailTextField setLeftImageWithImage:[UIImage imageNamed:@"email"] tint:tint];
    [passwordTextField setLeftImageWithImage:[UIImage imageNamed:@"password"] tint:tint];
    [confirmPasswordTextField setLeftImageWithImage:[UIImage imageNamed:@"confirm_password"] tint:tint];
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leaveViewController:(UIButton*)sender{
    [self prepareToLeaveViewController];
}

-(void)prepareToLeaveViewController{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [confirmPasswordTextField resignFirstResponder];
    
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)leaveToRootViewController:(UIButton*)sender{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [confirmPasswordTextField resignFirstResponder];
    
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    [self.tabBarController.tabBar setHidden:NO];
    if (self.isOwnNavigationBar){
        [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == emailTextField) {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField){
        [confirmPasswordTextField becomeFirstResponder];
    }
    
    else if (textField == confirmPasswordTextField){
        [self prepareToEmailRegister];
    }
    return YES;
}

-(void)emailRegister:(UIButton*)sender{
    [self prepareToEmailRegister];
}

-(void)prepareToEmailRegister{
    [confirmPasswordTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [SVProgressHUD show];
    if ([[emailTextField text] length] > 0 && [Utility validateEmailFormat:[emailTextField text]]) {
        if (passwordTextField.text.length < 8) {
            [self alertRegisterError:[Utility NSLocalizedString:@"PasswordTooShot"]];
        } else {
            if ([[passwordTextField text] length] > 0 && [[confirmPasswordTextField text] length] > 0 && [[passwordTextField text] isEqualToString:[confirmPasswordTextField text]]) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emailRegisterResponse:) name:@"RegisterSaraPad" object:nil];
                // call service
                NSDictionary *dictRegistInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [emailTextField text],@"EMAIL",
                                                [passwordTextField text],@"PASSWORD", nil];
                [api registerSaraPad:@"RegisterSaraPad" isByEmail:YES info:dictRegistInfo];
            } else {
                [self alertRegisterError:[Utility NSLocalizedString:@"PassNotMatch"]];
            }
        }
    }
    else {
        [self alertRegisterError:[Utility NSLocalizedString:@"EmailInValid"]];
    }
}

-(void)alertRegisterError:(NSString*)message {
    [SVProgressHUD dismiss];
    [lblError setText:message];
    [registerButton jitter];
    [lblError flash];
}
#pragma mark ------ facebookRegister ----------
- (IBAction)facebookRegisterTapping:(id)sender{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        [self fetchuserinfo];
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager setDefaultAudience:FBSDKDefaultAudienceEveryone];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"]//@[@"public_profile", @"email"]
                               fromViewController:self
                                          handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                              if (error) {
                                                  NSLog(@"Process error");
                                              }
                                              else if ([result.declinedPermissions containsObject:@"publish_actions"]) {
                                              }
                                              else if (result.isCancelled) {
                                                  NSLog(@"Cancelled");
                                              } else {
                                                  //NSLog(@"Logged in");
                                                  [self fetchuserinfo];
                                              }
                                              
                                          }];
    }
}

-(void)fetchuserinfo {
    if ([FBSDKAccessToken currentAccessToken]){
        //token available : EAAWG899ElswBACZB5SAgJyQhYazZAZBpXkifcZB8bwv38Gg8tMdTwSPZCpdzcZCF6BvcP5QsrtAEvRczYZCrfhAHxxbE5unsZCZARR8aREACvMS5Oenj2dxe3XlNLhkVtu8evJIGcgYI0XYk6I7xaxBapMRURoSoUKblK6kbvQoKJxZCtF4eakj2gXZCRgmbTDXF09clD7VYK4wFZBf2RwZBMLejz3ey0OfZAZA0KJFMENyjZCyWNQZDZD
        //NSLog(@"token available : %@",[[FBSDKAccessToken currentAccessToken] tokenString]);
        FBSDKGraphRequest *fbRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}];
        
        [fbRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error){
                NSDictionary *dictResult = (NSDictionary*)result;
                NSString *strProfileImageUrl = [[[dictResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
              
                dictRegistInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [[FBSDKAccessToken currentAccessToken] tokenString],@"AuthenToken",
                                                [dictResult objectForKey:@"id"],@"USERID",
                                                [dictResult objectForKey:@"name"],@"DISPLAYNAME",
                                                strProfileImageUrl,@"ProfileImageURL", nil];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialRegisterResponse:) name:@"RegisterSaraPad" object:nil];
                
               [api registerSaraPad:@"RegisterSaraPad" isByEmail:NO info:dictRegistInfo];
                
            }
            else{
                NSLog(@"error %@",error);
            }
        }];
    }
}

#pragma mark ----- lineRegisterTapping ------
- (IBAction)lineRegisterTapping:(id)sender{
    // Set the LINE Login Delegate
    [LineSDKLogin sharedInstance].delegate = self;
    [[LineSDKLogin sharedInstance] startLoginWithPermissions: @[@"profile", @"openid", @"email"]];
}

#pragma mark LineSDKLoginDelegate

- (void)didLogin:(LineSDKLogin *)login
      credential:(LineSDKCredential *)credential
         profile:(LineSDKProfile *)profile
           error:(NSError *)error
{
    if (error) {
        // Login failed with an error. Use the error parameter to identify the problem.
        NSLog(@"Error: %@", error.localizedDescription);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }
    else {
        
        // Login success. Extracts the access token, user profile ID, display name, status message, and profile picture.
        NSURL * pictureURL = profile.pictureURL;
        NSString * pictureUrlString;
        // If the user does not have a profile picture set, pictureURL will be nil
        if (pictureURL) {
            pictureUrlString = profile.pictureURL.absoluteString;
        }
        
        dictRegistInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                          credential.accessToken.accessToken,@"AuthenToken",
                          profile.userID,@"USERID",
                          profile.displayName,@"DISPLAYNAME",
                          pictureUrlString,@"ProfileImageURL", nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialRegisterResponse:) name:@"RegisterSaraPad" object:nil];
        
        [api registerSaraPad:@"RegisterSaraPad" isByEmail:NO info:dictRegistInfo];
    }
}

#pragma mark ------ Register Response -----
-(void)emailRegisterResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RegisterSaraPad" object:nil];
    [SVProgressHUD dismiss];
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        NSInteger messageCode = [[notification.userInfo objectForKey:@"MessageCode"] integerValue];
        if (messageCode == 200 ) {
            [SVProgressHUD showSuccessWithStatus:[Utility NSLocalizedString:@"VerifyEmail"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        } else if (messageCode == 203) {
            [self alertRegisterError:[Utility NSLocalizedString:@"register_code_203"]];
        }
        else {
            [self alertRegisterError:[Utility NSLocalizedString:@"RegisterError"]];
        }
    }
    else
        [self alertRegisterError:[Utility NSLocalizedString:@"RegisterError"]];
}

-(void)socialRegisterResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RegisterSaraPad" object:nil];
    //NSLog(@"Register Response : %@",[notification.userInfo description]);
    
     if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
         
         if ([[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0) {
             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
             AddMoreMemberInfoVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMoreMemberInfoVC"];
             [vc setMemberID:[notification.userInfo objectForKey:@"MEMBERID"]];
             [vc setProfileUrl:[dictRegistInfo objectForKey:@"ProfileImageURL"]];
             [vc setDisplayName:[dictRegistInfo objectForKey:@"DISPLAYNAME"]];
             [vc setPassword:@""];
             [vc setUserNameSigin:@""];
             [self.navigationController pushViewController:vc animated:YES];
         }
         else if (![[notification.userInfo objectForKey:@"REQUESTDATA"] boolValue] && [[notification.userInfo objectForKey:@"MEMBERID"] length] > 0){
             //insert member info
             MemberInfo *member = [[MemberInfo alloc] init];
             [member MemberInfo:[notification.userInfo objectForKey:@"MEMBERID"]
                     SigninName:@""
                       Password:@""
                    DisplayName:[Utility covertNsNullToNSString:[dictRegistInfo objectForKey:@"DISPLAYNAME"]]
                          Email:[Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"INSTANCESIGN"]]
                      Telephone:@""
                    DateOfBirth:@""
                     ProfileUrl:[Utility covertNsNullToNSString:[dictRegistInfo objectForKey:@"ProfileImageURL"]]
                     LastSignin:@""
                            Etc:@""];
             [Utility insertMemberInfo:member VC:self];
             
         }
         else
             [self alertRegisterError:[Utility NSLocalizedString:@"RegisterError"]];
     }
    else
        [self alertRegisterError:[Utility NSLocalizedString:@"RegisterError"]];
}


@end

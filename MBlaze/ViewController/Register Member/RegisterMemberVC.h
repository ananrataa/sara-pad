//
//  RegisterMemberVC.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/3/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LineSDK/LineSDK.h>
#import "ServiceAPI.h"

@interface RegisterMemberVC : UIViewController<UITextFieldDelegate,LineSDKLoginDelegate>{
    
    __weak IBOutlet  UIButton *fbButtonRegister;
    __weak IBOutlet  UIButton *lineButtonRegister;
    
    __weak IBOutlet  UIButton *registerButton;
  //  __weak IBOutlet  UIButton *backButton;
    
    __weak IBOutlet  UILabel *TitleVC;
    __weak IBOutlet  UILabel *orTextLabel;
    
//    __weak IBOutlet  UITextField *nameTextField;
//    __weak IBOutlet  UITextField *lastNameTextField;
    
    __weak IBOutlet  UITextField *emailTextField;
    __weak IBOutlet  UITextField *passwordTextField;
    __weak IBOutlet  UITextField *confirmPasswordTextField;
    __weak IBOutlet UILabel *lblError;
    
    //__weak IBOutlet UILabel *skipRegister;
    
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    
    __weak IBOutlet NSLayoutConstraint *rightConstraint;
    ServiceAPI *api;
    
    NSDictionary *dictRegistInfo;
    
    
}

@property (nonatomic, assign)BOOL isOwnNavigationBar;

- (IBAction)facebookRegisterTapping:(id)sender;
- (IBAction)lineRegisterTapping:(id)sender;

@end

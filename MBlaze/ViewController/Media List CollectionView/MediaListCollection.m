//
//  MediaListCollection.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaListCollection.h"
#import "CollectionViewCell.h"
#import "Utility.h"
#import "FilterList.h"
#import "ConstantValues.h"
#import "SVProgressHUD.h"
//#import "MediaDetailTVC.h"
#import "ConstantValues.h"
//#import "SearchPreviewViewController.h"
//#import "SearchResultViewController.h"
#import "HexColor.h"
#import "DBManager.h"
#import "SearchViewController.h"
#import "ThemeTemplate.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "SARA_PAD-Swift.h"

@interface MediaListCollection () {
    UILabel *headerName;
}

@end

@implementation MediaListCollection
//"@"mediaContentCell";
#define cellMediaIdentifier  @"mediaContentCell" //@"collectionCellMedia";
//static NSString * const cellHeaderIdentifier = @"collectionCellHeader";

@synthesize navigationTitle;
@synthesize jsonData;
@synthesize usingLibraryID;
@synthesize categoryID;
@synthesize subCategoryID;

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewDidLoad = YES;
    navigationHeight = self.navigationController.navigationBar.frame.size.height;
    statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    [self createRightNavigationTab];

    //self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    CGSize stringHeaderNameWidth = [Utility getWidthOfNSString:navigationTitle FontSize:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]]];
    float w = self.navigationController.navigationBar.frame.size.width - 3*navigationImageSize.width;
    float scrollViewWidth = w;
    //NSLog(@"title view width : %f",scrollViewWidth);
   
    headerName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,MAX(stringHeaderNameWidth.width, scrollViewWidth), navigationImageSize.height)];
    headerName.text = navigationTitle;
    headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
    headerName.textColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet2]];
    headerName.textAlignment = NSTextAlignmentCenter;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,scrollViewWidth, navigationImageSize.height)];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    [scrollView setShowsVerticalScrollIndicator:NO];
    [scrollView setAlwaysBounceVertical:NO];
//    [scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scrollView addSubview:headerName];
    scrollView.contentSize = headerName.frame.size;
    
//    // This is the magic sauce!
//    [scrollView layoutIfNeeded];
//    [scrollView sizeToFit];
//
//    // Now the frame is set (you can print it out)
//    [scrollView setTranslatesAutoresizingMaskIntoConstraints:YES]; // make nav bar happy
//
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationItem setTitleView:scrollView];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    /// calculate collection Cell Width & Height
    collectionCellWidth = (([[UIScreen mainScreen] bounds].size.width-32)/[Utility calaulateCollectionItemInRow]);
    collectionCellHeight =  ((collectionCellWidth*4)/3)+81;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellMediaIdentifier];
    self.collectionView.contentInset = UIEdgeInsetsMake(51, 8, 0, 8);

    userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    
    api = [[ServiceAPI alloc] init];
    
    mediaKind = @"";
    loadMediaCounter = 1;
    sortBy= 0;
    
    [SVProgressHUD show];
    
    arrayOfMedia = [[NSMutableArray alloc] init];
    arrayOfFilter = [[NSMutableArray alloc] init];
    //imageUrlCache = [[NSMutableDictionary alloc] init];
    isLoadingMore = NO;
    //isAddMoreData = NO;
    self.view.userInteractionEnabled = NO;
    [self loadFilterList];
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UIScrollView *sv = (UIScrollView*) headerName.superview;
    if (sv) {
        CGFloat stringHeaderNameWidth = [Utility getWidthOfNSString:navigationTitle FontSize:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]]].width;
        if (stringHeaderNameWidth > sv.frame.size.width) {
            [headerName scrollWithDuration:5];
        }
    }
}
-(void)leftTopBarTapping:(id)sender{
    if (self.isOwnNavigationBar)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ----------- Filter List --------------
-(void)loadFilterList{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterListResponse:) name:@"CategoryFilter" object:nil];
    NSString *deviceLaguage = [[userDefault objectForKey:AppLanguageKey] uppercaseString];
    
    [api mediaKindOfRoom:@"CategoryFilter" RoomId:usingLibraryID Language:deviceLaguage CategoryID:categoryID SubCatrgory:subCategoryID AppVersion:CurrentAppVersion Type:[self.kindOfRoomKeyValue[0] intValue] Value:(NSString*)self.kindOfRoomKeyValue[1]];
    
}

-(void)filterListResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CategoryFilter" object:nil];
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        for (NSDictionary *obj in notification.userInfo) {
//            NSLog(@"--------- category list filter ----------");
//            NSLog(@"KINDCODE : %@ ",[obj objectForKey:@"KINDCODE"]);
//            NSLog(@"KINDNAME : %@ ",[obj objectForKey:@"KINDNAME"]);
            
            /// set default media kind ////
            if ([[obj objectForKey:@"KINDCODE"] isEqualToString:@""]) {
                mediaKind = [Utility covertNsNullToNSString:[obj objectForKey:@"KINDCODE"]];
            }
            
            [arrayOfFilter addObject:@[[Utility covertNsNullToNSString:[obj objectForKey:@"KINDCODE"]],[Utility covertNsNullToNSString:[obj objectForKey:@"KINDNAME"]]]];
        }
    }
    
    [self createFilterListTab];
    
    /// get Media List from json //
    [self getMediaItemsFromJsonString];
}


#pragma mark ----- get Media Items from json --------
-(void)getMediaItemsFromJsonString{
   //NSLog(@"getMediaItemsFromJsonString : %@",[jsonData description]);
    if (![Utility isServiceResponseError:jsonData] && [Utility isJsonDictionaryNotNull:jsonData])
        for (NSDictionary *mediaItem in jsonData) {
            NSMutableDictionary *mulDicItem = [[NSMutableDictionary alloc] init];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                          forKey:@"MEDIAID"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]]
                          forKey:@"MEDIANAME"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                          forKey:@"AUTHOR"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                          forKey:@"POPULARVOTE"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                          forKey:@"MediaCoverUrl"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                          forKey:@"MEDIAKIND"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"ROOMID"]]
                          forKey:@"MEDIA_ROOMID"];
            [arrayOfMedia addObject:mulDicItem];
            
            //NSLog(@"Media Title : %@",[mediaItem objectForKey:@"MEDIANAME"]);
        }
    else if (isLoadingMore)
        loadMediaCounter--;

    [self.collectionView reloadData];
    [SVProgressHUD dismiss];
    self.view.userInteractionEnabled = YES;
    
    collegetionViewContentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    float loadMoreIndicatorYpoint = collegetionViewContentHeight>=self.collectionView.frame.size.height? collegetionViewContentHeight:self.collectionView.frame.size.height;
    
    [[self.view viewWithTag:909] removeFromSuperview];
    ///load more IndicatorView
    UIView *loadMoreIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
    loadMoreIndicatorView.frame = CGRectMake(0,loadMoreIndicatorYpoint, [[UIScreen mainScreen] bounds].size.width, 50);
    [loadMoreIndicatorView setBackgroundColor:[UIColor clearColor]];
    loadMoreIndicatorView.tag = 909;
    loadMoreActivityIndicator = nil;
    loadMoreActivityIndicator = [[loadMoreIndicatorView subviews] objectAtIndex:0];
    [loadMoreActivityIndicator setHidesWhenStopped:YES];
    [self.collectionView addSubview:loadMoreIndicatorView];
    
    if(loadMoreActivityIndicator.isAnimating)[loadMoreActivityIndicator stopAnimating];
  //  if (isLoadingMore)[self.collectionView setContentOffset:CGPointMake(0,(collegetionViewContentHeight - self.collectionView.bounds.size.height - 50)) animated:YES];
    isViewDidLoad = NO;
    isLoadingMore = NO;
}

#pragma mark --------- Create Right Navigation Tab ---------
-(void)createRightNavigationTab{
    sortBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sort-black"] style:UIBarButtonItemStylePlain target:self action:@selector(sortTopBarTapping:)];
    [sortBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    sortBarButton.imageInsets = UIEdgeInsetsMake(0, 40, 0, 0);

    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search-black"] style:UIBarButtonItemStyleDone target:self action:@selector(searchTopBarTapping:)];
    [searchBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    searchBarButton.imageInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    
//    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f,navigationHeight*2, navigationHeight)];
//    [toolbar setBackgroundImage:[UIImage new] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];;
//    [toolbar setTranslucent:NO];
//    toolbar.clipsToBounds = YES;
//    toolbar.backgroundColor = [UIColor greenColor];
//    NSArray* buttons = [NSArray arrayWithObjects:sortBarButton,searchBarButton, nil];
//    [toolbar setItems:buttons animated:YES];

    self.navigationItem.rightBarButtonItems = @[searchBarButton,sortBarButton];
    
}

#pragma mark ----------- Search ------------------
-(void)searchTopBarTapping:(id)sender{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SearchViewController *searchVC = [mainSB instantiateViewControllerWithIdentifier:@"SearchViewController_"];
    searchVC.libraryID = usingLibraryID;
    searchVC.categoryID = categoryID;
    searchVC.subCategoryID = subCategoryID;
    
    UINavigationController *nvg = [[UINavigationController alloc] initWithRootViewController:searchVC];
    
    [self presentViewController:nvg animated:YES completion:nil];

}


#pragma mark ------------ Sort --------------------
-(void)sortTopBarTapping:(id)sender{
    if (sortView == nil) {
        sortView = [[[NSBundle mainBundle] loadNibNamed:@"SortUIView" owner:self options:nil] objectAtIndex:0];
        sortView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.85];
        
        UIFont *font = [UIFont fontWithName:mainFont size:17.f];
        UIButton *alphabetSortBtn = [sortView.subviews objectAtIndex:0];
        [alphabetSortBtn addTarget:self action:@selector(sortingByAlphabet:) forControlEvents:UIControlEventTouchUpInside];
        [alphabetSortBtn setTitle:[Utility NSLocalizedString:@"Sort_by_Alphabet"] forState:UIControlStateNormal];
        [alphabetSortBtn.titleLabel setFont:font];
        [alphabetSortBtn setTitleColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]] forState:UIControlStateNormal];
        
        UIButton *dateSortBtn = [sortView.subviews objectAtIndex:1];
        [dateSortBtn addTarget:self action:@selector(sortingByDate:) forControlEvents:UIControlEventTouchUpInside];
        [dateSortBtn setTitle:[Utility NSLocalizedString:@"Sort_by_Date"] forState:UIControlStateNormal];
        [dateSortBtn.titleLabel setFont:font];
        [dateSortBtn setTitleColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]] forState:UIControlStateNormal];
        
        UIButton *ratingSortBtn = [sortView.subviews objectAtIndex:2];
        [ratingSortBtn addTarget:self action:@selector(sortingByRating:) forControlEvents:UIControlEventTouchUpInside];
        [ratingSortBtn setTitle:[Utility NSLocalizedString:@"Sort_by_Rating"] forState:UIControlStateNormal];
        [ratingSortBtn.titleLabel setFont:font];
        [ratingSortBtn setTitleColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]] forState:UIControlStateNormal];
        
        [sortView setFrame:CGRectMake(0.f, -sortView.frame.size.height, self.view.frame.size.width, sortView.frame.size.height)];
        
        [self.view addSubview:sortView];
        
        [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
             [sortView setFrame:CGRectMake(0.f, 0.0f, self.view.frame.size.width, sortView.frame.size.height)];
        } completion:nil];
    }
    else{
        [self dismissSortUIView];
    }
    
}

-(void)sortingByAlphabet:(UIButton*)sender{
    //NSLog(@"-------- sorting By Alphabet ------------");
    [SVProgressHUD show];
    [self dismissSortUIView];
    sortBy = 1;
    loadMediaCounter = 1;
    [self reloadMediaItemsWithFilterAndSortType];
    //[self sortMediaInSideArrayBy:@"MEDIANAME"];
}

-(void)sortingByDate:(UIButton*)sender{
    //NSLog(@"-------- sorting By Alphabet ------------");
    [SVProgressHUD show];
    [self dismissSortUIView];
    sortBy = 2;
    loadMediaCounter = 1;
    [self reloadMediaItemsWithFilterAndSortType];
    //[self sortMediaInSideArrayBy:@"MEDIANAME"];
}

-(void)sortingByRating:(UIButton*)sender{
    //NSLog(@"-------- sorting By Rating ------------");
    [SVProgressHUD show];
    [self dismissSortUIView];
    sortBy = 3;
    loadMediaCounter = 1;
    [self reloadMediaItemsWithFilterAndSortType];
    //[self sortMediaInSideArrayBy:@"POPULARVOTE"];
    
}

#pragma mark ----- Create Filter Tab -------------
-(void)createFilterListTab{
    if(!([arrayOfFilter count]>0))
        arrayOfFilter = [FilterList getFilterList];
    
    UIView *filterView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f,self.collectionView.frame.size.width, 50.f)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.tag = 809;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.f, 10.f,filterView.frame.size.width-20, filterView.frame.size.height-20)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.scrollEnabled = YES;
    scrollView.bounces = YES;
    
    float margin = 0.0f;
    float btn_width = 150.0f;
    float btn_height = 30.0f;
    
    for (NSArray *filter in arrayOfFilter){
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(margin, 0.0f, btn_width, btn_height)];
        [btn setTintColor:[UIColor whiteColor]];
        [btn setTitle:[Utility getLocalizeFilterName:filter[0]] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:mainFont size:17.0f];
        btn.backgroundColor  = [UIColor lightGrayColor];
        btn.accessibilityLabel = filter[0];
        [btn addTarget:self action:@selector(filterMediaTapping:) forControlEvents:UIControlEventTouchUpInside];
        [Utility setFont:btn.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
        
        if ([filter[0] isEqualToString:mediaKind]){
            btn.backgroundColor  = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
            [btn setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
            xScrollViewContentOffset = margin;
        }
        
        btn.clipsToBounds = YES;
        btn.layer.cornerRadius = 15.0f;
        
        [scrollView addSubview:btn];
        
        margin = margin + 5.0f + btn_width;
    }
    //scrollView.frame = CGRectMake(0.0f, 0.0f, filterView.frame.size.width, btn_height);
    [scrollView setContentSize:CGSizeMake(margin, btn_height)];
    
    scrollView.contentOffset = CGPointMake(xScrollViewContentOffset,0);
    
    [filterView addSubview:scrollView];
    
    [self.view addSubview:filterView];
}


#pragma mark ------- filter Media --------
-(void)filterMediaTapping:(UIButton*)sender{
    //NSLog(@"filter by code : %@ , Name : %@",sender.accessibilityLabel,sender.titleLabel.text);
    if(!([mediaKind isEqualToString:sender.accessibilityLabel])){
        [SVProgressHUD show];
        UIView *headerView = [self.view viewWithTag:809];
        UIScrollView *scrollView = (UIScrollView*)[headerView.subviews firstObject];
        for (UIButton *btn in scrollView.subviews) {
            btn.backgroundColor  = [UIColor lightGrayColor];
            [btn setTintColor:[UIColor whiteColor]];
        }
        sender.backgroundColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
        [sender setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
        mediaKind = sender.accessibilityLabel;
        //[self filterMediaByAttributeName:@"MEDIAKIND" AttributeValue:mediaKind];
        loadMediaCounter = 1;
        [self reloadMediaItemsWithFilterAndSortType];
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrayOfMedia count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *itemDic = [arrayOfMedia objectAtIndex:indexPath.row];
    
    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"default_media_cover.png"]];
    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
    
    NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
    //[cell.mediaCoverImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
    
    [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"] options:0 progress:nil completed:nil];
    
    //[imageUrlCache removeObjectForKey:imgUrl];
    //[imageUrlCache setValue:imgUrl forKey:imgUrl];
    
   /* mCache = [mediaCacheDic objectForKey:[itemDic objectForKey:@"MEDIAID"]];
    if ([Utility isImage:[mCache getCoverImageMedia]]) {
        //NSLog(@"Image Data : %lu",(unsigned long)[[mCache getCoverImageMedia] length]);
        [cell.mediaCoverImageView setImage:[UIImage imageWithData:[mCache getCoverImageMedia]]];
    }
    else{
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            //NSLog(@"Cover Url : %@",[itemDic objectForKey:@"MediaCoverUrl"]);
            NSData * imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[itemDic objectForKey:@"MediaCoverUrl"]]];
            if ([Utility isImage:imgData]) {
                MediaFileCache *cache = [[MediaFileCache alloc] init];
                [cache MediaFileCacheWithMediaID:[itemDic objectForKey:@"MEDIAID"] NSData:imgData];
                [mediaCacheDic setValue:cache forKey:[itemDic objectForKey:@"MEDIAID"]];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [cell.mediaCoverImageView setImage:[UIImage imageWithData:imgData]];
                    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"spider_man_three_ver7.jpg"]];
                    
                });
            }
        });
        
    }*/
    
    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
    [Utility setFont:cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
    [Utility setFont: cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    
    [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
    
    cell.videoImagePic.hidden = YES;
//    if ([[[itemDic objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"])
//        cell.videoImagePic.hidden = NO;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"collectionView didSelectItemAtIndexPath");
    // NSLog(@"------------ index : %li --------------",(long)indexPath.row );
    
    [SVProgressHUD show];
    NSDictionary *itemDic = [arrayOfMedia objectAtIndex:indexPath.row];
    [self loadMediaDetail:[itemDic objectForKey:@"MEDIA_ROOMID"] MediaID:[itemDic objectForKey:@"MEDIAID"]];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionCellWidth, collectionCellHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 8.0f;
}


#pragma mark - UIScrollViewDelegate Methods
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self dismissSortUIView];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"contentOffset y >> %.2f",(float)self.tableView.contentOffset.y);
    
    //    [self dismissSortUIView];
    //    if(self.searchController!=nil)[self dismissSearchView];
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore && !isViewDidLoad){
        [loadMoreActivityIndicator startAnimating];
        isLoadingMore = YES;
        showLoadingMore = NO;
    }
    
    if (scrollView.contentOffset.y <= (collegetionViewContentHeight - self.collectionView.bounds.size.height + 50) && isLoadingMore  && !showLoadingMore){
        [self.collectionView setContentOffset:CGPointMake(0,(collegetionViewContentHeight - self.collectionView.bounds.size.height + 50)) animated:YES];
        showLoadingMore = YES;
        self.view.userInteractionEnabled = NO;
        [SVProgressHUD show];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadMediaCounter ++;
            [self reloadMediaItemsWithFilterAndSortType];
        });
    }
}

-(void)dismissSortUIView{
    if (sortView != nil) {
        [sortBarButton setEnabled:NO];
        [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            [sortView setFrame:CGRectMake(0.f, -sortView.frame.size.height, self.view.frame.size.width, sortView.frame.size.height)];
        } completion:^(BOOL finished)
         {
             [sortView removeFromSuperview];
             sortView = nil;
             [sortBarButton setEnabled:YES];
             
         }];
        
       /* [UIView animateWithDuration:0.29f delay:0.0f options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            for (UIButton *btn in [sortView subviews]) {
                [btn removeFromSuperview];
            }
            sortView.frame = CGRectMake(sortView.frame.origin.x, sortView.frame.origin.x, sortView.frame.size.width,  0 );
            
        } completion:^(BOOL finished)
         {
             [sortView removeFromSuperview];
             sortView = nil;
             [sortBarButton setEnabled:YES];
             
         }]; */
    }
}


#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)libraryID MediaID:(NSString*)mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
//        MediaDetailTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        //vc.libraryID = usingLibraryID;
//        //vc.libraryName = [DBManager selectLibraryNameByID:usingLibraryID];
//        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        vc.jsonData = notification.userInfo;
//        vc.rootNavigationController = self;
//
//        [self.navigationController pushViewController:vc animated:YES];
        self.title = @"";
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"UnknownError"] Controller:(UIViewController*)self];
    
}


#pragma mark -------- load More Media Items with filter ------
-(void)reloadMediaItemsWithFilterAndSortType{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMediaItemsResponse:) name:@"MediaItemPreviewWithFilter" object:nil];
    
    [api mediaItems:@"MediaItemPreviewWithFilter" RoomId:usingLibraryID CategoryID:categoryID SubCatrgory:subCategoryID MediaKind:mediaKind SortBy:sortBy LoadIndex:loadMediaCounter AppVersion:CurrentAppVersion];
    
}

-(void)reloadMediaItemsResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemPreviewWithFilter" object:nil];
    //NSLog(@"reloadMediaItemsWithFilter result : %@",notification.userInfo);
    
    if(!isLoadingMore)
        [arrayOfMedia removeAllObjects];
    jsonData = notification.userInfo;
    [self getMediaItemsFromJsonString];
}

@end

//
//  MediaListCollection.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaFileCache.h"
#import "ServiceAPI.h"

@interface MediaListCollection : UICollectionViewController <UICollectionViewDelegateFlowLayout,UISearchControllerDelegate,UISearchBarDelegate>{
   
    float navigationHeight;
    float statusBarHeight;
    CGSize navigationImageSize;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
 //   UIBarButtonItem *leftBarButton;
    UIBarButtonItem *sortBarButton;
    
    UIView *sortView;
    
    NSMutableArray *arrayOfMedia;
    NSMutableArray *arrayOfFilter;
    //NSMutableDictionary *mediaCacheDic;
    
    //MediaFileCache *mCache;
    ServiceAPI *api;
    
    NSString *mediaKind;
    int xScrollViewContentOffset;
    
    NSString *deviceCode;

    NSUserDefaults *userDefault;
    
    int loadMediaCounter;
    int sortBy;
    
    BOOL isLoadingMore;
    BOOL isViewDidLoad;
   // BOOL isAddMoreData;
    
    NSString *textForSearch;
    
    UIView *bgViewNav;
    
    BOOL isSearching;
    
    UIActivityIndicatorView *loadMoreActivityIndicator;
    BOOL showLoadingMore;
    float collegetionViewContentHeight;
    
   // NSMutableDictionary *imageUrlCache;

}

@property (nonatomic, assign)BOOL isOwnNavigationBar;
@property (strong,nonatomic)NSString* categoryID;
@property (strong,nonatomic)NSString* subCategoryID;
@property (strong,nonatomic)NSString* navigationTitle;
@property (strong,nonatomic)NSArray* jsonData;
@property (strong,nonatomic)NSString *usingLibraryID;
@property (nonatomic, strong) UISearchController *searchController;

@property (strong,nonatomic)NSArray *kindOfRoomKeyValue;

@end

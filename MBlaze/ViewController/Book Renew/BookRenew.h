//
//  BookRenewViewController.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookRenew : UIViewController{
    
    UIBarButtonItem *leftBarButton;
    
    __weak IBOutlet UILabel *labelJsonNotification;
    
    
}

@property (strong,nonatomic)NSDictionary *jsonNotification;
@property (assign,nonatomic)BOOL isOwnNavigationBar;

@end

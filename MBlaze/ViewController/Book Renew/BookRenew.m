//
//  BookRenewViewController.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "BookRenew.h"
#import "Utility.h"

@interface BookRenew ()

@end

@implementation BookRenew

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Book Renew"];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName :
                                                                        [UIColor blackColor]};
    
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    CGSize navigationImageSize = CGSizeMake(navigationHeight-16, navigationHeight-16);
    
    leftBarButton = [[UIBarButtonItem alloc] initWithImage:[Utility rescaleImage:[UIImage imageNamed:@"arrow_back_black"] scaledToSize:navigationImageSize] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[UIColor blackColor]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    //NSLog(@"BookRenew json : %@",[self.jsonNotification description]);
    labelJsonNotification.text = @"BookRenew BookRenew BookRenew";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leftTopBarTapping:(id)sender{
    if (self.isOwnNavigationBar)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
    
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

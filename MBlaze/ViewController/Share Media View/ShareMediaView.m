//
//  ShareMediaView.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 1/6/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ShareMediaView.h"
#import "Utility.h"
#import "CustomActivityIndicator.h"
#import "ShareMedia.h"
#import "ConstantValues.h"

@implementation ShareMediaView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
       UIView *shareView = [[[NSBundle mainBundle] loadNibNamed:@"ShareMediaUIView" owner:self options:nil] objectAtIndex:0];
        [shareView setFrame:CGRectMake(0.f, self.frame.size.height, self.frame.size.width, 237)];
        
        UILabel *tilte = (UILabel*)[shareView.subviews objectAtIndex:1];
        tilte.text = [Utility NSLocalizedString:@"Share"];
        
        UIScrollView *scrollView = (UIScrollView*)[shareView.subviews objectAtIndex:0];
        [scrollView setScrollEnabled:YES];
        scrollView.frame = CGRectMake(0, 0, self.frame.size.height, 100);
        scrollView.contentSize = CGSizeMake(((15+60)*5)+15, 100);
        scrollView.contentOffset = CGPointMake(0,0);
        
        int btnTag = 0;
        for (UIView *subview in scrollView.subviews) {
            UIButton *btnshare = (UIButton*)[subview.subviews objectAtIndex:0];
            btnshare.tag = btnTag;
           // [btnshare addTarget:self action:@selector(btnTappingToShareMedia:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *lbShare = (UILabel*)[subview.subviews objectAtIndex:1];
            switch (btnTag) {
                case 0:
                    lbShare.text = [Utility NSLocalizedString:@"facebook"];
                    break;
                case 1:
                    lbShare.text = [Utility NSLocalizedString:@"line"];
                    break;
                case 2:
                    lbShare.text = [Utility NSLocalizedString:@"twitter"];
                    break;
                case 3:
                    lbShare.text = [Utility NSLocalizedString:@"email"];
                    break;
                case 4:
                    lbShare.text = [Utility NSLocalizedString:@"copy"];
                    break;
                    
                default:
                    break;
            }
            btnTag = btnTag+1;
        }
        
        UIButton *btnCancelShare = (UIButton*)[shareView.subviews lastObject];
        [btnCancelShare setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
       // [btnCancelShare addTarget:self action:@selector(shareMediaButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:shareView];
        
        [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            [shareView setFrame:CGRectMake(0.f, self.frame.size.height - shareView.frame.size.height, self.frame.size.width, 237)];
        } completion:nil];
    }
    return self;
}

-(void)dismissShareMediaView{
    [UIView animateWithDuration:0.55f delay:0.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        [self setFrame:CGRectMake(0.f, self.frame.size.height, self.frame.size.width, 237)];
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
    
}


//-(void)btnTappingToShareMedia:(UIButton *)sender{
//    [self dismissShareMediaView];
//    [CustomActivityIndicator presentActivityIndicatorView];
//    shareMediaType = sender.tag;
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareMediaGenerateID:) name:@"MediaGenQ" object:nil];
//    [api MediaGenerateShareCode:@"MediaGenQ" FucntionName:@"media" UserID:memberID MediaID:mediaID RoomID:libraryID];
//}

//-(void)shareMediaGenerateID:(NSNotification *)notification{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaGenQ" object:nil];
//    [CustomActivityIndicator dismissActivityIndicatorView];
//    if ([Utility isJsonDictionaryNotNull:notification.userInfo]) {
//        //Response
//        NSString *link2share = [NSString stringWithFormat:@"%@%@",ShareMediaUrl,[notification.userInfo objectForKey:@"Response"]];
//
//        ShareMedia *share = [[ShareMedia alloc] init];
//
//        if (shareMediaType == 0) {
//            //share to facebook
//            //[CustomActivityIndicator presentActivityIndicatorView];
//            [share shareMediaToFacebook:self URL:link2share];
//        }
//        else if (shareMediaType == 1) {
//            //share to line
//            [share shareMediaToLine:link2share];
//        }
//        else if (shareMediaType == 2) {
//            //shate to twitter
//            [share shareMediaToTwitter:link2share];
//        }
//        else if (shareMediaType == 3) {
//            //share to email
//            if ([MFMailComposeViewController canSendMail]) {
//                //Your code will go here
//                MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
//                mailVC.mailComposeDelegate = self;
//                [mailVC setSubject:[NSString stringWithFormat:@"%@ (Sara+Pad Application)",[dictionaryOfMedia objectForKey:@"MEDIANAME"]]];
//                [mailVC setMessageBody:link2share isHTML:NO];
//                [self presentViewController:mailVC animated:YES completion:nil];
//
//            } else {
//                //This device cannot send email
//                [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastFailed"] isSuccess:NO];
//            }
//
//        }
//        else if (shareMediaType == 4) {
//            //        [self dismissShareMediaView];
//            //        [share shareMediaToTwitter:link2share];
//            //copy link
//            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//            pasteboard.string = link2share;
//
//            [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastSuccess"] isSuccess:YES];
//        }
//
//    }
//}
//
//- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    NSString *strToast = @"ToastSuccess";
//    BOOL boolToast = YES;
//    BOOL isAllowToShow = YES;
//    switch (result) {
//        case MFMailComposeResultSent:
//            //Email sent
//            break;
//        case MFMailComposeResultSaved:
//            //Email saved
//            break;
//        case MFMailComposeResultCancelled:
//            //Handle cancelling of the email
//            strToast = @"ToastFailed";
//            boolToast = NO;
//            isAllowToShow = NO;
//            break;
//        case MFMailComposeResultFailed:
//            //Handle failure to send.
//            strToast = @"ToastFailed";
//            boolToast = NO;
//            break;
//        default:
//            //A failure occurred while completing the email
//            break;
//    }
//    [self dismissViewControllerAnimated:YES completion:NULL];
//
//    if(isAllowToShow)
//        [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:strToast] isSuccess:boolToast];
//}



@end

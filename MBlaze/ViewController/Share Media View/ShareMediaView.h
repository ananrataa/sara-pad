//
//  ShareMediaView.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 1/6/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ShareMediaView : UIView <MFMailComposeViewControllerDelegate> {
    NSInteger shareMediaType;
}

@end

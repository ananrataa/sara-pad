//
//  UserLibraryTVCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "UserLibraryTVCell.h"

@implementation UserLibraryTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.libraryPic.clipsToBounds = YES;
    self.libraryPic.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.libraryPic.layer.borderWidth = 1;
    self.libraryPic.layer.cornerRadius = self.libraryPic.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

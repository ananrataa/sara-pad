//
//  UserInfoTVCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 8/17/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userEmailName;

@property (weak, nonatomic) IBOutlet UILabel *lastSigninLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblBag;

@end

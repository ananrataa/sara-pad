//
//  PrivacyOpenSourceVC.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "PrivacyOpenSourceVC.h"
#import "Utility.h"
#import "CustomActivityIndicator.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "ConstantValues.h"

@interface PrivacyOpenSourceVC ()

@end

@implementation PrivacyOpenSourceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSLog(@"title : %@",self.navigationTitle);
   // NSLog(@"url : %@",self.url);
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationItem setTitle:self.navigationTitle];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    //navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissToRootViewController:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    [self prepare4LoadWebPage];
}

-(void)dismissToRootViewController:(id)sender{
    if (mWebView != nil)
        [mWebView stopLoading];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)dismissViewController:(id)sender{
    if (mWebView != nil)
        [mWebView stopLoading];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationTitle = nil;
    self.url = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepare4LoadWebPage{
    errorView.hidden = YES;
    
    [CustomActivityIndicator presentActivityIndicatorView];

    //NSURL *url_ = [NSURL URLWithString:self.url];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:self.url];
    mWebView.delegate = self;
    [mWebView loadRequest:urlRequest];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [CustomActivityIndicator dismissActivityIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [CustomActivityIndicator dismissActivityIndicatorView];
    errorLabel.text = [Utility NSLocalizedString:@"WebPageOpenTitle"];
    errorView.hidden = NO;
    
}

-(void)reloadWebPage:(UIButton *)sender{
    [self prepare4LoadWebPage];
}

@end

//
//  PrivacyOpenSourceVC.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyOpenSourceVC : UIViewController<UIWebViewDelegate>{
    
    __weak IBOutlet UIWebView *mWebView;
    
    __weak IBOutlet UILabel *errorLabel;
    
    __weak IBOutlet UIButton *reloadWebPageBtn;
    
    __weak IBOutlet UIView *errorView;
    
    //UIBarButtonItem *leftBarButton;
}


@property (strong,nonatomic)NSString* navigationTitle;
@property (strong,nonatomic)NSURL* url;
@end

//
//  AboutTVC.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 30/1/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AboutTVC.h"
#import "Utility.h"
#import "PrivacyOpenSourceVC.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "ConstantValues.h"

@interface AboutTVC (){
    
    CGSize screenSize;
}

@end

@implementation AboutTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"About"]];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"currentVersionCell"];
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"lastestVersionCell"];
    
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"policyCell"];
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"sourceCell"];
    
    screenSize = [[UIScreen mainScreen] bounds].size;
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
     //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:NO];
    
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:dummyView];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
}

-(void)dismissViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            //cell = [tableView dequeueReusableCellWithIdentifier:@"currentVersionCell" forIndexPath:indexPath];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"currentVersionCell"];
            
            [[cell viewWithTag:101] removeFromSuperview];
            [[cell viewWithTag:102] removeFromSuperview];
            
            [self createLabelInsideRow:cell Title:[Utility NSLocalizedString:@"AppCurrentVersion"] SubTitle:CurrentAppVersion];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        }
        else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"lastestVersionCell" forIndexPath:indexPath];
            
            [[cell viewWithTag:103] removeFromSuperview];
            [[cell viewWithTag:104] removeFromSuperview];
            
            [self createLabelInsideRow:cell Title:[Utility NSLocalizedString:@"LatestAppVersion"] SubTitle:[NSString stringWithFormat:@"%@",[userDefault objectForKey:LatestAppVersionKey]]];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    else{
        if(indexPath.row == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"policyCell" forIndexPath:indexPath];
            
             [self createLabelInsideRow:cell Title:[Utility NSLocalizedString:@"PrivacyPolicy"] SubTitle:@"→"];
        }
        else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"sourceCell" forIndexPath:indexPath];
            
             [self createLabelInsideRow:cell Title:[Utility NSLocalizedString:@"OpenSourceLicenses"] SubTitle:@"→"];
        }
    }
    
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}


-(void)createLabelInsideRow:(UITableViewCell *)cell Title:(NSString *)title SubTitle:(NSString *)subTitle{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, screenSize.width-130,50)];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont fontWithName:mainFont size:17.f];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.width-115, 0,100,50)];
    subTitleLabel.text = subTitle;
    subTitleLabel.textAlignment = NSTextAlignmentRight;
    subTitleLabel.textColor = [UIColor darkGrayColor];
    subTitleLabel.font = [UIFont fontWithName:mainFont size:17.f];

    [cell addSubview:titleLabel];
    [cell addSubview:subTitleLabel];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if(indexPath.row == 0){
             [self prepareForSegue:0];
        }
        else{
            [self prepareForSegue:1];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(int)check{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    PrivacyOpenSourceVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"PrivacyOpenSourceVC"];
    vc.navigationTitle = check==0? [Utility NSLocalizedString:@"PrivacyPolicy"] : [Utility NSLocalizedString:@"OpenSourceLicenses"];
    vc.url = check==0? [NSURL URLWithString:TermOfServiceUrl] : [NSURL URLWithString:PrivacyPolicyUrl];
    [self.navigationController pushViewController:vc animated:YES];
}
@end

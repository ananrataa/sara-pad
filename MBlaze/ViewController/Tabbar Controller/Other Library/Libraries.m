//
//  Tabbar3.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "Libraries.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "ConstantValues.h"
#import "TableViewCell.h"
#import "CollectionViewCell.h"
//#import "MediaDetailTVC.h"
#import "MediaFileCache.h"
#import "SVProgressHUD.h"
//#import "MediaListCollection.h"
#import "HistoryTVC.h"
#import "SignInVC.h"
#import "LibraryDetailVC.h"
#import "SettingTCV.h"
#import "Help.h"
#import "AboutTVC.h"
#import "RoomProfileLogo.h"
#import "DBManager.h"
#import "ThemeTemplate.h"
#import "HexColor.h"
#import "UIImageView+WebCache.h"
#import "SARA_PAD-Swift.h"
@interface Libraries () {
    NSString *CellIdentifier;
    NSString *roomName;
}

@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@end

@implementation Libraries

#define headerHeight 40.0f

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tabBarController.tabBar setHidden:NO];
    
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};

    
   

    CellIdentifier = @"lib_cell";
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"LibTableViewCell" bundle:NULL] forCellReuseIdentifier:CellIdentifier];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self setup];
}

-(void)setup {
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    barButtonLibrary = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
    [barButtonLibrary setBackgroundImage:[Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
    [barButtonLibrary addTarget:self action:@selector(libraryImageTapping:) forControlEvents:UIControlEventTouchUpInside];
    barButtonLibrary.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    barButtonLibrary.clipsToBounds = YES;
    barButtonLibrary.layer.cornerRadius = navigationImageSize.height/2;
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    UIBarButtonItem *libraryLogo = [[UIBarButtonItem alloc] initWithCustomView:barButtonLibrary];
    self.navigationItem.leftBarButtonItems = @[leftBarButton,libraryLogo];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    //// select current Library id ////
    curUsedLibraryID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    
    arrayOfLibraries = [[NSMutableArray alloc] init];
    
    
    api = [[ServiceAPI alloc] init];
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    
    [SVProgressHUD show];
    
    [self loadLibraryLogoImage];
    
    loadDataIndex = 1;
    //// load Other Library ///
    [self loadOtherLibraries];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"TabbarLibraries"]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    if (!([[userDefault objectForKey:KeyCuurrentUsedLribaryID] isEqualToString:curUsedLibraryID])) {
        [self setup];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangingLibrary:) name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchHistory:) name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signOut:) name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting:) name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signIn:) name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp:) name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAbout:) name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memberProfile) name:@"MemberProfile" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redeemCode) name:@"Redeem" object:nil];
 
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberProfile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Redeem" object:nil];

    //[[self.view viewWithTag:908] removeFromSuperview];
    [[self.view viewWithTag:909] removeFromSuperview];
    //[mediaCacheDic removeAllObjects];
}


-(void)leftTopBarTapping:(id)sender{
    //NSLog(@"------------- leftTopBarTapping --------------");
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)libraryImageTapping:(UIButton *)sender{
    [SVProgressHUD show];
    selectedLibraryID = curUsedLibraryID;
    [self loadLibraryInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ------- load Other Libraries
-(void)loadOtherLibraries{
    if (loadDataIndex == 1) {
        [arrayOfLibraries removeAllObjects];
    }
    if (!isNoMoreToLoad) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadOtherLibrariesResponse:) name:@"OtherLibraries" object:nil];
//        [api mediasLibrary:@"OtherLibraries" MediaKind:@"" LoadIndex:loadDataIndex];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibrariesResponse:) name:@"Libraries" object:nil];
        [api libraries:@"Libraries" index:loadDataIndex];
        
    } else {
        [SVProgressHUD dismiss];
    }
}

-(void)loadLibrariesResponse:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Libraries" object:nil];
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        if ([notification.userInfo isKindOfClass:[NSArray class]]) {
            for (NSDictionary *obj in notification.userInfo) {
                [arrayOfLibraries addObject: [[SPLib alloc] initWithJson:obj]];
            }
        }
        
        [self.tableView reloadData];
        
        [SVProgressHUD dismiss];
    } else {
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"no_more_data"] Controller:(UIViewController*)self];
        isNoMoreToLoad = YES;
    }
    
    if (isRefreshing)[self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    else if (isLoadingMore)[self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height) animated:YES];
    
    isLoadingMore = NO;
    isRefreshing = NO;
    self.view.userInteractionEnabled = YES;
}
-(void)loadOtherLibrariesResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OtherLibraries" object:nil];
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){        
        if ([notification.userInfo isKindOfClass:[NSArray class]]) {
            for (NSDictionary *catagoryObj in notification.userInfo) {
                //NSLog(@"CATEGORIESNAME : %@ ",[catagoryObj objectForKey:@"CATEGORIESNAME"]);
                NSMutableDictionary *mulDic = [[NSMutableDictionary alloc] init];
                [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"ROOMNAME"]]
                          forKey:@"ROOMNAME"];
                [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"ROOMID"]]
                          forKey:@"ROOMID"];
                [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"ROOMCODE"]]
                          forKey:@"ROOMCODE"];
                [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"MessaageText"]]
                          forKey:@"MessaageText"];
                
                NSMutableArray *arrayOfMediaItem = [[NSMutableArray alloc] init];
                if ([[catagoryObj objectForKey:@"MediaItems"] isKindOfClass:[NSArray class]] && [[catagoryObj objectForKey:@"MediaItems"] count] > 0)
                    for (NSDictionary *mediaItem in [catagoryObj objectForKey:@"MediaItems"]) {
                        //NSLog(@"MEDIANAME : %@",[mediaItem objectForKey:@"MEDIANAME"]);
                        NSMutableDictionary *mulDicItem = [[NSMutableDictionary alloc] init];
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                                      forKey:@"MEDIAID"];
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]] forKey:@"MEDIANAME"];
                        
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                                      forKey:@"AUTHOR"];
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                                      forKey:@"POPULARVOTE"];
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                                      forKey:@"MediaCoverUrl"];
                        [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                                      forKey:@"MEDIAKIND"];
                        
                        [arrayOfMediaItem addObject:mulDicItem];
                    }
                [mulDic setValue:arrayOfMediaItem forKey:@"MediaItems"];
                
                [arrayOfLibraries addObject:mulDic];
                
            }
        }
        
        if ([arrayOfLibraries count] == 1) {
            /// calculate collection Cell Width & Height
            collectionCellWidth = (([[UIScreen mainScreen] bounds].size.width-32)/[Utility calaulateCollectionItemInRow]);
            collectionCellHeight =  ((collectionCellWidth*4)/3)+90;
        }
        
        [self.tableView reloadData];
        
        [[self.view viewWithTag:909] removeFromSuperview];
        [SVProgressHUD dismiss];
    } else {
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"no_more_data"] Controller:(UIViewController*)self];
        isNoMoreToLoad = YES;
    }
    
    if (isRefreshing)[self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    else if (isLoadingMore)[self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height) animated:YES];
    
    isLoadingMore = NO;
    isRefreshing = NO;
    self.view.userInteractionEnabled = YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayOfLibraries count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LibTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    SPLib *curLib = (SPLib*)[arrayOfLibraries objectAtIndex:indexPath.row];
    cell.curLib = curLib;
    
//    if (cell == nil) {
//        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//
//    UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CollectionHeaderView" owner:self options:nil] objectAtIndex:0];
//    headerView.backgroundColor = [UIColor whiteColor];
//    headerView.frame = CGRectMake(0.0f, 0.0f, [[UIScreen mainScreen] bounds].size.width, headerHeight);
//
//    NSDictionary *header = [arrayOfLibraries objectAtIndex:indexPath.row];
//
//    CGSize stringHeaderNameWidth = [Utility getWidthOfNSString:[header objectForKey:@"ROOMNAME"] FontSize:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]]];
//
//    UILabel *headerName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, stringHeaderNameWidth.width, 30)];
//    headerName.text = [header objectForKey:@"ROOMNAME"];
//    headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
//    headerName.textColor = [UIColor blackColor];
//
//    UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
//    [seeMoreBtn setTitle:[Utility NSLocalizedString:@"SeeMore_Details"] forState:UIControlStateNormal];
//    // seeMoreBtn.titleLabel.font = [UIFont fontWithName:mainFont size:12.0f];
//    seeMoreBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//    seeMoreBtn.clipsToBounds = YES;
//    seeMoreBtn.layer.cornerRadius = 10.f;
//    seeMoreBtn.layer.borderWidth = 1.f;
//    seeMoreBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    [seeMoreBtn setTintColor:[UIColor blackColor]];
//    seeMoreBtn.accessibilityLabel = [header objectForKey:@"ROOMID"]; //**//
//    [seeMoreBtn addTarget:self action:@selector(libraryDetailTapping:) forControlEvents:UIControlEventTouchUpInside];
//    [Utility setFont:seeMoreBtn.titleLabel.font  WithFontSize:14.f];
//
//    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, headerView.frame.size.height-31, headerView.frame.size.width-(10+seeMoreBtn.frame.size.width+10+10), 30)];
//    [scrollView setShowsHorizontalScrollIndicator:NO];
//    [scrollView setShowsVerticalScrollIndicator:NO];
//    [scrollView setAlwaysBounceVertical:NO];
//    [scrollView addSubview:headerName];
//    scrollView.contentSize = headerName.frame.size;
//    [headerView addSubview:scrollView];
//
//    [cell.contentView addSubview:headerView];
//
//
//    /////change collectionView Layout
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//    layout.sectionInset = UIEdgeInsetsMake(headerHeight + 8,8,0,8);
//    layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
//    layout.minimumInteritemSpacing = 5;
//    layout.scrollDirection = [arrayOfLibraries count] == 1? UICollectionViewScrollDirectionVertical:UICollectionViewScrollDirectionHorizontal;
//    [cell.collectionView setCollectionViewLayout:layout];
//
//    cell.collectionView.tag = indexPath.row;
//    [cell.collectionView reloadData];
//
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(TableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
//    NSInteger index = cell.collectionView.indexPath.row;
////    cell.collectionView.tag = indexPath.row;
//    
//    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
//    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 95;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [SVProgressHUD show];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SPLib *curLib = (SPLib*)[arrayOfLibraries objectAtIndex:indexPath.row];
    selectedLibraryID = curLib.libId;
    [self loadLibraryInfo];
}
#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSDictionary *categoryDic = [arrayOfLibraries objectAtIndex:collectionView.tag];
    return [(NSMutableArray*)[categoryDic objectForKey:@"MediaItems"] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *categoryDic = [arrayOfLibraries objectAtIndex:collectionView.tag];
    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
    
    [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[itemDic objectForKey:@"MediaCoverUrl"]] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]  options:0 progress:nil completed:nil];
    
   
    
    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
    [Utility setFont:cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
    
    cell.videoImagePic.hidden = YES;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [SVProgressHUD show];
    
    //// load data /////
    NSDictionary *categoryDic = [arrayOfLibraries objectAtIndex:collectionView.tag];
    selectedLibraryID = [categoryDic objectForKey:@"ROOMID"];
    
    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
    
    [self loadMediaDetail:selectedLibraryID MediaID:[itemDic objectForKey:@"MEDIAID"]];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    categoryDic = nil;
    itemDic =nil;
}


#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isNoMoreToLoad) {
        return;
    }
    ///load more
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
    if (contentOffsetY - maxOffsetY > 20  && !isLoadingMore) {
        isLoadingMore = YES;
        showRefreshing = NO;
        [SVProgressHUD showWithStatus:@"Loading more..."];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadDataIndex ++;
            [self loadOtherLibraries];
        });
    }
    return;
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore) {
        isLoadingMore = YES;
        showRefreshing = NO;
    }
    /// refresh
    else if (scrollView.contentOffset.y <= -[Utility bounceHeightForLoadMore] && !isRefreshing) {
        isRefreshing = YES;
        showRefreshing = NO;
    }
    
    if (scrollView.contentOffset.y >= -50 && isRefreshing && !showRefreshing) {
//        showRefreshing = YES;
//        [self.tableView setContentOffset:CGPointMake(0, -50) animated:YES];
//        self.view.userInteractionEnabled = NO;
//        [SVProgressHUD showWithStatus:@"Loading more..."];
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            loadDataIndex = 1;
//            isNoMoreToLoad = NO;
//            [self loadOtherLibraries];
//        });
    }
    else if (scrollView.contentOffset.y <= (self.tableView.contentSize.height - self.tableView.bounds.size.height + 50) && isLoadingMore  && !showRefreshing){
        [self.tableView setContentOffset:CGPointMake(0,(self.tableView.contentSize.height - self.tableView.bounds.size.height + 50)) animated:YES];
        showRefreshing = YES;
        self.view.userInteractionEnabled = NO;
        [SVProgressHUD showWithStatus:@"Loading more..."];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadDataIndex ++;
            [self loadOtherLibraries];
        });
    }
    
//    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
//    CGFloat horizontalOffset = scrollView.contentOffset.x;
//    AFIndexedCollectionView *collectionView = (AFIndexedCollectionView *)scrollView;
//    NSInteger index = collectionView.indexPath.row;
//    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
    
}


#pragma mark ---------- ibrary Details Delegate ----------
-(void)libraryDetailTapping:(UIButton*)sender{
    //NSLog(@"--------- See More Tapping ---------");
    [SVProgressHUD show];
    selectedLibraryID = sender.accessibilityLabel;
    [self loadLibraryInfo];
}

#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    NSString *memberID = deviceCode;
    BOOL isMember = ([MemberInfo isSignin] || [OfflineShelf isOnlyOfflineModeEnable]) && [DBManager isMember];
    if (isMember) {
        memberID = [DBManager selectMemberID];
    } else {
        memberID = @"";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    [api roomsDetail:@"LibraryDetail" RoomID:selectedLibraryID memberID:memberID];
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Flag:0];
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]) {        
        ///set Library Info to ViewController
        UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        libraryDetailVC = [mainSB instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = [notification.userInfo objectForKey:@"ROOMNAME"];
        libraryDetailVC.libraryID = [notification.userInfo objectForKey:@"ROOMID"];
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        /// load items in Library
        [self loadMediaItemsInLibrary];
    } else {
        [SVProgressHUD dismiss];
        [self showAlertViewController:nil Message:[Utility NSLocalizedString:@"no_detail"] Flag:0];
    }
    
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    //api/DeviceMedia/MediaItems?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&catId={catId}&subId={subId}&sort={sort}&loadIndex={loadIndex}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    [api mediaItems:@"MediaItemsInLibrary" RoomId:selectedLibraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo]) {
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Flag:1];
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = (NSArray*)notification.userInfo;
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
    } else {
        [self showAlertViewController:nil Message:[Utility NSLocalizedString:@"no_more_data"] Flag:1];
    }
}


#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)libraryID MediaID:(NSString*)mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        self.title = @"";
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];

    } else {
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:@"no_more_data"] Controller:(UIViewController*)self];
    }
}

-(void)showAlertViewController:(NSString*)title Message:(NSString*)message Flag:(int)flag{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //                                   if (flag == 0)
                                   //                                       [self loadLibraryInfo];
                                   //                                   else
                                   //                                       [self loadMediaItemsInLibrary];
                               }];
    
    [alert addAction:okButton];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
}

#pragma mark ---------- Notification from Left Menu ------------

-(void)userChangingLibrary:(NSNotification *)notification{
    [self.tabBarController setSelectedIndex:0];
}


-(void)watchHistory:(NSNotification*)notification{
    HistoryViewController *vc = [[HistoryViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)signOut:(NSNotification*)notification{
     [Utility showAlertControllerForSignout:self.view.window.rootViewController];
}

-(void)signIn:(NSNotification*)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SignInVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)setting:(NSNotification *)notification{
    SettingViewController *vc = [[SettingViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)openHelp:(NSNotification *)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    Help *vc = [mainSB instantiateViewControllerWithIdentifier:@"Help"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAbout:(NSNotification *)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    AboutTVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"AboutTVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)loadLibraryLogoImage{
    RoomData *room = [DB getRoomWithId:curUsedLibraryID];
    if (room) {
        roomName = room.name;
        [[RoomAPI shared] getDataWithUrlString:room.symbol complettion:^(NSData *data) {
            if (data && [Utility isImage:data]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *img = [Utility rescaleImage:[UIImage imageWithData:data] scaledToSize:navigationImageSize];
                    [barButtonLibrary setImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *img = [Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize];
                    [barButtonLibrary setImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                });
            }
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *img = [Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize];
            [barButtonLibrary setImage:img forState:UIControlStateNormal];
            [RoomProfileLogo setRoomProfileLogo:img];
        });
    }
}

-(void)memberProfile {
    MemberProfileViewController *vc = [[MemberProfileViewController alloc] initWithMemberId:[DBManager selectMemberID]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)redeemCode {
    RedeemViewController *vc = [[RedeemViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:YES completion:NULL];
}
@end

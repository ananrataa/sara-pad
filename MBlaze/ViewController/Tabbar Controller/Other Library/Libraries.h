//
//  Tabbar3.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaFileCache.h"
#import "ServiceAPI.h"
#import "LibraryDetailVC.h"

@interface Libraries :UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate>{
    //UIBarButtonItem *leftBarButton;
    UIButton *barButtonLibrary;
    //UIBarButtonItem *rightBarButton;
    CGSize navigationImageSize;
    
    NSUserDefaults *userDefault;
    NSString *deviceCode;
    NSString *curUsedLibraryID;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    UIBarButtonItem *sortBarButton;
    UIBarButtonItem *searchBarButton;
    
    UIView *sortView;
    
    NSMutableArray *arrayOfLibraries;
    //NSMutableDictionary *mediaCacheDic;
    
    //MediaFileCache *mCache;
    
    ServiceAPI *api;
    int loadDataIndex;
    
    NSString *selectedLibraryID;
    LibraryDetailVC *libraryDetailVC;
    
    BOOL isLoadingMore;
    BOOL loadMoreEnable;
    BOOL isNoMoreToLoad;
    BOOL isRefreshing;
    BOOL showRefreshing;
}
@end

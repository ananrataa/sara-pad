//
//  JournalCollectionViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 24/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class JournalCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    var curJN: Journal? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func refreshView() {
        if let jn = curJN {
            thumbnail.image = jn.image
            lblTitle.text = jn.title
        }
    }
}

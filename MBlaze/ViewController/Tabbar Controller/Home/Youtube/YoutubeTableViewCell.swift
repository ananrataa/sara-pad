//
//  YoutubeTableViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 24/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class YoutubeTableViewCell: UITableViewCell {
    let ytId = "yt_cell"
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @objc var items: [YT] = [] {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib.init(nibName: "YoutubeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ytId)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension YoutubeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ytId, for: indexPath) as! YoutubeCollectionViewCell
        
        cell.curYT = items[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.frame.height
        return CGSize(width: 200, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SVProgressHUD.show()
        let yt = items[indexPath.item]
        let vc = YoutubeViewController(yt: yt)
        let nav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController?.present(nav, animated: true, completion: nil)
    }
    
}

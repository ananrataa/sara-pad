//
//  YoutubeCollectionViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 24/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class YoutubeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    var curYT: YT? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func refreshView() {
        if let yt = curYT {
            thumbnail.image = yt.image
            lblTitle.text = yt.title
        }
    }
}

//
//  HomeTVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBSliderView.h"
#import "Room.h"
#import "MediaFileCache.h"
#import "LibraryDetailVC.h"
#import "ServiceAPI.h"

@interface HomeTVC : UITableViewController<SBSliderDelegate,UICollectionViewDataSource, UICollectionViewDelegate>{
    CGRect screenSize;
    float tabbar_height;
    
    NSUserDefaults *userDefault;
    
    ServiceAPI *api;
    Room *defaultRoom;
    

    SBSliderView *slider;
    
    UIButton *barButtonLibrary;
    
    
    NSMutableArray *arrayOfFilter;
    NSMutableArray *arrayOfMedia;
    NSMutableArray *arrayOfSlideAds;
    NSMutableArray *arrayOfCatAds;
    NSMutableArray *catAdsLists;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    float navigationHeight;
    CGSize navigationImageSize;
    
    NSString *curUsedLibraryID;
    
    UITabBarController *tabBarController;
    
    NSDictionary *memberInfo;
    
    BOOL isMember;
    
    NSString *deviceCode;
    NSString *mediaKind;
    
    int xScrollViewContentOffset;
    
    BOOL isFullAdsAlreadyShow;
    
    NSString *textForSearch;
    
    CGRect tableCGRect;
    
    float adsSlideHeight;
    
    float tableViewContentOffset;
    
    
    UIActivityIndicatorView *refreshActivityIndicator;
    BOOL isRefreshing;
    BOOL showRefreshing;
    
    LibraryDetailVC *libraryDetailVC;
    NSString *roomName;
}

@end

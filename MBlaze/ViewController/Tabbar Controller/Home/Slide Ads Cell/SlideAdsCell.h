//
//  SlideAdsCell.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideAdsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *adsView;


@end

//
//  HomeTVC.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "HomeTVC.h"
#import "Utility.h"
#import "AppDelegate.h"
//#import "SearchPreviewViewController.h"
//#import "SearchResultViewController.h"
#import "ConstantValues.h"

#import "TableViewCell.h"
#import "CollectionViewCell.h"

#import "MediaFileCache.h"

#import "CustomActivityIndicator.h"
#import "MediaListCollection.h"
#import "FilterList.h"

#import "Help.h"
#import "HistoryTVC.h"
#import "SignInVC.h"
#import "SplashScreen.h"

#import "Ads.h"
#import "ArrayOfAds.h"

#import "DBManager.h"
#import "HexColor.h"
#import "SlideAdsCell.h"

#import "SearchViewController.h"
#import "SettingTCV.h"

#import "OfflineShelf.h"
#import "RoomProfileLogo.h"

#import "AboutTVC.h"

#import "ThemeTemplate.h"

#import "UIImageView+WebCache.h"

#import "SARA_PAD-Swift.h"
#import "SVProgressHUD.h"
@import Lottie;

@interface HomeTVC () {
    NSArray *pdfArray;
}

@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@end

@implementation HomeTVC

#define headerHeight 40.f
#define IS_IPHONEX (([[UIScreen mainScreen] bounds].size.height-812)?NO:YES)

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD show];
    [self.tabBarController.tabBar setHidden:NO];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    
    if ([OfflineShelf isOnlyOfflineModeEnable]) {
        
        [self.tabBarController setSelectedIndex:2];
        self.tabBarController.tabBar.userInteractionEnabled = NO;
    } else {
        userDefault = [NSUserDefaults standardUserDefaults];
        deviceCode = [userDefault objectForKey:KeyDeviceCode];
        
        [self.tabBarController setSelectedIndex:[[userDefault objectForKey:TabbarIndex] intValue]];
        
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        
        
        
        //// select current Library id ////
        curUsedLibraryID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
        screenSize = [[UIScreen mainScreen] bounds];
        
        ///calculate collection Cell Width & Height
        collectionCellWidth = (([[UIScreen mainScreen] bounds].size.width)/[Utility calaulateCollectionItemInRow]);
        collectionCellHeight =  ((collectionCellWidth*4)/3)+90;
        
        navigationHeight = self.navigationController.navigationBar.frame.size.height;
        navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
        
        barButtonLibrary = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
        [barButtonLibrary setBackgroundImage:[Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
        [barButtonLibrary addTarget:self action:@selector(libraryImageTapping:) forControlEvents:UIControlEventTouchUpInside];
        barButtonLibrary.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        barButtonLibrary.clipsToBounds = YES;
        barButtonLibrary.layer.cornerRadius = navigationImageSize.height/2;

        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftTopBarTapping:)];
        [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
        UIBarButtonItem *libraryLogo = [[UIBarButtonItem alloc] initWithCustomView:barButtonLibrary];
        self.navigationItem.leftBarButtonItems = @[leftBarButton,libraryLogo];
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search-black"] style:UIBarButtonItemStyleDone target:self action:@selector(rightTopBarTapping:)];
        [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        
        arrayOfMedia = [[NSMutableArray alloc] init];
        arrayOfFilter = [[NSMutableArray alloc] init];
        arrayOfSlideAds = [[NSMutableArray alloc] init];
        arrayOfCatAds = [[NSMutableArray alloc] init];
        catAdsLists = [[NSMutableArray alloc] init];
        
        api = [[ServiceAPI alloc] init];
        
        mediaKind = @"####";
        
        tableViewContentOffset = 0;
        
        self.contentOffsetDictionary = [NSMutableDictionary dictionary];
        
        /// for Ads Slider //
        adsSlideHeight = (screenSize.size.width*9)/25;
        
        ///create Library Image Button
        [self  loadTopAds];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTheme];
    [self initTabbarController];
    [self setTitle:[Utility NSLocalizedString:@"TabbarHome"]];
    if(![OfflineShelf isOnlyOfflineModeEnable]){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangingLibrary:) name:@"ChangeLibrary" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchHistory:) name:@"WatchHistory" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signOut:) name:@"SignOut" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting:) name:@"Setting" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signIn:) name:@"Signin" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp:) name:@"Help" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAbout:) name:@"About" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redeemCode) name:@"Redeem" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memberProfile) name:@"MemberProfile" object:nil];
        /// check to change Library//
        [self prepare4ChangeLibrary];
        ///refresh IndicatorView
        [[self.view viewWithTag:908] removeFromSuperview];
        UIView *refreshIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
        refreshIndicatorView.frame = CGRectMake(0, -50, [[UIScreen mainScreen] bounds].size.width, 50);
        [refreshIndicatorView setBackgroundColor:[UIColor clearColor]];
        refreshIndicatorView.tag = 908;
        refreshActivityIndicator = [[refreshIndicatorView subviews] objectAtIndex:0];
        [refreshActivityIndicator setHidesWhenStopped:YES];
        [self.tableView addSubview:refreshIndicatorView];
    }
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberProfile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Redeem" object:nil];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

-(void)setTheme{
    UIFont *font = [UIFont fontWithName:mainFont size:21];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:font};
    
    [self.navigationItem.rightBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [self.navigationItem.leftBarButtonItem setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    
    //Tabbars
    [self.tabBarController.view setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    [[self.tabBarController tabBar] setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet6]]];
    [[self.tabBarController tabBar] setUnselectedItemTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet5]]];
    
    NSArray *itemTitles = @[@"TabbarHome", @"TabbarCategory", @"TabbarShelf", @"TabbarLibraries"];
    for (UITabBarItem *item in self.tabBarController.tabBar.items) {
        NSInteger n = [self.tabBarController.tabBar.items indexOfObject:item];
        [item setTitle:[Utility NSLocalizedString: itemTitles[n]]];
    }
    [self.tabBarController.tabBar.items[0] setTitle:[Utility NSLocalizedString:@"TabbarHome"]];
    [self.tabBarController.tabBar.items[1] setTitle:[Utility NSLocalizedString:@"TabbarCategory"]];
    [self.tabBarController.tabBar.items[2] setTitle:[Utility NSLocalizedString:@"TabbarShelf"]];
//    [self.tabBarController.tabBar.items[3] setTitle:[Utility NSLocalizedString:@"TabbarTutor"]];
    [self.tabBarController.tabBar.items[3] setTitle:[Utility NSLocalizedString:@"TabbarLibraries"]];
    //Logo
    [self loadLibraryLogoImage];
    [SVProgressHUD dismiss];
}

-(void)refreshLoadData{
    [SVProgressHUD show];
    self.view.userInteractionEnabled = NO;
    [self loadTopAds];
}

-(void)loadLibraryLogoImage{
    RoomData *room = [DB getRoomWithId:curUsedLibraryID];
    if (room) {
        roomName = room.name;
        [[RoomAPI shared] getDataWithUrlString:room.symbol complettion:^(NSData *data) {
            if (data && [Utility isImage:data]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *img = [Utility rescaleImage:[UIImage imageWithData:data] scaledToSize:navigationImageSize];
                    
                    [barButtonLibrary setBackgroundImage:img forState:UIControlStateNormal];
//                    [barButtonLibrary setImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *img = [Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize];
                    [barButtonLibrary setImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                });
            }
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *img = [Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize];
            [barButtonLibrary setImage:img forState:UIControlStateNormal];
            [RoomProfileLogo setRoomProfileLogo:img];
        });
    }
}

-(void)libraryImageTapping:(UIButton *)sender{
    [SVProgressHUD show];
    [self loadLibraryInfo];
}

-(void)libraryNameTapping:(UIButton *)sender{
    NSLog(@"----- show linbrary info. ----------");
}

#pragma mark --------- resize tabbarController -------
-(void)initTabbarController{
    [[self.tabBarController tabBar] setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet6]]];
    //bar backgroucd color
    [[self.tabBarController tabBar] setBarTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet7]]];
    
}

#pragma  mark ---------- get Top Ads ---------------
-(void)loadTopAds{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTopAdsResponse:) name:@"SlideAds" object:nil];
//    [api getAds:@"SlideAds" RoomId:curUsedLibraryID AdsType:2 IsSystemOwner:@"false"];
    [api getTypeAds:@"SlideAds" RoomId:curUsedLibraryID AdsType:@"Top" mediaId:@""];
}

-(void)getTopAdsResponse:(NSNotification*)notification{
    [arrayOfSlideAds removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SlideAds" object:nil];
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]) {
        NSMutableArray *arrSlideAds = [[NSMutableArray alloc] init];
        if ([[notification.userInfo valueForKey:@"ADS_TOP5"] isKindOfClass:[NSArray class]]) {
            NSArray *adsArray = [NSArray arrayWithArray:[notification.userInfo valueForKey:@"ADS_TOP5"]];
            if (adsArray.count > 0) {
                for (NSDictionary *obj in adsArray) {
                    Ads *ads = [[Ads alloc] init];
                    [ads AdsFullScreen:[Utility covertNsNullToNSString:[obj objectForKey:@"ADSID"]]
                               AdsName:[Utility covertNsNullToNSString:[obj objectForKey:@"ADSNAME"]]
                           AdsImageUrl:[Utility covertNsNullToNSString:[obj objectForKey:@"AdsImageUrl"]]
                            AdsLinkUrl:[Utility covertNsNullToNSString:[obj objectForKey:@"LNKURL"]]
                               AdsDESP:[Utility covertNsNullToNSString:[obj objectForKey:@"DESP"]]
                             AdsRoomID:[Utility covertNsNullToNSString:[obj objectForKey:@"ROOMID"]]];
                    
                    [arrSlideAds addObject:ads];
                }
            }
        }
        arrayOfSlideAds = arrSlideAds;
    }
    [ArrayOfAds setArrayFullScreenAds:nil];
    [ArrayOfAds setArraySlideAds:arrayOfSlideAds];
    [self loadFilterList];
    
}

#pragma  mark ---------- get Cat Ads ---------------
-(void)loadCatAds {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCatAdsResponse:) name:@"getCatAdsResponse" object:nil];
    NSString *roomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    [api getTypeAds:@"getCatAdsResponse" RoomId:roomID AdsType:@"Horizontal" mediaId: @""];
}
-(void)loadCatAdsResponse:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getCatAdsResponse" object:nil];
    if ([Utility isServiceResponseError:notification.userInfo]) {
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///Open Area3 Ads
        if ([[notification.userInfo objectForKey:@"ADS_CATAGORIE"] isKindOfClass:[NSArray class]]) {
            NSArray *json = [NSArray arrayWithArray:[notification.userInfo objectForKey:@"ADS_CATAGORIE"]];
            if (json) {
                for (NSDictionary *js in json) {
                    NSString *path = [js valueForKey:@"AdsImageUrl"];
                    UIImage *img = [[SP shared] loadImageWith:path];
                    NSString *adsLink = [js valueForKey:@"LNKURL"];
                    NSString *adName = [js valueForKey:@"ADSNAME"];
                    if (img && adsLink) {
                        [arrayOfCatAds addObject:[[ADImage alloc] initWithImage:img adLink:adsLink name:adName]];
                    }
                }
            }
        }
    } else {
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
    
    
    //Arrange CatAds
    int n = (int)arrayOfMedia.count;
    catAdsLists = [NSMutableArray arrayWithArray:arrayOfMedia];
    int count = 0;
    if (arrayOfCatAds.count > 0) {
        NSMutableArray *tmp = [NSMutableArray arrayWithArray:arrayOfCatAds];
        for (int i = 1; i <= n; i++) {
            if ((i % 2) == 0) {
                ADImage *ad = (ADImage*)tmp.firstObject;
                [catAdsLists insertObject:ad atIndex:i+count];
                count += 1;
                [tmp removeObjectAtIndex:0];
                if (tmp.count == 0) {
                    tmp = [NSMutableArray arrayWithArray:arrayOfCatAds];
                }
            }
        }
        if (catAdsLists.count > 0) {
            if (![[catAdsLists lastObject] isKindOfClass:[ADImage class]]) {
                if (tmp.count > 0) {
                    [catAdsLists addObject:[tmp firstObject]];
                } else {
                    [catAdsLists addObject:[arrayOfCatAds firstObject]];
                }
            }
        }
    } else {
        [SVProgressHUD dismiss];
    }
    [self.tableView reloadData];
}

-(void)userTapAds:(UITapGestureRecognizer*)sender {
    if ([InternetConnection isConnectedToNetwork]) {
        ADImage *ad = [catAdsLists objectAtIndex:sender.view.tag];
        if ([ad isKindOfClass:[ADImage class]] && ad.adLink.length > 0) {
            WebOSViewController *vc = [[WebOSViewController alloc] initWithUrlPath:ad.adLink title: ad.adName];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:NULL];
        }
    }
}

- (void)sbslider:(SBSliderDelegate *)sbslider URL:(NSString*)url; {
    if (url.length > 0) {
        WebOSViewController *vc = [[WebOSViewController alloc] initWithUrlPath:url title:@""];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:NULL];
    }
}

#pragma mark ----------- Filter List --------------
-(void)loadFilterList{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterListResponse:) name:@"FilterList" object:nil];
    NSString *deviceLaguage = [[userDefault objectForKey:AppLanguageKey] uppercaseString];
    
    [api mediaKindOfRoom:@"FilterList" RoomId:curUsedLibraryID Language:deviceLaguage CategoryID:@"" SubCatrgory:@"" AppVersion:CurrentAppVersion Type:[KindOfRoomTypeDefault intValue] Value:@""];
  
}

-(void)filterListResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FilterList" object:nil];
    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        [arrayOfFilter removeAllObjects];
        for (NSDictionary *obj in notification.userInfo) {
            /// set default media kind ////
            if ([mediaKind isEqualToString:@"####"] && [[obj objectForKey:@"KINDCODE"] isEqualToString:@""]) {
                mediaKind = [Utility covertNsNullToNSString:[obj objectForKey:@"KINDCODE"]];
            }
            
            [arrayOfFilter addObject:@[[Utility covertNsNullToNSString:[obj objectForKey:@"KINDCODE"]],[Utility covertNsNullToNSString:[obj objectForKey:@"KINDNAME"]]]];
        }
    }
    
    ///// set Filter ///
    [FilterList FilterList:arrayOfFilter];
    
    [self loadCategorieMediaItemPreview];
}

#pragma mark ------------ CategorieMediaItemPreview -------------
-(void)loadCategorieMediaItemPreview{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorieMediaItemResponse:) name:@"MediaItemPreview" object:nil];
    
    [api categorieMediaItemPreview:@"MediaItemPreview" RoomId:curUsedLibraryID MediaKind:mediaKind AppVersion:CurrentAppVersion];
}
-(void)categorieMediaItemResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemPreview" object:nil];
    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        [arrayOfMedia removeAllObjects];
        for (NSDictionary *catagoryObj in notification.userInfo) {
            NSMutableDictionary *mulDic = [[NSMutableDictionary alloc] init];
            [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"CATEGORIESNAME"]]
                      forKey:@"CATEGORIESNAME"];
            [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"CATEGORIESID"]]
                      forKey:@"CATEGORIESID"];
            [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"CATEGORIESCODE"]]
                      forKey:@"CATEGORIESCODE"];
            [mulDic setValue:[Utility covertNsNullToNSString:[catagoryObj objectForKey:@"MessaageText"]]
                      forKey:@"MessaageText"];
            
            NSMutableArray *arrayOfMediaItem = [[NSMutableArray alloc] init];
            if ([[catagoryObj objectForKey:@"MediaItems"] isKindOfClass:[NSArray class]]) {
                for (NSDictionary *mediaItem in [catagoryObj objectForKey:@"MediaItems"]) {
                    NSMutableDictionary *mulDicItem = [[NSMutableDictionary alloc] init];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                                  forKey:@"MEDIAID"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]] forKey:@"MEDIANAME"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                                  forKey:@"AUTHOR"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                                  forKey:@"POPULARVOTE"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                                  forKey:@"MediaCoverUrl"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                                  forKey:@"MEDIAKIND"];
                    [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"ROOMID"]]
                                  forKey:@"ROOMID"];
                    [arrayOfMediaItem addObject:mulDicItem];
                }
            }
            [mulDic setValue:arrayOfMediaItem forKey:@"MediaItems"];
            
            [arrayOfMedia addObject:mulDic];
        }
                
        //Load CatAds
        [self loadCatAds];
    }
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.view.userInteractionEnabled = YES;
    isRefreshing = NO;
    
    tableCGRect = self.tableView.frame;
    [SVProgressHUD dismiss];
    
    //post notification to check user tap notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserTapNotification" object:nil];
}


#pragma mark ------- Tapping Menu --------------
-(void)leftTopBarTapping:(id)sender{
    //NSLog(@"------------- leftTopBarTapping --------------");
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark ---------- Tapping Search ----------
-(void)rightTopBarTapping:(id)sender{
    //NSLog(@"------------- rightBarButton --------------");
    
    if (@available(iOS 11.0, *)) {
        MediaSearchViewController *vc = [[MediaSearchViewController alloc] initWithRoomId:curUsedLibraryID];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    } else {
        UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        SearchViewController *searchVC = [mainSB instantiateViewControllerWithIdentifier:@"SearchViewController_"];
        searchVC.libraryID = curUsedLibraryID;
        searchVC.categoryID = @"";
        searchVC.subCategoryID = @"";
        
        UINavigationController *nvg = [[UINavigationController alloc] initWithRootViewController:searchVC];
        
        [self presentViewController:nvg animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return catAdsLists.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellIdentifier";
    
    if (indexPath.section==0) {
        [tableView registerNib:[UINib nibWithNibName:@"SlideAdsCell" bundle:nil] forCellReuseIdentifier:@"slideAdsCell"];
        SlideAdsCell *cell = (SlideAdsCell*)[tableView dequeueReusableCellWithIdentifier:@"slideAdsCell" forIndexPath:indexPath];
        UIView *v = [self.view viewWithTag:333];
        if (v) {
            [v removeFromSuperview];
        }
        if ([arrayOfSlideAds count] > 0) {
            slider = [[[NSBundle mainBundle] loadNibNamed:@"SBSliderView" owner:self options:nil] firstObject];
            slider.tag = 333;
            slider.delegate = self;
            [self.view addSubview:slider];
            [slider createSliderWithImages:arrayOfSlideAds WithAutoScroll:true inView:cell.adsView ];
            slider.frame = cell.adsView .frame;
            cell.adsView.backgroundColor = [UIColor clearColor];
        } else {
            cell.adsView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default_ads"]];
        }
        return cell;
    } else {
        TableViewCell *cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        ADImage *adCat = (ADImage*) [catAdsLists objectAtIndex:indexPath.row];
        
        if ([adCat isKindOfClass:[ADImage class]]) {
            CGRect rect = CGRectMake(0, 0, tableView.frame.size.width, collectionCellHeight);
            UIImageView * imgView = [[UIImageView alloc] initWithFrame:rect];
            [imgView setUserInteractionEnabled:YES];
            [imgView setImage:adCat.adImage];
            [imgView setContentMode:UIViewContentModeScaleAspectFit];
            [[imgView layer] setBorderColor:[HexColor SKColorHexString:@"#dedede"].CGColor];
            [[imgView layer] setBorderWidth:0.35];
            UITapGestureRecognizer *imgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapAds:)];
            [imgView addGestureRecognizer:imgTap];
            imgView.tag = indexPath.row;
            [cell.contentView addSubview:imgView];
        } else {
            UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CollectionHeaderView" owner:self options:nil] objectAtIndex:0];
            headerView.backgroundColor = [UIColor whiteColor];
            headerView.frame = CGRectMake(0.0f, 0.0f, [[UIScreen mainScreen] bounds].size.width, headerHeight);
            NSDictionary *header = [catAdsLists objectAtIndex:indexPath.row];
            CGSize stringHeaderNameWidth = [Utility getWidthOfNSString:[header objectForKey:@"CATEGORIESNAME"] FontSize:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:[Utility fontSize:17.f]]]];
            UILabel *headerName = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, stringHeaderNameWidth.width, 30)];
            headerName.tag = 101;
            headerName.text = [header objectForKey:@"CATEGORIESNAME"];
            headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:[Utility fontSize:17.f]]];
            UIView *lotView;
            if ([headerName.text isEqualToString:@"NEW"]) {
                headerName.text = @"";// [NSMutableString stringWithString: @" NEW "];
                headerName.backgroundColor = [UIColor redColor];
                headerName.textColor = [UIColor yellowColor];
                headerName.clipsToBounds = YES;
                [headerName sizeToFit];
                headerName.layer.cornerRadius = 5.f;
                //add Lottie annimation
                lotView = [[UIView alloc] initWithFrame:CGRectMake(-5, -5, 50, 50)];
                lotView.backgroundColor = [UIColor clearColor];
            }
            UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
            [seeMoreBtn setTitle:[Utility NSLocalizedString:@"SeeMore"] forState:UIControlStateNormal];
            seeMoreBtn.titleLabel.font = [UIFont fontWithName:mainFont size:14.0f];
            [seeMoreBtn setTintColor:[UIColor blackColor]];
            seeMoreBtn.accessibilityLabel = [header objectForKey:@"CATEGORIESID"]; //**//
            seeMoreBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            seeMoreBtn.clipsToBounds = YES;
            seeMoreBtn.layer.cornerRadius = 5.f;
            seeMoreBtn.layer.borderWidth = 0.75f;
            seeMoreBtn.layer.borderColor = [UIColor grayColor].CGColor;
            seeMoreBtn.tag = indexPath.row;
            [seeMoreBtn addTarget:self action:@selector(seeMoreTapping:) forControlEvents:UIControlEventTouchUpInside];
            
            UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 0, headerView.frame.size.width-(10+seeMoreBtn.frame.size.width+10+10), 55)];
            [scrollView setShowsHorizontalScrollIndicator:NO];
            [scrollView setShowsVerticalScrollIndicator:NO];
            [scrollView setAlwaysBounceVertical:NO];
            [scrollView addSubview:headerName];
            if (lotView) {
                LOTAnimationView *animationView = [LOTAnimationView animationNamed:@"new_button"];
                animationView.frame = lotView.bounds;
                animationView.clipsToBounds = YES;
                animationView.contentMode = UIViewContentModeScaleAspectFit;
                [animationView setLoopAnimation:YES];
                [lotView addSubview:animationView];
                [scrollView addSubview:lotView];
                [animationView play];
            }
            scrollView.contentSize = headerName.frame.size;
            
            [headerView addSubview:scrollView];
            
            [cell.contentView addSubview:headerView];
            
            /////change collectionView Layout
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            layout.sectionInset = UIEdgeInsetsMake(headerHeight + 5,8,0,8);
            layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
            layout.minimumLineSpacing = 5;
            layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            [cell.collectionView setCollectionViewLayout:layout];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell_ forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 ) {
        if ([[catAdsLists objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            TableViewCell *cell = (TableViewCell*)cell_;
            [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
            NSInteger index = cell.collectionView.indexPath.row;
            cell.collectionView.tag = indexPath.row;
            
            CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
            [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
        } else if ([[catAdsLists objectAtIndex:indexPath.row] isKindOfClass:[ADImage class]]) {
            cell_.alpha = 0;
            CATransform3D transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0);
            cell_.layer.transform = transform;
            [UIView animateWithDuration:1 animations:^{
                cell_.alpha = 1;
                cell_.layer.transform = CATransform3DIdentity;
            }];
        }
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }
    else{
        
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"UIHeaderView" owner:self options:nil] objectAtIndex:0];
        [headerView setBackgroundColor:[UIColor whiteColor]];
        headerView.tag = 809;
        
        UIScrollView *scrollView = (UIScrollView*)[headerView.subviews firstObject];
        scrollView.backgroundColor = [UIColor clearColor];
        
        float margin = 0.0f;
        float btn_width = 150.0f;
        float btn_height = 30.0f;
        
        for (NSArray *filter in arrayOfFilter){
            //NSLog(@"filter : %@ || %@",filter[0],filter[1]);
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(margin, 0.0f, btn_width, btn_height)];
            [btn setTintColor:[UIColor whiteColor]];
            [btn setTitle:filter[1] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:17.f]];
            btn.backgroundColor  = [UIColor lightGrayColor];
            btn.accessibilityLabel = filter[0];
            btn.tag = ((int)margin/3);
            [btn addTarget:self action:@selector(filterMediaTapping:) forControlEvents:UIControlEventTouchUpInside];
            
            btn.clipsToBounds = YES;
            btn.layer.cornerRadius = 15.0f;
            
            if ([filter[0] isEqualToString:mediaKind]){
                btn.backgroundColor  = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
                [btn setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
                xScrollViewContentOffset = margin;
            }
            
            [scrollView addSubview:btn];
            
            margin = margin + 5.0f + btn_width;
            
            //add icon
            CGFloat s = 22;
            CGFloat sp = (btn_height-s)/2;
            UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(btn_width - sp - s, sp, s, s)];
            NSString *mediaType = (NSString*)filter[0];
            if ([mediaType isEqualToString:@"A"]) {
                iconImageView.image = [UIImage imageNamed:@"iconaudio"];
            } else if ([mediaType isEqualToString:@"D"]) {
                iconImageView.image = [UIImage imageNamed:@"iconbook"];
            } else if ([mediaType isEqualToString:@"M"]) {
                iconImageView.image = [UIImage imageNamed:@"iconmagazine"];
            } else if ([mediaType isEqualToString:@"YU"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_youtube"];
            } else if ([mediaType isEqualToString:@"V"]) {
                iconImageView.image = [UIImage imageNamed:@"iconvideo"];
            } else if ([mediaType isEqualToString:@"E"]) {
                iconImageView.image = [UIImage imageNamed:@"iconepub"];
            } else if ([mediaType isEqualToString:@"T"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_material"];
            } else if ([mediaType isEqualToString:@"EP"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_pdf"];
            } else if ([mediaType isEqualToString:@"ES"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_sound"];
            } else if ([mediaType isEqualToString:@"EL"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_link"];
            } else if ([mediaType isEqualToString:@"EV"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_video"];
            } else if ([mediaType isEqualToString:@"CR"]) {
                iconImageView.image = [UIImage imageNamed:@"icon_tutor"];
            }
            
            [iconImageView setContentMode:UIViewContentModeScaleAspectFit];
            [btn addSubview:iconImageView];
            
        }
        scrollView.frame = CGRectMake(0.0f, 0.0f, headerView.frame.size.width, btn_height);
        [scrollView setContentSize:CGSizeMake(margin, btn_height)];
        
        scrollView.contentOffset = CGPointMake(xScrollViewContentOffset,0);
        
        [headerView addSubview:scrollView];
        
        return headerView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        return adsSlideHeight;
    } else {
        if ([[catAdsLists objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            return collectionCellHeight + headerHeight + 20;
        } else {
            return collectionCellHeight;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 0;
    } else {
        return 50.0; //// margin top & btm >> 10+10 , header height >> 30'
    }
}




#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    NSDictionary *categoryDic = [catAdsLists objectAtIndex:collectionView.tag];
    return [(NSMutableArray*)[categoryDic objectForKey:@"MediaItems"] count];
    
    //return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *categoryDic = [catAdsLists objectAtIndex:collectionView.tag];
    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
    
    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"default_media_cover.png"]];
    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
    
    NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
//    [cell.mediaCoverImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
    
    if ([[[itemDic objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"]) {
        [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]  options:0 progress:nil completed:nil];
    } else {
        [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]  options:0 progress:nil completed:nil];
    }
  
    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
    [Utility setFont:cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    
    [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
    
    cell.videoImagePic.hidden = YES;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [SVProgressHUD show];
    //// load data ////
    NSDictionary *categoryDic = [catAdsLists objectAtIndex:collectionView.tag];
    NSMutableArray *arrayOfItems = (NSMutableArray*)[categoryDic objectForKey:@"MediaItems"];
    NSDictionary *itemDic = [arrayOfItems objectAtIndex:indexPath.row];
    
    [self loadMediaDetail:[itemDic objectForKey:@"ROOMID"] MediaID:[itemDic objectForKey:@"MEDIAID"]];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y <= -[Utility bounceHeightForLoadMore] && !isRefreshing){
        //NSLog(@"contentOffset.y  : %f",scrollView.contentOffset.y);
        [refreshActivityIndicator startAnimating];
        //[self.tableView setScrollEnabled:NO];
        isRefreshing = YES;
        showRefreshing = NO;
        //[self.tableView setScrollsToTop:YES];
        //[self.tableView setContentOffset:CGPointMake(0, -50) animated:YES];
        //[self refreshLoadData];
    }
    
    if (scrollView.contentOffset.y >= -50 && isRefreshing && !showRefreshing) {
        showRefreshing = YES;
        [self.tableView setContentOffset:CGPointMake(0, -50) animated:YES];
        [self refreshLoadData];
    }
    
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    AFIndexedCollectionView *collectionView = (AFIndexedCollectionView *)scrollView;
    NSInteger index = collectionView.indexPath.row;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
}


#pragma mark ------- filter Media --------
-(void)filterMediaTapping:(UIButton*)sender{
    if(!([mediaKind isEqualToString:sender.accessibilityLabel])){
        [SVProgressHUD show];
        UIView *headerView = [self.tableView viewWithTag:809];
        UIScrollView *scrollView = (UIScrollView*)[headerView.subviews firstObject];
        for (UIButton *btn in scrollView.subviews) {
            btn.backgroundColor  = [UIColor lightGrayColor];
            [btn setTintColor:[UIColor whiteColor]];
        }
        mediaKind = sender.accessibilityLabel;
        [self loadCategorieMediaItemPreview];
    }
    
}


#pragma mark ---------- See more Delegate ----------
-(void)seeMoreTapping:(UIButton*)sender{
    [SVProgressHUD show];
    //// load data /////
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    [self loadMoreMediaItemsInCategory:sender.accessibilityLabel];
}


#pragma mark --------- See More Medai in Category ------
-(void)loadMoreMediaItemsInCategory:(NSString *)categoryID{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMoreMediaItemsResponse:) name:@"MoreMediaItemPreview" object:nil];
    
    [api mediaItems:@"MoreMediaItemPreview" RoomId:curUsedLibraryID CategoryID:categoryID SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}

-(void)loadMoreMediaItemsResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MoreMediaItemPreview" object:nil];
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *header = [catAdsLists objectAtIndex:selectedIndexPath.row];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MediaListCollection *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaListCollection"];
        vc.usingLibraryID = curUsedLibraryID;
        vc.navigationTitle = [header objectForKey:@"CATEGORIESNAME"];
        vc.categoryID = [header objectForKey:@"CATEGORIESID"];
        vc.subCategoryID = @"";
        vc.jsonData = (NSArray*) notification.userInfo;
        vc.kindOfRoomKeyValue = @[KindOfRoomTypeDefault,@""];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
}


#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)libraryID MediaID:(NSString*)mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    if ([userLimitData count] < 2) {
        userLimitData = @[[NSString stringWithFormat:@"%i",MaximunDateForBorrow],
                          [NSString stringWithFormat:@"%i",MaximunFilesDownload]];
    }
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}

#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    NSString *memberID = deviceCode;
    BOOL isMember = ([MemberInfo isSignin] || [OfflineShelf isOnlyOfflineModeEnable]) && [DBManager isMember];
    if (isMember)
        memberID = [DBManager selectMemberID];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    [api roomsDetail:@"LibraryDetail" RoomID:curUsedLibraryID memberID:memberID];
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    //NSLog(@"libraryDetailResponse : %@",notification.userInfo);
    
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        ///set Library Info to ViewController
        UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        libraryDetailVC = [mainSB instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = roomName;
        libraryDetailVC.libraryID = curUsedLibraryID;
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        
        /// load items in Library
        [self loadMediaItemsInLibrary];
    }
    else{
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    [api mediaItems:@"MediaItemsInLibrary" RoomId:curUsedLibraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = (NSArray*) notification.userInfo;
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
}


#pragma mark ---------- Notification from Left Menu ------------

-(void)userChangingLibrary:(NSNotification *)notification{
    [arrayOfCatAds removeAllObjects];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryAdsResponse:) name:@"getTypeAdsResponse" object:nil];
    NSString *roomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    [api getTypeAds:@"getTypeAdsResponse" RoomId:roomID AdsType:@"Main"  mediaId: @""];
}

-(void)loadLibraryAdsResponse:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getTypeAdsResponse" object:nil];
    if ([Utility isServiceResponseError:notification.userInfo]) {
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    } else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///Open Area1 Ads
        NSArray *json = [notification.userInfo objectForKey:@"FULLSCEEN"];
        if ([json isKindOfClass:[NSArray class]]) {
            Area1ViewController *vc = [[Area1ViewController alloc] initWithJson:json];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:^{
                [self prepare4ChangeLibrary];
            }];
        } else {
            [self prepare4ChangeLibrary];
        }
    } else {
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
}

-(void)watchHistory:(NSNotification*)notification{
//    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
//    HistoryTVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"HistoryTVC"];
    HistoryViewController *vc = [[HistoryViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
//    [self.navigationController pushViewController:vc animated:YES];
}

-(void)signOut:(NSNotification*)notification{
    [Utility showAlertControllerForSignout:self.view.window.rootViewController];
}

-(void)signIn:(NSNotification*)notification{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SignInVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)setting:(NSNotification *)notification{
    SettingViewController *vc = [[SettingViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)openHelp:(NSNotification *)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    Help *vc = [mainSB instantiateViewControllerWithIdentifier:@"Help"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAbout:(NSNotification *)notification {
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    AboutTVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"AboutTVC"];
     [self.navigationController pushViewController:vc animated:YES];
}

-(void)prepare4ChangeLibrary{
    NSString *roomID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    
    if (![curUsedLibraryID isEqualToString:roomID]) {
        [SVProgressHUD show];
        
        mediaKind = @"####";
        
        ///get new library id
        curUsedLibraryID = roomID;
        
        [arrayOfMedia removeAllObjects];
        [arrayOfFilter removeAllObjects];
        [arrayOfSlideAds removeAllObjects];
        
        isFullAdsAlreadyShow = NO;
        [self setTheme];
        [self loadTopAds];
    } else {
        [SVProgressHUD dismiss];
    }
}

-(void)memberProfile {
    MemberProfileViewController *vc = [[MemberProfileViewController alloc] initWithMemberId:[DBManager selectMemberID]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)redeemCode {
    RedeemViewController *vc = [[RedeemViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:YES completion:NULL];
}

@end

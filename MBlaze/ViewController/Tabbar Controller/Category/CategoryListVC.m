//
//  Tabbar1.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "CategoryListVC.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "ConstantValues.h"
#import "MediaListCollection.h"
#import "HistoryTVC.h"
#import "SignInVC.h"
#import "HexColor.h"
#import "SearchViewController.h"
#import "SettingTCV.h"

#import "Help.h"
#import "DBManager.h"
#import "RoomProfileLogo.h"
#import "AboutTVC.h"
#import "ThemeTemplate.h"
#import "SARA_PAD-Swift.h"

//#import "SearchPreviewViewController.h"
//#import "SearchResultViewController.h"

@interface CategoryListVC ()

@end

@implementation CategoryListVC

#define cellIdentifier @"categoryCell"

#define subCategoryButtonHeight 40.f
#define categoryButtonHeight 50.f

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"TabbarCategory"]];
    
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};

    userDefault = [NSUserDefaults standardUserDefaults];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    barButtonLibrary = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
    [barButtonLibrary setBackgroundImage:[Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
    [barButtonLibrary addTarget:self action:@selector(libraryImageTapping:) forControlEvents:UIControlEventTouchUpInside];
    barButtonLibrary.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    barButtonLibrary.clipsToBounds = YES;
    barButtonLibrary.layer.cornerRadius = navigationImageSize.height/2;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    UIBarButtonItem *libraryLogo = [[UIBarButtonItem alloc] initWithCustomView:barButtonLibrary];
    self.navigationItem.leftBarButtonItems = @[leftBarButton,libraryLogo];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search-black"] style:UIBarButtonItemStyleDone target:self action:@selector(searchTopBarTapping:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    curUsedLibraryID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    api = [[ServiceAPI alloc] init];
    
    arrayOfCategory = [[NSMutableArray alloc] init];
    
    [SVProgressHUD show];
    
    [self loadLibraryLogoImage];
    
    loadIndex = 1; //Default value is 1
    [self loadGategoryList];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    ///load new Category List when Change Library
    //NSLog(@"Log : %@",[Utility readLogFile]);
    if (!([[userDefault objectForKey:KeyCuurrentUsedLribaryID] isEqualToString:curUsedLibraryID])) {
        [self viewDidLoad];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangingLibrary:) name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchHistory:) name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signOut:) name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting:) name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp:) name:@"Help" object:nil];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePassword:) name:@"ChangePassword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signIn:) name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAbout:) name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memberProfile) name:@"MemberProfile" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redeemCode) name:@"Redeem" object:nil];
    
    ///refresh IndicatorView
    [[self.view viewWithTag:908] removeFromSuperview];
    UIView *refreshIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
    refreshIndicatorView.frame = CGRectMake(0, -50, [[UIScreen mainScreen] bounds].size.width, 50);
    [refreshIndicatorView setBackgroundColor:[UIColor clearColor]];
    refreshIndicatorView.tag = 908;
    refreshActivityIndicator = [[refreshIndicatorView subviews] objectAtIndex:0];
    [refreshActivityIndicator setHidesWhenStopped:YES];
    [self.tableView addSubview:refreshIndicatorView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchMediaRespones:) name:@"SearchMedia" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberProfile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Redeem" object:nil];
}

-(void)leftTopBarTapping:(id)sender{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)libraryImageTapping:(UIButton *)sender{
    [SVProgressHUD show];
    [self loadLibraryInfo];
}

-(void)searchTopBarTapping:(id)sender{
    if (@available(iOS 11.0, *)) {
        MediaSearchViewController *vc = [[MediaSearchViewController alloc] initWithRoomId:curUsedLibraryID];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    } else {
        UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        SearchViewController *searchVC = [mainSB instantiateViewControllerWithIdentifier:@"SearchViewController_"];
        searchVC.libraryID = curUsedLibraryID;
        searchVC.categoryID = @"";
        searchVC.subCategoryID = @"";
        
        UINavigationController *nvg = [[UINavigationController alloc] initWithRootViewController:searchVC];
        
        [self presentViewController:nvg animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ------------- load Gategory List ------------
-(void)loadGategoryList{
    //api/DeviceMedia/Categorie?deviceCode={deviceCode}&roomId={roomId}&loadIndex={loadIndex}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categoryResponse:) name:@"CategoryList" object:nil];
    [api categoryList:@"CategoryList" RoomID:curUsedLibraryID LoadIndex:loadIndex];
}

-(void)categoryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CategoryList" object:nil];
    //NSLog(@"categoryResponse : %@",[notification.userInfo description]);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo]) {
        [self showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"]];
        
    } else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]) {
        int i =0;
        [arrayOfCategory removeAllObjects];
        (subCategoryShowHideDic == nil)?subCategoryShowHideDic = [[NSMutableDictionary alloc] init]:[subCategoryShowHideDic removeAllObjects];;
        if ([notification.userInfo isKindOfClass:[NSArray class]]) {
            for (NSDictionary *catagoryObj in notification.userInfo) {
                NSMutableDictionary *mulDic = [[NSMutableDictionary alloc] init];
                //[mulDic setValue:[catagoryObj objectForKey:@"CATEGORIESCODE"] forKey:@"CATEGORIESCODE"];
                [mulDic setValue:[catagoryObj objectForKey:@"CATEGORIESID"] forKey:@"CATEGORIESID"];
                [mulDic setValue:[catagoryObj objectForKey:@"CATEGORIESNAME"] forKey:@"CATEGORIESNAME"];
                //[mulDic setValue:[catagoryObj objectForKey:@"MessaageText"] forKey:@"MessaageText"];
                
                //NSLog(@"-------------------------------------");
                //NSLog(@"cat name : %@",[catagoryObj objectForKey:@"CATEGORIESNAME"]);
                
                NSMutableArray *subCateArray = [[NSMutableArray alloc] init];
                for (NSDictionary *subCatagoryObj in [catagoryObj objectForKey:@"DeviceSubCategories"]) {
                    [subCateArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[subCatagoryObj objectForKey:@"SUBCATEGORIESID"],@"SUBCATEGORIESID",[subCatagoryObj objectForKey:@"SUBCATEGORIESNAME"],@"SUBCATEGORIESNAME", nil]];
                    
                    //NSLog(@"sub name : %@",[subCatagoryObj objectForKey:@"SUBCATEGORIESNAME"]);
                }
                
                [mulDic setValue:subCateArray forKey:@"SUBCATEGORIES"];
                
                [arrayOfCategory addObject:mulDic];
                [subCategoryShowHideDic setValue:@"H" forKey:[NSString stringWithFormat:@"%i",i]];
                i++;
            }
        }
        [self.tableView reloadData];
        
        float loadMoreIndicatorYpoint = self.tableView.contentSize.height>=self.tableView.frame.size.height? self.tableView.contentSize.height:self.tableView.frame.size.height;
        [[self.view viewWithTag:909] removeFromSuperview];
        ///load more IndicatorView
        UIView *loadMoreIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
        loadMoreIndicatorView.frame = CGRectMake(0, loadMoreIndicatorYpoint, [[UIScreen mainScreen] bounds].size.width, 50);
        [loadMoreIndicatorView setBackgroundColor:[UIColor clearColor]];
        loadMoreIndicatorView.tag = 909;
        loadMoreActivityIndicator = nil;
        loadMoreActivityIndicator = [[loadMoreIndicatorView subviews] objectAtIndex:0];
        [loadMoreActivityIndicator setHidesWhenStopped:YES];
        [self.tableView addSubview:loadMoreIndicatorView];
    } else {
        [self showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError]];
    }
    
    if(loadMoreActivityIndicator.isAnimating)[loadMoreActivityIndicator stopAnimating];
    if(refreshActivityIndicator.isAnimating)[refreshActivityIndicator stopAnimating];
    
    if (isRefreshing) {
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else if (isLoadingMore) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height) animated:YES];
    }
    
    isLoadingMore = NO;
    isRefreshing = NO;
    self.view.userInteractionEnabled = YES;
    // tableCGRect = self.tableView.frame;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [arrayOfCategory count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int subCatecount = (int)[[[arrayOfCategory objectAtIndex:section] objectForKey:@"SUBCATEGORIES"] count];
    if (subCatecount > 0)
        return 1;
    else
        return 0.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(cell != nil){
        
        [[cell viewWithTag:101] removeFromSuperview];
        
        UIView *mView = [[UIView alloc] initWithFrame:CGRectMake(15.f, 0.f, cell.frame.size.width-30.f, cell.frame.size.height)];
        mView.clipsToBounds = YES;
        mView.layer.cornerRadius = 10.f;
        mView.layer.borderWidth = 1.5f;
        mView.layer.borderColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]].CGColor;
        mView.backgroundColor = [UIColor whiteColor];
        mView.tag = 101;
        
        int subCatecount = (int)[[[arrayOfCategory objectAtIndex:indexPath.section] objectForKey:@"SUBCATEGORIES"] count];
        
        float subCategoryWidth = mView.frame.size.width - (8.f + 8.f);
        float marginY = 0.f;
        
        for (int i = 0; i<subCatecount; i++) {
            NSDictionary *subCateDic = [[[arrayOfCategory objectAtIndex:indexPath.section] objectForKey:@"SUBCATEGORIES"] objectAtIndex:i];
            
            //NSLog(@"subCateDic : %@",[subCateDic description]);
            
            marginY += 8.f;
            
            UIButton *subCateBtn = [[UIButton alloc] initWithFrame:CGRectMake(8.f, marginY, subCategoryWidth, subCategoryButtonHeight)];
            [subCateBtn addTarget:self action:@selector(subCategoryTapping:) forControlEvents:UIControlEventTouchUpInside];
            
            //CATEGORIESID
            subCateBtn.accessibilityValue = [[arrayOfCategory objectAtIndex:indexPath.section] objectForKey:@"CATEGORIESID"];
            //SUBCATEGORIESID
            subCateBtn.accessibilityLabel = [subCateDic objectForKey:@"SUBCATEGORIESID"];
            
            subCateBtn.backgroundColor = [UIColor whiteColor];
            subCateBtn.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:15.f]];
            subCateBtn.titleLabel.numberOfLines = 1;
            subCateBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [subCateBtn setTitle:[subCateDic objectForKey:@"SUBCATEGORIESNAME"] forState:UIControlStateNormal];
            [subCateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [subCateBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            
            marginY += subCategoryButtonHeight;
            
            [mView addSubview:subCateBtn];
            
            if (i<subCatecount-1){
                UIView *seperateLine = [[UIView alloc] initWithFrame:CGRectMake(30.f, marginY+4.f, mView.frame.size.width-60.f, 1.f)];
                seperateLine.backgroundColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
                [mView addSubview:seperateLine];
            }
        }
        
        [cell addSubview:mView];
        //cell.backgroundColor = [UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *mView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, tableView.frame.size.width, 70.f)];
    mView.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *catagoryL = [arrayOfCategory objectAtIndex:(int)section];
    float categoryWidth = tableView.frame.size.width - (15.f + 15.f); ///margin left, right + middle
    float categoryHeight = 50.f;
    
    UIButton *categoryBtnL = [[UIButton alloc] initWithFrame:CGRectMake(15.f, 10.f, categoryWidth, categoryHeight)];
    categoryBtnL.backgroundColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
    //categoryBtnL.layer.borderWidth = 1.5f;
    //categoryBtnL.layer.borderColor = [HexColor SKColorHexString:@"ce352"].CGColor;
    categoryBtnL.layer.cornerRadius = 20.f;
    categoryBtnL.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:17.f]];
    categoryBtnL.titleLabel.numberOfLines = 2;
    categoryBtnL.titleLabel.textAlignment = NSTextAlignmentCenter;
    [categoryBtnL setTitle:[catagoryL objectForKey:@"CATEGORIESNAME"] forState:UIControlStateNormal];
    [categoryBtnL setTitleColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]] forState:UIControlStateNormal];
    [categoryBtnL setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    categoryBtnL.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 35);
    categoryBtnL.tag = 103;
    categoryBtnL.accessibilityLabel = [catagoryL objectForKey:@"CATEGORIESID"];
    [categoryBtnL addTarget:self action:@selector(categoryTapping:) forControlEvents:UIControlEventTouchUpInside];
    
    float dropdownHeight = 25.f;
    UIButton *dropdown = [[UIButton alloc] initWithFrame:CGRectMake((categoryWidth+15)-(dropdownHeight+10), 10.f+(categoryHeight/2)-(dropdownHeight/2), dropdownHeight, dropdownHeight)];
    [dropdown setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
//    dropdown.tag = section;
//    [dropdown addTarget:self action:@selector(dropdownSubcatrgoryTapping:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *imgArrowUp = [[UIImage imageNamed:@"circle_arrow_up"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *imgArrowDown = [[UIImage imageNamed:@"circle_arrow_down"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    if ([[subCategoryShowHideDic objectForKey:[NSString stringWithFormat:@"%i",(int)section]] isEqualToString:@"S"]) {
        [dropdown setImage:imgArrowUp forState:UIControlStateNormal];
        [dropdown setImage:imgArrowUp forState:UIControlStateSelected];
    }
    else{
        [dropdown setImage:imgArrowDown forState:UIControlStateNormal];
        [dropdown setImage:imgArrowDown forState:UIControlStateSelected];
    }
    
    UIButton *dummyView = [[UIButton alloc] initWithFrame:CGRectMake((15+categoryWidth)-70, 10, categoryHeight+20, categoryHeight)];
    dummyView.backgroundColor = [UIColor clearColor];
    dummyView.tag = section;
    [dummyView addTarget:self action:@selector(dropdownSubcatrgoryTapping:) forControlEvents:UIControlEventTouchUpInside];
    
    [mView addSubview:categoryBtnL];
    
    if((int)[[catagoryL objectForKey:@"SUBCATEGORIES"] count] >0){
        [mView addSubview:dropdown];
        [mView addSubview:dummyView];
    }
    
    return mView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == [arrayOfCategory count]-1) {
        UIView *mView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, tableView.frame.size.width, 10.f)];
        mView.backgroundColor = [UIColor whiteColor];
        return mView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *subCateShowHide = [subCategoryShowHideDic objectForKey:[NSString stringWithFormat:@"%i",(int)indexPath.section]];
    if ([subCateShowHide isEqualToString:@"S"]) {
        int subCatecount = (int)[[[arrayOfCategory objectAtIndex:indexPath.section] objectForKey:@"SUBCATEGORIES"] count];
        if (subCatecount > 0)
            return (subCatecount* subCategoryButtonHeight) + ((subCatecount+1)*8);
    }
    return 0.f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (10.f + categoryButtonHeight + 10.f);
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == [arrayOfCategory count]-1) {
        return 10.f;
    }
    return 0.f;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    ///load more
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore){
        [loadMoreActivityIndicator startAnimating];
        isLoadingMore = YES;
        showRefreshing = NO;
    }
    /// refresh
    else if (scrollView.contentOffset.y <= -[Utility bounceHeightForLoadMore] && !isRefreshing){
        [refreshActivityIndicator startAnimating];
        isRefreshing = YES;
        showRefreshing = NO;
    }
    
    if (scrollView.contentOffset.y >= -50 && isRefreshing && !showRefreshing) {
        showRefreshing = YES;
        [self.tableView setContentOffset:CGPointMake(0, -50) animated:YES];
        self.view.userInteractionEnabled = NO;
        [SVProgressHUD show];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadIndex = 1;
            [self loadGategoryList];
            //NSLog(@"----- refresh data ------");
        });
    }
    else if (scrollView.contentOffset.y <= (self.tableView.contentSize.height - self.tableView.bounds.size.height + 50) && isLoadingMore  && !showRefreshing){
        [self.tableView setContentOffset:CGPointMake(0,(self.tableView.contentSize.height - self.tableView.bounds.size.height + 50)) animated:YES];
        showRefreshing = YES;
        self.view.userInteractionEnabled = NO;
        [SVProgressHUD show];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadIndex ++;
            [self loadGategoryList];
            NSLog(@"----- load more data ------");
        });
    }
}

-(void)categoryTapping:(UIButton*)sender{
    //NSLog(@"Cate ID : %@",sender.accessibilityLabel);
    [SVProgressHUD show];
    
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//    [layout setMinimumLineSpacing:20];
//    if (@available(iOS 11.0, *)) {
//        CategoriesCollectionViewController *vc = [[CategoriesCollectionViewController alloc] initWithCollectionViewLayout:layout];
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//        [self presentViewController:nav animated:YES completion:NULL];
//    } else {
//        // Fallback on earlier versions
//    }
    
    
    arrayOfCategorySeclected = @[sender.accessibilityLabel,sender.titleLabel.text,@""];

    [self loadMediaListItemsInCategory:sender.accessibilityLabel SubCategoryID:@""];
    
}

-(void)subCategoryTapping:(UIButton *)sender{
    //NSLog(@"Cate ID : %@",sender.accessibilityValue);
    //NSLog(@"SubCate ID : %@",sender.accessibilityLabel);
    [SVProgressHUD show];
    arrayOfCategorySeclected = @[@"",sender.titleLabel.text,sender.accessibilityLabel];//sender.accessibilityValue
    [self loadMediaListItemsInCategory:sender.accessibilityValue SubCategoryID:sender.accessibilityLabel];
}

-(void)dropdownSubcatrgoryTapping:(UIButton*)sender{
    [loadMoreActivityIndicator stopAnimating];
    [refreshActivityIndicator stopAnimating];
    [loadMoreActivityIndicator setHidden:true];
    [refreshActivityIndicator setHidden:true];
    
    NSString *subCateShowHide = [subCategoryShowHideDic objectForKey:[NSString stringWithFormat:@"%i",(int)sender.tag]];
    if ([subCateShowHide isEqualToString:@"H"]) {
        [subCategoryShowHideDic removeObjectForKey:[NSString stringWithFormat:@"%i",(int)sender.tag]];
        [subCategoryShowHideDic setValue:@"S" forKey:[NSString stringWithFormat:@"%i",(int)sender.tag]];
        
        //[sender setImage:[UIImage imageNamed:@"subCategory up1"] forState:UIControlStateNormal];
        // [sender setImage:[UIImage imageNamed:@"subCategory up"] forState:UIControlStateSelected];
    }
    else{
        [subCategoryShowHideDic removeObjectForKey:[NSString stringWithFormat:@"%i",(int)sender.tag]];
        [subCategoryShowHideDic setValue:@"H" forKey:[NSString stringWithFormat:@"%i",(int)sender.tag]];
        
        //[sender setImage:[UIImage imageNamed:@"subCategory down1"] forState:UIControlStateNormal];
        //[sender setImage:[UIImage imageNamed:@"subCategory down"] forState:UIControlStateSelected];
    }
    NSIndexSet *section = [[NSIndexSet alloc] initWithIndex:sender.tag];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];    
    //// when click on last Category
    if (sender.tag == [arrayOfCategory count]-1){
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
}


#pragma mark --------- Load Media List in Category ------
-(void)loadMediaListItemsInCategory:(NSString *)categoryID SubCategoryID:(NSString *)subCategoryID{
    ///api/DeviceMedia/MediaItems?deviceCode={deviceCode}&roomId={roomId}&mediaKind={mediaKind}&catId={catId}&subId={subId}&sort={sort}&loadIndex={loadIndex}
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaListItemsResponse:) name:@"MoreMediaItemPreview" object:nil];
    [api mediaItems:@"MoreMediaItemPreview" RoomId:curUsedLibraryID CategoryID:categoryID SubCatrgory:subCategoryID MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}

-(void)getMediaListItemsResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MoreMediaItemPreview" object:nil];
    //NSLog(@"MediaList Items : %@",notification.userInfo);
    //NSLog(@"ttt L %@",[arrayOfCategorySeclected description]);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MediaListCollection *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaListCollection"];
        vc.usingLibraryID = curUsedLibraryID;
        vc.navigationTitle = [arrayOfCategorySeclected[1] length]>0? arrayOfCategorySeclected[1]:@"";
        vc.categoryID = arrayOfCategorySeclected[0];
        vc.subCategoryID = ([arrayOfCategorySeclected[2] length] >0)? arrayOfCategorySeclected[2]:@"";
        vc.jsonData = notification.userInfo;
        vc.kindOfRoomKeyValue = @[KindOfRoomTypeDefault,@""];
         [self.navigationController pushViewController:vc animated:YES];
        
//        UINavigationController *nvg = [[UINavigationController alloc] initWithRootViewController:vc];
//        [self presentViewController:nvg animated:YES completion:nil];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}


#pragma mark ---------- Notification from Left Menu ------------

-(void)userChangingLibrary:(NSNotification *)notification{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    //[CustomActivityIndicator presentActivityIndicatorView];
    //[arrayOfCategory removeAllObjects];
    //[self loadGategoryList];
    
    [self.tabBarController setSelectedIndex:0];
}

-(void)watchHistory:(NSNotification*)notification{
    HistoryViewController *vc = [[HistoryViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)signOut:(NSNotification*)notification{
     [Utility showAlertControllerForSignout:self.view.window.rootViewController];
}

-(void)signIn:(NSNotification*)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SignInVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)setting:(NSNotification *)notification{
    SettingViewController *vc = [[SettingViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)openHelp:(NSNotification *)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    Help *vc = [mainSB instantiateViewControllerWithIdentifier:@"Help"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAbout:(NSNotification *)notification{
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    AboutTVC *vc = [mainSB instantiateViewControllerWithIdentifier:@"AboutTVC"];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)showAlertViewController:(NSString*)title Message:(NSString*)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    //    UIAlertAction* tryButton = [UIAlertAction
    //                                actionWithTitle:[Utility NSLocalizedString:@"Try_Again"]
    //                                style:UIAlertActionStyleDefault
    //                                handler:^(UIAlertAction * action)
    //                                {
    //
    //                                    [self viewDidLoad];
    //
    //                                }];
    //
    //    UIAlertAction* cancelButton = [UIAlertAction
    //                                actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
    //                                style:UIAlertActionStyleDefault
    //                                handler:^(UIAlertAction * action)
    //                                {
    //
    //                                }];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    //    [alert addAction:cancelButton];
    //    [alert addAction:tryButton];
    [alert addAction:okButton];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
}


-(void)loadLibraryLogoImage{
    ///load library Image
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSArray *arrOfLib = [DBManager selectLibraryPicAndName:curUsedLibraryID];
        roomName = (arrOfLib !=nil && [arrOfLib count] >0)? arrOfLib[0]: @"Unknown";
        UIImage *iimage = [RoomProfileLogo getRoomProfileLogo];
        if (iimage) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [barButtonLibrary setBackgroundImage:iimage forState:UIControlStateNormal];
            });
        }
        else{
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:(arrOfLib !=nil && [arrOfLib count] >= 2)? arrOfLib[1]: @""]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([Utility isImage:imgData]) {
                    UIImage *img = [Utility rescaleImage:[UIImage imageWithData:imgData] scaledToSize:navigationImageSize];
                    [barButtonLibrary setBackgroundImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                }
            });
        }
    });
}


#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    NSString *memberID = deviceCode;
    BOOL isMember = ([MemberInfo isSignin] || [OfflineShelf isOnlyOfflineModeEnable]) && [DBManager isMember];
    if (isMember)
        memberID = [DBManager selectMemberID];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    [api roomsDetail:@"LibraryDetail" RoomID:curUsedLibraryID memberID:memberID];
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    //NSLog(@"libraryDetailResponse : %@",notification.userInfo);
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Library Info to ViewController
        UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        libraryDetailVC = [mainSB instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = roomName;
        libraryDetailVC.libraryID = curUsedLibraryID;
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        
        /// load items in Library
        [self loadMediaItemsInLibrary];
    }
    else{
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
    
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    [api mediaItems:@"MediaItemsInLibrary" RoomId:curUsedLibraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = (NSArray*) notification.userInfo;
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
}

-(void)memberProfile {
    MemberProfileViewController *vc = [[MemberProfileViewController alloc] initWithMemberId:[DBManager selectMemberID]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)redeemCode {
    RedeemViewController *vc = [[RedeemViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:YES completion:NULL];
}

@end

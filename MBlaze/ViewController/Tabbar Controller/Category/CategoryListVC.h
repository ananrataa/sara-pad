//
//  Tabbar1.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"
#import "LibraryDetailVC.h"

@interface CategoryListVC : UITableViewController <UISearchBarDelegate>{
    //UIBarButtonItem *leftBarButton;
     UIButton *barButtonLibrary;
    //UIBarButtonItem *rightBarButton;
    
    CGSize navigationImageSize;
    
    NSUserDefaults *userDefault;
    
    ServiceAPI *api;
    
    NSMutableArray *arrayOfCategory;
    
    NSString *curUsedLibraryID;
    
    NSString *deviceCode;
    
    NSArray *arrayOfCategorySeclected;
    
    NSMutableDictionary *subCategoryShowHideDic;
    
    Boolean isShowSubCategory;
    int dropdownTappingAtSection;
    
    int loadIndex;
    
    BOOL isLoadingMore;
    //BOOL loadMoreEnable;
    
    UIActivityIndicatorView *refreshActivityIndicator;
    UIActivityIndicatorView *loadMoreActivityIndicator;
    BOOL isRefreshing;
    BOOL showRefreshing;
    
    LibraryDetailVC *libraryDetailVC;
    NSString *roomName;

}

@property (nonatomic, strong) UISearchController *searchController;

@end

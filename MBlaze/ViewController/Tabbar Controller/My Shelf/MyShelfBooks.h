//
//  Tabbar2.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import "ServiceAPI.h"
#import "LibraryDetailVC.h"

@interface MyShelfBooks :  UICollectionViewController <UICollectionViewDelegateFlowLayout>{
    
    NSUserDefaults *userDefault;
    NSString *memberID;
    NSString *deviceCode;
    NSString *curUsedLibraryID;
    
    //UIBarButtonItem *leftBarButton;
    UIButton *barButtonLibrary;
    //UIBarButtonItem *rightBarButton;
    CGSize navigationImageSize;
    
    LibraryDetailVC *libraryDetailVC;
    NSString *roomName;
    
//    float navigationHeight;
//    CGSize navigationImageSize;
    
    float collectionCellWidth;
    float collectionCellHeight;

    UIBarButtonItem *sortBarButton;
    UIBarButtonItem *searchBarButton;
    
    UIView *sortView;
    
    NSMutableArray *arrayofLibrary;
    NSMutableArray *arrayOfMedia;
    NSMutableArray *arrayshelfId;
    NSMutableDictionary *dictPresetInfoFootter;
    NSMutableDictionary *dictPresetShowMore;
    NSString *currentShelfId;
    
    //int xScrollViewContentOffset;
    
    ReaderDocument *document;
    //NSString *tempFloderName;
    NSString *filePath;
    NSString *tempPath;
    NSString *fileExtension;
    
    int currentSelectCell;
    int currentSelectSection;
    
    ServiceAPI *api;
    
    BOOL isSelectFirstTime;
    
   // float footterHeight;
    //float despBodyHeight;
    BOOL isMember;
    
}

//@property (strong,nonatomic)NSString* navigationTitle;

@end

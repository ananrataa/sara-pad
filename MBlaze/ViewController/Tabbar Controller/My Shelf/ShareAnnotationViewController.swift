//
//  ShareAnnotationViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/7/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class ShareAnnotationViewController: UIViewController, AlertHandler {
    let tableView:UITableView = {
        let tb = UITableView(frame: .zero, style: .plain)
        tb.backgroundColor = .orange
        tb.tableFooterView = UIView()
        return tb
    }()
    var currentSelected: IndexPath?
    var sharedItems:[SPAnnotationList] = []
    var roomId: String
    var mediaId: String
    var memberId: String
    
    @objc init(roomId: String, mediaId: String, memberId: String) {
        self.roomId = roomId
        self.mediaId = mediaId
        self.memberId = memberId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    func setup() {
        self.title = String.localizedString(key: "Shared Notes")
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save(_:)))
        let att = [NSAttributedString.Key.font: UIFont(name: mainFont, size: 17)!]
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(att, for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(att, for: .normal)
        
        view.addSubview(tableView)
        tableView.pin(to: view)
        tableView.dataSource = self
        tableView.delegate = self
        
        //Load data
        memberId = "ac740489-8a60-4fb7-913a-a26575333b53"
        mediaId = "df751472-4446-4127-9480-b64461c8023f"
        roomId = "3b125216-c64f-4593-a1bc-9a5e558adab8"
        RoomAPI.shared.sharedAnnotations(roomId: roomId, mediaId: mediaId, memberId: memberId, os: "ios") { (data:[SPAnnotationList]) in
            self.sharedItems = data
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }

    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    @objc func save(_ sender: UIBarButtonItem) {
        if let indexPath = currentSelected {
            let alertTitle = String.localizedString(key: "Annotation Confirm")
            let message = String.localizedString(key: "Annotation Message")
            let strSave = String.localizedString(key: "Save")
            confirmAlert(title: alertTitle, messgae: message, okTitle: strSave) {[unowned self] (action) in
                if action == .OK {
                    let item = self.sharedItems[indexPath.row]
                    let message = String.localizedString(key: "Shared Success") + " '\(item.displayName)'."
                    self.successAlert(message: message, duration: 1.5, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        } else {
            infoAlert(message: String.localizedString(key: "Shared Error"), duration: 3)
        }
    }
}

@available(iOS 11.0, *)
extension ShareAnnotationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sharedItems.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String.localizedString(key: "Shared by")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell_shared")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell_shared")
            cell?.imageView?.tintColor = .gray
            cell?.textLabel?.font = UIFont(name: mainFont, size: 17)
        }
        cell?.imageView?.image = #imageLiteral(resourceName: "shared_by")
        cell?.textLabel?.text = sharedItems[indexPath.row].displayName
        if let curIndexPath = currentSelected, curIndexPath == indexPath {
            cell?.accessoryView = UIImageView(image: #imageLiteral(resourceName: "checked"))
        } else {
            cell?.accessoryView = nil
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 44)))
        v.backgroundColor = UIColor.white.withAlphaComponent(0.65)
        let lblHeader = UILabel()
        lblHeader.font = UIFont(name: mainFont, size: 17)
        lblHeader.backgroundColor = .clear
        lblHeader.text = String.localizedString(key: "Shared by")
        v.addSubview(lblHeader)
        lblHeader.anchor(top: v.topAnchor, leading: v.leadingAnchor, bottom: v.bottomAnchor, trailing: v.trailingAnchor, padding:.init(top: 0, left: 12, bottom: 0, right: 0))
        return v
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 44)))
        v.backgroundColor = UIColor.white.withAlphaComponent(0.65)
        let lblFooter = UILabel()
        lblFooter.font = UIFont(name: mainFont, size: 14)
        lblFooter.backgroundColor = .clear
        lblFooter.text = String.localizedString(key: "Shared Info")
        v.addSubview(lblFooter)
        lblFooter.anchor(top: v.topAnchor, leading: v.leadingAnchor, bottom: v.bottomAnchor, trailing: v.trailingAnchor, padding:.init(top: 0, left: 12, bottom: 0, right: 0))
        return v
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SVProgressHUD.dismiss()
        tableView.deselectRow(at: indexPath, animated: true)
        currentSelected = indexPath
        tableView.reloadData()
    }
}

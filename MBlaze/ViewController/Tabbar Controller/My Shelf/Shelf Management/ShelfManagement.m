//
//  ShelfManagement.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 22/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "ShelfManagement.h"
#import "Utility.h"
#import "ConstantValues.h"
#import "SVProgressHUD.h"
#import "DBManager.h"
#import "MediaBorrowLimit.h"
#import "ThemeTemplate.h"
#import "HexColor.h"
#import "AnnotationFileManagement.h"
#import "SARA_PAD-Swift.h"
#import "SVProgressHUD.h"

@interface ShelfManagement ()

@end

@implementation ShelfManagement

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"syncHighlightCell"];
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"mediaBorrowCell"];
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"annotationCell"];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefaults objectForKey:KeyDeviceCode];
    isAotoSyncPdfAnnotation = [userDefaults boolForKey:AutoSyncPdfAnnatationKey];
    
    isMember = [deviceCode isEqualToString:self.memberID]? false:true;
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    //Hide TabBar
    [self setExtendedLayoutIncludesOpaqueBars:YES];
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:[Utility NSLocalizedString:@"shelfManagement"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissViewController:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"syncHighlightCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.textLabel.font = [UIFont fontWithName:mainFont size:17];
        cell.textLabel.text = [Utility NSLocalizedString:@"syncHightlight"];
        UISwitch *switchBtn = [[UISwitch alloc] init];
        [switchBtn setOn:isMember? isAotoSyncPdfAnnotation:false];
        [switchBtn addTarget:self action:@selector(switchButtonOnOff:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchBtn;
        switchBtn.enabled = isMember;
    }
    else if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mediaBorrowCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        
        cell.textLabel.font = [UIFont fontWithName:mainFont size:17];
        cell.textLabel.text = [Utility NSLocalizedString:@"checkBorrowLimit"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"annotationCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        
        cell.textLabel.font = [UIFont fontWithName:mainFont size:17];
        cell.textLabel.text = [Utility NSLocalizedString:@"AnnotationFile"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView;
    if (section == 0) {
        CGRect viewFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 70);
        footerView = [[UIView alloc] initWithFrame:viewFrame];
        
        UILabel *titlelabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, self.tableView.frame.size.width-20, 21)];
        titlelabel.font = [UIFont fontWithName:mainFont size:13.f];
        titlelabel.numberOfLines = 2;
        titlelabel.textColor = [UIColor darkGrayColor];
        titlelabel.text = [Utility NSLocalizedString:@"DescriptionSyncHightlight"];
        
        [footerView addSubview:titlelabel];
        
    }
    
    else if (section == 2) {
        CGRect viewFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 70);
        footerView = [[UIView alloc] initWithFrame:viewFrame];
        
        UILabel *titlelabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, self.tableView.frame.size.width-20, 21)];
        titlelabel.font = [UIFont fontWithName:mainFont size:13.f];
        titlelabel.numberOfLines = 2;
        titlelabel.textColor = [UIColor darkGrayColor];
        titlelabel.text = [Utility NSLocalizedString:@"DescriptionAnnotationFileManage"];
        
        [footerView addSubview:titlelabel];
        
    }
    
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        [self checkMediaBorrowLimit];
    } else if (indexPath.section == 2){
        if (isMember) {
            [self loadAnnotationFile];
        } else {
            [SVProgressHUD showInfoWithStatus:[Utility NSLocalizedString:@"MemberOnly"]];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)switchButtonOnOff:(UISwitch*)sender{
    if (sender.on)
        [sender setOn:NO];
    else
        [sender setOn:YES];
    //NSLog(@"switch Button status : %@",sender.on? @"YES" : @"NO");
    [userDefaults setBool:sender.on forKey:AutoSyncPdfAnnatationKey];
    [userDefaults synchronize];
    
}


#pragma mark --------- Check Media Borrow Limit --------
-(void)checkMediaBorrowLimit{
    //api/DeviceMedia/LimitRent?deviceCode={deviceCode}&itemLimit={itemLimit}&dayLimit={dayLimit}
    
    [SVProgressHUD show];
    [self setTitle:@""];
    if (@available(iOS 11.0, *)) {
        BorrowStatusViewController *vc = [[BorrowStatusViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];

    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkMediaBorrowLimitResult:) name:@"checkMediaBorrowLimit" object:nil];
        api = [[ServiceAPI alloc] init];
        [api limitRent:@"checkMediaBorrowLimit" LimitItem:0 LimitDay:0];
    }
    
    
}

-(void)checkMediaBorrowLimitResult:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"checkMediaBorrowLimit" object:nil];
     //NSLog(@"MediaBorrowLimit Result : %@",[notification.userInfo description]);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        NSMutableArray *jsonArray = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in notification.userInfo) {
            NSMutableDictionary * multiDic = [[NSMutableDictionary alloc] init];
            [multiDic setValue:[dic objectForKey:@"ROOMID"] forKey:@"ROOMID"];
            [multiDic setValue:[dic objectForKey:@"ROOMNAME"] forKey:@"ROOMNAME"];
            [multiDic setValue:[dic objectForKey:@"LIMITRENT"] forKey:@"LIMITRENT"];
            [multiDic setValue:[dic objectForKey:@"BALANCE"] forKey:@"BALANCE"];
            [multiDic setValue:[dic objectForKey:@"AMOUNT"] forKey:@"AMOUNT"];
            [multiDic setValue:[dic objectForKey:@"SYMBOL"] forKey:@"SYMBOL"];
            [jsonArray addObject:multiDic];
        }
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        MediaBorrowLimit *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaBorrowLimit"];
        vc.jsonData = jsonArray;
        vc.isMember = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else{
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    }
}

#pragma  mark ---- check Media Limit & Rent Limit -------
-(NSArray*)checkUserAvailableToDownload:(BOOL)isMember{
    //BOOL isMember = [DBManager isMember];
    if (isMember)
        return [NSArray arrayWithArray:[DBManager selectUserDayAndMediaLimit]];
    else
        return [NSArray arrayWithObjects:[NSString stringWithFormat:@"%i",MaximunDateForBorrow],
                [NSString stringWithFormat:@"%i",MaximunFilesDownload],nil];
}


#pragma mark ------- Annotation File Management ---------
-(void)loadAnnotationFile{
    [SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(annotationFileResponse:) name:@"AnnotationFile" object:nil];
    api = [[ServiceAPI alloc] init];
    [api annotationFile:@"AnnotationFile" MemberID:self.memberID];
}

-(void)annotationFileResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AnnotationFile" object:nil];
    //NSLog(@"annotationFile Result : %@",[notification.userInfo description]);
    
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        AnnotationFileManagement *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AnnotationFileManagement"];
        [vc setJsonDict:notification.userInfo];
        [vc setMemberID:self.memberID];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else{
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:@"UnknownError"] Controller:(UIViewController*)self];
    }
}
@end

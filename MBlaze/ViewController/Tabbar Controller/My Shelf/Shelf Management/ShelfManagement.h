//
//  ShelfManagement.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 22/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface ShelfManagement : UITableViewController
{
    NSUserDefaults *userDefaults;
    BOOL isAotoSyncPdfAnnotation;
    NSString * deviceCode;
    BOOL isMember;
    ServiceAPI *api;
    
}

@property (strong,nonatomic)NSString *memberID;

@end

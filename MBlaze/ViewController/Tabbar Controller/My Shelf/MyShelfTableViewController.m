//
//  MyShelfTableViewController.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MyShelfTableViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "DBManager.h"
#import "ConstantValues.h"
#import "CustomActivityIndicator.h"
#import "MediaDetailTVC.h"
#import "OfflineMediaCollectionCell.h"
#import "CollectionHeaderView2.h"
#import "HistoryTVC.h"
#import "ForgotPasswordVC.h"
#import "SignInVC.h"
#import "OfflineShelf.h"
#import "HexColor.h"
#import "Member.h"
#import "SettingTCV.h"
#import "MediaBorrowLimit.h"
#import "Help.h"
#import "AboutTVC.h"
#import "SARA_PAD-Swift.h"
#import <PDFKit/PDFKit.h>
#import "RoomProfileLogo.h"
#import "TableViewCell.h"

@interface MyShelfTableViewController ()


@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@end

@implementation MyShelfTableViewController

#define cellMediaIdentifier  @"mediaOfflineContentCell" //@"ccell"//
#define cellHeaderIdentifier @"collectionViewHeader2"
#define headerHeight 40.0f
#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationItem setTitle:[Utility NSLocalizedString:@"MyShelf"]];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName :
                                                                        [UIColor blackColor]};
    /// calculate collection Cell Width & Height
    collectionCellWidth = IPAD?((self.tableView.frame.size.width-32)/3):((self.tableView.frame.size.width-24)/2);
    collectionCellHeight =  ((collectionCellWidth*4.f)/3.f)+95.f;
    
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    navigationImageSize = CGSizeMake(navigationHeight-16, navigationHeight-16);
    
    UIButton *menu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
    [menu setImage:[Utility rescaleImage:[UIImage imageNamed:@"menu"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
    [menu addTarget:self action:@selector(leftTopBarTapping:) forControlEvents:UIControlEventTouchUpInside];
    menu.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    
    barButtonLibrary = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
    [barButtonLibrary setBackgroundImage:[Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
    [barButtonLibrary addTarget:self action:@selector(libraryImageTapping:) forControlEvents:UIControlEventTouchUpInside];
    barButtonLibrary.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    barButtonLibrary.clipsToBounds = YES;
    barButtonLibrary.layer.cornerRadius = navigationImageSize.height/2;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
    UIBarButtonItem *libraryLogo = [[UIBarButtonItem alloc] initWithCustomView:barButtonLibrary];
    self.navigationItem.leftBarButtonItems = @[leftBarButton,libraryLogo];
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
}

-(void)viewWillAppear:(BOOL)animated{
    if([OfflineShelf isOnlyOfflineModeEnable]){
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    else{
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangingLibrary:) name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchHistory:) name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signOut:) name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting:) name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp:) name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signIn:) name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAbout:) name:@"About" object:nil];
    
    [CustomActivityIndicator presentActivityIndicatorView];
    [self loadLibraryLogoImage];
    [Utility removeExpiredMediaOnShelf];
    [self dismissOpenReturnViewForPreviousCell];
    currentSelectCell = 0;
    currentSelectSection = 0;
    isSelectFirstTime = YES;
    
    currentShelfId = @"0";
    [self getShelf];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"About" object:nil];
    
}

-(void)leftTopBarTapping:(id)sender{
    //    NSLog(@"------------- leftTopBarTapping --------------");
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)libraryImageTapping:(UIButton *)sender{
    [CustomActivityIndicator presentActivityIndicatorView];
    [self loadLibraryInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getShelf{
    BOOL isSignin = [DBManager isMember];
    arrayshelfId = @[@[@"0"]];
    if (isSignin)
        switch ([DBManager getCountAvailableShelf]) {
            case 0:
                arrayshelfId = @[@[@"0",[Utility NSLocalizedString:@"userShelf"]]];
                break;
            case 1:
                arrayshelfId = @[@[@"0",[Utility NSLocalizedString:@"userShelf"]]];
                break;
            case 2:
                arrayshelfId = @[@[@"0",[Utility NSLocalizedString:@"userShelf"]],@[@"1",[Utility NSLocalizedString:@"memberShelf"]]];
                break;
            case 3:
                arrayshelfId = @[@[@"0",[Utility NSLocalizedString:@"userShelf"]],@[@"1",[Utility NSLocalizedString:@"memberShelf"]],@[@"2",[Utility NSLocalizedString:@"ownerShelf"]]];
                break;
            default:
                break;
        }
    
    [self getMediaByShelfId:currentShelfId];
}

-(void)getMediaByShelfId:(NSString *)shelfId{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        arrayofLibrary = [DBManager selectLibraryNameOnMyShelfId:shelfId];
        arrayOfMedia = [DBManager selectMediaOnMyShelfId:shelfId];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [CustomActivityIndicator dismissActivityIndicatorView];
        });
        
        //        for (NSArray *libraryArr in arrayofLibrary) {
        //            NSString *attributeName  = @"getLibraryID";
        //            NSString *attributeValue = libraryArr[1];
        //            NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K MATCHES %@",attributeName, attributeValue];
        //
        //            NSMutableArray *arrayOfItem = [[NSMutableArray alloc] init];
        //            arrayOfItem = [[arrayMedia filteredArrayUsingPredicate:predicate] mutableCopy];
        //            NSLog(@"%@",[arrayOfItem description]);
        //        }
        
    });
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(TableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
    NSInteger index = cell.collectionView.indexPath.row;
    cell.collectionView.tag = indexPath.row;
    
    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [arrayofLibrary count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([arrayofLibrary count] > 0)
        return [self getNumberOfItemInLibrary:[arrayofLibrary objectAtIndex:section]];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //     UICollectionViewCell *cell = (UICollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
    
    OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    ///cover
    NSString *path = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaCoverPath]];
    
    [cell.mediaCoverImageView setImage:[Utility loadImageWithFilePath:path defaultImage:[UIImage imageNamed:@"default_media_cover.png"]]];
    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
    cell.mediaCoverImageView.userInteractionEnabled = YES;
    
    cell.mediaTitleLabel.text = [myShelfData getMediaTitle];
    cell.mediaAuthorLabel.text = [myShelfData getMediaAuthor];
    cell.mediaExpLabel.text = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Exp"],[Utility covertDateToFormat:[myShelfData getDateExp] InputFormat:@"yyyyMMdd" OutputFormat:@"dd/MM/yyyy"]];
    
    [Utility setFont: cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    [Utility setFont:cell.mediaExpLabel.font WithFontSize:[Utility fontSize:12.f]];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[myShelfData getMediaType]];
    
    cell.videoImagePic.hidden = YES;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = uiView;
    
    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == currentSelectSection && indexPath.row == currentSelectCell && !isSelectFirstTime) {
        return NO;
    } else {
        return YES;
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"collectionView didSelectItemAtIndexPath");
    // NSLog(@"------------ index : %li --------------",(long)indexPath.row );
    
    OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[collectionView cellForItemAtIndexPath:indexPath];
    UIImageView *cover = (UIImageView*)cell.mediaCoverImageView;
    
    for (UIView *subViews in [cover subviews]) {
        [subViews removeFromSuperview];
    }
    
    if (!isSelectFirstTime) {
        [self dismissOpenReturnViewForPreviousCell];
    }
    
    isSelectFirstTime = NO;
    
    currentSelectSection = (int)indexPath.section;
    currentSelectCell = (int)indexPath.row;
    
    CGRect actionViewRect = cover.frame;
    UIView *actionView = [[UIView alloc] initWithFrame:actionViewRect];
    actionView.backgroundColor = [[HexColor SKColorHexString:@"0E0F20"] colorWithAlphaComponent:0.9f];
    actionView.tag = indexPath.row;
    
    [cover addSubview:actionView];
    
    float buttonHeight = 40.f;
    float buttonWidth = (actionViewRect.size.width-60) ;
    float buttonYpoint = (actionViewRect.size.height/2) - 15 - buttonHeight;
    
    UIButton *openBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, buttonYpoint, buttonWidth, buttonHeight)];
    [openBtn setBackgroundColor:[HexColor SKColorHexString:@"D44C27"]];
    [openBtn setTitle:[Utility NSLocalizedString:@"Open"] forState:UIControlStateNormal];
    [openBtn addTarget:self action:@selector(prepare4OpenMedia:) forControlEvents:UIControlEventTouchUpInside];
    openBtn.clipsToBounds = YES;
    openBtn.layer.cornerRadius = 12.f;
    [Utility setFont:openBtn.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
    
    float buttonYpoint1 =  (actionViewRect.size.height/2) + 15 ;
    UIButton *returnedBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, buttonYpoint1, buttonWidth, buttonHeight)];
    //[returnedBtn setBackgroundColor:[HexColor SKColorHexString:@"0E0F20"]];
    [returnedBtn setTitle:[Utility NSLocalizedString:@"Return"] forState:UIControlStateNormal];
    returnedBtn.titleLabel.textColor = [UIColor whiteColor];
    [returnedBtn addTarget:self action:@selector(prepare4ReturnMedia:) forControlEvents:UIControlEventTouchUpInside];
    returnedBtn.clipsToBounds = YES;
    returnedBtn.layer.cornerRadius = 12.f;
    returnedBtn.layer.borderWidth = 2.f;
    returnedBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    returnedBtn.enabled = YES;
    [Utility setFont:returnedBtn.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
    
    if([OfflineShelf isOnlyOfflineModeEnable]){
        returnedBtn.enabled = NO;
        returnedBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        returnedBtn.titleLabel.textColor = [UIColor lightGrayColor];
        
    }
    
    [actionView addSubview:openBtn];
    [actionView addSubview:returnedBtn];
    
    //[collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    // UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        CollectionHeaderView2 *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderIdentifier forIndexPath:indexPath];
        if (indexPath.section==0)
            headerView.frame = CGRectMake(0.f, 30.f, headerView.frame.size.width, headerView.frame.size.height);
        headerView.backgroundColor = [UIColor whiteColor];
        
        NSArray *header = [arrayofLibrary objectAtIndex:indexPath.section];
        // NSLog(@"Header Name : %@",header[1]);
        
        UILabel *headerName = (UILabel*)[headerView.subviews firstObject];
        headerName.text = [header count]>=3? header[2]:@"";
        headerName.textColor = [UIColor blackColor];
        headerName.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
        
        UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
        seeMoreBtn.hidden = YES;
        //        [seeMoreBtn setTitle:@"See more >>" forState:UIControlStateNormal];
        //        seeMoreBtn.titleLabel.font = [UIFont fontWithName:mainFont size:12.0f];
        //        [seeMoreBtn setTintColor:[UIColor blackColor]];
        //        seeMoreBtn.accessibilityLabel = [header count]>0? header[0]:@""; //**//
        //        [seeMoreBtn addTarget:self action:@selector(seeMoreTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        return headerView;
    }
    else
        return nil;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width, headerHeight);
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //[self dismissOpenReturnViewForPreviousCell];
}


#pragma mark ------- dismiss Open & Return View for All Cell -----
-(void)dismissOpenReturnViewForPreviousCell{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectCell inSection:currentSelectSection];
//    OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
//    UIImageView *cover = (UIImageView*)cell.mediaCoverImageView;
//    for (UIView *subViews in [cover subviews]) {
//        [subViews removeFromSuperview];
//    }
    
    //[self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark ------- Open Media --------
-(void)prepare4OpenMedia:(UIButton*)sender{
    
    [CustomActivityIndicator presentActivityIndicatorView];
    
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
    //// floder name ///
    tempFloderName = [NSString stringWithFormat:@"%@/%@",temporyFloderName,[myShelfData getLibraryID]];
    
    filePath  = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaFilePath]];
    
    //NSLog(@"Read PDF at path : %@" ,filePath);
    
    //filePath =  [myShelfData getMediaFilePath];
    [self prepare4ReadPDF:[myShelfData getMediaOriginalName] UserID:[myShelfData getMemberID]];
}


#pragma mark ---------- See more Tapping ----------
-(void)seeMoreTapping:(UIButton*)sender{
    // NSLog(@"---------- See more Tapping ----------");
}

#pragma mark ---------- Notification from Left Menu ------------
-(void)userChangingLibrary:(NSNotification *)notification{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [self.tabBarController setSelectedIndex:0];
}

-(void)watchHistory:(NSNotification*)notification{
    HistoryTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryTVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)signOut:(NSNotification*)notification{
    [Utility prepare4SignOut:self.view.window.rootViewController];
}

-(void)signIn:(NSNotification*)notification{
    SignInVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)setting:(NSNotification *)notification{
    SettingTCV *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingTCV"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openHelp:(NSNotification *)notification{
    Help *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Help"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAbout:(NSNotification *)notification{
    AboutTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutTVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSInteger)getNumberOfItemInLibrary:(NSArray*)libArray{
    NSString *attributeName  = @"getLibraryID";
    NSString *attributeValue = libArray[1];
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K MATCHES %@",attributeName, attributeValue];
    return [[arrayOfMedia filteredArrayUsingPredicate:predicate] count];
}

-(NSMutableArray*)getItemInLibrary:(NSArray*)libArray{
    NSString *attributeName  = @"getLibraryID";
    NSString *attributeValue = libArray[1];
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K MATCHES %@",attributeName, attributeValue];
    NSMutableArray *arrayOfItem = [[NSMutableArray alloc] init];
    arrayOfItem = [[arrayOfMedia filteredArrayUsingPredicate:predicate] mutableCopy];
    return arrayOfItem;
}


#pragma -------- Prepare for Read PDF File --------
-(void)prepare4ReadPDF:(NSString*)origiFileName UserID:(NSString*)userID{
    //    NSString *path = [[NSBundle mainBundle] URLForResource:@"Patipada" withExtension:@"epub"];
    //NSLog(@"prepare4ReadPDF at path : %@" ,filePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] && [[[filePath lastPathComponent] pathExtension] caseInsensitiveCompare:@"pdf"] == NSOrderedSame) {
        
        if (@available(iOS 11.0, *)) {
            //NSLog(@"fileExistsAtPath : %@",filePath);
            NSURL *url = [NSURL fileURLWithPath:filePath];
            PDFDocument *pdfDoc = [[PDFDocument alloc] initWithURL:url];
            NSString *isPassLocked = nil;
            if([pdfDoc isLocked]){
                isPassLocked = [Utility generatePassword:origiFileName];
                [pdfDoc unlockWithPassword:isPassLocked];
            }
            NSArray *arrMediaID = [[[filePath stringByDeletingPathExtension] lastPathComponent] componentsSeparatedByString:@"_"];
            
            PDFReaderViewController * class = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PDFReaderViewController"];
            [class setPasswordLock:isPassLocked];
            [class setPdfDocument:pdfDoc];
            [class setMMediaID:arrMediaID[1]];
            [class setUserID:userID];
            
            [CustomActivityIndicator dismissActivityIndicatorView];
            
            // [self.navigationController pushViewController:class animated:YES];
            UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:class];
            [self presentViewController:nv animated:YES completion:nil];
        }
        else{
            NSString *password = [Utility generatePassword:origiFileName];
            document = [ReaderDocument withDocumentFilePath:filePath password:password];
            
            if (document) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissReaderViewController:) name:@"dismissReaderViewController" object:nil];
                NSArray *arrMediaID = [[[filePath stringByDeletingPathExtension] lastPathComponent] componentsSeparatedByString:@"_"];
                //NSLog(@"prepare4ReadPDF mediaID : %@" ,arrMediaID[1]);
                ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document MediaID:arrMediaID[1]];
                readerViewController.delegate = (id) self;
                readerViewController.hidesBottomBarWhenPushed = YES;
                
                [CustomActivityIndicator dismissActivityIndicatorView];
                
                [self.navigationController pushViewController:readerViewController animated:YES];
            }
            else{
                [CustomActivityIndicator dismissActivityIndicatorView];
                [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:[Utility NSLocalizedString:@"UnOpenFile"] Controller:(UIViewController*)self];
            }
        }
    }
    else{
        [CustomActivityIndicator dismissActivityIndicatorView];
        [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:[Utility NSLocalizedString:@"UnOpenFile"] Controller:(UIViewController*)self];
    }
    
}

#pragma mark - ReaderViewControllerDelegate methods
-(void)dismissReaderViewController:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissReaderViewController" object:nil];
    
    [self dismissOpenReturnViewForPreviousCell];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&error];
        
        if (error)
            NSLog(@"Failed to remove item at path: %@", tempPath);
        //else
        //NSLog(@"Success to remove item at path: %@", filePath);
    }
    
    self.tabBarController.delegate = (id) self;
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -------- Return Media by User ----------
-(void)prepare4ReturnMedia:(UIButton*)sender{
    //api/DeviceMedia/MediaReturn?deviceCode={deviceCode}&MediaId={MediaId}&RoomId={RoomId}&MemberId={MemberId}
    [CustomActivityIndicator presentActivityIndicatorView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMediaResponse:) name:@"ReturnMedia" object:nil];
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
    NSString *param;
    if ([[myShelfData getMemberID] isEqualToString:deviceCode])
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaReturn?deviceCode=%@&MediaId=%@&RoomId=%@&MemberId=",hostIpRoomAPI,deviceCode,[myShelfData getMediaID],[myShelfData getLibraryID]];
    else
        param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaReturn?deviceCode=%@&MediaId=%@&RoomId=%@&MemberId=%@",hostIpRoomAPI,deviceCode,[myShelfData getMediaID],[myShelfData getLibraryID],[myShelfData getMemberID]];
    
    //NSLog(@"prepare4ReturnMedia : %@",param);
    [service sendHTTPRequest:param NotificationName:@"ReturnMedia" ReturnType:@"JSON" HttpMethod:@"GET"];
    
    myShelfData = nil;
}

-(void)returnMediaResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReturnMedia" object:nil];
    //NSLog(@"Return Media Response : %@",[notification.userInfo description]);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if([notification.userInfo isKindOfClass:[NSString class]]){
        [Utility showAlertViewController:@"Return Media Failed!" Message:[notification.userInfo description] Controller:(UIViewController*)self];
    }
    else{
        OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
        //delete file
        [Utility removeFileInDirectory:[myShelfData getMediaFilePath]];
        //delete media on shelf && bookmark remind
        [DBManager deleteMediaOnshelf:[myShelfData getMediaID]];
        // delete last open page
        [userDefault removeObjectForKey:[NSString stringWithFormat:@"lastOpenPage_%@",[myShelfData getMediaID]]];
        //delete book mark
        [userDefault removeObjectForKey:[NSString stringWithFormat:@"bookmark_%@",[myShelfData getMediaID]]];
        
        [arrayOfMedia removeAllObjects];
        [arrayofLibrary removeAllObjects];
        myShelfData = nil;
        
        
        [CustomActivityIndicator dismissActivityIndicatorView];
        
        [self viewWillAppear:YES];
    }
}



#pragma mark --------- Check Media Borrow Limit --------
-(void)checkMediaBorrowLimit:(UIButton *)sender{
    //api/DeviceMedia/LimitRent?deviceCode={deviceCode}&itemLimit={itemLimit}&dayLimit={dayLimit}
    
    [CustomActivityIndicator presentActivityIndicatorView];
    
    BOOL isMember = [DBManager isMember];
    if (!isMember) {
        int amount = (int)[DBManager selectTotalMediaOnshelf:deviceCode];
        NSMutableDictionary * multiDic = [[NSMutableDictionary alloc] init];
        [multiDic setValue:@"" forKey:@"ROOMID"];
        [multiDic setValue:@"" forKey:@"ROOMNAME"];
        [multiDic setValue:[NSString stringWithFormat:@"%i",MaximunFilesDownload] forKey:@"LIMITRENT"];
        [multiDic setValue:[NSString stringWithFormat:@"%i",(MaximunFilesDownload-amount)] forKey:@"BALANCE"];
        [multiDic setValue:[NSString stringWithFormat:@"%i",amount] forKey:@"AMOUNT"];
        
        [CustomActivityIndicator dismissActivityIndicatorView];
        
        MediaBorrowLimit *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaBorrowLimit"];
        vc.jsonData = [NSMutableArray arrayWithObjects:multiDic, nil];
        vc.isMember = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        NSArray *limitArr = [self checkUserAvailableToDownload:isMember];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkMediaBorrowLimitResult:) name:@"checkMediaBorrowLimit" object:nil];
        NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/LimitRent?deviceCode=%@&itemLimit=%i&dayLimit=%i",hostIpRoomAPI,deviceCode,[limitArr[1] intValue],[limitArr[0] intValue]];
        
        [service sendHTTPRequest:param NotificationName:@"checkMediaBorrowLimit" ReturnType:@"JSON" HttpMethod:@"GET"];
    }
    
}

-(void)checkMediaBorrowLimitResult:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"checkMediaBorrowLimit" object:nil];
    // NSLog(@"MediaBorrowLimit Result : %@",[notification.userInfo description]);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        NSMutableArray *jsonArray = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in notification.userInfo) {
            NSMutableDictionary * multiDic = [[NSMutableDictionary alloc] init];
            [multiDic setValue:[dic objectForKey:@"ROOMID"] forKey:@"ROOMID"];
            [multiDic setValue:[dic objectForKey:@"ROOMNAME"] forKey:@"ROOMNAME"];
            [multiDic setValue:[dic objectForKey:@"LIMITRENT"] forKey:@"LIMITRENT"];
            [multiDic setValue:[dic objectForKey:@"BALANCE"] forKey:@"BALANCE"];
            [multiDic setValue:[dic objectForKey:@"AMOUNT"] forKey:@"AMOUNT"];
            [multiDic setValue:[dic objectForKey:@"SYMBOL"] forKey:@"SYMBOL"];
            
            
            [jsonArray addObject:multiDic];
        }
        
        MediaBorrowLimit *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaBorrowLimit"];
        vc.jsonData = jsonArray;
        vc.isMember = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else{
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    }
}

#pragma  mark ---- check Media Limit & Rent Limit -------
-(NSArray*)checkUserAvailableToDownload:(BOOL)isMember{
    //BOOL isMember = [DBManager isMember];
    if (isMember)
        return [NSArray arrayWithArray:[DBManager selectUserDayAndMediaLimit]];
    else
        return [NSArray arrayWithObjects:[NSString stringWithFormat:@"%i",MaximunDateForBorrow],
                [NSString stringWithFormat:@"%i",MaximunFilesDownload],nil];
}


-(void)loadLibraryLogoImage{
    ///load library Image
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSArray *arrOfLib = [DBManager selectLibraryPicAndName:curUsedLibraryID];
        roomName = (arrOfLib !=nil && [arrOfLib count] >0)? arrOfLib[0]: @"Unknown";
        UIImage *iimage = [RoomProfileLogo getRoomProfileLogo];
        if (iimage) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [barButtonLibrary setBackgroundImage:iimage forState:UIControlStateNormal];
            });
        }
        else{
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:(arrOfLib !=nil && [arrOfLib count] >= 2)? arrOfLib[1]: @""]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([Utility isImage:imgData]) {
                    UIImage *img = [Utility rescaleImage:[UIImage imageWithData:imgData] scaledToSize:navigationImageSize];
                    [barButtonLibrary setBackgroundImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                }
            });
        }
    });
}


#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceRoom/RoomsDetail?deviceCode=%@&roomId=%@",hostIpRoomAPI,deviceCode,curUsedLibraryID];
    [service sendHTTPRequest:param NotificationName:@"LibraryDetail" ReturnType:@"JSON" HttpMethod:@"GET"];
    
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    //NSLog(@"libraryDetailResponse : %@",notification.userInfo);
    if ([Utility isServiceResponseError:notification.userInfo]){
        [CustomActivityIndicator dismissActivityIndicatorView];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Library Info to ViewController
        libraryDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = roomName;
        libraryDetailVC.libraryID = curUsedLibraryID;
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        
        /// load items in Library
        [self loadMediaItemsInLibrary];
    }
    else{
        [CustomActivityIndicator dismissActivityIndicatorView];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
    
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/MediaItems?deviceCode=%@&roomId=%@&mediaKind=%@&catId=%@&subId=&sort=%i&loadIndex=%i",hostIpRoomAPI,deviceCode,curUsedLibraryID,@"",@"",0,1];
    
    [service sendHTTPRequest:param NotificationName:@"MediaItemsInLibrary" ReturnType:@"JSON" HttpMethod:@"GET"];
}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = notification.userInfo;
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
}


@end

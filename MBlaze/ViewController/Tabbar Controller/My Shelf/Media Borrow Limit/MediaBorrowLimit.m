//
//  MediaBorrowLimit.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 10/3/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MediaBorrowLimit.h"
#import "Utility.h"
#import "HexColor.h"
#import "MediaBorrowLimitCell.h"
#import "ThemeTemplate.h"
#import "ConstantValues.h"

@interface MediaBorrowLimit ()

@end

@implementation MediaBorrowLimit

@synthesize jsonData;
@synthesize isMember;

#define reuseIdentifierCell @"mediaBorrowLimitCell"
#define headerCellHeight 50.f

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"checkBorrowLimit"]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = CGSizeMake(navigationHeight-25, navigationHeight-25);
    
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:dummyView];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(rightTopBarTapping:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MediaBorrowLimitCell" bundle:nil] forCellReuseIdentifier:reuseIdentifierCell];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

-(void)rightTopBarTapping:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [jsonData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MediaBorrowLimitCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierCell forIndexPath:indexPath];
    
    NSDictionary *dicJson = [jsonData objectAtIndex:indexPath.section];
    
    cell.lblAmount.text = [Utility NSLocalizedString:@"AMOUNT"];
    cell.lblBalance.text = [Utility NSLocalizedString:@"BALANCE"];
    cell.lblTotal.text = [Utility NSLocalizedString:@"LIMITRENT"];
    
    cell.amountLabel.text = [NSString stringWithFormat:@"%@ %@",[dicJson objectForKey:@"AMOUNT"],[Utility NSLocalizedString:@"Books"]];
    
    cell.balanceLabel.text = [NSString stringWithFormat:@"%@ %@",[dicJson objectForKey:@"BALANCE"],[Utility NSLocalizedString:@"Books"]];
    
    cell.rentLimitTotalLabel.text = [NSString stringWithFormat:@"%@ %@",[dicJson objectForKey:@"LIMITRENT"],[Utility NSLocalizedString:@"Books"]];    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (isMember){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, self.tableView.frame.size.width-10, headerCellHeight)];
        headerView.backgroundColor = [UIColor whiteColor];
        
        UIView *headerLineView = [[UIView alloc] initWithFrame:CGRectMake(15, headerCellHeight-1,tableView.frame.size.width-10, 1)];
        headerLineView.backgroundColor = [UIColor blackColor];
        
        NSDictionary *dicJson = [jsonData objectAtIndex:section];
        
        UIImageView *roomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, headerCellHeight-16,  headerCellHeight-16)];
        roomImageView.layer.cornerRadius = roomImageView.frame.size.height/2;
        roomImageView.image = [UIImage imageNamed:@"default_library"];
        roomImageView.layer.shadowOpacity = 0.25;
        roomImageView.layer.shadowOffset = CGSizeMake(2, 2);
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            NSData *imgData  = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dicJson objectForKey:@"SYMBOL"]]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([Utility isImage:imgData]) {
                    roomImageView.image = [UIImage imageWithData:imgData];
                }
            });
        });

        UILabel *headerName = [[UILabel alloc] initWithFrame:CGRectMake(10 + roomImageView.frame.size.width + 10,0, self.tableView.frame.size.width-10, headerCellHeight)];
        headerName.text = [dicJson objectForKey:@"ROOMNAME"];
        headerName.textColor = [UIColor blackColor];
        headerName.font = [UIFont fontWithName:mainFont size:17.f];
        
        [headerView addSubview:roomImageView];
        [headerView addSubview:headerName];
        [headerView addSubview:headerLineView];
        
        return headerView;
    }
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 107.f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (isMember)
        return headerCellHeight;
    return 0.f;
    
}

@end

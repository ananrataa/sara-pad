//
//  BorrowStatusTableViewCell.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class BorrowStatusTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblBorrow: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblBorrowValue: UILabel!
    @IBOutlet weak var lblBalanceValue: UILabel!
    @IBOutlet weak var lblRoomName: UILabel!
    
    var status: SPDeviceLimitRent? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func refreshView() {
        lblTotal.text = String.localizedString(key: "LIMITRENT")
        lblBorrow.text = String.localizedString(key: "AMOUNT")
        lblBalance.text = String.localizedString(key: "BALANCE")
        if let status = status {
            lblRoomName.text = status.roomName
            lblTotalValue.text = "\(status.limitRent)"
            lblBorrowValue.text = "\(status.amount)"
            lblBalanceValue.text = "\(status.balance)"
        } else {
            lblRoomName.text = ""
            lblTotal.text = "-"
            lblBorrow.text = "-"
            lblBalance.text = "-"
        }
    }
}

//
//  BorrowStatusViewController.swift
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 31/7/18.
//  Copyright © 2018 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import RxSwift

@available(iOS 11.0, *)
class BorrowStatusViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let cellId = "status_cell"
    let disposeBag = DisposeBag()
    var roomsList: [SPDeviceLimitRent] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        self.title = String.localizedString(key: "checkBorrowLimit")
        TransparentTheme.shared.applyNavigationBar(vc: self)
        navigationController?.navigationBar.barStyle = .black
        tableView.register(UINib.init(nibName: "BorrowStatusTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        MediaModel.shared.createDeviceLimitRentObservable(items: 0, days: 0)
            .subscribe(onNext: {[weak self] (items) in
                DispatchQueue.main.sync {
                    self?.roomsList = items
                    self?.tableView.reloadData()
                }
                SVProgressHUD.dismiss()
            }).disposed(by: disposeBag)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        LightTheme.shared.applyNavigationBar(vc: self)
        navigationController?.navigationBar.barStyle = .default
    }
}

@available(iOS 11.0, *)
extension BorrowStatusViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomsList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 174
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BorrowStatusTableViewCell
        
        cell.status = roomsList[indexPath.row]
        return cell
    }
    
}

//
//  Tabbar2.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "MyShelfBooks.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "DBManager.h"
#import "ConstantValues.h"
#import "SVProgressHUD.h"

#import "OfflineMediaCollectionCell.h"
#import "CollectionHeaderView2.h"
#import "CollectionFooterReusableView.h"
#import "HistoryTVC.h"
#import "SignInVC.h"
#import "OfflineShelf.h"
#import "HexColor.h"
#import "MemberInfo.h"
#import "SettingTCV.h"
#import "MediaBorrowLimit.h"
#import "Help.h"
#import "AboutTVC.h"
#import <PDFKit/PDFKit.h>
#import "RoomProfileLogo.h"
#import "ShelfManagement.h"
#import "ThemeTemplate.h"
#import "MediaPreset.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"
#import "SVProgressHUD.h"

@interface MyShelfBooks ()

@end

@implementation MyShelfBooks

#define cellMediaIdentifier  @"mediaOfflineContentCell" //@"ccell"//
#define cellHeaderIdentifier @"collectionViewHeader2"
#define cellFooterIdentifier @"CollectionViewFooter"
#define headerHeight 40.0f
#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"TabbarShelf"]];
    [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};

    /// calculate collection Cell Width & Height
    collectionCellWidth = IPAD?((self.collectionView.frame.size.width-32)/3):((self.collectionView.frame.size.width-24)/2);
    collectionCellHeight =  ((collectionCellWidth*4.f)/3.f)+95.f;
    
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
    barButtonLibrary = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navigationImageSize.width, navigationImageSize.height)];
    [barButtonLibrary setBackgroundImage:[Utility rescaleImage:[UIImage imageNamed:@"default_library"] scaledToSize:navigationImageSize] forState:UIControlStateNormal];
    [barButtonLibrary addTarget:self action:@selector(libraryImageTapping:) forControlEvents:UIControlEventTouchUpInside];
    barButtonLibrary.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    barButtonLibrary.clipsToBounds = YES;
    barButtonLibrary.layer.cornerRadius = navigationImageSize.height/2;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    UIBarButtonItem *libraryLogo = [[UIBarButtonItem alloc] initWithCustomView:barButtonLibrary];
    self.navigationItem.leftBarButtonItems = @[leftBarButton,libraryLogo];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings"] style:UIBarButtonItemStyleDone target:self action:@selector(shelfSettingsTapping:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Register Header cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionHeaderView2" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderIdentifier];
    // Register Item cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"OfflineMediaCollectionCell" bundle:nil] forCellWithReuseIdentifier:cellMediaIdentifier];
    //Register Footer cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionFooterReusableView" bundle:nil]  forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:cellFooterIdentifier];
    
    [self.collectionView setContentInset:UIEdgeInsetsMake(50, 0, 0, 0)];
    
    /////change collectionView Layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(8.f,8.f,8.f,8.f);
    layout.itemSize = CGSizeMake(collectionCellWidth, collectionCellHeight);
    layout.minimumInteritemSpacing = 4.f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:layout];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    //// select current Library id ////
    curUsedLibraryID = [userDefault objectForKey:KeyCuurrentUsedLribaryID];
    deviceCode= [userDefault objectForKey:KeyDeviceCode];
    api = [[ServiceAPI alloc] init];
    
    memberID = deviceCode;
    isMember = ([MemberInfo isSignin] || [OfflineShelf isOnlyOfflineModeEnable]) && [DBManager isMember];
    if (isMember)
        memberID = [DBManager selectMemberID];
    
    arrayOfMedia = [[NSMutableArray alloc] init];
    arrayofLibrary = [[NSMutableArray alloc] init];
    arrayshelfId = [[NSMutableArray alloc] init];
    dictPresetInfoFootter = [[NSMutableDictionary alloc] init];
    dictPresetShowMore = [[NSMutableDictionary alloc] init];
    
    isSelectFirstTime = YES;
    currentShelfId = PublicShelfId;
    
    if (!([[userDefault objectForKey:LastOpenShelf] length] > 0)) {
        [userDefault setValue:PublicShelfId forKey:LastOpenShelf];
        [userDefault synchronize];
        currentShelfId = PublicShelfId;
    }
    else if (isMember){
        if([DBManager isMediaPresetRoomExist:[userDefault objectForKey:LastOpenShelf]])
            currentShelfId = [userDefault objectForKey:LastOpenShelf];
        else
            currentShelfId = PublicShelfId;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    if (!([[userDefault objectForKey:KeyCuurrentUsedLribaryID] isEqualToString:curUsedLibraryID])) {
        [self viewDidLoad];
    }
    [self.tabBarController.tabBar setHidden:NO];
    if([OfflineShelf isOnlyOfflineModeEnable]){
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    else{
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangingLibrary:) name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchHistory:) name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signOut:) name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting:) name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp:) name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signIn:) name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAbout:) name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memberProfile) name:@"MemberProfile" object:nil];
    [self loadLibraryLogoImage];
    [self loadMediaOnShelf];
}

-(void)loadMediaOnShelf{
    [SVProgressHUD show];
    //sync Media on Shelf
    [self syncMediaOnCloud:memberID];
    [Utility checkToRemoveExpiredMediaOnShelf];
    [self dismissOpenReturnViewForPreviousCell];
    
    [arrayshelfId removeAllObjects];
    [arrayOfMedia removeAllObjects];
    [arrayofLibrary removeAllObjects];
    [dictPresetInfoFootter removeAllObjects];
    [dictPresetShowMore removeAllObjects];
    currentSelectCell = 0;
    currentSelectSection = 0;
    isSelectFirstTime = YES;
    
    [self getShelf];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WatchHistory" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SignOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Setting" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Signin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Help" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"About" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberProfile" object:nil];
}

-(void)leftTopBarTapping:(id)sender{
    //    NSLog(@"------------- leftTopBarTapping --------------");
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)libraryImageTapping:(UIButton *)sender{
    [SVProgressHUD show];
    [self loadLibraryInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getShelf{
    BOOL isSignin = [DBManager isMember];
    [arrayshelfId addObject:@[PublicShelfId,[Utility NSLocalizedString:@"userShelf"]]];
    if (isSignin){
        [arrayshelfId addObject:@[MemberShelfId,[Utility NSLocalizedString:@"memberShelf"]]];
        
        NSMutableArray *presetItems = [DBManager selectPresetRoomItems:memberID];
        for (NSArray*preset in presetItems) {
            [arrayshelfId addObject:@[preset[1],preset[2]]];
        }
        
        //Load SaleShelf
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadSaleShelfResponse:) name:@"SaleShelf" object:nil];
        [api saleShelf:@"SaleShelf" MemberID:memberID];
    } else {
        [self.view addSubview:[self createShelfHeader]];
        [self getMediaByShelfId:currentShelfId];
    }
    
}

-(void)loadSaleShelfResponse:(NSNotification *)notification {
    if (![Utility isServiceResponseError:notification.userInfo]) {
        NSArray *json = [NSArray arrayWithArray:notification.userInfo];
        SPSaleShelf *saleShelves = [[SPSaleShelf alloc] initWithJson:json];
        [arrayshelfId addObject:@[SaleShelfId, @"Buy/Rent"]];
    }
    [self.view addSubview:[self createShelfHeader]];
    [self getMediaByShelfId:currentShelfId];
}

-(void)getMediaByShelfId:(NSString *)shelfId{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        
        if(![shelfId isEqualToString:MemberShelfId] && ![shelfId isEqualToString:PublicShelfId]){
            //for preset shelfId = room id
            arrayofLibrary = [DBManager selectPresetSubjectItems:shelfId];
            arrayOfMedia = [DBManager selectPresetMediaOnMyShelfId:shelfId MemberID:memberID];
            dictPresetInfoFootter = [DBManager selectMediaPresetInfoByRoomId:shelfId];
            for (NSString *key in [dictPresetInfoFootter allKeys]) {
                [dictPresetShowMore setValue:@"NO" forKey:key];
            }
        }
        else{
            
            arrayofLibrary = [DBManager selectLibraryNameOnMyShelfId:shelfId MemberID:memberID];
            arrayOfMedia = [DBManager selectMediaOnMyShelfId:shelfId MemberID:memberID];
        }
        
        //footterHeight = 250.f;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            [self.collectionView scrollsToTop];
            
            [SVProgressHUD dismiss];
            
        });
    });
}

-(UIView*)createShelfHeader{
    [[self.view viewWithTag:809] removeFromSuperview];
    CGRect frameCGRect = CGRectMake(0, 0, self.view.frame.size.width, 50);
    UIView *headerView = [[UIView alloc] initWithFrame:frameCGRect];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    headerView.tag = 809;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(8.0f, 8.0f, frameCGRect.size.width-16, 30.0f)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    float margin = 0.0f;
    float btn_height = 30.0f;
    float btn_width;
    
    for (NSArray *shelf in arrayshelfId){
        //NSLog(@"shelf name : %@",shelf[0]);
        CGSize TextWidth = [Utility getWidthOfNSString:shelf[1] FontSize:[UIFont fontWithName:mainFont size:[Utility fontSize:17.f]]];
        btn_width = TextWidth.width + 20;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(margin, 0.0f, btn_width, btn_height)];
        [btn setTintColor:[UIColor whiteColor]];
        [btn setTitle:shelf[1] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:17.f]];
        btn.backgroundColor  = [UIColor lightGrayColor];
        btn.accessibilityLabel = shelf[0];
        btn.tag = ((int)margin/3);
        [btn addTarget:self action:@selector(changeShelfTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        btn.clipsToBounds = YES;
        btn.layer.cornerRadius = 15.0f;
        
        if ([shelf[0] isEqualToString:currentShelfId]){
            btn.backgroundColor  = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
            [btn setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
            // xScrollViewContentOffset = margin;
        }
        [scrollView addSubview:btn];
        
        margin = margin + 5.0f + btn_width;
    }
    //CGRectMake(0.0f, 0.0f, headerView.frame.size.width, btn_height);
    [scrollView setContentSize:CGSizeMake(margin, btn_height)];
    
    //scrollView.contentOffset = CGPointMake(xScrollViewContentOffset,0);
    [headerView addSubview:scrollView];
    
    return headerView;
}

-(void)changeShelfTapping:(UIButton*)sender{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectCell inSection:currentSelectSection];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    if(!([currentShelfId isEqualToString:sender.accessibilityLabel])){
        [SVProgressHUD show];
        UIView *headerView = [self.view viewWithTag:809];
        UIScrollView *scrollView = (UIScrollView*)[headerView.subviews firstObject];
        for (UIButton *btn in scrollView.subviews) {
            btn.backgroundColor  = [UIColor lightGrayColor];
            [btn setTintColor:[UIColor whiteColor]];
        }
        sender.backgroundColor  = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
        [sender setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
        currentShelfId = sender.accessibilityLabel;
        
        [userDefault setValue:currentShelfId forKey:LastOpenShelf];
        [userDefault synchronize];
        
        [self dismissOpenReturnViewForPreviousCell];
        [self getMediaByShelfId:currentShelfId];
    }
}

-(void)shelfSettingsTapping:(id)sender{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    ShelfManagement *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ShelfManagement"];
    [vc setMemberID:memberID];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [arrayofLibrary count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([arrayofLibrary count] > 0)
        return [self getNumberOfItemInLibrary:[arrayofLibrary objectAtIndex:section]];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    ///cover
    NSString *path = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaCoverPath]];
    
    [cell.mediaCoverImageView setImage:[Utility loadImageWithFilePath:path defaultImage:[UIImage imageNamed:@"default_media_cover.png"]]];
    cell.mediaCoverImageView.userInteractionEnabled = YES;
//    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
//    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
//    cell.mediaCoverImageView.clipsToBounds = YES;
//    cell.mediaCoverImageView.layer.borderWidth = 0.1f;
//    cell.mediaCoverImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.mediaTitleLabel.text = [myShelfData getMediaTitle];
    
    cell.mediaAuthorLabel.text = [myShelfData getMediaAuthor];
    cell.shareButton.alpha = 0;
    [cell.shareButton addTarget:self action:@selector(shareAnnotation:) forControlEvents:UIControlEventTouchUpInside];
    //check exp date
    NSString *expDate = [myShelfData getDateExp];
    if (![expDate isEqualToString:MediaPresetToRemove] && ![expDate isEqualToString:MediaPresetNoExpireDate]){
        expDate = [Utility covertDateToFormat:[myShelfData getDateExp] InputFormat:@"yyyyMMdd" OutputFormat:@"dd/MM/yyyy"];
    }
    else if ([expDate isEqualToString:MediaPresetNoExpireDate]){
        expDate = @"-";
    }
    
    cell.mediaExpLabel.text = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Exp"],expDate];
    
    [Utility setFont: cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    [Utility setFont:cell.mediaExpLabel.font WithFontSize:[Utility fontSize:12.f]];
    
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[myShelfData getMediaType]];
    
    cell.videoImagePic.hidden = YES;

    NSString *fileContentPath = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaFilePath]];
    if ([[myShelfData getMediaFilePath] length] > 0 && [[NSFileManager defaultManager] fileExistsAtPath:fileContentPath]){
        cell.contentView.alpha = 1.0f;
        cell.accessibilityValue = @"YES";
    }
    else{
        cell.contentView.alpha = 0.5f;
        cell.accessibilityValue = @"NO";
    }
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"collectionView didSelectItemAtIndexPath");
    //NSLog(@"------------ index : %li --------------",(long)indexPath.row );
    
     OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    //dont forget edcode media id (only Preset Shelf)
    //Preset Shelf room id = shelfid
    
    BOOL isFileExist = [cell.accessibilityValue isEqualToString:@"YES"]? YES : NO;
    
    UIImageView *cover = (UIImageView*)cell.mediaCoverImageView;
    
    for (UIView *subViews in [cover subviews]) {
        [subViews removeFromSuperview];
    }
    
    if (!isSelectFirstTime) {
        [self dismissOpenReturnViewForPreviousCell];
    }
    
    isSelectFirstTime = NO;
    
    currentSelectSection = (int)indexPath.section;
    currentSelectCell = (int)indexPath.row;
    
    CGRect actionViewRect = cover.frame;
    UIView *actionView = [[UIView alloc] initWithFrame:actionViewRect];
    actionView.backgroundColor = [[HexColor SKColorHexString:@"0E0F20"] colorWithAlphaComponent:0.9f];
    actionView.tag = indexPath.row;
    
    [cover addSubview:actionView];
    
    float buttonHeight = 40.f;
    float buttonWidth = (actionViewRect.size.width-60) ;
    float buttonYpoint = (actionViewRect.size.height/2) - 15 - buttonHeight;
    
    UIButton *openBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, buttonYpoint, buttonWidth, buttonHeight)];
    [openBtn setBackgroundColor:[HexColor SKColorHexString:@"D44C27"]];
    [openBtn setTitle:[Utility NSLocalizedString: isFileExist? @"Open" : @"Download"] forState:UIControlStateNormal];
    openBtn.clipsToBounds = YES;
    openBtn.layer.cornerRadius = 12.f;
    [openBtn.titleLabel setFont:[UIFont fontWithName:mainFont size:17]];
    
    
    if ([currentShelfId isEqualToString:PublicShelfId] || [currentShelfId isEqualToString:MemberShelfId]) {
        openBtn.accessibilityValue = [myShelfData getMediaID];
        openBtn.accessibilityLabel = [myShelfData getLibraryID];
    }
    else{
        //for Preset shelf
        openBtn.accessibilityValue = [Utility decodeMediaIDPreset:[myShelfData getMediaID]];
        openBtn.accessibilityLabel = [myShelfData getShelfID];
    }
    
    [openBtn removeTarget:self action:@selector(prepare4OpenMedia:) forControlEvents:UIControlEventTouchUpInside];
    [openBtn removeTarget:self action:@selector(prepare4DownloadMedia:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isFileExist)
        [openBtn addTarget:self action:@selector(prepare4OpenMedia:) forControlEvents:UIControlEventTouchUpInside];
    else
        [openBtn addTarget:self action:@selector(prepare4DownloadMedia:) forControlEvents:UIControlEventTouchUpInside];
    
    float buttonYpoint1 =  (actionViewRect.size.height/2) + 15 ;
    UIButton *returnedBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, buttonYpoint1, buttonWidth, buttonHeight)];
    NSString *title = ([currentShelfId isEqualToString:PublicShelfId] || [currentShelfId isEqualToString:MemberShelfId])? @"Return":@"RemoveFile";
    [returnedBtn setTitle:[Utility NSLocalizedString:title] forState:UIControlStateNormal];
    returnedBtn.titleLabel.textColor = [UIColor whiteColor];
    [returnedBtn addTarget:self action:@selector(prepare4ReturnMedia:) forControlEvents:UIControlEventTouchUpInside];
    returnedBtn.clipsToBounds = YES;
    returnedBtn.layer.cornerRadius = 12.f;
    returnedBtn.layer.borderWidth = 1.f;
    returnedBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [returnedBtn.titleLabel setFont:[UIFont fontWithName:mainFont size:17]];
    returnedBtn.enabled = YES;
    [Utility setFont:returnedBtn.titleLabel.font WithFontSize:[Utility fontSize:17.f]];
    
    if([OfflineShelf isOnlyOfflineModeEnable]){
        returnedBtn.enabled = NO;
        returnedBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        returnedBtn.titleLabel.textColor = [UIColor lightGrayColor];
    }
    
    [actionView addSubview:openBtn];
    [actionView addSubview:returnedBtn];
    
    cell.contentView.alpha = 1.0;
    
    cell.shareButton.alpha = 1;
    //[collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    // UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        CollectionHeaderView2 *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellHeaderIdentifier forIndexPath:indexPath];
        if (indexPath.section==0)
            headerView.frame = CGRectMake(0.f,0.f, headerView.frame.size.width, headerView.frame.size.height);
        headerView.backgroundColor = [UIColor whiteColor];
        
        NSArray *header = [arrayofLibrary objectAtIndex:indexPath.section];
        // NSLog(@"Header Name : %@",header[1]);
        
        NSString *headerName = [header count]>=3? header[2]:@"";
        CGSize labelWidth = [Utility getWidthOfNSString:headerName FontSize:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:18.f]]];
        
        UILabel *headerLabel = (UILabel*)[headerView.subviews firstObject];
        UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x, headerLabel.frame.origin.y,labelWidth.width, headerLabel.frame.size.height)];
        headerLabel1.text = headerName;
        headerLabel1.textColor = [UIColor blackColor];
        headerLabel1.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:18.f]];
        
        UIButton *seeMoreBtn = (UIButton*)[headerView.subviews lastObject];
        seeMoreBtn.hidden = YES;
        
        //add scrollview
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width-20, headerView.frame.size.height)];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        [scrollView setShowsVerticalScrollIndicator:NO];
        [scrollView setAlwaysBounceVertical:NO];
        [scrollView addSubview:headerLabel1];
        scrollView.contentSize = headerLabel1.frame.size;
        
        [headerView addSubview:scrollView];
        
        return headerView;
    }
    else if (kind == UICollectionElementKindSectionFooter){
        if(![currentShelfId isEqualToString:MemberShelfId] && ![currentShelfId isEqualToString:PublicShelfId]){
            
            CollectionFooterReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:cellFooterIdentifier forIndexPath:indexPath];
            footerView.clipsToBounds = YES;
    
            NSArray *sectionInfo = [arrayofLibrary objectAtIndex:indexPath.section];
            MediaPreset *presetInfo = (MediaPreset *)[dictPresetInfoFootter objectForKey:sectionInfo[1]];
            
            UILabel *subjectNameTitle = (UILabel*)[footerView viewWithTag:101];
            UILabel *subjectNameBody = (UILabel*)[footerView viewWithTag:102];
            subjectNameTitle.text = [Utility NSLocalizedString:@"Subject"];
            subjectNameBody.text = [[presetInfo subjectName] length]>0? [presetInfo subjectName]:@"-";
            subjectNameBody.numberOfLines = 5;
            
            UILabel *subjectCodeTitle = (UILabel*)[footerView viewWithTag:201];
            UILabel *subjectCodeBody = (UILabel*)[footerView viewWithTag:202];
            subjectCodeTitle.text = [Utility NSLocalizedString:@"Code"];
            subjectCodeBody.text = [[presetInfo subjectCode] length]>0? [presetInfo subjectCode]:@"-";
            
            UILabel *sectionTitle = (UILabel*)[footerView viewWithTag:301];
            UILabel *sectionBody = (UILabel*)[footerView viewWithTag:302];
            sectionTitle.text = [Utility NSLocalizedString:@"Section"];
            sectionBody.text = [[presetInfo sectionCode] length]>0? [presetInfo sectionCode]:@"-";
            
            UILabel *semesterTitle = (UILabel*)[footerView viewWithTag:401];
            UILabel *semesterBody = (UILabel*)[footerView viewWithTag:402];
            semesterTitle.text = [Utility NSLocalizedString:@"SemesterYear"];
            semesterBody.text = [NSString stringWithFormat:@"%@/%@",[[presetInfo semester]length]>0? [presetInfo semester]:@"-",[[presetInfo year]length]>0? [presetInfo year]:@"-"];
            
            NSString *despText = [[presetInfo desp]length]>0? [presetInfo desp]:@"-﻿";
            float despBodyHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Description"],despText]];
        
            BOOL isShowMoreDesp = [[dictPresetShowMore objectForKey:[presetInfo subjectID]] boolValue];
           
            UILabel *despTitle = (UILabel*)[footerView viewWithTag:501];
            despTitle.text = [NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Description"],despText];
            despTitle.numberOfLines = (despBodyHeight > 70 && isShowMoreDesp)? 20:3;
            
            UIButton *seemore = (UIButton*)[footerView viewWithTag:601];
            seemore.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
            [seemore setImage:[UIImage imageNamed:isShowMoreDesp? @"up-arrow":@"down-arrow"] forState:UIControlStateNormal];
            seemore.hidden = (despBodyHeight > 70)? NO:YES;
            [seemore removeTarget:self action:@selector(seemoreDespTapping:) forControlEvents:UIControlEventTouchUpInside];
            [seemore addTarget:self action:@selector(seemoreDespTapping:) forControlEvents:UIControlEventTouchUpInside];
            
            return footerView;
        }
        else
            return nil;
        
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width, headerHeight);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if(![currentShelfId isEqualToString:MemberShelfId] && ![currentShelfId isEqualToString:PublicShelfId]){
        float footterHeight = 242;
        NSArray *sectionInfo = [arrayofLibrary objectAtIndex:section];
        MediaPreset *presetInfo = (MediaPreset *)[dictPresetInfoFootter objectForKey:sectionInfo[1]];
        BOOL isShowMoreDesp = [[dictPresetShowMore objectForKey:[presetInfo subjectID]] boolValue];
        if ([[presetInfo desp]length]>0 && isShowMoreDesp) {
            float despBodyHeight = [self calculateTextHeight:[NSString stringWithFormat:@"%@ %@",[Utility NSLocalizedString:@"Description"],[presetInfo desp]]];
            footterHeight = footterHeight - (21*3) + despBodyHeight + 25;
        }
        //calculate subject title
        float subjectTitleHeight = [self calculateTextHeight:[presetInfo subjectName]] - 21;
        
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width, footterHeight+subjectTitleHeight);
    }
    
    return CGSizeMake(0, 0);
}

-(void)seemoreDespTapping:(UIButton *)sender{
    NSArray *sectionInfo = [arrayofLibrary objectAtIndex:[sender.accessibilityValue integerValue]];
    MediaPreset *presetInfo = (MediaPreset *)[dictPresetInfoFootter objectForKey:sectionInfo[1]];
    BOOL isShowMoreDesp = [[dictPresetShowMore objectForKey:[presetInfo subjectID]] boolValue];
    if(isShowMoreDesp){
        // go collapse
        [dictPresetShowMore removeObjectForKey:[presetInfo subjectID]];
        [dictPresetShowMore setValue:@"NO" forKey:[presetInfo subjectID]];
        [sender setImage:[UIImage imageNamed:@"down-arrow"] forState:UIControlStateNormal];
    }
    else{
        // go expande
        [dictPresetShowMore removeObjectForKey:[presetInfo subjectID]];
        [dictPresetShowMore setValue:@"YES" forKey:[presetInfo subjectID]];
        [sender setImage:[UIImage imageNamed:@"up-arrow"] forState:UIControlStateNormal];
    }

     [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[sender.accessibilityValue integerValue]]];
    
}


- (float) calculateTextHeight:(NSString *) chatText {
    chatText = [chatText length] > 0 ? chatText:@"XOXOXO";
    CGRect textRect = [chatText boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]]} context:nil];
    return textRect.size.height;
}

#pragma mark - UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self dismissOpenReturnViewForPreviousCell];
}

#pragma mark ------- dismiss Open & Return View for All Cell -----
-(void)dismissOpenReturnViewForPreviousCell{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectCell inSection:currentSelectSection];
    OfflineMediaCollectionCell *cell = (OfflineMediaCollectionCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.alpha = 0.5f;
    if ([cell.accessibilityValue isEqualToString:@"YES"])
        cell.contentView.alpha = 1.0f;
    //[self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    UIImageView *cover = (UIImageView*)cell.mediaCoverImageView;
    for (UIView *subViews in [cover subviews]) {
        [subViews removeFromSuperview];
    }
    
    for (NSIndexPath *indexPath in [self.collectionView indexPathsForSelectedItems]) {
        [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
    
    cell.shareButton.alpha = 0;
}

#pragma mark -------- Return Media by User ----------
-(void)prepare4ReturnMedia:(UIButton*)sender{
    if ([currentShelfId isEqualToString:PublicShelfId] || [currentShelfId isEqualToString:MemberShelfId])
        [self showAlertControllerMediaReturn];
    
    else
        [self showAlertControllerRemoveMediaPreset];
}

#pragma mark ---- download media ----
-(void)prepare4DownloadMedia:(UIButton*)sender{
    [SVProgressHUD show];
    [self loadMediaDetail:sender.accessibilityLabel MediaID:sender.accessibilityValue];
}

#pragma mark ------- Open Media --------
-(void)prepare4OpenMedia:(UIButton*)sender{
    
    [SVProgressHUD show];
    
    double delayInSeconds = 0.5; // set the time
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
        
        filePath  = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaFilePath]];
        //NSLog(@"Read PDF at path : %@" ,filePath);
        //filePath =  [myShelfData getMediaFilePath];
        if ([currentShelfId isEqualToString:PublicShelfId] || [currentShelfId isEqualToString:MemberShelfId]) {
            [self prepare4ReadPDF:[myShelfData getMediaOriginalName] UserID:[myShelfData getMemberID] MediaID:[myShelfData getMediaID] RoomID:[myShelfData getLibraryID]];
        }
        else{
            //for Preset shelf
            [self prepare4ReadPDF:[myShelfData getMediaOriginalName] UserID:[myShelfData getMemberID] MediaID:[Utility decodeMediaIDPreset:[myShelfData getMediaID]] RoomID:[myShelfData getShelfID]];
        }
    });
}


#pragma mark ---------- See more Tapping ----------
-(void)seeMoreTapping:(UIButton*)sender{
    // NSLog(@"---------- See more Tapping ----------");
}

#pragma mark ---------- Notification from Left Menu ------------
-(void)userChangingLibrary:(NSNotification *)notification{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"ChangeLibrary" object:nil];
    [self.tabBarController setSelectedIndex:0];
}

-(void)watchHistory:(NSNotification*)notification{
    HistoryViewController *vc = [[HistoryViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)signOut:(NSNotification*)notification{
     [Utility showAlertControllerForSignout:self.view.window.rootViewController];
}

-(void)signIn:(NSNotification*)notification{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SignInVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)setting:(NSNotification *)notification{
    SettingViewController *vc = [[SettingViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:NULL];
}

-(void)openHelp:(NSNotification *)notification{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    Help *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Help"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAbout:(NSNotification *)notification{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    AboutTVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AboutTVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSInteger)getNumberOfItemInLibrary:(NSArray*)libArray{
    NSString *attributeName  = @"getLibraryID";
    NSString *attributeValue = libArray[1];
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K MATCHES %@",attributeName, attributeValue];
    return [[arrayOfMedia filteredArrayUsingPredicate:predicate] count];
}

-(NSMutableArray*)getItemInLibrary:(NSArray*)libArray{
    NSString *attributeName  = @"getLibraryID";
    NSString *attributeValue = libArray[1];
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K MATCHES %@",attributeName, attributeValue];
    NSMutableArray *arrayOfItem = [[NSMutableArray alloc] init];
    arrayOfItem = [[arrayOfMedia filteredArrayUsingPredicate:predicate] mutableCopy];
    return arrayOfItem;
}
-(void)memberProfile {
//    MemberProfileViewController *vc = [[MemberProfileViewController alloc] initWithMemberId:[DBManager selectMemberID]];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    [self presentViewController:nav animated:YES completion:NULL];
}

#pragma -------- Prepare for Read PDF File --------
-(void)prepare4ReadPDF:(NSString*)origiFileName UserID:(NSString*)userID MediaID:(NSString*)mediaId RoomID:(NSString *)roomID{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] &&
        [[[filePath lastPathComponent] pathExtension] caseInsensitiveCompare:@"pdf"] == NSOrderedSame) {
        
        [[SP shared] loadPdfAdsWithRoomId:roomID mediaId:mediaId completion:^(ADPdf *ads) {
            if (ads != nil) {
                NSURL *url = [NSURL fileURLWithPath:filePath];
                PDFDocument *pdfDoc = [[PDFDocument alloc] initWithURL:url];
                NSString *pdfPassword = nil;
                if([pdfDoc isLocked]){
                    pdfPassword = [Utility generatePassword:origiFileName];
                    [pdfDoc unlockWithPassword:pdfPassword];
                }
                //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissReaderViewController:) name:@"dismissReaderViewController" object:nil];
                
                BOOL isAllowToUploadAnnatationFile = ((currentShelfId != PublicShelfId) && [userDefault boolForKey:AutoSyncPdfAnnatationKey] && ![deviceCode isEqualToString:userID])? YES : NO;
                
                NSString *filePath = [NSString stringWithFormat:@"%@/Annotation_%@_%@_%@.txt",PDFAnnotationFolderName,userID,mediaId,roomID];
                NSString *strUrl = [NSString stringWithFormat:@"%@api/DeviceMedia/UploadAnnotation",hostIpRoomAPI];
                PdfAnnotationInfo *info = [[PdfAnnotationInfo alloc] initWithRoomId:roomID
                                                                            mediaId:mediaId
                                                                        pdfDocument:pdfDoc
                                                                           filePath:filePath
                                                                         uploadPath:strUrl
                                                                   removeFileOnExit:false
                                                                          canUpload:isAllowToUploadAnnatationFile
                                                                          isPreview:false
                                                                             pdfURL:url
                                                                           password:pdfPassword
                                                                                ads:ads];
                PdfViewController *pdfVC = [[PdfViewController alloc] initWithInfo: info];
                UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:pdfVC];
                [self presentViewController:nv animated:YES completion:nil];
            } else {
                [SVProgressHUD showInfoWithStatus:@"Cannot open file."];
            }
        }];
    }
    else{
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:[Utility NSLocalizedString:@"UnOpenFile"] Controller:(UIViewController*)self];
    }
    
}

#pragma mark - ReaderViewControllerDelegate methods
-(void)dismissReaderViewController:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissReaderViewController" object:nil];
    [self dismissOpenReturnViewForPreviousCell];
   
    /*if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (error)
            NSLog(@"Failed to remove item at path: %@", filePath);
        //else
        //NSLog(@"Success to remove item at path: %@", filePath);
    } */
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 11.0) {
        self.tabBarController.delegate = (id) self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)loadLibraryLogoImage{
    ///load library Image
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSArray *arrOfLib = [DBManager selectLibraryPicAndName:curUsedLibraryID];
        roomName = (arrOfLib !=nil && [arrOfLib count] >0)? arrOfLib[0]: @"Unknown";
        UIImage *iimage = [RoomProfileLogo getRoomProfileLogo];
        if (iimage) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [barButtonLibrary setBackgroundImage:iimage forState:UIControlStateNormal];
            });
        }
        else{
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:(arrOfLib !=nil && [arrOfLib count] >= 2)? arrOfLib[1]: @""]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([Utility isImage:imgData]) {
                    UIImage *img = [Utility rescaleImage:[UIImage imageWithData:imgData] scaledToSize:navigationImageSize];
                    [barButtonLibrary setBackgroundImage:img forState:UIControlStateNormal];
                    [RoomProfileLogo setRoomProfileLogo:img];
                }
            });
        }
    });
}


#pragma mark --------- Load Library Info ------
-(void)loadLibraryInfo{
    //api/DeviceRoom/RoomsDetail?deviceCode={deviceCode}&roomId={roomId}
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLibraryInfoResponse:) name:@"LibraryDetail" object:nil];
    [api roomsDetail:@"LibraryDetail" RoomID:curUsedLibraryID memberID:memberID];
}

-(void)loadLibraryInfoResponse:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LibraryDetail" object:nil];
    //NSLog(@"libraryDetailResponse : %@",notification.userInfo);
    if ([Utility isServiceResponseError:notification.userInfo]){
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    }
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Library Info to ViewController
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        libraryDetailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LibraryDetailVC"];
        libraryDetailVC.libraryTitle = roomName;
        libraryDetailVC.libraryID = curUsedLibraryID;
        libraryDetailVC.jsonLibraryInfo = notification.userInfo;
        
        /// load items in Library
        [self loadMediaItemsInLibrary];
    }
    else{
        [SVProgressHUD dismiss];
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
    }
    
}

#pragma mark ------- load Media Items in Library ---------
-(void)loadMediaItemsInLibrary{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMediaItemsInLibraryResponse:) name:@"MediaItemsInLibrary" object:nil];
    [api mediaItems:@"MediaItemsInLibrary" RoomId:curUsedLibraryID CategoryID:@"" SubCatrgory:@"" MediaKind:@"" SortBy:0 LoadIndex:1 AppVersion:CurrentAppVersion];
}
-(void)loadMediaItemsInLibraryResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemsInLibrary" object:nil];
    // NSLog(@"loadMediaItemsResponse : %@",notification.userInfo);
    
    [SVProgressHUD dismiss];
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        ///set Media Items
        libraryDetailVC.jsonMediaItems = notification.userInfo;
        [self.navigationController pushViewController:libraryDetailVC animated:YES];
    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:self];
}

#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)libraryID MediaID:(NSString*)mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    BOOL isPreset = ![currentShelfId isEqualToString:PublicShelfId] && ![currentShelfId isEqualToString:MemberShelfId]? YES:NO;
    
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:libraryID LimtItem:(int)[userLimitData[1] intValue] IsPreset:isPreset];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",[NSString stringWithFormat:@"%@",[notification.userInfo description]]);
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
//        MediaDetailTVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        //vc.libraryID = curUsedLibraryID;
//        //vc.libraryName = [DBManager selectLibraryNameByID:curUsedLibraryID];
//        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        vc.jsonData = notification.userInfo;
//        vc.rootNavigationController = self;
//
//        if (![currentShelfId isEqualToString:PublicShelfId] && ![currentShelfId isEqualToString:MemberShelfId]) {
//            OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
//            vc.isMediaPreset = YES;
//            vc.subjectID = [myShelfData getSubjectID];
//        }
//
//
//        [self.navigationController pushViewController:vc animated:YES];
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}

-(void)showAlertControllerRemoveMediaPreset{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:[Utility NSLocalizedString:@"RemoveMediaPreset"]
                                                                  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* removeMedia = [UIAlertAction
                                  actionWithTitle:[Utility NSLocalizedString:@"RemoveFile"]
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self removeMediaFileFromShelf];
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
    
    [alert addAction:removeMedia];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showAlertControllerMediaReturn{
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:[Utility NSLocalizedString:@"OptionReturn"]
                                                                  message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* returnMedia = [UIAlertAction
                                  actionWithTitle:[Utility NSLocalizedString:@"Return"]
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self returnMediaFileFromShelf];
                                      
                                  }];
    
    UIAlertAction* removeMedia = [UIAlertAction
                                  actionWithTitle:[Utility NSLocalizedString:@"RemoveFile"]
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                     
                                      [self removeMediaFileFromShelf];
                                      
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
    
    [alert addAction:returnMedia];
    [alert addAction:removeMedia];
    [alert addAction:cancelAction];
    
    if(IPAD){
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentSelectCell inSection:currentSelectSection];
        UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
        alert.popoverPresentationController.sourceView = self.collectionView;
        alert.popoverPresentationController.sourceRect = [attributes frame];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark ------- Remove Media file ------------
-(void)removeMediaFileFromShelf{
    [SVProgressHUD show];
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
    if ([[myShelfData getMediaFilePath] length] > 0) {
        NSString * path = [[Utility documentPath] stringByAppendingPathComponent:[myShelfData getMediaFilePath]];
        
        if([DBManager checkFileAvailableToRemove:[myShelfData getMediaID] FilePath:[myShelfData getMediaFilePath]]){
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            if (error)
                NSLog(@"remove file error : %@",[error description]);
        }
        [DBManager removeMediaOnShelfFilePath:[myShelfData getMediaID]];
        [arrayOfMedia removeAllObjects];
        [arrayofLibrary removeAllObjects];
        [SVProgressHUD dismiss];
        [self loadMediaOnShelf];
    }
    else{
        [SVProgressHUD dismiss];
        [self dismissOpenReturnViewForPreviousCell];
        [self.collectionView reloadData];
    }
}

#pragma mark -------- Returm Media -----------
-(void)returnMediaFileFromShelf{
    //api/DeviceMedia/MediaReturn?deviceCode={deviceCode}&MediaId={MediaId}&RoomId={RoomId}&MemberId={MemberId}
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMediaResponse:) name:@"ReturnMedia" object:nil];
    OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
    [api mediaReturn:@"ReturnMedia" MemberID:[myShelfData getMemberID] RoomID:[myShelfData getLibraryID] MediaID:[myShelfData getMediaID]];
}
-(void)returnMediaResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReturnMedia" object:nil];
    //NSLog(@"Return Media Response : %@",[notification.userInfo description]);
    
    if ([[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200){
        OfflineShelf *myShelfData = [[self getItemInLibrary:[arrayofLibrary objectAtIndex:currentSelectSection]] objectAtIndex:currentSelectCell];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeMediaFinish:) name:[myShelfData getMediaID] object:nil];
        [Utility removeExpiredMediaOnShelf:[myShelfData getMediaID] CoverPath:[myShelfData getMediaCoverPath] FilePath:[myShelfData getMediaFilePath] MemberID:[myShelfData getMemberID] RoomID:[myShelfData getLibraryID]];
    }
    else{
        [SVProgressHUD dismiss];
        if ([Utility isServiceResponseError:notification.userInfo])
            [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
        
        else if([notification.userInfo isKindOfClass:[NSString class]]){
            [Utility showAlertViewController:@"Return Media Failed!" Message:[notification.userInfo description] Controller:(UIViewController*)self];
        }
        else
            [Utility showAlertViewController:@"Return Media Failed!" Message:nil Controller:(UIViewController*)self];
    }
   
}

-(void)removeMediaFinish:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[notification.userInfo objectForKey:@"MediaID"] object:nil];
    [SVProgressHUD dismiss];
    [self loadMediaOnShelf];
}

#pragma mark ----- sync Media on Cloud ----
-(void)syncMediaOnCloud:(NSString *)memberID{
    if(!([userDefault integerForKey:LastSyncCloudMedia] > 0 ) ||
       [userDefault integerForKey:LastSyncCloudMedia] < [[Utility currentDate] integerValue]){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncMediaOnCloudResponse:) name:@"syncMediaOnCloud" object:nil];
        [api syncMemberCloudMedia:@"syncMediaOnCloud" MemberID:memberID];
    }
    else{
         //NSLog(@"----- Media on Cloud is up to date ------");
        [self syncMediaPreset:memberID];
    }
}

-(void)syncMediaOnCloudResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"syncMediaOnCloud" object:nil];
    
    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        [self writeMediaToMyShelf:notification.userInfo ShelfID:MemberShelfId];
        [userDefault setInteger:[[Utility currentDate] integerValue] forKey:LastSyncCloudMedia];
        [userDefault synchronize];
        
        // next to sync Perset Media
        [self syncMediaPreset:memberID];
    }
    else{
        // next to sync Perset Media
        [self syncMediaPreset:memberID];
    }
    [SVProgressHUD dismiss];
}

#pragma mark ---- sync Preset ------
-(void)syncMediaPreset:(NSString *) memberID{
    //||[userDefault integerForKey:LastSyncPresetMedia] < [[Utility currentDate] integerValue]
    if(!([userDefault integerForKey:LastSyncPresetMedia] > 0 ) ||
       [userDefault integerForKey:LastSyncPresetMedia] < [[Utility currentDate] integerValue]){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncMediaPresetResponse:) name:@"SyncMediaPreset" object:nil];
        [api syncMediasPreset:@"SyncMediaPreset" MemberID:memberID];
    }
    else{
        //NSLog(@"----- Media Preset is up to date ------");
        [SVProgressHUD dismiss];
    }
}

-(void)syncMediaPresetResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SyncMediaPreset" object:nil];
    //NSLog(@"Media Preset:\n%@",[notification userInfo]);

    if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        //NSLog(@"Preset : %@",notification.userInfo);
        [self writeMediaToMyShelf:notification.userInfo ShelfID:PresetShelfId];
        [userDefault setInteger:[[Utility currentDate] integerValue] forKey:LastSyncPresetMedia];
        [userDefault synchronize];
        [self loadMediaOnShelf];
    }
    
    [SVProgressHUD dismiss];
    
}

-(void)writeMediaToMyShelf:(NSDictionary *)dictJson ShelfID:(NSString *)shelfID{
    if ([shelfID isEqualToString:PresetShelfId]) {
        [DBManager createMediaPresetTable];
        for( NSDictionary *level1 in ((NSArray*)dictJson)){
            NSString *folderName = [NSString stringWithFormat:@"%@/%@",sarapadFloderName,[level1 objectForKey:@"ROOMID"]];
            //crate dir ///
            [Utility createNewDirectory:folderName isSupportDirectory:FALSE];
            
            for (NSDictionary *presetItems in [level1 objectForKey:@"PresetItems"]) {
                //NSLog(@"%@",[presetItems objectForKey:@"SUBJECTNAME"]);
                MediaPreset *preSet = [[MediaPreset alloc] init];
                [preSet MediaPreset:[presetItems objectForKey:@"SUBJECTID"]
                           MemberID:memberID
                             ROOMID:[presetItems objectForKey:@"ROOMID"]
                           ROOMNAME:[presetItems objectForKey:@"ROOMNAME"]
                        SUBJECTCODE:[presetItems objectForKey:@"SUBJECTCODE"]
                        SUBJECTNAME:[presetItems objectForKey:@"SUBJECTNAME"]
                               DESP:[presetItems objectForKey:@"DESP"]
                        SECTIONCODE:[presetItems objectForKey:@"SECTIONCODE"]
                               YEAR:[presetItems objectForKey:@"YEAR"]
                           SEMESTER:[presetItems objectForKey:@"SEMESTER"]
                         UPDATEDATE:[presetItems objectForKey:@"UPDATEDATE"]];
                [DBManager insertMediaPreset:preSet];
                
                for (NSDictionary *mediaItems in [presetItems objectForKey:@"MediaItems"]) {
                    //NSLog(@"%@",[mediaItems objectForKey:@"MEDIANAME"]);
                    
                    //dowload Media Cover
                    NSString *mediaID = [mediaItems objectForKey:@"MEDIAID"];
                    NSString* coverFileAtPath = [self downloadMediaCover:mediaID Folder:folderName Url:[mediaItems objectForKey:@"MediaCoverUrl"]];
                    
                    //create MediaID for preset
                    mediaID = [Utility encodeMediaIDPreset:[mediaItems objectForKey:@"MEDIAID"]];
                    
                    NSString *subjectName = [NSString stringWithFormat:@"%@ : %@",[presetItems objectForKey:@"SUBJECTCODE"],[presetItems objectForKey:@"SUBJECTNAME"]];
                    
                    //generate exp
                    NSString *expDate = [mediaItems objectForKey:@"RENTEXPIRE"];
                    if (![expDate isEqualToString:MediaPresetToRemove] && ![expDate isEqualToString:MediaPresetNoExpireDate])
                        expDate = [Utility covertDateToFormat:[mediaItems objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"];
                    
                    OfflineShelf *myShelf = [[OfflineShelf alloc] init];
                    [myShelf OfflineShelf:mediaID
                                 MemberID:memberID
                                  ShelfID:[presetItems objectForKey:@"ROOMID"]
                                LibraryID:[presetItems objectForKey:@"SUBJECTID"]
                              LibraryName:subjectName
                               MediaTitle:[mediaItems objectForKey:@"MEDIANAME"]
                              MediaAuthor:[mediaItems objectForKey:@"AUTHOR"]
                                MediaType:[mediaItems objectForKey:@"MEDIAKIND"]
                        MediaCoverFileURL:[mediaItems objectForKey:@"MediaCoverUrl"]
                       MediaCoverFilePath:coverFileAtPath
                            MediaFilePath:@""
                             MediaFileURL:@""
                               DateBorrow:@""
                                  DateExp:expDate
                        MediaOriginalName:@""
                                SubjectID:[presetItems objectForKey:@"SUBJECTID"]];
                    [DBManager insertOfflineShelf:myShelf isPreset:YES];
                }
            }
        }
    }
    else if ([shelfID isEqualToString:MemberShelfId]){
        //NSLog(@"dict : %@",dictJson);
        for(NSDictionary *roomData in ((NSArray*)dictJson)){
            //NSLog(@"RoomID : %@",[roomData objectForKey:@"ROOMID"]);
            for (NSDictionary *mediaItems in [roomData objectForKey:@"MediaItems"]) {
                
                //dowload Media Cover
                NSString *folderName = [NSString stringWithFormat:@"%@/%@",sarapadFloderName,[roomData objectForKey:@"ROOMID"]];
                NSString *mediaID = [mediaItems objectForKey:@"MEDIAID"];
                NSString* coverFileAtPath = [self downloadMediaCover:mediaID Folder:folderName Url:[mediaItems objectForKey:@"MediaCoverUrl"]];
                
                OfflineShelf *myShelf = [[OfflineShelf alloc] init];
                //insert Metadata (file not download yet!!)
                [myShelf OfflineShelf:[mediaItems objectForKey:@"MEDIAID"]
                             MemberID:memberID
                              ShelfID:MemberShelfId
                            LibraryID:[roomData objectForKey:@"ROOMID"]
                          LibraryName:[roomData objectForKey:@"ROOMNAME"]
                           MediaTitle:[mediaItems objectForKey:@"MEDIANAME"]
                          MediaAuthor:[mediaItems objectForKey:@"AUTHOR"]
                            MediaType:[mediaItems objectForKey:@"MEDIAKIND"]
                    MediaCoverFileURL:[mediaItems objectForKey:@"MediaCoverUrl"]
                   MediaCoverFilePath:coverFileAtPath
                        MediaFilePath:@""
                         MediaFileURL:@""
                           DateBorrow:[Utility covertDateToFormat:[mediaItems objectForKey:@"RENTDATE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                              DateExp:[Utility covertDateToFormat:[mediaItems objectForKey:@"RENTEXPIRE"] InputFormat:@"yyyyMMddHHmmss" OutputFormat:@"yyyyMMdd"]
                    MediaOriginalName:@""
                            SubjectID:@""];
                
                [DBManager syncForInsertUpdateMemberCloudMedia:myShelf];
            }
            
        }
    }
    
}

-(NSString*)downloadMediaCover:(NSString *)mediaID Folder:(NSString*)folderName Url:(NSString*)_url{
    NSString* coverImgThumbName = [NSString stringWithFormat:@"%@_%@_thumb.png",memberID,mediaID];
    NSString *docPath  = [[Utility documentPath] stringByAppendingPathComponent:[folderName stringByAppendingPathComponent:coverImgThumbName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath: docPath]) {
        NSString* url = [_url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        if ([Utility isImage:imgData]) {
            [Utility saveMediaCoverImageFromData:imgData withFileName:coverImgThumbName inDirectory:folderName];
        }
    }
    
    // NSString* coverFileAtPath = [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
    
    return [folderName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", coverImgThumbName]];
}

#pragma mark ---- Share Annotation ------
-(void)shareAnnotation:(UIButton*) sender {
    if (@available(iOS 11.0, *)) {
        [SVProgressHUD setStatus:@"Loading..."];
        ShareAnnotationViewController *vc = [[ShareAnnotationViewController alloc] initWithRoomId:@"" mediaId:@"" memberId:memberID];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:NULL];
    } else {
        // Fallback on earlier versions
        [SVProgressHUD showErrorWithStatus:@"ฟังก์ชั่นนี้ไม่สามารถใช้งานบนเครื่องของคุณ\nการใช้งานต้องการ iOS 11.0 ขึ้นไป"];
    }
}

@end

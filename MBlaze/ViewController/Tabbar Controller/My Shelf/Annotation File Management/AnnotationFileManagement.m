//
//  AnnotationFileManagement.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AnnotationFileManagement.h"
#import "AnnotationFileTableCell.h"
#import "Utility.h"
#import "ThemeTemplate.h"
#import "HexColor.h"
#import "CustomActivityIndicator.h"
#import "ConstantValues.h"

@interface AnnotationFileManagement ()

@end

@implementation AnnotationFileManagement

#define cellIndentifier @"annotationFileCell"
#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@synthesize jsonDict;

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.tabBarController.tabBar setHidden:YES];
    [self.navigationItem setTitle:[Utility NSLocalizedString:@"AnnotationFile"]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    
    //float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    //CGSize navigationImageSize = CGSizeMake(navigationHeight-25, navigationHeight-25);
    
    UIBarButtonItem * leftBarButton = [[UIBarButtonItem alloc] initWithTitle:[Utility NSLocalizedString:@"DeleteAllFile"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteAllAnnotationFile:)];
    [leftBarButton setTintColor:[UIColor redColor]];
    
    UIBarButtonItem * rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_black"] style:UIBarButtonItemStyleDone target:self action:@selector(rightTopBarTapping:)];
    [rightBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    //rightBarButton.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    
}

-(void)deleteAllAnnotationFile:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Utility NSLocalizedString:@"DeleteAllFileAlert"] message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
         NSString *jsonString = [self createJsonStringToDeleteAnnotationFile:0 AtRow:0 isDeleteAll:YES];
        
        if ([jsonString isEqualToString:@""])
            [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:nil Controller:self];
        
        else
            [self deletingAnnotationFile:jsonString];
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)rightTopBarTapping:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [(NSArray*)jsonDict count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[(NSDictionary*)[(NSArray*)jsonDict objectAtIndex:section] objectForKey:@"ITEMS"] count] ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AnnotationFileTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    
    NSDictionary *dictItem = [[(NSDictionary*)[(NSArray*)jsonDict objectAtIndex:indexPath.section] objectForKey:@"ITEMS"] objectAtIndex:indexPath.row];
    
    cell.labelFileName.text = [dictItem objectForKey:@"MEDIANAME"];
    cell.labelFIleSize.text = [self getFileFormula:[[dictItem objectForKey:@"FILESIZE"] doubleValue]];
   // cell.accessibilityLabel = [dictItem objectForKey:@"MEDIAID"];
    //cell.accessibilityValue = [dictItem objectForKey:@"FILEURL"];
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *mview  = [[[NSBundle mainBundle] loadNibNamed:@"AnnotationHeaderView" owner:self options:nil] objectAtIndex:0];
    
    NSDictionary *dictItem = [(NSArray*)jsonDict objectAtIndex:section];
    
    UILabel *labelLibraryName = (UILabel*)[mview.subviews objectAtIndex:1];
    labelLibraryName.text = [dictItem objectForKey:@"ROOMNAME"];
    
    //Library Storage
    UIView *viewBgBarLibraryStorage = (UIView*)[mview.subviews objectAtIndex:2];
    viewBgBarLibraryStorage.layer.cornerRadius = 9.f;
    
    UILabel *labelLibraryStorage = (UILabel*)[mview.subviews objectAtIndex:3];
    labelLibraryStorage.text = [Utility NSLocalizedString:@"LibraryStorage"];
   
    UILabel *labelUsedStorageOfLibrary = (UILabel*)[mview.subviews objectAtIndex:8];
    labelUsedStorageOfLibrary.text = [NSString stringWithFormat:@"free %.2f%%",[[dictItem objectForKey:@"ROOMSIZE"] doubleValue]];
    
    //Member Personal Storage
    UIView *viewBgBarMemberStorage = (UIView*)[mview.subviews objectAtIndex:4];
    viewBgBarMemberStorage.layer.cornerRadius = 9.f;
    
    UILabel *labelMemberStorage = (UILabel*)[mview.subviews objectAtIndex:5];
    labelMemberStorage.text = [Utility NSLocalizedString:@"MemberStorage"];
    
    double freePersonalStorageSize = 100 - (([[dictItem objectForKey:@"PERSONUSED"] doubleValue]/[[dictItem objectForKey:@"PERSONSIZE"] doubleValue])*100);
    
    if ([[dictItem objectForKey:@"PERSONSIZE"] doubleValue] <= [[dictItem objectForKey:@"PERSONUSED"] doubleValue] ) {
        freePersonalStorageSize = 0;
    }
    
    UILabel *labelUsedStorageOfMember = (UILabel*)[mview.subviews objectAtIndex:9];
    labelUsedStorageOfMember.text = [NSString stringWithFormat:@"free %.2f%%",freePersonalStorageSize];
    
    //set bar percent
    float widthBarStorage = viewBgBarLibraryStorage.bounds.size.width;
    UIView *viewBarUsedStorageOfLibrary = (UIView*)[mview.subviews objectAtIndex:6];
    viewBarUsedStorageOfLibrary.layer.cornerRadius = 9.f;
    //Trailing
    viewBarUsedStorageOfLibrary.translatesAutoresizingMaskIntoConstraints = false;
    [[viewBarUsedStorageOfLibrary.trailingAnchor constraintEqualToAnchor:viewBgBarLibraryStorage.leadingAnchor
                                                                constant:[self calculateBarWidthPercentOfStorageUsed:[[dictItem objectForKey:@"ROOMSIZE"] doubleValue] BarWidth:widthBarStorage]] setActive:true];
    
    widthBarStorage = viewBgBarMemberStorage.bounds.size.width;
    UIView *viewBarUsedStorageOfMember = (UIView*)[mview.subviews objectAtIndex:7];
    viewBarUsedStorageOfMember.layer.cornerRadius = 9.f;
    //Trailing
    viewBarUsedStorageOfMember.translatesAutoresizingMaskIntoConstraints = false;
    [[viewBarUsedStorageOfMember.trailingAnchor constraintEqualToAnchor:viewBgBarMemberStorage.leadingAnchor
                                        constant:[self calculateBarWidthPercentOfStorageUsed:freePersonalStorageSize BarWidth:widthBarStorage]] setActive:true];
    
    [mview addSubview:labelLibraryName];
    [mview addSubview:viewBgBarLibraryStorage];
    [mview addSubview:labelLibraryStorage];
    [mview addSubview:viewBgBarMemberStorage];
    [mview addSubview:labelMemberStorage];
    [mview addSubview:viewBarUsedStorageOfLibrary];
    [mview addSubview:viewBarUsedStorageOfMember];
    [mview addSubview:labelUsedStorageOfLibrary];
    [mview addSubview:labelUsedStorageOfMember];

    return mview;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 150;
}

-(float)calculateBarWidthPercentOfStorageUsed:(float)percentFreeSpace BarWidth:(float)barWidth{
    barWidth = barWidth-50;
    return (barWidth - (barWidth/100)*percentFreeSpace);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dictItem = [[(NSDictionary*)[(NSArray*)jsonDict objectAtIndex:indexPath.section] objectForKey:@"ITEMS"] objectAtIndex:indexPath.row];
    
    [self shareOrDeleteFile:[dictItem objectForKey:@"FILEURL"] IndexPath:indexPath];
}

-(void)shareOrDeleteFile:(NSString *)filepath IndexPath:(NSIndexPath *)indexPath{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *shareFile = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"ShareFile"]
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [self prepareToShareAnnotationFile:indexPath];
                                                         }];
    UIAlertAction *deleteFile = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"DeleteFile"]
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                
                                                                NSString *jsonString = [self createJsonStringToDeleteAnnotationFile:indexPath.section AtRow:indexPath.row isDeleteAll:NO];
                                                                
                                                                [self prepateToDeleteAnnotationFile:jsonString];
                                                            }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"]
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
    
    [alert addAction:shareFile];
    [alert addAction:deleteFile];
    [alert addAction:cancelAction];
    
    if(IPAD){
        alert.popoverPresentationController.sourceView = self.tableView;
        alert.popoverPresentationController.sourceRect = [self.tableView rectForRowAtIndexPath:indexPath];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(NSString*)getFileFormula:(float)fileSize{
    float newSize = fileSize;
    NSString *fileFormula = @"Bytes";
    
    if (fileSize >= 1000000) {
        newSize = fileSize/1000000;
        fileFormula = @"MB";
    }
    
    return [NSString stringWithFormat:@"%.2f %@",newSize,fileFormula];
}

#pragma mark ------ Remove Annotation File ----
-(void)prepateToDeleteAnnotationFile:(NSString*)jsonString{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Utility NSLocalizedString:@"DeleteFileAlert"] message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if ([jsonString isEqualToString:@""])
            [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:nil Controller:self];
        else
            [self deletingAnnotationFile:jsonString];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:[Utility NSLocalizedString:@"Cancel"] style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)deletingAnnotationFile:(NSString*)jsonString{
    [CustomActivityIndicator presentActivityIndicatorView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAnnotationFileResponse:) name:@"RemoveAnnotationFile" object:nil];
    if (api == nil)
        api = [[ServiceAPI alloc] init];
    [api RemoveAnnotationFile:@"RemoveAnnotationFile" JsonString:jsonString];
}
-(void)deleteAnnotationFileResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RemoveAnnotationFile" object:nil];
    [CustomActivityIndicator dismissActivityIndicatorView];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        if ([[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200 )
            [self loadAnnotationFile];
        
        else
            [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastFailed"] isSuccess:YES];
    }
}

#pragma mark ------- Annotation File Management ---------
-(void)loadAnnotationFile{
    [CustomActivityIndicator presentActivityIndicatorView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(annotationFileResponse:) name:@"AnnotationFile" object:nil];
    if (api == nil)
        api = [[ServiceAPI alloc] init];
    [api annotationFile:@"AnnotationFile" MemberID:self.memberID];
}

-(void)annotationFileResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AnnotationFile" object:nil];
    //NSLog(@"annotationFile Result : %@",[notification.userInfo description]);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else {
        jsonDict = notification.userInfo;
        [self.tableView reloadData];
    }
}

#pragma mark ------- Share Annotation File --------
-(void)prepareToShareAnnotationFile:(NSIndexPath*)indexPath{
    UIAlertController *shareAlert = [UIAlertController alertControllerWithTitle:@"Share Annotation File" message: [NSMutableString stringWithString: @"Input Email"] preferredStyle:UIAlertControllerStyleAlert];
    
    [shareAlert addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = [NSMutableString stringWithString: @"Email"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:[NSMutableString stringWithString: @"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = shareAlert.textFields;
        UITextField * email = textfields[0];
        //NSLog(@"share to email : %@",email.text);
        NSString *path = [NSString stringWithFormat:@"%@%@",[(NSDictionary*)[(NSArray*)jsonDict objectAtIndex:indexPath.section] objectForKey:@"ROOMPATH"],[[[(NSDictionary*)[(NSArray*)jsonDict objectAtIndex:indexPath.section] objectForKey:@"ITEMS"] objectAtIndex:indexPath.row] objectForKey:@"FILEURL"]];
        
        [self shareAnnotationFileByEmail:email.text FilePath:path];
    }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:[NSMutableString stringWithString: @"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [shareAlert addAction:actionOK];
    [shareAlert addAction:actionCancel];
    
    [self presentViewController:shareAlert animated:YES completion:nil];
   
}

-(void)shareAnnotationFileByEmail:(NSString*)email FilePath:(NSString*)filepath{
    if ([Utility validateEmailFormat:email]) {
        [CustomActivityIndicator presentActivityIndicatorView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareAnnotationFileResponse:) name:@"ShareAnnotationFile" object:nil];
        if (api == nil)
            api = [[ServiceAPI alloc] init];
        [api ShareAnnotationFile:@"ShareAnnotationFile" FilePath:filepath Username:email];
    }
    else
        [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:[Utility NSLocalizedString:@"EmailInValid"] Controller:self];
    
}
-(void)shareAnnotationFileResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShareAnnotationFile" object:nil];
    [CustomActivityIndicator dismissActivityIndicatorView];
    /*
     return MessageCode model
     - Code 200 = สำเร็จ
     - Code 201 = Full Disk
     - Code 202 = No Member.
     - Code 203 = Unavailable to share file.
     - Code 204 = error;
     */
    //NSLog(@"share annotation response : %@",notification.userInfo);
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        if ([[notification.userInfo objectForKey:@"MessageCode"] intValue] == 200) {
            [CustomActivityIndicator presentToastView:[Utility NSLocalizedString:@"ToastSuccess"] isSuccess:YES];
        }
        else{
            NSString *errorMsg = nil;
            switch ([[notification.userInfo objectForKey:@"MessageCode"] intValue]) {
                case 201:
                    errorMsg = @"There is not enough available storage.";
                    break;
                case 202:
                    errorMsg = @"Email is not Member in this Room.";
                    break;
                case 203:
                    errorMsg = @"Unavailable to share file";
                    break;
                case 204:
                    break;
                default:
                    break;
            }
            
            [Utility showAlertViewController:[Utility NSLocalizedString:@"SomethingWrong"] Message:errorMsg Controller:self];
        }
    }
    
}

-(NSString*)createJsonStringToDeleteAnnotationFile:(NSInteger)tableViewSection AtRow:(NSInteger)rowNumber isDeleteAll:(BOOL)isDeleteAll{
    if (!isDeleteAll) {
        NSDictionary *dictRoomObject = (NSDictionary*)[(NSArray*)jsonDict objectAtIndex:tableViewSection];
        NSDictionary *dictRemoveFile = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [dictRoomObject objectForKey:@"ROOMID"],@"ROOMID",
                                        [dictRoomObject objectForKey:@"ROOMPATH"],@"ROOMPATH",
                                        @[[[[dictRoomObject objectForKey:@"ITEMS"] objectAtIndex:rowNumber] objectForKey:@"FILEURL"]],@"ITEMS",nil];
        //array NSDictionary to Json String
        return [self jsonToStringJsonArray:@[dictRemoveFile]];
    }
    else{
        NSMutableArray *mulArrayJsonDict = [[NSMutableArray alloc] init];
        for (NSDictionary *dictRoomObject in (NSArray*)jsonDict) {
            NSMutableArray *mulArrayFilePath = [[NSMutableArray alloc] init];
            for (NSDictionary *dictItem in [dictRoomObject objectForKey:@"ITEMS"]) {
                [mulArrayFilePath addObject:[dictItem objectForKey:@"FILEURL"]];
            }
            [mulArrayJsonDict addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [dictRoomObject objectForKey:@"ROOMID"],@"ROOMID",
                                         [dictRoomObject objectForKey:@"ROOMPATH"],@"ROOMPATH",
                                         mulArrayFilePath,@"ITEMS",nil]];
        }
        
        //array NSDictionary to Json String
        return [self jsonToStringJsonArray:mulArrayJsonDict];
    }
}

-(NSString *)jsonToStringJsonArray:(id)json{
    //array NSDictionary to Json String
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&err];
    NSString * myString = @"";
    myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"JSOM String : %@",myString);
    return myString;
}

@end

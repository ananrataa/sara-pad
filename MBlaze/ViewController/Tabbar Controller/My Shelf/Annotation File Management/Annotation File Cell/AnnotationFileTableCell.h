//
//  AnnotationFileTableCell.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnotationFileTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelFileName;
@property (weak, nonatomic) IBOutlet UILabel *labelFIleSize;
@property (weak, nonatomic) IBOutlet UIImageView *imageCheckbox;

@end

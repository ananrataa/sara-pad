//
//  AnnotationFileTableCell.m
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "AnnotationFileTableCell.h"

@implementation AnnotationFileTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  AnnotationFileManagement.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/5/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface AnnotationFileManagement : UITableViewController{
    
    ServiceAPI *api;
}


@property(strong,nonatomic)NSDictionary *jsonDict;
@property(strong,nonatomic)NSString *memberID;
@end

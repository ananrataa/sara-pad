//
//  MyShelfTableViewController.h
//  SARA-PAD
//
//  Created by MBOX Multimedia Co., Ltd. on 21/2/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import "RestfulServices.h"
#import "LibraryDetailVC.h"

@interface MyShelfTableViewController : UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    
    NSUserDefaults *userDefault;
    NSString *memberID;
    NSString *deviceCode;
    NSString *curUsedLibraryID;
    
    //UIBarButtonItem *leftBarButton;
    UIButton *barButtonLibrary;
    //UIBarButtonItem *rightBarButton;
    CGSize navigationImageSize;
    
    LibraryDetailVC *libraryDetailVC;
    NSString *roomName;
    
    //    float navigationHeight;
    //    CGSize navigationImageSize;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    UIBarButtonItem *sortBarButton;
    UIBarButtonItem *searchBarButton;
    
    UIView *sortView;
    
    NSMutableArray *arrayofLibrary;
    NSMutableArray *arrayOfMedia;
    NSArray *arrayshelfId;
    NSString *currentShelfId;
    
    int xScrollViewContentOffset;
    
    ReaderDocument *document;
    NSString *tempFloderName;
    NSString *filePath;
    NSString *tempPath;
    NSString *fileExtension;
    
    int currentSelectCell;
    int currentSelectSection;
    
    RestfulServices *service;
    
    BOOL isSelectFirstTime;
}

@end

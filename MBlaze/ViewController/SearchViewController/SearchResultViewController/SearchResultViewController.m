//
//  SearchResultViewController.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/11/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SearchResultViewController.h"
#import "CollectionViewCell.h"
#import "Utility.h"
//#import "FilterList.h"
#import "ConstantValues.h"
#import "CustomActivityIndicator.h"
#import "ConstantValues.h"
#import "DBManager.h"
#import "HexColor.h"
#import "ThemeTemplate.h"
#import "UIImageView+WebCache.h"
#import "SARA_PAD-Swift.h"

@interface SearchResultViewController ()

@end

@implementation SearchResultViewController

#define cellMediaIdentifier  @"mediaContentCell" //@"collectionCellMedia";
//static NSString * const cellHeaderIdentifier = @"collectionCellHeader";


@synthesize libraryID;
@synthesize jsonData;
@synthesize textForSearch;
@synthesize arrayCatSubCat;

- (void)viewDidLoad {
    [super viewDidLoad];
    isViewDidLoad = YES;
    
    navigationHeight = self.navigationController.navigationBar.frame.size.height;
    statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationItem setTitle:[NSString stringWithFormat:@"คำค้นหา \"%@\"",textForSearch]];
    [self.tabBarController.tabBar setHidden:YES];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]], NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    self.navigationController.navigationBar.barTintColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet1]];
    
    /// calculate collection Cell Width & Height
    collectionCellWidth = (([[UIScreen mainScreen] bounds].size.width-32)/[Utility calaulateCollectionItemInRow]);
    collectionCellHeight =  ((collectionCellWidth*4)/3)+90;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellMediaIdentifier];
    self.collectionView.contentInset = UIEdgeInsetsMake(58, 8, 8, 8);

    //navigationImageSize = [Utility calculateNavigationbarImageSize:navigationHeight isLeft:YES];
    
//    UIView *bgViewNav = [[UIView alloc] initWithFrame:CGRectMake(0.0f, navigationHeight-2, [[UIScreen mainScreen] bounds].size.width, 2)];
//    bgViewNav.backgroundColor = [UIColor blackColor];
//    [self.navigationController.navigationBar addSubview:bgViewNav];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_back_black"] style:UIBarButtonItemStyleDone target:self action:@selector(leftTopBarTapping:)];
    [leftBarButton setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet2]]];
    leftBarButton.imageInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    
    api = [[ServiceAPI alloc] init];
    arrayOfMedia = [[NSMutableArray alloc] init];
    arrayOfFilter = [NSMutableArray arrayWithObjects:@[@"0",[Utility NSLocalizedString:@"CurrentLib"]],@[@"1",[Utility NSLocalizedString:@"YourLib"]],@[@"2",[Utility NSLocalizedString:@"AllLib"]], nil];
    //imageUrlCache = [[NSMutableDictionary alloc] init];
    
    searchBy = @"0";
    loadMediaCounter = 1;
    sortBy= 0;
    isLoadingMore = NO;
    
    [CustomActivityIndicator presentActivityIndicatorView];
    
    //[self createRightNavigationTab];
    [self createFilterListTab];
    [self getMediaItemsFromJsonString];
    
    ///send user search keyword to service
    [self sendUserSearchKeywordTextToService:textForSearch RoomID:[userDefault objectForKey:KeyCuurrentUsedLribaryID]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leftTopBarTapping:(id)sender{
   // [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getMediaItemsFromJsonString{
    //NSLog(@"Result json : %@",[jsonData description]);
    if (![Utility isServiceResponseError:jsonData] && [Utility isJsonDictionaryNotNull:jsonData]){
        for (NSDictionary *mediaItem in jsonData) {
            NSMutableDictionary *mulDicItem = [[NSMutableDictionary alloc] init];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAID"]]
                          forKey:@"MEDIAID"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIANAME"]]
                          forKey:@"MEDIANAME"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"AUTHOR"]]
                          forKey:@"AUTHOR"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"POPULARVOTE"]]
                          forKey:@"POPULARVOTE"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MediaCoverUrl"]]
                          forKey:@"MediaCoverUrl"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"MEDIAKIND"]]
                          forKey:@"MEDIAKIND"];
            [mulDicItem setValue:[Utility covertNsNullToNSString:[mediaItem objectForKey:@"ROOMID"]]
                          forKey:@"ROOMID"];
            [arrayOfMedia addObject:mulDicItem];
            
            //NSLog(@"Media Title : %@",[mediaItem objectForKey:@"MEDIANAME"]);
        }
        [self.collectionView reloadData];
        self.view.userInteractionEnabled = YES;
        
        collegetionViewContentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height+20;
        float loadMoreIndicatorYpoint = collegetionViewContentHeight>=self.collectionView.frame.size.height? collegetionViewContentHeight:self.collectionView.frame.size.height;
        
        //NSLog(@"collegetionViewContentHeight : %f",collegetionViewContentHeight);
        
        [[self.view viewWithTag:909] removeFromSuperview];
        ///load more IndicatorView
        UIView *loadMoreIndicatorView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicatorView" owner:self options:nil] objectAtIndex:0];
        loadMoreIndicatorView.frame = CGRectMake(0,loadMoreIndicatorYpoint, [[UIScreen mainScreen] bounds].size.width, 50);
        [loadMoreIndicatorView setBackgroundColor:[UIColor clearColor]];
        loadMoreIndicatorView.tag = 909;
        loadMoreActivityIndicator = nil;
        loadMoreActivityIndicator = [[loadMoreIndicatorView subviews] objectAtIndex:0];
        [loadMoreActivityIndicator setHidesWhenStopped:YES];
        [self.collectionView addSubview:loadMoreIndicatorView];
        
    }
    else{
        if (noResultView != nil){
            [noResultView removeFromSuperview];
            noResultView = nil;
        }
        noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        noResultView.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:noResultView.frame];
        label.text = [NSMutableString stringWithString: @"🔍ไม่พบผลการค้นหา"];
        //label.font = [UIFont systemFontOfSize:17.f];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        [Utility setFont:label.font WithFontSize:[Utility fontSize:17.f]];
        
        [noResultView addSubview:label];
        [self.view addSubview:noResultView];
        
        self.view.userInteractionEnabled = YES;
        
        if([arrayOfMedia count] > 0){
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3.f * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //code to be executed on the main queue after delay
                self.view.userInteractionEnabled = YES;
                if (noResultView != nil) {
                    [noResultView removeFromSuperview];
                    noResultView = nil;
                }
            });
        }
    }

    [CustomActivityIndicator dismissActivityIndicatorView];
    if(loadMoreActivityIndicator.isAnimating)[loadMoreActivityIndicator stopAnimating];
    
    isViewDidLoad = NO;
    isLoadingMore = NO;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrayOfMedia count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellMediaIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *itemDic = [arrayOfMedia objectAtIndex:indexPath.row];
    
    //[cell.mediaCoverImageView setImage:[UIImage imageNamed:@"default_media_cover.png"]];
    [cell.mediaCoverImageView setBackgroundColor:[UIColor whiteColor]];
    cell.mediaCoverImageView.contentMode = UIViewContentModeScaleToFill;
    
    //[cell.mediaCoverImageView sd_setImageWithURL:[NSURL URLWithString:[itemDic objectForKey:@"MediaCoverUrl"]] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]];
    NSString *imgUrl = [itemDic objectForKey:@"MediaCoverUrl"];
    [cell.mediaCoverImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_media_cover.png"]  options:0 progress:nil completed:nil];
    
    //[imageUrlCache removeObjectForKey:imgUrl];
    //[imageUrlCache setValue:imgUrl forKey:imgUrl];
    
    
   
    cell.mediaTitleLabel.text = [itemDic objectForKey:@"MEDIANAME"];
    cell.mediaAuthorLabel.text = [itemDic objectForKey:@"AUTHOR"];
    [Utility setFont:cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:14.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:11.f]];
    
    [Utility setMediaRating:[itemDic objectForKey:@"POPULARVOTE"] ImageView1:cell.starImageView1 ImageView2:cell.starImageView2 ImageView3:cell.starImageView3 ImageView4:cell.starImageView4 ImageView5:cell.starImageView5];
    [Utility setMediaTypeIcon:cell.mediaTypePic MediaType:[itemDic objectForKey:@"MEDIAKIND"]];
     cell.videoImagePic.hidden = YES;
//    if ([[[itemDic objectForKey:@"MEDIAKIND"] lowercaseString] isEqualToString:@"v"])
//        cell.videoImagePic.hidden = NO;
    
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.selectedBackgroundView = uiView;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"collectionView didSelectItemAtIndexPath");
    // NSLog(@"------------ index : %li --------------",(long)indexPath.row );
    [CustomActivityIndicator presentActivityIndicatorView];
    NSDictionary *itemDic = [arrayOfMedia objectAtIndex:indexPath.row];
    [self loadMediaDetail:[itemDic objectForKey:@"MEDIAID"] RoomID:[itemDic objectForKey:@"ROOMID"]];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(collectionCellWidth, collectionCellHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 8.0f;
}


#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (noResultView != nil) {
        [noResultView removeFromSuperview];
        noResultView = nil;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
  
    if(scrollView.contentOffset.y >= 100 && (scrollView.contentOffset.y + scrollView.frame.size.height) > (scrollView.contentSize.height + scrollView.contentInset.bottom + [Utility bounceHeightForLoadMore]) && !isLoadingMore && !isViewDidLoad){
        [loadMoreActivityIndicator startAnimating];
        isLoadingMore = YES;
        showLoadingMore = NO;
    }
    
    if (scrollView.contentOffset.y <= (collegetionViewContentHeight - self.collectionView.bounds.size.height + 50) && isLoadingMore  && !showLoadingMore){
        [self.collectionView setContentOffset:CGPointMake(0,(collegetionViewContentHeight - self.collectionView.bounds.size.height + 50)) animated:YES];
        showLoadingMore = YES;
        self.view.userInteractionEnabled = NO;
        [CustomActivityIndicator presentActivityIndicatorView];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            loadMediaCounter ++;
           [self loadMoreMediaItemsWithFlag:[searchBy intValue]];
        });
    }
}


#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)mediaID RoomID:(NSString*)room_id{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    [api browsMediaDetail:@"MediaItemDetail" MediaID:mediaID RoomId:room_id LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
//        MediaDetailTVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        //vc.libraryID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
//        // vc.libraryName = [DBManager selectLibraryNameByID:libraryID];
//        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        vc.jsonData = notification.userInfo;
//        vc.rootNavigationController = self;
//
//        [self.navigationController pushViewController:vc animated:YES];
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            [self.tabBarController setSelectedIndex:2];
        };
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
        [Utility showAlertViewController:nil Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}


#pragma mark -------- load More Media Items with filter ------
-(void)loadMoreMediaItemsWithFlag:(int)searchFlag{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMoreMediaItemsResponse:) name:@"LoadMoreMediaItemsResponse" object:nil];
    [api mediaSearch:@"LoadMoreMediaItemsResponse" RoomID:libraryID SearchFlag:searchFlag CategoryID:arrayCatSubCat[0] SubCatrgory:arrayCatSubCat[1] MediaKind:@"" TextSearch:textForSearch LoadIndex:loadMediaCounter];
}

-(void)loadMoreMediaItemsResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoadMoreMediaItemsResponse" object:nil];
    //NSLog(@"reloadMediaItemsWithFilter result : %@",notification.userInfo);
    jsonData = notification.userInfo;
    [self getMediaItemsFromJsonString];
}

 #pragma mark ----- Create Filter Tab -------------
-(void)createFilterListTab{
    UIView *filterView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f,self.collectionView.frame.size.width, 50.f)];
    filterView.backgroundColor = [UIColor whiteColor];
    filterView.tag = 809;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.f, 10.f,filterView.frame.size.width-20, filterView.frame.size.height-20)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    float margin = 0.0f;
    float btn_width = 150.0f;
    float btn_height = 30.0f;
    
    for (NSArray *filter in arrayOfFilter){
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(margin, 0.0f, btn_width, btn_height)];
        [btn setTintColor:[UIColor whiteColor]];
        [btn setTitle:filter[1] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:mainFont size:[Utility fontSize:17.f]];
        btn.backgroundColor  = [UIColor lightGrayColor];
        btn.accessibilityLabel = filter[0];
        [btn addTarget:self action:@selector(filterMediaTapping:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([filter[0] isEqualToString:searchBy]){
            btn.backgroundColor  = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
            [btn setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
            xScrollViewContentOffset = margin;
        }
        
        btn.clipsToBounds = YES;
        btn.layer.cornerRadius = 15.0f;
        
        [scrollView addSubview:btn];
        
        margin = margin + 5.0f + btn_width;
    }
    //scrollView.frame = CGRectMake(0.0f, 0.0f, filterView.frame.size.width, btn_height);
    [scrollView setContentSize:CGSizeMake(margin, btn_height)];
    
    scrollView.contentOffset = CGPointMake(xScrollViewContentOffset,0);
    
    [filterView addSubview:scrollView];
    
    [self.view addSubview:filterView];
}
 
 #pragma mark ------- filter Media --------
-(void)filterMediaTapping:(UIButton*)sender{
    //NSLog(@"filter by code : %@ , Name : %@",sender.accessibilityLabel,sender.titleLabel.text);
    
    [CustomActivityIndicator presentActivityIndicatorView];
    UIView *headerView = [self.view viewWithTag:809];
    UIScrollView *scrollView = (UIScrollView*)[headerView.subviews firstObject];
    for (UIButton *btn in scrollView.subviews) {
        btn.backgroundColor  = [UIColor lightGrayColor];
        [btn setTintColor:[UIColor whiteColor]];
    }
    
    sender.backgroundColor = [HexColor SKColorHexString:[ThemeTemplate getThemeSet3]];
    [sender setTintColor:[HexColor SKColorHexString:[ThemeTemplate getThemeSet4]]];
    searchBy = sender.accessibilityLabel;
    loadMediaCounter = 1;
    [self searchMediaWithFilter:[sender.accessibilityLabel intValue]];
    
}

-(void)searchMediaWithFilter:(int)searchFlag{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchMediaResponse:) name:@"SearchediaWithFilter" object:nil];
    
    [api mediaSearch:@"SearchediaWithFilter" RoomID:libraryID SearchFlag:searchFlag CategoryID:arrayCatSubCat[0] SubCatrgory:arrayCatSubCat[1] MediaKind:@"" TextSearch:textForSearch LoadIndex:loadMediaCounter];
}


-(void)searchMediaResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchediaWithFilter" object:nil];
    //NSLog(@"Search Result : %@",[notification.userInfo description]);
    
    jsonData = notification.userInfo;
    [arrayOfMedia removeAllObjects];
    [self getMediaItemsFromJsonString];
    
}


#pragma mark ------- History User Search Keyword -------
-(void)sendUserSearchKeywordTextToService:(NSString *)searchText RoomID:(NSString *)roomID{
    [api findHistory:@"UserSearchKeyword" SearchKeyword:searchText RoomID:roomID];
}

@end

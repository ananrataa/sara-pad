//
//  SearchResultViewController.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/11/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface SearchResultViewController : UICollectionViewController<UICollectionViewDelegateFlowLayout,UISearchBarDelegate>{
    float navigationHeight;
    float statusBarHeight;
    CGSize navigationImageSize;
    
    float collectionCellWidth;
    float collectionCellHeight;
    
    //UIBarButtonItem *leftBarButton;
    //UIBarButtonItem *sortBarButton;
    //UIBarButtonItem *searchBarButton;
    
    UIView *sortView;
    
    NSMutableArray *arrayOfMedia;
    NSMutableArray *arrayOfFilter;
    //NSMutableDictionary *mediaCacheDic;
    
    //MediaFileCache *mCache;
    ServiceAPI *api;
    
    NSString *searchBy;
    int xScrollViewContentOffset;
    
    NSString *deviceCode;
    
    NSUserDefaults *userDefault;
    
    int loadMediaCounter;
    int sortBy;
    
    BOOL isLoadingMore;
    BOOL isViewDidLoad;
    
    UIView *noResultView;
    
    UIActivityIndicatorView *loadMoreActivityIndicator;
    BOOL showLoadingMore;
    float collegetionViewContentHeight;
    
    //NSMutableDictionary *imageUrlCache;
}

//@property (strong,nonatomic)NSString* categoryID;
@property (strong,nonatomic)NSString* textForSearch;
@property (strong,nonatomic)NSArray *arrayCatSubCat;
@property (strong,nonatomic)NSString *libraryID;
@property (strong,nonatomic)NSDictionary *jsonData;

@property (nonatomic, strong) UISearchController *searchController;

@end

//
//  SearchMediaPreviewTVC.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/12/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestfulServices.h"

@interface SearchPreviewViewController1 : UITableViewController{
    RestfulServices *service;
    NSString *deviceCode;
}

@property (strong,nonatomic)UISearchBar *searchView;

//@property (nonatomic, assign) id<ListSearchMediaResults> delegate;
@property (nonatomic, strong)NSMutableArray *arrayOfMedia;
@property (nonatomic, strong)NSString *libraryID;
@property (nonatomic, assign) UIViewController* delegate;
@end

//
//  ListOfSearchCell.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SearchPreviewCell.h"

@implementation SearchPreviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

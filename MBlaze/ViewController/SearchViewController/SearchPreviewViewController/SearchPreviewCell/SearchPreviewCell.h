//
//  ListOfSearchCell.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 9/8/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPreviewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mediaKindImage;
@property (weak, nonatomic) IBOutlet UILabel *mediaTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mediaAuthorLabel;

@end

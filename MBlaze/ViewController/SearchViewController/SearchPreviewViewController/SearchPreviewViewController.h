//
//  SearchViewController.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/31/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestfulServices.h"

//@protocol ListSearchMediaResults <NSObject>
//
//@optional
//
//@end


@interface SearchPreviewViewController : UITableViewController{
    
    RestfulServices *service;
    NSString *deviceCode;
    NSString *libraryID;
    
}


@property (strong,nonatomic)UISearchBar *searchView;

//@property (nonatomic, assign) id<ListSearchMediaResults> delegate;
@property (nonatomic, strong)NSMutableArray *arrayOfMedia;
//@property (nonatomic, strong)NSString *libraryID;
@property (nonatomic, assign) UIViewController* delegate;

@end

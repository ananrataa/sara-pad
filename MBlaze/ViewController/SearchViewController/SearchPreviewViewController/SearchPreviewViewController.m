//
//  SearchViewController.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/31/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SearchPreviewViewController.h"
#import "HexColor.h"
#import "SearchPreviewCell.h"
#import "Utility.h"
#import "CustomActivityIndicator.h"
#import "ConstantValues.h"
#import "MediaDetailTVC.h"
#import "DBManager.h"

@interface SearchPreviewViewController ()

@end

@implementation SearchPreviewViewController

@synthesize arrayOfMedia;
//@synthesize libraryID;

#define itemCellIdentifier @"listOfSearchCell"

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.backgroundColor = [UIColor lightGrayColor];
    
    service = [[RestfulServices alloc] init];
    deviceCode = [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceCode];

    [self.tableView registerNib:[UINib nibWithNibName:@"SearchPreviewCell" bundle:nil] forCellReuseIdentifier:itemCellIdentifier];
    
    //arrayOfMedia = [[NSMutableArray alloc] init];
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [arrayOfMedia count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    SearchPreviewCell *cell = (SearchPreviewCell *)[tableView dequeueReusableCellWithIdentifier:itemCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *item = [arrayOfMedia objectAtIndex:indexPath.row];
    
    ///set media kind image
    [Utility setMediaTypeIcon:cell.mediaKindImage MediaType:[item objectForKey:@"MEDIAKIND"]];
    
    cell.mediaTitleLabel.text = [item objectForKey:@"MEDIANAME"];
    cell.mediaAuthorLabel.text = [item objectForKey:@"AUTHOR"];
    
    libraryID = [item objectForKey:@"ROOMID"];
    
    return cell;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 58.f;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.searchView resignFirstResponder];
    
    [CustomActivityIndicator presentActivityIndicatorView:@"loading..."];
    
    NSDictionary *item = [arrayOfMedia objectAtIndex:indexPath.row];
    [self loadMediaDetail:[item objectForKey:@"MEDIAID"]];
    
}

#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)mediaID{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    //NSLog(@"checkUserLimitDayAndMedia : %@",[userLimitData description]);
    
    NSString *param = [NSString stringWithFormat:@"%@api/DeviceMedia/BrowsMediaDetail?deviceCode=%@&RoomId=%@&MediaId=%@&ItemLimitMember=%i",hostIpRoomAPI,deviceCode,libraryID,mediaID,(int)[userLimitData[1] intValue]];
    NSLog(@"Param : %@",param);
    [service sendHTTPRequest:param NotificationName:@"MediaItemDetail" ReturnType:@"JSON" HttpMethod:@"GET"];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        MediaDetailTVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        vc.libraryID = libraryID;
        // vc.libraryName = [DBManager selectLibraryNameByID:libraryID];
        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
        vc.jsonData = notification.userInfo;
        vc.isOwnNavigationBar = NO;
        
    
        [self.delegate.navigationController pushViewController:vc animated:YES];
        
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
//        [self setModalPresentationStyle:UIModalPresentationCurrentContext];
//        [self presentViewController:navigationController animated:YES completion:nil];

    }
    else
        [Utility showAlertViewController:@"" Message:UnknownError Controller:(UIViewController*)self];
    
}




@end

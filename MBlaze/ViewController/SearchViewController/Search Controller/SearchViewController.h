//
//  SearchViewController.h
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface SearchViewController : UITableViewController<UITextFieldDelegate>{
    ServiceAPI *api;
    NSString *deviceCode;
    
    NSMutableArray *arrayOfMedia;
    
    NSString *libraryIdOfSelectItem;
    
    BOOL isSearching;
    
    float navigationHeight;
    float statutBarHeight;
    
    //CGRect tableCGRect;
}

@property (nonatomic, strong) UITextField *searchTextField;
@property (nonatomic, strong)NSString *categoryID;
@property (nonatomic, strong)NSString *subCategoryID;
@property (nonatomic, strong)NSString *libraryID;

@end

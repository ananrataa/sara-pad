//
//  SearchViewController.m
//  SARAPAD
//
//  Created by MBOX Multimedia Co., Ltd. on 9/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import "SearchViewController.h"
#import "HexColor.h"
#import "Utility.h"
#import "ConstantValues.h"

#import "SearchPreviewCell.h"
#import "SearchResultViewController.h"
#import "CustomActivityIndicator.h"
#import "ConstantValues.h"
//#import "MediaDetailTVC.h"
#import "DBManager.h"
#import "SARA_PAD-Swift.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

#define itemCellIdentifier @"listOfSearchCell"

@synthesize libraryID;
@synthesize categoryID;
@synthesize subCategoryID;

- (void)viewDidLoad {
    [super viewDidLoad];    
    self.view.backgroundColor = [HexColor SKColorHexString:@"21202f"];
    [self.view setUserInteractionEnabled:NO];
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    navigationHeight = self.navigationController.navigationBar.frame.size.height;
    statutBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    // NSLog(@"1navigation H : %f",navigationHeight);
    //NSLog(@"1status H : %f",statutBarHeight);
    
    self.navigationController.navigationBar.translucent = NO;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SearchPreviewCell" bundle:nil] forCellReuseIdentifier:itemCellIdentifier];
    self.tableView.backgroundColor = [HexColor SKColorHexString:@"21202f"];
    
    // Create a NSTextAttachment with your image
    NSTextAttachment*   placeholderImageTextAttachment = [[NSTextAttachment alloc] init];
    placeholderImageTextAttachment.image = [UIImage imageNamed:@"search-black"];
    // Use 'bound' to adjust position and size
    placeholderImageTextAttachment.bounds = CGRectMake(0, 0, 16, 16);
    NSMutableAttributedString*  placeholderImageString = [[NSAttributedString attributedStringWithAttachment:placeholderImageTextAttachment] mutableCopy];
    // Append the placeholder text
    NSMutableAttributedString*  placeholderString = [[NSMutableAttributedString alloc] initWithString:[Utility NSLocalizedString:@"SearchPlaceHolder"]  attributes: @{NSFontAttributeName: [UIFont fontWithName:mainFont size:[Utility fontSize:14.f]]}];
    [placeholderImageString appendAttributedString:placeholderString];
    
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, 30.0)];
    self.searchTextField.backgroundColor = [HexColor SKColorHexString:@"E8E8E8"];;
    [self.searchTextField setBorderStyle:UITextBorderStyleRoundedRect];
    self.searchTextField.delegate = self;
    self.searchTextField.textColor = [UIColor blackColor];
    self.searchTextField.attributedPlaceholder = placeholderImageString;
    [self.searchTextField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    [self.searchTextField setKeyboardAppearance:UIKeyboardAppearanceLight];
    [self.searchTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
    [self.searchTextField setSpellCheckingType:UITextSpellCheckingTypeNo];
    [self.searchTextField setClearButtonMode:UITextFieldViewModeAlways];
    
    UIButton *cancelLabel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    [cancelLabel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelLabel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [cancelLabel setTitle:[Utility NSLocalizedString:@"Cancel"] forState:UIControlStateNormal];
    cancelLabel.titleLabel.font = [UIFont fontWithName:mainFontHeader size:[Utility fontSize:17.f]];
    [cancelLabel setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [cancelLabel setContentEdgeInsets:UIEdgeInsetsMake(0, 8, 0, -8)];
    [cancelLabel addTarget:self action:@selector(cancelButtonTapping:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithCustomView:cancelLabel];
    //    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonTapping:)];
    //[cancel setTintColor:[UIColor blackColor]];
    //[cancel setBackgroundVerticalPositionAdjustment:5 forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.rightBarButtonItem = cancel;
    self.navigationItem.titleView = self.searchTextField;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    deviceCode = [userDefault objectForKey:KeyDeviceCode];
    api = [[ServiceAPI alloc] init];
    
    arrayOfMedia = [[NSMutableArray alloc] init];
    
//    [self.tableView setTableFooterView:[[UIView alloc] init]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //NSLog(@"------ viewWillAppear --------");
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchMediaPreviewResponse:) name:@"SearchViewController" object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.frame = CGRectMake(0, navigationHeight + statutBarHeight, self.view.frame.size.width, self.view.frame.size.height);
            self.tableView.contentInset = UIEdgeInsetsMake(5.f, 0, 5, 0);
            [self.searchTextField becomeFirstResponder];
            [self.view setUserInteractionEnabled:YES];
            isSearching = NO;
        });
    });
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //NSLog(@"------ viewWillDisappear --------");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchViewController" object:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrayOfMedia count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    SearchPreviewCell *cell = (SearchPreviewCell *)[tableView dequeueReusableCellWithIdentifier:itemCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *item = [arrayOfMedia objectAtIndex:indexPath.row];
    
    ///set media kind image
    [Utility setMediaTypeIcon:cell.mediaKindImage MediaType:[item objectForKey:@"MEDIAKIND"]];
    
    cell.mediaTitleLabel.text = [item objectForKey:@"MEDIANAME"];
    cell.mediaTitleLabel.textColor = [HexColor SKColorHexString:@"#e0ebeb"];
    cell.mediaAuthorLabel.text = [item objectForKey:@"AUTHOR"];
    cell.mediaAuthorLabel.textColor = [HexColor SKColorHexString:@"#75a3a3"];
    [Utility setFont: cell.mediaTitleLabel.font WithFontSize:[Utility fontSize:17.f]];
    [Utility setFont:cell.mediaAuthorLabel.font WithFontSize:[Utility fontSize:16.f]];
    
    libraryIdOfSelectItem = [item objectForKey:@"ROOMID"];
    
    cell.backgroundColor = [UIColor clearColor]; //[HexColor SKColorHexString:@"0e0f20"];
    cell.contentView.backgroundColor = [UIColor clearColor];
   // [tableView setSeparatorColor:[UIColor darkGrayColor]]; AFEEEE
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 58.f;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.searchTextField resignFirstResponder];
    
    [CustomActivityIndicator presentActivityIndicatorView];
    
    NSDictionary *item = [arrayOfMedia objectAtIndex:indexPath.row];
    [self loadMediaDetail:[item objectForKey:@"MEDIAID"] SearchText:self.searchTextField.text];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.searchTextField resignFirstResponder];
}


#pragma mark ---------- Search ---------

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
//    NSLog(@"Text : %@",string);
//    NSLog(@"NSRange : %lu , %lu",(unsigned long)range.length,(unsigned long)range.location);
//    NSLog(@"------------------");
    
    if (((unsigned long)range.length) == 1)
       // NSLog(@"delete text : %@", [textField.text substringWithRange:NSMakeRange(0,[textField.text length] - 1)]);
        [self searchMediaPreviewForText:[textField.text substringWithRange:NSMakeRange(0,[textField.text length] - 1)]];
    else
        // NSLog(@"searchMediaPreviewForText : %@",[textField.text stringByAppendingString:string]);
        [self searchMediaPreviewForText:[textField.text stringByAppendingString:string]];
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!isSearching) {
        isSearching = YES;
        [textField resignFirstResponder];
        [CustomActivityIndicator presentActivityIndicatorView];
         //NSLog(@"textFieldShouldReturn : %@",textField.text);
        [self searchMediaForText:textField.text];
    }
    return YES;
}

-(void)cancelButtonTapping:(UIButton*)sender{
    [self dismissSearchView];
}
-(void)dismissSearchView{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchViewController" object:nil];
    [self.searchTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ----------- Search Media Preview ------------
-(void)searchMediaPreviewForText:(NSString *)text4Search{
    [api cancelHttpService];
    [api mediaSearch:@"SearchViewController" RoomID:libraryID SearchFlag:0 CategoryID:categoryID SubCatrgory:subCategoryID MediaKind:@"" TextSearch:text4Search LoadIndex:1];
}
-(void)searchMediaPreviewResponse:(NSNotification *)notification{
    //NSLog(@"Search Result : %@",[notification.userInfo description]);
    //[arrayOfMedia removeAllObjects];
    if(![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        [arrayOfMedia removeAllObjects];
        for (NSDictionary *dicItems in notification.userInfo) {
            NSMutableDictionary *mediaItemDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 [dicItems objectForKey:@"MEDIAID"],@"MEDIAID",
                                                 [dicItems objectForKey:@"MEDIAKIND"],@"MEDIAKIND",
                                                 [dicItems objectForKey:@"MEDIANAME"],@"MEDIANAME",
                                                 [dicItems objectForKey:@"AUTHOR"],@"AUTHOR",
                                                 [dicItems objectForKey:@"ROOMID"],@"ROOMID", nil];
            [arrayOfMedia addObject:mediaItemDic];
        }
        
        [self.tableView reloadData];
    }
    else if (![Utility isServiceResponseError:notification.userInfo]){
        [arrayOfMedia removeAllObjects];
        [self.tableView reloadData];
    }
}

#pragma mark ------ Search Media ------
-(void)searchMediaForText:(NSString*)text4Search{
    [api cancelHttpService];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchMediaRespones:) name:@"SearchMedia_ListMedia" object:nil];
    [api mediaSearch:@"SearchMedia_ListMedia" RoomID:libraryID SearchFlag:0 CategoryID:categoryID SubCatrgory:subCategoryID MediaKind:@"" TextSearch:text4Search LoadIndex:1];
}

-(void)searchMediaRespones:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchMedia_ListMedia" object:nil];
    //NSLog(@"Search Result : %@",[notification.userInfo description]);
    [CustomActivityIndicator dismissActivityIndicatorView];
    isSearching = NO;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
    SearchResultViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SearchResultViewController"];
    vc.libraryID = libraryID;
    vc.jsonData = notification.userInfo;
    vc.textForSearch = self.searchTextField.text;
    vc.arrayCatSubCat = @[categoryID,subCategoryID];
    
    UINavigationController *searchController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:searchController animated:YES completion:nil];
    
    //[self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ------------- Load Media Detail --------
-(void)loadMediaDetail:(NSString*)mediaID SearchText:(NSString*)searchText{
    [api cancelHttpService];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
    NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
    [api browsMediaDetailSearch:@"MediaItemDetail" RoomID:libraryIdOfSelectItem MediaID:mediaID SearchText:searchText LimitItem:(int)[userLimitData[1] intValue]];
}

-(void)getMediaDetailResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"media detail : %@",notification.userInfo);
    
    [CustomActivityIndicator dismissActivityIndicatorView];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
//        MediaDetailTVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        vc.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        //vc.libraryID = libraryID;
//        // vc.libraryName = [DBManager selectLibraryNameByID:libraryID];
//        vc.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        vc.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        vc.jsonData = notification.userInfo;
//        vc.isOwnNavigationBar = NO;
//        vc.rootNavigationController = self;
//
//        [self.navigationController pushViewController:vc animated:YES];
        

    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
    
}

@end

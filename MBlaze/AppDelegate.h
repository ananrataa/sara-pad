//
//  AppDelegate.h
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import <UserNotifications/UserNotifications.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>{
    
    NSUserDefaults *userDefaults;
    NSDictionary *userInfoRemoteNotification;
    //UILocalNotification *tapNotification;
    UIApplication *_application;
    BOOL appIsAlive;
}

-(void)relaunchApplication;

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)MMDrawerController *drawerContainer;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

@end


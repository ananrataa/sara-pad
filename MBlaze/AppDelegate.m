//
//  AppDelegate.m
//  MBlaze
//
//  Created by MBOX Multimedia Co., Ltd. on 7/20/2560 BE.
//  Copyright © 2560 MBOX Multimedia Co., Ltd. All rights reserved.
//[DBManager deleteAllRowInMyShelfTable];
//    [DBManager deleteAllRowInMemberTable];
//[DBManager deleteAllRowInLibraryTable];
//[Utility removeAllFilesInDirectory:sarapadFloderName];
//

#import "AppDelegate.h"
#import "SplashScreen.h"

#import "MyTabbarController.h"
#import "LeftSlideMenuVC.h"
#import "RightSlideMenuVC.h"
#import "DBManager.h"

#import "ConstantValues.h"
#import "Utility.h"

#import <UserNotifications/UserNotifications.h>

#import "MediaDetailTVC.h"
#import "ServiceAPI.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <LineSDK/LineSDK.h>

#import "SignInVC.h"
#import "HexColor.h"
#import "SVProgressHUD.h"
#import "SARA_PAD-Swift.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
NSURL *externalPdf;
@synthesize drawerContainer;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //TabBar
    UIImage *tabBarBackground = [UIImage imageNamed:@"wood_4.jpg"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [UITabBar appearance].clipsToBounds = YES;
    NSDictionary *attBarButton = @{NSFontAttributeName:[UIFont fontWithName:sukhumvitFont size:17]};
    [[UIBarButtonItem appearance] setTitleTextAttributes:attBarButton forState:UIControlStateNormal];
    
    //UINavigationBar
    NSDictionary *att = @{NSFontAttributeName:[UIFont fontWithName:mainFont size:21]};
    [[UINavigationBar appearance] setTitleTextAttributes:att];
    //SVProgressHUD
    [SVProgressHUD setBackgroundColor:[UIColor darkGrayColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setFont:[UIFont fontWithName:mainFont size:15]];
    // get deviceCode //
    userDefaults = [NSUserDefaults standardUserDefaults];
    // set default Language //
    if(!([[userDefaults objectForKey:AppLanguageKey] length] > 0)){
        NSString *lang = [[Utility getDeviceLanguage] objectForKey:@"kCFLocaleLanguageCodeKey"] ;
        [userDefaults setValue:[NSArray arrayWithObjects:[lang lowercaseString], nil]  forKey:@"AppleLanguages"];
        [userDefaults setValue:[lang uppercaseString]  forKey:AppLanguageKey];
        [userDefaults synchronize];
    }
    //set default tabbar index
    UIFont *font = [UIFont fontWithName:mainFont size:12];
    NSDictionary *attTabBar = @{NSFontAttributeName:font};
    [[UITabBarItem appearance] setTitleTextAttributes:attTabBar forState:UIControlStateNormal];

    [userDefaults setInteger:0 forKey:TabbarIndex];
    [userDefaults synchronize];
    if (!([[userDefaults objectForKey:KeyDeviceCode] length] > 0)) {
        [userDefaults setValue:[[NSUUID UUID] UUIDString] forKey:KeyDeviceCode];
        [userDefaults synchronize];
    }
    //launchOptions have Local Notification Key
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]) {
        _application = application;
        [userDefaults setInteger:2 forKey:TabbarIndex];
        [userDefaults synchronize];
    }
    //launchOptions have Remote Notification Key
    else if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]){
        userInfoRemoteNotification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
        _application = application;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeNotification:) name:@"UserTapNotification" object:nil];
      
    }
    //launchOptions have open url
    else if ([launchOptions objectForKey:UIApplicationLaunchOptionsURLKey]){
        NSURL *url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
        userInfoRemoteNotification = [NSDictionary dictionaryWithObjectsAndKeys:DeepAppLinkHeader,@"Header",[url absoluteString],@"DeepLinkURL", nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeNotification:) name:@"UserTapNotification" object:nil];
    }
    //launchOptions require user sign in
    else{
        userInfoRemoteNotification = [NSDictionary dictionaryWithObjectsAndKeys:UserSigninRequire,@"Header", nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeNotification:) name:@"UserTapNotification" object:nil];
    }

    [self registerForRemoteNotifications];
    
    // creat table if not exist
    if (![DBManager columnExists:@"ETC" inTableWithName:@"MEMBER_INFO_TABLE"]) {
        [DBManager dropMemberTable];
        [DBManager deleteAllRowInLibraryTable];
    }

    [DBManager createMemberInfoTable];
    [DBManager createRoomInfoTable];
    [DBManager createOfflineShelfTable];
    [DBManager createBookmarkRemindTable];
    [DBManager upgradeDatabase];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(splashScreenNotification:) name:@"SplashScreen" object:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashScreen *splash = (SplashScreen*)[storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
    self.window.rootViewController = splash;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //external Pdf
//    NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
//    if ([url isFileURL]) {
//        externalPdf = url;
//    }

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    //[Utility writeLogFile:@"applicationWillResignActive,"];
    // active to inactive state by press home or swicth to another app
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits
    //NSLog(@"applicationDidEnterBackground");
    [self updateDateTimeLastLeaveApp];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    if ([OfflineShelf isOnlyOfflineModeEnable]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        SplashScreen *splash = (SplashScreen*)[storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
        self.window.rootViewController = splash;
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    appIsAlive = YES;
    
    NSString *startDate = [[NSUserDefaults standardUserDefaults] objectForKey:LastLeaveApp];
    NSString *currentDate = [Utility currentDateTimeWithFormatter:@"yyyyMMddHHmmss"];
    //get time between two dates
    if([startDate length] > 0 && ([Utility dateToSecond:startDate EndDate:currentDate DateFormat:@"yyyyMMddHHmmss"]/3600 >= 36)){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:LastLeaveApp];
        [DBManager deleteAllRowInMemberTable];
        [DBManager deleteAllRowInLibraryTable];
        [self relaunchApplication];
    }
    
    [self checkMemberRemainLogin];
    
    if (externalPdf) {
        [self openExternalPdf];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //[Utility writeLogFile:@"applicationWillTerminate , "];
    appIsAlive = NO;
    [self updateDateTimeLastLeaveApp];
}

#pragma mark --- To handle the returned result of the authentication process from the LINE app --
#pragma mark -- handle results from the native Facebook app when you perform a Login or Share action. --
//available in iOS 10 and above
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    NSLog(@"openURL : %@",[url description]);
    if ([url isFileURL]) {
        externalPdf = url;
        return YES;
    }
    
    if ([[url description] containsString:@"sarapad://"] && appIsAlive) {
        [self recieveUrlDeepLink:[url description]];
    }
    
    return ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]  annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
            || [[LineSDKLogin sharedInstance] handleOpenURL:url]);
    
}

#pragma mark ---------- receive notification from splash screen -----
-(void)splashScreenNotification:(NSNotificationCenter*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SplashScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(splashScreenNotification:) name:@"SplashScreen" object:nil];
    [self buildNavigationDrawer];
    
    //open external Pdf
    if (externalPdf) {
        [self openExternalPdf];
    }
    
    //Redeem code
    if (isRedeem) {
        isRedeem = NO;
        RedeemViewController *vc = [[RedeemViewController alloc] init];
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        UIViewController *topVC = [[SP shared] topViewController];
        if (topVC) {
            [topVC presentViewController:vc animated:YES completion:nil];
            externalPdf = nil;
        }
    }
}
-(void)openExternalPdf {
    MMDrawerController *mmdController = (MMDrawerController*) self.window.rootViewController;
    if (mmdController == nil || externalPdf == nil) {
        return;
    }
    
    PDFDocument *pdfDoc = [[PDFDocument alloc] initWithURL:externalPdf];
    ADPdf *pdfAds = [[ADPdf alloc] initWithInterval:0 images:@[]];
    PdfAnnotationInfo *info = [[PdfAnnotationInfo alloc] initWithRoomId:@""
                                                                mediaId:@""
                                                            pdfDocument:pdfDoc
                                                               filePath:@""
                                                             uploadPath:@""
                                                       removeFileOnExit:NO
                                                              canUpload:NO
                                                              isPreview:NO
                                                                 pdfURL:externalPdf
                                                               password:@""
                                                                    ads:pdfAds];
    PdfViewController *pdfVC = [[PdfViewController alloc] initWithInfo: info];
    UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:pdfVC];
    UIViewController *topVC = [[SP shared] topViewController];
    if (topVC) {
        [topVC presentViewController:nv animated:YES completion:nil];
        externalPdf = nil;
    }
}
#pragma mark ---------- build Navigation Drawer ------------
-(void)buildNavigationDrawer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    MyTabbarController *mainPage = (MyTabbarController*) [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
    
    LeftSlideMenuVC *leftSideMenu = (LeftSlideMenuVC*)[storyboard instantiateViewControllerWithIdentifier:@"LeftSlideMenuVC"];
    // RightSlideMenuVC *rightSlideMenu = (RightSlideMenuVC*)[storyboard instantiateViewControllerWithIdentifier:@"RightSlideMenuVC"];
    
    UINavigationController *leftSlideMenuNav = [[UINavigationController alloc] initWithRootViewController:leftSideMenu];
    
    drawerContainer = [[MMDrawerController alloc] initWithCenterViewController:mainPage leftDrawerViewController:leftSlideMenuNav];
    double max =  [[UIScreen mainScreen] bounds].size.width * 0.65;
    max = MIN(max, 265);
    drawerContainer.maximumLeftDrawerWidth = max;
    [drawerContainer setBezelPanningCenterViewRange:30.f];
    [drawerContainer setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeBezelPanningCenterView];
    [drawerContainer setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
    
    [drawerContainer.view setHidden:YES];
    [drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:NO completion:^(BOOL finished) {
        if (finished) {
            [drawerContainer closeDrawerAnimated:NO completion:NULL];
            [drawerContainer.view setHidden:NO];
        }
    }];
    self.window.rootViewController = drawerContainer;
}


#pragma mark ------ register For Notifications ----------
- (void)registerForRemoteNotifications {
    
    // Register your app for remote notifications.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                //NSLog(@"request authorization succeeded!");
                // For iOS 10 display notification (sent via APNS)
                //[UNUserNotificationCenter currentNotificationCenter].delegate = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                    //NSLog(@"ios 10++ : %@",[UIApplication sharedApplication].isRegisteredForRemoteNotifications? @"YES" : @"NO");
                });
            }
        }];
#endif
#if defined(__IPHONE_9_3) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_3
        // iOS 9 or before
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        //NSLog(@"ios 9-- : %@",[UIApplication sharedApplication].isRegisteredForRemoteNotifications? @"YES" : @"NO");
#endif
        
}

//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
//    NSLog(@"Did register - %@", notificationSettings);
//}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString * notiToken = [[[deviceToken description] componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet]invertedSet]]componentsJoinedByString:@""];
    NSLog(@"Notifications deviceToken = %@",notiToken);
    if ([deviceToken length] > 0) {
        [userDefaults setValue:notiToken forKey:NotificationTokenID];
        [userDefaults synchronize];
        
        ServiceAPI *api = [[ServiceAPI alloc] init];
        [api updateNotificationTokenID:@"" TokenID:notiToken];
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Register Notifications Error :%@",str);
}

#pragma mark ------ Receieve Notifications ---------
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"willPresentNotification : %@", userInfo);
    if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaExpNotificationHeader]) {
        if ([[userInfo objectForKey:@"isMember"] boolValue] && [DBManager isMember]) {
            completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
        } else if (![[userInfo objectForKey:@"isMember"] boolValue]) {
            completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
        }
        
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaFollowNotificationHeader]) {
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaPresetNotificationHeader]) {
         completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:RoomRegisterHeader]) {
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MergeNotiSender]) {
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MergeNotiReceiever]) {
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert| UNAuthorizationOptionBadge);
    }
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"didReceiveNotificationResponse");
}
#pragma mark ------ Receieve Data content ---------------
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification]
    NSLog(@"Receieve Data content : \n%@",[userInfo description]);
    
    if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaFollowNotificationHeader]) {
        if (application.applicationState == UIApplicationStateActive || appIsAlive) {
            // Application is running in foreground
            [self prepareToBrowsMediaDetail:userInfo];
        }
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:UserExpiredNotificationHeader]) {
        if([DBManager isMember]){
            [DBManager deleteAllRowInMemberTable];
            [DBManager deleteAllRowInLibraryTable];
            [self showSignOutViewController];
        }
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaPresetNotificationHeader]){
        [userDefaults removeObjectForKey:LastSyncPresetMedia];
        [userDefaults synchronize];
        [self segueToTabbarMyShelf:application];
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MergeNotiSender]) {
        //เตะคนถูก Merge
        NSLog(@"Noti to Sender");
        if([DBManager isMember]){
            [SVProgressHUD showInfoWithStatus:[Utility NSLocalizedString:@"merge_logout"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [DBManager deleteAllRowInMemberTable];
                [DBManager deleteAllRowInLibraryTable];
                [self showSignOutViewController];
            });
        }
    } else if ([[userInfo objectForKey:@"Header"] isEqualToString:MergeNotiReceiever]) {
        //refresh คนรับ
        NSLog(@"Noti to Receiver");
        if([DBManager isMember]){
            [SVProgressHUD showInfoWithStatus:[Utility NSLocalizedString:@"merge_refresh"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self relaunchApplication];
            });
        }
    }
    
    //for UIApplicationStateBackground or UIApplicationStateInactive
    //wait for init tabbar done and handle at homeNotification method
    completionHandler(UIBackgroundFetchResultNewData);
    //| UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNNotificationPresentationOptionAlert
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    [self segueToTabbarMyShelf:application];
    NSLog(@"didReceiveLocalNotification");
}

//segue to My Shelf Tabbar while UIApplicationStateActive
-(void)segueToTabbarMyShelf:(UIApplication *)application{
    if (application.applicationState == UIApplicationStateActive || appIsAlive) {
        [userDefaults setInteger:2 forKey:TabbarIndex];
        [userDefaults synchronize];
        [self buildNavigationDrawer];
    }
    else{
        [userDefaults setInteger:2 forKey:TabbarIndex];
        [userDefaults synchronize];
    }
}

-(void)homeNotification:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UserTapNotification" object:nil];
    
    if ([userInfoRemoteNotification count] > 0){
        if ([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:MediaFollowNotificationHeader]) {
            //launchOptions is Remote Notification
            [self prepareToBrowsMediaDetail:userInfoRemoteNotification];
            //[self application:_application didReceiveRemoteNotification:userInfoRemoteNotification];
            _application = nil;
            userInfoRemoteNotification = nil;
        } else if ([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:MediaPresetNotificationHeader]){
            [userDefaults removeObjectForKey:LastSyncPresetMedia];
            [userDefaults synchronize];
            [userDefaults setInteger:2 forKey:TabbarIndex];
            [userDefaults synchronize];
        } else if  ([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:DeepAppLinkHeader]){
            [self recieveUrlDeepLink:[userInfoRemoteNotification objectForKey:@"DeepLinkURL"]];
        } else if([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:UserSigninRequire]){
           /* if (![DBManager isMember]) {
                [[self window] makeKeyAndVisible];
                // get the root view controller
                MMDrawerController *mmdController = (MMDrawerController*) self.window.rootViewController;
                
                //add destination viewcontroller
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                SignInVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
                [vc setIsOwnNavigationBar:YES];
                // add navigation controller
                UINavigationController *nVC = [[UINavigationController alloc] initWithRootViewController:vc];
                // segue viewcontroller
                [[mmdController centerViewController] presentViewController:nVC animated:YES completion:nil];
            } */
        } else if ([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:MergeNotiSender]) {
            NSLog(@"MergeNotiSender");
        } else if ([[userInfoRemoteNotification objectForKey:@"Header"] isEqualToString:MergeNotiReceiever]) {
            NSLog(@"MergeNotiReciever");
        }
    }
}

-(void)prepareToBrowsMediaDetail:(NSDictionary *) userInfo{
    if ([[userInfo objectForKey:@"Header"] isEqualToString:MediaFollowNotificationHeader] ||
        [[userInfo objectForKey:@"Header"] isEqualToString:DeepAppLinkHeader]){
        [SVProgressHUD show];
        
        NSString *mediaId = [userInfo objectForKey:@"MediaID"];
        NSString *roomId = [userInfo objectForKey:@"RoomID"];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
            //            [self.tabBarController setSelectedIndex:2];
        };
        vc.isNavigation = YES;
        UINavigationController *nVC = [[UINavigationController alloc] initWithRootViewController:vc];
        
        MMDrawerController *mmdController = (MMDrawerController*) self.window.rootViewController;
        [[mmdController centerViewController] presentViewController:nVC animated:YES completion:nil];
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMediaDetailResponse:) name:@"MediaItemDetail" object:nil];
//        NSArray *userLimitData = [Utility checkUserLimitDayAndMedia];
//        //NSLog(@"checkUserLimitDayAndMedia : %@",[userLimitData description]);
//
//        ServiceAPI *api = [[ServiceAPI alloc] init];
//        [api browsMediaDetail:@"MediaItemDetail" MediaID:[userInfo objectForKey:@"MediaID"] RoomId:[userInfo objectForKey:@"RoomID"] LimtItem:(int)[userLimitData[1] intValue] IsPreset:NO];
    }
}

-(void)getMediaDetailResponse:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MediaItemDetail" object:nil];
    //NSLog(@"MediaDetailResponse : %@",[notification.userInfo description]);
    [SVProgressHUD dismiss];
    
    if ([Utility isServiceResponseError:notification.userInfo])
        [Utility showAlertViewController:nil Message:[notification.userInfo objectForKey:@"error"] Controller:(UIViewController*)self];
    
    else if (![Utility isServiceResponseError:notification.userInfo] && [Utility isJsonDictionaryNotNull:notification.userInfo]){
        
        [[self window] makeKeyAndVisible];
        // get the root view controller
        MMDrawerController *mmdController = (MMDrawerController*) self.window.rootViewController;
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//        //init ViewController
//        MediaDetailTVC *mediaDetail = [storyboard instantiateViewControllerWithIdentifier:@"MediaDetailTVC"];
//        mediaDetail.mediaID = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
//        mediaDetail.mediaType = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAKIND"]];
//        mediaDetail.mediaTitle = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIANAME"]];
//        mediaDetail.jsonData = notification.userInfo;
//        mediaDetail.isOwnNavigationBar = YES;
//        // add navigation controller
//        UINavigationController *nVC = [[UINavigationController alloc] initWithRootViewController:mediaDetail];
//        // segue viewcontroller
//        [[mmdController centerViewController] presentViewController:nVC animated:YES completion:nil];
        
        
        NSString *mediaId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"MEDIAID"]];
        NSString *roomId = [Utility covertNsNullToNSString:[notification.userInfo objectForKey:@"ROOMID"]];
        MediaDetailViewController *vc = [[MediaDetailViewController alloc] initWithMediaId:mediaId roomId:roomId];
        vc.completion = ^(SPMediaDownload *mediaInfo) {
//            [self.tabBarController setSelectedIndex:2];
        };
        vc.isNavigation = YES;
        UINavigationController *nVC = [[UINavigationController alloc] initWithRootViewController:vc];
        [[mmdController centerViewController] presentViewController:nVC animated:YES completion:nil];
        
    }
    else
        [Utility showAlertViewController:@"" Message:[Utility NSLocalizedString:UnknownError] Controller:(UIViewController*)self];
}


-(void)relaunchApplication{
    //NSLog(@"Language : %@",[userDefaults objectForKey:@"AppleLanguages"]);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(splashScreenNotification:) name:@"SplashScreen" object:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashScreen *splash = (SplashScreen*)[storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
    self.window.rootViewController = splash;
}


-(void)showSignOutViewController{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:[Utility NSLocalizedString:@"NewSignin"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[Utility NSLocalizedString:@"OK"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   [self relaunchApplication];
                                   
                               }];
    
    [alert addAction:okButton];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
    //[self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark ----- Member Remain Login ------
-(void)checkMemberRemainLogin{
    NSString *memberID = [DBManager selectMemberID];
    if([memberID length] > 0){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isMemberLoginResponse:) name:@"MemberRemainLogin" object:nil];
        ServiceAPI *api = [[ServiceAPI alloc] init];
        [api isMemberRemainLogin:@"MemberRemainLogin" MemberID:memberID];
    }
}

-(void)isMemberLoginResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MemberRemainLogin" object:nil];
   // NSLog(@"isMemberLogin : %@",notification.userInfo);
    if ([Utility isJsonDictionaryNotNull:notification.userInfo] && [[notification.userInfo objectForKey:@"MessageCode"] intValue] == 204){
        NSLog(@"Member Remain Login No ---> is User");
        [self clearMemberData];
    }
}

-(void)clearMemberData{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:LastLeaveApp] length] > 0) {
        [DBManager deleteAllRowInMemberTable];
        [DBManager deleteAllRowInLibraryTable];
        [self relaunchApplication];
    }
}

-(void)updateDateTimeLastLeaveApp{
    [userDefaults setValue:[Utility currentDateTimeWithFormatter:@"yyyyMMddHHmmss"] forKey:LastLeaveApp];
    [userDefaults synchronize];
}


#pragma mark -------- recieve URL Deep Link --------
-(void)recieveUrlDeepLink:(NSString*)url{
    NSLog(@"URL Deep Link : %@",url);
    NSString *strUrl = [[url description] stringByReplacingOccurrencesOfString:@"sarapad://" withString:@""];
    
    NSArray *arrayHeaderName = [([strUrl componentsSeparatedByString:@"?"])[0] componentsSeparatedByString:@"&"] ;

    NSMutableDictionary *dictKeyValue = [[NSMutableDictionary alloc] init];
    for (NSString *strKeyValue in arrayHeaderName) {
        NSArray *arrayKeyValue = [strKeyValue componentsSeparatedByString:@"="];
        if (arrayKeyValue.count > 1) {
            [dictKeyValue setValue:arrayKeyValue[1] forKey:arrayKeyValue[0]];
        }
    }
    
    //NSLog(@"Key Value : %@",[dictKeyValue description]);
    NSString *funcName = [dictKeyValue objectForKey:@"FUNCTION"];
    if([funcName isEqualToString:@"media"] || [funcName isEqualToString:@"rent"]){
        NSDictionary *userDict = [NSDictionary dictionaryWithObjectsAndKeys:DeepAppLinkHeader,@"Header",
                                  [dictKeyValue objectForKey:@"MID"],@"MediaID",
                                  [dictKeyValue objectForKey:@"LID"],@"RoomID", nil];
        [self prepareToBrowsMediaDetail:userDict];
    }
//    NSString *msg = [NSString stringWithFormat:@"Deep link:\n %@\n%@", funcName, url];
//    [SVProgressHUD showInfoWithStatus:msg];
}



#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.≥
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"SARA_PAD"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end

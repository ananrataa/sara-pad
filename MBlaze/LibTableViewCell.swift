//
//  LibTableViewCell.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 12/10/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class LibTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBookCount: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @objc var curLib: SPLib? {
        didSet {
            refreshView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        logo.layer.cornerRadius = 30
        
        logo.layer.shadowOpacity = 0.35
        logo.layer.shadowOffset = .init(width: 1, height: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func refreshView() {
        if let lib = curLib {
            if let image = lib.image {
                logo.image = image
            } else {
                logo.image = UIImage(named: "default_library")
            }
            if Locale.preferredLanguages[0] == "en" {
                lblName.text = "Library: " + lib.name
                lblBookCount.text = "Total media: " + (lib.mediaCount > 0 ? "\(lib.mediaCount)" : "-")
            } else {
                lblName.text = "ห้องสมุด: " + lib.name
                lblBookCount.text = "จำนวนหนังสือและสื่อต่างๆ: " + (lib.mediaCount > 0 ? "\(lib.mediaCount)" : "-")
            }

        }
    }
}

//
//  ShelfViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 13/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

class ShelfViewController: UIViewController, AlertHandler {
    var isNavigate: Bool = false
    let shelf_thai = [#imageLiteral(resourceName: "public.png"), #imageLiteral(resourceName: "member.png"), #imageLiteral(resourceName: "purchase.png"), #imageLiteral(resourceName: "course.png")]
    let shelf_eng = [#imageLiteral(resourceName: "public_eng.png"), #imageLiteral(resourceName: "member_eng.png"), #imageLiteral(resourceName: "purchase_eng.png"), #imageLiteral(resourceName: "course_eng.png")]
    let gap:CGFloat = 20
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.showsHorizontalScrollIndicator = false
        sv.delaysContentTouches = true
        return sv
    }()
    let shelfColors:[UIColor] = [.shelfPublic, .shelfMember, .shelfPurchase, .shelfClass]
    lazy var libButton: UIButton = {
        let btn = UIButton()
        btn.addTarget(self, action: #selector(libraryTapped), for: .touchUpInside)
        btn.clipsToBounds = true
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 0.35
        return btn
    }()
    @objc var shelfButtons: [ARButton] = []
    var indicators: [UIView] = []
    var curIndexPath: IndexPath!
    
    @objc var curMediaId: String?
    @objc var curShelf: Int = 0
    let shelfView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }()
    var libraryNames: [String] = []
    var shelfMedias: [[OfflineShelfData]] = []
    var memberId: String = {
        var id = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
        if MemberInfo.isSignin() || DBManager.isMember() {
            id = DBManager.selectMemberID()
        }
        return id
    }()
    
    var shelfCollectionVC: ShelfCollectionViewController!
    var presetShelfTableVC: PresetShelfTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        setupNavigation()
        setup()
        setupLeftMenuNotifications()
    }
    deinit {
        print("DEINIT: XXXXX")
    }
    fileprivate func loadShelf() -> Int {
        if let shelfId = curSelectedShelfId {
            return [2,1,3,0][shelfId]
        }
        //check badge
        for i in 0...3 {
            if shelfButtons[i].badge > 0 {
                return i
            }
        }
        //check value
        for i in 0...3 {
            if shelfButtons[i].value > 0 {
                return i
            }
        }
        return 0
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
       
        if isNavigate {
            isNavigate.toggle()
            return
        }
        if InternetConnection.isConnectedToNetwork() {
            syncMedias { [unowned self] in
                DispatchQueue.main.async {
                    self.refreshBadge()
                    self.curShelf = self.loadShelf()
                    self.shelfTapped(self.shelfButtons[self.curShelf])
                }
            }
        } else {
            self.refreshBadge()
            self.curShelf = self.loadShelf()
            self.shelfTapped(self.shelfButtons[self.curShelf])
            SVProgressHUD.dismiss()
        }
        
        //Notifications
//        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (requests) in
//            requests.forEach({ (request) in
//                print("\(request.identifier)")
//            })
//        })
        
//        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
//            notifications.forEach({ (notification) in
//                print("\(notification.request.identifier)")
//            })
//        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .libraryChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: .history, object: nil)
        NotificationCenter.default.removeObserver(self, name: .signOut, object: nil)
        NotificationCenter.default.removeObserver(self, name: .signIn, object: nil)
        NotificationCenter.default.removeObserver(self, name: .setting, object: nil)
        NotificationCenter.default.removeObserver(self, name: .help, object: nil)
        NotificationCenter.default.removeObserver(self, name: .about, object: nil)
        NotificationCenter.default.removeObserver(self, name: .memberProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: .redeemCourse, object: nil)
    }
    func setupNavigation() {
        LightTheme.shared.applyNavigationBar(vc: self)
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .done, target: self, action: #selector(menuTapped))
        let h = (navigationController?.navigationBar.frame.height)!*0.8
        libButton.frame = CGRect(x: 0, y: 0, width: h, height: h)
        let image = Utility.rescale(UIImage(named: "default_library"), scaledTo: .init(width: h, height:h))
        libButton.setImage(image, for: .normal)
        if let curLibraryId = UserDefaults.standard.string(forKey: KeyCuurrentUsedLribaryID) {
            DB.getRoomLogo(id: curLibraryId) {[weak self] (img) in
                if let img = img {
                    let image = Utility.rescale(img, scaledTo: .init(width: h, height:h))
                    DispatchQueue.main.async {
                        self?.libButton.setImage(image, for: .normal)
                        self?.libButton.layer.cornerRadius = h/2
                        let libBarButtonItem = UIBarButtonItem(customView: (self?.libButton)!)
                        self?.navigationItem.leftBarButtonItems = [leftBarButtonItem, libBarButtonItem]
                    }
                }
            }
        }
        
        //Right navigationItem
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "settings"), style: .done, target: self, action: #selector(shelfManagement))
    }
    @objc func shelfManagement() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = mainStoryboard.instantiateViewController(withIdentifier: "ShelfManagement") as? ShelfManagement {
            vc.memberID = memberId
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func setup() {
        title = String.localizedString(key: "TabbarShelf")
    
        view.addSubview(scrollView)
        view.addSubview(shelfView)
        view.addConstraintsWithFormat(format: "H:|-12-[v0]-12-|", views: shelfView)
        view.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: scrollView)
        view.addConstraintsWithFormat(format: "V:|-0-[v0(74)]-12-[v1]-20-|", views: scrollView, shelfView)
        let images = [#imageLiteral(resourceName: "public.png"), #imageLiteral(resourceName: "member.png"), #imageLiteral(resourceName: "purchase.png"), #imageLiteral(resourceName: "course.png")]
        var rect1 = CGRect(x: gap, y: 16, width: 150, height: 50)
        var rect2 = CGRect(x: gap, y: 70, width: 150, height: 4)
        for i in 0...3 {
            if i == 3 {
                rect1.size.width = 200
                rect2.size.width = 200
            }
            let btn = ARButton(frame: rect1)
            btn.tag = i
            btn.tintColor = .lightGray
            btn.setImage(images[i], for: .normal)
            btn.addTarget(self, action: #selector(shelfTapped(_:)), for: .touchUpInside)
            shelfButtons.append(btn)
            scrollView.addSubview(btn)
            
            let v = UIView(frame: rect2)
            indicators.append(v)
            scrollView.addSubview(v)
            v.backgroundColor = .clear
            rect1.origin.x += (gap + 150)
            rect2.origin.x += (gap + 150)
        }
        scrollView.contentSize = CGSize(width: 5*gap + 3*150 + 200, height: 74)
    }

    
    @objc func shelfSelected(at index: Int) {
        indicators.forEach { (v) in
            v.backgroundColor = .clear
        }
        indicators[index].backgroundColor = .red
        curSelectedShelfId = [3,1,0,2][index]
    }
  
}

extension ShelfViewController {
    @objc func shelfTapped(_ sender: UIButton) {
        shelfView.subviews.forEach { (v) in
            v.removeFromSuperview()
        }
        curShelf = sender.tag
        //set background
        let images = SP.shared.currentLanguage() == "en" ? shelf_eng : shelf_thai
        shelfButtons.forEach { (btn) in
            if btn == sender {
                let image = images[btn.tag].withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
                btn.setImage(image, for: .normal)
            } else {
                let image = images[btn.tag].withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
                btn.setImage(image, for: .normal)
                btn.tintColor = UIColor.lightGray
            }
        }
        shelfSelected(at: sender.tag)
        let selectedShelfId = [Shelf_Public, Shelf_Member, Shelf_Sale, Shelf_Preset][sender.tag]
        let medias = DB.offlineMedias()
        let data = medias.filter({($0.shelfId ?? "" == selectedShelfId) && ($0.memberId ?? "" == memberId)})
        let groupData = Dictionary(grouping: data, by: {$0.libraryName ?? ""})
        shelfMedias = Array(groupData.values)
        libraryNames = Array(groupData.keys)
        
        DispatchQueue.main.async {
            if self.curShelf < 3 {
                self.shelfCollectionVC = ShelfCollectionViewController(libraryNames: self.libraryNames, shelfMedias: self.shelfMedias)
                self.shelfCollectionVC.openMedia = self.openMedia
                self.shelfCollectionVC.refreshBadge = self.refreshBadge
                self.shelfCollectionVC.removeMedia = self.removeMedia
                self.shelfCollectionVC.collectionView?.frame = self.shelfView.bounds
                self.shelfView.addSubview(self.shelfCollectionVC.collectionView!)
                //animate new media
                guard
                    let id = curSelectedMediaId,
                    let data = self.shelfMedias.first(where: { (roomData) -> Bool in
                        roomData.filter{$0.mediaId ?? "" == id}.count > 0
                    }),
                    let media = data.filter({$0.mediaId ?? "" == id}).first else {return}
                
                if let i = self.shelfMedias.firstIndex(of: data), let j = data.firstIndex(of: media) {
                    let indexPath = IndexPath(item: j, section: i)
                    //Overide reloadData to wait until reload data
                    self.shelfCollectionVC.collectionView?.reloadData {
                        self.shelfCollectionVC.collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
                    }
                }
            } else {
                self.presetShelfTableVC = PresetShelfTableViewController(memberId: self.memberId)
                self.presetShelfTableVC.refreshBadge = self.refreshBadge
                self.presetShelfTableVC.tableView.frame = self.shelfView.bounds
                self.shelfView.addSubview(self.presetShelfTableVC.tableView)
                self.presetShelfTableVC.openMediaDetail = self.openMedia
            }
            self.refreshBadge()
            
            //scrollView to selected shelf
            var rect = sender.frame
            rect.origin.x -= self.gap
            rect.size.width += 2*self.gap
            self.scrollView.scrollRectToVisible(rect, animated: true)
            SVProgressHUD.dismiss()
        }
    }
    func openMedia() {
        isNavigate = true
    }
    func refreshShelf() {
        shelfTapped(shelfButtons[curShelf])
    }
    func refreshBadge() {
        //update badge: first clear all badges
        self.shelfButtons[0].badge = 0
        self.shelfButtons[1].badge = 0
        self.shelfButtons[2].badge = 0
        self.shelfButtons[3].badge = 0
        
        self.shelfButtons[0].value = 0
        self.shelfButtons[1].value = 0
        self.shelfButtons[2].value = 0
        self.shelfButtons[3].value = 0
        //Badges
        let medias = DB.offlineMedias()
        let shelfData = Dictionary(grouping: medias.filter{$0.isNewDownload && $0.memberId ?? "" == self.memberId}, by: {$0.shelfId ?? ""})
        for (key, value) in shelfData {
            switch key {
            case Shelf_Public:self.shelfButtons[0].badge = value.count
            case Shelf_Member:self.shelfButtons[1].badge = value.count
            case Shelf_Sale:self.shelfButtons[2].badge = value.count
            case Shelf_Preset:self.shelfButtons[3].badge = value.count
            default: break
            }
        }
        //Value
        let allData = Dictionary(grouping: medias.filter{$0.memberId ?? "" == self.memberId}, by: {$0.shelfId ?? ""})
        for (key, value) in allData {
            switch key {
            case Shelf_Public:self.shelfButtons[0].value = value.count
            case Shelf_Member:self.shelfButtons[1].value = value.count
            case Shelf_Sale:self.shelfButtons[2].value = value.count
            case Shelf_Preset:self.shelfButtons[3].value = value.count
            default: break
            }
        }
        //if guest only Public has value
        if !MemberInfo.isSignin() {
            self.shelfButtons[1].badge = 0
            self.shelfButtons[2].badge = 0
            self.shelfButtons[3].badge = 0
            
            self.shelfButtons[1].value = 0
            self.shelfButtons[2].value = 0
            self.shelfButtons[3].value = 0
        }
    }
}

//MARK:- Sync Medias
extension ShelfViewController {
    func syncMedias(completion:@escaping ()->()) {
        //sync Public -> Member -> Preset
        DB.syncPublicShelf {
            RoomAPI.shared.syncMemberMedia(memberId: self.memberId) {[weak self] (success) in
                if success, let memberId = self?.memberId {
                    RoomAPI.shared.syncPresetMedia(memberId: memberId) { (data) in
                        if let data = data {
                            DispatchQueue.main.async {
                                DB.syncPresetShelf(data: data, memberId: memberId){
                                    completion()
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                completion()
                            }
                        }
                    }
                } else {
                    completion()
                }
            }
        }
    }
}
//MARK:- Remove Media
extension ShelfViewController {
    @objc func removeMedia(_ indexPath: IndexPath) {
        let offMedia = shelfMedias[indexPath.section][indexPath.item]
        self.confirmAlert(title: "คืน หนังสือ - สื่อ", messgae: "\(offMedia.mediaTitle ?? "")", okTitle: "คืน") { (actBtn) in
            if actBtn == .OK {
                RoomAPI.shared.returnMedia(media: offMedia, completion: {[weak self] (success) in
                    if success {
                        DispatchQueue.main.async {
                            DB.deleteOfflineMedia(media: offMedia)
                            self?.refreshBadge()
                            self?.refreshShelf()
                            
                        }
                    }
                })
            } else {
                if let vc = self.shelfCollectionVC {
                    vc.collectionView?.reloadItems(at: [indexPath])
                }
            }
        }
        
    }

}
//MARK:- Menu Tapped
extension ShelfViewController {
    @objc func menuTapped() {
        if let appDel = UIApplication.shared.delegate as? AppDelegate {
            appDel.drawerContainer.toggle(.left, animated: true) { (_) in
                
            }
        }
    }
    @objc func libraryTapped() {
        guard let roomId = UserDefaults.standard.string(forKey: KeyCuurrentUsedLribaryID), let roomData = DB.getRoom(id: roomId)  else {return}
        let memberId = SP.shared.getMemberId()
        RoomAPI.shared.libraryInfo(roomId: roomId, memberId: memberId) { (json) in
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            if let json = json, let libraryDetailVC = mainSB.instantiateViewController(withIdentifier: "LibraryDetailVC") as? LibraryDetailVC {
                libraryDetailVC.libraryTitle = roomData.name ?? ""
                libraryDetailVC.libraryID = roomId
                libraryDetailVC.jsonLibraryInfo = json
                RoomAPI.shared.libraryMediaItems(roomId: roomId, completion: {[weak self] (json) in
                    if let json = json {
                        libraryDetailVC.jsonMediaItems = json
                        DispatchQueue.main.async {
                            self?.navigationController?.pushViewController(libraryDetailVC, animated: true)
                        }
                    }
                })
            }
        }
    }
    
    func setupLeftMenuNotifications() {
        NotificationCenter.default.addObserver(forName: .libraryChanged, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                self?.tabBarController?.selectedIndex = 0
            }
        }
        
        NotificationCenter.default.addObserver(forName: .signOut, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let alertTitle = "\(String.localizedString(key: "Youwant")) \(String.localizedString(key: "SignOut"))"
                self?.confirmAlert(title: alertTitle, messgae: "", okTitle: "OK", selectedAction: { (btn) in
                    if btn == .OK {
                        let memberId = DBManager.selectMemberID() ?? ""
                        MemberAPi.shared.memberSignOut(memberId: memberId, completion: {[weak self] (code) in
                            if code == 200 {
                                DispatchQueue.main.sync {
                                    DBManager.deleteAllRowInMemberTable()
                                    DBManager.deleteAllRowInLibraryTable()
                                    UserDefaults.standard.removeObject(forKey: LastLeaveApp)
                                    UserDefaults.standard.removeObject(forKey: AutoSyncPdfAnnatationKey)
                                    UserDefaults.standard.synchronize()
                                    
                                    //Line
                                    Login.sharedInstance().canLoginWithLineApp()
                                    //FB
                                    FBSDKLoginManager.init().logOut()
                                    
                                    //start splash
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    if let vc = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreen") as? SplashScreen {
                                        self?.view.window?.rootViewController = vc
                                    }
                                }
                            }
                        })
                    }
                })
            }
        }
        NotificationCenter.default.addObserver(forName: .setting, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let vc = SettingViewController()
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: {
                    
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: .signIn, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "SignInVC") as? SignInVC {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .help, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "Help") as? Help {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }            }
        }
        
        NotificationCenter.default.addObserver(forName: .about, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let mainSB = UIStoryboard(name: "Main", bundle: nil)
                if let vc = mainSB.instantiateViewController(withIdentifier: "AboutTVC") as? AboutTVC {
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .history, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let vc = HistoryViewController()
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .memberProfile, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                guard let memberId = DBManager.selectMemberID() else {return}
                let vc = MemberProfileViewController(memberId: memberId)
                let nav = UINavigationController(rootViewController: vc)
                self?.present(nav, animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .redeemCourse, object: nil, queue: nil) {[weak self] (_) in
            if self?.tabBarController?.selectedIndex == 2 {
                let vc = RedeemViewController()
                vc.modalPresentationStyle = .overFullScreen
                self?.present(vc, animated: true, completion: nil)
            }
        }
    }
}


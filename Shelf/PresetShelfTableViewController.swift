//
//  PresetShelfTableViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit

class PresetShelfTableViewController: UITableViewController {
    var libNames: [String] = [] {
        didSet {
            libShowRows = libNames.map{!$0.isEmpty}
        }
    }
    var libShowRows: [Bool] = []
    var libSubjects: [[String]] = [] {
        didSet {
            subjectShowRows = libSubjects.map{$0.map{!$0.isEmpty}}
        }
    }
    var subjectShowRows:[[Bool]] = []
    var subjectMedias: [[[OfflineShelfData]]] = []
    var memberId: String
    //Call back
    var refreshBadge: (()->())?
    var openMediaDetail: (()->())?
    init(memberId: String) {
        self.memberId = memberId
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(SubjectTableViewCell.self, forCellReuseIdentifier: SubjectTableViewCell.identification)
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        tableView.showsVerticalScrollIndicator = false
        DB.getPresetShelfData(memberId: memberId) { (libNames, subjects, medias) in
            self.libNames = libNames
            self.libSubjects = subjects
            self.subjectMedias = medias
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return libNames.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return libSubjects[section].count
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard subjectShowRows[indexPath.section][indexPath.row] else {return 50}
        let n: CGFloat = 2
        let w = (tableView.frame.width - (n-1)*12)/n
        return (w*4/3 + 88 + 33 + 12)
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 45))
        headerView.backgroundColor = .white
        //Toggle button
        let btn = UIButton()
        btn.setImage(UIImage(named: "toggle"), for: .normal)
        if libShowRows[section] {
            btn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        btn.tag = section
        btn.addTarget(self, action: #selector(toggleLibary(_:)), for: .touchUpInside)
        btn.tintColor = .gray
        
        let lblHeader = UILabel()
        lblHeader.font = UIFont(name: "\(mainFontHeader)", size: 20) ?? UIFont.preferredFont(forTextStyle: .title2)
        lblHeader.textColor = .darkGray
        if SP.shared.currentLanguage() == "en" {
            lblHeader.text = "Library: \(libNames[section])"
        } else {
            lblHeader.text = "ห้องสมุด: \(libNames[section])"
        }
        
        
        let sepView = UIView()
        sepView.backgroundColor = .darkGray
        headerView.addSubview(lblHeader)
        headerView.addSubview(btn)
        headerView.addSubview(sepView)
        headerView.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: lblHeader)
        headerView.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: sepView)
        headerView.addConstraintsWithFormat(format: "V:[v0]-0-[v1(1)]-12-|", views: lblHeader, sepView)
        
        headerView.addConstraintsWithFormat(format: "V:[v0(36)]-12-|", views: btn)
        headerView.addConstraintsWithFormat(format: "H:[v0(36)]-0-|", views: btn)
        
        return headerView
    }
    @objc func toggleLibary(_ sender: UIButton) {
        let curStatus = self.libShowRows[sender.tag]
        UIView.animate(withDuration: 0.3, animations: {
            if curStatus {
                sender.transform = .identity
            } else {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
        }) { (_) in
            //toggle lib value
            self.libShowRows[sender.tag] = !curStatus
            
            //toggle subject value
            let n = self.subjectShowRows[sender.tag].count
            self.subjectShowRows[sender.tag] = Array(repeating: self.libShowRows[sender.tag], count: n)
            self.tableView.reloadSections([sender.tag], with: .automatic)
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubjectTableViewCell.identification
            , for: indexPath) as! SubjectTableViewCell
        let libIndex = indexPath.section
        let subjectIndex = indexPath.row
        
        cell.indexPath = indexPath
        cell.toggleRowHidden = self.toggleRowHidden
        cell.showRow = subjectShowRows[libIndex][subjectIndex]
        cell.subject = libSubjects[libIndex][subjectIndex]
        cell.medias = subjectMedias[libIndex][subjectIndex]
        //Call back
        if let refreshBadge = self.refreshBadge {
            cell.refreshBadge = refreshBadge
        }
        if let openMediaDetail = self.openMediaDetail {
            cell.openMediaDetail = openMediaDetail
        }
        cell.mediaSelected = mediaDidSelected
        //Toggle button
        UIView.animate(withDuration: 0.3) {
            cell.toggleView.setTitle(String.localizedString(key:self.subjectShowRows[libIndex][subjectIndex] ? "hide" : "show" ), for: .normal)
        }
        return cell
    }
    
    func toggleRowHidden(show: Bool, indexPath: IndexPath?)  {
        if let indexPath = indexPath {
            subjectShowRows[indexPath.section][indexPath.row] = show
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
            //update library toggle button
            let countShow =  (subjectShowRows.flatMap{$0.map{$0}}).filter{$0 == true}.count
            libShowRows[indexPath.section] = countShow > 0
        }
    }
    
    func mediaDidSelected(media: OfflineShelfData, collectionView: UICollectionView, indexPath: IndexPath) {
        RoomAPI.shared.activateCourseStartDate(mediaId: media.mediaId ?? "") { (code, message1) in
            print("\(code): \(message1)")
        }
        let mediaType = media.mediaType ?? ""
        var isFileExist = false
        if ["D", "M"].contains(mediaType) {
            isFileExist = SP.shared.isFileExist(media: media)
        } else if ["V", "A"].contains(mediaType) {
            isFileExist = (media.fileUrl ?? "").count > 0
        }
        
        if !isFileExist {
            media.isNewDownload = false
            DB.saveData()
            if let refreshBadge = self.refreshBadge {
                refreshBadge()
            }
            collectionView.reloadItems(at: [indexPath])
            //Hide cardView
            if let cell = collectionView.cellForItem(at: indexPath) as? OfflineMediaCollectionViewCell {
                cell.cardView.isHidden = true
            }
           
            if let mediaId = media.mediaId, let roomId = media.libraryId {
                let vc = MediaDetailViewController(mediaId: mediaId, roomId: roomId)
                vc.isNavigation = true
                vc.shelfSubject = media.subjectId
                vc.title = media.mediaTitle ?? ""
                vc.completion = {(_) in
                    DispatchQueue.main.async {
                        collectionView.reloadItems(at: [indexPath])
                        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
                    }
                }
                let nav = UINavigationController(rootViewController: vc)
                if let topVC = SP.shared.topViewController() {
                    if let vc = topVC as? ShelfViewController {
                        vc.isNavigate = true
                    }
                    topVC.present(nav, animated: true, completion:nil)
                }
            }
        }
    }
}


class SubjectTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var curIndexPath: IndexPath!
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    lazy var headerView: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont(name: "\(mainFontHeader)", size: 16) ?? UIFont.preferredFont(forTextStyle: .title2)
        lbl.textColor = UIColor.gray
        lbl.numberOfLines = 2
        return lbl
    }()
    lazy var toggleView: UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont(name: "\(mainFontHeader)", size: 16) ?? UIFont.preferredFont(forTextStyle: .title2)
        btn.setTitleColor(.lightGray, for: .normal)
        btn.addTarget(self, action: #selector(toggleShowDetail(_:)), for: .touchUpInside)
        btn.clipsToBounds = true
        return btn
    }()
    var subject: String = ""
    var showRow: Bool = true
    var indexPath: IndexPath?
    var medias:[OfflineShelfData] = [] {
        didSet {
            let font = UIFont(name: "\(mainFontHeader)", size: 16) ?? UIFont.preferredFont(forTextStyle: .title2)
            let subjTitle = SP.shared.currentLanguage() == "en" ? "Subject:" : "ชั้นเรียน:"
            let itemTitle = SP.shared.currentLanguage() == "en" ? "items" : "รายการ"
            let attString1 = NSMutableAttributedString(string: "\(subjTitle) \(subject)\n", attributes: [NSAttributedString.Key.font:font,
                                                                                        NSAttributedString.Key.foregroundColor:UIColor.darkGray])
            let attString2 = NSAttributedString(string: "\(medias.count) \(itemTitle)", attributes: [NSAttributedString.Key.font:font,
                                                                                       NSAttributedString.Key.foregroundColor:UIColor.lightGray])
            attString1.append(attString2)
            headerView.attributedText = attString1
            collectionView.reloadData()
        }
    }
    var toggleRowHidden:((Bool, IndexPath?)->())?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    //Call back
    var refreshBadge: (()->())?
    var openMediaDetail: (()->())?
    var mediaSelected: ((OfflineShelfData, UICollectionView, IndexPath)->())?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        contentView.addSubview(collectionView)
        contentView.addSubview(headerView)
        contentView.addSubview(toggleView)
        
        contentView.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: collectionView)
        contentView.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: headerView)
        contentView.addConstraintsWithFormat(format: "V:|-0-[v0(50)][v1]-0-|", views: headerView, collectionView)
        collectionView.register(UINib.init(nibName: "OfflineMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: OfflineMediaCollectionViewCell.identification)
        
        contentView.addConstraintsWithFormat(format: "H:[v0(60)]-0-|", views: toggleView)
        contentView.addConstraintsWithFormat(format: "V:|-8-[v0(34)]", views: toggleView)

        contentView.bringSubviewToFront(toggleView)
    }
    
    //MARK:- UICollectionView dataSource & delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OfflineMediaCollectionViewCell.identification, for: indexPath) as! OfflineMediaCollectionViewCell
        let media = medias[indexPath.item]
        cell.curMedia = media
        //show only read button
        cell.btnreturn.setImage(nil, for: .normal)
        cell.btnRead.tag = indexPath.item
        cell.btnRead.addTarget(self, action: #selector(openMedia(_:)), for: .touchUpInside)

        
        let filePath = SP.shared.documentPath().appendingPathComponent(media.coverPath ?? "").path
        DispatchQueue.main.async {
            if FileManager.default.fileExists(atPath: filePath) {
                cell.coverImageView.image = UIImage(contentsOfFile: filePath)
            } else {
                if let image = SP.shared.downloadMediaCover(curMedia: media) {
                    cell.coverImageView.image = image
                } else {
                    cell.coverImageView.image = UIImage(named: "default_media_cover.png")
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Toggle button
        let deviceName = UIDevice.modelName
        let n: CGFloat = deviceName.contains("iPad") ? 4.5 : deviceName.contains("iPhone 5") ? 2 : 2.5
        let w = (contentView.frame.width - (n-1)*12)/n
        return CGSize(width: w, height: showRow ? (w*4/3 + 88) : 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if let indexPaths = collectionView.indexPathsForSelectedItems, indexPaths.contains(indexPath) {
            collectionView.deselectItem(at: indexPath, animated: true)
            curIndexPath = nil
            return false
        }
        curIndexPath = indexPath
        return true
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let offMedia = self.medias[indexPath.item]
        if let mediaSelected = self.mediaSelected {
            mediaSelected(offMedia, collectionView, indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let mediaId = curSelectedMediaId, let shelfId = curSelectedShelfId else {return}
        let media = self.medias[indexPath.item]
        if (media.mediaId ?? "") == mediaId && (media.shelfId ?? "") == shelfId.toString() {
            if let cell = cell as? OfflineMediaCollectionViewCell {
                cell.newDownloadImgView.alpha = 0
                cell.mediaTypeImgView.alpha = 0
                cell.btnShareAnnotation.alpha = 0
                cell.coverImageView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 3, delay: 0.3, usingSpringWithDamping: 0.15, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                        cell.coverImageView.transform = .identity
                    }) { (success) in
                        if success {
                            UIView.animate(withDuration: 0.3, animations: {
                                cell.newDownloadImgView.alpha = 1
                                cell.mediaTypeImgView.alpha = 1
                                cell.btnShareAnnotation.alpha = 1
                            })
                        }
                    }
                }
            }
        }
    }
    
    @objc func toggleShowDetail(_ sender: UIButton) {
        showRow.toggle()
        if let toggle = toggleRowHidden {
            toggle(showRow, indexPath)
        }
    }
    
    @objc func openMedia(_ sender:UIButton) {
        guard let indexPath = curIndexPath else {return}
        //check if media == current selected
        let media = medias[indexPath.item]
        if let mediaId = curSelectedMediaId, let shelfId = curSelectedShelfId,
            (media.mediaId ?? "") == mediaId && (media.shelfId ?? "") == shelfId.toString() {
            curSelectedMediaId = nil
            curSelectedShelfId = nil
        }
        
        SP.shared.loadSVProgressHUD(status: "Loading...") {
            //open Media detail
            if let openPreset = self.openMediaDetail {
                openPreset()
            }
            let offMedia = self.medias[indexPath.item]
            offMedia.isNewDownload = false
            DB.saveData()
            self.collectionView.reloadItems(at: [indexPath])
            if let refreshBadge = self.refreshBadge {
                refreshBadge()
            }
            
            let type = (offMedia.mediaType ?? "").uppercased()
            switch type {
            case "D", "M": self.readMedia(offMedia: offMedia)
            case "YU": self.openYoutube(offMedia: offMedia)
            case "A", "V": self.playMedia(offMedia: offMedia)
            case "EP", "EL", "ES", "EV": self.openExternalLink(offMedia: offMedia)
            default:break
                
            }
        }
    }
    fileprivate func loadPdfAds(_ roomId: String, _ mediaId: String, _ pdfDoc: PDFDocument, _ docPath: URL, _ pdfPassword: String) {
        SP.shared.loadPdfAds(roomId: roomId, mediaId: mediaId) { (pdfAds) in
            if let ads = pdfAds {
                let info = PdfAnnotationInfo(roomId: roomId,
                                             mediaId: mediaId,
                                             pdfDocument: pdfDoc,
                                             filePath: docPath.path,
                                             uploadPath: "",
                                             removeFileOnExit: false,
                                             canUpload: true,
                                             isPreview: false,
                                             pdfURL: docPath,
                                             password: pdfPassword,
                                             ads: ads)
                let pdfVC = PdfViewController(info: info)
                let nav = UINavigationController(rootViewController: pdfVC)
                if let topVC = SP.shared.topViewController() {
                    topVC.present(nav, animated: true){
                        ARProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    func readMedia(offMedia: OfflineShelfData) {
        guard let path = offMedia.filePath else {
            return
        }
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...")
        }
        let docPath = SP.shared.documentPath().appendingPathComponent(path)
        let roomId = offMedia.libraryId ?? ""
        let mediaId = offMedia.mediaId ?? ""
        let fileName = offMedia.originalFileName ?? ""
        let fileType = (docPath.lastPathComponent as NSString).pathExtension
        if fileType.lowercased() == "pdf" {
            var pdfPassword = ""
            //try local file
            if let path = SP.shared.getLocalMediaPath(roomId: roomId, fileUrl: offMedia.fileUrl ?? "") {
                let url = URL(fileURLWithPath: path)
                if let pdfDoc = PDFDocument(url: url) {
                    if pdfDoc.isLocked {
                        pdfPassword = Utility.generatePassword(fileName)
                        guard pdfDoc.unlock(withPassword: pdfPassword) else {
                            SVProgressHUD.showError(withStatus: "Document locked!")
                            return
                        }
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    } else {
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    }
                }
                //else try online
            } else if InternetConnection.isConnectedToNetwork(),
                let docPath = URL(string: offMedia.fileUrl ?? ""),
                let pdfDoc = PDFDocument(url: docPath) {
                var pdfPassword = ""
                if pdfDoc.isLocked {
                    pdfPassword = Utility.generatePassword(fileName)
                    guard pdfDoc.unlock(withPassword: pdfPassword) else {return}
                }
                loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
            } else {
                SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
            }
        } else {
            SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
        }
    }
    
    func openYoutube(offMedia: OfflineShelfData) {
        guard let roomId = offMedia.libraryId,
            let id = NSString(string: offMedia.fileUrl ?? "").lastPathComponent.components(separatedBy: "?v=").last
            else {return}
        let yt = YT(roomID: roomId, id: id)
        let vc = YoutubeViewController(yt: yt)
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: true, completion: nil)
        }
    }
    func openExternalLink(offMedia: OfflineShelfData) {
        
        let vc = WebOSViewController(urlPath: offMedia.fileUrl ?? "", title: offMedia.mediaTitle ?? "")
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: true) {
                if (offMedia.mediaType ?? "").uppercased() == "ES" {
                    SVProgressHUD.dismiss()
                }
                RoomAPI.shared.activateCourseStartDate(mediaId: offMedia.mediaId ?? "", completion: { (code, message1) in
                    //Activated media
                })
            }
        }
    }
    
    @objc func playMedia(offMedia: OfflineShelfData) {
        guard let roomId = offMedia.libraryId,
            let mediaId = offMedia.mediaId,
            let mediaFileUrl = offMedia.fileUrl,
            let topVC = SP.shared.topViewController()
        else {
            openMediaDetail(offMedia: offMedia)
            return
        }
        let type = offMedia.mediaType?.uppercased() == "V" ? "Video" : "Audio"
        RoomAPI.shared.getMediaAds(roomId: roomId, adsType: type, mediaId: mediaId) { (json) in
            if let json = json {
                let vc = Area5ViewController(roomId: roomId, mediaPath: mediaFileUrl, json: json, title: offMedia.mediaTitle ?? "")
                let nav = UINavigationController(rootViewController: vc)
                SP.shared.loadSVProgressHUD(status: "Loading...", completing: {
                    topVC.present(nav, animated: true) {
                        RoomAPI.shared.activateCourseStartDate(mediaId: mediaId, completion: { (code, message1) in
                            //Activated media
                        })
                    }
                })
            } else {
                SVProgressHUD.showError(withStatus: "Cannot play media!")
            }
        }
    }
    
    func openMediaDetail(offMedia: OfflineShelfData) {
        //open Media detail
        SVProgressHUD.dismiss()
        if let mediaId = offMedia.mediaId, let roomId = offMedia.libraryId {
            let vc = MediaDetailViewController(mediaId: mediaId, roomId: roomId)
            vc.isNavigation = true
            vc.shelfSubject = offMedia.subjectId
            vc.title = offMedia.mediaTitle ?? ""
            vc.completion = { (_) in
                
            }
            let nav = UINavigationController(rootViewController: vc)
            if let topVC = SP.shared.topViewController() {
                topVC.present(nav, animated: true, completion:nil)
            }
        }
    }
}

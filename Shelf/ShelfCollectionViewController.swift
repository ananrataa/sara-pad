//
//  ShelfCollectionViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 23/11/2561 BE.
//  Copyright © 2561 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit
import PDFKit
import SafariServices

class ShelfCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var libraryNames: [String]
    var shelfMedias: [[OfflineShelfData]]
    var curIndexPath: IndexPath!
    var refreshBadge: (()->())?
    var removeMedia:((IndexPath)->())?
    var openMedia: (()->())?
    //MARK:- Buy/Rent
    lazy var userBooksLimit:Int = {
        guard let data = Utility.checkUserLimitDayAndMedia() as? [Int], data.count > 1 else {return Int(MaximunFilesDownload)}
        return data[1]
    }()
    
    init(libraryNames: [String], shelfMedias: [[OfflineShelfData]]) {
        self.libraryNames = libraryNames
        self.shelfMedias = shelfMedias
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        super.init(collectionViewLayout: layout)
        collectionView?.backgroundColor = .clear
        collectionView?.showsVerticalScrollIndicator = false
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(UINib.init(nibName: "OfflineMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: OfflineMediaCollectionViewCell.identification)
        collectionView?.register(SectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeaderView.identification)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SVProgressHUD.dismiss()
    }
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return libraryNames.count
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shelfMedias[section].count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OfflineMediaCollectionViewCell.identification, for: indexPath) as! OfflineMediaCollectionViewCell
        let media = shelfMedias[indexPath.section][indexPath.item]
        cell.curMedia = media
        
        let filePath = SP.shared.documentPath().appendingPathComponent(media.coverPath ?? "").path
        DispatchQueue.main.async {
            if FileManager.default.fileExists(atPath: filePath) {
                cell.coverImageView.image = UIImage(contentsOfFile: filePath)
            } else if let image = SP.shared.downloadMediaCover(curMedia: media) {
                cell.coverImageView.image = image
            } else {
                cell.coverImageView.image = UIImage(named: "default_media_cover.png")
            }
        }
        //set button
        cell.btnRead.tag = indexPath.item
        cell.btnRead.addTarget(self, action: #selector(openMedia(_:)), for: .touchUpInside)
        
        if media.shelfId == "0" {
            //Sale
            cell.btnreturn.setImage(#imageLiteral(resourceName: "shelf_extension.png"), for: .normal)
        } else {
            cell.btnreturn.setImage(#imageLiteral(resourceName: "shelf_return.png"), for: .normal)
        }
        
        //return or rent more
        cell.btnreturn.tag = indexPath.item
        if media.shelfId == Shelf_Sale {
            if media.isOwnPurchase() {
                cell.btnreturn.alpha = 0
            } else {
                cell.btnreturn.alpha = 1
                cell.btnreturn.addTarget(self, action: #selector(mediaPurchase), for: .touchUpInside)
            }
        } else {
            cell.btnreturn.addTarget(self, action: #selector(removeMedia(_:)), for: .touchUpInside)
        }
        
        //Shared Annotation
//        cell.btnShareAnnotation.isHidden = !DBManager.isMember()
//        cell.btnShareAnnotation.addTarget(self, action: #selector(shareAnnotation), for: .touchUpInside)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 33+12)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let deviceName = UIDevice.modelName
        let n: CGFloat = deviceName.contains("iPad") ? 4 : (deviceName.contains("iPhone 5") ? 2 : 3)
        let w = (collectionView.frame.width - (n-1)*12)/n
        return CGSize(width: w, height: w*4/3 + 88)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionHeaderView.identification, for: indexPath) as! SectionHeaderView
            if SP.shared.currentLanguage() == "en" {
                headerView.headerText = "Library: \(libraryNames[indexPath.section])"
            } else {
                headerView.headerText = "ห้องสมุด: \(libraryNames[indexPath.section])"
            }
            headerView.seperateView.backgroundColor = .darkGray
            return headerView
        default:
            return UICollectionReusableView()
            //assert(false, "Unexpected element kind")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if let indexPaths = collectionView.indexPathsForSelectedItems, indexPaths.contains(indexPath) {
            collectionView.deselectItem(at: indexPath, animated: true)
            curIndexPath = nil
            return false
        }
        curIndexPath = indexPath
        //check if file exist
        let offMedia = shelfMedias[indexPath.section][indexPath.item]
        if URL(string: offMedia.fileUrl ?? "") == nil {
            offMedia.isNewDownload = false
            DB.saveData()
            //open Media detail
            SVProgressHUD.dismiss()
            if let mediaId = offMedia.mediaId, let roomId = offMedia.libraryId {
                let vc = MediaDetailViewController(mediaId: mediaId, roomId: roomId)
                vc.isNavigation = true
                vc.shelfSubject = offMedia.subjectId
                vc.completion = {[weak self] (_) in
                    DispatchQueue.main.async {
                        if let indexPath = self?.curIndexPath {
                            self?.collectionView!.reloadItems(at: [indexPath])
                            self?.collectionView!.scrollToItem(at: indexPath, at: .left, animated: true)
                        }
                    }
                }
                let nav = UINavigationController(rootViewController: vc)
                if let topVC = SP.shared.topViewController() {
                    topVC.present(nav, animated: true, completion:nil)
                }
            }
            return false
        }
        return true
    }

    //Animated new Media
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let mediaId = curSelectedMediaId, let shelfId = curSelectedShelfId else {return}
        let media = shelfMedias[indexPath.section][indexPath.item]
        if (media.mediaId ?? "") == mediaId && (media.shelfId ?? "") == shelfId.toString() {
            if let cell = cell as? OfflineMediaCollectionViewCell {
                cell.newDownloadImgView.alpha = 0
                cell.mediaTypeImgView.alpha = 0
//                cell.btnShareAnnotation.alpha = 0
                cell.coverImageView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                UIView.animate(withDuration: 3, delay: 0.3, usingSpringWithDamping: 0.15, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    cell.coverImageView.transform = .identity
                }) { (success) in
                    if success {
                        UIView.animate(withDuration: 0.3, animations: {
                            cell.newDownloadImgView.alpha = 1
                            cell.mediaTypeImgView.alpha = 1
//                            cell.btnShareAnnotation.alpha = 1
                        })
                    }
                }
            }
        }
    }
}

//MARK:- Open Media
extension ShelfCollectionViewController {
    @objc func openMedia(_ sender:UIButton) {
        guard let indexPath = curIndexPath else {return}
        //check if media == current selected
        let media = shelfMedias[indexPath.section][indexPath.item]
        if let mediaId = curSelectedMediaId, let shelfId = curSelectedShelfId,
            (media.mediaId ?? "") == mediaId && (media.shelfId ?? "") == shelfId.toString() {
            curSelectedMediaId = nil
            curSelectedShelfId = nil
        }
        
        SP.shared.loadSVProgressHUD(status: "Loading...") {
            let offMedia = self.shelfMedias[indexPath.section][indexPath.item]
            offMedia.isNewDownload = false
            DB.saveData()
            self.collectionView?.reloadItems(at: [indexPath])
            if let refreshBadge = self.refreshBadge {
                refreshBadge()
            }
            
            let type = (offMedia.mediaType ?? "").uppercased()
            switch type {
            case "D", "M": self.readMedia(offMedia: offMedia)
            case "YU": self.openYoutube(offMedia: offMedia)
            case "EP", "EL", "ES", "EV": self.openExternalLink(offMedia: offMedia)
            default:break
                
            }
        }
    }
    
    fileprivate func loadPdfAds(_ roomId: String, _ mediaId: String, _ pdfDoc: PDFDocument, _ docPath: URL, _ pdfPassword: String) {
        SP.shared.loadPdfAds(roomId: roomId, mediaId: mediaId) { (pdfAds) in
            if let ads = pdfAds {
                var memberId = UserDefaults.standard.string(forKey: KeyDeviceCode) ?? ""
                var isMember = false
                if MemberInfo.isSignin() || DBManager.isMember() {
                    memberId = DBManager.selectMemberID()
                    isMember = true
                }
                let filePath = String(format: "%@/An_%@_%@_%@_ios.txt", PDFAnnotationFolderName, memberId, mediaId, roomId)
                let strUrl = String(format: "%@api/DeviceMedia/UploadAnnotation", hostIpRoomAPI)
                let info = PdfAnnotationInfo(roomId: roomId,
                                             mediaId: mediaId,
                                             pdfDocument: pdfDoc,
                                             filePath: filePath,
                                             uploadPath: strUrl,
                                             removeFileOnExit: false,
                                             canUpload: isMember,
                                             isPreview: false,
                                             pdfURL: docPath,
                                             password: pdfPassword,
                                             ads: ads)
                let pdfVC = PdfViewController(info: info)
                let nav = UINavigationController(rootViewController: pdfVC)
                if let topVC = SP.shared.topViewController() {
                    if let openMedia = self.openMedia {
                        openMedia()
                    }
                    topVC.present(nav, animated: true){
                        ARProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    
    func readMedia(offMedia: OfflineShelfData) {
        guard let docPath = URL(string: offMedia.fileUrl ?? "") else {
            SVProgressHUD.dismiss()
            if let mediaId = offMedia.mediaId, let roomId = offMedia.libraryId {
                let vc = MediaDetailViewController(mediaId: mediaId, roomId: roomId)
                vc.isNavigation = true
                vc.shelfSubject = offMedia.subjectId
                vc.completion = {[weak self] (_) in
                    DispatchQueue.main.async {
                        if let indexPath = self?.curIndexPath {
                            self?.collectionView!.reloadItems(at: [indexPath])
                            self?.collectionView!.scrollToItem(at: indexPath, at: .left, animated: true)
                        }
                    }
                }
                let nav = UINavigationController(rootViewController: vc)
                if let topVC = SP.shared.topViewController() {
                    if let openMedia = self.openMedia {
                        openMedia()
                    }
                    topVC.present(nav, animated: true, completion:nil)
                }
            }
            return
        }
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...")
        }
        let roomId = offMedia.libraryId ?? ""
        let mediaId = offMedia.mediaId ?? ""
        let fileName = offMedia.originalFileName ?? ""
        let fileType = (docPath.lastPathComponent as NSString).pathExtension
        
        if fileType.lowercased() == "pdf" {
            var pdfPassword = ""
            //try local file
            if let path = SP.shared.getLocalMediaPath(roomId: roomId, fileUrl: offMedia.fileUrl ?? "") {
                let url = URL(fileURLWithPath: path)
                if let pdfDoc = PDFDocument(url: url) {
                    if pdfDoc.isLocked {
                        pdfPassword = Utility.generatePassword(fileName)
                        guard pdfDoc.unlock(withPassword: pdfPassword) else {
                            SVProgressHUD.showError(withStatus: "Document locked!")
                            return
                        }
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    } else {
                        loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
                    }
                }
            //else try online
            } else if InternetConnection.isConnectedToNetwork(),
                let docPath = URL(string: offMedia.fileUrl ?? ""),
                let pdfDoc = PDFDocument(url: docPath) {
                var pdfPassword = ""
                if pdfDoc.isLocked {
                    pdfPassword = Utility.generatePassword(fileName)
                    guard pdfDoc.unlock(withPassword: pdfPassword) else {return}
                }
                loadPdfAds(roomId, mediaId, pdfDoc, docPath, pdfPassword)
            } else {
                SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
            }
        } else {
            SVProgressHUD.showError(withStatus: "Cannot open media.\nTry again later.")
        }
    }
    
    func openYoutube(offMedia: OfflineShelfData) {
        guard let roomId = offMedia.libraryId,
            let id = NSString(string: offMedia.fileUrl ?? "").lastPathComponent.components(separatedBy: "?v=").last
            else {return}
        let yt = YT(roomID: roomId, id: id)
        let vc = YoutubeViewController(yt: yt)
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            if let openMedia = self.openMedia {
                openMedia()
            }
            topVC.present(nav, animated: true, completion: nil)
        }
    }
    func openExternalLink(offMedia: OfflineShelfData) {
        let vc = WebOSViewController(urlPath: offMedia.fileUrl ?? "", title: offMedia.mediaTitle ?? "")
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            if let openMedia = self.openMedia {
                openMedia()
            }
            topVC.present(nav, animated: true) {
                DispatchQueue.main.async {
                    if ["ES", "EV"].contains((offMedia.mediaType ?? "").uppercased())  {
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
}

//MARK:- Remove Media
extension ShelfCollectionViewController {
    @objc func removeMedia(_ sender: UIButton) {
        guard let indexPath = curIndexPath else {return}
        if let removeMedia = self.removeMedia {
            removeMedia(indexPath)
        }
        
    }
}

class SectionHeaderView: UICollectionReusableView {
    let lblHeader: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont(name: "\(mainFontHeader)", size: 20) ?? UIFont.preferredFont(forTextStyle: .title2)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    let seperateView: UIView = {
        let v = UIView()
        return v
    }()
    var headerText: String? {
        didSet {
            lblHeader.text = headerText
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        addSubview(lblHeader)
        addSubview(seperateView)
//        self.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: lblHeader)
        self.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: seperateView)
        self.addConstraintsWithFormat(format: "V:[v0]-0-[v1(1.5)]-12-|", views: lblHeader, seperateView)
        lblHeader.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 4, left: 8, bottom: 4, right: 0))
    }
    
    
}

//MARK:- Media Purchase
extension ShelfCollectionViewController {
    @objc func mediaPurchase() {
        guard let indexPath = curIndexPath else {return}
        let media = shelfMedias[indexPath.section][indexPath.item]
        let roomId = media.libraryId ?? ""
        let mediaId = media.mediaId ?? ""
        RoomAPI.shared.browseMediaDetail(roomId: roomId, mediaId: mediaId, limit: userBooksLimit) {[weak self] (media) in
            if let curMedia = media {
                if curMedia.isBuy && curMedia.priceList.count > 0 {
                    let vc = PurchaseViewController(media: curMedia)
                    vc.startDownload = self?.startDownload
                    vc.modalPresentationStyle = .overCurrentContext
                    if let topVC = SP.shared.topViewController() {
                        DispatchQueue.main.async {
                            topVC.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    func startDownload() {
        guard let indexPath = curIndexPath else {return}
        let media = shelfMedias[indexPath.section][indexPath.item]
        let roomId = media.libraryId ?? ""
        let mediaId = media.mediaId ?? ""
        RoomAPI.shared.mediaDownload(mediaId: mediaId, roomId: roomId) {[weak self] (mediaInfo) in
            if let info = mediaInfo, let offMedia = self?.shelfMedias[indexPath.section][indexPath.item] {
                offMedia.dateExp = info.expDate
                self?.collectionView?.reloadItems(at: [indexPath])
            }
        }
    }
        
}

//MARK:- Share Annotation
extension ShelfCollectionViewController {
    @objc func shareAnnotation(_ sender: UIButton) {
        guard let cell = sender.superview?.superview as? UICollectionViewCell,
        let indexPath = self.collectionView?.indexPath(for: cell) else {return}
        
        let media = shelfMedias[indexPath.section][indexPath.item]
        let memberId = SP.shared.getMemberId()
        guard let roomId = media.libraryId, let mediaId = media.mediaId
            else {return}
        let vc = ShareAnnotationViewController(roomId: roomId, mediaId: mediaId, memberId: memberId)
        let nav = UINavigationController(rootViewController: vc)
        if let topVC = SP.shared.topViewController() {
            topVC.present(nav, animated: true) {
                
            }
        }
    }
}

//
//  ReadingViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 17/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class ReadingViewController: UIViewController {
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelBook: UILabel!
    @IBOutlet weak var labelPage: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelMinute: UILabel!
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var pickerView: ARChartPickerView!
    @IBOutlet weak var textViewInfo: UITextView!
    @IBOutlet weak var labelSum: UILabel!
    @IBOutlet weak var labelAverage: UILabel!
    @IBOutlet weak var vsEffectView: UIVisualEffectView!
    @IBOutlet weak var unitBook: UILabel!
    @IBOutlet weak var unitPage: UILabel!
    @IBOutlet weak var unitHour: UILabel!
    @IBOutlet weak var unitMinute: UILabel!
    @IBOutlet weak var unitSecond: UILabel!
    @IBOutlet weak var titleYTD: UILabel!
    @IBOutlet weak var titleAverage: UILabel!
    @IBOutlet weak var unitYTD: UILabel!
    @IBOutlet weak var unitAverage: UILabel!
    
    var plotData: [ReadPlotData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ประวัติการอ่าน"
        //Prepare data
        plotData = DataModel.getData()

        vsEffectView.alpha = 0.9
        textViewInfo.layer.cornerRadius = 7
        let fontSize: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 15:13
        let font =  UIFont(name: sukhumvitLightFont, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize, weight: .light)
        let att:  [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.white]
        if SP.shared.currentLanguage() == "en" {
            textViewInfo.attributedText = NSAttributedString(string: "⏱ NOP World Culture Score Index data reports that consumers in India are most likely to spend time reading, at an average of 10.42 hours per week, followed by consumers in Thailand and China (at 9.24 hours and 8 hours per week respectively).", attributes: att)
        } else {
        textViewInfo.attributedText = NSAttributedString(string: "⏱ ข้อมูลจาก NOP World Culture Score Index ระบุว่า ชาวอินเดียให้เวลากับการอ่านมากที่สุดในโลก 10.42 ชั่วโมงต่อคนต่อสัปดาห์ รองลงมาคือไทย 9.24 ชั่วโมง ตามด้วยจีน 8 ชั่วโมง ฟิลิปปินส์ 7.36 ชั่วโมง อียิปต์ 7.30 ชั่วโมง สาธารณรัฐเช็ก 7.24 ชั่วโมง รัสเซีย 7.06 ชั่วโมง ฝรั่งเศสและสวีเดนเท่ากันที่ 6.54 ชั่วโมง และซาอุดีอาระเบีย 6.48 ชั่วโมง", attributes: att)
        }
        //Localization
        if SP.shared.currentLanguage() == "en" {
            unitBook.text = "book"
            unitPage.text = "page"
            unitHour.text = "hrs."
            unitMinute.text = "mins."
            unitSecond.text = "secs."
            titleYTD.text = "YTD"
            titleAverage.text = "Average"
            unitYTD.text = "hrs."
            unitAverage.text = "hrs./week"
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        plotData = DataModel.sampleData()
        pickerView.items = plotData
        pickerView.onChartSelected = { (item) in
            self.labelDate.text = item.date
            self.labelBook.textAnimateCounter("\(item.books)", duration: 0.3)
            self.labelPage.textAnimateCounter("\(item.pages)", duration: 0.3)
            self.labelHour.textAnimateCounter("\(item.hours)", duration: 0.3)
            self.labelMinute.textAnimateCounter("\(item.minutes)", duration: 0.3)
            self.labelSecond.textAnimateCounter("\(item.seconds)", duration: 0.3)
        }
        textViewInfo.scrollRangeToVisible(NSRange(location: 0, length: 0))
        //Stats
        if plotData.count > 0 {
            let sumSeconds = plotData.map{$0.toSeconds()}.reduce(0, +)
            let sumHours = Double(Int(100*sumSeconds/3600))/100
            let days = Double(plotData.count)
            let avgHours = Double(Int(100*7*sumHours/days))/100
            labelSum.textAnimateCounter(sumHours.toString(dec: 2), duration: 0.4)
            labelAverage.textAnimateCounter(avgHours.toString(dec: 2), duration: 0.4)
            //scroll to current date
            pickerView.selectRow(plotData.count - 1, inComponet: 0, animated: true)
        } else {
            labelDate.text = SP.shared.currentLanguage() == "en" ? "No reading data." : "ยังไม่มีข้อมูลการอ่าน"
        }
        
        SVProgressHUD.dismiss()
    }
   
}

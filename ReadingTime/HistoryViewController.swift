//
//  HistoryViewController.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 18/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgImageView: UIImageView!
    
    let menuItems = ["Viewed", "Rented", "Reading", "Voted", "Following"]
    var historyVC: HistoryTVC?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = String.localizedString(key: "History")
        TransparentTheme.shared.applyNavigationBar(vc: self)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "x"), style: .done, target: self, action: #selector(done))
        collectionView.register(MenuHistoryCell.self, forCellWithReuseIdentifier: MenuHistoryCell.identification)
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: "HistoryTVC") as? HistoryTVC {
            historyVC = vc
            historyVC?.delegate = self
        }        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.contentOffset = CGPoint(x: 12, y: 0)
        collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        collectionView(collectionView, didSelectItemAt: IndexPath(item: 0, section: 0))
        bgImageView.alpha = 1
    }
    
    @objc func done() {
        SVProgressHUD.dismiss()
        dismiss(animated: true, completion: nil)
    }
}

extension HistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuHistoryCell.identification, for: indexPath) as! MenuHistoryCell
        cell.refreshView(key: menuItems[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        for v in contentView.subviews {
            v.removeFromSuperview()
        }
        if indexPath.item == 2 {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let vc = ReadingViewController(nibName: "ReadingViewControllerPad", bundle: nil)
                vc.view.frame = contentView.bounds
                contentView.addSubview(vc.view)
            } else {
                let vc = ReadingViewController()
                vc.view.frame = contentView.bounds
                contentView.addSubview(vc.view)
            }
        } else if let vc = historyVC {
            vc.tableView.frame = contentView.bounds
            contentView.addSubview(vc.tableView)
            vc.setHistoryType(Int32(indexPath.item))
        }
    }
}

extension HistoryViewController: HistoryDelegate {
    func historyDidSelected(_ mediaId: String!, roomId: String!) {
        let vc = MediaDetailViewController(mediaId: mediaId, roomId: roomId)
        navigationController?.pushViewController(vc, animated: true)
    }
}
class MenuHistoryCell: BaseCell {
    let labelMenu: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont(name: sukhumvitFont, size: 17)
        lbl.textColor = .white
        lbl.backgroundColor = .lightGray
        lbl.layer.cornerRadius = 15
        lbl.layer.masksToBounds = true
        return lbl
    }()
    
    override func setup() {
        backgroundColor = .clear
        addSubview(labelMenu)
        addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: labelMenu)
        addConstraintsWithFormat(format: "V:|-10-[v0]-10-|", views: labelMenu)
    }
    
    override var isSelected: Bool {
        didSet {
            labelMenu.backgroundColor = isSelected ? UIColor(hexString: ThemeTemplate.getThemeSet3()) : UIColor.lightGray
        }
    }
    func refreshView(key: String) {
        labelMenu.text = String.localizedString(key: key)
    }
}

//
//  DataModel.swift
//  SARA-PAD
//
//  Created by Anan Ratanasethakul on 22/4/19.
//  Copyright © 2019 MBOX Multimedia Co., Ltd. All rights reserved.
//

import Foundation

class DataModel {
    static func getData() -> [ReadPlotData]{
        var allData = DB.readingRecords()
        guard allData.count > 0 else {
            return []
        }
        allData.sort{$0.date ?? "" < $1.date ?? ""}
        let groupByDate = Dictionary(grouping: allData, by: {$0.date ?? ""})
        var plotData: [ReadPlotData] = []
        let locale = SP.shared.currentLanguage() == "en" ? "en_US_POSIX" : "th_TH"
        for (key, values) in groupByDate {
            if let dateUS = key.toDateWith(format: "yyyyMMdd", locale: "en_US_POSIX") {
                let yyyymmdd = dateUS.stringWithFormat("yyyyMMdd", locale: locale)
                let date = dateUS.stringWithFormat("d MMMM yyyy", locale: locale)
                let ddmm  = dateUS.stringWithFormat("d/M", locale: locale)
                let day = dateUS.dayOfWeek() - 1
                let groupByBook = Dictionary(grouping: values, by: {$0.mediaId})
                let books = groupByBook.keys.count
                let pages = values.count
                let duration = Int(values.map{$0.duration}.reduce(0, +))
                let hours = duration/3600
                let minutes = (duration % 3600) / 60
                let seconds = (duration % 3600) % 60
                let data = ReadPlotData(yyyymmdd: yyyymmdd, date: date, ddmm: ddmm, day: day, books: books, pages: pages, hours: hours, minutes: minutes, seconds: seconds, dateUS: dateUS)
                plotData.append(data)
            }
        }
        plotData.sort{$0.yyyymmdd < $1.yyyymmdd}
        plotData.forEach { (data) in
            print(data.dateUS.stringWithFormat("yyyyMMdd", locale: "en_US_POSIX"))
        }
        //insert blank data
        let dateStart = plotData[0].dateUS
        let dateEnd = Date().stringWithFormat("yyyyMMdd", locale: "en_US_POSIX")
        var n = 0
        var curDate = dateStart.stringWithFormat("yyyyMMdd", locale: "en_US_POSIX")
        while curDate.toInt() <= dateEnd.toInt() {
            if plotData.filter({$0.dateUS.stringWithFormat("yyyyMMdd", locale: "en_US_POSIX") == curDate}).count == 0 {
                if let d = curDate.toDateWith(format: "yyyyMMdd", locale: "en_US_POSIX") {
                    let yyyymmdd = d.stringWithFormat("yyyyMMdd", locale: locale)
                    let date = d.stringWithFormat("d MMMM yyyy", locale: locale)
                    let ddmm = d.stringWithFormat("d/M", locale: locale)
                    let day = d.dayOfWeek() - 1
                    plotData.append(ReadPlotData(yyyymmdd: yyyymmdd, date: date, ddmm: ddmm, day: day, books: 0, pages: 0, hours: 0, minutes: 0, seconds: 0, dateUS: curDate.toDateWith(format: "yyyyMMdd", locale: "en_US_POSIX")!))
                }
            }
            n += 1
            curDate = dateStart.nextDateWith(days: n)?.stringWithFormat("yyyyMMdd", locale: "en_US_POSIX") ?? ""
        }
        
        return plotData.sorted{$0.yyyymmdd < $1.yyyymmdd}
    }
    
    static func sampleData() -> [ReadPlotData] {
        let data1 = ReadPlotData(yyyymmdd: "25620401", date: "1 เมษายน 2562", ddmm: "1/4", day: 1, books: 1, pages: 5, hours: 0, minutes: 12, seconds: 37, dateUS: Date())
        let data2 = ReadPlotData(yyyymmdd: "25620402", date: "2 เมษายน 2562", ddmm: "2/4", day: 2, books: 0, pages: 0, hours: 0, minutes: 0, seconds: 0, dateUS: Date())
        let data3 = ReadPlotData(yyyymmdd: "25620403", date: "3 เมษายน 2562", ddmm: "3/4", day: 3, books: 2, pages: 25, hours: 0, minutes: 34, seconds: 39, dateUS: Date())
        let data4 = ReadPlotData(yyyymmdd: "25620404", date: "4 เมษายน 2562", ddmm: "4/4", day: 4, books: 1, pages: 15, hours: 0, minutes: 18, seconds: 23, dateUS: Date())
        let data5 = ReadPlotData(yyyymmdd: "25620405", date: "5 เมษายน 2562", ddmm: "5/4", day: 5, books: 1, pages: 10, hours: 0, minutes: 15, seconds: 49, dateUS: Date())
        let data6 = ReadPlotData(yyyymmdd: "25620406", date: "6 เมษายน 2562", ddmm: "6/4", day: 6, books: 1, pages: 5, hours: 0, minutes: 10, seconds: 11, dateUS: Date())
        let data7 = ReadPlotData(yyyymmdd: "25620407", date: "7 เมษายน 2562", ddmm: "7/4", day: 0, books: 2, pages: 35, hours: 1, minutes: 12, seconds: 17, dateUS: Date())
        let data8 = ReadPlotData(yyyymmdd: "25620408", date: "8 เมษายน 2562", ddmm: "8/4", day: 1, books: 1, pages: 7, hours: 0, minutes: 12, seconds: 8, dateUS: Date())
        let data9 = ReadPlotData(yyyymmdd: "25620409", date: "9 เมษายน 2562", ddmm: "9/4", day: 2, books: 1, pages: 15, hours: 0, minutes: 22, seconds: 46, dateUS: Date())
        let data10 = ReadPlotData(yyyymmdd: "25620410", date: "10 เมษายน 2562", ddmm: "10/4", day: 3, books: 1, pages: 5, hours: 0, minutes: 15, seconds: 44, dateUS: Date())
        let data11 = ReadPlotData(yyyymmdd: "25620411", date: "11 เมษายน 2562", ddmm: "11/4", day: 4, books: 1, pages: 5, hours: 0, minutes: 12, seconds: 37, dateUS: Date())
        let data12 = ReadPlotData(yyyymmdd: "25620412", date: "12 เมษายน 2562", ddmm: "12/4", day: 5, books: 0, pages: 0, hours: 0, minutes: 0, seconds: 0, dateUS: Date())
        let data13 = ReadPlotData(yyyymmdd: "25620413", date: "13 เมษายน 2562", ddmm: "13/4", day: 6, books: 2, pages: 25, hours: 0, minutes: 34, seconds: 39, dateUS: Date())
        
        let data14 = ReadPlotData(yyyymmdd: "25620414", date: "14 เมษายน 2562", ddmm: "14/4", day: 0, books: 1, pages: 5, hours: 0, minutes: 12, seconds: 37, dateUS: Date())
        let data15 = ReadPlotData(yyyymmdd: "25620415", date: "15 เมษายน 2562", ddmm: "15/4", day: 1, books: 0, pages: 0, hours: 0, minutes: 0, seconds: 0, dateUS: Date())
        let data16 = ReadPlotData(yyyymmdd: "25620416", date: "16 เมษายน 2562", ddmm: "16/4", day: 2, books: 2, pages: 25, hours: 0, minutes: 34, seconds: 39, dateUS: Date())
        let data17 = ReadPlotData(yyyymmdd: "25620417", date: "17 เมษายน 2562", ddmm: "17/4", day: 3, books: 1, pages: 15, hours: 0, minutes: 18, seconds: 23, dateUS: Date())
        let data18 = ReadPlotData(yyyymmdd: "25620418", date: "18 เมษายน 2562", ddmm: "18/4", day: 4, books: 1, pages: 10, hours: 0, minutes: 15, seconds: 49, dateUS: Date())
        let data19 = ReadPlotData(yyyymmdd: "25620419", date: "19 เมษายน 2562", ddmm: "19/4", day: 5, books: 1, pages: 5, hours: 0, minutes: 10, seconds: 11, dateUS: Date())
        let data20 = ReadPlotData(yyyymmdd: "25620420", date: "20 เมษายน 2562", ddmm: "20/4", day: 6, books: 2, pages: 35, hours: 1, minutes: 12, seconds: 17, dateUS: Date())
        let data21 = ReadPlotData(yyyymmdd: "25620421", date: "21 เมษายน 2562", ddmm: "21/4", day: 0, books: 1, pages: 7, hours: 0, minutes: 12, seconds: 8, dateUS: Date())
        let data22 = ReadPlotData(yyyymmdd: "25620422", date: "22 เมษายน 2562", ddmm: "22/4", day: 1, books: 1, pages: 15, hours: 0, minutes: 22, seconds: 46, dateUS: Date())
        let data23 = ReadPlotData(yyyymmdd: "25620423", date: "23 เมษายน 2562", ddmm: "23/4", day: 2, books: 1, pages: 5, hours: 0, minutes: 15, seconds: 12, dateUS: Date())
        let data24 = ReadPlotData(yyyymmdd: "25620424", date: "24 เมษายน 2562", ddmm: "24/4", day: 3, books: 1, pages: 12, hours: 0, minutes: 18, seconds: 34, dateUS: Date())
        let data25 = ReadPlotData(yyyymmdd: "25620425", date: "25 เมษายน 2562", ddmm: "25/4", day: 4, books: 1, pages: 5, hours: 0, minutes: 10, seconds: 23, dateUS: Date())
        let data26 = ReadPlotData(yyyymmdd: "25620426", date: "26 เมษายน 2562", ddmm: "26/4", day: 5, books: 2, pages: 25, hours: 1, minutes: 5, seconds: 17, dateUS: Date())
        let data27 = ReadPlotData(yyyymmdd: "25620427", date: "27 เมษายน 2562", ddmm: "27/4", day: 6, books: 1, pages: 3, hours: 0, minutes: 5, seconds: 31, dateUS: Date())
        let data28 = ReadPlotData(yyyymmdd: "25620428", date: "28 เมษายน 2562", ddmm: "28/4", day: 0, books: 1, pages: 11, hours: 0, minutes: 15, seconds: 29, dateUS: Date())
        let data29 = ReadPlotData(yyyymmdd: "25620429", date: "29 เมษายน 2562", ddmm: "29/4", day: 1, books: 1, pages: 35, hours: 0, minutes: 45, seconds: 30, dateUS: Date())
        let data30 = ReadPlotData(yyyymmdd: "25620430", date: "30 เมษายน 2562", ddmm: "30/4", day: 2, books: 1, pages: 9, hours: 0, minutes: 9, seconds: 11, dateUS: Date())
        
        let items = [data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11, data12, data13, data14, data15, data16, data17, data18, data19, data20, data21, data22, data23, data24, data25, data26, data27, data28, data29, data30]
        return items
    }
}


struct ReadPlotData {
    var yyyymmdd: String = ""
    var date: String = ""
    var ddmm: String = ""
    var day: Int = 0
    var books: Int = 0
    var pages: Int = 0
    var hours: Int = 0
    var minutes: Int = 0
    var seconds: Int = 0
    var dateUS: Date
    func toSeconds() -> CGFloat {
        return CGFloat(hours*3600 + minutes*60 + seconds)
    }
}
